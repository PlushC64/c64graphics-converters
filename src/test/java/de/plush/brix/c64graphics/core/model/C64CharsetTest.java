package de.plush.brix.c64graphics.core.model;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.stream.Stream;

import org.junit.*;

public class C64CharsetTest {

	@Test
	public void testLoadROM() throws Exception {
		Stream.of(C64Charset.ROM.values()).forEach(this::testLoadROM);
	}

	private void testLoadROM(final C64Charset.ROM rom) {
		try {
			Assert.assertNotNull("" + rom, C64Charset.load(rom));
		} catch (final RuntimeException e) {
			fail("" + rom, e);
		}
	}

}
