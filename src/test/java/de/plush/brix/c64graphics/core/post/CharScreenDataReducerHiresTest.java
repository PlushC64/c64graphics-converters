package de.plush.brix.c64graphics.core.post;

import java.awt.Point;
import java.util.*;

import org.junit.*;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.model.C64Charset.ROM;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.post.AbstractCharScreenDataReducerHires.*;
import de.plush.brix.c64graphics.core.util.charpixeldistance.C64CharHiresPixelDistance;
import de.plush.brix.c64graphics.core.util.collections.HashBag;

public class CharScreenDataReducerHiresTest {

	private C64Charset charsetUpperCase;
	private C64Charset charsetLowerCase;
	private C64Screen screenLowerCase;
	private C64Screen screenUpperCase;
	private C64CharsetScreen charScreenLowerCase;
	private C64CharsetScreen charScreenUpperCase;
	private C64Char lowerCaseA;
	private C64Char upperCaseA;
	private CharScreenDataReducerHires reducer;

	@Before
	public void setup() {
		charsetLowerCase = C64Charset.load(ROM.C64_LowerCase);
		screenLowerCase = CharsetFormats.size1x1.createScreen();
		charScreenLowerCase = new C64CharsetScreen(charsetLowerCase, screenLowerCase);

		charsetUpperCase = C64Charset.load(ROM.C64_UpperCase);
		screenUpperCase = CharsetFormats.size1x1.createScreen();
		charScreenUpperCase = new C64CharsetScreen(charsetUpperCase, screenUpperCase);

		final Point pointA = new Point(1, 0);
		lowerCaseA = charScreenLowerCase.blockAt(pointA);
		upperCaseA = charScreenUpperCase.blockAt(pointA);
		reducer = new CharScreenDataReducerHires(C64CharHiresPixelDistance.HAMMING, Prefer.SmallLocalImpact);
	}

	@Test
	public void testCreateLocator() throws Exception {
		final Locator locator = AbstractCharScreenDataReducerHires.createLocator(new Point(0, 1), charScreenLowerCase);
		Assert.assertTrue("contains charScreen as key", locator.locations.containsKey(charScreenLowerCase));
		Assert.assertTrue("contains point in charScreen", locator.locations.get(charScreenLowerCase).contains(new Point(0, 1)));
	}

	@Test
	public void testCreateLocators() throws Exception {
		final Map<C64Char, Locator> locators = AbstractCharScreenDataReducerHires.createLocators(charScreenLowerCase);

		final Point pointA = new Point(1, 0);
		Assert.assertTrue("lowerCaseA present as key", locators.containsKey(lowerCaseA));
		Assert.assertEquals("lowerCaseA  has one position", 1, locators.get(lowerCaseA).locations.size());
		Assert.assertEquals("lowerCaseA  has one position in charScreenLowerCase", 1, locators.get(lowerCaseA).blockPositions(charScreenLowerCase).size());
		Assert.assertEquals("position of lower case a", pointA, locators.get(lowerCaseA).blockPositions(charScreenLowerCase).iterator().next());
	}

	@Test
	public void testAllCharacterLocations() throws Exception {
		final Map<C64Char, Locator> allCharLocations = reducer.allCharacterLocations(List.of(charScreenLowerCase, charScreenUpperCase));

		Assert.assertEquals("lowerCaseA  has one position", 1, allCharLocations.get(lowerCaseA).locations.size());
		Assert.assertEquals("lowerCaseA  has one position in charScreenLowerCase", 1,
				allCharLocations.get(lowerCaseA).blockPositions(charScreenLowerCase).size());

		Assert.assertEquals("upperCaseA  has two positions", 2, allCharLocations.get(upperCaseA).locations.size());
		Assert.assertEquals("upperCaseA  has one position in charScreenLowerCase", 1,
				allCharLocations.get(upperCaseA).blockPositions(charScreenLowerCase).size());
		Assert.assertEquals("upperCaseA  has one position in charScreenUpperCase", 1,
				allCharLocations.get(upperCaseA).blockPositions(charScreenUpperCase).size());

		Assert.assertEquals("upperCaseA  position in charScreenUpperCase", new Point(1, 0),
				allCharLocations.get(upperCaseA).blockPositions(charScreenUpperCase).iterator().next());

		final int screenCodeUpperCaseA = charScreenLowerCase.charset().screenCodeOf(upperCaseA).getAsByte();
		Assert.assertEquals("upperCaseA  position in charScreenLowerCase", new Point(screenCodeUpperCaseA % 40, screenCodeUpperCaseA / 40),
				allCharLocations.get(upperCaseA).blockPositions(charScreenLowerCase).iterator().next());
	}

	@Test
	public void testNumberOfLocations() throws Exception {
		final Map<C64Char, Locator> allCharLocations = reducer.allCharacterLocations(List.of(charScreenLowerCase, charScreenUpperCase));
		Assert.assertEquals("upperCase A occurrences", 2, reducer.numberOfLocations(allCharLocations.get(upperCaseA)));
		Assert.assertEquals("lowerCase a occurrences", 1, reducer.numberOfLocations(allCharLocations.get(lowerCaseA)));
	}

	@Test
	public void testReplaceCharacterEverywhere() throws Exception {
		final Point pointA = new Point(1, 0);
		final C64Char victim = upperCaseA;
		final C64Char surrogate = lowerCaseA;

		final var substitution = new VictimToSurrogateMapping(victim, surrogate, null, 0f);
		final Locator victimLocator = AbstractCharScreenDataReducerHires.createLocator(pointA, charScreenUpperCase);
		final Locator surrogateLocator = AbstractCharScreenDataReducerHires.createLocator(pointA, charScreenLowerCase);

		final Map<C64Char, CharScreenDataReducerHires.Locator> chrToLocs = new LinkedHashMap<>();
		chrToLocs.put(victim, victimLocator);
		chrToLocs.put(surrogate, surrogateLocator);
		
		// Assert.assertEquals("before", victim, charScreenUpperCase.blockAt(pointA));
		reducer.replaceCharacterEverywhere(substitution, victimLocator, chrToLocs);
		// Assert.assertEquals("after", surrogate, charScreenUpperCase.blockAt(pointA));
		Assert.assertEquals("chrToLocs has victimPositions mapped to surrogate", 2, chrToLocs.get(surrogate).locations.size());
		Assert.assertFalse("chrToLocs has mapping for victimPositions removed", chrToLocs.containsKey(victim));
	}

	@Test
	public void testOccurrenceCounts() throws Exception {
		final Map<C64Char, Locator> allCharLocations = reducer.allCharacterLocations(List.of(charScreenLowerCase, charScreenUpperCase));
		final HashBag<C64Char> occurrenceCounts = reducer.occurrenceCounts(allCharLocations);
		Assert.assertEquals("lower case a occurrences", 1, occurrenceCounts.occurrencesOf(lowerCaseA));
		Assert.assertEquals("upper case A occurrences", 2, occurrenceCounts.occurrencesOf(upperCaseA));
	}

	@Test
	public void testReduce() throws Exception {
		final C64CharsetMultiScreen result = new CharScreenDataReducerHires(C64CharHiresPixelDistance.HAMMING_AND_FINE_CIRCULAR_DENSITY_CHANGE,
				Prefer.SmallLocalImpact)
				.reduce(List.of(charScreenLowerCase, charScreenUpperCase));
		Assert.assertTrue("charset size smaller $100", result.charsetSize() <= 256);
		Assert.assertEquals("number of result screens ", 2, result.screens().length);
		// System.out.println(result.screen(0));
		// System.out.println();
		// System.out.println(result.screen(1));
		//
		// new ImageDisplay<>(3f).show(new C64CharsetScreenToImageHires().apply(new C64CharsetScreen(result.charset(), result.screen(0))), "0");
		// new ImageDisplay<>(3f).show(new C64CharsetScreenToImageHires().apply(new C64CharsetScreen(result.charset(), result.screen(1))), "1");
		// //
		// System.in.read();
	}

}
