package de.plush.brix.c64graphics.core.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.plush.brix.c64graphics.core.model.PETSCII.Case;

public class PETSCIITest {

	@Test
	public void testCharToScreencodeLower() {
		assertEquals((byte) 0x00, PETSCII.charToScreencode('@', Case.Lower));
		assertEquals((byte) 0x01, PETSCII.charToScreencode('a', Case.Lower));
		assertEquals((byte) 0x1a, PETSCII.charToScreencode('z', Case.Lower));
		assertEquals((byte) 0x30, PETSCII.charToScreencode('0', Case.Lower));
		assertEquals((byte) 0x39, PETSCII.charToScreencode('9', Case.Lower));
		assertEquals((byte) 0x20, PETSCII.charToScreencode(' ', Case.Lower));
		assertEquals((byte) 0x3f, PETSCII.charToScreencode('?', Case.Lower));

		assertEquals((byte) 0x41, PETSCII.charToScreencode('A', Case.Lower));
		assertEquals((byte) 0x5a, PETSCII.charToScreencode('Z', Case.Lower));
	}

	@Test
	public void testCharToScreencodeUpper() {
		assertEquals((byte) 0, PETSCII.charToScreencode('@', Case.Upper));
		assertEquals((byte) 1, PETSCII.charToScreencode('A', Case.Upper));
		assertEquals((byte) 26, PETSCII.charToScreencode('Z', Case.Upper));
		assertEquals((byte) 0x30, PETSCII.charToScreencode('0', Case.Upper));
		assertEquals((byte) 0x39, PETSCII.charToScreencode('9', Case.Upper));
		assertEquals((byte) 0x20, PETSCII.charToScreencode(' ', Case.Lower));
		assertEquals((byte) 0x3f, PETSCII.charToScreencode('?', Case.Upper));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCharToScreencodeUpper_lowerCaseLetterNotFound() {
		assertEquals((byte) 0x41, PETSCII.charToScreencode('a', Case.Upper));
	}


}
