package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;
import static de.plush.brix.c64graphics.core.util.Utils.hiLoNibbleToByte;
import static org.junit.Assert.assertEquals;

import java.awt.Color;

import org.junit.*;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;

public class C64BitmapmodePictureMulticolorFliToImageTest {

	private C64BitmapmodePictureMulticolorFli c64MulticolorFLI;

	private C64BitmapmodePictureMulticolorFliToImage conv;

	@Before
	public void setUp() throws Exception {
		conv = new C64BitmapmodePictureMulticolorFliToImage(PALETTE);
		c64MulticolorFLI = createTestImage();
	}

	public static C64BitmapmodePictureMulticolorFli createTestImage() {
		final C64Screen screen0 = new C64Screen(hiLoNibbleToByte(GRAY.colorCode(), LIGHT_GRAY.colorCode()));
		final C64Screen screen1 = new C64Screen(hiLoNibbleToByte(BLUE.colorCode(), LIGHT_BLUE.colorCode()));
		final C64Screen screen2 = new C64Screen(hiLoNibbleToByte(GREEN.colorCode(), LIGHT_GREEN.colorCode()));
		final C64Screen screen3 = new C64Screen(hiLoNibbleToByte(RED.colorCode(), LIGHT_RED.colorCode()));
		final C64Screen screen4 = new C64Screen(BLACK.colorCode());
		final C64Screen screen5 = new C64Screen(BLACK.colorCode());
		final C64Screen screen6 = new C64Screen(BLACK.colorCode());
		final C64Screen screen7 = new C64Screen(BLACK.colorCode());
		final C64Screen[] screens = new C64Screen[] { screen0, screen1, screen2, screen3, screen4, screen5, screen6, screen7 };

		final C64ColorRam cram = new C64ColorRam(WHITE.colorCode());
		final byte backgroundColorCode = BLACK.colorCode();

		final C64Bitmap bitmap = new C64Bitmap(new C64Char(new byte[] { //
				(byte) 0b00_01_10_11, //
				(byte) 0b00_01_10_11, //
				(byte) 0b00_01_10_11, //
				(byte) 0b00_01_10_11, //

				0, //
				0, //
				0, //
				0//
		}));

		return new C64BitmapmodePictureMulticolorFli(bitmap, screens, cram, backgroundColorCode);
	}

	@Test
	public void testApply_SetsBackgroundColorCode() throws Exception {
		final PreProcessedMulticolorFliImage image = conv.apply(c64MulticolorFLI);
		assertEquals("code in image", image.backgroundColorCode(), BLACK.colorCode());
	}

	@Test
	public void testApply_PatternLine0() throws Exception {
		final PreProcessedMulticolorFliImage image = conv.apply(c64MulticolorFLI);
		assertEquals("00 pixel (bg)", new Color(image.getRGB(1, 0)), BLACK.color());
		assertEquals("01 pixel (screen,hi nibble)", new Color(image.getRGB(3, 0)), GRAY.color());
		assertEquals("10 pixel (screen, lo nibble)", new Color(image.getRGB(5, 0)), LIGHT_GRAY.color());
		assertEquals("11 pixel (cram)", new Color(image.getRGB(7, 0)), WHITE.color());
	}

	@Test
	public void testApply_PatternLine1() throws Exception {
		final PreProcessedMulticolorFliImage image = conv.apply(c64MulticolorFLI);
		assertEquals("00 pixel (bg)", new Color(image.getRGB(1, 1)), BLACK.color());
		assertEquals("01 pixel (screen,hi nibble)", new Color(image.getRGB(3, 1)), BLUE.color());
		assertEquals("10 pixel (screen, lo nibble)", new Color(image.getRGB(5, 1)), LIGHT_BLUE.color());
		assertEquals("11 pixel (cram)", new Color(image.getRGB(7, 1)), WHITE.color());
	}

	@Test
	public void testApply_PatternLine2() throws Exception {
		final PreProcessedMulticolorFliImage image = conv.apply(c64MulticolorFLI);
		assertEquals("00 pixel (bg)", new Color(image.getRGB(1, 2)), BLACK.color());
		assertEquals("01 pixel (screen,hi nibble)", new Color(image.getRGB(3, 2)), GREEN.color());
		assertEquals("10 pixel (screen, lo nibble)", new Color(image.getRGB(5, 2)), LIGHT_GREEN.color());
		assertEquals("11 pixel (cram)", new Color(image.getRGB(7, 2)), WHITE.color());
	}

	@Test
	public void testApply_PatternLine3() throws Exception {
		final PreProcessedMulticolorFliImage image = conv.apply(c64MulticolorFLI);
		assertEquals("00 pixel (bg)", new Color(image.getRGB(1, 3)), BLACK.color());
		assertEquals("01 pixel (screen,hi nibble)", new Color(image.getRGB(3, 3)), RED.color());
		assertEquals("10 pixel (screen, lo nibble)", new Color(image.getRGB(5, 3)), LIGHT_RED.color());
		assertEquals("11 pixel (cram)", new Color(image.getRGB(7, 3)), WHITE.color());
	}

}
