package de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces;

import static org.junit.Assert.assertEquals;

import java.awt.Color;

import org.junit.Test;

public class ColorConversionsTest {
	final Color gray = new Color(128, 128, 128);

	@Test
	public void testYUV() {
		final var converted = ColorConversions.convertRGBtoYUV(gray);
		final var reconverted = new Color(ColorConversions.convertYUVtoRGB(converted));
		assertEquals(gray, reconverted);
	}

	@Test
	public void testYCbCrJPEG() {
		final var converted = ColorConversions.convertRGBtoYCbCrJPEG(gray);
		final var reconverted = new Color(ColorConversions.convertYCbCrJPEGToRGB(converted));
		assertEquals(gray, reconverted);
	}

	@Test
	public void testYPbPrJPEG() {
		final var converted = ColorConversions.convertRGBtoYPbPr(gray);
		final var reconverted = new Color(ColorConversions.convertYPbPrtoRGB(converted));
		assertEquals(gray, reconverted);
	}
}
