package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;
import static org.junit.Assert.*;

import java.awt.Color;
import java.io.*;
import java.util.Set;

import org.junit.*;

import de.plush.brix.c64graphics.core.util.collections.ByteSet;

public class C64ColorsColodoreTest {

	private static PrintStream err;

	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

	private final PrintStream errStream = new PrintStream(errContent);

	@BeforeClass
	public static void saveStdErr() {
		err = System.err;
	}

	@AfterClass
	public static void restoreStdErr() {
		System.setErr(err);
	}

	@Before
	public void startCaptureStdErr() {
		System.setErr(errStream);
	}

	@After
	public void resetStdErrCapture() {
		errContent.reset();
	}

	@Test(expected = NullPointerException.class)
	public void testPalettel_colorClusters_null() {
		C64ColorsColodore.PALETTE.colorClusters(null);
		assertEquals("Logged a warning about an unkown ColorClusterIndexProvider", true, errContent.toString().contains("Unknown ColorClusterIndexProvider"));
	}

	@Test
	public void testPalette_colorClusters_VIC() {
		final Set<Set<Color>> colorClusters = C64ColorsColodore.PALETTE.colorClusters(ColorClusterIndexes.VIC);
		assertNotNull("colorClusters", colorClusters);
		assertEquals("colorClusters.size", 3, colorClusters.size());
		assertEquals("No Warning logged", true, errContent.toString().isEmpty());
	}

	@Test
	public void testPalette_colorClusters_VIC_clusterContent() {
		final Set<Set<Color>> colorClusters = C64ColorsColodore.PALETTE.colorClusters(ColorClusterIndexes.VIC);
		assertEquals("contains cluster of red, blue, brown and dark_gray", //
				true, //
				colorClusters.contains(Set.of(RED.color, BLUE.color, BROWN.color, DARK_GRAY.color)));
	}

	@Test
	public void testPalette_colorClusters_Custom() {
		final Set<Set<Color>> colorClusters = C64ColorsColodore.PALETTE.colorClusters(() -> Set.of(//
				ByteSet.ofUnsigned(0x06, 0x09), //
				ByteSet.ofUnsigned(0x03, 0x0f)//
		));
		assertNotNull("colorClusters", colorClusters);
		assertEquals("colorClusters.size", 2, colorClusters.size());
		assertEquals("contains cluster of blue and brown", //
				true, //
				colorClusters.contains(Set.of(BLUE.color, BROWN.color)));
		assertEquals("contains cluster of cyan and light gray", //
				true, //
				colorClusters.contains(Set.of(CYAN.color, LIGHT_GRAY.color)));

		assertEquals("Logged a warning about an unkown ColorClusterIndexProvider", true, errContent.toString().contains("Unknown ColorClusterIndexProvider"));
	}

	@Test
	public void testPalette_colorClusters_Custom_invalidColorCode() {
		final Set<Set<Color>> colorClusters = C64ColorsColodore.PALETTE.colorClusters(() -> Set.of(//
				ByteSet.ofUnsigned(0x06, 0xff), //
				ByteSet.ofUnsigned(0x03, 0x0f)//
		));
		assertNotNull("colorClusters", colorClusters);
		assertEquals("colorClusters.empty", true, colorClusters.isEmpty());
		System.out.println(errContent.toString());
		assertEquals("Logged a warning about an unkown ColorClusterIndexProvider", true, errContent.toString().contains("Unknown ColorClusterIndexProvider"));
		assertEquals("Logged a warning about an unkown Color", true, errContent.toString().contains("ff is not a valid color"));
	}
}
