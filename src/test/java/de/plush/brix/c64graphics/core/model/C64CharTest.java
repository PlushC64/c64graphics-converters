package de.plush.brix.c64graphics.core.model;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Point;

import org.junit.Test;

public class C64CharTest {

	@Test
	public void testReversed() throws Exception {
		final var c = new C64Char(new byte[] { //
				(byte) 0b01010101, //
				(byte) 0b11011011, //
				(byte) 0b11110000, //
				(byte) 0b10101010, //
				(byte) 0b01010101, //
				(byte) 0b11011011, //
				(byte) 0b11110000, //
				(byte) 0b11111111, //
		});
		final var reversed = new C64Char(new byte[] { //
				(byte) 0b10101010, //
				(byte) 0b00100100, //
				(byte) 0b00001111, //
				(byte) 0b01010101, //
				(byte) 0b10101010, //
				(byte) 0b00100100, //
				(byte) 0b00001111, //
				(byte) 0b00000000, //
		});
		assertEquals(reversed, c.reversed());
	}

	@Test
	public void testRolledPixelsUp() throws Exception {
		final var c = new C64Char(new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 });
		assertEquals(new C64Char(new byte[] { 1, 2, 3, 4, 5, 6, 7, 0 }), c.rolledPixelsUp(1));
		assertEquals(new C64Char(new byte[] { 2, 3, 4, 5, 6, 7, 0, 1 }), c.rolledPixelsUp(2));
		assertEquals(new C64Char(new byte[] { 3, 4, 5, 6, 7, 0, 1, 2 }), c.rolledPixelsUp(3));
		assertEquals(new C64Char(new byte[] { 4, 5, 6, 7, 0, 1, 2, 3 }), c.rolledPixelsUp(4));
		assertEquals(new C64Char(new byte[] { 5, 6, 7, 0, 1, 2, 3, 4 }), c.rolledPixelsUp(5));
		assertEquals(new C64Char(new byte[] { 6, 7, 0, 1, 2, 3, 4, 5 }), c.rolledPixelsUp(6));
		assertEquals(new C64Char(new byte[] { 7, 0, 1, 2, 3, 4, 5, 6 }), c.rolledPixelsUp(7));
		assertSame(c, c.rolledPixelsUp(8));
		assertEquals(new C64Char(new byte[] { 1, 2, 3, 4, 5, 6, 7, 0 }), c.rolledPixelsUp(9));

		assertEquals(new C64Char(new byte[] { 7, 0, 1, 2, 3, 4, 5, 6 }), c.rolledPixelsUp(-1));
	}

	@Test
	public void testRolledPixelsDown() throws Exception {
		final var c = new C64Char(new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 });
		assertEquals(new C64Char(new byte[] { 7, 0, 1, 2, 3, 4, 5, 6 }), c.rolledPixelsDown(1));
		assertEquals(new C64Char(new byte[] { 6, 7, 0, 1, 2, 3, 4, 5 }), c.rolledPixelsDown(2));
		assertEquals(new C64Char(new byte[] { 5, 6, 7, 0, 1, 2, 3, 4 }), c.rolledPixelsDown(3));
		assertEquals(new C64Char(new byte[] { 4, 5, 6, 7, 0, 1, 2, 3 }), c.rolledPixelsDown(4));
		assertEquals(new C64Char(new byte[] { 3, 4, 5, 6, 7, 0, 1, 2 }), c.rolledPixelsDown(5));
		assertEquals(new C64Char(new byte[] { 2, 3, 4, 5, 6, 7, 0, 1 }), c.rolledPixelsDown(6));
		assertEquals(new C64Char(new byte[] { 1, 2, 3, 4, 5, 6, 7, 0 }), c.rolledPixelsDown(7));
		assertSame(c, c.rolledPixelsDown(8));
		assertEquals(new C64Char(new byte[] { 7, 0, 1, 2, 3, 4, 5, 6 }), c.rolledPixelsDown(9));

		assertEquals(new C64Char(new byte[] { 1, 2, 3, 4, 5, 6, 7, 0 }), c.rolledPixelsDown(-1));
	}

	@Test
	public void testRolledPixelsLeft() throws Exception {
		final var c = new C64Char(new byte[] { //
				(byte) 0b10000000, //
				(byte) 0b01000000, //
				(byte) 0b00100000, //
				(byte) 0b00010000, //
				(byte) 0b00001000, //
				(byte) 0b00000100, //
				(byte) 0b00000010, //
				(byte) 0b00000001, //
		});
		assertEquals(new C64Char(new byte[] { //
				(byte) 0b00000001, //
				(byte) 0b10000000, //
				(byte) 0b01000000, //
				(byte) 0b00100000, //
				(byte) 0b00010000, //
				(byte) 0b00001000, //
				(byte) 0b00000100, //
				(byte) 0b00000010, //
		}), c.rolledPixelsLeft(1));
		assertEquals(new C64Char(new byte[] { //
				(byte) 0b00000100, //
				(byte) 0b00000010, //
				(byte) 0b00000001, //
				(byte) 0b10000000, //
				(byte) 0b01000000, //
				(byte) 0b00100000, //
				(byte) 0b00010000, //
				(byte) 0b00001000, //
		}), c.rolledPixelsLeft(3));
		assertSame(c, c.rolledPixelsLeft(8));
		assertEquals(c.rolledPixelsLeft(1), c.rolledPixelsLeft(9));

		assertEquals(c.rolledPixelsRight(1), c.rolledPixelsLeft(-1));
	}

	@Test
	public void testRolledPixelsRight() throws Exception {
		final var c = new C64Char(new byte[] { //
				(byte) 0b10000000, //
				(byte) 0b01000000, //
				(byte) 0b00100000, //
				(byte) 0b00010000, //
				(byte) 0b00001000, //
				(byte) 0b00000100, //
				(byte) 0b00000010, //
				(byte) 0b00000001, //
		});
		assertEquals(new C64Char(new byte[] { //
				(byte) 0b01000000, //
				(byte) 0b00100000, //
				(byte) 0b00010000, //
				(byte) 0b00001000, //
				(byte) 0b00000100, //
				(byte) 0b00000010, //
				(byte) 0b00000001, //
				(byte) 0b10000000, //
		}), c.rolledPixelsRight(1));
		assertEquals(new C64Char(new byte[] { //
				(byte) 0b00010000, //
				(byte) 0b00001000, //
				(byte) 0b00000100, //
				(byte) 0b00000010, //
				(byte) 0b00000001, //
				(byte) 0b10000000, //
				(byte) 0b01000000, //
				(byte) 0b00100000, //
		}), c.rolledPixelsRight(3));
		assertSame(c, c.rolledPixelsRight(8));
		assertEquals(c.rolledPixelsRight(1), c.rolledPixelsRight(9));

		assertEquals(c.rolledPixelsLeft(1), c.rolledPixelsRight(-1));
	}

	@Test
	public void testPixelsShiftedUp() throws Exception {
		final var c = new C64Char(new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 });
		assertEquals(new C64Char(new byte[] { 1, 2, 3, 4, 5, 6, 7, 0 }), c.shiftedPixelsUp(1));
		assertEquals(new C64Char(new byte[] { 2, 3, 4, 5, 6, 7, 0, 0 }), c.shiftedPixelsUp(2));
		assertEquals(new C64Char(new byte[] { 3, 4, 5, 6, 7, 0, 0, 0 }), c.shiftedPixelsUp(3));
		assertEquals(new C64Char(new byte[] { 4, 5, 6, 7, 0, 0, 0, 0 }), c.shiftedPixelsUp(4));
		assertEquals(new C64Char(new byte[] { 5, 6, 7, 0, 0, 0, 0, 0 }), c.shiftedPixelsUp(5));
		assertEquals(new C64Char(new byte[] { 6, 7, 0, 0, 0, 0, 0, 0 }), c.shiftedPixelsUp(6));
		assertEquals(new C64Char(new byte[] { 7, 0, 0, 0, 0, 0, 0, 0 }), c.shiftedPixelsUp(7));
		assertSame(C64Char.BLANK, c.shiftedPixelsUp(8));
		assertSame(C64Char.BLANK, c.shiftedPixelsUp(9));

		assertEquals(new C64Char(new byte[] { 0, 0, 1, 2, 3, 4, 5, 6 }), c.shiftedPixelsUp(-1));
		assertEquals(new C64Char(new byte[] { 0, 0, 0, 1, 2, 3, 4, 5 }), c.shiftedPixelsUp(-2));
	}

	@Test
	public void testPixelShiftedDown() throws Exception {
		final var c = new C64Char(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 });
		assertEquals(new C64Char(new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 }), c.shiftedPixelsDown(1));
		assertEquals(new C64Char(new byte[] { 0, 0, 1, 2, 3, 4, 5, 6 }), c.shiftedPixelsDown(2));
		assertEquals(new C64Char(new byte[] { 0, 0, 0, 1, 2, 3, 4, 5 }), c.shiftedPixelsDown(3));
		assertEquals(new C64Char(new byte[] { 0, 0, 0, 0, 1, 2, 3, 4 }), c.shiftedPixelsDown(4));
		assertEquals(new C64Char(new byte[] { 0, 0, 0, 0, 0, 1, 2, 3 }), c.shiftedPixelsDown(5));
		assertEquals(new C64Char(new byte[] { 0, 0, 0, 0, 0, 0, 1, 2 }), c.shiftedPixelsDown(6));
		assertEquals(new C64Char(new byte[] { 0, 0, 0, 0, 0, 0, 0, 1 }), c.shiftedPixelsDown(7));
		assertSame(C64Char.BLANK, c.shiftedPixelsDown(8));
		assertSame(C64Char.BLANK, c.shiftedPixelsDown(9));

		assertEquals(new C64Char(new byte[] { 2, 3, 4, 5, 6, 7, 8, 0 }), c.shiftedPixelsDown(-1));
		assertEquals(new C64Char(new byte[] { 3, 4, 5, 6, 7, 8, 0, 0 }), c.shiftedPixelsDown(-2));
	}

	@Test
	public void testShiftedPixelsLeft() throws Exception {
		final var c = new C64Char(new byte[] { //
				(byte) 0b11011011, //
				(byte) 0b00100100, //
				(byte) 0b11111111, //
				(byte) 0b10101010, //
				(byte) 0b11011011, //
				(byte) 0b00100100, //
				(byte) 0b11111111, //
				(byte) 0b10101010, //
		});
		assertEquals(new C64Char(new byte[] { //
				(byte) 0b10110110, //
				(byte) 0b01001000, //
				(byte) 0b11111110, //
				(byte) 0b01010100, //
				(byte) 0b10110110, //
				(byte) 0b01001000, //
				(byte) 0b11111110, //
				(byte) 0b01010100, //
		}), c.shiftedPixelsLeft(1));
		assertEquals(new C64Char(new byte[] { //
				(byte) 0b11011000, //
				(byte) 0b00100000, //
				(byte) 0b11111000, //
				(byte) 0b01010000, //
				(byte) 0b11011000, //
				(byte) 0b00100000, //
				(byte) 0b11111000, //
				(byte) 0b01010000, //
		}), c.shiftedPixelsLeft(3));
		assertSame(C64Char.BLANK, c.shiftedPixelsLeft(8));
		assertSame(C64Char.BLANK, c.shiftedPixelsLeft(9));

		assertEquals(c.shiftedPixelsRight(1), c.shiftedPixelsLeft(-1));
	}

	@Test
	public void testShiftedPixelsRight() throws Exception {
		final var c = new C64Char(new byte[] { //
				(byte) 0b11011011, //
				(byte) 0b00100100, //
				(byte) 0b11111111, //
				(byte) 0b10101010, //
				(byte) 0b11011011, //
				(byte) 0b00100100, //
				(byte) 0b11111111, //
				(byte) 0b10101010, //
		});
		assertEquals(new C64Char(new byte[] { //
				(byte) 0b01101101, //
				(byte) 0b00010010, //
				(byte) 0b01111111, //
				(byte) 0b01010101, //
				(byte) 0b01101101, //
				(byte) 0b00010010, //
				(byte) 0b01111111, //
				(byte) 0b01010101, //
		}), c.shiftedPixelsRight(1));
		assertEquals(new C64Char(new byte[] { //
				(byte) 0b00011011, //
				(byte) 0b00000100, //
				(byte) 0b00011111, //
				(byte) 0b00010101, //
				(byte) 0b00011011, //
				(byte) 0b00000100, //
				(byte) 0b00011111, //
				(byte) 0b00010101, //
		}), c.shiftedPixelsRight(3));
		assertSame(C64Char.BLANK, c.shiftedPixelsRight(8));
		assertSame(C64Char.BLANK, c.shiftedPixelsRight(9));

		assertEquals(c.shiftedPixelsLeft(1), c.shiftedPixelsRight(-1));
	}

	@Test
	public void testFlippedHorizontally() throws Exception {
		final var c = new C64Char(new byte[] { //
				(byte) 0b10000000, //
				(byte) 0b01000000, //
				(byte) 0b00100000, //
				(byte) 0b00010000, //
				(byte) 0b00011000, //
				(byte) 0b00100000, //
				(byte) 0b01000000, //
				(byte) 0b10000000, //
		});
		assertEquals(new C64Char(new byte[] { //
				(byte) 0b00000001, //
				(byte) 0b00000010, //
				(byte) 0b00000100, //
				(byte) 0b00001000, //
				(byte) 0b00011000, //
				(byte) 0b00000100, //
				(byte) 0b00000010, //
				(byte) 0b00000001, //
		}), c.flippedHorizontally());

		assertEquals(c, c.flippedHorizontally().flippedHorizontally());
	}

	@Test
	public void testFlippedVertically() throws Exception {
		final var c = new C64Char(new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 });
		assertEquals(new C64Char(new byte[] { 7, 6, 5, 4, 3, 2, 1, 0 }), c.flippedVertically());

		assertEquals(c, c.flippedVertically().flippedVertically());
	}

	@Test
	public void testIsPixelSet() {
		final var c = new C64Char(new byte[] { //
				(byte) 0b10000000, //
				(byte) 0b01000000, //
				(byte) 0b00100000, //
				(byte) 0b00010000, //
				(byte) 0b00011000, //
				(byte) 0b10000100, //
				(byte) 0b01000010, //
				(byte) 0b00100001, //
		});
		var p = new Point(0, 0);
		assertEquals(true, c.isPixelSet(p), p::toString);
		p = new Point(1, 1);
		assertEquals(true, c.isPixelSet(p), p::toString);
		p = new Point(6, 6);
		assertEquals(true, c.isPixelSet(p), p::toString);
		p = new Point(0, 5);
		assertEquals(true, c.isPixelSet(p), p::toString);
		p = new Point(1, 6);
		assertEquals(true, c.isPixelSet(p), p::toString);
		p = new Point(2, 7);
		assertEquals(true, c.isPixelSet(p), p::toString);
		p = new Point(7, 7);
		assertEquals(true, c.isPixelSet(p), p::toString);

		p = new Point(1, 0);
		assertEquals(false, c.isPixelSet(p), p::toString);
		p = new Point(0, 1);
		assertEquals(false, c.isPixelSet(p), p::toString);
		p = new Point(5, 2);
		assertEquals(false, c.isPixelSet(p), p::toString);
		p = new Point(7, 0);
		assertEquals(false, c.isPixelSet(p), p::toString);
		p = new Point(7, 6);
		assertEquals(false, c.isPixelSet(p), p::toString);

	}

	@Test
	public void testWithPixelSetPoint() {
		assertNotSame(C64Char.BLANK, C64Char.BLANK.withPixelSet(new Point(2, 3)));
		assertNotEquals(C64Char.BLANK, C64Char.BLANK.withPixelSet(new Point(2, 3)));
		assertEquals(true, C64Char.BLANK.withPixelSet(new Point(2, 3)).isPixelSet(new Point(2, 3)));
	}

	@Test
	public void testWithPixelSetPoint_charNotBlank() {
		final var c = new C64Char(new byte[] { //
				(byte) 0b10000000, //
				(byte) 0b01000000, //
				(byte) 0b00100000, //
				(byte) 0b00010000, //
				(byte) 0b00011000, //
				(byte) 0b10000100, //
				(byte) 0b01000010, //
				(byte) 0b00100001, //
		});
		assertEquals(c.withPixelSet(new Point(3, 2)), new C64Char(new byte[] { //
				(byte) 0b10000000, //
				(byte) 0b01000000, //
				(byte) 0b00110000, //
				(byte) 0b00010000, //
				(byte) 0b00011000, //
				(byte) 0b10000100, //
				(byte) 0b01000010, //
				(byte) 0b00100001, //
		}));
	}

	@Test
	public void testWithPixelSetPoint_bounds() {
		assertThrows(IllegalArgumentException.class, () -> C64Char.BLANK.withPixelSet(-1, 0), "x<0");
		assertThrows(IllegalArgumentException.class, () -> C64Char.BLANK.withPixelSet(8, 0), "x>7");
		assertThrows(IllegalArgumentException.class, () -> C64Char.BLANK.withPixelSet(0, -1), "y< 0");
		assertThrows(IllegalArgumentException.class, () -> C64Char.BLANK.withPixelSet(0, 8), "x<7");
	}

	@Test
	public void testWithPixelSetPoint_optimization() {
		assertSame(C64Char.FULL, C64Char.FULL.withPixelSet(1, 1));
		assertSame(C64Char.FULL, new C64Char(new byte[] { //
				(byte) 0b10111111, //
				(byte) 0b11111111, //
				(byte) 0b11111111, //
				(byte) 0b11111111, //
				(byte) 0b11111111, //
				(byte) 0b11111111, //
				(byte) 0b11111111, //
				(byte) 0b11111111, //
		}).withPixelSet(1, 0));
	}

	@Test
	public void testWithPixelDeletedPoint() {
		assertNotSame(C64Char.FULL, C64Char.FULL.withPixelDeleted(new Point(2, 3)));
		assertNotEquals(C64Char.FULL, C64Char.FULL.withPixelDeleted(new Point(2, 3)));
		assertEquals(false, C64Char.FULL.withPixelDeleted(new Point(2, 3)).isPixelSet(new Point(2, 3)));
	}

	@Test
	public void testWithPixelDeletedPoint_charNotBlank() {
		final var c = new C64Char(new byte[] { //
				(byte) 0b10000000, //
				(byte) 0b01000000, //
				(byte) 0b00100000, //
				(byte) 0b00010000, //
				(byte) 0b00011000, //
				(byte) 0b10000100, //
				(byte) 0b01000010, //
				(byte) 0b00100001, //
		});
		assertEquals(c.withPixelDeleted(new Point(1, 1)), new C64Char(new byte[] { //
				(byte) 0b10000000, //
				(byte) 0b00000000, //
				(byte) 0b00100000, //
				(byte) 0b00010000, //
				(byte) 0b00011000, //
				(byte) 0b10000100, //
				(byte) 0b01000010, //
				(byte) 0b00100001, //
		}));
	}

	@Test
	public void testWithPixelDeletedPoint_bounds() {
		assertThrows(IllegalArgumentException.class, () -> C64Char.BLANK.withPixelDeleted(-1, 0), "x<0");
		assertThrows(IllegalArgumentException.class, () -> C64Char.BLANK.withPixelDeleted(8, 0), "x>7");
		assertThrows(IllegalArgumentException.class, () -> C64Char.BLANK.withPixelDeleted(0, -1), "y< 0");
		assertThrows(IllegalArgumentException.class, () -> C64Char.BLANK.withPixelDeleted(0, 8), "x<7");
	}

	@Test
	public void testWithPixelDeletedPoint_optimization() {
		assertSame(C64Char.BLANK, C64Char.BLANK.withPixelDeleted(1, 1));
		assertSame(C64Char.BLANK, new C64Char(new byte[] { //
				(byte) 0b00000000, //
				(byte) 0b10000000, //
				(byte) 0b00000000, //
				(byte) 0b00000000, //
				(byte) 0b00000000, //
				(byte) 0b00000000, //
				(byte) 0b00000000, //
				(byte) 0b00000000, //
		}).withPixelDeleted(0, 1));
	}

}
