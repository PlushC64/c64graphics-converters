package de.plush.brix.c64graphics.core.util.collections;

import static java.util.Collections.synchronizedList;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

import de.plush.brix.c64graphics.core.util.*;

public class ByteArrayListTest {

	@Test
	public void testByteArrayList() throws Exception {
		final var list = new ByteArrayList();
		assertTrue(list.isEmpty());
		assertEquals(0, list.size());
	}

	@Test
	public void testByteArrayListByteArray() throws Exception {
		final var list = new ByteArrayList((byte) 0, (byte) 0, (byte) 0);
		assertFalse(list.isEmpty());
		assertEquals(3, list.size());
	}

	@Test
	public void testByteArrayListByteArray_arrayAndListNotConnected() throws Exception {
		final var bytes = new byte[] { 0, 0, 0 };
		final var list = new ByteArrayList(bytes);
		bytes[1] = (byte) 2;
		assertEquals(0, list.get(2));
		list.set(2, (byte) 44);
		assertEquals(0, bytes[2]);
	}

	@Test
	public void testEmpty() throws Exception {
		final var list = ByteArrayList.empty();
		assertTrue(list.isEmpty());
		assertEquals(0, list.size());
	}

	@Test
	public void testOfByteArray() throws Exception {
		final var list = ByteArrayList.of((byte) 0, (byte) 0, (byte) 0);
		assertFalse(list.isEmpty());
		assertEquals(3, list.size());
	}

	@Test
	public void testOfByteArray_arrayAndListNotConnected() throws Exception {
		final var bytes = new byte[] { 0, 0, 0 };
		final var list = ByteArrayList.of(bytes);
		bytes[1] = (byte) 2;
		assertEquals(0, list.get(2));
		list.set(2, (byte) 44);
		assertEquals(0, bytes[2]);
	}

	@Test(expected = NegativeArraySizeException.class)
	public void testOfNValues_negative_amount() throws Exception {
		ByteArrayList.ofNValues(-1, (byte) 10);
	}

	@Test
	public void testOfNValues_zero() throws Exception {
		final var list = ByteArrayList.ofNValues(0, (byte) 10);
		assertEquals(0, list.size());
	}

	@Test
	public void testOfNValues_nonZero() throws Exception {
		final var list = ByteArrayList.ofNValues(3, (byte) 10);
		assertEquals(ByteArrayList.of((byte) 10, (byte) 10, (byte) 10), list);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testGet_outOfBounds() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		list.get(3);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testGet_outOfBounds_negative() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		list.get(-1);
	}

	@Test
	public void testGet() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		assertEquals((byte) 11, list.get(0));
		assertEquals((byte) 22, list.get(1));
		assertEquals((byte) 33, list.get(2));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testSe_outOfBoundst() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		list.set(3, (byte) -223);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testSet_outOfBounds_negative() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		list.set(-1, (byte) -223);
	}

	@Test
	public void testAddByte() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		assertEquals(3, list.size());
		list.add((byte) 44);
		assertEquals(4, list.size());
	}

	@Test
	public void testAddByte_100times_growingCapacity() throws Exception {
		final var list = ByteArrayList.empty();
		// final int size = Integer.MAX_VALUE - 8; // max array length
		final int size = 100;
		for (int t = 0; t < size; ++t) {
			list.add((byte) (t & 0xff));
		}
		assertEquals(size, list.size());
		for (int t = 0; t < size; ++t) {
			assertEquals((byte) (t & 0xff), list.get(t));
		}
	}

	@Test
	public void testAddIntByte() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		list.add(1, (byte) 100);
		assertEquals(4, list.size());
		assertEquals((byte) 11, list.get(0));
		assertEquals((byte) 100, list.get(1));
		assertEquals((byte) 22, list.get(2));
		assertEquals((byte) 33, list.get(3));
	}

	@Test
	public void testAddIntByte_lastIndex() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		list.add(2, (byte) 100);
		assertEquals(4, list.size());
		assertEquals((byte) 11, list.get(0));
		assertEquals((byte) 22, list.get(1));
		assertEquals((byte) 100, list.get(2));
		assertEquals((byte) 33, list.get(3));
	}

	@Test
	public void testAddIntByte_lastIndexPlusOne() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		list.add(3, (byte) 100);
		assertEquals(4, list.size());
		assertEquals((byte) 11, list.get(0));
		assertEquals((byte) 22, list.get(1));
		assertEquals((byte) 33, list.get(2));
		assertEquals((byte) 100, list.get(3));
	}

	@Test
	public void testAddAllByteArrayList() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		final var list2 = ByteArrayList.of((byte) 44, (byte) 55, (byte) 66, (byte) 77, (byte) 88, (byte) 99, (byte) 0xaa, (byte) 0xbb);
		list.addAll(list2);
		assertEquals(11, list.size);
		assertEquals(ByteArrayList.of((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66, (byte) 77, (byte) 88, (byte) 99, (byte) 0xaa,
				(byte) 0xbb), list);
	}

	@Test
	public void testAddAllByteArrayList_emptyAdd() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		list.addAll(ByteArrayList.empty());
		assertEquals(3, list.size);
		assertEquals(ByteArrayList.of((byte) 11, (byte) 22, (byte) 33), list);
	}

	@Test
	public void testAddAllByteArrayList_adddToEmpty() throws Exception {
		final var list = ByteArrayList.empty();
		final var list2 = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		list.addAll(list2);
		assertEquals(3, list.size);
		assertEquals(ByteArrayList.of((byte) 11, (byte) 22, (byte) 33), list);
	}

	@Test
	public void testRemoveLast() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		list.removeLast();
		assertEquals(2, list.size());
		assertEquals((byte) 11, list.get(0));
		assertEquals((byte) 22, list.get(1));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testRemoveLast_emptyList() throws Exception {
		final var list = ByteArrayList.empty();
		list.removeLast();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testRemoveLastN_emptyList() throws Exception {
		final var list = ByteArrayList.empty();
		list.removeLastN(10);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testRemoveLastN_tooManyElements() throws Exception {
		final var list = ByteArrayList.of((byte) 1, (byte) 2, (byte) 3, (byte) 4);
		list.removeLastN(5);
	}

	@Test
	public void testRemoveLastN_allElements() throws Exception {
		final var list = ByteArrayList.of((byte) 1, (byte) 2, (byte) 3, (byte) 4);
		list.removeLastN(4);
		assertEquals("is empty", true, list.isEmpty());
	}

	@Test
	public void testRemoveLastN() throws Exception {
		final var list = ByteArrayList.of((byte) 1, (byte) 2, (byte) 3, (byte) 4, (byte) 5);
		list.removeLastN(3);
		assertEquals("remaining Eleemts", 2, list.size());
	}


	@Test
	public void testRemove() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		final byte removed = list.remove(1);
		assertEquals(2, list.size());
		assertEquals((byte) 11, list.get(0));
		assertEquals((byte) 22, removed);
		assertEquals((byte) 33, list.get(1));
	}

	@Test
	public void testRemove_lastIndex() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		final byte removed = list.remove(2);
		assertEquals(2, list.size());
		assertEquals((byte) 11, list.get(0));
		assertEquals((byte) 22, list.get(1));
		assertEquals((byte) 33, removed);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testRemove_empty() throws Exception {
		final var list = ByteArrayList.empty();
		list.remove(0);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testRemove_negativeIndex() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		list.remove(-1);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testRemove_IndexTooHigh() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		list.remove(3);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testRemoveN_emptyList() throws Exception {
		final var list = ByteArrayList.empty();
		list.removeN(1, 10);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testRemoveN_tooMany() throws Exception {
		final var list = ByteArrayList.of((byte) 1, (byte) 2, (byte) 3, (byte) 4, (byte) 5);
		list.removeN(0, 6);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testRemoveN_tooManyAfterIndex() throws Exception {
		final var list = ByteArrayList.of((byte) 1, (byte) 2, (byte) 3, (byte) 4, (byte) 5);
		list.removeN(2, 4);
	}

	@Test
	public void testRemoveN_exactlyAsManyAsAfterIndex() throws Exception {
		final var list = ByteArrayList.of((byte) 1, (byte) 2, (byte) 3, (byte) 4, (byte) 5);
		list.removeN(2, 3);
		assertEquals("remaining elements", 2, list.size());
	}

	@Test
	public void testRemoveN_first3() throws Exception {
		final var list = ByteArrayList.of((byte) 1, (byte) 2, (byte) 3, (byte) 4, (byte) 5);
		list.removeN(0, 3);
		assertEquals("remaining elements", 2, list.size());
		assertEquals("remaining elements", ByteArrayList.of((byte) 4, (byte) 5), list);
	}

	@Test
	public void testRemoveN() throws Exception {
		final var list = ByteArrayList.of((byte) 1, (byte) 2, (byte) 3, (byte) 4, (byte) 5);
		list.removeN(2, 2);
		assertEquals("remaining elements", 3, list.size());
		assertEquals("remaining elements", ByteArrayList.of((byte) 1, (byte) 2, (byte) 5), list);
	}


	@Test
	public void testIsEmpty() throws Exception {
		assertFalse(ByteArrayList.of((byte) 11, (byte) 22, (byte) 33).isEmpty());
		assertTrue(ByteArrayList.empty().isEmpty());
		final var list = ByteArrayList.of((byte) 11);
		list.remove(0);
		assertTrue(list.isEmpty());
		list.add((byte) 0);
		assertFalse(list.isEmpty());
	}

	@Test
	public void testHashCode_emptyList_isStable() throws Exception {
		final var list = ByteArrayList.empty();
		assertEquals(list.hashCode(), list.hashCode());
	}

	@Test
	public void testHashCode_non_empty_list_isStable() throws Exception {
		final var list = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		assertEquals(list.hashCode(), list.hashCode());
	}

	@Test
	public void testHashCode_equalListsShoudHaveSameHashCode() throws Exception {
		final var list1 = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		final var list2 = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		assertEquals(list1.hashCode(), list2.hashCode());
	}

	@Test
	public void testHashCode_equalListsOfDifferentCapacityShoudHaveSameHashCode() throws Exception {
		final var list1 = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		final var list2 = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		for (int t = 0; t < 100; ++t) {
			list2.add((byte) (t % 0xff)); // force capacity growth
		}
		for (int t = 0; t < 100; ++t) { // remove items again, capacity may not shrink
			list2.removeLast();
		}
		assertEquals(list1.hashCode(), list2.hashCode());
	}

	@Test // this cannot be guaranteed for every list, but should usually be the case
	public void testHashCode_notEqualForListsWithDifferentElements() throws Exception {
		final var list1 = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		final var list2 = ByteArrayList.of((byte) 11, (byte) 44, (byte) 33);
		assertNotEquals(list1.hashCode(), list2.hashCode());
	}

	@Test // this cannot be guaranteed for every list, but should usually be the case
	public void testHashCode_notEqualForListsWithEqualElementsInDifferentOrder() throws Exception {
		final var list1 = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		final var list2 = ByteArrayList.of((byte) 11, (byte) 33, (byte) 22);
		assertNotEquals(list1.hashCode(), list2.hashCode());
	}

	@Test
	public void testEqualsObject_null() throws Exception {
		final var list1 = ByteArrayList.empty();
		assertFalse(list1.equals((Object) null));
	}

	@Test
	public void testEqualsObject_sameObject() throws Exception {
		final var list1 = ByteArrayList.of((byte) 11, (byte) 22, (byte) 44);
		assertTrue(list1.equals(list1));
	}

	@Test
	public void testEqualsObject_emptyVsNonEmpty() throws Exception {
		final var list1 = ByteArrayList.empty();
		final var list2 = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		assertFalse(list1.equals(list2));
		assertFalse("equals should be symmetric", list2.equals(list1));
	}

	@Test
	public void testEqualsObject_equalLists() throws Exception {
		final var list1 = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		final var list2 = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		assertTrue(list1.equals(list2));
		assertTrue("equals should be symmetric", list2.equals(list1));
	}

	@Test
	public void testEqualsObject_equalListsOfDIfferentCapacity() throws Exception {
		final var list1 = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		final var list2 = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		for (int t = 0; t < 100; ++t) {
			list2.add((byte) (t % 0xff)); // force capacity growth
		}
		for (int t = 0; t < 100; ++t) { // remove items again, capacity may not shrink
			list2.removeLast();
		}
		assertTrue(list1.equals(list2));
		assertTrue("equals should be symmetric", list2.equals(list1));
	}

	@Test
	public void testEqualsObject_ListsWithDifferentElements() throws Exception {
		final var list1 = ByteArrayList.of((byte) 11, (byte) 22, (byte) 44);
		final var list2 = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		assertFalse(list1.equals(list2));
		assertFalse("equals should be symmetric", list2.equals(list1));
	}

	@Test
	public void testEqualsObject_ListsWithSameElementsInDifferentOrder() throws Exception {
		final var list1 = ByteArrayList.of((byte) 11, (byte) 22, (byte) 33);
		final var list2 = ByteArrayList.of((byte) 11, (byte) 33, (byte) 22);
		assertFalse(list1.equals(list2));
		assertFalse("equals should be symmetric", list2.equals(list1));
	}

	@Test
	public void testEqualsObject_anyExtendedClass() throws Exception {
		final var list1 = new ByteArrayList((byte) 11, (byte) 22, (byte) 44);
		final var list2 = new ByteArrayList((byte) 11, (byte) 22, (byte) 33) {
			@SuppressWarnings("unused")
			boolean isPopulated() {
				return !isEmpty();
			}
		};
		assertFalse(list1.equals(list2));
		assertFalse("equals should be symmetric", list2.equals(list1));
	}

	@Test
	public void testToArray() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33);
		final var arr = list.toArray();
		assertEquals(3, arr.length);
		assertEquals((byte) 11, arr[0]);
		assertEquals((byte) 22, arr[1]);
		assertEquals((byte) 33, arr[2]);
	}

	@Test
	public void testToArray_arrayDisconnectedFromList() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33);
		final var arr = list.toArray();
		arr[2] = (byte) 55;
		assertEquals((byte) 11, list.get(0));
		assertEquals((byte) 22, list.get(1));
		assertEquals((byte) 33, list.get(2));

		list.set(1, (byte) 66);
		assertEquals((byte) 22, arr[1]);
	}

	@Test
	public void testSort() {
		final var list = new ByteArrayList((byte) 66, (byte) -20, (byte) 11, (byte) 55, (byte) 33, (byte) -1);
		list.sort();
		assertEquals(ByteArrayList.of((byte) -20, (byte) -1, (byte) 11, (byte) 33, (byte) 55, (byte) 66), list);
	}

	@Test
	public void testSortUnsigned() {
		final var list = new ByteArrayList((byte) 66, (byte) 0xff, (byte) 0xa0, (byte) 55, (byte) 33, (byte) 44, (byte) 66);
		list.sortUnsigend();
		assertEquals(ByteArrayList.of((byte) 33, (byte) 44, (byte) 55, (byte) 66, (byte) 66, (byte) 0xa0, (byte) 0xff), list);
	}

	@Test
	public void testSubList() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 4);
		assertEquals(3, subList.size());
		assertEquals((byte) 22, subList.get(0));
		assertEquals((byte) 33, subList.get(1));
		assertEquals((byte) 44, subList.get(2));
	}

	@Test
	public void testEqualsByteSubList_parentNotEqualSublistIfSubListIsSmaller() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 4);
		assertFalse(list.equals(subList));
		assertFalse("equals should be symmetric", subList.equals(list));
	}

	@Test
	public void testEqualsByteSubList_parentEqualSublistIfSubListIsSameSize() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(0, list.size());
		assertTrue(list.equals(subList));
		assertTrue("equals should be symmetric", subList.equals(list));
	}

	@Test
	public void testEqualsByteSubList_differentParents_equalSubListContent() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var list2 = new ByteArrayList((byte) 111, (byte) 22, (byte) 33, (byte) 44, (byte) 77, (byte) 88);
		final var subList1 = list.subList(1, 4);
		final var subList2 = list2.subList(1, 4);
		assertTrue(subList1.equals(subList2));
		assertTrue("equals should be symmetric", subList2.equals(subList1));
	}

	@Test
	public void testEqualsByteSubList_subListEqualToAnotherList() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 4);
		final var list2 = new ByteArrayList((byte) 22, (byte) 33, (byte) 44);
		assertTrue(subList.equals(list2));
		assertTrue("equals should be symmetric", list2.equals(subList));
	}

	@Test
	public void testSubList_contentChangesIfParentChangesWithinView_bySetInsideView() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 4);
		assertEquals((byte) 22, subList.get(0));
		assertEquals((byte) 33, subList.get(1));
		assertEquals((byte) 44, subList.get(2));
		list.set(2, (byte) 77);
		assertEquals((byte) 22, subList.get(0));
		assertEquals((byte) 77, subList.get(1));
		assertEquals((byte) 44, subList.get(2));
		assertEquals(3, subList.size());
	}

	@Test
	public void testSubList_contentChangesIfParentChangesWithinView_byAddInsideView() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 4);

		assertEquals((byte) 22, subList.get(0));
		assertEquals((byte) 33, subList.get(1));
		assertEquals((byte) 44, subList.get(2));
		list.add(2, (byte) 77);
		assertEquals((byte) 22, subList.get(0));
		assertEquals((byte) 77, subList.get(1));
		assertEquals((byte) 33, subList.get(2));
		assertEquals(3, subList.size());
	}

	@Test
	public void testSubList_contentChangesIfParentChangesWithinView_byAddBeforeView() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 4);

		assertEquals((byte) 22, subList.get(0));
		assertEquals((byte) 33, subList.get(1));
		assertEquals((byte) 44, subList.get(2));
		list.add(0, (byte) 77);
		assertEquals((byte) 11, subList.get(0));
		assertEquals((byte) 22, subList.get(1));
		assertEquals((byte) 33, subList.get(2));
		assertEquals(3, subList.size());
	}

	@Test
	public void testSubList_contentDoesNotChangeIfParentChangesOutsideView_bySetBeforeView() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 4);

		assertEquals((byte) 22, subList.get(0));
		assertEquals((byte) 33, subList.get(1));
		assertEquals((byte) 44, subList.get(2));
		list.set(0, (byte) 77);
		assertEquals((byte) 22, subList.get(0));
		assertEquals((byte) 33, subList.get(1));
		assertEquals((byte) 44, subList.get(2));
		assertEquals(3, subList.size());
	}

	@Test
	public void testSubList_contentDoesNotChangeIfParentChangesOutsideView_bySetAfterView() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 4);

		assertEquals((byte) 22, subList.get(0));
		assertEquals((byte) 33, subList.get(1));
		assertEquals((byte) 44, subList.get(2));
		list.set(4, (byte) 77);
		assertEquals((byte) 22, subList.get(0));
		assertEquals((byte) 33, subList.get(1));
		assertEquals((byte) 44, subList.get(2));
		assertEquals(3, subList.size());
	}

	@Test
	public void testSubList_contentDoedNotChangesIfParentChangesOutsideView_byAddAfterView() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 4);

		assertEquals((byte) 22, subList.get(0));
		assertEquals((byte) 33, subList.get(1));
		assertEquals((byte) 44, subList.get(2));
		list.add(4, (byte) 77);
		assertEquals((byte) 22, subList.get(0));
		assertEquals((byte) 33, subList.get(1));
		assertEquals((byte) 44, subList.get(2));
		assertEquals(3, subList.size());
	}

	@Test
	public void testSubList_backingListChangedIfSubListChanged_add() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 4);

		assertEquals(ByteArrayList.of((byte) 22, (byte) 33, (byte) 44), subList);

		subList.add((byte) 77);

		assertEquals(ByteArrayList.of((byte) 22, (byte) 33, (byte) 44, (byte) 77), subList);
		assertEquals(ByteArrayList.of((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 77, (byte) 55, (byte) 66), list);
		assertEquals(4, subList.size());
		assertEquals(7, list.size());
	}

	@Test
	public void testSubList_backingListChangedIfSubListChanged_addInt() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 4);

		assertEquals(ByteArrayList.of((byte) 22, (byte) 33, (byte) 44), subList);

		subList.add(1, (byte) 77);

		assertEquals(ByteArrayList.of((byte) 22, (byte) 77, (byte) 33, (byte) 44), subList);
		assertEquals(ByteArrayList.of((byte) 11, (byte) 22, (byte) 77, (byte) 33, (byte) 44, (byte) 55, (byte) 66), list);
		assertEquals(4, subList.size());
		assertEquals(7, list.size());
	}

	@Test
	public void testSubList_backingListChangedIfSubListChanged_set() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 4);

		assertEquals(ByteArrayList.of((byte) 22, (byte) 33, (byte) 44), subList);

		subList.set(1, (byte) 77);

		assertEquals(ByteArrayList.of((byte) 22, (byte) 77, (byte) 44), subList);
		assertEquals(ByteArrayList.of((byte) 11, (byte) 22, (byte) 77, (byte) 44, (byte) 55, (byte) 66), list);
		assertEquals(3, subList.size());
		assertEquals(6, list.size());
	}

	@Test
	public void testSubList_backingListChangedIfSubListChanged_removeLast() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 4);

		assertEquals(ByteArrayList.of((byte) 22, (byte) 33, (byte) 44), subList);

		subList.removeLast();

		assertEquals(ByteArrayList.of((byte) 22, (byte) 33), subList);
		assertEquals(ByteArrayList.of((byte) 11, (byte) 22, (byte) 33, (byte) 55, (byte) 66), list);
		assertEquals(2, subList.size());
		assertEquals(5, list.size());
	}

	@Test
	public void testSubList_backingListChangedIfSubListChanged_remove() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 4);

		assertEquals(ByteArrayList.of((byte) 22, (byte) 33, (byte) 44), subList);

		subList.remove(1);

		assertEquals(ByteArrayList.of((byte) 22, (byte) 44), subList);
		assertEquals(ByteArrayList.of((byte) 11, (byte) 22, (byte) 44, (byte) 55, (byte) 66), list);
		assertEquals(2, subList.size());
		assertEquals(5, list.size());
	}

	@Test
	public void testSubList_AddAllByteArrayList() throws Exception {
		final var list = ByteArrayList.of((byte) 4, (byte) 5, (byte) 6, (byte) 7, (byte) 8, (byte) 9, (byte) 0xa, (byte) 0xb);
		final var sublist = list.subList(1, 4);
		assertEquals(ByteArrayList.of((byte) 5, (byte) 6, (byte) 7), sublist);

		sublist.addAll(ByteArrayList.of((byte) 1, (byte) 2, (byte) 3));

		assertEquals(ByteArrayList.of((byte) 5, (byte) 6, (byte) 7, (byte) 1, (byte) 2, (byte) 3), sublist);
		assertEquals(6, sublist.size);
		assertEquals(ByteArrayList.of((byte) 4, (byte) 5, (byte) 6, (byte) 7, (byte) 1, (byte) 2, (byte) 3, (byte) 8, (byte) 9, (byte) 0xa, (byte) 0xb), list);
		assertEquals(11, list.size);
	}

	@Test
	public void testSubList_AddAllByteArrayList_emptyAdd() throws Exception {
		final var list = ByteArrayList.of((byte) 4, (byte) 5, (byte) 6, (byte) 7, (byte) 8, (byte) 9, (byte) 0xa, (byte) 0xb);
		final var sublist = list.subList(1, 5);
		sublist.addAll(ByteArrayList.empty());
		assertEquals(4, sublist.size);
		assertEquals(8, list.size);
		assertEquals(ByteArrayList.of((byte) 5, (byte) 6, (byte) 7, (byte) 8), sublist);
		assertEquals(ByteArrayList.of((byte) 4, (byte) 5, (byte) 6, (byte) 7, (byte) 8, (byte) 9, (byte) 0xa, (byte) 0xb), list);
	}

	@Test
	public void testSubList_AddAllByteArrayList_addToEmpty() throws Exception {
		final var list = ByteArrayList.of((byte) 4, (byte) 5, (byte) 6, (byte) 7, (byte) 8, (byte) 9, (byte) 0xa, (byte) 0xb);
		final var sublist = list.subList(1, 1);
		assertTrue(sublist.isEmpty());

		sublist.addAll(ByteArrayList.of((byte) 5, (byte) 6, (byte) 7, (byte) 8));
		assertEquals(4, sublist.size);
		assertEquals(12, list.size);

		assertEquals(ByteArrayList.of((byte) 4, (byte) 5, (byte) 6, (byte) 7, (byte) 8, (byte) 5, (byte) 6, (byte) 7, (byte) 8, (byte) 9, (byte) 0xa,
				(byte) 0xb), list);
	}

	@Test
	public void testSubList_AddAllIntByteArrayList() throws Exception {
		final var list = ByteArrayList.of((byte) 4, (byte) 5, (byte) 6, (byte) 7, (byte) 8, (byte) 9, (byte) 0xa, (byte) 0xb);
		final var sublist = list.subList(1, 4);
		assertEquals(ByteArrayList.of((byte) 5, (byte) 6, (byte) 7), sublist);

		sublist.addAll(2, ByteArrayList.of((byte) 1, (byte) 2, (byte) 3));

		assertEquals(ByteArrayList.of((byte) 5, (byte) 6, (byte) 1, (byte) 2, (byte) 3, (byte) 7), sublist);
		assertEquals(6, sublist.size);
		assertEquals(ByteArrayList.of((byte) 4, (byte) 5, (byte) 6, (byte) 1, (byte) 2, (byte) 3, (byte) 7, (byte) 8, (byte) 9, (byte) 0xa, (byte) 0xb), list);
		assertEquals(11, list.size);
	}

	@Test
	public void testSubList_hashCode_empty() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 1);
		assertEquals(ByteArrayList.empty().hashCode(), subList.hashCode());
	}

	@Test
	public void testSubList_hashCode_equalsEqualByteList() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 4);
		assertEquals(ByteArrayList.of((byte) 22, (byte) 33, (byte) 44).hashCode(), subList.hashCode());
	}

	@Test
	public void testSubList_toArray_empty() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 1);
		assertArrayEquals(new byte[0], subList.toArray());
	}

	@Test
	public void testSubList_toArray_withContent() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		final var subList = list.subList(1, 4);
		assertArrayEquals(new byte[] { (byte) 22, (byte) 33, (byte) 44 }, subList.toArray());
	}

	@Test
	public void testSubList_Sort() {
		final var list = new ByteArrayList((byte) 66, (byte) -20, (byte) 11, (byte) 55, (byte) 33, (byte) -1);
		final var subList = list.subList(1, 5);
		subList.sort();
		assertEquals(ByteArrayList.of((byte) -20, (byte) 11, (byte) 33, (byte) 55), subList);
		assertEquals(ByteArrayList.of((byte) 66, (byte) -20, (byte) 11, (byte) 33, (byte) 55, (byte) -1), list);
	}

	@Test
	public void testSubList_SortUnsigned() {
		final var list = new ByteArrayList((byte) 66, (byte) 0xff, (byte) 0xa0, (byte) 55, (byte) 33, (byte) 44, (byte) 66);
		final var subList = list.subList(1, 5);
		assertEquals(ByteArrayList.of((byte) 0xff, (byte) 0xa0, (byte) 55, (byte) 33), subList);
		subList.sortUnsigend();
		assertEquals(ByteArrayList.of((byte) 33, (byte) 55, (byte) 0xa0, (byte) 0xff), subList);
		assertEquals(ByteArrayList.of((byte) 66, (byte) 33, (byte) 55, (byte) 0xa0, (byte) 0xff, (byte) 44, (byte) 66), list);
	}

	@Test
	public void testSubList_isEmpty_empty() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		assertTrue(list.subList(1, 1).isEmpty());
	}

	@Test
	public void testSubList_isEmpty_withContent() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66);
		assertFalse(list.subList(1, 4).isEmpty());
	}

	@Test
	public void testSubList_smokeTest() {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 55, (byte) 66, (byte) 77);
		final var subList = list.subList(1, 6);
		final var subListOfSubList = subList.subList(1, 3);

		assertArrayEquals(ByteArrayList.of((byte) 33, (byte) 44).toArray(), subListOfSubList.toArray());
		assertTrue(subListOfSubList.equals(ByteArrayList.of((byte) 33, (byte) 44)));
		assertTrue("equals should be symmetric", ByteArrayList.of((byte) 33, (byte) 44).equals(subListOfSubList));

		subListOfSubList.add((byte) 88);
		assertEquals(ByteArrayList.of((byte) 33, (byte) 44, (byte) 88), subListOfSubList);
		assertEquals(ByteArrayList.of((byte) 22, (byte) 33, (byte) 44, (byte) 88, (byte) 55, (byte) 66), subList);
		assertEquals(ByteArrayList.of((byte) 11, (byte) 22, (byte) 33, (byte) 44, (byte) 88, (byte) 55, (byte) 66, (byte) 77), list);

		subListOfSubList.remove(0);
		assertEquals(ByteArrayList.of((byte) 44, (byte) 88), subListOfSubList);
		assertEquals(ByteArrayList.of((byte) 22, (byte) 44, (byte) 88, (byte) 55, (byte) 66), subList);
		assertEquals(ByteArrayList.of((byte) 11, (byte) 22, (byte) 44, (byte) 88, (byte) 55, (byte) 66, (byte) 77), list);

	}

	@Test
	public void testContains() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 77, (byte) 44, (byte) 55, (byte) 66, (byte) 33);
		assertTrue(list.contains((byte) 55));
		assertFalse(list.contains((byte) 88));
		assertFalse(ByteArrayList.empty().contains((byte) 11));
	}

	@Test
	public void testSublist_Contains() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 77, (byte) 44, (byte) 55, (byte) 66, (byte) 33);
		final var sublist = list.subList(2, 4);
		assertTrue(sublist.contains((byte) 44));
		assertFalse(sublist.contains((byte) 88));
		assertFalse(ByteArrayList.empty().subList(0, 0).contains((byte) 11));
	}

	@Test
	public void testFilter() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 77, (byte) 44, (byte) 55, (byte) 66, (byte) 33);
		final var filtered = list.filter(b -> b > 40);
		assertEquals(ByteArrayList.of((byte) 77, (byte) 44, (byte) 55, (byte) 66), filtered);

		assertEquals(0, ByteArrayList.empty().filter(b -> b <= 10).size());
	}

	@Test
	public void testReject() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 77, (byte) 44, (byte) 55, (byte) 66, (byte) 33);
		final var filtered = list.reject(b -> b > 40);
		assertEquals(ByteArrayList.of((byte) 11, (byte) 22, (byte) 33), filtered);

		assertEquals(0, ByteArrayList.empty().reject(b -> b <= 10).size());
	}

	@Test
	public void testForEach() throws Exception {
		final var set = ByteArrayList.empty();
		final var invocations = new AtomicInteger(0);
		set.forEach(b -> invocations.incrementAndGet());
		assertEquals(0, invocations.get());
	}

	@Test
	public void testForEach_nonEmpty() throws Exception {
		final var found = synchronizedList(new ArrayList<Byte>());
		final var list = ByteArrayList.of((byte) 80, (byte) 10, (byte) 10, (byte) 10, (byte) 101, (byte) 110);

		list.forEach(b -> found.add(b));

		assertEquals(6, found.size());
		assertEquals(List.of((byte) 80, (byte) 10, (byte) 10, (byte) 10, (byte) 101, (byte) 110), found);
	}

	@Test
	public void testSublist_ForEach_nonEmpty() throws Exception {
		final var found = synchronizedList(new ArrayList<Byte>());
		final var list = ByteArrayList.of((byte) 80, (byte) 10, (byte) 10, (byte) 10, (byte) 101, (byte) 110);
		final var sublist = list.subList(2, 5);

		sublist.forEach(b -> found.add(b));

		assertEquals(3, found.size());
		assertEquals(List.of((byte) 10, (byte) 10, (byte) 101), found);
	}

	@Test
	public void testMapToInt() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 0xff, (byte) 0xaa, (byte) 55, (byte) 66, (byte) 33);
		final var mappedArr = list.mapToInt(Utils::toPositiveInt).toArray();
		assertArrayEquals(new int[] { 11, 22, 0xff, 0xaa, 55, 66, 33 }, mappedArr);

		assertArrayEquals(new int[0], ByteArrayList.empty().mapToInt(b -> b).toArray());
	}

	@Test
	public void testMapToObj() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) 0xff, (byte) 0xaa, (byte) 55, (byte) 66, (byte) 33);
		final var mapped = list.mapToObj(Utils::toPositiveInt).collect(toList());
		assertEquals(List.of(11, 22, 0xff, 0xaa, 55, 66, 33), mapped);

		assertEquals(List.of(), ByteArrayList.empty().mapToObj(b -> b).collect(toList()));
	}

	@Test
	public void testAnySatisfy() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) -1, (byte) -10, (byte) 55, (byte) 66, (byte) 33);
		assertTrue(list.anySatisfy(b -> b < 0));
		assertFalse(list.anySatisfy(b -> b > 70));
		assertFalse(ByteArrayList.empty().anySatisfy(b -> true));
	}

	@Test
	public void testAllSatisfy() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) -1, (byte) -10, (byte) 55, (byte) 66, (byte) 33);
		assertTrue(list.allSatisfy(b -> b < 70));
		assertFalse(list.allSatisfy(b -> b > 0));
		assertFalse(ByteArrayList.empty().allSatisfy(b -> true));
	}

	@Test
	public void testNoneSatisfy() throws Exception {
		final var list = new ByteArrayList((byte) 11, (byte) 22, (byte) -1, (byte) -10, (byte) 55, (byte) 66, (byte) 33);
		assertFalse(list.noneSatisfy(b -> b < 70));
		assertTrue(list.noneSatisfy(b -> b > 70));
		assertTrue(ByteArrayList.empty().noneSatisfy(b -> false));
	}

	@Test
	public void testToString() throws Exception {
		assertEquals("ByteArrayList [bytes=16,21,ff, size=3]", ByteArrayList.of((byte) 22, (byte) 33, (byte) 0xff).toString());
		assertEquals("ByteArrayList [bytes=21,ff, size=2]", ByteArrayList.of((byte) 22, (byte) 33, (byte) 0xff).subList(1, 3).toString());
	}

	@Test
	public void testZip_bothSameSize() throws Exception {
		final var list = new ByteArrayList((byte) 1, (byte) 2, (byte) 3);
		final var list2 = List.of("a", "b", "c");
		final var result = list.zip(list2);

		assertEquals(List.of(//
				ByteObjectPair.of((byte) 1, "a")//
				, ByteObjectPair.of((byte) 2, "b")//
				, ByteObjectPair.of((byte) 3, "c")//
		), result);
	}

	@Test
	public void testZip_otherSizeLarger() throws Exception {
		final var list = new ByteArrayList((byte) 1, (byte) 2, (byte) 3);
		final var list2 = List.of("a", "b", "c", "d");
		final var result = list.zip(list2);

		assertEquals(List.of(//
				ByteObjectPair.of((byte) 1, "a")//
				, ByteObjectPair.of((byte) 2, "b")//
				, ByteObjectPair.of((byte) 3, "c")//
		), result);
	}

	@Test
	public void testZip_otherSizeSmaller() throws Exception {
		final var list = new ByteArrayList((byte) 1, (byte) 2, (byte) 3, (byte) 4);
		final var list2 = List.of("a", "b", "c");
		final var result = list.zip(list2);

		assertEquals(List.of(//
				ByteObjectPair.of((byte) 1, "a")//
				, ByteObjectPair.of((byte) 2, "b")//
				, ByteObjectPair.of((byte) 3, "c")//
		), result);
	}

	@Test
	public void testSublist_zip_SameSize() throws Exception {
		final var list = new ByteArrayList((byte) 1, (byte) 2, (byte) 3, (byte) 4, (byte) 5, (byte) 6);
		final var sublist = list.subList(2, 5);

		final var list2 = List.of("a", "b", "c");
		final var result = sublist.zip(list2);

		assertEquals(List.of(//
				ByteObjectPair.of((byte) 3, "a")//
				, ByteObjectPair.of((byte) 4, "b")//
				, ByteObjectPair.of((byte) 5, "c")//
		), result);
	}

}
