package de.plush.brix.c64graphics.core.util;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.*;

import java.io.*;
import java.util.*;
import java.util.function.IntFunction;
import java.util.stream.Stream;

import org.junit.*;

import de.plush.brix.c64graphics.core.util.collections.IntArrayList;

@SuppressWarnings({ "static-method", "boxing", "resource" })
public class UtilsTest {

	@Test
	public final void testToHexString() {
		assertEquals("ff", Utils.toHexString((byte) 0xff));
		assertEquals("80", Utils.toHexString((byte) 0x80));
		assertEquals("0a", Utils.toHexString((byte) 0x0a));
	}

	@Test
	public final void testToHexStringInt() {
		assertEquals("ff", Utils.toHexString(0xff));
		assertEquals("80", Utils.toHexString(0x80));
		assertEquals("0100", Utils.toHexString(0x0100));
		assertEquals("1000", Utils.toHexString(0x1000));
		assertEquals("00010000", Utils.toHexString(0x10000));
		assertEquals("01000000", Utils.toHexString(0x1000000));
		assertEquals("10000000", Utils.toHexString(0x10000000));
	}

	@Test
	public final void testToBinaryString() {
		assertEquals("11111111", Utils.toBinaryString((byte) 0xff));
		assertEquals("10000000", Utils.toBinaryString((byte) 0x80));
		assertEquals("10101010", Utils.toBinaryString((byte) 0xaa));
		assertEquals("00001010", Utils.toBinaryString((byte) 0x0a));
	}

	@Test
	public final void subsetStream() {
		final List<ArrayList<Integer>> subsets = Utils.subsetStream(asList(1, 2, 3), 2, ArrayList::new)//
				.collect(toList());
		assertEquals("subsets.size()", 3, subsets.size());
		assertEquals("contains (1,2) || (2,1) ", true, subsets.contains(asList(1, 2)) || subsets.contains(asList(2, 1)));
		assertEquals("contains (1,3) || (3,1) ", true, subsets.contains(asList(1, 3)) || subsets.contains(asList(3, 1)));
		assertEquals("contains (2,3) || (3,2) ", true, subsets.contains(asList(2, 3)) || subsets.contains(asList(3, 2)));
	}

	@Test
	public final void testRollLeft_0() {
		final byte[] array = new byte[] { 1, 2, 3 };
		Utils.rollLeft(array, 1);
		assertArrayEquals(new byte[] { 2, 3, 1 }, array);
	}

	@Test
	public final void testRollLeft_1() {
		final byte[] array = new byte[] { 1, 2, 3 };
		Utils.rollLeft(array, 0);
		assertArrayEquals(new byte[] { 1, 2, 3 }, array);
	}

	@Test
	public final void testRollLeft_2() {
		final byte[] array = new byte[] { 1, 2, 3 };
		Utils.rollLeft(array, 2);
		assertArrayEquals(new byte[] { 3, 1, 2 }, array);
	}

	@Test
	public final void testRollLeft_3() {
		final byte[] array = new byte[] { 1, 2, 3 };
		Utils.rollLeft(array, 3);
		assertArrayEquals(new byte[] { 1, 2, 3 }, array);
	}

	@Test
	public final void testRollLeft_empty() {
		final byte[] array = new byte[] {};
		Utils.rollLeft(array, 2);
		assertArrayEquals(new byte[] {}, array);
	}

	@Test
	public final void testRollLeft_single() {
		final byte[] array = new byte[] { 1 };
		Utils.rollLeft(array, 2);
		assertArrayEquals(new byte[] { 1 }, array);
	}

	@Test
	public final void testRollLeft_rollMoreOftenThanElements() {
		final byte[] array = new byte[] { 1, 2, 3 };
		Utils.rollLeft(array, 4);
		assertArrayEquals(new byte[] { 2, 3, 1 }, array);
	}

	@Test
	public final void testRollRight_0() {
		final byte[] array = new byte[] { 1, 2, 3 };
		Utils.rollRight(array, 0);
		assertArrayEquals(new byte[] { 1, 2, 3 }, array);
	}

	@Test
	public final void testRollRight_1() {
		final byte[] array = new byte[] { 1, 2, 3 };
		Utils.rollRight(array, 1);
		assertArrayEquals(new byte[] { 3, 1, 2 }, array);
	}

	@Test
	public final void testRollRight_2() {
		final byte[] array = new byte[] { 1, 2, 3 };
		Utils.rollRight(array, 2);
		assertArrayEquals(new byte[] { 2, 3, 1 }, array);
	}

	@Test
	public final void testRollRight_3() {
		final byte[] array = new byte[] { 1, 2, 3 };
		Utils.rollRight(array, 3);
		assertArrayEquals(new byte[] { 1, 2, 3 }, array);
	}

	@Test
	public final void testRollRight_empty() {
		final byte[] array = new byte[] {};
		Utils.rollRight(array, 2);
		assertArrayEquals(new byte[] {}, array);
	}

	@Test
	public final void testRollRight_single() {
		final byte[] array = new byte[] { 1 };
		Utils.rollRight(array, 2);
		assertArrayEquals(new byte[] { 1 }, array);
	}

	@Test
	public final void testRolRight_rollMoreOftenThanElements() {
		final byte[] array = new byte[] { 1, 2, 3 };
		Utils.rollRight(array, 4);
		assertArrayEquals(new byte[] { 3, 1, 2 }, array);
	}

	@Test
	public void testClose_empty() {
		Utils.close(Collections.emptyList());
	}

	@Test
	public void testClose() {
		final var result = new ArrayList<Integer>();
		final IntFunction<AutoCloseable> close = (i) -> () -> result.add(i);

		Utils.close(List.of(close.apply(1), close.apply(2)));

		assertEquals("1 in result", true, result.contains(1));
		assertEquals("2 in result", true, result.contains(2));
	}

	@Test
	public void testClose_ExceptionContainsAllProblems() {
		final IntFunction<AutoCloseable> closeErr = (i) -> () -> { throw new Exception("" + i); };

		try {
			Utils.close(List.of(closeErr.apply(1), closeErr.apply(2), closeErr.apply(3)));
		} catch (final IllegalStateException e) {
			assertNotNull(e);
			assertNotNull("error message", e);
			assertEquals("error message contains 1", true, e.getMessage().contains("1"));
			assertNotNull("cause", e.getCause());
			assertNotNull("cause message", e);
			assertEquals("cause message contains 1", true, e.getCause().getMessage().contains("1"));
			assertEquals("cause suppressed not empty", true, e.getCause().getSuppressed().length > 0);
			assertNotNull("cause suppressed message", e.getCause().getSuppressed()[0].getMessage());
			assertEquals("cause suppressed message contains 2", true, e.getCause().getSuppressed()[0].getMessage().contains("2"));
			assertEquals("cause suppressed message contains 3", true, e.getCause().getSuppressed()[1].getMessage().contains("3"));
		}
		// assertEquals("2 in result", true, result.contains(2));
	}

	@Test
	public void testBreadthFirstSearch() {
		class Node {
			int value;

			Node left;

			Node right;

			Node(final int value, final Node left, final Node right) {
				this.value = value;
				this.left = left;
				this.right = right;
			}

			Stream<Node> children() {
				return Stream.of(left, right).filter(n -> n != null);
			}
		}

		final Node rightRightRight = new Node(9, null, null);
		final Node leftLeftRight = new Node(8, null, null);
		final Node leftLeftLeft = new Node(7, null, null);

		final Node rightRight = new Node(6, null, rightRightRight);
		final Node rightLeft = new Node(5, null, null);
		final Node leftRight = new Node(4, null, null);
		final Node leftLeft = new Node(3, leftLeftLeft, leftLeftRight);

		final Node right = new Node(2, rightLeft, rightRight);
		final Node left = new Node(1, leftLeft, leftRight);
		final Node root = new Node(0, left, right);

		final var ints = IntArrayList.empty();
		Utils.breadthFirstSearch(root, Node::children, n -> !ints.add(n.value));
		assertEquals("should traverse left-to-right, root-to-leaf", IntArrayList.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9), ints);

		ints.clear();
		final var found = Utils.breadthFirstSearch(root, Node::children, n -> ints.add(n.value) && n.value == 5);
		assertEquals("should fnd a node", true, found.isPresent());
		assertSame("should find correct node", rightLeft, found.get());
		assertEquals("should not explore more than necessary", IntArrayList.of(0, 1, 2, 3, 4, 5), ints);
	}

	@Test
	public void testSavingFileWithoutAddress() throws IOException {
		final File tempFile = File.createTempFile("getClass().getSimpleName()", ".tmp");
		tempFile.deleteOnExit();

		final byte[] writtenBytes = new byte[] { (byte) 10, (byte) 20, (byte) 33, (byte) 33 };
		Utils.writeFile(tempFile, writtenBytes);
		
		final byte[] reReadBytes = Utils.readFile(tempFile, StartAddress.NotInFile);
		Assert.assertArrayEquals(writtenBytes, reReadBytes);
	}

	@Test
	public void testSavingFileWithAddress() throws IOException {
		final File tempFile = File.createTempFile("getClass().getSimpleName()", ".tmp");
		tempFile.deleteOnExit();
		final byte[] writtenBytes = new byte[] { (byte) 10, (byte) 20, (byte) 33, (byte) 33 };
		Utils.writeFile(tempFile, writtenBytes, 0x2000);

		final byte[] reReadBytes = Utils.readFile(tempFile.toURI(), StartAddress.InFile);
		Assert.assertArrayEquals(writtenBytes, reReadBytes);
	}

	@Test
	public void testSavingFileWithAddress_addressCorrectlySetInFile() throws IOException {
		final File tempFile = File.createTempFile("getClass().getSimpleName()", ".tmp");
		tempFile.deleteOnExit();

		final byte[] writtenBytes = new byte[] { (byte) 10, (byte) 20, (byte) 33, (byte) 55 };
		Utils.writeFile(tempFile, writtenBytes, 0x2010);

		final byte[] reReadBytes = Utils.readFile(tempFile, StartAddress.NotInFile);
		Assert.assertEquals((byte) 0x10, reReadBytes[0]);
		Assert.assertEquals((byte) 0x20, reReadBytes[1]);
	}

}
