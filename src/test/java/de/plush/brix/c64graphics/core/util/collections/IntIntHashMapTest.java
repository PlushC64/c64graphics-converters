package de.plush.brix.c64graphics.core.util.collections;

import static de.plush.brix.c64graphics.core.util.IntIntPair.pair;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

import org.junit.Test;

import de.plush.brix.c64graphics.core.util.IntIntPair;

public class IntIntHashMapTest {

	@Test
	public void testIntIntHashMap() throws Exception {
		final var map = new IntIntHashMap();
		assertTrue(map.isEmpty());
		assertEquals(IntIntHashMap.DEFAULT_NO_VALUE, map.no_value);
	}

	@Test
	public void testIntIntHashMapInt_capacityTooSmall() throws Exception {
		@SuppressWarnings("unused")
		final var map = new IntIntHashMap(1);
		// will just override user request with it's own minimum capacity.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIntIntHashMapInt_testIntIntHashMapIntFloat_capacityTooBig() throws Exception {
		@SuppressWarnings("unused")
		final var map = new IntIntHashMap(IntIntHashMap.MAXIMUM_CAPACITY + 1);
	}

	@Test
	public void testIntIntHashMapInt_maxCapacity() throws Exception {
		@SuppressWarnings("unused")
		final var map = new IntIntHashMap(IntIntHashMap.MAXIMUM_CAPACITY / 2);
	}

	@Test
	public void testIntIntHashMapIntFloat_capacityTooSmall() throws Exception {
		@SuppressWarnings("unused")
		final var map = new IntIntHashMap(1, 0.75f);
		// will just override user request with it's own minimum capacity.
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIntIntHashMapIntFloat_testIntIntHashMapIntFloat_capacityTooBig() throws Exception {
		@SuppressWarnings("unused")
		final var map = new IntIntHashMap(IntIntHashMap.MAXIMUM_CAPACITY + 1, 0.75f);
	}

	@Test
	public void testIntIntHashMapIntFloat_maxCapacity() throws Exception {
		@SuppressWarnings("unused")
		final var map = new IntIntHashMap(IntIntHashMap.MAXIMUM_CAPACITY / 2, 0.75f);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIntIntHashMapIntFloat_fillFactorTooSmall() throws Exception {
		@SuppressWarnings("unused")
		final var map = new IntIntHashMap(1 << 8, 0f);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIntIntHashMapIntFloat_fillFactorTooHigh() throws Exception {
		@SuppressWarnings("unused")
		final var map = new IntIntHashMap(1 << 8, 1f);
	}

	@Test
	public void testIntIntHashMapIntVoid() throws Exception {
		final var map = new IntIntHashMap(Integer.MIN_VALUE + 1, null);
		assertEquals(Integer.MIN_VALUE + 1, map.no_value);
	}

	@Test
	public void testGet_EmptyMap() throws Exception {
		final var map = new IntIntHashMap();
		final var random = new Random(123);
		for (int x = 0; x < 100; ++x) {
			final int key = random.nextInt();
			assertEquals(map.no_value, map.get(key), "unexpected value for key: " + key);
		}
		assertEquals(map.no_value, map.get(Integer.MAX_VALUE), "unexpected value for key: " + Integer.MAX_VALUE);
		assertEquals(map.no_value, map.get(Integer.MIN_VALUE), "unexpected value for key: " + Integer.MIN_VALUE);
		assertEquals(map.no_value, map.get(IntIntHashMap.FREE_KEY), "unexpected value for key: " + IntIntHashMap.FREE_KEY);
	}

	@Test
	public void testGet_free_key() throws Exception {
		final var map = IntIntHashMap.of(IntIntHashMap.FREE_KEY, 110);
		assertEquals(110, map.get(IntIntHashMap.FREE_KEY));
	}

	@Test
	public void testGet() throws Exception {
		final var map = IntIntHashMap.of(//
				10, 110, //
				20, 220//
		);
		assertEquals(110, map.get(10));
		assertEquals(220, map.get(20));
	}

	@Test
	public void testGetOptional_EmptyMap() throws Exception {
		final var map = new IntIntHashMap();
		final var random = new Random(123);
		for (int x = 0; x < 100; ++x) {
			final int key = random.nextInt();
			assertTrue(map.getOptional(key).isEmpty(), "unexpected value for key: " + key);
		}
		assertTrue(map.getOptional(Integer.MAX_VALUE).isEmpty(), "unexpected value for key: " + Integer.MAX_VALUE);
		assertTrue(map.getOptional(Integer.MIN_VALUE).isEmpty(), "unexpected value for key: " + Integer.MIN_VALUE);
		assertTrue(map.getOptional(IntIntHashMap.FREE_KEY).isEmpty(), "unexpected value for key: " + IntIntHashMap.FREE_KEY);
	}

	@Test
	public void testGetOptional_free_key() throws Exception {
		final var map = IntIntHashMap.of(IntIntHashMap.FREE_KEY, 110);
		assertTrue(map.getOptional(IntIntHashMap.FREE_KEY).isPresent());
		assertEquals(110, map.getOptional(IntIntHashMap.FREE_KEY).getAsInt());
	}

	@Test
	public void testGetOptional() throws Exception {
		final var map = IntIntHashMap.of(//
				10, 110, //
				20, 220//
		);
		assertTrue(map.getOptional(10).isPresent());
		assertEquals(110, map.getOptional(10).getAsInt());
		assertTrue(map.getOptional(20).isPresent());
		assertEquals(110, map.getOptional(10).getAsInt());
	}

	@Test
	public void testUpdate_valueDoesNotExist() throws Exception {
		final var map = IntIntHashMap.of(//
				10, 110, //
				20, 220//
		);
		map.update(50, 550, (k, v) -> v + 100);
		assertEquals(550, map.get(50));
	}

	@Test
	public void testUpdate_valueDoesNotExist_usingFreeKey() throws Exception {
		final var map = IntIntHashMap.of(//
				10, 110, //
				20, 220 //
		);
		map.update(IntIntHashMap.FREE_KEY, 550, (k, v) -> v + 100);
		assertEquals(550, map.get(IntIntHashMap.FREE_KEY));
	}

	@Test
	public void testUpdate_valueExists() throws Exception {
		final var map = IntIntHashMap.of(//
				10, 110, //
				20, 220, //
				50, 550//
		);
		map.update(50, 550, (k, v) -> v + 100);
		assertEquals(650, map.get(50));
	}

	@Test
	public void testUpdate_valueExists_usingFreeKey() throws Exception {
		final var map = IntIntHashMap.of(//
				10, 110, //
				20, 220, //
				IntIntHashMap.FREE_KEY, 550);
		map.update(IntIntHashMap.FREE_KEY, 550, (k, v) -> v + 100);
		assertEquals(650, map.get(IntIntHashMap.FREE_KEY));
	}

	@Test
	public void testPutUpdateGetOptional_hashCollisionsAndRehash() throws Exception {
		final var map = new IntIntHashMap(0);
		for (int t = 0; t < 64; ++t) {
			map.put(t, t + 100);
		}
		for (int t = 0; t < 64; ++t) {
			map.update(t, t + 100, (k, v) -> v + 100);
		}
		for (int t = 0; t < 64; ++t) {
			assertTrue(map.getOptional(t).isPresent());
			assertEquals(t + 200, map.getOptional(t).getAsInt());
		}
		for (int t = 64; t < 128; ++t) {
			map.update(t, t + 100, (k, v) -> v + 100);
		}
		for (int t = 64; t < 128; ++t) {
			assertTrue(map.getOptional(t).isPresent());
			assertEquals(t + 100, map.getOptional(t).getAsInt());
		}
	}

	@Test
	public void testPutGetOptional_hashCollisionsAndRehash() throws Exception {
		final var map = new IntIntHashMap(16);
		for (int t = 0; t < 64; ++t) {
			map.put(t, t + 100);
		}
		for (int t = 0; t < 64; ++t) {
			assertTrue(map.getOptional(t).isPresent());
			assertEquals(t + 100, map.getOptional(t).getAsInt());
		}
		for (int t = 64; t < 128; ++t) {
			assertTrue(map.getOptional(t).isEmpty());
		}
	}

	@Test
	public void testPutGet_hashCollisionsAndRehash() throws Exception {
		final var map = new IntIntHashMap(16);
		for (int t = 0; t < 64; ++t) {
			map.put(t, t + 100);
		}
		for (int t = 0; t < 64; ++t) {
			assertEquals(t + 100, map.get(t));
		}
	}

	@Test
	public void testPutRemovePutGet_hashCollisionsAndRehash() throws Exception {
		final var map = new IntIntHashMap(16);
		for (int t = 0; t < 64; ++t) {
			map.put(t, t + 100);
		}

		for (int t = 0; t < 64; ++t) {
			assertEquals(t + 100, map.get(t));
		}

		// remove some
		for (int t = 16; t < 32; ++t) {
			assertEquals(t + 100, map.remove(t));
		}

		for (int t = 0; t < 16; ++t) {
			assertEquals(t + 100, map.get(t));
		}
		for (int t = 16; t < 32; ++t) {
			assertEquals(map.no_value, map.get(t));
		}
		for (int t = 32; t < 64; ++t) {
			assertEquals(t + 100, map.get(t));
		}

		// reinsert all
		for (int t = 0; t < 64; ++t) {
			map.put(t, t + 100);
		}

		for (int t = 0; t < 64; ++t) {
			assertEquals(t + 100, map.get(t));
		}

		// remove all
		for (int t = 0; t < 64; ++t) {
			map.remove(t);
		}
		assertTrue(map.isEmpty());

	}

	@Test
	public void testRemovFree_key() throws Exception {
		final var map = IntIntHashMap.of(IntIntHashMap.FREE_KEY, 110);
		assertEquals(110, map.remove(IntIntHashMap.FREE_KEY));
		assertEquals(map.no_value, map.get(IntIntHashMap.FREE_KEY));
		assertEquals(map.no_value, map.remove(IntIntHashMap.FREE_KEY));
	}

	@Test
	public void testRemovContainsKey_emptyMapShouldNotContainfreeKey() throws Exception {
		final var map = new IntIntHashMap();
		assertFalse(map.containsKey(IntIntHashMap.FREE_KEY));
	}

	@Test
	public void testRemovContainsKey_mapWitFreeKeyMappingShouldContainFreeKey() throws Exception {
		final var map = IntIntHashMap.of(IntIntHashMap.FREE_KEY, 110);
		assertTrue(map.containsKey(IntIntHashMap.FREE_KEY));
	}

	@Test
	public void testRemovContainsKey_mapWithMappingToNoValueShouldContainKey() throws Exception {
		final var map = new IntIntHashMap();
		map.put(10, map.no_value);
		assertTrue(map.containsKey(10));
	}

	@Test
	public void testSize() throws Exception {
		final var map = IntIntHashMap.of(//
				10, 110, //
				20, 220, //
				30, 330//
		);
		assertEquals(3, map.size());
	}

	@Test
	public void testSize_decreasesOnRemove() throws Exception {
		final var map = IntIntHashMap.of(//
				10, 110, //
				20, 220, //
				30, 330, //
				40, 440//
		);
		assertEquals(4, map.size());
		map.remove(20);
		assertEquals(3, map.size());
	}

	@Test
	public void testSize_doesNotDecreasesOnRemovalOfNonExistingElement() throws Exception {
		final var map = IntIntHashMap.of(//
				10, 110, //
				20, 220, //
				30, 330, //
				40, 440, //
				50, 550//
		);
		assertEquals(5, map.size());
		map.remove(110);
		assertEquals(5, map.size());
	}

	@Test
	public void testSize_increasesOnAddingfAnElement() throws Exception {
		final var map = IntIntHashMap.of(//
				10, 110, //
				20, 220, //
				30, 330, //
				40, 440, //
				50, 550, //
				60, 660//
		);
		assertEquals(6, map.size());
		map.put(70, 770);
		assertEquals(7, map.size());
	}

	@Test
	public void testSize_doesNotIncreasesOnReplacingAnElement() throws Exception {
		final var map = IntIntHashMap.of(//
				10, 110, //
				20, 220, //
				30, 330, //
				40, 440, //
				50, 550, //
				60, 660, //
				70, 770//
		);
		assertEquals(7, map.size());
		map.put(40, 4400);
		assertEquals(7, map.size());
	}

	@Test
	public void testIsEmpty_notEmpty() throws Exception {
		final var map = IntIntHashMap.of(//
				10, 110, //
				20, 220, //
				30, 330, //
				40, 440, //
				50, 550, //
				60, 660, //
				70, 770, //
				80, 880//
		);
		assertFalse(map.isEmpty());
	}

	@Test
	public void testIsEmpty_afterRemoval() throws Exception {
		final var map = IntIntHashMap.of(//
				10, 110 //
		);
		map.remove(10);
		assertTrue(map.isEmpty());
	}

	@Test
	public void testKeyStream() throws Exception {
		final var map = IntIntHashMap.of(//
				10, 110, //
				20, 220, //
				30, 330, //
				40, 440, //
				50, 550, //
				60, 660, //
				70, 770, //
				80, 880, //
				90, 990//
		);
		final var keys = map.keyStream().sorted().toArray();
		final var expectedKeys = new int[] { 10, 20, 30, 40, 50, 60, 70, 80, 90 };
		assertArrayEquals(expectedKeys, keys);
	}

	@Test
	public void testKeyStream_includesFreeKey() throws Exception {
		final var map = IntIntHashMap.of(//
				10, 110, //
				20, 220, //
				30, 330, //
				40, 440, //
				50, 550, //
				60, 660, //
				70, 770, //
				80, 880, //
				90, 990, //
				100, 1000//
		);
		map.put(IntIntHashMap.FREE_KEY, 2200);
		final var keys = map.keyStream().sorted().toArray();
		final var expectedKeys = new int[] { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, IntIntHashMap.FREE_KEY };
		Arrays.sort(expectedKeys); // because of FREE_KEY that could be anything
		assertArrayEquals(expectedKeys, keys);
	}

	@Test
	public void testKeyStream_empty() throws Exception {
		final var map = new IntIntHashMap();
		assertEquals(0, map.keyStream().count());
	}

	@Test
	public void testValueStream() throws Exception {
		final var map = IntIntHashMap.of(//
				pair(10, 110), //
				pair(20, 220), //
				pair(30, 330), //
				pair(40, 440), //
				pair(50, 550), //
				pair(60, 660), //
				pair(70, 770), //
				pair(80, 880), //
				pair(90, 990), //
				pair(100, 1000), //
				pair(110, 1100) //
		);
		final var values = map.valueStream().sorted().toArray();
		final var expectedValues = new int[] { 110, 220, 330, 440, 550, 660, 770, 880, 990, 1000, 1100 };
		assertArrayEquals(expectedValues, values);
	}

	@Test
	public void testValueStream_usingFreeKey() throws Exception {
		final var map = IntIntHashMap.of(//
				pair(10, 110), //
				pair(20, 220), //
				pair(30, 330), //
				pair(40, 440), //
				pair(50, 550), //
				pair(60, 660), //
				pair(70, 770), //
				pair(80, 880), //
				pair(90, 990), //
				pair(100, 1000), //
				pair(IntIntHashMap.FREE_KEY, 1100) //
		);
		final var values = map.valueStream().sorted().toArray();
		final var expectedValues = new int[] { 110, 220, 330, 440, 550, 660, 770, 880, 990, 1000, 1100 };
		assertArrayEquals(expectedValues, values);
	}

	@Test
	public void testValueStream_usingNoValue() throws Exception {
		final var map = IntIntHashMap.of(//
				pair(10, 110), //
				pair(20, 220), //
				pair(30, 330), //
				pair(40, 440), //
				pair(50, 550), //
				pair(60, 660), //
				pair(70, 770), //
				pair(80, 880), //
				pair(90, 990), //
				pair(100, 1000) //
		);
		map.put(100, map.no_value); // is essentially the same as deleting a value

		final var values = map.valueStream().sorted().toArray();
		final var expectedValues = new int[] { 110, 220, 330, 440, 550, 660, 770, 880, 990, map.no_value };
		Arrays.sort(expectedValues);
		assertArrayEquals(expectedValues, values);
	}

	@Test
	public void testValueStream_empty() throws Exception {
		final var map = new IntIntHashMap();
		assertEquals(0, map.valueStream().count());
	}

	@Test
	public void testEntryStream_empty() throws Exception {
		final var map = new IntIntHashMap();
		assertEquals(0, map.entryStream().count());
	}

	@Test
	public void testEntryStream_populated() throws Exception {
		final var map = IntIntHashMap.of(//
				pair(10, 110), //
				pair(20, 220), //
				pair(30, 330) //
		);
		assertEquals(3, map.entryStream().count());
		assertTrue(map.entryStream().anyMatch(pair(10, 110)::equals));
		assertTrue(map.entryStream().anyMatch(pair(20, 220)::equals));
		assertTrue(map.entryStream().anyMatch(pair(30, 330)::equals));
	}

	@Test
	public void testEntryStream_populated_usingFreeKey() throws Exception {
		final var map = IntIntHashMap.of(//
				pair(10, 110), //
				pair(20, 220), //
				pair(30, 330), //
				pair(IntIntHashMap.FREE_KEY, 440));
		assertEquals(4, map.entryStream().count());
		assertTrue(map.entryStream().anyMatch(pair(10, 110)::equals));
		assertTrue(map.entryStream().anyMatch(pair(20, 220)::equals));
		assertTrue(map.entryStream().anyMatch(pair(30, 330)::equals));
		assertTrue(map.entryStream().anyMatch(pair(IntIntHashMap.FREE_KEY, 440)::equals));
	}

	@Test
	public void testToArray() throws Exception {
		final var map = IntIntHashMap.of(//
				pair(10, 110), //
				pair(20, 220), //
				pair(30, 330) //
		);
		final var arr = map.toArray();
		Arrays.sort(arr);
		assertEquals(3, arr.length);
		assertArrayEquals(new IntIntPair[] { pair(10, 110), pair(20, 220), pair(30, 330), }, arr);
	}

	@Test
	public void testToSortedArray() throws Exception {
		final var map = IntIntHashMap.of(//
				pair(20, 220), //
				pair(10, 110), //
				pair(30, 330) //
		);
		final var arr = map.toSortedArray();
		assertEquals(3, arr.length);
		assertArrayEquals(new IntIntPair[] { pair(10, 110), pair(20, 220), pair(30, 330), }, arr);
	}

	@Test
	public void testEquals_bothEmpty() throws Exception {
		assertTrue(new IntIntHashMap().equals(new IntIntHashMap()));
	}

	@Test
	public void testEquals_symmetry() throws Exception {
		final var map1 = new IntIntHashMap();
		final var map2 = new IntIntHashMap() {
			@SuppressWarnings("unused")
			boolean isPopulated() {
				return !isEmpty();
			}
		};
		assertFalse(map1.equals(map2));
		assertFalse(map2.equals(map1), "equals expected to be symmetric!");
	}

	@Test
	public void testEquals_emptyVsFilled() throws Exception {
		assertFalse(new IntIntHashMap(16, null).equals(IntIntHashMap.of(10, 110)));
	}

	@Test
	public void testEquals_sameObject() throws Exception {
		final var map = new IntIntHashMap();
		assertTrue(map.equals(map));
	}

	@Test
	public void testEquals_null() throws Exception {
		assertFalse(new IntIntHashMap().equals(null));
	}

	@Test
	public void testEquals_sameContentButDifferentCapacity() throws Exception {
		final var map1 = new IntIntHashMap(16);
		map1.put(10, 110);
		final var map2 = new IntIntHashMap(32);
		map2.put(10, 110);
		assertTrue(map1.equals(map2));
		assertTrue(map2.equals(map1), "equals expected to be symmetric!");
	}

	@Test
	public void testEquals_sameContentButDifferentLoadFactor() throws Exception {
		final var map1 = new IntIntHashMap(16, 0.5f);
		map1.put(10, 110);
		final var map2 = new IntIntHashMap(16, 0.8f);
		map2.put(10, 110);
		assertTrue(map1.equals(map2));
		assertTrue(map2.equals(map1), "equals expected to be symmetric!");
	}

	@Test
	public void testEquals_sameContentButDifferentNoValue() throws Exception {
		final var map1 = new IntIntHashMap(Integer.MIN_VALUE + 1, null);
		map1.put(10, 110);
		final var map2 = new IntIntHashMap(0, null);
		map2.put(10, 110);
		assertTrue(map1.equals(map2));
		assertTrue(map2.equals(map1), "equals expected to be symmetric!");
	}

	@Test
	public void testEquals_sameContentUsingNoValue() throws Exception {
		final var map1 = new IntIntHashMap(10, null); // no_value = 10
		map1.put(100, 10); // value == no_value
		final var map2 = new IntIntHashMap(0, null); // no_value = 0
		map2.put(100, 10); // value != no_value
		assertTrue(map1.equals(map2));
		assertTrue(map2.equals(map1), "equals expected to be symmetric!");
	}

	@Test
	public void testEquals_sameContentUsingFREE_KEY() throws Exception {
		final var map1 = new IntIntHashMap();
		map1.put(IntIntHashMap.FREE_KEY, 110);
		final var map2 = new IntIntHashMap();
		map2.put(IntIntHashMap.FREE_KEY, 110);
		assertTrue(map1.equals(map2));
		assertTrue(map2.equals(map1), "equals expected to be symmetric!");
	}

	@Test
	public void testEquals_sameSizeDifferentKeys() throws Exception {
		assertFalse(IntIntHashMap.of(20, 110).equals(IntIntHashMap.of(10, 110)));
	}

	@Test
	public void testEquals_sameSizeDifferentValues() throws Exception {
		assertFalse(IntIntHashMap.of(10, 110).equals(IntIntHashMap.of(10, 220)));
	}

	@Test
	public void testHashCode_empty() throws Exception {
		assertEquals(new IntIntHashMap().hashCode(), new IntIntHashMap().hashCode());
	}

	@Test
	public void testHashCode_filled_sameEntries() throws Exception {
		assertEquals(IntIntHashMap.of(10, 110).hashCode(), IntIntHashMap.of(10, 110).hashCode());
	}

	@Test
	public void testHashCode_filled_sameEntriesDiifferentOrder() throws Exception {
		assertEquals(IntIntHashMap.of(10, 110, 20, 220).hashCode(), IntIntHashMap.of(20, 220, 10, 110).hashCode());
	}

	@Test
	public void testHashCode_filled_differentValues() throws Exception {
		assertNotEquals(IntIntHashMap.of(10, 110).hashCode(), IntIntHashMap.of(10, 220).hashCode());
	}

	@Test
	public void testHashCode_filled_differentKeys() throws Exception {
		assertNotEquals(IntIntHashMap.of(10, 110).hashCode(), IntIntHashMap.of(20, 110).hashCode());
	}

	@Test
	public void testHashCode_sameResultOnSubsequentCallsWithNoMofification() throws Exception {
		final var map = IntIntHashMap.of(10, 110);
		final int hashCode = map.hashCode();
		assertEquals(hashCode, map.hashCode());
	}

	@Test
	public void testHashCode_changesOnPut() throws Exception {
		final var map = IntIntHashMap.of(10, 110);
		final int hashCode = map.hashCode();
		map.put(20, 220);
		assertNotEquals(hashCode, map.hashCode());
	}

	@Test
	public void testHashCode_changesOnRemove() throws Exception {
		final var map = IntIntHashMap.of(10, 110, 20, 220);
		final int hashCode = map.hashCode();
		map.remove(20);
		assertNotEquals(hashCode, map.hashCode());
	}

	@Test
	public void testHashCode_changesOnClear() throws Exception {
		final var map = IntIntHashMap.of(10, 110, 20, 220);
		final int hashCode = map.hashCode();
		map.clear();
		assertNotEquals(hashCode, map.hashCode());
	}

	@Test
	public void testHashCode_sameResultAfterRemoveAndAdd() throws Exception {
		final var map = IntIntHashMap.of(10, 110);
		final int hashCode = map.hashCode();
		map.remove(10);
		map.put(10, 110);
		assertEquals(hashCode, map.hashCode());
	}

	@Test
	public void testHashCode_sameContentDifferentCapacity() throws Exception {
		final var map1 = new IntIntHashMap(16);
		map1.put(10, 110);
		final var map2 = new IntIntHashMap(32);
		map2.put(10, 110);
		assertEquals(map1.hashCode(), map2.hashCode());
	}

	@Test
	public void testHashCode_sameContentDifferentLoadFactors() throws Exception {
		final var map1 = new IntIntHashMap(16, 0.75f);
		map1.put(10, 110);
		final var map2 = new IntIntHashMap(16, 0.5f);
		map2.put(10, 110);
		assertEquals(map1.hashCode(), map2.hashCode());
	}

	@Test
	public void testHashCode_sameContentDifferentNoValue() throws Exception {
		final var map1 = new IntIntHashMap(999, null);
		map1.put(10, 110);
		final var map2 = new IntIntHashMap(0, null);
		map2.put(10, 110);
		assertEquals(map1.hashCode(), map2.hashCode());
	}

	@Test
	public void testHashCode_sameContentUsingNoValue() throws Exception {
		final var map1 = new IntIntHashMap(10, null); // no_value = 10
		map1.put(100, 10); // value == no_value
		final var map2 = new IntIntHashMap(0, null); // no_value = 0
		map2.put(100, 10); // value != no_value
		assertEquals(map1.hashCode(), map2.hashCode());
	}

	@Test
	public void testHashCode_sameContentUsingFREE_KEY() throws Exception {
		final var map1 = new IntIntHashMap();
		map1.put(IntIntHashMap.FREE_KEY, 110);
		final var map2 = new IntIntHashMap();
		map2.put(IntIntHashMap.FREE_KEY, 110);
		assertEquals(map1.hashCode(), map2.hashCode());
	}

	@Test
	public void testClear_removesMappings() throws Exception {
		final var map = IntIntHashMap.of(10, 110, 20, 220);
		map.clear();
		assertFalse(map.containsKey(10));
		assertFalse(map.containsKey(20));
		assertTrue(map.get(10) == map.no_value);
		assertTrue(map.get(20) == map.no_value);
	}

	@Test
	public void testClear_removesFREE_KEYmapping() throws Exception {
		final var map = new IntIntHashMap();
		map.put(IntIntHashMap.FREE_KEY, 110);
		map.put(20, 220);
		map.clear();
		assertFalse(map.containsKey(IntIntHashMap.FREE_KEY));
		assertFalse(map.containsKey(20));
		assertTrue(map.get(IntIntHashMap.FREE_KEY) == map.no_value);
		assertTrue(map.get(20) == map.no_value);
	}

	@Test
	public void testClear_size_becomesZero() throws Exception {
		final var map = IntIntHashMap.of(10, 110, 20, 220);
		map.clear();
		assertEquals(0, map.size());
	}

	@Test
	public void testClear_isEmpty() throws Exception {
		final var map = IntIntHashMap.of(10, 110, 20, 220);
		map.clear();
		assertTrue(map.isEmpty());
	}

	@Test
	public void testClear_makesToArrayReturnAnEmptyArray() throws Exception {
		final var map = IntIntHashMap.of(10, 110, 20, 220);
		map.clear();
		final var arr = map.toArray();
		assertArrayEquals(new IntIntPair[] {}, arr);
	}

	@Test
	public void testClear_makesToSortedArrayReturnAnEmptyArray() throws Exception {
		final var map = IntIntHashMap.of(10, 110, 20, 220);
		map.clear();
		final var arr = map.toSortedArray();
		assertArrayEquals(new IntIntPair[] {}, arr);
	}

	@Test
	public void testClear_doesNotResetDefaultNoValue() throws Exception {
		final var map = new IntIntHashMap(1000, null);
		map.clear();
		assertEquals(1000, map.no_value);
	}

	@Test
	public void testClear_andRefillWorks() throws Exception {
		final var map = IntIntHashMap.of(10, 110, 20, 220);
		map.clear();
		map.put(30, 330);
		map.put(40, 440);
		assertEquals(2, map.size());
		final var arr = map.toSortedArray();
		assertArrayEquals(new IntIntPair[] { pair(30, 330), pair(40, 440) }, arr);
	}

}
