package de.plush.brix.c64graphics.core.model;

import java.io.*;
import java.net.*;

import org.junit.*;

import de.plush.brix.c64graphics.core.util.*;

public class C64BitmapmodePictureMulticolorTest {

	private URL koalaURL;

	@Before
	public void setup() {
		koalaURL = getClass().getResource("resources/cop-gun.kla");
	}

	@Test
	public void testExampleKoalaHasCorrectStartAddress() throws URISyntaxException {
		final byte[] fileBytes = Utils.readFile(koalaURL, StartAddress.NotInFile);
		Assert.assertEquals((byte) 0x00, fileBytes[0]); // low byte of $6000
		Assert.assertEquals((byte) 0x60, fileBytes[1]); // high byte of $6000
	}

	@Test
	public void testLoadSaveLoadKoala() throws URISyntaxException, IOException {
		final var original = C64BitmapmodePictureMulticolor.loadKoala(koalaURL.toURI(), StartAddress.InFile);
		
		// final var originalPc = new C64BitmapmodePictureMulticolorToImage(C64ColorsColodore.PALETTE).apply(original);
		// new ImageDisplay<>().show(originalPc, "original");
		
		final File tempFile = File.createTempFile("getClass().getSimpleName()", ".tmp");
		tempFile.deleteOnExit();

		original.saveKoala(tempFile.toURI(), StartAddress.InFile);
		final var reloaded = C64BitmapmodePictureMulticolor.loadKoala(tempFile.toURI(), StartAddress.InFile);

		// final var reloadedPc = new C64BitmapmodePictureMulticolorToImage(C64ColorsColodore.PALETTE).apply(original);
		// new ImageDisplay<>().show(reloadedPc, "reloaded");

		Assert.assertTrue(original.equals(reloaded));
	}

}
