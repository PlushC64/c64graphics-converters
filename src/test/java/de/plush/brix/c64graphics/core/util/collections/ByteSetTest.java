package de.plush.brix.c64graphics.core.util.collections;

import static de.plush.brix.c64graphics.core.util.Utils.toPositiveInt;
import static java.util.Collections.synchronizedSet;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.*;

import de.plush.brix.c64graphics.core.util.Utils;

public class ByteSetTest {

	@Test
	public void testByteSet_newSetIsEmpty() throws Exception {
		final var set = new ByteSet();
		assertTrue(set.isEmpty());
		assertFalse(set.contains((byte) 0));
	}

	@Test
	public void testByteSet_newSetDoesNotContainZero() throws Exception {
		final var set = new ByteSet();
		assertFalse(set.contains((byte) 0));
	}

	@Test
	public void testByteSetByteSet_copyOfEmptyEqualsOriginal() throws Exception {
		final var original = new ByteSet();
		final var copy = new ByteSet(original);
		assertEquals(original, copy);
	}

	@Test
	public void testByteSetByteSet_copyOfNonEmptyEqualsOriginal() throws Exception {
		final var original = new ByteSet();
		original.add((byte) 123);
		original.add((byte) 230);
		final var copy = new ByteSet(original);
		assertEquals(original, copy);
	}

	@Test
	public void testByteSetByteSet_copyDispatchedFromOriginal() throws Exception {
		final var original = new ByteSet();
		final var copy = new ByteSet(original);
		copy.add((byte) 10);
		assertNotEquals(original, copy);
		assertFalse(original.contains((byte) 10));
	}

	@Test
	public void testOf() throws Exception {
		final var bytes = new byte[] { (byte) -10, (byte) 123 };
		final var set = ByteSet.of(bytes);
		assertTrue(set.contains((byte) -10));
		assertTrue(set.contains((byte) 123));
	}

	@Test
	public void testOfUnsigned() throws Exception {
		final var set = ByteSet.ofUnsigned(0xFF, 0xFE);
		assertEquals(2, set.size());
		assertEquals(ByteSet.of((byte) -1, (byte) -2), set);
	}

	@Test
	public void testEmpty() throws Exception {
		final var set = ByteSet.empty();
		assertTrue(set.isEmpty());
	}

	@Ignore
	@Test
	public void testSize() throws Exception {
		throw new RuntimeException("not yet implemented");
	}

	@Test
	public void testIsEmpty() throws Exception {
		final var set = ByteSet.of((byte) 80, (byte) 10);
		assertFalse(set.isEmpty());
		set.remove((byte) 80);
		assertFalse(set.isEmpty());
		set.remove((byte) 10);
		assertTrue(set.isEmpty());
	}

	@Test
	public void testAdd() throws Exception {
		final var set = new ByteSet();
		set.add((byte) -200);
		assertTrue(set.contains((byte) -200));
		assertEquals(1, set.size());
		set.add((byte) 12);
		assertTrue(set.contains((byte) 12));
		assertEquals(2, set.size());
	}

	@Test
	public void testAdd_returnNewValue() throws Exception {
		final var set = new ByteSet();
		assertTrue(set.add((byte) -200));
	}

	@Test
	public void testAdd_UpperBound() throws Exception {
		final var set = new ByteSet();
		set.add((byte) 0xff);
		assertTrue(set.contains((byte) 0xff));
	}

	@Test
	public void testAdd_LowerBound() throws Exception {
		final var set = new ByteSet();
		set.add((byte) 0);
		assertTrue(set.contains((byte) 0));
	}

	@Test
	public void testAdd_SameValueTwice() throws Exception {
		final var set = new ByteSet();
		set.add((byte) -200);
		set.add((byte) -200);
		assertEquals(1, set.size());
	}

	@Test
	public void testAddUnsigned() throws Exception {
		final var set = new ByteSet();
		set.addUnsigned(0xFF);
		set.addUnsigned(0xFE);
		assertEquals(2, set.size());
		assertEquals(ByteSet.of((byte) -1, (byte) -2), set);
	}

	@Test
	public void testAdd_SameValuetwiceShojldReturnFalseOnSecondAdd() throws Exception {
		final var set = new ByteSet();
		set.add((byte) -200);
		assertFalse(set.add((byte) -200));
	}

	@Test
	public void testAddAllByteSet() throws Exception {
		final var set = ByteSet.of((byte) 80, (byte) 10);
		final var set2 = ByteSet.of((byte) 80, (byte) 30, (byte) 60);
		assertTrue(set.addAll(set2));
		assertEquals(4, set.size());
		assertTrue(set.contains((byte) 80));
		assertTrue(set.contains((byte) 10));
		assertTrue(set.contains((byte) 30));
		assertTrue(set.contains((byte) 60));
	}

	@Test
	public void testAddAllByteSetSameValues() throws Exception {
		final var set = ByteSet.of((byte) 80, (byte) 10);
		final var set2 = ByteSet.of((byte) 80, (byte) 10);
		assertFalse(set.addAll(set2));
	}

	@Test
	public void testAddAllByteSetSameObject() throws Exception {
		final var set = ByteSet.of((byte) 80, (byte) 10);
		assertFalse(set.addAll(set));
	}

	@Test
	public void testAddAllByteSetEmpty() throws Exception {
		final var set = ByteSet.of((byte) 80, (byte) 10);
		assertFalse(set.addAll(ByteSet.empty()));
	}

	@Test
	public void testAddAllByteArraySameValues() throws Exception {
		final var set = ByteSet.of((byte) 80, (byte) 30, (byte) 60);
		final var arr = new byte[] { (byte) 80, (byte) 30, (byte) 60 };
		assertFalse(set.addAll(arr));
		assertEquals(3, set.size());
	}

	@Test
	public void testAddAllUnsigned() throws Exception {
		final var set = new ByteSet();
		set.addAllUnsigned(0xFF, 0xFE);
		assertEquals(2, set.size());
		assertEquals(ByteSet.of((byte) -1, (byte) -2), set);
	}

	@Test
	public void testAddAllUnsigned_ExistingValue() throws Exception {
		final var set = ByteSet.ofUnsigned(0xFF, 0xFE);
		assertEquals("return value", false, set.addAllUnsigned(0xFF));
		assertEquals("size", 2, set.size());
	}

	@Test
	public void testAddAllUnsigned_NonExistingValue() throws Exception {
		final var set = ByteSet.ofUnsigned(0xFF, 0xFE);
		assertEquals("return value", true, set.addAllUnsigned(0xFC));
		assertEquals("size", 3, set.size());
	}

	@Test
	public void testRemoveFromEmpty() throws Exception {
		final var set = ByteSet.empty();
		assertFalse(set.remove((byte) 111));
		assertTrue(set.isEmpty());
	}

	@Test
	public void testRemoveNonExistingElement() throws Exception {
		final var set = ByteSet.of((byte) 1, (byte) 2, (byte) 3);
		assertFalse(set.remove((byte) 10));
		assertEquals(3, set.size());
	}

	@Test
	public void testRemoveExistingElement() throws Exception {
		final var set = ByteSet.of((byte) 1, (byte) 2, (byte) 3);
		assertTrue(set.remove((byte) 2));
		assertEquals(2, set.size());
	}

	@Test
	public void testRemoveAllByteSet_Empty() throws Exception {
		final var set = ByteSet.of((byte) 1, (byte) 2, (byte) 3);
		assertFalse(set.removeAll(ByteSet.empty()));
		assertEquals(3, set.size());
	}

	@Test
	public void testRemoveAllByteSet_samnObject() throws Exception {
		final var set = ByteSet.of((byte) 1, (byte) 2, (byte) 3);
		assertTrue(set.removeAll(set));
		assertTrue(set.isEmpty());
	}

	@Test
	public void testRemoveAllByteSet_existingEntries() throws Exception {
		final var set = ByteSet.of((byte) 1, (byte) 2, (byte) 3);
		final var set2 = ByteSet.of((byte) 1, (byte) 3);
		assertTrue(set.removeAll(set2));
		assertEquals(1, set.size());
		assertFalse(set.contains((byte) 1));
		assertTrue(set.contains((byte) 2));
		assertFalse(set.contains((byte) 3));
	}

	@Test
	public void testRemoveAllByteSet_nonExistingEntries() throws Exception {
		final var set = ByteSet.of((byte) 1, (byte) 2, (byte) 3);
		final var set2 = ByteSet.of((byte) 10, (byte) 20, (byte) 30);
		assertFalse(set.removeAll(set2));
		assertEquals(3, set.size());
	}

	@Test
	public void testRemoveAllByteArray_Empty() throws Exception {
		final var set = ByteSet.of((byte) 80, (byte) 30, (byte) 60);
		assertFalse(set.removeAll(new byte[] {}));
		assertEquals(3, set.size());
	}

	@Test
	public void testRemoveAllByteArray_allExistingEntries() throws Exception {
		final var set = ByteSet.of((byte) 80, (byte) 30, (byte) 60);
		final var arr = new byte[] { (byte) 80, (byte) 30, (byte) 60 };
		assertTrue(set.removeAll(arr));
		assertTrue(set.isEmpty());
	}

	@Test
	public void testRemoveAllByteArray_NoExistingEntries() throws Exception {
		final var set = ByteSet.of((byte) 80, (byte) 30, (byte) 60);
		final var arr = new byte[] { (byte) 8, (byte) 3, (byte) 6 };
		assertFalse(set.removeAll(arr));
		assertFalse(set.isEmpty());
	}

	@Test
	public void testRemoveAllByteArray() throws Exception {
		final var set = ByteSet.of((byte) 80, (byte) 30, (byte) 60);
		final var arr = new byte[] { (byte) 80, (byte) 10, (byte) 60 };
		assertTrue(set.removeAll(arr));
		assertEquals(1, set.size());
	}

	@Test
	public void testContains() throws Exception {
		assertTrue(ByteSet.of((byte) 80, (byte) 30, (byte) 60).contains((byte) 80));
		assertFalse(ByteSet.of((byte) 80, (byte) 30, (byte) 60).contains((byte) 0));
	}

	@Test
	public void testWith_notSame() throws Exception {
		final var set = ByteSet.empty();
		final var set2 = set.with((byte) 80);
		assertNotSame(set, set2);
		assertFalse(set.contains((byte) 80));
	}

	@Test
	public void testWith_empty() throws Exception {
		final var set = ByteSet.empty();
		final var set2 = set.with((byte) 80);
		assertTrue(set2.contains((byte) 80));
	}

	@Test
	public void testWith_nonEmpty() throws Exception {
		final var set = ByteSet.of((byte) 10, (byte) 20);
		final var set2 = set.with((byte) 80);
		assertTrue(set2.contains((byte) 10));
		assertTrue(set2.contains((byte) 20));
		assertTrue(set2.contains((byte) 80));
	}

	@Test
	public void testWithAll_notSame() throws Exception {
		final var set = ByteSet.empty();
		final var set2 = ByteSet.of((byte) 0xff);
		final var set3 = set.withAll(set2);
		assertNotSame(set, set3);
		assertTrue(set.isEmpty());
		assertFalse(set2.isEmpty());
		assertFalse(set3.isEmpty());
	}

	@Test
	public void testWithAll_BothEmpty() throws Exception {
		final var set = ByteSet.empty();
		final var set2 = set.withAll(ByteSet.empty());
		assertTrue(set2.isEmpty());
	}

	@Test
	public void testWithAll_EmptyPlusFilled() throws Exception {
		final var set = ByteSet.empty();
		final var set2 = set.withAll(ByteSet.of((byte) 180, (byte) 110));
		assertTrue(set.isEmpty());
		assertTrue(set2.contains((byte) 180));
		assertTrue(set2.contains((byte) 110));
	}

	@Test
	public void testWithAll_FilledPlusEmpty() throws Exception {
		final var set = ByteSet.of((byte) 180, (byte) 110);
		final var set2 = set.withAll(ByteSet.empty());
		assertFalse(set2.isEmpty());
		assertTrue(set2.contains((byte) 180));
		assertTrue(set2.contains((byte) 110));
	}

	@Test
	public void testWithAllDoesNotPolluteParameter() throws Exception {
		final var set = ByteSet.of((byte) 180, (byte) 110);
		final var set2 = ByteSet.of((byte) 200, (byte) 210);
		final var set3 = set.withAll(set2);
		assertFalse(set2.contains((byte) 180));
		assertFalse(set2.contains((byte) 110));
		assertTrue(set3.contains((byte) 200));
		assertTrue(set3.contains((byte) 210));
	}

	@Test
	public void testWithAll() throws Exception {
		final var set = ByteSet.of((byte) 180, (byte) 110);
		final var set2 = ByteSet.of((byte) 200, (byte) 210);
		final var set3 = set.withAll(set2);
		assertTrue(set3.contains((byte) 180));
		assertTrue(set3.contains((byte) 110));
		assertTrue(set3.contains((byte) 200));
		assertTrue(set3.contains((byte) 210));

		assertFalse(set2.contains((byte) 180));
		assertFalse(set2.contains((byte) 110));
	}

	@Test
	public void testWithout() throws Exception {
		final var set = ByteSet.of((byte) 180, (byte) 110);
		final var set2 = set.without((byte) 180);
		assertFalse(set2.contains((byte) 180));
	}

	@Test
	public void testWithout_DOesNotPoluteOriginal() throws Exception {
		final var set = ByteSet.of((byte) 180, (byte) 110);
		set.without((byte) 180);
		assertTrue(set.contains((byte) 180));
	}

	@Test
	public void testWithoutAll_NoExistingEntries() throws Exception {
		final var set = ByteSet.of((byte) 180, (byte) 110);
		final var set2 = ByteSet.of((byte) 200, (byte) 210);
		final var set3 = set.withoutAll(set2);
		assertTrue(set3.equals(set));
	}

	@Test
	public void testWithoutAll_existingEntries() throws Exception {
		final var set = ByteSet.of((byte) 180, (byte) 110);
		final var set2 = ByteSet.of((byte) 180);
		final var set3 = set.withoutAll(set2);
		assertFalse(set3.isEmpty());
		assertEquals(1, set3.size());
		assertTrue(set3.contains((byte) 110));
	}

	@Test
	public void testWithoutAll_doesNotPolluteParameter() throws Exception {
		final var set = ByteSet.of((byte) 180, (byte) 110);
		final var set2 = ByteSet.of((byte) 180);
		set.withoutAll(set2);
		assertFalse(set2.isEmpty());
		assertEquals(1, set2.size());
		assertTrue(set2.contains((byte) 180));
		assertFalse(set2.contains((byte) 110));
	}

	@Test
	public void testClear() throws Exception {
		final var set = ByteSet.of((byte) 180, (byte) 110);
		set.clear();
		assertTrue(set.isEmpty());
		assertFalse(set.contains((byte) 180));
		assertFalse(set.contains((byte) 110));
	}

	@Test
	public void testClear_alreadyEmpty() throws Exception {
		final var set = ByteSet.empty();
		set.clear();
		assertTrue(set.isEmpty());
	}

	@Test
	public void testSelect_empty() throws Exception {
		final var set = ByteSet.empty();
		final var selection = set.select(b -> b > 100);
		assertTrue(selection.isEmpty());
	}

	@Test
	public void testSelect() throws Exception {
		final var set = ByteSet.of((byte) 80, (byte) 10, (byte) 101, (byte) 110);
		final var selection = set.select(b -> b > 100);
		assertFalse(selection.contains((byte) 80));
		assertFalse(selection.contains((byte) 10));
		assertTrue(selection.contains((byte) 101));
		assertTrue(selection.contains((byte) 110));
		assertEquals(2, selection.size());
	}

	@Test
	public void testReject_empty() throws Exception {
		final var set = ByteSet.empty();
		final var selection = set.reject(b -> b > 100);
		assertTrue(selection.isEmpty());
	}

	@Test
	public void testReject() throws Exception {
		final var set = ByteSet.of((byte) 80, (byte) 10, (byte) 101, (byte) 110);
		final var selection = set.reject(b -> b > 100);
		assertTrue(selection.contains((byte) 80));
		assertTrue(selection.contains((byte) 10));
		assertFalse(selection.contains((byte) 101));
		assertFalse(selection.contains((byte) 110));
		assertEquals(2, selection.size());
	}

	@Test
	public void testForEach_empty() throws Exception {
		final var set = ByteSet.empty();
		final var invocations = new AtomicInteger(0);
		set.forEach(b -> invocations.incrementAndGet());
		assertEquals(0, invocations.get());
	}

	@Test
	public void testForEach_nonEmpty() throws Exception {
		final var found = synchronizedSet(new HashSet<Byte>());
		final var set = ByteSet.of((byte) 80, (byte) 10, (byte) 101, (byte) 110);
		set.forEach(b -> found.add(b));
		assertEquals(4, found.size());
		assertTrue(found.contains((byte) 80));
		assertTrue(found.contains((byte) 10));
		assertTrue(found.contains((byte) 101));
		assertTrue(found.contains((byte) 110));
	}

	@Test
	public void testForEach_equalBytesInInit() throws Exception {
		final var found = synchronizedSet(new HashSet<Byte>());
		final var invocations = new AtomicInteger(0);
		final var set = ByteSet.of((byte) 80, (byte) 10, (byte) 80, (byte) 110);
		set.forEach(b -> { found.add(b); invocations.incrementAndGet(); });
		assertEquals(3, found.size());
		assertEquals(3, invocations.get());
		assertTrue(found.contains((byte) 80));
		assertTrue(found.contains((byte) 10));
		assertTrue(found.contains((byte) 110));
	}

	@Test
	public void testMap_Empty() throws Exception {
		final var set = ByteSet.empty();
		final List<String> result = set.map(b -> Utils.toHexString(b)).collect(toList());
		assertTrue(result.isEmpty());
	}

	@Test
	public void testMap() throws Exception {
		final var set = ByteSet.of((byte) 0xaa, (byte) 0xbb, (byte) 0x80, (byte) 0x01);
		final List<String> result = set.map(b -> Utils.toHexString(b)).collect(toList());
		assertEquals(4, result.size());
		assertTrue(result.contains("aa"));
		assertTrue(result.contains("bb"));
		assertTrue(result.contains("80"));
		assertTrue(result.contains("01"));
	}

	@Test
	public void testMapToInt_empty() throws Exception {
		final var set = ByteSet.empty();
		final int[] arr = set.mapToInt(Utils::toPositiveInt).toArray();
		assertEquals(0, arr.length);
	}

	@Test
	public void testMapToInt() throws Exception {
		final var set = ByteSet.of((byte) 0xaa, (byte) 0xbb, (byte) 0x80, (byte) 0x01);
		final int[] sortedArray = set.mapToInt(Utils::toPositiveInt).sorted().toArray();
		assertEquals(4, sortedArray.length);
		assertEquals(0x01, sortedArray[0]);
		assertEquals(0x80, sortedArray[1]);
		assertEquals(0xaa, sortedArray[2]);
		assertEquals(0xbb, sortedArray[3]);
	}

	@Test
	public void testAnySatisfy_empty() throws Exception {
		final var set = ByteSet.empty();
		assertFalse(set.anySatisfy(b -> true));
	}

	@Test
	public void testAnySatisfy() throws Exception {
		final var set = ByteSet.of((byte) 0xaa, (byte) 0xbb, (byte) 0x80, (byte) 0x01);
		assertTrue(set.anySatisfy(b -> true));
		assertTrue(set.anySatisfy(b -> b == (byte) 0xbb));
		assertTrue(set.anySatisfy(b -> toPositiveInt(b) < 0xc0));
		assertFalse(set.anySatisfy(b -> toPositiveInt(b) > 0xc0));
	}

	@Test
	public void testAllSatisfy_empty() throws Exception {
		final var set = ByteSet.empty();
		assertFalse(set.allSatisfy(b -> true));
	}

	@Test
	public void testAllSatisfy() throws Exception {
		final var set = ByteSet.of((byte) 0xaa, (byte) 0xbb, (byte) 0x80, (byte) 0x01);
		assertTrue(set.allSatisfy(b -> true));
		assertFalse(set.allSatisfy(b -> b == (byte) 0xbb));
		assertTrue(set.allSatisfy(b -> toPositiveInt(b) < 0xc0));
		assertFalse(set.allSatisfy(b -> toPositiveInt(b) < 0xa0));
		assertFalse(set.allSatisfy(b -> toPositiveInt(b) > 0xc0));
	}

	@Test
	public void testNoneSatisfy_Empty() throws Exception {
		final var set = ByteSet.empty();
		assertTrue(set.noneSatisfy(b -> false));
		assertTrue(set.noneSatisfy(b -> true));
	}

	@Test
	public void testNoneSatisfy() throws Exception {
		final var set = ByteSet.of((byte) 0xaa, (byte) 0xbb, (byte) 0x80, (byte) 0x01);
		assertTrue(set.noneSatisfy(b -> false));
		assertFalse(set.noneSatisfy(b -> true));

		assertFalse(set.noneSatisfy(b -> b == (byte) 0xbb));
		assertFalse(set.noneSatisfy(b -> toPositiveInt(b) < 0xc0));
		assertTrue(set.noneSatisfy(b -> toPositiveInt(b) > 0xc0));
	}

	@Test
	public void testCount_empty() throws Exception {
		final var set = ByteSet.empty();
		assertEquals(0, set.count(b -> true));
	}

	@Test
	public void testCount() throws Exception {
		final var set = ByteSet.of((byte) 0xaa, (byte) 0xbb, (byte) 0x80, (byte) 0x01);
		assertEquals(1, set.count(b -> b == (byte) 0xbb));
		assertEquals(0, set.count(b -> b == (byte) 0xff));
		assertEquals(4, set.count(b -> toPositiveInt(b) < 0xc0));
		assertEquals(2, set.count(b -> toPositiveInt(b) < 0xa0));
		assertEquals(0, set.count(b -> toPositiveInt(b) > 0xc0));
	}

	@Test
	public void testSum_Empty() throws Exception {
		final var set = ByteSet.empty();
		assertEquals(0, set.sum());
	}

	@Test
	public void testSum() throws Exception {
		final var set = ByteSet.of((byte) 2, (byte) 5, (byte) 3);
		assertEquals(10, set.sum());
	}

	@Test
	public void testSum_negative() throws Exception {
		final var set = ByteSet.of((byte) -2, (byte) -5, (byte) -3);
		assertEquals(-10, set.sum());
	}

	@Test
	public void testSum_negativeAndPositiveMix() throws Exception {
		final var set = ByteSet.of((byte) -3, (byte) 5, (byte) 3);
		assertEquals(5, set.sum());
	}

	@Test
	public void testSum_byteOverflow() throws Exception {
		final var set = ByteSet.of((byte) (Byte.MAX_VALUE - 1), Byte.MAX_VALUE);
		assertEquals(253, set.sum());
	}

	@Test
	public void testSumUnsigned_empty() throws Exception {
		final var set = ByteSet.empty();
		assertEquals(0, set.sumUnsigned());
	}

	@Test
	public void testSumUnsigned() throws Exception {
		final var set = ByteSet.of((byte) 0x10, (byte) 0x80, (byte) 5);
		assertEquals(0x95, set.sumUnsigned());
	}

	@Test
	public void testSumUnsigned_ByteOverflow() throws Exception {
		final var set = ByteSet.of((byte) 0xff, (byte) 0xfe);
		assertEquals(0x1fd, set.sumUnsigned());
	}

	@Test(expected = NoSuchElementException.class)
	public void testMin_empty() throws Exception {
		final var set = ByteSet.empty();
		set.min();
	}

	@Test
	public void testMin_single() throws Exception {
		final var set = ByteSet.of((byte) -2);
		assertEquals((byte) -2, set.min());
	}

	@Test
	public void testMin() throws Exception {
		final var set = ByteSet.of((byte) 10, (byte) -5, (byte) 0);
		assertEquals((byte) -5, set.min());
	}

	@Test
	public void testMin_ByteMinMaxValue() throws Exception {
		final var set = ByteSet.of(Byte.MAX_VALUE, Byte.MIN_VALUE, (byte) 0);
		assertEquals(Byte.MIN_VALUE, set.min());
	}

	@Test
	public void testMinIfEmpty_Empty() throws Exception {
		final var set = ByteSet.empty();
		assertEquals((byte) 10, set.minIfEmpty((byte) 10));
	}

	@Test
	public void testMinIfEmpty_NotEmpty() throws Exception {
		final var set = ByteSet.of((byte) 10, (byte) -5, (byte) 0);
		assertEquals((byte) -5, set.minIfEmpty((byte) 2));
	}

	@Test(expected = NoSuchElementException.class)
	public void testMax_empty() throws Exception {
		final var set = ByteSet.empty();
		set.max();
	}

	@Test
	public void testMax_ByteMinMaxValue() throws Exception {
		final var set = ByteSet.of(Byte.MAX_VALUE, Byte.MIN_VALUE, (byte) 0);
		assertEquals(Byte.MAX_VALUE, set.max());
	}

	@Test
	public void testMax_NotEmpty() throws Exception {
		final var set = ByteSet.of((byte) 10, (byte) -5, (byte) 0);
		assertEquals((byte) 10, set.max());
	}

	@Test
	public void testMaxIfEmpty_Empty() throws Exception {
		final var set = ByteSet.empty();
		assertEquals((byte) 10, set.maxIfEmpty((byte) 10));
	}

	@Test
	public void testMaxIfEmpty_NotEmpty() throws Exception {
		final var set = ByteSet.of((byte) 10, (byte) -5, (byte) 0);
		assertEquals((byte) 10, set.maxIfEmpty((byte) 10));
	}

	@Test(expected = ArithmeticException.class)
	public void testAverage_Empty() throws Exception {
		ByteSet.empty().average();
	}

	@Test
	public void testAverage_Single() throws Exception {
		final var set = ByteSet.of((byte) 10);
		assertEquals(10.0, set.average(), 0.01);
	}

	@Test
	public void testAverage_Even() throws Exception {
		final var set = ByteSet.of((byte) 10, (byte) 20);
		assertEquals(15.0, set.average(), 0.01);
	}

	@Test
	public void testAverage_Odd() throws Exception {
		final var set = ByteSet.of((byte) 10, (byte) 40, (byte) 30);
		assertEquals(26.66, set.average(), 0.01);
	}

	@Test(expected = ArithmeticException.class)
	public void testMedian_Empty() throws Exception {
		ByteSet.empty().median();
	}

	@Test
	public void testMedian_Single() throws Exception {
		final var set = ByteSet.of((byte) 10);
		assertEquals(10.0, set.median(), 0.01);
	}

	@Test
	public void testMedian_Even() throws Exception {
		final var set = ByteSet.of((byte) 10, (byte) 20);
		assertEquals(15.0, set.median(), 0.01);
	}

	@Test
	public void testMedian_Odd() throws Exception {
		final var set = ByteSet.of((byte) 10, (byte) 40, (byte) 30);
		assertEquals(30.0, set.median(), 0.01);
	}

	@Test
	public void testToArray_Empty() throws Exception {
		final var set = ByteSet.empty();
		final var arr = set.toArray();
		assertEquals(0, arr.length);
	}

	@Test
	public void testToArray() throws Exception {
		final var set = ByteSet.of((byte) -10, (byte) 40, (byte) 30);
		final var arr = set.toArray();
		Arrays.sort(arr);
		assertEquals((byte) -10, arr[0]);
		assertEquals((byte) 30, arr[1]);
		assertEquals((byte) 40, arr[2]);
	}

	@Test
	public void testToSortedArray_Empty() throws Exception {
		final var set = ByteSet.empty();
		final var arr = set.toSortedArray();
		assertEquals(0, arr.length);
	}

	@Test
	public void testToSortedArray() throws Exception {
		final var set = ByteSet.of((byte) -10, (byte) 40, (byte) 30);
		final var arr = set.toSortedArray();
		assertEquals((byte) -10, arr[0]);
		assertEquals((byte) 30, arr[1]);
		assertEquals((byte) 40, arr[2]);
	}

	@Test
	public void testToSortedArrayUnsigned_empty() throws Exception {
		final var set = ByteSet.empty();
		final var arr = set.toSortedArrayUnsigned();
		assertEquals(0, arr.length);
	}

	@Test
	public void testToSortedArrayUnsigned() throws Exception {
		final var set = ByteSet.of((byte) 0xff, (byte) 0x7f, (byte) 0x80, (byte) 0);
		final var arr = set.toSortedArrayUnsigned();
		assertEquals((byte) 0x00, arr[0]);
		assertEquals((byte) 0x7f, arr[1]);
		assertEquals((byte) 0x80, arr[2]);
		assertEquals((byte) 0xff, arr[3]);
	}

	@Test
	public void testToString_empty() throws Exception {
		final var set = ByteSet.empty();
		assertEquals("ByteSet [bytes=, size=0]", set.toString());
	}

	@Test
	public void testToString_() throws Exception {
		final var set = ByteSet.of((byte) 0xff, (byte) 0x7f, (byte) 0x80, (byte) 0);
		System.out.println(set);
		assertEquals("ByteSet [bytes=0,7f,80,ff, size=4]", set.toString());
	}

	@Test
	public void testEquals_sameObject() throws Exception {
		final var set = ByteSet.empty();
		assertTrue(set.equals(set));
	}

	@Test
	public void testEquals_null() throws Exception {
		final var set = ByteSet.empty();
		assertFalse(set.equals(null));
	}

	@Test
	public void testEquals_subclass() throws Exception {
		final var set = ByteSet.empty();
		final var set2 = new ByteSet() {
			@SuppressWarnings("unused")
			boolean isPopulated() {
				return !isEmpty();
			}
		};
		assertFalse(set.equals(set2));
		assertFalse("equals expected to be symmetric!", set2.equals(set));
	}

	@Test
	public void testEquals_sameSizeDifferentContent() throws Exception {
		final var set = ByteSet.of((byte) 0xff, (byte) 0x7f);
		final var set2 = ByteSet.of((byte) 0x80, (byte) 0);
		assertFalse(set.equals(set2));
		assertFalse("equals expected to be symmetric!", set2.equals(set));
	}

	@Test
	public void testEquals_sameContent() throws Exception {
		final var set = ByteSet.of((byte) 0xff, (byte) 0x7f);
		final var set2 = ByteSet.of((byte) 0xff, (byte) 0x7f);
		assertTrue(set.equals(set2));
		assertTrue("equals expected to be symmetric!", set2.equals(set));
	}

	@Test
	public void testEquals_sameContentDifferentOrder() throws Exception {
		final var set = ByteSet.of((byte) 0xff, (byte) 0x7f);
		final var set2 = ByteSet.of((byte) 0x7f, (byte) 0xff);
		assertTrue(set.equals(set2));
		assertTrue("equals expected to be symmetric!", set2.equals(set));
	}

	@Test
	public void testEquals_differentSizeOverlappingContent() throws Exception {
		final var set = ByteSet.of((byte) 0xff, (byte) 0x7f);
		final var set2 = ByteSet.of((byte) 0xff);
		assertFalse(set.equals(set2));
		assertFalse("equals expected to be symmetric!", set2.equals(set));
	}

	@Test
	public void testHashCode_sameContentSameHashCode() throws Exception {
		assertEquals(ByteSet.empty().hashCode(), ByteSet.of((byte) 123).without((byte) 123).hashCode());
		assertEquals(ByteSet.of((byte) 123, (byte) 10).hashCode(), ByteSet.of((byte) 10).with((byte) 123).hashCode());
	}

	@Test
	public void testHashCode_stable() throws Exception {
		assertEquals(ByteSet.empty().hashCode(), ByteSet.empty().hashCode());
		assertEquals(ByteSet.of((byte) 123).hashCode(), ByteSet.of((byte) 123).hashCode());
	}

	@Test
	public void testHashCode_diff() throws Exception {
		// this cannot be not guaranteed, as int has less than 256 bits, but should hold in about 3/4 of all cases
		assertNotEquals(ByteSet.of((byte) 12, (byte) -2).hashCode(), ByteSet.of((byte) 10, (byte) 0).hashCode());
		assertNotEquals(ByteSet.of((byte) 100).hashCode(), ByteSet.of((byte) 123).hashCode());
	}
}
