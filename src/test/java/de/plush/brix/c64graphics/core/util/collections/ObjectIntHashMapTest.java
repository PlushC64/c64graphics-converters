package de.plush.brix.c64graphics.core.util.collections;

import static de.plush.brix.c64graphics.core.util.ObjectIntPair.pair;
import static java.util.Comparator.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Test;

import de.plush.brix.c64graphics.core.util.ObjectIntPair;

public class ObjectIntHashMapTest {

	@Test
	public void testObjectIntHashMap() throws Exception {
		final var map = new ObjectIntHashMap<String>();
		assertTrue(map.isEmpty());
		assertEquals(ObjectIntHashMap.DEFAULT_NO_VALUE, map.no_value);
	}

	@Test
	public void testObjectIntHashMapInt_capacityTooSmall() throws Exception {
		@SuppressWarnings("unused")
		final var map = new ObjectIntHashMap<String>(1);
		// will just override user request with it's own minimum capacity.
	}

	@Test
	public void testGet_EmptyMap() throws Exception {
		final var map = new ObjectIntHashMap<String>();
		final var random = new Random(123);
		for (int x = 0; x < 100; ++x) {
			final String key = "" + random.nextInt();
			assertEquals(map.no_value, map.get(key), "unexpected value for key: " + key);
		}
		assertEquals(map.no_value, map.get(null), "unexpected value for key: " + null);
	}

	@Test
	public void testGet_free_key() throws Exception {
		final var map = ObjectIntHashMap.<String> of(null, 110);
		assertEquals(110, map.get(null));
	}

	@Test
	public void testGet() throws Exception {
		final var map = ObjectIntHashMap.of(//
				"10", 110, //
				"20", 220//
		);
		assertEquals(110, map.get("10"));
		assertEquals(220, map.get("20"));
	}

	@Test
	public void testGet_null() throws Exception {
		final var map = ObjectIntHashMap.of(//
				"10", 110, //
				"20", 220//
		);
		assertEquals(map.no_value, map.get(null));
	}

	@Test
	public void testGetOptional_EmptyMap() throws Exception {
		final var map = new ObjectIntHashMap<String>();
		final var random = new Random(123);
		for (int x = 0; x < 100; ++x) {
			final String key = "" + random.nextInt();
			assertTrue(map.getOptional(key).isEmpty(), "unexpected value for key: " + key);
		}
		assertTrue(map.getOptional(null).isEmpty(), "unexpected value for key: " + null);
	}

	@Test
	public void testGetOptional_free_key() throws Exception {
		final var map = ObjectIntHashMap.<String> of(null, 110);
		assertTrue(map.getOptional(null).isPresent());
		assertEquals(110, map.getOptional(null).getAsInt());
	}

	@Test
	public void testGetOptional() throws Exception {
		final var map = ObjectIntHashMap.of(//
				"10", 110, //
				"20", 220//
		);
		assertTrue(map.getOptional("10").isPresent());
		assertEquals(110, map.getOptional("10").getAsInt());
		assertTrue(map.getOptional("20").isPresent());
		assertEquals(110, map.getOptional("10").getAsInt());
	}

	@Test
	public void testUpdate_valueDoesNotExist() throws Exception {
		final var map = ObjectIntHashMap.of(//
				"10", 110, //
				"20", 220//
		);
		map.update("50", 550, v -> v + 100);
		assertEquals(550, map.get("50"));
	}

	@Test
	public void testUpdate_valueDoesNotExist_usingFreeKey() throws Exception {
		final var map = ObjectIntHashMap.of(//
				"10", 110, //
				"20", 220 //
		);
		map.update(null, 550, v -> v + 100);
		assertEquals(550, map.get(null));
	}

	@Test
	public void testUpdate_valueExists() throws Exception {
		final var map = ObjectIntHashMap.of(//
				"10", 110, //
				"20", 220, //
				"50", 550//
		);
		map.update("50", 550, v -> v + 100);
		assertEquals(650, map.get("50"));
	}

	@Test
	public void testUpdate_valueExists_usingFreeKey() throws Exception {
		final var map = ObjectIntHashMap.of(//
				"10", 110, //
				"20", 220, //
				null, 550);
		map.update(null, 550, v -> v + 100);
		assertEquals(650, map.get(null));
	}

	@Test
	public void testPutUpdateGetOptional_hashCollisionsAndRehash() throws Exception {
		final var map = new ObjectIntHashMap<String>(0);
		for (int t = 0; t < 64; ++t) {
			map.put("" + t, t + 100);
		}
		for (int t = 0; t < 64; ++t) {
			map.update("" + t, t + 100, v -> v + 100);
		}
		for (int t = 0; t < 64; ++t) {
			assertTrue(map.getOptional("" + t).isPresent());
			assertEquals(t + 200, map.getOptional("" + t).getAsInt());
		}
		for (int t = 64; t < 128; ++t) {
			map.update("" + t, t + 100, v -> v + 100);
		}
		for (int t = 64; t < 128; ++t) {
			assertTrue(map.getOptional("" + t).isPresent());
			assertEquals(t + 100, map.getOptional("" + t).getAsInt());
		}
	}

	@Test
	public void testPutGetOptional_hashCollisionsAndRehash() throws Exception {
		final var map = new ObjectIntHashMap<String>(16);
		for (int t = 0; t < 64; ++t) {
			map.put("" + t, t + 100);
		}
		for (int t = 0; t < 64; ++t) {
			assertTrue(map.getOptional("" + t).isPresent());
			assertEquals(t + 100, map.getOptional("" + t).getAsInt());
		}
		for (int t = 64; t < 128; ++t) {
			assertTrue(map.getOptional("" + t).isEmpty());
		}
	}

	@Test
	public void testPutGet_hashCollisionsAndRehash() throws Exception {
		final var map = new ObjectIntHashMap<String>(16);
		for (int t = 0; t < 64; ++t) {
			map.put("" + t, t + 100);
		}
		for (int t = 0; t < 64; ++t) {
			assertEquals(t + 100, map.get("" + t));
		}
	}

	@Test
	public void testPutRemovePutGet_hashCollisionsAndRehash() throws Exception {
		final var map = new ObjectIntHashMap<String>(16);
		for (int t = 0; t < 64; ++t) {
			map.put("" + t, t + 100);
		}

		for (int t = 0; t < 64; ++t) {
			assertEquals(t + 100, map.get("" + t));
		}

		// remove some
		for (int t = 16; t < 32; ++t) {
			map.remove("" + t);
		}

		for (int t = 0; t < 16; ++t) {
			assertEquals(t + 100, map.get("" + t));
		}
		for (int t = 16; t < 32; ++t) {
			assertEquals(map.no_value, map.get("" + t));
		}
		for (int t = 32; t < 64; ++t) {
			assertEquals(t + 100, map.get("" + t));
		}

		// reinsert all
		for (int t = 0; t < 64; ++t) {
			map.put("" + t, t + 100);
		}

		for (int t = 0; t < 64; ++t) {
			assertEquals(t + 100, map.get("" + t));
		}

		// remove all
		for (int t = 0; t < 64; ++t) {
			map.remove("" + t);
		}
		assertTrue(map.isEmpty());

	}

	@Test
	public void testRemovFree_key() throws Exception {
		final var map = ObjectIntHashMap.of(null, 110);
		map.remove(null);
		assertEquals(map.no_value, map.get(null));
	}

	@Test
	public void testRemovContainsKey_emptyMapShouldNotContainfreeKey() throws Exception {
		final var map = new ObjectIntHashMap<String>();
		assertFalse(map.containsKey(null));
	}

	@Test
	public void testRemovContainsKey_mapWitFreeKeyMappingShouldContainFreeKey() throws Exception {
		final var map = ObjectIntHashMap.of(null, 110);
		assertTrue(map.containsKey(null));
	}

	@Test
	public void testRemovContainsKey_mapWithMappingToNoValueShouldContainKey() throws Exception {
		final var map = new ObjectIntHashMap<String>();
		map.put("10", map.no_value);
		assertTrue(map.containsKey("10"));
	}

	@Test
	public void testSize() throws Exception {
		final var map = ObjectIntHashMap.of(//
				"10", 110, //
				"20", 220, //
				"30", 330//
		);
		assertEquals(3, map.size());
	}

	@Test
	public void testSize_decreasesOnRemove() throws Exception {
		final var map = ObjectIntHashMap.of(//
				"10", 110, //
				"20", 220, //
				"30", 330, //
				"40", 440//
		);
		assertEquals(4, map.size());
		map.remove("20");
		assertEquals(3, map.size());
	}

	@Test
	public void testSize_doesNotDecreasesOnRemovalOfNonExistingElement() throws Exception {
		final var map = ObjectIntHashMap.of(//
				"10", 110, //
				"20", 220, //
				"30", 330, //
				"40", 440, //
				"50", 550//
		);
		assertEquals(5, map.size());
		map.remove("110");
		assertEquals(5, map.size());
	}

	@Test
	public void testSize_increasesOnAddingfAnElement() throws Exception {
		final var map = ObjectIntHashMap.of(//
				"10", 110, //
				"20", 220, //
				"30", 330, //
				"40", 440, //
				"50", 550, //
				"60", 660//
		);
		assertEquals(6, map.size());
		map.put("70", 770);
		assertEquals(7, map.size());
	}

	@Test
	public void testSize_doesNotIncreasesOnReplacingAnElement() throws Exception {
		final var map = ObjectIntHashMap.of(//
				"10", 110, //
				"20", 220, //
				"30", 330, //
				"40", 440, //
				"50", 550, //
				"60", 660, //
				"70", 770//
		);
		assertEquals(7, map.size());
		map.put("40", 4400);
		assertEquals(7, map.size());
	}

	@Test
	public void testIsEmpty_notEmpty() throws Exception {
		final var map = ObjectIntHashMap.of(//
				"10", 110, //
				"20", 220, //
				"30", 330, //
				"40", 440, //
				"50", 550, //
				"60", 660, //
				"70", 770, //
				"80", 880//
		);
		assertFalse(map.isEmpty());
	}

	@Test
	public void testIsEmpty_afterRemoval() throws Exception {
		final var map = ObjectIntHashMap.of(//
				"10", 110 //
		);
		map.remove("10");
		assertTrue(map.isEmpty());
	}

	@Test
	public void testKeyStream() throws Exception {
		final var map = ObjectIntHashMap.of(//
				"10", 110, //
				"20", 220, //
				"30", 330, //
				"40", 440, //
				"50", 550, //
				"60", 660, //
				"70", 770, //
				"80", 880, //
				"90", 990//
		);
		final var keys = map.keyStream().sorted().toArray();
		final var expectedKeys = new String[] { "10", "20", "30", "40", "50", "60", "70", "80", "90" };
		assertArrayEquals(expectedKeys, keys);
	}

	@Test
	public void testKeyStream_includesFreeKey() throws Exception {
		final var map = ObjectIntHashMap.of(//
				"10", 110, //
				"20", 220, //
				"30", 330, //
				"40", 440, //
				"50", 550, //
				"60", 660, //
				"70", 770, //
				"80", 880, //
				"90", 990, //
				"100", 1000//
		);
		map.put(null, 2200);
		final var keys = map.keyStream().sorted(nullsLast(naturalOrder())).toArray();
		final var expectedKeys = new String[] { "10", "20", "30", "40", "50", "60", "70", "80", "90", "100", null };
		Arrays.sort(expectedKeys, nullsLast(naturalOrder())); // because of NullKey that could be anything
		assertArrayEquals(expectedKeys, keys);
	}

	@Test
	public void testKeyStream_empty() throws Exception {
		final var map = new ObjectIntHashMap<String>();
		assertEquals(0, map.keyStream().count());
	}

	@Test
	public void testValueStream() throws Exception {
		final var map = ObjectIntHashMap.of(//
				pair("10", 110), //
				pair("20", 220), //
				pair("30", 330), //
				pair("40", 440), //
				pair("50", 550), //
				pair("60", 660), //
				pair("70", 770), //
				pair("80", 880), //
				pair("90", 990), //
				pair("100", 1000), //
				pair("110", 1100) //
		);
		final var values = map.valueStream().sorted().toArray();
		final var expectedValues = new int[] { 110, 220, 330, 440, 550, 660, 770, 880, 990, 1000, 1100 };
		assertArrayEquals(expectedValues, values);
	}

	@Test
	public void testValueStream_usingFreeKey() throws Exception {
		final var map = ObjectIntHashMap.of(//
				pair("10", 110), //
				pair("20", 220), //
				pair("30", 330), //
				pair("40", 440), //
				pair("50", 550), //
				pair("60", 660), //
				pair("70", 770), //
				pair("80", 880), //
				pair("90", 990), //
				pair("100", 1000), //
				pair(null, 1100) //
		);
		final var values = map.valueStream().sorted().toArray();
		final var expectedValues = new int[] { 110, 220, 330, 440, 550, 660, 770, 880, 990, 1000, 1100 };
		assertArrayEquals(expectedValues, values);
	}

	@Test
	public void testValueStream_usingNoValue() throws Exception {
		final var map = ObjectIntHashMap.of(//
				pair("10", 110), //
				pair("20", 220), //
				pair("30", 330), //
				pair("40", 440), //
				pair("50", 550), //
				pair("60", 660), //
				pair("70", 770), //
				pair("80", 880), //
				pair("90", 990), //
				pair("100", 1000) //
		);
		map.put("100", map.no_value); // is essentially the same as deleting a value

		final var values = map.valueStream().sorted().toArray();
		final var expectedValues = new int[] { 110, 220, 330, 440, 550, 660, 770, 880, 990, map.no_value };
		Arrays.sort(expectedValues);
		assertArrayEquals(expectedValues, values);
	}

	@Test
	public void testValueStream_empty() throws Exception {
		final var map = new ObjectIntHashMap<String>();
		assertEquals(0, map.valueStream().count());
	}

	@Test
	public void testEntryStream_empty() throws Exception {
		final var map = new ObjectIntHashMap<String>();
		assertEquals(0, map.entryStream().count());
	}

	@Test
	public void testEntryStream_populated() throws Exception {
		final var map = ObjectIntHashMap.of(//
				pair("10", 110), //
				pair("20", 220), //
				pair("30", 330) //
		);
		assertEquals(3, map.entryStream().count());
		assertTrue(map.entryStream().anyMatch(pair("10", 110)::equals));
		assertTrue(map.entryStream().anyMatch(pair("20", 220)::equals));
		assertTrue(map.entryStream().anyMatch(pair("30", 330)::equals));
	}

	@Test
	public void testEntryStream_populated_usingFreeKey() throws Exception {
		final var map = ObjectIntHashMap.of(//
				pair("10", 110), //
				pair("20", 220), //
				pair("30", 330), //
				pair(null, 440));
		assertEquals(4, map.entryStream().count());
		assertTrue(map.entryStream().anyMatch(pair("10", 110)::equals));
		assertTrue(map.entryStream().anyMatch(pair("20", 220)::equals));
		assertTrue(map.entryStream().anyMatch(pair("30", 330)::equals));
		assertTrue(map.entryStream().anyMatch(pair(null, 440)::equals));
	}

	@Test
	public void testForEachKey_nullKeyIncluded() throws Exception {
		final var map = ObjectIntHashMap.of(//
				pair("10", 110), //
				pair("20", 220), //
				pair("30", 330), //
				pair(null, 440));
		final AtomicBoolean found = new AtomicBoolean();
		map.forEachKey(k -> {
			if (k == null) {
				found.set(true);
			}
		});
		assertTrue(found.get());
	}

	@Test
	public void testToArray() throws Exception {
		final var map = ObjectIntHashMap.of(//
				pair("10", 110), //
				pair("20", 220), //
				pair("30", 330) //
		);
		final var arr = map.toArray();
		Arrays.sort(arr);
		assertEquals(3, arr.length);
		assertArrayEquals(new ObjectIntPair[] { pair("10", 110), pair("20", 220), pair("30", 330), }, arr);
	}

	@Test
	public void testToSortedArray() throws Exception {
		final var map = ObjectIntHashMap.of(//
				pair("20", 220), //
				pair("10", 110), //
				pair("30", 330) //
		);
		final var arr = map.toSortedArray();
		assertEquals(3, arr.length);
		assertArrayEquals(new ObjectIntPair[] { pair("10", 110), pair("20", 220), pair("30", 330), }, arr);
	}

	@Test
	public void testToSortedArray_withNullKey() throws Exception {
		final var map = ObjectIntHashMap.of(//
				pair(null, 250), //
				pair("20", 220), //
				pair("10", 110), //
				pair("30", 330) //
		);
		final var arr = map.toSortedArray();
		assertEquals(4, arr.length);
		assertArrayEquals(new ObjectIntPair[] { pair(null, 250), pair("10", 110), pair("20", 220), pair("30", 330), }, arr);
	}

	@Test
	public void testEquals_bothEmpty() throws Exception {
		assertTrue(new ObjectIntHashMap<String>().equals(new ObjectIntHashMap<String>()));
	}

	@Test
	public void testEquals_symmetry() throws Exception {
		final var map1 = new ObjectIntHashMap<String>();
		final var map2 = new ObjectIntHashMap<String>() {
			@SuppressWarnings("unused")
			boolean isPopulated() {
				return !isEmpty();
			}
		};
		assertFalse(map1.equals(map2));
		assertFalse(map2.equals(map1), "equals expected to be symmetric!");
	}

	@Test
	public void testEquals_emptyVsFilled() throws Exception {
		assertFalse(new ObjectIntHashMap<String>(16, null).equals(ObjectIntHashMap.of("10", 110)));
	}

	@Test
	public void testEquals_sameObject() throws Exception {
		final var map = new ObjectIntHashMap<String>();
		assertTrue(map.equals(map));
	}

	@Test
	public void testEquals_null() throws Exception {
		assertFalse(new ObjectIntHashMap<String>().equals(null));
	}

	@Test
	public void testEquals_sameContentButDifferentCapacity() throws Exception {
		final var map1 = new ObjectIntHashMap<String>(16);
		map1.put("10", 110);
		final var map2 = new ObjectIntHashMap<String>(32);
		map2.put("10", 110);
		assertTrue(map1.equals(map2));
		assertTrue(map2.equals(map1), "equals expected to be symmetric!");
	}

	@Test
	public void testEquals_sameContentButDifferentNoValue() throws Exception {
		final var map1 = new ObjectIntHashMap<String>(Integer.MIN_VALUE + 1, null);
		map1.put("10", 110);
		final var map2 = new ObjectIntHashMap<String>(0, null);
		map2.put("10", 110);
		assertTrue(map1.equals(map2));
		assertTrue(map2.equals(map1), "equals expected to be symmetric!");
	}

	@Test
	public void testEquals_sameContentUsingNoValue() throws Exception {
		final var map1 = new ObjectIntHashMap<String>(10, null); // no_value = 10
		map1.put("100", 10); // value == no_value
		final var map2 = new ObjectIntHashMap<String>(0, null); // no_value = 0
		map2.put("100", 10); // value != no_value
		assertTrue(map1.equals(map2));
		assertTrue(map2.equals(map1), "equals expected to be symmetric!");
	}

	@Test
	public void testEquals_sameContentUsingNullKey() throws Exception {
		final var map1 = new ObjectIntHashMap<String>();
		map1.put(null, 110);
		final var map2 = new ObjectIntHashMap<String>();
		map2.put(null, 110);
		assertTrue(map1.equals(map2));
		assertTrue(map2.equals(map1), "equals expected to be symmetric!");
	}

	@Test
	public void testEquals_sameSizeDifferentKeys() throws Exception {
		assertFalse(ObjectIntHashMap.of(20, 110).equals(ObjectIntHashMap.of(10, 110)));
	}

	@Test
	public void testEquals_sameSizeDifferentValues() throws Exception {
		assertFalse(ObjectIntHashMap.of(10, 110).equals(ObjectIntHashMap.of(10, 220)));
	}

	@Test
	public void testHashCode_empty() throws Exception {
		assertEquals(new ObjectIntHashMap<String>().hashCode(), new ObjectIntHashMap<String>().hashCode());
	}

	@Test
	public void testHashCode_filled_sameEntries() throws Exception {
		assertEquals(ObjectIntHashMap.of("10", 110).hashCode(), ObjectIntHashMap.of("10", 110).hashCode());
	}

	@Test
	public void testHashCode_filled_sameEntriesDiifferentOrder() throws Exception {
		assertEquals(ObjectIntHashMap.of("10", 110, "20", 220).hashCode(), ObjectIntHashMap.of("20", 220, "10", 110).hashCode());
	}

	@Test
	public void testHashCode_filled_differentValues() throws Exception {
		assertNotEquals(ObjectIntHashMap.of("10", 110).hashCode(), ObjectIntHashMap.of("10", 220).hashCode());
	}

	@Test
	public void testHashCode_filled_differentKeys() throws Exception {
		assertNotEquals(ObjectIntHashMap.of("10", 110).hashCode(), ObjectIntHashMap.of("20", 110).hashCode());
	}

	@Test
	public void testHashCode_sameResultOnSubsequentCallsWithNoMofification() throws Exception {
		final var map = ObjectIntHashMap.of("10", 110);
		final int hashCode = map.hashCode();
		assertEquals(hashCode, map.hashCode());
	}

	@Test
	public void testHashCode_changesOnPut() throws Exception {
		final var map = ObjectIntHashMap.of("10", 110);
		final int hashCode = map.hashCode();
		map.put("20", 220);
		assertNotEquals(hashCode, map.hashCode());
	}

	@Test
	public void testHashCode_changesOnRemove() throws Exception {
		final var map = ObjectIntHashMap.of("10", 110, "20", 220);
		final int hashCode = map.hashCode();
		map.remove("20");
		assertNotEquals(hashCode, map.hashCode());
	}

	@Test
	public void testHashCode_changesOnClear() throws Exception {
		final var map = ObjectIntHashMap.of("10", 110, "20", 220);
		final int hashCode = map.hashCode();
		map.clear();
		assertNotEquals(hashCode, map.hashCode());
	}

	@Test
	public void testHashCode_sameResultAfterRemoveAndAdd() throws Exception {
		final var map = ObjectIntHashMap.of("10", 110);
		final int hashCode = map.hashCode();
		map.remove("10");
		map.put("10", 110);
		assertEquals(hashCode, map.hashCode());
	}

	@Test
	public void testHashCode_sameContentDifferentCapacity() throws Exception {
		final var map1 = new ObjectIntHashMap<String>(16);
		map1.put("10", 110);
		final var map2 = new ObjectIntHashMap<String>(32);
		map2.put("10", 110);
		assertEquals(map1.hashCode(), map2.hashCode());
	}

	@Test
	public void testHashCode_sameContentDifferentNoValue() throws Exception {
		final var map1 = new ObjectIntHashMap<String>(999, null);
		map1.put("10", 110);
		final var map2 = new ObjectIntHashMap<String>(0, null);
		map2.put("10", 110);
		assertEquals(map1.hashCode(), map2.hashCode());
	}

	@Test
	public void testHashCode_sameContentUsingNoValue() throws Exception {
		final var map1 = new ObjectIntHashMap<String>(10, null); // no_value = 10
		map1.put("100", 10); // value == no_value
		final var map2 = new ObjectIntHashMap<String>(0, null); // no_value = 0
		map2.put("100", 10); // value != no_value
		assertEquals(map1.hashCode(), map2.hashCode());
	}

	@Test
	public void testHashCode_sameContentUsingNullKey() throws Exception {
		final var map1 = new ObjectIntHashMap<String>();
		map1.put(null, 110);
		final var map2 = new ObjectIntHashMap<String>();
		map2.put(null, 110);
		assertEquals(map1.hashCode(), map2.hashCode());
	}

	@Test
	public void testClear_removesMappings() throws Exception {
		final var map = ObjectIntHashMap.of("10", 110, "20", 220);
		map.clear();
		assertFalse(map.containsKey(10));
		assertFalse(map.containsKey(20));
		assertTrue(map.get("10") == map.no_value);
		assertTrue(map.get("20") == map.no_value);
	}

	@Test
	public void testClear_removesNullKeymapping() throws Exception {
		final var map = new ObjectIntHashMap<String>();
		map.put(null, 110);
		map.put("20", 220);
		map.clear();
		assertFalse(map.containsKey(null));
		assertFalse(map.containsKey("20"));
		assertTrue(map.get(null) == map.no_value);
		assertTrue(map.get("20") == map.no_value);
	}

	@Test
	public void testClear_size_becomesZero() throws Exception {
		final var map = ObjectIntHashMap.of("10", 110, "20", 220);
		map.clear();
		assertEquals(0, map.size());
	}

	@Test
	public void testClear_isEmpty() throws Exception {
		final var map = ObjectIntHashMap.of("10", 110, "20", 220);
		map.clear();
		assertTrue(map.isEmpty());
	}

	@Test
	public void testClear_makesToArrayReturnAnEmptyArray() throws Exception {
		final var map = ObjectIntHashMap.of("10", 110, "20", 220);
		map.clear();
		final var arr = map.toArray();
		assertArrayEquals(new ObjectIntPair[] {}, arr);
	}

	@Test
	public void testClear_makesToSortedArrayReturnAnEmptyArray() throws Exception {
		final var map = ObjectIntHashMap.of("10", 110, "20", 220);
		map.clear();
		final var arr = map.toSortedArray();
		assertArrayEquals(new ObjectIntPair[] {}, arr);
	}

	@Test
	public void testClear_doesNotResetDefaultNoValue() throws Exception {
		final var map = new ObjectIntHashMap<String>(1000, null);
		map.clear();
		assertEquals(1000, map.no_value);
	}

	@Test
	public void testClear_andRefillWorks() throws Exception {
		final var map = ObjectIntHashMap.of("10", 110, "20", 220);
		map.clear();
		map.put("30", 330);
		map.put("40", 440);
		assertEquals(2, map.size());
		final var arr = map.toSortedArray();
		assertArrayEquals(new ObjectIntPair[] { pair("30", 330), pair("40", 440) }, arr);
	}

}
