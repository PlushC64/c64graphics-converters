package de.plush.brix.c64graphics.core.util.collections;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import de.plush.brix.c64graphics.core.util.CountableInt;

public class IntHashBagTest {

	@Test
	public void testIntHashBag() throws Exception {

		final var bag = new IntHashBag();
		assertTrue(bag.isEmpty());
		assertEquals(0, bag.size());
		assertEquals(0, bag.sizeDistinct());
	}

	@Test
	public void testIntHashBagInt_negative_value() throws Exception {
		final var bag = new IntHashBag(-1); // expected to go with the minimum capacity
		assertTrue(bag.isEmpty());
		assertEquals(0, bag.size());
		assertEquals(0, bag.sizeDistinct());
	}

	@Test
	public void testIntHashBagInt() throws Exception {
		final var bag = new IntHashBag(256);
		assertTrue(bag.isEmpty());
		assertEquals(0, bag.size());
		assertEquals(0, bag.sizeDistinct());
	}

	@Test
	public void testEmpty() throws Exception {
		final var bag = IntHashBag.empty();
		assertTrue(bag.isEmpty());
	}

	@Test
	public void testOf() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 3);
		assertFalse(bag.isEmpty());
		assertEquals(6, bag.size());
		assertEquals(3, bag.sizeDistinct());
		assertTrue(bag.contains(1));
		assertTrue(bag.contains(2));
		assertTrue(bag.contains(3));
	}

	@Test
	public void testAddInt() throws Exception {
		final var bag = IntHashBag.empty();
		boolean change = bag.add(1);
		assertTrue(change);
		assertFalse(bag.isEmpty());
		assertTrue(bag.contains(1));
		assertEquals(1, bag.occurrencesOf(1));

		change = bag.add(2);
		assertTrue(change);
		assertTrue(bag.contains(2));
		assertEquals(2, bag.size());
		assertEquals(2, bag.sizeDistinct());

		change = bag.add(2);
		assertTrue(change);
		assertTrue(bag.contains(2));
		assertEquals(2, bag.occurrencesOf(2));
		assertEquals(3, bag.size());
		assertEquals(2, bag.sizeDistinct());
	}

	@Test
	public void testAddCountableInt() throws Exception {
		final var bag = IntHashBag.empty();
		boolean change = bag.add(CountableInt.ofValue(1).withCount(3));
		assertTrue(change);
		assertTrue(bag.contains(1));
		assertEquals(3, bag.occurrencesOf(1));
		assertEquals(3, bag.size());
		assertEquals(1, bag.sizeDistinct());

		change = bag.add(CountableInt.ofValue(2).withCount(6));
		assertTrue(change);
		assertTrue(bag.contains(2));
		assertEquals(6, bag.occurrencesOf(2));
		assertEquals(9, bag.size());
		assertEquals(2, bag.sizeDistinct());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddCountableInt_negativeValue() throws Exception {
		final var bag = IntHashBag.empty();
		bag.add(CountableInt.ofValue(1).withCount(-3));
	}

	@Test
	public void testAddCountableInt_zeroValue() throws Exception {
		final var bag = IntHashBag.empty();
		final boolean change = bag.add(CountableInt.ofValue(1).withCount(0));
		assertFalse(change);
		assertFalse(bag.contains(1));
		assertEquals(0, bag.occurrencesOf(1));
		assertEquals(0, bag.size());
		assertEquals(0, bag.sizeDistinct());
		assertTrue(bag.isEmpty());
	}

	@Test
	public void testAddAllIntArray_empty() throws Exception {
		final var bag = IntHashBag.empty();
		final boolean change = bag.addAll(new int[0]);
		assertFalse(change);
		assertEquals(0, bag.size());
		assertEquals(0, bag.sizeDistinct());
		assertTrue(bag.isEmpty());
	}

	@Test
	public void testAddAllIntArray() throws Exception {
		final var bag = IntHashBag.empty();
		final boolean change = bag.addAll(1, 2, 2, 3, 3, 3);
		assertTrue(change);
		assertFalse(bag.isEmpty());
		assertEquals(6, bag.size());
		assertEquals(3, bag.sizeDistinct());
		assertTrue(bag.contains(1));
		assertTrue(bag.contains(2));
		assertTrue(bag.contains(3));
		assertEquals(1, bag.occurrencesOf(1));
		assertEquals(2, bag.occurrencesOf(2));
		assertEquals(3, bag.occurrencesOf(3));
	}

	@Test
	public void testAddAllIntArrayList_empty() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3);
		final boolean change = bag.addAll(IntArrayList.empty());
		assertFalse(change);
		assertFalse(bag.isEmpty());
		assertEquals(3, bag.size());
		assertEquals(3, bag.sizeDistinct());
	}

	@Test
	public void testAddAllIntArrayList() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3);
		final boolean change = bag.addAll(IntArrayList.of(3, 4, 5, 6, 6));
		assertTrue(change);
		assertFalse(bag.isEmpty());
		assertEquals(8, bag.size());
		assertEquals(6, bag.sizeDistinct());
		assertTrue(bag.contains(1));
		assertTrue(bag.contains(2));
		assertTrue(bag.contains(3));
		assertTrue(bag.contains(4));
		assertTrue(bag.contains(5));
		assertTrue(bag.contains(6));
		assertEquals(1, bag.occurrencesOf(1));
		assertEquals(1, bag.occurrencesOf(2));
		assertEquals(2, bag.occurrencesOf(3));
		assertEquals(1, bag.occurrencesOf(4));
		assertEquals(1, bag.occurrencesOf(5));
		assertEquals(2, bag.occurrencesOf(6));
	}

	@Test
	public void testAddAllCountableIntArray_empty() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3);
		final boolean change = bag.addAll(new CountableInt[] {});
		assertFalse(change);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddAllCountableIntArray_negativeCount() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3);
		bag.addAll(new CountableInt[] { //
				CountableInt.ofValue(1).withCount(-1)//
		});
	}

	@Test
	public void testAddAllCountableIntArray_zeroCount() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3);
		final boolean change = bag.addAll(new CountableInt[] { //
				CountableInt.ofValue(1).withCount(0), //
		});
		assertFalse(change);
	}

	@Test(expected = ArithmeticException.class)
	public void testAddAllCountableIntArray_longCount() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3);
		final boolean change = bag.addAll(new CountableInt[] { //
				CountableInt.ofValue(1).withCount(1L + Integer.MAX_VALUE), //
		});
		assertFalse(change);
	}

	@Test
	public void testAddAllCountableIntArray() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3);
		final boolean change = bag.addAll(new CountableInt[] { //
				CountableInt.ofValue(1).withCount(1), //
				CountableInt.ofValue(2).withCount(5), //
				CountableInt.ofValue(4).withCount(8), //
		});
		assertTrue(change);
		assertFalse(bag.isEmpty());
		assertEquals(17, bag.size());
		assertEquals(4, bag.sizeDistinct());
		assertTrue(bag.contains(1));
		assertTrue(bag.contains(2));
		assertTrue(bag.contains(3));
		assertTrue(bag.contains(4));
		assertEquals(2, bag.occurrencesOf(1));
		assertEquals(6, bag.occurrencesOf(2));
		assertEquals(1, bag.occurrencesOf(3));
		assertEquals(8, bag.occurrencesOf(4));
	}

	@Test
	public void testAddAllIntHashBag_empty() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3);
		final boolean change = bag.addAll(IntHashBag.empty());
		assertFalse(change);
		assertEquals(3, bag.size());
	}

	@Test
	public void testAddAllIntHashBag() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3);
		final boolean change = bag.addAll(IntHashBag.of(3, 4, 5));
		assertTrue(change);
		assertEquals(6, bag.size());
		assertEquals(5, bag.sizeDistinct());
		assertEquals(1, bag.occurrencesOf(1));
		assertEquals(1, bag.occurrencesOf(2));
		assertEquals(2, bag.occurrencesOf(3));
		assertEquals(1, bag.occurrencesOf(4));
		assertEquals(1, bag.occurrencesOf(5));
	}

	@Test
	public void testRemoveInt_empty() throws Exception {
		final var bag = IntHashBag.empty();
		final boolean change = bag.remove(1);
		assertFalse(change);
	}

	@Test
	public void testRemoveInt_valueNotExisting() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3);
		final boolean change = bag.remove(4);
		assertFalse(change);
		assertEquals(3, bag.size());
		assertEquals(3, bag.sizeDistinct());
	}

	@Test
	public void testRemoveInt_reducingCount() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.remove(3);
		assertTrue(change);
		assertEquals(4, bag.size());
		assertEquals(3, bag.sizeDistinct());
	}

	@Test
	public void testRemoveInt_reducingCountToZero() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.remove(1);
		assertTrue(change);
		assertFalse(bag.contains(1));
		assertEquals(4, bag.size());
		assertEquals(2, bag.sizeDistinct());
	}

	@Test
	public void testRemoveCountableInt_empty() throws Exception {
		final var bag = IntHashBag.empty();
		final boolean change = bag.remove(CountableInt.ofValue(1).withCount(2));
		assertFalse(change);
	}

	@Test
	public void testRemoveCountableInt_valueNotExisting() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.remove(CountableInt.ofValue(4).withCount(2));
		assertFalse(change);
	}

	@Test
	public void testRemoveCountableInt_reducingCount() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.remove(CountableInt.ofValue(3).withCount(2));
		assertTrue(change);
		assertEquals(3, bag.size());
		assertEquals(3, bag.sizeDistinct());
		assertEquals(1, bag.occurrencesOf(1));
		assertEquals(1, bag.occurrencesOf(2));
		assertEquals(1, bag.occurrencesOf(3));
	}

	@Test
	public void testRemoveCountableInt_reducingCountToZero() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.remove(CountableInt.ofValue(3).withCount(3));
		assertTrue(change);
		assertEquals(2, bag.size());
		assertEquals(2, bag.sizeDistinct());
		assertEquals(1, bag.occurrencesOf(1));
		assertEquals(1, bag.occurrencesOf(2));
		assertEquals(0, bag.occurrencesOf(3));
		assertFalse(bag.contains(3));
	}

	@Test
	public void testRemoveCountableInt_removingMoreThanExist() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.remove(CountableInt.ofValue(3).withCount(4));
		assertTrue(change);
		assertEquals(2, bag.size());
		assertEquals(2, bag.sizeDistinct());
		assertEquals(1, bag.occurrencesOf(1));
		assertEquals(1, bag.occurrencesOf(2));
		assertEquals(0, bag.occurrencesOf(3));
		assertFalse(bag.contains(3));
	}

	@Test
	public void testRemoveCountableInt_removingZeroAmount() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.remove(CountableInt.ofValue(3).withCount(0));
		assertFalse(change);
		assertEquals(5, bag.size());
		assertEquals(3, bag.sizeDistinct());
		assertEquals(1, bag.occurrencesOf(1));
		assertEquals(1, bag.occurrencesOf(2));
		assertEquals(3, bag.occurrencesOf(3));
		assertTrue(bag.contains(3));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRemoveCountableInt_removingNegativeAmount() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		bag.remove(CountableInt.ofValue(3).withCount(-2));
	}

	@Test
	public void testRemoveAllOccurrences() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3);
		final boolean change = bag.removeAllOccurrences(2);
		assertTrue(change);
		assertEquals(2, bag.size());
		assertEquals(2, bag.sizeDistinct());
	}

	@Test
	public void testRemoveAllOccurrences_nonOccurring() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3);
		final boolean change = bag.removeAllOccurrences(4);
		assertFalse(change);
		assertEquals(4, bag.size());
		assertEquals(3, bag.sizeDistinct());
	}

	@Test
	public void testRemoveAllIntArray_empty() throws Exception {
		final var bag = IntHashBag.empty();
		final boolean change = bag.removeAll(1, 2, 3);
		assertFalse(change);

	}

	@Test
	public void testRemoveAllIntArray_noValues() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.removeAll(new int[0]);
		assertFalse(change);
		assertEquals(5, bag.size());
	}

	@Test
	public void testRemoveAllIntArray() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.removeAll(1, 2, 3);
		assertTrue(change);
		assertEquals(2, bag.size());
		assertEquals(1, bag.sizeDistinct());
		assertFalse(bag.contains(1));
		assertFalse(bag.contains(2));
		assertTrue(bag.contains(3));
	}

	@Test
	public void testRemoveAllCountableIntArray_emptyArray() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.removeAll(new CountableInt[0]);
		assertFalse(change);
		assertEquals(5, bag.size());
		assertEquals(3, bag.sizeDistinct());
	}

	@Test
	public void testRemoveAllCountableIntArray_zeroCount() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.removeAll(//
				CountableInt.ofValue(3).withCount(0), //
				CountableInt.ofValue(1).withCount(0)//
		);
		assertFalse(change);
		assertEquals(5, bag.size());
		assertEquals(3, bag.sizeDistinct());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRemoveAllCountableIntArray_negativeCount() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		bag.removeAll(//
				CountableInt.ofValue(3).withCount(-1) //
		);
	}

	@Test
	public void testRemoveAllCountableIntArray_valueDoesntExist() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.removeAll(//
				CountableInt.ofValue(4).withCount(10), //
				CountableInt.ofValue(0).withCount(3)//
		);
		assertFalse(change);
		assertEquals(5, bag.size());
		assertEquals(3, bag.sizeDistinct());
	}

	@Test
	public void testRemoveAllCountableIntArray_reduceCount() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.removeAll(//
				CountableInt.ofValue(3).withCount(2) //
		);
		assertTrue(change);
		assertEquals(3, bag.size());
		assertEquals(3, bag.sizeDistinct());
	}

	@Test
	public void testRemoveAllCountableIntArray_reduceCountToZero() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.removeAll(//
				CountableInt.ofValue(3).withCount(3) //
		);
		assertTrue(change);
		assertEquals(2, bag.size());
		assertEquals(2, bag.sizeDistinct());
	}

	@Test
	public void testRemoveAllCountableIntArray_takeOutMoreThanContained() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.removeAll(//
				CountableInt.ofValue(3).withCount(4) //
		);
		assertTrue(change);
		assertEquals(2, bag.size());
		assertEquals(2, bag.sizeDistinct());
	}

	@Test
	public void testRemoveAllCountableIntArray_longValue() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.removeAll(//
				CountableInt.ofValue(3).withCount(1L + Integer.MAX_VALUE) // should be the same as removing all
		);
		assertTrue(change);
		assertEquals(2, bag.size());
		assertEquals(2, bag.sizeDistinct());
	}

	@Test
	public void testRemoveAllCountableIntArray() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.removeAll(//
				CountableInt.ofValue(1).withCount(1), //
				CountableInt.ofValue(3).withCount(2) //
		);
		assertTrue(change);
		assertEquals(2, bag.size());
		assertEquals(2, bag.sizeDistinct());
		assertEquals(0, bag.occurrencesOf(1));
		assertEquals(1, bag.occurrencesOf(2));
		assertEquals(1, bag.occurrencesOf(3));
	}

	@Test
	public void testRemoveAllIntHashBag_removeEmptyBag() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.removeAll(IntHashBag.empty());
		assertFalse(change);
		assertEquals(5, bag.size());
		assertEquals(3, bag.sizeDistinct());
	}

	@Test
	public void testRemoveAllIntHashBag_removeBagThatIsLarger_sameEntries() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.removeAll(IntHashBag.of(1, 1, 2, 2, 3, 3, 3, 3, 3, 3));
		assertTrue(change);
		assertEquals(0, bag.size());
		assertTrue(bag.isEmpty());
	}

	@Test
	public void testRemoveAllIntHashBag_removeBagThatIsLarger_partiallyDifferentEntries() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.removeAll(IntHashBag.of(2, 4, 4, 4, 4, 4));
		assertTrue(change);
		assertEquals(4, bag.size());
		assertEquals(2, bag.sizeDistinct());
	}

	@Test
	public void testRemoveAllIntHashBag_entirelyDifferentEntries() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.removeAll(IntHashBag.of(4, 5, 6, 6));
		assertFalse(change);
		assertEquals(5, bag.size());
		assertEquals(3, bag.sizeDistinct());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSet_negativeAmount() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		bag.set(CountableInt.ofValue(2).withCount(-4));
	}

	@Test
	public void testSet_sameAmount() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.set(CountableInt.ofValue(3).withCount(3));
		assertFalse(change);
		assertEquals(5, bag.size());
		assertEquals(3, bag.sizeDistinct());
	}

	@Test
	public void testSet_valueDoesNotExist() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.set(CountableInt.ofValue(4).withCount(3)); // in this situation it's the same as an add
		assertTrue(change);
		assertEquals(8, bag.size());
		assertEquals(4, bag.sizeDistinct());
		assertEquals(3, bag.occurrencesOf(4));
	}

	@Test
	public void testSet_settingToZeroAmountOfExistingValueShouldReduceSizeAndRemoveIt() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.set(CountableInt.ofValue(3).withCount(0)); // in this situation it's the same as an add
		assertTrue(change);
		assertEquals(2, bag.size());
		assertEquals(2, bag.sizeDistinct());
		assertEquals(0, bag.occurrencesOf(3));
		assertFalse(bag.contains(3));
	}

	@Test
	public void testSet() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		final boolean change = bag.set(CountableInt.ofValue(2).withCount(4));
		assertTrue(change);
		assertEquals(8, bag.size());
		assertEquals(3, bag.sizeDistinct());
		assertEquals(4, bag.occurrencesOf(2));
	}

	@Test
	public void testClear() throws Exception {
		final var bag = IntHashBag.of(1, 2, 3, 3, 3);
		bag.clear();
		assertTrue(bag.isEmpty());
		assertEquals(0, bag.distinctValues().toArray().length);
	}

	@Test
	public void testDistinctValues() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 3);
		final var arr = bag.distinctValues().sorted().toArray();
		assertArrayEquals(new int[] { 1, 2, 3 }, arr);
	}

	@Test
	public void testValues() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 3);
		final var arr = bag.values().sorted().toArray();
		assertArrayEquals(new int[] { 1, 2, 2, 3, 3, 3 }, arr);
	}

	@Test
	public void testOccurrences() throws Exception {
		final var bag = IntHashBag.of(1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3);
		final var arr = bag.occurrences().sorted().toArray();
		assertArrayEquals(new CountableInt[] { //
				CountableInt.ofValue(1).withCount(2), //
				CountableInt.ofValue(2).withCount(4), //
				CountableInt.ofValue(3).withCount(6)//
		}, arr);
	}

	@Test
	public void testBottomOccurrences_empty() throws Exception {
		final var bag = IntHashBag.empty();
		assertTrue(bag.bottomOccurrences(2).isEmpty());
	}

	@Test
	public void testBottomOccurrences() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 3);
		final List<CountableInt> occurrences = bag.bottomOccurrences(2);
		assertEquals(List.of( //
				CountableInt.ofValue(1).withCount(1), //
				CountableInt.ofValue(2).withCount(2) //
		), occurrences);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBottomOccurrences_negativeValue() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 3);
		bag.bottomOccurrences(-2);
	}

	@Test
	public void testBottomOccurrences_zeroValue() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 3);
		assertTrue(bag.bottomOccurrences(0).isEmpty());
	}

	@Test
	public void testBottomOccurrences_moreThanBagValues() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 3);
		final List<CountableInt> occurrences = bag.bottomOccurrences(8);
		assertEquals(List.of( //
				CountableInt.ofValue(1).withCount(1), //
				CountableInt.ofValue(2).withCount(2), //
				CountableInt.ofValue(3).withCount(3) //
		), occurrences);
	}

	@Test
	public void testBottomOccurrences_sameAmountOfOccurrences() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 4, 4, 4); // 2 and 3 occur the same number of times
		final List<CountableInt> occurrences = bag.bottomOccurrences(2); // the bottom two are 1, 2 and 3 (2 and 3 share the rank).
		assertEquals(List.of( //
				CountableInt.ofValue(1).withCount(1), //
				CountableInt.ofValue(2).withCount(2), //
				CountableInt.ofValue(3).withCount(2) //
		), occurrences);
	}

	@Test
	public void testTopOccurrences() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 4, 4, 4); // 2 and 3 occur the same number of times
		final List<CountableInt> occurrences = bag.topOccurrences(2); // the top two are 4, 3 and 2 (3 and 2 share the rank).
		assertEquals(List.of( //
				CountableInt.ofValue(4).withCount(3), //
				CountableInt.ofValue(3).withCount(2), //
				CountableInt.ofValue(2).withCount(2) //
		), occurrences);
	}

	@Test
	public void testEquals_null() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 3);
		assertFalse(bag.equals(null));
	}

	@Test
	public void testEquals_sameObject() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 3);
		assertTrue(bag.equals(bag));
	}

	@Test
	public void testEquals_sameContent() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 3);
		final var bag2 = IntHashBag.of(1, 2, 2, 3, 3, 3);
		assertTrue(bag.equals(bag2));
		assertTrue("equals should be symmetric", bag2.equals(bag));
	}

	@Test
	public void testEquals_sameValuesDifferentCounts() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 3);
		final var bag2 = IntHashBag.of(1, 2, 2, 3, 3, 3, 3);
		assertFalse(bag.equals(bag2));
		assertFalse("equals should be symmetric", bag2.equals(bag));
	}

	@Test
	public void testEquals_extendedClass() throws Exception {
		final var bag = new IntHashBag();
		final var bag2 = new IntHashBag() {
			@SuppressWarnings("unused")
			boolean isPopulated() {
				return !isEmpty();
			}
		};
		assertFalse(bag.equals(bag2));
		assertFalse("equals should be symmetric", bag2.equals(bag));
	}

	@Test
	public void testEquals_differentCapacity() throws Exception {
		final var bag = new IntHashBag(64);
		bag.add(2);
		final var bag2 = new IntHashBag(512);
		bag2.add(2);
		assertTrue(bag.equals(bag2));
		assertTrue("equals should be symmetric", bag2.equals(bag));
	}

	@Test
	public void testHashCode_empty() throws Exception {
		final var bag = IntHashBag.empty();
		assertEquals(0, bag.hashCode());
		assertEquals("subsequent calls are expected to yield the same hashCode", 0, bag.hashCode());
	}

	@Test
	public void testHashCode_sameContent() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 3);
		final var bag2 = IntHashBag.of(1, 2, 2, 3, 3, 3);
		assertEquals(bag.hashCode(), bag2.hashCode());
	}

	@Test
	public void testHashCode_sameContent_differentCapacity() throws Exception {
		final var bag = new IntHashBag(64);
		bag.add(2);
		final var bag2 = new IntHashBag(512);
		bag2.add(2);
		assertEquals(bag.hashCode(), bag2.hashCode());
	}

	@Test
	public void testHashCode_differentContent() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 3);
		final var bag2 = IntHashBag.of(1, 2, 2, 3, 3, 4);
		assertNotEquals(bag.hashCode(), bag2.hashCode());
	}

	@Test
	public void testHashCode_willChangeOnAdd() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 3);
		final int hc1 = bag.hashCode();
		bag.add(2);
		final int hc2 = bag.hashCode();
		assertNotEquals(hc1, hc2);
	}

	@Test
	public void testHashCode_willChangeOnRemove() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 3);
		final int hc1 = bag.hashCode();
		bag.remove(2);
		final int hc2 = bag.hashCode();
		assertNotEquals(hc1, hc2);
	}

	@Test
	public void testHashCode_willChangeOnSetWithDifferentAmount() throws Exception {
		final var bag = IntHashBag.of(1, 2, 2, 3, 3, 3);
		final int hc1 = bag.hashCode();
		bag.set(CountableInt.ofValue(2).withCount(3));
		final int hc2 = bag.hashCode();
		assertNotEquals(hc1, hc2);
	}

}
