package de.plush.brix.c64graphics.core.model;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;
import static de.plush.brix.c64graphics.core.util.Utils.hiLoNibbleToByte;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.*;

import de.plush.brix.c64graphics.core.model.C64BitmapmodePictureMulticolorFli;

public class C64BitmapmodePictureMulticolorFliTest {

	@Before
	public void setUp() throws Exception {
		// intentionally left blank
	}

	public static byte[] createFliDesignerFbiCrewBinary() {
		final byte[] binary = new byte[0x4400];
		Arrays.fill(binary, 0x0000, 1000, WHITE.colorCode()); // cram
		Arrays.fill(binary, 0x0400 * 1, 0x0400 * 1 + 1000, hiLoNibbleToByte(GRAY.colorCode(), LIGHT_GRAY.colorCode())); // screen0
		Arrays.fill(binary, 0x0400 * 2, 0x0400 * 2 + 1000, hiLoNibbleToByte(BLUE.colorCode(), LIGHT_BLUE.colorCode())); // screen1
		Arrays.fill(binary, 0x0400 * 3, 0x0400 * 3 + 1000, hiLoNibbleToByte(GREEN.colorCode(), LIGHT_GREEN.colorCode())); // screen2
		Arrays.fill(binary, 0x0400 * 4, 0x0400 * 4 + 1000, hiLoNibbleToByte(RED.colorCode(), LIGHT_RED.colorCode())); // screen3
		Arrays.fill(binary, 0x0400 * 5, 0x0400 * 5 + 1000, BLACK.colorCode()); // screen4
		Arrays.fill(binary, 0x0400 * 6, 0x0400 * 6 + 1000, BLACK.colorCode()); // screen5
		Arrays.fill(binary, 0x0400 * 7, 0x0400 * 7 + 1000, BLACK.colorCode()); // screen6
		Arrays.fill(binary, 0x0400 * 8, 0x0400 * 8 + 1000, BLACK.colorCode()); // screen7

		Arrays.fill(binary, 0x2400, 0x2400 + 0x2000, (byte) 0b00_01_10_11); // Bitmap
		return binary;
	}

	@Test
	public void testFromFliDesignerFbiCrewBinary() throws Exception {
		final byte[] fbiCrewBinary = createFliDesignerFbiCrewBinary();
		final C64BitmapmodePictureMulticolorFli multicolorFLI = C64BitmapmodePictureMulticolorFli.fromFliDesignerFbiCrewBinary(fbiCrewBinary);
		assertEquals("cram", WHITE.colorCode(), multicolorFLI.colorCodeAt(0, 0));
		assertEquals("screen0", hiLoNibbleToByte(GRAY.colorCode(), LIGHT_GRAY.colorCode()), multicolorFLI.screenCodeAt(0, 0, 0));
		assertEquals("screen0", hiLoNibbleToByte(BLUE.colorCode(), LIGHT_BLUE.colorCode()), multicolorFLI.screenCodeAt(1, 0, 0));
		assertEquals("screen0", hiLoNibbleToByte(GREEN.colorCode(), LIGHT_GREEN.colorCode()), multicolorFLI.screenCodeAt(2, 0, 0));
		assertEquals("screen0", hiLoNibbleToByte(RED.colorCode(), LIGHT_RED.colorCode()), multicolorFLI.screenCodeAt(3, 0, 0));
		assertEquals("BitmapL0", (byte) 0b00_01_10_11, multicolorFLI.bitmapBlockAt(0, 0).bytes[0]);
		assertEquals("BitmapL1", (byte) 0b00_01_10_11, multicolorFLI.bitmapBlockAt(0, 0).bytes[1]);
		assertEquals("BitmapL2", (byte) 0b00_01_10_11, multicolorFLI.bitmapBlockAt(0, 0).bytes[2]);
		assertEquals("BitmapL3", (byte) 0b00_01_10_11, multicolorFLI.bitmapBlockAt(0, 0).bytes[3]);
		assertEquals("BitmapL4", (byte) 0b00_01_10_11, multicolorFLI.bitmapBlockAt(0, 0).bytes[4]);
		assertEquals("BitmapL5", (byte) 0b00_01_10_11, multicolorFLI.bitmapBlockAt(0, 0).bytes[5]);
		assertEquals("BitmapL6", (byte) 0b00_01_10_11, multicolorFLI.bitmapBlockAt(0, 0).bytes[6]);
		assertEquals("BitmapL7", (byte) 0b00_01_10_11, multicolorFLI.bitmapBlockAt(0, 0).bytes[7]);
	}

}
