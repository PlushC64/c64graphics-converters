package de.plush.brix.c64graphics.core.pipeline.stages.compression;

import static de.plush.brix.c64graphics.core.util.Utils.toHexString;
import static org.junit.Assert.assertEquals;

import org.junit.*;

import de.plush.brix.c64graphics.core.pipeline.stages.compression.RLEpacker2;

public class RLEpacker2Test {

	private RLEpacker2 packer;

	@Before
	public void setUp() throws Exception {
		packer = new RLEpacker2();
	}

	@Test
	public void testApplySimpleExample() throws Exception {
		final var raw = new byte[] { //
				(byte) 1, (byte) 1, //
				(byte) 2, //
				(byte) 3, (byte) 4, (byte) 5 //
		};

		final var expected = new byte[] { //
				(byte) 0x01, (byte) 1, // unfold 2x "1"
				(byte) 0x83, (byte) 2, (byte) 3, (byte) 4, (byte) 5, // literal 3x "2, 3,4,5"
				(byte) 0 // endmark
		};

		// final var expected = new byte[] { //
		// (byte) 0x01, (byte) 1, // unfold 2x "1"
		// (byte) 0x81, (byte) 2, // literal 1x "2"
		// (byte) 0x82, (byte) 3, (byte) 4, (byte) 5, // literal 2x "3,4,5"
		// (byte) 0 //endmark
		// };
		final var packed = packer.apply(raw);
		assertEquals(toHexString(expected), toHexString(packed));
	}

	@Test
	public void testApplySimpleExample2() throws Exception {
		final var raw = new byte[] { //
				(byte) 1, //
				(byte) 2, (byte) 2, //
				(byte) 3, (byte) 3, (byte) 3, //
				(byte) 4, (byte) 4, (byte) 4, (byte) 4, //
		};

		final var expected = new byte[] { //
				(byte) 0x80, (byte) 1, // literal 1x "1"
				(byte) 0x01, (byte) 2, // unfold 2x "2"
				(byte) 0x02, (byte) 3, // unfold 3x "3"
				(byte) 0x03, (byte) 4, // unfold 4x "4"
				(byte) 0 // endmark
		};
		final var packed = packer.apply(raw);
		assertEquals(toHexString(expected), toHexString(packed));
	}

	@Test
	public void testEmptyInput() throws Exception {
		final var raw = new byte[] { //
		};

		final var expected = new byte[] { //
				(byte) 0 // endmark
		};
		final var packed = packer.apply(raw);
		assertEquals(toHexString(expected), toHexString(packed));
	}

	@Test
	public void testEndmarkAsOnlyByte() throws Exception {
		final var raw = new byte[] { //
				(byte) 0 // endmark
		};

		final var expected = new byte[] { //
				(byte) 0x80, (byte) 0, // literal 1x "0"
				(byte) 0 // endmark
		};
		final var packed = packer.apply(raw);
		assertEquals(toHexString(expected), toHexString(packed));
	}

	@Test
	public void testEndmarkAsOnlyByteInMinimumrunLength() throws Exception {
		final var raw = new byte[] { //
				(byte) 0, (byte) 0 //
		};

		final var expected = new byte[] { //
				(byte) 0x01, (byte) 0, // unfild 2x "0"
				(byte) 0 // endmark
		};
		final var packed = packer.apply(raw);
		assertEquals(toHexString(expected), toHexString(packed));
	}
}
