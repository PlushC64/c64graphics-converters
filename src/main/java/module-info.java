
module de.plush.brix.c64graphics.core {
	exports de.plush.brix.c64graphics.core;

	exports de.plush.brix.c64graphics.core.model;

	exports de.plush.brix.c64graphics.core.pipeline.stages.conversion;

	exports de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

	exports de.plush.brix.c64graphics.core.pipeline;

	exports de.plush.brix.c64graphics.core.pipeline.stages.colors;

	exports de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces;

	exports de.plush.brix.c64graphics.core.util.collections;

	exports de.plush.brix.c64graphics.core.pipeline.stages.dithering;

	exports de.plush.brix.c64graphics.core.post;

	exports de.plush.brix.c64graphics.core.util.charpixeldistance;

	exports de.plush.brix.c64graphics.core.pipeline.displays;

	exports de.plush.brix.c64graphics.core.util;

	exports de.plush.brix.c64graphics.core.emitters;

	exports de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes;

	exports de.plush.brix.c64graphics.core.pipeline.stages.misc;

	exports de.plush.brix.c64graphics.core.util.functions;

	exports de.plush.brix.c64graphics.core.pipeline.stages;

	exports de.plush.brix.c64graphics.core.model.generators;

	exports de.plush.brix.c64graphics.core._experimental.wavelet;

	exports de.plush.brix.c64graphics.core.pipeline.stages.compression;

	opens de.plush.brix.c64graphics.core.model to com.jsoniter;

	requires transitive java.desktop; // also needed to use this lib

	requires transitive java.xml; // used in / used in stages.conversion.ImageToSvgString and SvgStringToImage, and stages.manipulation XmlDocumentAccess

	requires com.jsoniter; // used internally in core.model.PETSCII

	requires net.sourceforge.argparse4j; // used in stages.compression.tinycrunch

	requires svg.salamander; // used in stages.conversion.ImageToSvgString and SvgStringToImage

	requires jankovicsandras.imagetracer; // used in stages.conversion.ImageToSvgString

	requires static com.github.spotbugs.annotations; // not optional, as spotbugs needs it at runtime on the compiled artifact, which sucks

	requires static jsr305; // optional, only used for compilation, not at runtime

}
