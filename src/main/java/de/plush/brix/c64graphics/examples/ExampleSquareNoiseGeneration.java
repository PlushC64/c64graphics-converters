package de.plush.brix.c64graphics.examples;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.function.IntUnaryOperator;

import de.plush.brix.c64graphics.core.emitters.SquareNoiseEmitter;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;

public class ExampleSquareNoiseGeneration {

	public static void main(final String[] args) {
		final IntUnaryOperator noiseGeneratorSeed = (frame) -> frame;
		final IntUnaryOperator rectangleNumber = (frame) -> 250;

		// final var rgb = Math.abs(noiseGenerator.eval(Math.sin(x / 10.0), Math.sin(y / 10.0), 12)) > 0.1 ? backgroundColor.getRGB()
		// : pixelColor.getRGB();

		final SquareNoiseEmitter emitter = new SquareNoiseEmitter(4, new Dimension(320, 200), noiseGeneratorSeed, rectangleNumber, new Dimension(5, 5),
				new Dimension(20, 20), Color.CYAN.brighter(), Color.GREEN) {
			@Override
			protected void stop(final Timer timer) {
				// nonstop
			}
		};
		@SuppressWarnings("resource")
		final ImageDisplay<BufferedImage> display = new ImageDisplay<>(3);
		emitter.add(display);
		emitter.run();
	}

}
