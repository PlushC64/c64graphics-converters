package de.plush.brix.c64graphics.examples;

import static java.lang.Math.*;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import de.plush.brix.c64graphics.core._experimental.wavelet.HaarWaveletImage;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;

/** Main class used for starting the conversion. */
public class ExampleHaarWavelet {

	@SuppressWarnings("resource")
	public static void main(final String[] args) throws URISyntaxException, IOException {

		final var picName = "paintface.jpg";
		final var uri = ExampleHaarWavelet.class.getResource("resources").toURI();
		final var imageLocation = Paths.get(uri).resolve(picName).toUri().toURL();

		final BufferedImage image = ImageIO.read(imageLocation);
		final var waveletImage1 = new HaarWaveletImage(image);
		final var waveletImage2 = new HaarWaveletImage(image);
		final var waveletImage3 = new HaarWaveletImage(image);
		final var waveletImage4 = new HaarWaveletImage(image);
		// final var waveletImage5 = new HaarWaveletImage(image);

		// final var wavelets = waveletImage.imageInterpretationOfWavelets(ColorChannel.Green);

		final double variance = 10;
		// waveletImage.forEachCoefficient(level -> level < 4, (level, coeff) -> coeff < -variance || variance < coeff ? coeff : 0.0);
		waveletImage1.forEachCoefficient(level -> true, (level, coeff) -> coeff < -variance || variance < coeff ? coeff : 0.0);
		waveletImage2.forEachCoefficient(level -> level < 4, (level, coeff) -> max(-pow(2, level), min(-pow(2, level), coeff)));
		waveletImage3.forEachCoefficient(level -> level < 6, (level, coeff) -> coeff / (8 - level));
		waveletImage4.forEachCoefficient(level -> level < 1, (level, coeff) -> coeff *= 1.8);

		// final double variance2 = 5;
		// waveletImage5.forEachCoefficient(level -> level < 2, (level, coeff) -> coeff < -variance2 || variance2 < coeff ? coeff : 0.0);
		// waveletImage5.forEachCoefficient(ColorChannel.Blue, level -> level < 3, (level, coeff) -> clamp(-0x10, coeff, 0x10));

		new ImageDisplay<>(2f).show(image, "original");
		// new ImageDisplay<>(2f).show(wavelets, "original wavelets, green channel");
		new ImageDisplay<>(2f).show(waveletImage1.toImage(), "modified1");
		new ImageDisplay<>(2f).show(waveletImage2.toImage(), "modified2");
		new ImageDisplay<>(2f).show(waveletImage3.toImage(), "modified3");
		new ImageDisplay<>(2f).show(waveletImage4.toImage(), "modified4");
		// new ImageDisplay<>(2f).show(waveletImage5.toImage(), "modified5");

	}

	// private static double clamp(final double min, final double value, final double max) {
	// return min(max, max(min, value));
	// }

}
