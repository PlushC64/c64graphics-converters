package de.plush.brix.c64graphics.examples;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.*;
import java.util.concurrent.TimeUnit;
import java.util.function.*;

import de.plush.brix.c64graphics.core.emitters.StarAnimationEmitter;
import de.plush.brix.c64graphics.core.model.C64Sprite;
import de.plush.brix.c64graphics.core.pipeline.PipelineConsumer;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.ColorBlender.Detect;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.Wait;
import de.plush.brix.c64graphics.core.post.ImagesToSpriteAnimation;

/** Main class used for starting the conversion. */
public class ExampleStarAnimationToSpritesHires {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	public static void main(final String[] args) throws IOException {
		Files.createDirectories(Paths.get(outDir));

		final Supplier<Color> backgroundColor = () -> Color.WHITE;
		final Color pixelColor = Color.BLACK;

		// creating a rather big star and have the scaling sort out smoothing the edges for a small sprite.
		final int starBranches = 5;
		final int starFrames = 5;
		final var generatedStarSize = new Dimension(C64Sprite.WIDTH_PIXELS * 10, C64Sprite.HEIGHT_PIXELS * 10);

		final var c64SpriteMatrix = new Dimension(1, 1); // 1x1 sprite grid (single sprite as target)
		final var targetSize = new Dimension(c64SpriteMatrix.width * C64Sprite.WIDTH_PIXELS, c64SpriteMatrix.height * C64Sprite.HEIGHT_PIXELS);

		@SuppressWarnings("resource")
		final Function<BufferedImage, BufferedImage> pipeline = //
				new ImageDisplay<BufferedImage>(2, "original")//
						.andThen(new Rescale(targetSize, backgroundColor))//
						.andThen(new ColorBlender(backgroundColor, pixelColor, Detect.PixelColor))// now we have two colors
						.andThen(new ImageDisplay<BufferedImage>(2, "scaled", Grid.sizeC64Sprite))//
						.andThen(new Wait<>(500, TimeUnit.MILLISECONDS))
		//
		;
		final File fileOut = new File(outDir + "starSprites.bin");
		try (final var emitter = new StarAnimationEmitter(starFrames, generatedStarSize, starBranches, backgroundColor.get(), pixelColor);
				final var imagesToSpriteAnimation = new PipelineConsumer<>(pipeline, //
						new ImagesToSpriteAnimation(c64SpriteMatrix, pixelColor, fileOut))//
		) {
			emitter.add(imagesToSpriteAnimation);
			emitter.run();
		}
	}
}
