package de.plush.brix.c64graphics.examples;

import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.function.Function;

import javax.imageio.ImageIO;

import de.plush.brix.c64graphics.core.model.C64CharsetScreen;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.ImageToC64BitmapmodePictureHires;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.HiEddiColorClashResolver;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.FindMostCommonColor.Calculate;

/** Main class used for starting the conversion. */
public class ExampleImageToHires {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	public static void main(final String[] args) throws URISyntaxException, IOException {

		// final String picName = "Geisha-by-Luca-Tarlazzi-513x640.jpg";
		final String picName = "geisha-lovely-woman-face-1.jpg";
		final URI uri = ExampleImageToHires.class.getResource("resources").toURI();
		final File imageFile = Paths.get(uri).resolve(picName).toFile();
		Files.createDirectories(Paths.get(outDir));

		final Palette palette = C64ColorsColodore.PALETTE;
		final ColorDistance colorDistanceMeasure = ColorDistance.EUCLIDEAN_WEIGHTED;
		final Dithering dithering = Dithering.Atkinson;

		final FindMostCommonColor backgroundProviderPC = new FindMostCommonColor(Calculate.FIRST_FRAME_AND_REUSE);

		@SuppressWarnings("resource")
		final Function<BufferedImage, ?> pipeline = backgroundProviderPC// get most common color
				.andThen(new Rescale(C64CharsetScreen.sizePixels(), backgroundProviderPC))// using color finder to fill frame when resizing
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, dithering))// we now have c64 colors in hires resolution
				.andThen(new ImageToC64BitmapmodePictureHires(palette, new HiEddiColorClashResolver(colorDistanceMeasure).andThen(new ImageDisplay<>(4f,
						Grid.sizeC64Char))//
				)).andThen(pic -> pic.binaryProviderHiEddi())// some C64 models aren't binary providers themselves, but provide several - we need to pick one
				.andThen(new WriteC64BinaryToFile<>(n -> 0x2000, n -> outDir + picName.replaceAll("\\.jpg", "") + ".hed"))//
		;

		pipeline.apply(ImageIO.read(imageFile));
	}

}
