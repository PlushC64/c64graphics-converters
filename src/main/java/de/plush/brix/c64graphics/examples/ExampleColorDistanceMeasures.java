package de.plush.brix.c64graphics.examples;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.swing.*;

import de.plush.brix.c64graphics.core.model.C64Bitmap;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.FindMostCommonColor;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.FindMostCommonColor.Calculate;

public class ExampleColorDistanceMeasures {

	public static void main(final String[] args) throws URISyntaxException, IOException {
		final String picName = "paintface.jpg";
		final URI uri = ExampleColorDistanceMeasures.class.getResource("resources").toURI();
		final File imageFile = Paths.get(uri).resolve(picName).toFile();

		show(imageFile, C64ColorsColodore.PALETTE, Dithering.BackslashMulticolor, ColorDistance.values());

	}

	private static void show(final File imageFile, final Palette palette, final Dithering dithering, final ColorDistanceMeasure[] distanceMeasures)
			throws IOException {
		final var originalImage = ImageIO.read(imageFile);
		final var measureToImage = Arrays.stream(distanceMeasures).parallel().collect(Collectors.toMap(//
				measure -> measure, //
				measure -> withKoalaPainterLimits(originalImage, palette, measure, dithering), //
				(x, y) -> y, //
				LinkedHashMap::new//
		));
		EventQueue.invokeLater(() -> {
			final JFrame frame = new JFrame("overview");
			frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			frame.getContentPane().setLayout(new GridLayout(0, 4));
			frame.add(panel("Original", new Rescale(C64Bitmap.sizePixels(), () -> Color.black).apply(originalImage)));
			measureToImage.forEach((measure, image) -> frame.add(panel(ColorDistance.name(measure), image)));
			frame.pack();
			frame.setVisible(true);
		});

	}

	private static JPanel panel(final String headline, final BufferedImage image) {
		if (!EventQueue.isDispatchThread()) { throw new IllegalCallerException("Caller did not call this method on the Event Queue"); }
		final var label = new JLabel(headline);
		label.setFont(new Font("Dialog", Font.PLAIN, 18));
		final var imageLabel = new JLabel(new ImageIcon(image));
		final var panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(label, BorderLayout.NORTH);
		panel.add(imageLabel, BorderLayout.CENTER);
		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		return panel;
	}

	private static BufferedImage withKoalaPainterLimits(final BufferedImage original, final Palette palette, final ColorDistanceMeasure colorDistanceMeasure,
			final Dithering dithering) {
		final FindMostCommonColor backgroundProviderC64 = new FindMostCommonColor(Calculate.FIRST_FRAME_AND_REUSE);
		final Supplier<Color> backgroundProviderPC = () -> Color.BLACK;

		final Function<BufferedImage, BufferedImage> pipeline = new Rescale(C64Bitmap.sizePixels(), backgroundProviderPC) //
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, dithering))// we now have c64 colors in hires resolution
				.andThen(new Pixelize(new Dimension(2, 1)))// pixelization will create non-c64 colors
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, Dithering.None))// We can't use dithering here as we would get hires pixels
				.andThen(backgroundProviderC64)// find the C64-palette color for the background
				.andThen(new ImageToC64BitmapmodePictureMulticolor(palette, backgroundProviderC64, colorDistanceMeasure)) //
				.andThen(new C64BitmapmodePictureMulticolorToImage(palette));

		return pipeline.apply(original);
	}

}
