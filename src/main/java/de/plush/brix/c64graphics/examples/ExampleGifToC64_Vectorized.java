package de.plush.brix.c64graphics.examples;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;
import static de.plush.brix.c64graphics.core.util.Utils.*;
import static java.util.stream.Collectors.toList;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URI;
import java.nio.file.*;
import java.util.*;
import java.util.List;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.IntStream;

import org.w3c.dom.*;

import de.plush.brix.c64graphics.core.emitters.GifEmitter;
import de.plush.brix.c64graphics.core.model.C64Sprite;
import de.plush.brix.c64graphics.core.pipeline.PipelineConsumer;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.ColorBlender.Detect;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.FindMostCommonColor.Calculate;
import de.plush.brix.c64graphics.core.util.Utils;

/** Main class used for starting the conversion. */
public class ExampleGifToC64_Vectorized {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	public static void main(final String[] args) throws Exception {
		final String picName = "backflip.gif";
		final Insets insets = new Insets(14, 24, 28, 26);

		final Color defaultBackgroundColor = WHITE.color;
		// final String picName = "breaker1.gif";
		// final Insets insets = new Insets(0, 0, 0, 0);

		// final String picName = "gymnastics.gif";
		// final Insets insets = new Insets(20, 20, 24, 10);

		final URI uri = ExampleGifToC64_Vectorized.class.getResource("resources").toURI();
		final File imageFile = Paths.get(uri).resolve(picName).toFile();
		Files.createDirectories(Paths.get(outDir));

		final Palette palette = () -> new Color[] { BLACK.color, WHITE.color };
		final ColorDistance colorDistanceMeasure = ColorDistance.EUCLIDEAN;
		final Dithering dithering = Dithering.None;

		final FindMostCommonColor backgroundProviderPC = new FindMostCommonColor(Calculate.FIRST_FRAME_AND_REUSE);

		@SuppressWarnings("resource")
		final Function<BufferedImage, byte[]> pipeline = backgroundProviderPC// get most common color
				.andThen(new ColorBlender(() -> WHITE.color, BLACK.color, Detect.PixelColor, 50d))//
				.andThen(new Border(() -> new Color(0xff, 0, 0, 0x80), insets)) //
				.andThen(new ImageDisplay<>(1f, "original with cut marks", Grid.Invisible))//
				.andThen(new ImageCrop(insets)) //
				.andThen(new Rescale(new Dimension(4 * C64Sprite.WIDTH_PIXELS, 4 * C64Sprite.HEIGHT_PIXELS - 1), () -> Color.WHITE))// using color finder
																																	// to fill
																																	// frame when resizing
				.andThen(new ImageDisplay<>(4f, "scaled", Grid.sizeC64Sprite))//
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, dithering)) //
				// .andThen(new ImageDisplay<>(4f, "palette applied", Grid.sizeC64Sprite))//
				.andThen(new ImageToSvgString()//
						.withNumberOfColors(2)//
						.withErrorTresholdForQuadraticLines(0.0001f)//
						.withCoordinatesRoundedToDecimalPlace(0)//
						.withEdgeNodePathsShorterThanThisDiscardedForNoiseReduction(50)// 50
						.withSvgDesciptionsEnabled(false)//
				) //
				.andThen(new XmlDocumentAccess((doc) -> {
					final Node rootNode = doc.getFirstChild();
					final NodeList list = rootNode.getChildNodes();
					IntStream.range(0, list.getLength()) //
							.mapToObj(list::item) //
							.filter(item -> "path".equalsIgnoreCase(item.getNodeName()))//
							.filter(item -> item.getAttributes().getNamedItem("fill") != null) //
							.forEach(item -> item.getAttributes().getNamedItem("fill").setTextContent("rgb(255,255,255)")) //
					;
				})).andThen(new Function<String, String>() {
					final Function<String, ?> display = new SvgStringToImage()//
							.andThen(new ImageDisplay<>(8f, "back fromSVG", Grid.sizeC64Sprite));

					@Override
					public String apply(final String frame) {
						display.apply(frame);
						return frame;
					}
				}) //
				.andThen(new StringToXmlDocument()) //
				.andThen(doc -> {
					final Color color = BLACK.color;
					final List<List<Point>> lines = pathsOfStrokeColor(doc, color, colorDistanceMeasure);
					lines.get(lines.size() - 1).add(new Point(0xFF, 0xFF)); // add frame end mark
					final List<Point> line = lines.get(0);
					// if (lines.size() > 1) { System.out.println("WAAAH!" + lines.size()); System.exit(1); }

					// final var str = line.stream().map(p -> Integer.toString(p.x, 10) + ", " + Integer.toString(p.y, 10))
					// .collect(Collectors.joining(", ", "\t.byte ", ""));
					// System.out.println(str);
					return toByteArray(line.stream(), point -> new byte[] { toUnsignedByte(point.x), toUnsignedByte(point.y) });
				}) //
					// .andThen(new Wait<>(1000, TimeUnit.MILLISECONDS))///
		;

		final Consumer<List<byte[]>> postProcessor = (frames) -> {
			final Function<byte[], ?> fileWriter = new BinaryToC64BinaryProvider().andThen(new WriteC64BinaryToFile<>(frameNr -> outDir + picName
					+ ".xycoords.bin"));//

			final byte[] lastFrame = frames.get(frames.size() - 1); // set animation end mark
			Arrays.fill(lastFrame, lastFrame.length - 2, lastFrame.length, toUnsignedByte(0xFE));

			final byte[] allFrames = Utils.toByteArray(frames.stream());
			fileWriter.apply(allFrames);
		};

		try (final var emitter = new GifEmitter(imageFile, defaultBackgroundColor); //
				final var consumer = new PipelineConsumer<>(pipeline, postProcessor)) {
			emitter.add(consumer);
			emitter.run();
		}
		System.exit(0);
	}

	private static List<List<Point>> pathsOfStrokeColor(final Document doc, final Color color, final ColorDistance colorDistanceMeasure) {
		final Pattern coordinatePattern = Pattern.compile("(?:\\s*[ML]\\s*)(?<x>[0-9]+\\.[0-9]+)\\s*(?<y>[0-9]+\\.[0-9]+)");
		final Node rootNode = doc.getFirstChild();
		final NodeList list = rootNode.getChildNodes();
		return IntStream.range(0, list.getLength()) //
				.mapToObj(list::item) //
				.filter(item -> "path".equalsIgnoreCase(item.getNodeName()))//
				.filter(item -> item.getAttributes().getNamedItem("stroke") != null) //
				.filter(item -> isPathOfColor(item, color, colorDistanceMeasure)) //
				.map(item -> item.getAttributes().getNamedItem("d").getTextContent()) //
				.map(pathDefinition -> pointsFrom(pathDefinition, coordinatePattern))//
				.collect(toList());
	}

	private static ArrayList<Point> pointsFrom(final String pathDefinition, final Pattern coordinatePattern) {
		final Matcher m = coordinatePattern.matcher(pathDefinition);
		final var points = new ArrayList<Point>();
		while (m.find()) {
			final int x = (int) Float.parseFloat(m.group("x")); // math.round may give a value greater max
			final int y = (int) Float.parseFloat(m.group("y"));
			points.add(new Point(x, y));
		}

		return points;
	}

	private static boolean isPathOfColor(final Node pathItem, final Color targetColor, final ColorDistance colorDistanceMeasure) {
		final String strokeDef = pathItem.getAttributes().getNamedItem("stroke").getTextContent();
		if (strokeDef.toLowerCase().startsWith("rgb")) {
			final String[] rgb = strokeDef.substring(4).split("[(,)]");
			final int[] rgbInts = Arrays.stream(rgb).mapToInt(Integer::parseInt).toArray();
			final Color color = new Color(rgbInts[0], rgbInts[1], rgbInts[2]);
			final double distance = colorDistanceMeasure.distanceOf(targetColor, color);
			return distance < 0.2;
		}
		return false;
	}

}
