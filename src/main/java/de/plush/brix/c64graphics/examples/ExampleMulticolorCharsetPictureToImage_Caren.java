package de.plush.brix.c64graphics.examples;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;

import java.awt.Rectangle;
import java.net.*;
import java.nio.file.*;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.displays.*;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.CharmodeColorClashResolverMulticolor;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.Mask;
import de.plush.brix.c64graphics.core.util.*;

public class ExampleMulticolorCharsetPictureToImage_Caren {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	private static URI file(final String fileName) throws URISyntaxException {
		final URI dataDir = ExampleMulticolorCharsetPictureToImage_Caren.class.getResource("resources").toURI();
		return Paths.get(dataDir).resolve(fileName).toFile().toURI();
	}

	public static void main(final String[] args) throws Exception {
		Files.createDirectories(Paths.get(outDir));

		final C64Charset charset = C64Charset.loadCharset(file("caren_char.dat"), StartAddress.InFile);
		final C64Screen screen = C64Screen.loadScreen(file("caren_vmem.dat"), StartAddress.InFile);
		final C64ColorRam colorRam = C64ColorRam.loadColorRam(file("caren_d800.dat"), StartAddress.InFile);
		final C64CharsetScreenColorRam cs = new C64CharsetScreenColorRam(charset, screen, colorRam);

		final Palette palette = C64ColorsColodore.PALETTE;
		final ColorDistance colorDistanceMeasure = ColorDistance.EUCLIDEAN;

		@SuppressWarnings("resource")
		final Function<C64CharsetScreenColorRam, ?> pipeline //
				= new C64CharmodePictureToImageMulticolor(palette)//
						.withForcedBackgroundColor(BLUE.color)//
						.withForcedMulticolor1D022(LIGHT_RED.color)//
						.withForcedMulticolor2D023(YELLOW.color)//
						.andThen(new ImageDisplay<>(2.0f, "1. Image from raw char, screen and colorram data", Grid.Invisible))//
						.andThen(new Mask(C64ColorsColodore.BLACK.color, // mask lower 8 lines of (garbage)
								new Rectangle(//
										0, //
										(C64Bitmap.HEIGHT_BLOCKS - 8) * C64Char.HEIGHT_PIXELS, //
										C64Bitmap.WIDTH_PIXELS, //
										8 * C64Char.HEIGHT_PIXELS)//
						))//
						.andThen(new ImageDisplay<>(2.0f, "2. Image with mask applied", Grid.Invisible))//
						.andThen(new ImageToC64CharmodePictureMulticolor(palette, colorDistanceMeasure, CharacterReduction.UsingImageColorsOnly, //
								new CharmodeColorClashResolverMulticolor(palette, colorDistanceMeasure)//
										.andThen(new ImageDisplay<>(2.0f, "3. Removed clashed before reconverting", Grid.Invisible))//
						))//
						.andThen(new Display<>(new C64CharmodePictureToImageMulticolor(palette), //
								new ImageDisplay<>(2.0f, "4. Reconverted masked image to char, screen and colorram", Grid.Invisible)))//
						.andThen(csc -> {
							Utils.writeFile(outDir + "caren_reconverted_char.dat", csc.charsetBinary(), 0x2000);
							Utils.writeFile(outDir + "caren_reconverted_vmem.dat", csc.screenBinary(), 0x2800);
							Utils.writeFile(outDir + "caren_reconverted_d800.dat", csc.colorRamBinary(), 0x2c00);
							return csc;
						})
		//
		;

		pipeline.apply(cs);

		System.out.println("ok");

	}

}
