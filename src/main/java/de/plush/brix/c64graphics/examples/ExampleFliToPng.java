package de.plush.brix.c64graphics.examples;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.*;
import java.nio.file.*;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.model.C64BitmapmodePictureMulticolorFli;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.C64BitmapmodePictureMulticolorFliToImage;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.WriteImageToFile;
import de.plush.brix.c64graphics.core.util.StartAddress;
import de.plush.brix.c64graphics.core.util.UtilsImage.ImageFormat;

/** Main class used for starting the conversion. */
public class ExampleFliToPng {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	public static void main(final String[] args) throws URISyntaxException, IOException {

		final String picName = "blackmail.fli";
		final URI uri = ExampleFliToPng.class.getResource("resources").toURI();
		final URI imageLocation = Paths.get(uri).resolve(picName).toUri();

		final var outDirPath = Paths.get(outDir);
		if (!Files.exists(outDirPath)) {
			Files.createDirectory(outDirPath);
		}
		@SuppressWarnings("resource")
		final Function<C64BitmapmodePictureMulticolorFli, BufferedImage> pipeline = //
				new C64BitmapmodePictureMulticolorFliToImage(C64ColorsColodore.PALETTE)//
						.andThen(new WriteImageToFile<>(ImageFormat.PNG, n -> outDir + picName.replaceAll("\\.fli", "") + "fbi.png"))//
						.andThen(new ImageDisplay<>(4, "as png", Grid.sizeC64Char));

		pipeline.apply(C64BitmapmodePictureMulticolorFli.loadFliGraphBlackmail(imageLocation, StartAddress.InFile));
	}

}
