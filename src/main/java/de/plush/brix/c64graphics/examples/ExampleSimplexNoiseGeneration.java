package de.plush.brix.c64graphics.examples;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.function.*;

import de.plush.brix.c64graphics.core.emitters.SimplexNoiseEmitter;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;

public class ExampleSimplexNoiseGeneration {

	public static void main(final String[] args) {
		// final DoubleFunction<Color> valueToColor = (value) -> Math.abs(value) > 0.06 ? Color.WHITE : Color.BLACK;
//		final DoubleFunction<Color> valueToColor = (value) -> value >= 0 ? Color.WHITE : Color.BLACK;
final DoubleFunction<Color> valueToColor = (value) -> value > 0.45 ? Color.WHITE : Color.BLACK;
//		// @formatter:off
//		final DoubleFunction<Color> valueToColor = (value) -> new Color(
//				(int) (Math.abs(value) * 100d),
//				(int) (Math.abs(value) * 100d),
//				(int) (Math.abs(value) * 255d)
//		).brighter().brighter();
//		 // @formatter:on
		final IntUnaryOperator noiseGeneratorSeed = (frame) -> 0;
		final ToDoubleBiFunction<Point, Integer> scaleX = (pos, frame) -> 0.04 * pos.x;
		final ToDoubleBiFunction<Point, Integer> scaleY = (pos, frame) -> 0.04 * pos.y;
		final ToDoubleBiFunction<Point, Integer> scaleZ = (pos, frame) -> 0.25 * frame;

		// final var rgb = Math.abs(noiseGenerator.eval(Math.sin(x / 10.0), Math.sin(y / 10.0), 12)) > 0.1 ? backgroundColor.getRGB()
		// : pixelColor.getRGB();

		try (final SimplexNoiseEmitter emitter = new SimplexNoiseEmitter(4, new Dimension(320, 200), valueToColor, noiseGeneratorSeed, scaleX, scaleY, scaleZ) {
			@Override
			protected void stop(final Timer timer) {
				// nonstop
			}
		}) {
			@SuppressWarnings("resource")
			final ImageDisplay<BufferedImage> display = new ImageDisplay<>(3);
			emitter.add(display);
			emitter.run();
		}
	}

}
