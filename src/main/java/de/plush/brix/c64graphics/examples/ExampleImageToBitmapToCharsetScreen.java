package de.plush.brix.c64graphics.examples;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.function.Function;

import javax.imageio.ImageIO;

import de.plush.brix.c64graphics.core.pipeline.displays.*;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.PaletteConversion;

/** Main class used for starting the conversion. */
public class ExampleImageToBitmapToCharsetScreen {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	public static void main(final String[] args) throws URISyntaxException, IOException {

		final String picName = "WOODY.png";
		final URI uri = ExampleImageToBitmapToCharsetScreen.class.getResource("resources").toURI();
		final File imageFile = Paths.get(uri).resolve(picName).toFile();
		Files.createDirectories(Paths.get(outDir));

		final Palette palette = () -> new Color[] { BLACK.color, BLUE.color, LIGHT_BLUE.color, LIGHT_GRAY.color };
		final var colorDistanceMeasure = ColorDistance.EUCLIDEAN;
		final var dithering = Dithering.None;
		final var grid = Grid.Invisible;
		@SuppressWarnings("resource")
		final Function<BufferedImage, ?> pipeline = //
				new ImageDisplay<BufferedImage>(2f, "original") //
						.andThen(new PaletteConversion(palette, colorDistanceMeasure, dithering))// we now have c64 colors in hires resolution
						.andThen(new ImageToC64BitmapMulticolor(BLACK, BLUE.color, LIGHT_BLUE.color, LIGHT_GRAY.color))//
						.andThen(new Display<>(new C64BitmapToImageMulticolor(C64ColorsColodore.PALETTE, BLACK, DARK_GRAY.color, GRAY.color, LIGHT_GRAY.color),
								new ImageDisplay<>(2f, "bitmap", grid))) //
						.andThen(new C64BitmapToC64CharsetScreenMulticolor(colorDistanceMeasure))//
						.andThen(new Display<>(new C64CharsetScreenToImageMulticolor(), new ImageDisplay<>(2f, "charset+screen", grid))) //
		;

		pipeline.apply(ImageIO.read(imageFile));
	}

}
