package de.plush.brix.c64graphics.examples;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;
import static de.plush.brix.c64graphics.core.util.Utils.*;
import static de.plush.brix.c64graphics.core.util.UtilsImage.loadImageResource;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.*;
import java.time.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.emitters.ImageBlendingEmitter;
import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.PipelineConsumer;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.CharmodeColorClashResolverMulticolor;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.*;
import de.plush.brix.c64graphics.core.util.*;
import de.plush.brix.c64graphics.core.util.UtilsImage.ImageFormat;

/** Main class used for starting the conversion. */
public class ExamplePngBlendingToKoalaAndCharScreen {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	@SuppressWarnings("resource")
	public static void main(final String[] args) throws IOException {
		Files.createDirectories(Paths.get(outDir));

		final Function<BufferedImage, BufferedImage> blueTint //
				= new PaletteConversion(C64ColorsColodore.PALETTE, ColorDistance.EUCLIDEAN, Dithering.None)//
						.andThen(new ColorReplacement(color -> {
							return color.equals(BLACK.color)
									? BLACK.color // @formatter:off
									: color.equals(WHITE.color) ? CYAN.color // @formatter:off
									: color.equals(RED.color) ? BLUE.color
									: color.equals(CYAN.color) ? CYAN.color //
									: color.equals(PURPLE.color) ? LIGHT_BLUE.color //
									: color.equals(GREEN.color) ? LIGHT_BLUE.color //
									: color.equals(BLUE.color) ? BLUE.color //
									: color.equals(YELLOW.color) ? CYAN.color //
									: color.equals(ORANGE.color) ? LIGHT_BLUE.color //
									: color.equals(BROWN.color) ? BLUE.color //
									: color.equals(LIGHT_RED.color) ? LIGHT_BLUE.color //
									: color.equals(DARK_GRAY.color) ? BLUE.color //
									: color.equals(GRAY.color) ? LIGHT_BLUE.color //
									: color.equals(LIGHT_GREEN.color) ? CYAN.color //
									: color.equals(LIGHT_BLUE.color) ? LIGHT_BLUE.color //
									: color.equals(LIGHT_GRAY.color) ? CYAN.color //
									: color; //@formatter:on
						}));

		final BufferedImage startImage = blueTint.apply(loadImageResource(ExamplePngBlendingToKoalaAndCharScreen.class, "resources/reluge1.png"));
		final BufferedImage endImage = blueTint.apply(loadImageResource(ExamplePngBlendingToKoalaAndCharScreen.class, "resources/reluge2.png"));

		final List<AutoCloseable> closeOnExit = new ArrayList<>();
		final Palette palette = () -> new Color[] { BLACK.color, BLUE.color, LIGHT_BLUE.color, CYAN.color };
		final Function<BufferedImage, ?> pipeline = new ImageDisplay<BufferedImage>(3, "original", Grid.Invisible, closeOnExit::add)//
				.andThen(new PaletteConversion(palette, ColorDistance.EUCLIDEAN, Dithering.Atkinson))//
				.andThen(new Pixelize(new Dimension(2, 1)))//
				.andThen(new PaletteConversion(palette, ColorDistance.EUCLIDEAN, Dithering.None))//
				.andThen(new ImageDisplay<BufferedImage>(3, "Palette applied", Grid.Invisible, closeOnExit::add))//
				.andThen(new WriteImageToFile<>(ImageFormat.PNG, n -> outDir + "reluge-image" + String.format("%02d", n) + ".png"))//
				.andThen(new ImageToC64BitmapMulticolor(BLACK, BLUE.color, LIGHT_BLUE.color, CYAN.color))//
				.andThen(new WriteC64BinaryToFile<>(n -> 0x2000, n -> outDir + "reluge-bitmap" + String.format("%02d", n) + ".prg"))//
				.andThen(bm -> new C64BitmapmodePictureMulticolor(bm, //
						new C64Screen(toUnsignedByte(toPositiveInt(BLUE.colorCode()) << 4 | LIGHT_BLUE.colorCode())), //
						new C64ColorRam(CYAN.colorCode()), //
						BLACK.colorCode()))//
				.andThen(kla -> kla.binaryProviderKoala()) //
				.andThen(new WriteC64BinaryToFile<>(n -> 0x6000, n -> outDir + "reluge-koala" + String.format("%02d", n) + ".kla"))//
				.andThen(binary -> C64BitmapmodePictureMulticolor.fromKoalaBinary(binary.toC64Binary()))//
				.andThen(new C64BitmapmodePictureMulticolorToImage(PALETTE))//
				.andThen(new ImageToC64CharmodePictureMulticolor(PALETTE, ColorDistance.EUCLIDEAN, CharacterReduction.UsingImageColorsOnly, //
						new CharmodeColorClashResolverMulticolor(PALETTE, ColorDistance.EUCLIDEAN)//
								.withForcedBackgroundColor(BLUE.color)//
								.withForcedMulticolor1D022(LIGHT_BLUE.color)//
								.withForcedMulticolor2D023(CYAN.color)//
								.withForcedMulticolorBlocksAt(IntStream.range(0, 1000)//
										.mapToObj(n -> new Point(n % C64Screen.WIDTH_BLOCKS, n / C64Screen.WIDTH_BLOCKS))//
										.toArray(Point[]::new))))//
				.andThen(new WriteC64BinaryToFile<>(n -> 0x2000, n -> outDir + "reluge-cs-screen-cram-bg" + String.format("%02d", n) + ".cmode")) //
				.andThen(new C64CharmodePictureToImageMulticolor())//
				.andThen(new ImageDisplay<BufferedImage>(3f, "as Charmode pic", closeOnExit::add))//
		// .andThen(new C64BitmapToC64CharsetScreenMulticolor(ColorDistance.EUCLIDEAN))//
		// .andThen(new C64CharsetScreenToImageMulticolor(PALETTE, BLACK, BLUE.color, LIGHT_BLUE.color,
		// PALETTE.colorThatReplacesThisColorInD800MulticolorCharmode(CYAN.color).get()))//
		// .andThen(new ImageDisplay<>(3f, "charScreen", Grid.Invisible, closeOnExit::add))//
		;

		final BlendFunction<BufferedImage> blendFunction = (s, alpha, e) -> {
			final BufferedImage result = UtilsImage.deepCopy(s);
			final Graphics2D g = result.createGraphics();
			try {
				g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) alpha));
				g.drawImage(e, 0, 0, null);
				return result;
			} finally {
				g.dispose();
			}
		};

		final int steps = 16;
		final double[] magnitudes = IntStream.range(0, steps).mapToDouble(n -> 1.0 / (steps - 1) * n).peek(System.out::println).toArray();
		final var emitter = new ImageBlendingEmitter(startImage, endImage, blendFunction, magnitudes);

		final var start = Instant.now();

		try (emitter; var consumer = new PipelineConsumer<>(pipeline, closeOnExit)) {
			emitter.add(consumer);
			emitter.run();
		}
		System.out.println("Total time = " + Duration.between(start, Instant.now()).toSeconds() + " seconds");
	}
}