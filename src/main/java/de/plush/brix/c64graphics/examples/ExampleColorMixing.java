package de.plush.brix.c64graphics.examples;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Stream;

import javax.swing.*;
import javax.swing.border.MatteBorder;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;

public class ExampleColorMixing {

	public static void main(final String[] args) {
		EventQueue.invokeLater(ExampleColorMixing::show);
	}

	private static void show() {
		final var gap = 10;
		final var mixes = List.of( //
				mixesPanel(Color.PINK, Color.DARK_GRAY) //
				, mixesPanel(Color.CYAN, Color.ORANGE) //
				, mixesPanel(Color.YELLOW, Color.BLUE) //
				, mixesPanel(Color.RED, Color.BLUE) //
				, mixesPanel(Color.GREEN, Color.MAGENTA) //
				, mixesPanel(Color.CYAN, Color.RED) //
				, mixesPanel(Color.BLUE, Color.CYAN) //
				, mixesPanel(Color.PINK, Color.ORANGE) //
				, mixesPanel(Color.DARK_GRAY, Color.LIGHT_GRAY) //
				, mixesPanel(C64ColorsColodore.BLUE.color(), C64ColorsColodore.BROWN.color()) //
				, mixesPanel(C64ColorsColodore.RED.color(), C64ColorsColodore.DARK_GRAY.color()) //
				, mixesPanel(C64ColorsColodore.PURPLE.color(), C64ColorsColodore.ORANGE.color()) //
				, mixesPanel(C64ColorsColodore.GREEN.color(), C64ColorsColodore.LIGHT_RED.color()) //
				, mixesPanel(C64ColorsColodore.GREEN.color(), C64ColorsColodore.LIGHT_BLUE.color()) //
				, mixesPanel(C64ColorsColodore.LIGHT_BLUE.color(), C64ColorsColodore.LIGHT_RED.color()) //
		);//
		final var panel = new JPanel();
		final var layout = new GridLayout(mixes.size(), 1);
		layout.setVgap(gap);

		panel.setLayout(layout);
		panel.setBackground(Color.BLACK);
		panel.setBorder(new MatteBorder(gap, gap, gap, gap, Color.BLACK));
		mixes.forEach(panel::add);

		final JFrame frame = new JFrame(ExampleColorMixing.class.getSimpleName());
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setContentPane(panel);
		frame.pack();
		frame.setVisible(true);
	}

	private static JPanel mixesPanel(final Color a, final Color b) {
		record MC(ColorMix m, Color color) {/**/}
		final Stream<MC> mixes = Arrays.stream(ColorMix.values())//
				.map((final ColorMix mixer) -> new MC(mixer, mixer.apply(a, b)))//
				.sorted(Comparator//
						// .<MC> comparingInt(mc -> mc.m().ordinal()).reversed()//
						.<MC> comparingInt(mc -> mc.color().getRGB())//
				);

		final var mixPanel = new JPanel() {
			@Override
			protected void paintComponent(final Graphics g) {
				g.setClip(null);
				g.setColor(a);
				g.fillRect(0, 0, getWidth(), getHeight());
				g.setColor(b);
				for (int y = 0; y < getHeight(); y += 2) {
					g.drawLine(0, y, getWidth() / 2, y);
				}
				for (int y = 0; y < getHeight(); ++y) {
					for (int x = getWidth() / 2 + (y % 2 == 0 ? 1 : 0); x < getWidth(); x += 2) {
						g.drawLine(x, y, x, y);
					}
				}
			}
		};
		mixPanel.add(new JLabel("          "));
		mixPanel.setBackground(Color.BLACK);
		mixPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		final var layout = new FlowLayout();
		layout.setHgap(0);
		layout.setVgap(0);
		mixPanel.setLayout(layout);
		mixes.map(mix -> { //
			final var label = new JLabel(mix.m.name()); //
			label.setHorizontalAlignment(SwingConstants.CENTER);
			label.setAlignmentX(Component.CENTER_ALIGNMENT);
			label.setBackground(mix.color());
			label.setOpaque(true);
			label.setForeground(Color.BLACK);
			label.setBorder(new MatteBorder(10, 10, 10, 10, mix.color())); //
			label.setToolTipText(mix.color().toString());
			return label;
		}).forEach(mixPanel::add);
		mixPanel.add(new JLabel("          "));

		final var top = new JPanel();
		top.setBackground(a);

		final var bottom = new JPanel();
		bottom.setBackground(b);

		final var overview = new JPanel();
		overview.setAlignmentX(Component.CENTER_ALIGNMENT);

		overview.setLayout(new BorderLayout(1, 1));
		overview.setBackground(Color.BLACK);
		overview.add(top, BorderLayout.NORTH);

		overview.add(mixPanel, BorderLayout.CENTER);
		overview.add(bottom, BorderLayout.SOUTH);
		final var fixPanel = new JPanel();
		fixPanel.setBackground(Color.BLACK);
		fixPanel.setLayout(new BorderLayout());
		fixPanel.setMaximumSize(fixPanel.getPreferredSize());
		fixPanel.setMinimumSize(fixPanel.getPreferredSize());
		fixPanel.add(overview, BorderLayout.NORTH);
		return fixPanel;
	}
}
