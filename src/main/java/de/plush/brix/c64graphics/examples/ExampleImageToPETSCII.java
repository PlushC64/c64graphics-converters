package de.plush.brix.c64graphics.examples;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.function.Function;

import javax.imageio.ImageIO;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.model.C64Charset.ROM;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.FindMostCommonColor;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.FindMostCommonColor.Calculate;
import de.plush.brix.c64graphics.core.util.charpixeldistance.C64CharHiresPixelDistance;

/** Main class used for starting the conversion. */
public class ExampleImageToPETSCII {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	public static void main(final String[] args) throws URISyntaxException, IOException {

		// final String picName = "das-ende-1489872225001.jpg";
		final String picName = "paintface.jpg";
		// final String picName = "geisha-lovely-woman-face-1.jpg";
		final URI uri = ExampleImageToPETSCII.class.getResource("resources").toURI();
		final File imageFile = Paths.get(uri).resolve(picName).toFile();
		Files.createDirectories(Paths.get(outDir));

		final Palette palette = () -> new Color[] { BLACK.color, WHITE.color };
		final ColorDistance colorDistanceMeasure = ColorDistance.EUCLIDEAN_WEIGHTED;
		final Dithering dithering = Dithering.None;

		final FindMostCommonColor backgroundProviderPC = new FindMostCommonColor(Calculate.FIRST_FRAME_AND_REUSE);

		@SuppressWarnings("resource")
		final Function<BufferedImage, ?> pipeline = backgroundProviderPC// get most common color
				.andThen(new Rescale(C64CharsetScreen.sizePixels(), backgroundProviderPC))// using color finder to fill frame when resizing
				.andThen(new ImageDisplay<>(2f, "scaled", Grid.sizeC64Char))//
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, dithering))//
				.andThen(new ImageDisplay<>(2f, "palette applied", Grid.Invisible))//
				.andThen(new ImageToC64BitmapHires(() -> WHITE.color, BLACK.color)) //
				.andThen(new C64BitmapToC64CharsetScreenHiresUsingCharset(C64Charset.load(ROM.C64_UpperCase),
						C64CharHiresPixelDistance.HAMMING_AND_FINE_CIRCULAR_DENSITY_CHANGE))//
				.andThen(new C64CharsetScreenToImageHires(palette, () -> WHITE.color, BLACK.color))//
				.andThen(new ImageDisplay<>(2f, "converted", Grid.Invisible))
		// .andThen(pic -> pic.binaryProviderHiEddi())// some C64 models aren't binary providers themselves, but provide several - we need to pick one
		// .andThen(new WriteC64BinaryToFile<>(n -> 0x6000, n -> outDir + picName.replaceAll("\\.jpg", "") + ".hed"))//
		;

		pipeline.apply(ImageIO.read(imageFile));
	}

}
