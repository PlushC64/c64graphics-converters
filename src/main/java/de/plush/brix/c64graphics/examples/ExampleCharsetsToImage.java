package de.plush.brix.c64graphics.examples;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;
import static de.plush.brix.c64graphics.core.util.UtilExceptions.throwEx;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.net.*;
import java.nio.file.Paths;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.model.C64Charset;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.util.StartAddress;

public class ExampleCharsetsToImage {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	private static URI file(final String fileName) throws URISyntaxException {
		final URI dataDir = ExampleMulticolorCharsetPictureToImage_Caren.class.getResource("resources/sample_charsets").toURI();
		return Paths.get(dataDir).resolve(fileName).toFile().toURI();
	}

	private enum Mode {
		Hires, Multicolor
	}

	@SuppressWarnings("resource")
	private static void show(final C64Charset charset, final CharsetFormat format, final Mode mode, final Color mc1, final Color mc2, final Color d800) {
		final Palette palette = C64ColorsColodore.PALETTE;
		final Supplier<Color> backgroundColorProvider = () -> C64ColorsColodore.BLACK.color;

		final IPipelineStage<C64Charset, BufferedImage> pipeline = //
				mode == Mode.Multicolor ? new C64CharsetToImageMulticolor(format, palette, backgroundColorProvider, mc1, mc2, d800) //
						: mode == Mode.Hires ? new C64CharsetToImageHires(format) //
								: throwEx(new IllegalArgumentException("mode"));

		final BufferedImage image = pipeline.apply(charset);
		new ImageDisplay<>(2.0f, format.toString() + "," + mode).apply(image);
	}

	public static void main(final String[] args) throws Exception {
		final var _1x2hires = C64Charset.loadCharset(file("1x2 hires.prg"), StartAddress.InFile);
		final var _2x1multi = C64Charset.loadCharset(file("2x1 multi.prg"), StartAddress.InFile);
		final var _2x2multi = C64Charset.loadCharset(file("2x2 multi.prg"), StartAddress.InFile);
		final var _3x3multi = C64Charset.loadCharset(file("3x3 multi.prg"), StartAddress.InFile);

		show(_1x2hires, CharsetFormats.size1x2_wowCharMaster, Mode.Hires, null, null, null);
		show(_2x1multi, CharsetFormats.size2x1_wowCharMaster, Mode.Multicolor, LIGHT_BLUE.color, BLUE.color, CYAN.color);
		show(_2x2multi, CharsetFormats.size2x2_wowCharMaster, Mode.Multicolor, LIGHT_BLUE.color, BLUE.color, CYAN.color);
		show(_3x3multi, CharsetFormats.size3x3_wowCharMaster, Mode.Multicolor, BLUE.color, LIGHT_BLUE.color, CYAN.color);
	}
}
