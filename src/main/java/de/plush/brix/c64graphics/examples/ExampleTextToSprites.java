package de.plush.brix.c64graphics.examples;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.model.C64Charset.ROM;
import de.plush.brix.c64graphics.core.model.PETSCII.Case;
import de.plush.brix.c64graphics.core.pipeline.displays.*;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.util.Utils;

public class ExampleTextToSprites {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	public static void main(final String[] args) {

		final var palette = C64ColorsColodore.PALETTE;


		final var screencodes = new byte[][] { //
				PETSCII.stringToScreenCodes("Code:    Music:      ", Case.Lower), //
				PETSCII.stringToScreenCodes("Brix+Map Laxity/Vibrants", Case.Lower), //
				PETSCII.stringToScreenCodes("Normal is boring", Case.Lower), //
				PETSCII.stringToScreenCodes("", Case.Lower), //
				PETSCII.stringToScreenCodes("White Lines ", Case.Lower), //
				PETSCII.stringToScreenCodes("", Case.Lower), //
				PETSCII.stringToScreenCodes("No loading", Case.Lower), //
				PETSCII.stringToScreenCodes("No cry!", Case.Lower), //
		};
		final Dimension sizeSprites = new Dimension(8, 4);

		final var charset = C64Charset.load(ROM.PET_LowerCase);

		final var image = new BufferedImage(320, 200, BufferedImage.TYPE_INT_RGB);
		final var g = image.createGraphics();
		try {
			final var charToImage = new C64CharToImageHires(palette, C64ColorsColodore.BLACK, C64ColorsColodore.RED.color);
			int py = -C64Char.HEIGHT_PIXELS;
			for (int y = 0; y < screencodes.length; ++y) {
				py += C64Char.HEIGHT_PIXELS;
				if (y > 0 && y % 2 == 0) {
					py += 5; // Sprites are only 21 high, every third line skip too small bottom 5 pixels to next sprite top
				}
				for (int x = 0; x < screencodes[y].length; ++x) {
					final var chr = charset.characterBlockOf(screencodes[y][x]).get();
					final var chrImage = charToImage.apply(chr);
					g.drawImage(chrImage, x * 8, py, 8, 8, null);
				}
			}
		} finally {
			g.dispose();
		}

		@SuppressWarnings({ "unchecked", "resource" })
		final Function<BufferedImage, C64SpriteGrid> pipeline = //
				new ImageDisplay<BufferedImage>(4f, "generated charset", Grid.sizeC64Sprite) //
						.andThen(new ImageToC64SpriteGridHires(C64ColorsColodore.RED.color, new Point(0, 0), sizeSprites))//
						.andThen(new Display<>(new C64SpriteGridToImageHires(palette, C64ColorsColodore.RED.color), new ImageDisplay<>(4f,
								"generated SpriteGrid", Grid.sizeC64Sprite)));

		final var spriteGrid = pipeline.apply(image);
		Utils.writeFile(outDir + "sprites.bin", spriteGrid.toC64BinaryIgnoreEmptySprites());

	}
}
