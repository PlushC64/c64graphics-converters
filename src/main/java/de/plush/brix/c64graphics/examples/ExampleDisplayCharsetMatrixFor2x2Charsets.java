package de.plush.brix.c64graphics.examples;

import java.util.function.Function;

import de.plush.brix.c64graphics.core.model.C64Charset;
import de.plush.brix.c64graphics.core.model.C64Charset.ROM;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;

public class ExampleDisplayCharsetMatrixFor2x2Charsets {

	public static void main(final String[] args) {
		final Function<C64Charset, ?> pipeline = //
				new C64CharsetToImageHires(CharsetFormats.size2x2_wowCharMaster)//
						.andThen(x -> new ImageDisplay<>(4f, "generated charset", Grid.sizeC64Char).apply(x)) //
		;

		pipeline.apply(C64Charset.load(ROM.C64_LowerCase));
	}
}
