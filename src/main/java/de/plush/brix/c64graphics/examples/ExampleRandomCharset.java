package de.plush.brix.c64graphics.examples;

import java.io.IOException;
import java.nio.file.*;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.model.C64Charset;
import de.plush.brix.c64graphics.core.model.generators.WhiteNoiseCharset;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.util.Utils;

public class ExampleRandomCharset {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	public static void main(final String[] args) throws IOException {
		Files.createDirectories(Paths.get(outDir));

		final C64Charset randomCharset1 = new WhiteNoiseCharset()//
				.withRandom(new Random(0xff))//
				.withSteps(64)//
				.ditheredFromMinToMaxPixels(0, 64).reversed();

		final C64Charset randomCharset2 = new WhiteNoiseCharset()//
				.withRandom(new Random(0x0e))//
				.withSteps(64)//
				.ditheredFromMinToMaxPixelsRetainingPositions(0, 64);

		final C64Charset randomCharset3 = new WhiteNoiseCharset()//
				.withRandom(new Random(0xff))//
				.withSteps(C64Charset.MAX_CHARACTERS - 2)//
				.uniformWithPixelsPerChar(8 * 8 / 2);

		final var nr = new AtomicInteger();
		final Function<C64Charset, ?> pipeline = //
				new C64CharsetToImageHires(CharsetFormats.size1x1_withFillChar$FF)//
						.andThen(x -> new ImageDisplay<>(4f, "generated charset " + nr.incrementAndGet(), Grid.sizeC64Char).apply(x)) //
		;
		pipeline.apply(randomCharset1);
		pipeline.apply(randomCharset2);
		pipeline.apply(randomCharset3);

		Utils.writeFile(outDir + "random_charset1.bin", randomCharset1.toC64Binary());
		Utils.writeFile(outDir + "random_charset2.bin", randomCharset2.toC64Binary());
		Utils.writeFile(outDir + "random_charset3.bin", randomCharset3.toC64Binary());

		// System.in.read();
		// System.exit(0);
	}
}
