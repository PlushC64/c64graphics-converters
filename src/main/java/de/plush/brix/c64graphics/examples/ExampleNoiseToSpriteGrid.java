package de.plush.brix.c64graphics.examples;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.*;

import de.plush.brix.c64graphics.core.emitters.*;
import de.plush.brix.c64graphics.core.model.C64SpriteGrid;
import de.plush.brix.c64graphics.core.pipeline.PipelineConsumer;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.ImageToC64SpriteGridHires;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.Wait;
import de.plush.brix.c64graphics.core.util.Utils;

public class ExampleNoiseToSpriteGrid {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/" + ExampleNoiseToSpriteGrid.class.getSimpleName() + "/";
	}

	enum Pattern {

		Rectangle() {
			@Override
			AbstractImageEmitter<BufferedImage> emitter() {
				final IntUnaryOperator numberOfRectangles = (frameNr) -> 250;
				final IntUnaryOperator noiseGeneratorSeed = (frameNr) -> frameNr;
				return new SquareNoiseEmitter(4, new Dimension(320, 200), noiseGeneratorSeed, numberOfRectangles, new Dimension(5, 5), new Dimension(20, 20), //
						Color.WHITE, Color.BLACK);//

			}
		},
		SimplexWavy() {
			@Override
			AbstractImageEmitter<BufferedImage> emitter() {
				final DoubleFunction<Color> valueToColor = (value) -> Math.abs(value) > 0.06 ? Color.WHITE : Color.BLACK;
				final var frames = 4;
				final ToDoubleBiFunction<Point, Integer> scaleX = (pos, frameNr) -> 0.04 * pos.x;
				final ToDoubleBiFunction<Point, Integer> scaleY = (pos, frameNr) -> 0.04 * pos.y;
				final ToDoubleBiFunction<Point, Integer> scaleZ = (pos, frameNr) -> 1.0 / frames * frameNr;
				final IntUnaryOperator noiseGeneratorSeed = (frameNr) -> 0;
				return new SimplexNoiseEmitter(frames, new Dimension(320, 200), valueToColor, noiseGeneratorSeed, scaleX, scaleY, scaleZ);

			}
		},
		SimplexCow() {
			@Override
			AbstractImageEmitter<BufferedImage> emitter() {
				final DoubleFunction<Color> valueToColor = (value) -> value >= 0 ? Color.WHITE : Color.BLACK;
				// final DoubleFunction<Color> valueToColor = (value) -> value > 0.45 ? Color.WHITE : Color.BLACK;
		//@formatter:off
//				final DoubleFunction<Color> valueToColor = (value) -> new Color(
//						(int) (Math.abs(value) * 100d),
//						(int) (Math.abs(value) * 100d),
//						(int) (Math.abs(value) * 255d)
//				).brighter().brighter();
				 // @formatter:on
				final var frames = 4;
				final ToDoubleBiFunction<Point, Integer> scaleX = (pos, frameNr) -> 0.04 * pos.x;
				final ToDoubleBiFunction<Point, Integer> scaleY = (pos, frameNr) -> 0.04 * pos.y;
				final ToDoubleBiFunction<Point, Integer> scaleZ = (pos, frameNr) -> 1.0 / frames * frameNr;
				final IntUnaryOperator noiseGeneratorSeed = (frameNr) -> 0;
				return new SimplexNoiseEmitter(frames, new Dimension(320, 200), valueToColor, noiseGeneratorSeed, scaleX, scaleY, scaleZ);

			}
		},
		SimplexBluePlasma() {
			@Override
			AbstractImageEmitter<BufferedImage> emitter() {
		//@formatter:off
				final DoubleFunction<Color> valueToColor = (value) -> new Color(
						(int) (Math.abs(value) * 100d),
						(int) (Math.abs(value) * 100d),
						(int) (Math.abs(value) * 255d)
				).brighter().brighter();
		//@formatter:on
				final var frames = 4;
				final ToDoubleBiFunction<Point, Integer> scaleX = (pos, frameNr) -> 0.04 * pos.x;
				final ToDoubleBiFunction<Point, Integer> scaleY = (pos, frameNr) -> 0.04 * pos.y;
				final ToDoubleBiFunction<Point, Integer> scaleZ = (pos, frameNr) -> 1.0 / frames * frameNr;
				final IntUnaryOperator noiseGeneratorSeed = (frameNr) -> 0;
				return new SimplexNoiseEmitter(frames, new Dimension(320, 200), valueToColor, noiseGeneratorSeed, scaleX, scaleY, scaleZ);

			}
		}
		//
		;

		abstract AbstractImageEmitter<BufferedImage> emitter();

	}

	public static void main(final String[] args) {
		final List<AutoCloseable> resourcesToClose = new ArrayList<>();

		@SuppressWarnings("resource")
		final Function<BufferedImage, C64SpriteGrid> pipeline //
				= new ImageDisplay<BufferedImage>(3f, "original", resourcesToClose::add) //
						.andThen(new ImageToC64SpriteGridHires(Color.WHITE, new Point(0, 0), new Dimension(4, 6)))//
						.andThen(new Wait<>(500, TimeUnit.MILLISECONDS))
		//
		;

		final Consumer<List<C64SpriteGrid>> postProcessing = (final List<C64SpriteGrid> spriteGrids) -> {
			final var binaries = Utils.toByteArray(spriteGrids.stream().map(C64SpriteGrid::toC64Binary));
			Utils.writeFile(outDir + "sprites_background4x6x4.bin", binaries);
		};

		try (final var emitter = Pattern.Rectangle.emitter(); //
				final var imageConsumer = new PipelineConsumer<>(pipeline, postProcessing, resourcesToClose);) {
			emitter.add(imageConsumer);
			emitter.run();
		}
	}

}
