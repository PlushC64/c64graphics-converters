package de.plush.brix.c64graphics.examples;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.function.Function;

import javax.imageio.ImageIO;

import de.plush.brix.c64graphics.core.model.C64Bitmap;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.ImageToC64BitmapmodePictureMulticolor;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.FindMostCommonColor.Calculate;

/** Main class used for starting the conversion. */
public class ExampleImageToKoala {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	public static void main(final String[] args) throws URISyntaxException, IOException {

		final String picName = "Geisha-by-Luca-Tarlazzi-513x640.jpg";
		// final String picName = "geisha-lovely-woman-face-1.jpg";
		final URI uri = ExampleImageToHires.class.getResource("resources").toURI();
		final File imageFile = Paths.get(uri).resolve(picName).toFile();
		Files.createDirectories(Paths.get(outDir));

		final Palette palette = C64ColorsColodore.PALETTE;
		final ColorDistance colorDistanceMeasure = ColorDistance.EUCLIDEAN_WEIGHTED;
		final Dithering dithering = Dithering.Atkinson;

		final FindMostCommonColor backgroundProviderPC = new FindMostCommonColor(Calculate.FIRST_FRAME_AND_REUSE);
		final FindMostCommonColor backgroundProviderC64 = new FindMostCommonColor(Calculate.FIRST_FRAME_AND_REUSE);

		final Function<BufferedImage, ?> pipeline = backgroundProviderPC// get most common color
				.andThen(new Rescale(C64Bitmap.sizePixels(), backgroundProviderPC))// using color finder to fill frame when resizing
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, dithering))// we now have c64 colors in hires resolution
				.andThen(new Pixelize(new Dimension(2, 1)))// pixelization will create non-c64 colors
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, Dithering.None))// We can't use dithering here as we would get hires pixels
				.andThen(backgroundProviderC64)// find the C64-palette color for the background
				.andThen(new ImageToC64BitmapmodePictureMulticolor(palette, backgroundProviderC64, colorDistanceMeasure)//
						.withPreferredMulticolor1D022Colors(C64ColorsColodore.BLACK.color)// take additional control over color assignment
				)//
				// .andThen(new Display<>(new C64BitmapmodePictureMulticolorToImage(palette), new ImageDisplay<>(3f)))//
				.andThen(pic -> pic.binaryProviderKoala())// some C64 models aren't binary providers themselves, but provide several - here we need to pick one
				.andThen(new WriteC64BinaryToFile<>(n -> 0x6000, n -> outDir + picName.replaceAll("\\.jpg", "") + ".kla"))//
		;

		pipeline.apply(ImageIO.read(imageFile));
	}

}
