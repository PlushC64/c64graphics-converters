package de.plush.brix.c64graphics.examples;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.function.Function;

import javax.imageio.ImageIO;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.displays.*;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.FindMostCommonColor.Calculate;
import de.plush.brix.c64graphics.core.util.UtilsImage.ImageFormat;

/** Main class used for starting the conversion. */
public class ExampleImagetoHalfcharFli {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	public static void main(final String[] args) throws URISyntaxException, IOException {

		final String picName = "leeds-25209-neo-victorian-fantasy-leeds.jpg";
		final URI uri = ExampleImagetoHalfcharFli.class.getResource("resources").toURI();
		final File imageFile = Paths.get(uri).resolve(picName).toFile();
		Files.createDirectories(Paths.get(outDir));

		final Palette palette = C64ColorsColodore.PALETTE;
		// final Dithering dithering = Dithering.Jarvis_Judice_Ninke;
		// final Dithering dithering = Dithering.BrixInterleaveMulticolor;
		final Dithering dithering = Dithering.SlashMulticolor;
		final ColorDistance colorDistanceMeasure = ColorDistance.EUCLIDEAN;

		final FindMostCommonColor backgroundProviderPC = new FindMostCommonColor(Calculate.FIRST_FRAME_AND_REUSE);

		@SuppressWarnings("resource")
		final Function<BufferedImage, ?> pipeline = backgroundProviderPC// get most common color
				.andThen(new Rescale(C64CharsetScreen.sizePixels(), backgroundProviderPC))// using color finder to fill frame when resizing
				.andThen(new ImageDisplay<>(3f, "scaled", Grid.Invisible))//
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, dithering))// we now have c64 colors in hires resolution
				// .andThen(new ImageDisplay<>(3f, "palette applied", Grid.sizeC64Char))//
				.andThen(new Pixelize(new Dimension(2, 1)))// pixelization will create non-c64 colors
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, Dithering.None))// We can't use dithering here as we would get hires pixels
				// .andThen(new ImageDisplay<>(3f, "pixelized to multicolor", Grid.sizeC64Char))//
				.andThen(new ImageToC64BitmapmodePictureMulticolorHalfcharFli(palette, C64ColorsColodore.BLACK, colorDistanceMeasure))//
				.andThen(new Display<>(new C64BitmapmodePictureMulticolorHalfcharFliToImage(palette), //
						new ImageDisplay<>(3f, "Multicolor_Halfchar_FLI", Grid.Invisible))) //
				.andThen(pic -> pic.binaryProviderPlush())// some C64 models aren't binary providers themselves, but provide several - here we pick one
				.andThen(new WriteC64BinaryToFile<>(n -> 0x3b00, n -> outDir + picName.replaceAll("\\.jpg", "") + ".hcf"))//
				// create a proof image:
				.andThen(new C64BinaryProviderToBinary())//
				.andThen(C64BitmapmodePictureMulticolorHalfcharFli::fromPlushBinary)//
				.andThen(new C64BitmapmodePictureMulticolorHalfcharFliToImage(palette))//
				.andThen(new WriteImageToFile<>(ImageFormat.PNG, n -> outDir + picName.replaceAll("\\.jpg", ".proof.png")))//
		;

		pipeline.apply(ImageIO.read(imageFile));
		// System.exit(0);
	}
}
