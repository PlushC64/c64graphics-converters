package de.plush.brix.c64graphics.examples;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.*;
import java.nio.file.*;
import java.util.function.Function;

import javax.imageio.ImageIO;

import de.plush.brix.c64graphics.core.model.C64CharsetScreen;
import de.plush.brix.c64graphics.core.pipeline.displays.*;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.HiEddiWithHiresSpriteOverlayColorClashResolver;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.FindMostCommonColor.Calculate;

/** Main class used for starting the conversion. */
public class ExampleImageToHiresWithSpriteOverlay {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	public static void main(final String[] args) throws URISyntaxException, Exception {

		// final String picName = "Geisha-by-Luca-Tarlazzi-513x640.jpg";
		final String picName = "Geisha-by-Luca-Tarlazzi-513x640.jpg";
		final URI uri = ExampleImageToHiresWithSpriteOverlay.class.getResource("resources").toURI();
		final File imageFile = Paths.get(uri).resolve(picName).toFile();
		Files.createDirectories(Paths.get(outDir));

		final Palette palette = C64ColorsColodore.PALETTE;
		final ColorDistance colorDistanceMeasure = ColorDistance.EUCLIDEAN_WEIGHTED;
		final Dithering dithering = Dithering.Atkinson;

		final FindMostCommonColor backgroundProviderPC = new FindMostCommonColor(Calculate.FIRST_FRAME_AND_REUSE);

		@SuppressWarnings("resource")
		final Function<BufferedImage, ?> pipeline = backgroundProviderPC// get most common color
				.andThen(new Rescale(C64CharsetScreen.sizePixels(), backgroundProviderPC))// using color finder to fill frame when resizing
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, dithering))// we now have c64 colors in hires resolution
				.andThen(new ImageToC64BitmapmodePictureHiresWithHiresSpriteOverlay(palette, new HiEddiWithHiresSpriteOverlayColorClashResolver(new Dimension(6,
						9), new Point(12, 1), colorDistanceMeasure)).withPreferredBackgroundColors(WHITE.color())//
								.withPreferredForegroundColors(DARK_GRAY.color(), GRAY.color())//
				).andThen(new Display<>(new C64BitmapmodePictureHiresWithHiresSpriteOverlayToImage(palette), new ImageDisplay<>(2.0f, "result " + palette + " "
						+ colorDistanceMeasure, Grid.Invisible)))//
				.andThen(pic -> pic.binaryProviderHiEddiFollowedBySpriteData())//
				.andThen(new WriteC64BinaryToFile<>(n -> 0x4000, n -> outDir + picName.replaceAll("\\.jpg", "") + ".hedspr"))//
		;

		pipeline.apply(ImageIO.read(imageFile));
	}

}
