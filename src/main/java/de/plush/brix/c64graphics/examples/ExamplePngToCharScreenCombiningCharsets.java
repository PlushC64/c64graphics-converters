package de.plush.brix.c64graphics.examples;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.*;
import java.nio.file.*;
import java.util.Random;
import java.util.function.Function;

import javax.imageio.ImageIO;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.model.generators.WhiteNoiseCharset;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.CharmodeColorClashResolverMulticolor;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.*;
import de.plush.brix.c64graphics.core.util.*;
import de.plush.brix.c64graphics.core.util.UtilsImage.ImageFormat;

/**
 * Example for a complex Logo conversion:
 * 1. creates a random charset for a "flamer" effect.
 * 2. changes a logo from PNG to charset+screen+colorRA, replacing colors to avoid nasty artifacts
 * 3. moves the logo down by a few characters
 * 3. adds the result charset to the flamer charset and modifies all screen codes acordingly
 */
public class ExamplePngToCharScreenCombiningCharsets {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	public static void main(final String[] args) throws URISyntaxException, IOException {

		final String picName = "Excess-Minimalist_MC_logo1.png";
		final URI uri = ExamplePngToCharScreenCombiningCharsets.class.getResource("resources").toURI();
		final URI imageFile = Paths.get(uri).resolve(picName).toUri();
		Files.createDirectories(Paths.get(outDir));

		final Palette palette = C64ColorsColodore.PALETTE;
		final ColorDistance colorDistanceMeasure = ColorDistance.EUCLIDEAN;

		final C64Charset flamerCharset = new WhiteNoiseCharset()//
				.withRandom(new Random(0xff))//
				.withSteps(64)//
				.ditheredFromMinToMaxPixels(0, 64)//
		// .reversed()
		;

		final Function<BufferedImage, BufferedImage> pipeline = //
				new PaletteConversion(C64ColorsColodore.PALETTE, colorDistanceMeasure, Dithering.None)//
						.andThen(new ImageDisplay<>(2, "original", Grid.Invisible))//
						.andThen(new ColorReplacement(color -> //
						color.equals(C64ColorsColodore.CYAN.color) ? C64ColorsColodore.LIGHT_GRAY.color()//
								: color.equals(C64ColorsColodore.ORANGE.color) ? C64ColorsColodore.LIGHT_GRAY.color()//
										: color.equals(C64ColorsColodore.GRAY.color) ? C64ColorsColodore.LIGHT_BLUE.color()//
												: color.equals(C64ColorsColodore.RED.color) ? C64ColorsColodore.LIGHT_BLUE.color//
														: color.equals(C64ColorsColodore.DARK_GRAY.color) ? C64ColorsColodore.BLUE.color()//
																: color.equals(C64ColorsColodore.BROWN.color) ? C64ColorsColodore.BLUE.color//
																		: color //

						))//
						.andThen(new ImageDisplay<>(2, "colorChange", Grid.Invisible))//
						.andThen(new ImageToC64CharmodePictureMulticolor(palette, colorDistanceMeasure, CharacterReduction.UsingAllPaletteColors,
								new CharmodeColorClashResolverMulticolor(palette, colorDistanceMeasure)//
										.withForcedBackgroundColor(Color.BLACK) //
										.andThen(new ImageDisplay<>(2, "no_color_clashes", Grid.sizeC64Char)))//
						).andThen(cmp -> { cmp.rollDownBlockwise(8); cmp.rollRightBlockwise(0); return cmp; })//
						.andThen(cmp -> {
							final var screen = cmp.screen();
							System.out.println("flamer Charset: " + flamerCharset.size());
							System.out.println("logo Charset: " + cmp.charsetSize());
							final var charset = new C64Charset();
							charset.addAllCharactersEvenIfAlreadyPresent(flamerCharset.charStream());
							System.out.println("new Charset with flamerChars: " + charset.size());

							charset.addAllCharactersEvenIfAlreadyPresent(cmp.charset().charStream());
							System.out.println("new Charset with flamerChars+logoChars: " + charset.size());
							final int flamerCharsetSize = flamerCharset.size();
							final ObjByteToByteFunction<Point> addFlamerChars = (pos, code) -> Utils.toUnsignedByte((code + flamerCharsetSize) % 0xff);
							screen.modifyEveryScreenCode(addFlamerChars);

							return new C64CharsetScreenColorRam(charset, screen, cmp.colorRam(), //
									cmp.backgroundColorCode(), cmp.mc1_D022ColorCode(), cmp.mc2_D023ColorCode());
						})//
						.andThen(cmp -> {
							cmp.switchScreenCodes((byte) 0x20, cmp.screenCodeAt(0, 0)); // switch logo blank to system blank
							System.out.println(cmp.screen());
							return cmp;
						}).andThen(new Sidechannels<>(//
								new WriteC64BinaryToFile<>(n -> 0x2000, n -> outDir + picName.replaceAll("\\.png", "") + ".charset")//
										.compose(C64CharsetScreenColorRam::charset)//
								, new WriteC64BinaryToFile<>(n -> 0x2800, n -> outDir + picName.replaceAll("\\.png", "") + ".screen")//
										.compose(C64CharsetScreenColorRam::screen)//
								, new WriteC64BinaryToFile<>(n -> 0x2c00, n -> outDir + picName.replaceAll("\\.png", "") + ".colorRam")//
										.compose(C64CharsetScreenColorRam::colorRam)//
						)).andThen(new C64CharmodePictureToImageMulticolor(palette)) //
						.andThen(new WriteImageToFile<>(ImageFormat.PNG, n -> outDir + picName.replaceAll("\\.png", "") + ".cs.png"))//
						.andThen(new ImageDisplay<>(2, "as char+screen -> png", Grid.Invisible))//
		;

		pipeline.apply(ImageIO.read(imageFile.toURL()));

	}

}
