package de.plush.brix.c64graphics.examples;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.*;
import java.util.stream.*;

import javax.imageio.ImageIO;
import javax.swing.*;

import de.plush.brix.c64graphics.core.model.C64Bitmap;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.FindMostCommonColor;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.FindMostCommonColor.Calculate;

public class ExampleDitherings {

	public static void main(final String[] args) {
		show(C64ColorsPepto.PALETTE, ColorDistance.DIN99b, Dithering.values(), "no_copy_g2_04.jpg", "blue-macaws.jpg", "g1_15.jpg", "visu45.jpg", "mind01.jpg",
				"zone02.jpg");
	}

	private static File loadImage(final String picName) throws URISyntaxException {
		final URI uri = ExampleDitherings.class.getResource("resources").toURI();
		final File imageFile = Paths.get(uri).resolve(picName).toFile();
		return imageFile;
	}

	private static void show(final Palette palette, final ColorDistanceMeasure distanceMeasure, final DitheringSpec[] ditherings, final String... picNames) {
		EventQueue.invokeLater(() -> {
			final var ui = createUI(ditherings, picNames);

			ui.limitsCombo.addActionListener(evt -> show(palette, distanceMeasure, ditherings, ui));
			ui.picNameCombo.addActionListener(evt -> show(palette, distanceMeasure, ditherings, ui));
			ui.limitsCombo.setSelectedIndex(0);
			ui.frame.pack();
			ui.frame.setVisible(true);
		});
	}

	private static UI createUI(final DitheringSpec[] ditherings, final String[] picNames) {
		final var examples = new JPanel();
		examples.setLayout(new GridLayout(0, 5));

		final var limitsCombo = new JComboBox<>(Limits.values());
		// don't set index yet, to make a first change later
		limitsCombo.setMaximumSize(limitsCombo.getPreferredSize());

		final var picNameCombo = new JComboBox<>(picNames);
		picNameCombo.setSelectedIndex(0);
		picNameCombo.setMaximumSize(picNameCombo.getPreferredSize());

		final var progressBar = new JProgressBar(0, ditherings.length);
		progressBar.setStringPainted(true);
		progressBar.setVisible(false);

		final var controlPanel = new JPanel();
		final var layout = new BoxLayout(controlPanel, BoxLayout.X_AXIS);
		controlPanel.setLayout(layout);
		controlPanel.add(new JLabel("Pic: "));
		controlPanel.add(Box.createHorizontalStrut(5));
		controlPanel.add(picNameCombo);
		controlPanel.add(Box.createHorizontalStrut(20));
		controlPanel.add(new JLabel("Limits: "));
		controlPanel.add(Box.createHorizontalStrut(5));
		controlPanel.add(limitsCombo);
		controlPanel.add(Box.createHorizontalStrut(20));
		controlPanel.add(progressBar);
		controlPanel.add(Box.createHorizontalGlue());

		final JFrame frame = new JFrame("overview");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(controlPanel, BorderLayout.NORTH);
		frame.getContentPane().add(examples, BorderLayout.CENTER);
		return new UI(examples, limitsCombo, picNameCombo, progressBar, frame);
	}

	static record UI(JPanel examples, JComboBox<Limits> limitsCombo, JComboBox<String> picNameCombo, JProgressBar progressBar, JFrame frame) {/**/}

	private static void show(final Palette palette, final ColorDistanceMeasure distanceMeasure, final DitheringSpec[] ditherings, final UI ui) {
		try {
			ui.progressBar.setVisible(true);
			ui.picNameCombo.setEnabled(false);
			ui.limitsCombo.setEnabled(false);
			final String picName = ui.picNameCombo.getItemAt(ui.picNameCombo.getSelectedIndex());
			final Limits selection = ui.limitsCombo.getItemAt(ui.limitsCombo.getSelectedIndex());
			final var originalImage = ImageIO.read(loadImage(picName));

			final IntConsumer progress = index -> EventQueue.invokeLater(() -> ui.progressBar.setValue(index + 1));
			CompletableFuture.supplyAsync(() -> produceImages(originalImage, palette, distanceMeasure, ditherings, selection, progress))//
					.thenAccept(ditheringToImage -> {
						EventQueue.invokeLater(() -> {
							ui.examples.removeAll();
							ui.examples.add(panel("Original", new Rescale(C64Bitmap.sizePixels(), () -> Color.black).apply(originalImage)));
							ditheringToImage.forEach((dithering, image) -> ui.examples.add(panel(Dithering.name(dithering), image)));
							EventQueue.invokeLater(() -> {
								ui.progressBar.setVisible(false);
								ui.picNameCombo.setEnabled(true);
								ui.limitsCombo.setEnabled(true);
							});
							ui.frame.pack();
							ui.frame.repaint();
						});
					}).exceptionally(t -> { t.printStackTrace(); return null; })//
			;
		} catch (final IOException | URISyntaxException e) {
			e.printStackTrace();
		}
	}

	private static Map<DitheringSpec, BufferedImage> produceImages(final BufferedImage originalImage, final Palette palette,
			final ColorDistanceMeasure distanceMeasure, final DitheringSpec[] ditherings, final Limits limits, final IntConsumer progress) {

		final Function<DitheringSpec, BufferedImage> producer = dithering -> limits.producer.produce(originalImage, palette, distanceMeasure, dithering);

		final Map<DitheringSpec, BufferedImage> ditheringToImage = IntStream.range(0, ditherings.length) // attention: parrallel is slower
				.peek(progress::accept)//
				.mapToObj(index -> ditherings[index]).collect(Collectors.toMap(//
						dithering -> dithering, //
						producer::apply, //
						(x, y) -> y, //
						LinkedHashMap::new//
				));
		return ditheringToImage;
	}

	private static JPanel panel(final String headline, final BufferedImage image) {
		if (!EventQueue.isDispatchThread()) { throw new IllegalCallerException("Caller did not call this method on the Event Queue"); }
		final var label = new JLabel(headline);
		label.setFont(new Font("Dialog", Font.BOLD, 14));
		final var imageLabel = new JLabel(new ImageIcon(image));
		final var panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(label, BorderLayout.NORTH);
		panel.add(imageLabel, BorderLayout.CENTER);
		// panel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		return panel;
	}

	static BufferedImage withoutLimits(final BufferedImage original, final Palette palette, final ColorDistanceMeasure colorDistanceMeasure,
			final DitheringSpec dithering) {
		final Function<BufferedImage, BufferedImage> pipeline = new Rescale(C64Bitmap.sizePixels(), () -> Color.BLACK) //
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, dithering))// we now have c64 colors in hires resolution
		;
		System.out.println(dithering);
		return pipeline.apply(original);
	}

	static BufferedImage withKoalaPainterLimits(final BufferedImage original, final Palette palette, final ColorDistanceMeasure colorDistanceMeasure,
			final DitheringSpec dithering) {
		final FindMostCommonColor backgroundProviderC64 = new FindMostCommonColor(Calculate.FIRST_FRAME_AND_REUSE);
		final Function<BufferedImage, BufferedImage> pipeline = new Rescale(C64Bitmap.sizePixels(), () -> Color.BLACK) //
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, dithering))// we now have c64 colors in hires resolution
				.andThen(new Pixelize(new Dimension(2, 1)))// pixelization will create non-c64 colors
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, Dithering.None))// We can't use dithering here as we would get hires pixels
				.andThen(backgroundProviderC64)// find the C64-palette color for the background
				.andThen(new ImageToC64BitmapmodePictureMulticolor(palette, backgroundProviderC64, colorDistanceMeasure)) //
				.andThen(new C64BitmapmodePictureMulticolorToImage(palette))//
		;
		System.out.println(dithering);
		return pipeline.apply(original);
	}

	static BufferedImage withMulticolorFliLimits(final BufferedImage original, final Palette palette, final ColorDistanceMeasure colorDistanceMeasure,
			final DitheringSpec dithering) {
		final FindMostCommonColor backgroundProviderC64 = new FindMostCommonColor(Calculate.FIRST_FRAME_AND_REUSE);
		final Function<BufferedImage, BufferedImage> pipeline = new Rescale(C64Bitmap.sizePixels(), () -> Color.BLACK) //
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, dithering))// we now have c64 colors in hires resolution
				.andThen(new Pixelize(new Dimension(2, 1)))// pixelization will create non-c64 colors
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, Dithering.None))// We can't use dithering here as we would get hires pixels
				.andThen(backgroundProviderC64)// find the C64-palette color for the background
				.andThen(new ImageToC64BitmapmodePictureMulticolorFli(palette, backgroundProviderC64, colorDistanceMeasure)) //
				.andThen(new C64BitmapmodePictureMulticolorFliToImage(palette))//
		;
		System.out.println(dithering);
		return pipeline.apply(original);
	}

	static BufferedImage withHiresFLILimits(final BufferedImage original, final Palette palette, final ColorDistanceMeasure colorDistanceMeasure,
			final DitheringSpec dithering) {
		final Function<BufferedImage, BufferedImage> pipeline = new Rescale(C64Bitmap.sizePixels(), () -> Color.BLACK) //
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, dithering))// we now have c64 colors in hires resolution
				.andThen(new ImageToC64BitmapmodePictureHiresFli(palette, colorDistanceMeasure)) //
				.andThen(new C64BitmapmodePictureHiresFliToImage(palette))//
		;
		System.out.println(dithering);
		return pipeline.apply(original);
	}

	static BufferedImage withHiresLimits(final BufferedImage original, final Palette palette, final ColorDistanceMeasure colorDistanceMeasure,
			final DitheringSpec dithering) {
		final Function<BufferedImage, BufferedImage> pipeline = new Rescale(C64Bitmap.sizePixels(), () -> Color.BLACK) //
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, dithering))// we now have c64 colors in hires resolution
				.andThen(new ImageToC64BitmapmodePictureHires(palette, colorDistanceMeasure)) //
				.andThen(new C64BitmapmodePictureHiresToImage(palette))//
		;
		System.out.println(dithering);
		return pipeline.apply(original);
	}

	interface Producer {
		BufferedImage produce(final BufferedImage originalImage, final Palette palette, final ColorDistanceMeasure distanceMeasure,
				final DitheringSpec dithering);

	}

	enum Limits {
		None(ExampleDitherings::withoutLimits), //
		Koala_Painter(ExampleDitherings::withKoalaPainterLimits), //
		Hires(ExampleDitherings::withHiresLimits), //
		Multicolor_FLI(ExampleDitherings::withMulticolorFliLimits), //
		Hires_FLI(ExampleDitherings::withHiresFLILimits)//
		;

		private final Producer producer;

		Limits(final Producer producer) {
			this.producer = producer;
		}

		@Override
		public String toString() {
			return name().replaceAll("_", " ").toString();
		}
	}
}
