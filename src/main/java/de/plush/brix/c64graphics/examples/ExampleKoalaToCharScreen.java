package de.plush.brix.c64graphics.examples;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.*;
import java.nio.file.*;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.model.C64BitmapmodePictureMulticolor;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.CharmodeColorClashResolverMulticolor;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.*;
import de.plush.brix.c64graphics.core.util.StartAddress;
import de.plush.brix.c64graphics.core.util.UtilsImage.ImageFormat;

/** Main class used for starting the conversion. */
public class ExampleKoalaToCharScreen {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	public static void main(final String[] args) throws URISyntaxException, IOException {

		final String picName = "lost boys.kla";
		final URI uri = ExampleKoalaToCharScreen.class.getResource("resources").toURI();
		final URI imageFile = Paths.get(uri).resolve(picName).toUri();
		Files.createDirectories(Paths.get(outDir));

		final Palette palette = C64ColorsColodore.PALETTE;
		final ColorDistance colorDistanceMeasure = ColorDistance.EUCLIDEAN_WEIGHTED;

		@SuppressWarnings("resource")
		final Function<C64BitmapmodePictureMulticolor, BufferedImage> pipeline = //
				new C64BitmapmodePictureMulticolorToImage(C64ColorsColodore.PALETTE)//
						.andThen(new ImageDisplay<>(2, "original", Grid.Invisible))//
						.andThen(new ImageToC64CharmodePictureMulticolor(palette, colorDistanceMeasure, CharacterReduction.UsingImageColorsOnly,
								new CharmodeColorClashResolverMulticolor(palette, colorDistanceMeasure)//
										.withForcedBackgroundColor(Color.BLACK))) //
						// .andThen(new BlockwiseRoll<>(Right.by(1), Up.by(8))) //
						.andThen(new WriteC64BinaryToFile<>(n -> 0x2000, n -> outDir + picName.replaceAll("\\.koa", "") + ".charScreen")) //
						// .andThen(new C64BinaryProviderToBinary()) //
						// .andThen(binary -> C64CharsetScreenColorRam.fromC64Binary(binary))//
						.andThen(new C64CharmodePictureToImageMulticolor(palette)) //
						.andThen(new WriteImageToFile<>(ImageFormat.PNG, n -> outDir + picName.replaceAll("\\.koa", "") + ".koa.png"))
						.andThen(new ImageDisplay<>(2, "as char+screen -> png", Grid.Invisible));

		pipeline.apply(C64BitmapmodePictureMulticolor.loadKoala(imageFile, StartAddress.InFile));

	}

}
