package de.plush.brix.c64graphics.examples;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces.ColorConversions.convertRGBtoYUV;
import static java.util.Arrays.asList;
import static java.util.Comparator.comparingDouble;
import static java.util.stream.Collectors.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.util.Utils;

public class ExampleColorDistanceMeasures2 {

	protected ExampleColorDistanceMeasures2() {
		throw new UnsupportedOperationException();
	}

	private static Map<Set<Color>, Double> distances(final Set<Set<Color>> colorPairs, final ColorDistanceMeasure measure) {
		return colorPairs.stream().map(colorPair -> {
			final var it = colorPair.iterator();
			final Color a = it.next();
			final Color b = it.next();
			return Map.entry(colorPair, measure.distanceOf(a, b));
		}).sorted((e1, e2) -> Double.compare(e1.getValue(), e2.getValue()))//
				.collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (x, y) -> y, LinkedHashMap::new));
	}

	public static BufferedImage image(final Palette palette, final ColorDistanceMeasure measure) {
		final var colorSet = new LinkedHashSet<>(asList(palette.colors()));
		final Set<Set<Color>> allColorPairs = Utils.subsetStream(colorSet, 2, //
				n -> new TreeSet<>(comparingDouble(c -> convertRGBtoYUV(c).Y())))// sorting darker color of color pair tops
				.collect(toCollection(LinkedHashSet::new));

		final Map<Set<Color>, Double> distances = distances(allColorPairs, measure);

		final var top = 30;
		final var bottom = 10;
		final var magY = 20;
		final var magX = 10;
		final var gapX = 3;
		final var gapY = 1;
		final var img = new BufferedImage(gapX + distances.size() * (magX + gapX), top + 2 * magY + gapY + bottom, BufferedImage.TYPE_INT_ARGB);
		final var g = img.createGraphics();
		try {
			g.setColor(Color.black);
			g.setFont(new Font("Dialog", Font.PLAIN, 18));
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.drawString(ColorDistance.name(measure), 0, 20);

			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
			int t = -1;
			for (final var set : distances.keySet()) {
				++t;
				final var pair = set.toArray(Color[]::new);

				final Color c1 = pair[0];
				final Color c2 = pair[1];

				g.setColor(c1);
				g.fillRect(gapX + t * (magX + gapX), top + 0 * magY, magX, magY);
				g.setColor(c2);
				g.fillRect(gapX + t * (magX + gapX), top + 1 * magY + gapY, magX, magY);
			}
		} finally {
			g.dispose();
		}
		return img;
	}

	@SuppressWarnings("resource")
	public static void show(final Palette palette, final ColorDistanceMeasure... measures) {

		final var images = Arrays.stream(measures).map(m -> image(palette, m)).collect(toList());

		final int width = images.stream().mapToInt(BufferedImage::getWidth).max().orElseThrow();
		final int height = images.stream().mapToInt(BufferedImage::getHeight).sum();

		final var overview = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		final var g = overview.createGraphics();
		try {
			IntStream.range(0, images.size())//
					.forEach(index -> g.drawImage(images.get(index), 0, index * height / images.size(), null));
		} finally {
			g.dispose();
		}
		new ImageDisplay<>().show(overview, "");
	}

	/** Can be used to display where two measures differ. */
	@SuppressWarnings("resource")
	public static void showDiff(final Palette palette, final ColorDistanceMeasure measureLeft, final ColorDistanceMeasure measureRight) {

		final String title = ColorDistance.name(measureLeft) + " - " + ColorDistance.name(measureRight);

		final var colorSet = new LinkedHashSet<>(asList(palette.colors()));

		final Set<Set<Color>> allColorPairs = Utils.subsetStream(colorSet, 2, //
				n -> new TreeSet<>(comparingDouble(c -> convertRGBtoYUV(c).Y())))// sorting darker color of color pair left
				.collect(toCollection(LinkedHashSet::new));

		final Map<Set<Color>, Double> distancesLeft = distances(allColorPairs, measureLeft);
		final Map<Set<Color>, Double> distancesRight = distances(allColorPairs, measureRight);

		System.out.println("left:  " + distancesLeft);
		System.out.println("right: " + distancesRight);
		System.out.println("---");

		System.out.println("left: min = " + distancesLeft.values().stream().min(Double::compare));
		System.out.println("left: max = " + distancesLeft.values().stream().max(Double::compare));

		System.out.println("right: min = " + distancesRight.values().stream().min(Double::compare));
		System.out.println("right: max = " + distancesRight.values().stream().max(Double::compare));

		System.out.println("---");
		System.out.println("diff -> distance(" + ColorDistance.name(measureLeft) + ") - distance(" + ColorDistance.name(measureRight) + ")");
		final Map<Set<Color>, Double> diff = distancesLeft.entrySet().stream()//
				.map(e -> Map.entry(e.getKey(), e.getValue() - distancesRight.get(e.getKey())))//
				.sorted((e1, e2) -> Double.compare(e1.getValue(), e2.getValue()))//
				.collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (x, y) -> y, LinkedHashMap::new));
		diff.entrySet().stream()//
				.forEach((e) -> System.out.println(e.getKey() + "\t\t: " + e.getValue()));

		final var magY = 8;
		final var gapY = 1;
		final var magX = 20;
		final var gapX = 3;
		final var marker = 20;
		final var img = new BufferedImage(4 * magX + 3 * gapX + marker, 120 * (magY + gapY), BufferedImage.TYPE_INT_ARGB);
		final var g = img.createGraphics();
		try {

			final var itrLeft = distancesLeft.keySet().iterator();
			final var itrRight = distancesRight.keySet().iterator();
			int t = -1;
			while (itrLeft.hasNext()) {
				++t;
				final var left = itrLeft.next();
				final var right = itrRight.next();
				final var leftArr = left.toArray(Color[]::new);
				final var rightArr = right.toArray(Color[]::new);

				final Color l1 = leftArr[0];
				final Color l2 = leftArr[1];
				final Color r1 = rightArr[0];
				final Color r2 = rightArr[1];

				g.setColor(l1);
				g.fillRect(0 * magX, t * (magY + gapY), magX, magY);
				g.setColor(l2);
				g.fillRect(1 * magX, t * (magY + gapY), magX, magY);

				g.setColor(r1);
				g.fillRect(2 * magX + gapX, t * (magY + gapY), magX, magY);
				g.setColor(r2);
				g.fillRect(3 * magX + gapX, t * (magY + gapY), magX, magY);

				if (!left.equals(right)) {
					g.setColor(Color.RED);
					g.fillRect(4 * magX + 2 * gapX, t * (magY + gapY) + 1, marker, magY - 2);
				}

			}
		} finally {
			g.dispose();
		}
		new ImageDisplay<>().show(img, "similarities " + title);
	}

	public static void main(final String[] args) {
		// showDiff(C64ColorsColodore.PALETTE, ColorDistance.CMC_lc_11, ColorDistance.CMC_lc_21);
		// showDiff(C64ColorsColodore.PALETTE, ColorDistance.DIN99b, ColorDistance.DIN99o);
		show(C64ColorsColodore.PALETTE, ColorDistance.values());
	}
}
