package de.plush.brix.c64graphics.examples;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URI;
import java.nio.file.*;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.emitters.GifEmitter;
import de.plush.brix.c64graphics.core.model.C64CharsetScreen;
import de.plush.brix.c64graphics.core.pipeline.PipelineConsumer;
import de.plush.brix.c64graphics.core.pipeline.displays.*;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.FindMostCommonColor.Calculate;

/** Main class used for starting the conversion. */
public class ExampleGifToC64 {

	final static String outDir;
	static {
		final String tempDir = System.getProperty("java.io.tmpdir");
		outDir = tempDir + "c64graphics/";
	}

	@SuppressWarnings("resource")
	public static void main(final String[] args) throws Exception {

		final String picName = "ragga-dance.gif";
		final URI uri = ExampleGifToC64.class.getResource("resources").toURI();
		final File imageFile = Paths.get(uri).resolve(picName).toFile();
		Files.createDirectories(Paths.get(outDir));

		final Palette palette = () -> new Color[] { BLACK.color, WHITE.color };
		final ColorDistance colorDistanceMeasure = ColorDistance.EUCLIDEAN_WEIGHTED;
		final Dithering dithering = Dithering.Atkinson;

		final FindMostCommonColor backgroundProviderPC = new FindMostCommonColor(Calculate.FIRST_FRAME_AND_REUSE);

		final Function<BufferedImage, ?> pipeline = backgroundProviderPC// get most common color
				.andThen(new Rescale(C64CharsetScreen.sizePixels(), backgroundProviderPC))// using color finder to fill frame when resizing
				.andThen(new ImageDisplay<>(2f, "scaled", Grid.Invisible))//
				.andThen(new PaletteConversion(palette, colorDistanceMeasure, dithering)) //
				.andThen(new ImageDisplay<>(2f, "palette applied", Grid.sizeC64Char))//
				.andThen(new ImageToC64BitmapHires(() -> WHITE.color, BLACK.color)) //
				.andThen(new C64BitmapToC64CharsetScreenHires(colorDistanceMeasure))//
				.andThen(new Display<>(//
						new C64CharsetScreenToImageHires(palette, () -> WHITE.color, BLACK.color), //
						new ImageDisplay<>(2f, "converted", Grid.sizeC64Char)))//
				.andThen(pic -> pic.binaryProviderCharsetFirst())//
				.andThen(new WriteC64BinaryToFile<>(n -> 0x2000, n -> outDir + n + "charscreen_" + picName.replaceAll("\\.jpg", "") + ".cs"))//
		// .andThen(new Wait<>(1000, TimeUnit.MILLISECONDS))//
		;

		try (final var consumer = new PipelineConsumer<>(pipeline); //
				final var emitter = new GifEmitter(imageFile);) {
			emitter.add(consumer);
			emitter.run();
		}
		System.exit(0);
	}

}
