package de.plush.brix.c64graphics.examples;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.swing.*;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.ColorDistance;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;

public class ExampleQuantizations {

	private static final Dimension targetSizePixels = new Dimension(464, 290);

	public static void main(final String[] args) throws URISyntaxException, IOException {
		// final var picName = "no_copy_g2_04.jpg";
		final var picName = "paintface.jpg";
		final URI uri = ExampleQuantizations.class.getResource("resources").toURI();
		final File imageFile = Paths.get(uri).resolve(picName).toFile();

		show(imageFile, //
				new Quantize(16).withNeuQuantQuantization().withColorDistanceMeasure(ColorDistance.DIN99o), //
				new Quantize(16).withMedianCutQuantization().withSubSampleRates(4, 4), //
				new Quantize(16).withOctreeQuantization() //
		);
	}

	private static void show(final File imageFile, final Quantize... quantizers) throws IOException {
		final var originalImage = ImageIO.read(imageFile);
		final Map<Quantize, BufferedImage> ditheringToImage = Arrays.<Quantize> stream(quantizers).parallel().collect(Collectors.toMap(//
				quantizer -> quantizer, //
				quantizer -> quantized(originalImage, quantizer), //
				(x, y) -> y, //
				LinkedHashMap::new//
		));
		EventQueue.invokeLater(() -> {
			final JFrame frame = new JFrame("overview");
			frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			frame.getContentPane().setLayout(new GridLayout(2, 2));
			frame.add(panel("Original", new Rescale(targetSizePixels, () -> Color.black).apply(originalImage)));
			ditheringToImage.forEach((quantizer, image) -> frame.add(panel(quantizer.getClass().getSimpleName(), image)));
			frame.pack();
			frame.setVisible(true);
		});

	}

	private static JPanel panel(final String headline, final BufferedImage image) {
		if (!EventQueue.isDispatchThread()) { throw new IllegalCallerException("Caller did not call this method on the Event Queue"); }
		final var label = new JLabel(headline);
		label.setFont(new Font("Dialog", Font.PLAIN, 18));
		final var imageLabel = new JLabel(new ImageIcon(image));
		final var panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(label, BorderLayout.NORTH);
		panel.add(imageLabel, BorderLayout.CENTER);
		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		return panel;
	}

	private static BufferedImage quantized(final BufferedImage original, final Quantize quantizer) {
		final Supplier<Color> backgroundProviderPC = () -> Color.BLACK;
		final Function<BufferedImage, BufferedImage> pipeline = new Rescale(targetSizePixels, backgroundProviderPC) //
				.andThen(quantizer);// we now have c64 colors in hires resolution
		return pipeline.apply(original);
	}

}
