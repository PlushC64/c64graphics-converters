package de.plush.brix.c64graphics.core.util.collections;

import java.util.*;

public class LRUMap<K, V> extends LinkedHashMap<K, V> {

	private final int maxSize;

	public LRUMap(final int maxSize) {
		this.maxSize = maxSize;

	}

	@Override
	@SuppressWarnings("unused")
	protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
		return size() >= maxSize;
	}
}
