
package de.plush.brix.c64graphics.core.util;

public class IntIntPair implements Comparable<IntIntPair> {

	private final int one;

	private final int two;

	IntIntPair(final int one, final int two) {
		this.one = one;
		this.two = two;
	}

	public static IntIntPair pair(final int one, final int two) {
		return new IntIntPair(one, two);
	}

	/** same as "pair", may read better depending on the context */
	public static IntIntPair of(final int one, final int two) {
		return new IntIntPair(one, two);
	}

	public int getOne() {
		return one;
	}

	public int getTwo() {
		return two;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + one;
		result = prime * result + two;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final IntIntPair other = (IntIntPair) obj;
		if (one != other.one) { return false; }
		if (two != other.two) { return false; }
		return true;
	}

	@Override
	public String toString() {
		return one + ":" + two;
	}

	@Override
	public int compareTo(final IntIntPair that) {
		final int i = one < that.getOne() ? -1 : one > that.getOne() ? 1 : 0;
		if (i != 0) { return i; }
		return two < that.getTwo() ? -1 : two > that.getTwo() ? 1 : 0;
	}
}
