package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;

import java.awt.*;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.Palette;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.C64CharToImageMulticolor.Mode;

public class C64BitmapmodePictureMulticolorHalfcharFliToImage
		implements IPipelineStage<C64BitmapmodePictureMulticolorHalfcharFli, PreProcessedMulticolorFliImage> {

	private final Palette palette;

	private final int partialImageHeight;

	public C64BitmapmodePictureMulticolorHalfcharFliToImage(final Palette palette) {
		this.palette = palette;
		partialImageHeight = C64Char.HEIGHT_PIXELS / 2;
	}

	@Override
	public PreProcessedMulticolorFliImage apply(final C64BitmapmodePictureMulticolorHalfcharFli c64MulticolorHalfcharFli) {
		final byte backgroundColorCode = c64MulticolorHalfcharFli.backgroundColorCode();
		final Color background = palette.color(toUnsignedByte(backgroundColorCode & 0x0F));
		final PreProcessedMulticolorFliImage result = new PreProcessedMulticolorFliImage(C64Bitmap.WIDTH_PIXELS, C64Bitmap.HEIGHT_PIXELS,
				BufferedImage.TYPE_INT_RGB, backgroundColorCode);
		final Graphics2D g = result.createGraphics();
		try {
			final C64Bitmap bitmap = c64MulticolorHalfcharFli.bitmap();
			final C64Screen[] screens = c64MulticolorHalfcharFli.screens();
			final C64ColorRam colorRam = c64MulticolorHalfcharFli.colorRam();

			for (int yBlock = 0; yBlock < C64Bitmap.HEIGHT_BLOCKS; ++yBlock) {
				for (int xBlock = 0; xBlock < C64Bitmap.WIDTH_BLOCKS; ++xBlock) {
					for (int lineStart = 0; lineStart < C64Char.HEIGHT_PIXELS; lineStart += partialImageHeight) {
						final Point blockPos = new Point(xBlock, yBlock);
						final BufferedImage blockImage = blockImage(blockPos, bitmap, screens[lineStart / partialImageHeight], colorRam, background);
						final BufferedImage partialImage = blockImage.getSubimage(0, lineStart, C64Char.WIDTH_PIXELS, partialImageHeight);
						g.drawImage(partialImage, blockPos.x * C64Char.WIDTH_PIXELS, blockPos.y * C64Char.HEIGHT_PIXELS + lineStart, null);
					}
				}
			}
		} finally {
			g.dispose();
		}
		return result;
	}

	public BufferedImage blockImage(final Point blockPos, final C64Bitmap bitmap, final C64Screen screen, final C64ColorRam colorRam, final Color background) {
		final byte screenCode = screen.screenCodeAt(blockPos.x, blockPos.y);
		final Color mc1 = palette.color(toUnsignedByte(screenCode >>> 4 & 0x0F));
		final Color mc2 = palette.color(toUnsignedByte(screenCode & 0x0F));
		final Color cram = palette.color(toUnsignedByte(colorRam.colorCodeAt(blockPos.x, blockPos.y) & 0x0F));
		final C64Char block = bitmap.blockAt(blockPos.x, blockPos.y);
		return new C64CharToImageMulticolor(palette, () -> background, mc1, mc2, cram, Mode.Bitmap).apply(block);
	}

}
