/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.image.BufferedImage;
import java.util.function.IntPredicate;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class ColumnDoublerEffect implements IPipelineStage<BufferedImage, BufferedImage> {

	private final IntPredicate columnDoublingTrigger;

	public ColumnDoublerEffect() {
		this(x -> x % 2 == 0);
	}

	public ColumnDoublerEffect(final IntPredicate columnDoublingTrigger) {
		this.columnDoublingTrigger = columnDoublingTrigger;
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		for (int x = 0; x < result.getWidth(); ++x) {
			copyColumn(original, result, x, x);
			if (columnDoublingTrigger.test(x)) {
				copyColumn(original, result, x, x + 1);
				++x;
			}

		}
		return result;
	}

	private static void copyColumn(final BufferedImage original, final BufferedImage result, final int x, final int x2) {
		for (int y = 0; y < result.getHeight(); ++y) {
			final int rgb = original.getRGB(x, y);
			result.setRGB(x2, y, rgb);
		}
	}
}