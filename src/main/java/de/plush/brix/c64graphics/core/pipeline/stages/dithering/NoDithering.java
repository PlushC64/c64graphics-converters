package de.plush.brix.c64graphics.core.pipeline.stages.dithering;

import static java.util.Arrays.stream;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Map;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.util.collections.LRUMap;

final class NoDithering implements IPipelineStage<BufferedImage, BufferedImage> {

	final Map<Color, Color> closestColorsCache = new LRUMap<>(1024 * 64);

	private final Function<Color, Color> computeClosestColorFunction;

	public NoDithering(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure) {
		final var targetPaletteColors = palette.colors();
		computeClosestColorFunction = originalColor -> stream(targetPaletteColors)//
				.min(colorDistanceMeasure.comparingDistanceTo(originalColor))//
				.get();
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());
		final Graphics2D g = (Graphics2D) result.getGraphics();
		try {
			// parallel processing did not cut the cheese. if any, it was marginally slower with more memory overhead.
			for (int y = 0; y < original.getHeight(); ++y) {
				for (int x = 0; x < original.getWidth(); ++x) {
					final Color originalColor = new Color(original.getRGB(x, y));
					final Color newColor = clostestColorTo(originalColor);
					result.setRGB(x, y, newColor.getRGB());
				}
			}
		} finally {
			g.dispose();
		}
		return result;
	}

	private Color clostestColorTo(final Color originalColor) {
		return closestColorsCache.computeIfAbsent(originalColor, computeClosestColorFunction);
	}

}