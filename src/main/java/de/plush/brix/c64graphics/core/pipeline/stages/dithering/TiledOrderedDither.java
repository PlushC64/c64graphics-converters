package de.plush.brix.c64graphics.core.pipeline.stages.dithering;

import static de.plush.brix.c64graphics.core.util.UtilExceptions.uncheckedSupplier;
import static java.lang.Math.min;
import static java.lang.Runtime.getRuntime;
import static java.lang.Thread.currentThread;
import static java.util.Arrays.stream;
import static java.util.concurrent.Executors.newFixedThreadPool;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.util.collections.LRUMap;

final class TiledOrderedDither implements IPipelineStage<BufferedImage, BufferedImage>, DitheringStrengthSupport {

	public static final double DEFAULT_DITHERING_STRENGTH = 0.33;

	private ExecutorService threadPool;

	private final Object threadPoolLock = new Object();

	private volatile boolean poolBusy;

	private final DitheringSpec ditheringSpec;

	private final ColorDistanceMeasure colorDistanceMeasure;

	private final Palette palette;

	private final double ditheringStrength;

	private final Function<Color, Color> computeClosestColorFunction;

	private final ConcurrentHashMap<Thread, Map<Color, Color>> caches = new ConcurrentHashMap<>(getRuntime().availableProcessors() * 2 + 1, 0.75f, getRuntime()
			.availableProcessors());

	TiledOrderedDither(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure, final DitheringSpec ditheringSpec) {
		this(palette, colorDistanceMeasure, ditheringSpec, DEFAULT_DITHERING_STRENGTH);
	}

	TiledOrderedDither(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure, final DitheringSpec ditheringSpec,
			final double ditheringStrength) {
		this.palette = palette;
		this.colorDistanceMeasure = colorDistanceMeasure;
		this.ditheringSpec = ditheringSpec;
		this.ditheringStrength = ditheringStrength;
		final var targetPaletteColors = palette.colors();
		computeClosestColorFunction = originalColor -> stream(targetPaletteColors)//
				.min(colorDistanceMeasure.comparingDistanceTo(originalColor))//
				.get();
	}

	/**
	 * @param ditheringStrength
	 *            should be a value between 0.1 and 1.0, values out of bounds may unexpected effects.
	 *            Default: {@value TiledOrderedDither#DEFAULT_DITHERING_STRENGTH}
	 * @return a new instance with set dithering strength.
	 */
	@Override
	public TiledOrderedDither withDitheringStrength(final double ditheringStrength) {
		return new TiledOrderedDither(palette, colorDistanceMeasure, ditheringSpec, ditheringStrength);
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());
		final Graphics2D g = (Graphics2D) result.getGraphics();
		try {
			poolBusy = true;
			initThreadPool();
			// ordered dithering does not work "in place", so original colors don't need to be in the result image prior to error distribution
			final List<Future<?>> futures = new ArrayList<>();
			final Dimension matrixSize = ditheringSpec.matrixSize();
			for (int yStart = 0; yStart < original.getHeight(); yStart += matrixSize.height) {
				for (int xStart = 0; xStart < original.getWidth(); xStart += matrixSize.width) {
					final Point start = new Point(xStart, yStart);
					futures.add(threadPool.submit(fixedBlockOrderedDithering(original, result, start)));
				}
			}
			futures.stream().forEach(future -> uncheckedSupplier(future::get).get());
		} finally {
			g.dispose();
			poolBusy = false;
		}
		return result;
	}

	private Runnable fixedBlockOrderedDithering(final BufferedImage original, final BufferedImage result, final Point globalStart) {
		return () -> {
			final Dimension matrixSize = ditheringSpec.matrixSize();
			for (int y = 0; y < matrixSize.getHeight(); ++y) {
				for (int x = 0; x < matrixSize.getWidth(); ++x) {
					final Point global = new Point(globalStart.x + x, globalStart.y + y);
					final Point matrix = new Point(x, y);
					final OptionalDouble errorMultiplier = ditheringSpec.errorMultiplier(matrix, global);
					if (errorMultiplier.isPresent() //
							&& !outOfBounds(original.getWidth(), 0, global.x) //
							&& !outOfBounds(original.getHeight(), 0, global.y)) {

						// see: https://en.wikipedia.org/wiki/Ordered_dithering
						final Color current = new Color(original.getRGB(global.x, global.y));
						final double weight = errorMultiplier.getAsDouble();
						final int newRed = min(0xFF, Math.max(0, (int) (current.getRed() + ditheringStrength * current.getRed() * weight)));
						final int newGreen = min(0xFF, Math.max(0, (int) (current.getGreen() + ditheringStrength * current.getGreen() * weight)));
						final int newBlue = min(0xFF, Math.max(0, (int) (current.getBlue() + ditheringStrength * current.getBlue() * weight)));
						final Color newColor = clostestColorTo(new Color(newRed, newGreen, newBlue));
						result.setRGB(global.x, global.y, newColor.getRGB());
					}
				}
			}
		};
	}

	static boolean outOfBounds(final int max, final int center, final int offset) {
		return center + offset < 0 || center + offset > max - 1;
	}

	/*
	 * Note: I tried a common cache and a synchronized block, that was extremely slow a common cache guarded by a spin lock was also significantly slower
	 * than
	 * using one cache for each thread.
	 * Having no cache at all was slightly better for multitreaded dithering on a rich color set, but significantly harms straight application of a palette
	 * on
	 * an already reduced palette.
	 */
	private Color clostestColorTo(final Color originalColor) {
		final Map<Color, Color> closestColorsCache = caches.computeIfAbsent(currentThread(), thread -> newCache());
		return closestColorsCache.computeIfAbsent(originalColor, computeClosestColorFunction);
	}

	private static LinkedHashMap<Color, Color> newCache() {
		return new LRUMap<>(1024 * 64); // cache most frequently used 64.000 colors:
	}

	/**
	 * Create a thread pool that shuts itself down after some idle time (shutdown-hook will not fire while this pool's threads are running!). Just returns
	 * the last thread pool if it's not yet shut down.
	 */
	private void initThreadPool() {
		synchronized (threadPoolLock) {
			if (threadPool == null || threadPool.isTerminated()) {
				threadPool = newFixedThreadPool(getRuntime().availableProcessors() + 1);
				getRuntime().addShutdownHook(new Thread(threadPool::shutdownNow));
				threadPool.submit(() -> {
					synchronized (threadPoolLock) {
						while (poolBusy && !Thread.currentThread().isInterrupted()) {
							try {
								TimeUnit.SECONDS.timedWait(threadPoolLock, 5);
							} catch (final InterruptedException e) {
								// we don't care if the shutter is shut while it's waiting to shut
							}
						}
						threadPool.shutdown();
						// Important: pool.isTerminated() does not suffice, it may not be terminated while this operation is still running.
						threadPool = null;
					}
				});
			}
		}
	}

}