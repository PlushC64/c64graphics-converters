package de.plush.brix.c64graphics.core._experimental.wavelet;

import static de.plush.brix.c64graphics.core._experimental.wavelet.ColorChannel.*;
import static java.util.stream.Stream.of;

import java.awt.Dimension;
import java.awt.image.*;
import java.util.function.*;
import java.util.stream.IntStream;

public class HaarWaveletImage {

	private final MonochromeHaarWaveletImage alpha;

	private final MonochromeHaarWaveletImage red;

	private final MonochromeHaarWaveletImage green;

	private final MonochromeHaarWaveletImage blue;

	public HaarWaveletImage(final BufferedImage rbgaImage) {
		alpha = monochromeHaarWaveletImageFrom(rbgaImage, ColorChannel.Alpha);
		red = monochromeHaarWaveletImageFrom(rbgaImage, ColorChannel.Red);
		green = monochromeHaarWaveletImageFrom(rbgaImage, ColorChannel.Green);
		blue = monochromeHaarWaveletImageFrom(rbgaImage, ColorChannel.Blue);
	}

	public void forEachCoefficient(final IntPredicate levelMatcher, final DoubleBinaryOperator modifier) {
		of(Alpha, Red, Green, Blue).forEach(channel -> forEachCoefficient(channel, levelMatcher, modifier));
	}

	public void forEachCoefficient(final ColorChannel channel, final IntPredicate levelMatcher, final DoubleBinaryOperator modifier) {
		waveletImageForChannel(channel).forEachCoefficient(levelMatcher, modifier);
	}

	public BufferedImage toImage() {
		final var dim = dimension();
		final var combiner = new ChannelCombiner(alpha.toImage(), red.toImage(), green.toImage(), blue.toImage());
		final var image = new BufferedImage(dim.width, dim.height, BufferedImage.TYPE_INT_ARGB);
		IntStream.range(0, dim.width * dim.height).parallel()//
				.forEach(index -> { final int x = index % dim.width, y = index / dim.width; image.setRGB(x, y, combiner.rgb(x, y)); });
		return image;
	}

	public BufferedImage imageInterpretationOfWavelets(final ColorChannel channel) {
		return waveletImageForChannel(channel).imageInterpretation();
	}

	/**
	 * @param level
	 *            a number between 0 and {@link #levels()}, where 0 shows the first iteration of wavelets transformation as image, {@link #levels()} shows
	 *            all
	 *            iterations. -1 shows no iteration (i.e. the original image of said channel as grayscale).
	 */
	public BufferedImage imageInterpretationOfWavelets(final ColorChannel channel, final int level) {
		return waveletImageForChannel(channel).imageInterpretation(level);
	}

	public Dimension dimension() {
		return new Dimension(red.dimension()); // all channels have the same dimensions
	}

	public int levels() {
		return red.levels(); // all channels have the same amount of levels
	}

	private MonochromeHaarWaveletImage waveletImageForChannel(final ColorChannel channel) {
		switch (channel) {
		case Alpha:
			return alpha;
		case Red:
			return red;
		case Green:
			return green;
		case Blue:
			return blue;
		default:
			throw new IllegalArgumentException("unsupported channel " + channel);
		}
	}

	private static MonochromeHaarWaveletImage monochromeHaarWaveletImageFrom(final BufferedImage rgbaImage, final ColorChannel channel) {
		final var dim = new Dimension(rgbaImage.getWidth(), rgbaImage.getHeight());
		final var data = new double[dim.width][dim.height];
		for (int y = 0; y < dim.height; ++y) {
			for (int x = 0; x < dim.width; ++x) {
				final int value = channel.fromRgb(rgbaImage.getRGB(x, y));
				data[x][y] = value;
			}
		}
		return new MonochromeHaarWaveletImage(data);
	}

	private static class ChannelCombiner {

		private final WritableRaster alpha;

		private final WritableRaster red;

		private final WritableRaster green;

		private final WritableRaster blue;

		ChannelCombiner(final BufferedImage alpha, final BufferedImage red, final BufferedImage green, final BufferedImage blue) {
			checkType(alpha, "alpha");
			checkType(red, "red");
			checkType(green, "green");
			checkType(blue, "blue");
			this.alpha = alpha.getRaster();
			this.red = red.getRaster();
			this.green = green.getRaster();
			this.blue = blue.getRaster();
		}

		int rgb(final int x, final int y) {
			final var pix = new int[1];
			final int a = ColorChannel.Alpha.toRgb(alpha.getPixel(x, y, pix)[0]);
			final int r = ColorChannel.Red.toRgb(red.getPixel(x, y, pix)[0]);
			final int g = ColorChannel.Green.toRgb(green.getPixel(x, y, pix)[0]);
			final int b = ColorChannel.Blue.toRgb(blue.getPixel(x, y, pix)[0]);
			return a | r | g | b;
		}

		private static void checkType(final BufferedImage image, final String name) {
			if (image.getType() != BufferedImage.TYPE_BYTE_GRAY) {
				throw new IllegalArgumentException(name + " image of not type BufferedImage.TYPE_BYTE_GRAY");
			}
		}
	}

	// public static BufferedImage toBufferedImage(final Image img) {
	// if (img instanceof BufferedImage) { return (BufferedImage) img; }
	// final BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
	// final Graphics2D bGr = bimage.createGraphics();
	// bGr.drawImage(img, 0, 0, null);
	// bGr.dispose();
	// return bimage;
	// }
	//
	//
	// /** * convert a BufferedImage to RGB colourspace */
	// public static BufferedImage convertColorspace(final BufferedImage image, final int newType) {
	// final BufferedImage raw_image = image;
	// final var converted = new BufferedImage(raw_image.getWidth(), raw_image.getHeight(), newType);
	// final var hints = new RenderingHints(Map.of(//
	// RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY//
	// , RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY//
	// , RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE//
	// , RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF//
	// ));
	// final ColorConvertOp transform = new ColorConvertOp(hints);
	// transform.filter(raw_image, image);
	// return converted;
	// }
}
