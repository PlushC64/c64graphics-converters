package de.plush.brix.c64graphics.core.pipeline.stages.misc;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.*;

import de.plush.brix.c64graphics.core.model.C64BinaryProvider;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.BinaryToC64BinaryProvider;
import de.plush.brix.c64graphics.core.util.Utils;

/**
 * Writes a binary, provided by a {@link C64BinaryProvider} such as a {@link BinaryToC64BinaryProvider} to a file.<br>
 * Objects of this class hold a counter that counts the number of calls of this pipeline stage. This counter is the input to functions for generating the load
 * address or a file name of each file written. These functions can be provided in the different constructors of this class. See also
 * {@linkplain #WriteC64BinaryToFile(IntUnaryOperator, IntFunction)}. These function's parameter value (the counter) starts with <tt>0</tt> and increases with
 * every call to @link {@link #apply(C64BinaryProvider)}.
 *
 * @author Wanja Gayk
 *
 * @param <T>
 *            the type of object run through this pipe (must be a {@link C64BinaryProvider} implementation).
 */
public class WriteC64BinaryToFile<T extends C64BinaryProvider> implements Function<T, T> {

	private final AtomicInteger number = new AtomicInteger();

	private final Optional<IntUnaryOperator> loadAddressGenerator;

	private final IntFunction<String> fileNameGenerator;

	/**
	 * Using this constructor the binary data will be written without any load address.
	 *
	 * @param fileNameGenerator
	 *            a function that maps the current binary provider's number to a path. <br/>
	 *            The function's parameter value starts with <tt>0</tt> and increases with every call to @link {@link #apply(C64BinaryProvider)}.
	 */
	public WriteC64BinaryToFile(final IntFunction<String> fileNameGenerator) {
		this.loadAddressGenerator = Optional.empty();
		this.fileNameGenerator = fileNameGenerator;
	}

	/**
	 * Using this constructor the binary data will be written, including a load address, which is the same for every file.
	 *
	 * @param loadAddress
	 *            provides a 16 bit value denoting the load address. It will prefix the written binary in Little-Endian format, as common on the C64.
	 * @param fileNameGenerator
	 *            a function that maps the current binary provider's number to a path. <br/>
	 *            The function's parameter value starts with <tt>0</tt> and increases with every call to @link {@link #apply(C64BinaryProvider)}.
	 */
	public WriteC64BinaryToFile(final int loadAddress, final IntFunction<String> fileNameGenerator) {
		this(n -> loadAddress, fileNameGenerator);
	}

	/**
	 * Using this constructor the binary data will be written, including a load address.
	 *
	 * @param loadAddressGenerator
	 *            generates from the current binary provider's number a 16 bit value denoting the load address. It will prefix the written binary in
	 *            Little-Endian format, as common on the C64. The function's parameter value starts with <tt>0</tt> and increases with every call to @link
	 *            {@link #apply(C64BinaryProvider)}.
	 * @param fileNameGenerator
	 *            a function that maps the current binary provider's number to a path. <br/>
	 *            The function's parameter value starts with <tt>0</tt> and increases with every call to @link {@link #apply(C64BinaryProvider)}.
	 */
	public WriteC64BinaryToFile(final IntUnaryOperator loadAddressGenerator, final IntFunction<String> fileNameGenerator) {
		this.loadAddressGenerator = Optional.of(loadAddressGenerator);
		this.fileNameGenerator = fileNameGenerator;
	}

	/**
	 * Writes the file and returns the parameter value.
	 */
	@Override
	public T apply(final T binaryProvider) {
		final int n = number.getAndIncrement();
		final String filePath = fileNameGenerator.apply(n);
		final byte[] c64Binary = binaryProvider.toC64Binary();
		if (loadAddressGenerator.isPresent()) {
			Utils.writeFile(filePath, c64Binary, loadAddressGenerator.get().applyAsInt(n));
		} else {
			Utils.writeFile(filePath, c64Binary);
		}
		return binaryProvider;
	}

	/**
	 * Resets the image counter to 0.
	 */
	public void resetNumber() {
		while (number.compareAndSet(number.get(), 0)) {
			Thread.yield();
		}
	}
}
