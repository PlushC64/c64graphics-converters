package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;

import java.awt.*;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.Palette;

public class C64BitmapmodePictureHiresFliToImage implements IPipelineStage<C64BitmapmodePictureHiresFli, BufferedImage> {

	private final Palette palette;

	public C64BitmapmodePictureHiresFliToImage(final Palette palette) {
		this.palette = palette;
	}

	@Override
	public BufferedImage apply(final C64BitmapmodePictureHiresFli bitmapScreen) {
		final C64Bitmap bitmap = bitmapScreen.bitmap();

		final BufferedImage result = new BufferedImage(C64Bitmap.WIDTH_PIXELS, C64Bitmap.HEIGHT_PIXELS, BufferedImage.TYPE_INT_RGB);
		final Graphics2D g = result.createGraphics();
		try {
			for (int xBlock = 0; xBlock < C64Bitmap.WIDTH_BLOCKS; ++xBlock) {
				for (int yBlock = 0; yBlock < C64Bitmap.HEIGHT_BLOCKS; ++yBlock) {
					final C64Char block = bitmap.blockAt(xBlock, yBlock);
					for (int line = 0; line < C64Char.HEIGHT_PIXELS; ++line) {
						final byte screenCode = bitmapScreen.screenCodeAt(line, xBlock, yBlock);
						final Color pixelColor = palette.color(toUnsignedByte((screenCode >>> 4) & 0x0F));
						final Color background = palette.color(toUnsignedByte(screenCode & 0x0F));
						final BufferedImage blockImage = new C64CharToImageHires(palette, () -> background, pixelColor).apply(block);
						final BufferedImage lineImage = blockImage.getSubimage(0, line, C64Char.WIDTH_PIXELS, 1);
						g.drawImage(lineImage, xBlock * C64Char.WIDTH_PIXELS, line + yBlock * C64Char.HEIGHT_PIXELS, null);
					}
				}
			}
		} finally {
			g.dispose();
		}
		return result;
	}

}
