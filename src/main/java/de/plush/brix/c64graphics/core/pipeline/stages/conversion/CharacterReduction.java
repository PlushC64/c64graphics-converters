package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

public enum CharacterReduction {
	UsingImageColorsOnly, UsingAllPaletteColors
}