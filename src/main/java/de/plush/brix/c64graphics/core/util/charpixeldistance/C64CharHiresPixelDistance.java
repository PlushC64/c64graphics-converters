package de.plush.brix.c64graphics.core.util.charpixeldistance;

import de.plush.brix.c64graphics.core.model.C64Char;

public enum C64CharHiresPixelDistance implements C64CharHiresPixelDistanceMeasure {
	/**
	 * This is a fast option, it just counts different pixels, but suffers from a lack of regional awareness. Two chars can be considered of equal distance to a
	 * template, regardless, if the differences are in the same region.
	 */
	HAMMING(new HiresPixelDistanceHamming()), //
	/**
	 * This is a slow option, it is the normalized sum of edits per pixel-line to create the same result..
	 *
	 * @see <a href ="https://en.wikipedia.org/wiki/Levenshtein_distance">https://en.wikipedia.org/wiki/Levenshtein_distance</a>
	 */
	LEVENSHTEIN(new HiresPixelDistanceLevenshtein()), //
	/**
	 * This is an option that is based on comparing the a very rough circular change of density between regions top-left, top-right, bottom-right and
	 * bottom-left. See {@link HiresPixelDistanceCircularDensityChangeRough} for a detailed exception.<br>
	 * It will not care if the details are the same, nor will it care for overall brightness, but it has regional awareness.
	 */
	ROUGH_CIRCULAR_PIXEL_DENSITY_CHANGE(new HiresPixelDistanceCircularDensityChangeRough()), //
	/**
	 * This is an option that is based on comparing the exact circular change of density between regions top-left, top-right, bottom-right and bottom-left. See
	 * {@link HiresPixelDistanceCircularDensityChangeFine} for a detailed exception.<br>
	 * It will not care if the details are the same, nor will it care for overall brightness, but it has regional awareness.
	 */
	FINE_CIRCULAR_PIXEL_DENSITY_CHANGE(new HiresPixelDistanceCircularDensityChangeFine()), //
	/**
	 * Combines the Hamming distance and the rough circular pixel density change measurement to get a measurement that both values the number of equal pixels
	 * and cares if the differences occur in the same region.
	 */
	HAMMING_AND_ROUGH_CIRCULAR_DENSITY_CHANGE((c1, c2) -> (2d * HAMMING.distanceOf(c1, c2) + ROUGH_CIRCULAR_PIXEL_DENSITY_CHANGE.distanceOf(c1, c2)) / 3d), //
	/**
	 * Combines the Hamming distance and the exact circular pixel density change measurement to get a measurement that both values the number of equal pixels
	 * and cares if the differences occur in the same region.
	 */
	HAMMING_AND_FINE_CIRCULAR_DENSITY_CHANGE((c1, c2) -> (2d * HAMMING.distanceOf(c1, c2) + FINE_CIRCULAR_PIXEL_DENSITY_CHANGE.distanceOf(c1, c2)) / 3d), //
	;

	private final C64CharHiresPixelDistanceMeasure measure;

	private C64CharHiresPixelDistance(final C64CharHiresPixelDistanceMeasure measure) {
		this.measure = measure;
	}

	/**
	 * @return a value between 0 and 1(inclusive). Length of the vector of all measurements.
	 * @see {@link C64CharHiresPixelDistanceMeasure#combined(C64CharHiresPixelDistanceMeasure...)}
	 */
	@SuppressWarnings("javadoc")
	public static C64CharHiresPixelDistanceMeasure combined(final C64CharHiresPixelDistanceMeasure... measures) {
		return C64CharHiresPixelDistanceMeasure.combined(measures);
	}

	@Override
	public double distanceOf(final C64Char c1, final C64Char c2) {
		return measure.distanceOf(c1, c2);
	}

}
