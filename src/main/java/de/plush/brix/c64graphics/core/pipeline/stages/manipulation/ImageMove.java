/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class ImageMove implements IPipelineStage<BufferedImage, BufferedImage> {

	private final Supplier<Color> backgroundSupplier;

	private final Point displacement;

	public ImageMove(final Supplier<Color> backgroundSupplier, final Point displacement) {
		this.backgroundSupplier = backgroundSupplier;
		this.displacement = new Point(displacement);
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		final Graphics2D g = (Graphics2D) result.getGraphics();
		try {
			g.setColor(backgroundSupplier.get());
			g.fillRect(0, 0, original.getWidth(), original.getHeight());
			g.drawImage(original, displacement.x, displacement.y, null);
			return result;
		} finally {
			g.dispose();
		}

	}
}