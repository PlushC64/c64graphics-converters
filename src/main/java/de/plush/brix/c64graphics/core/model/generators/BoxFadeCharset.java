package de.plush.brix.c64graphics.core.model.generators;

import java.util.Objects;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.Alignment;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.Alignment.*;
import de.plush.brix.c64graphics.core.util.Utils;

public class BoxFadeCharset {

	public enum Frame {
		None(0, 0b11111111), SinglePixel(1, 0b01111110);

		private final int size;

		private int pattern;

		Frame(final int size, final int pattern) {
			this.size = size;
			this.pattern = pattern;
		}

	}

	private final C64Charset charset;

	private final Horizontal horizontal;

	private final Vertical vertical;

	private final Frame frame;

	public BoxFadeCharset() {
		this(new C64Charset(), Horizontal.LEFT, Vertical.TOP, Frame.None);
	}

	private BoxFadeCharset(final C64Charset charset, final Horizontal horizontal, final Vertical vertical, final Frame frame) {
		this.charset = charset;
		this.horizontal = horizontal;
		this.vertical = vertical;
		this.frame = frame;
	}

	/**
	 * If this is given, will the returned generator will produce a copy of the given character set with added characters. Make sure there are enough characters
	 * free.
	 */
	public BoxFadeCharset withCharset(final C64Charset charset) {
		Objects.requireNonNull(charset, "C64Charset");
		return new BoxFadeCharset(charset, horizontal, vertical, frame);
	}

	public BoxFadeCharset withAlignment(final Alignment.Horizontal horizontal) {
		Objects.requireNonNull(charset, "Alignment.Horizontal ");
		return new BoxFadeCharset(charset, horizontal, vertical, frame);
	}

	public BoxFadeCharset withAlignment(final Alignment.Vertical vertical) {
		Objects.requireNonNull(charset, "Alignment.Vertical ");
		return new BoxFadeCharset(charset, horizontal, vertical, frame);
	}

	public BoxFadeCharset withFrame(final Frame frame) {
		Objects.requireNonNull(charset, "Frame");
		return new BoxFadeCharset(charset, horizontal, vertical, frame);
	}

	/** Creates a sequence of characters, starting with a blank, a small pixel and growing boxes until the char is full. **/
	public C64Charset create() {
		// create right-top aligned fade:
		final C64Charset charset = new C64Charset(this.charset);
		charset.addCharacter(C64Char.BLANK);
		for (int step = frame.size + 1; step < 8; ++step) {
			final var bytes = new byte[8];
			for (int y = 0; y < step; ++y) {
				for (int x = 0; x < step; ++x) {
					bytes[y] = Utils.setBit(bytes[y], 7 - x, true);
				}
			}
			if (frame != Frame.None) {
				bytes[0] = 0;
				bytes[7] = 0;
				Utils.applyToEach(bytes, b -> (byte) (b & frame.pattern));
			}
			charset.addCharacter(aligned(step, new C64Char(bytes)));
		}
		if (frame == Frame.None) {
			charset.addCharacter(C64Char.FULL);
		}
		return charset;
	}

	private C64Char aligned(final int step, C64Char chr) {
		switch (horizontal) {
		case LEFT:
			break;
		case RIGHT:
			chr = chr.flippedHorizontally();
			break;
		case CENTER:
			chr = chr.rolledPixelsRight(8 - step >> 1);
			break;
		default:
			throw new IllegalArgumentException("Unsupported Alignment: " + horizontal);
		}
		switch (vertical) {
		case TOP:
			break;
		case BOTTOM:
			chr = chr.flippedVertically();
			break;
		case CENTER:
			chr = chr.rolledPixelsDown(8 - step >> 1);
			break;
		default:
			throw new IllegalArgumentException("Unsupported Alignment: " + vertical);
		}
		return chr;
	}

}
