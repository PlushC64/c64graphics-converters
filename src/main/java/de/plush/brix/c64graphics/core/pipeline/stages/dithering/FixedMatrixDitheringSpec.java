package de.plush.brix.c64graphics.core.pipeline.stages.dithering;

import static de.plush.brix.c64graphics.core.util.Utils.deepCopy;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.util.Utils;

public class FixedMatrixDitheringSpec implements DitheringSpec, DitheringStrengthSupport, MatrixBoostSupport {

	public static final boolean DEFAULT_BOOST = false;

	private final double errorMultiplier;

	private final int[][] matrix;

	private final double[][] preMultipliedMatrix;

	private final Dimension matrixSize;

	private final double ditheringStrength;

	private final boolean boost;

	/**
	 * DitheringSpec for simple matrices without a center point.<br>
	 * Sample setup for a 2x2 ordered dither: <code>
	 * <pre>
	 *
	 *  final int[][] ordered2x2Matrix = new int[][]{
	 *   {0,2},
	 *   {3,1}};
	 *  final double errorMultiplier = 1.0/4;
	 *  final DitheringSpec ordered2x2Spec = new FixedMatrixDitheringSpec(errorMultiplier, ordered2x2Matrix);
	 * </pre>
	 *</code>
	 * See: {@link Dithering}
	 * 
	 * @param errorMultiplier
	 *            the error divisor
	 * @param matrix
	 *            multiplier matrix.
	 * @param boost
	 *            if set true, a matrix like {{0,1,2}} will be transformed to {{1,2,3}} and the multiplier will be changed from 1.0/3 to 1.0/4, also the matrix
	 *            normalization (-0.5) will be removed.<br>
	 *            This will usually lead to a dithering will less dark
	 *            streaks/spots, but an overall brighter picture.
	 * @param ditheringStrength
	 *            should be a value between 0.1 and 1.0, values out of bounds may unexpected effects.
	 *            Default: {@value TiledOrderedDither#DEFAULT_DITHERING_STRENGTH}
	 */
	public FixedMatrixDitheringSpec(final double errorMultiplier, final int[][] matrix, final boolean boost, final double ditheringStrength) {
		this.errorMultiplier = errorMultiplier;
		this.matrix = deepCopy(matrix);
		matrixSize = FixedMatrixDitheringSpec.sizeOf(matrix);
		this.ditheringStrength = ditheringStrength;
		this.boost = boost;
		preMultipliedMatrix = preMultipliedMatrix();
	}

	/**
	 * DitheringSpec for simple matrices without a center point.<br>
	 * Sample setup for a 2x2 ordered dither: <code>
	 * <pre>
	 *
	 *  final int[][] ordered2x2Matrix = new int[][]{
	 *   {0,2},
	 *   {3,1}};
	 *  final double errorMultiplier = 1.0/4;
	 *  final DitheringSpec ordered2x2Spec = new FixedMatrixDitheringSpec(errorMultiplier, ordered2x2Matrix);
	 * </pre>
	 *</code>
	 * See: {@link Dithering}
	 * 
	 * @param errorMultiplier
	 *            the error divisor
	 * @param matrix
	 *            multiplier matrix.
	 * @param ditheringStrength
	 *            should be a value between 0.1 and 1.0, values out of bounds may unexpected effects.
	 *            Default: {@value TiledOrderedDither#DEFAULT_DITHERING_STRENGTH}
	 */
	public FixedMatrixDitheringSpec(final double errorMultiplier, final int[][] matrix, final double ditheringStrength) {
		this(errorMultiplier, matrix, DEFAULT_BOOST, ditheringStrength);
	}

	/**
	 * DIthering Spec for simple matrices without a center point.<br>
	 * Sample setup for a 2x2 ordered dither: <code>
	 * <pre>
	 *
	 *  final int[][] ordered2x2Matrix = new int[][]{
	 *   {0,2},
	 *   {3,1}};
	 *  final double errorMultiplier = 1.0/4;
	 *  final DitheringSpec ordered2x2Spec = new FixedMatrixDitheringSpec(errorMultiplier, ordered2x2Matrix);
	 * </pre>
	 *</code>
	 *
	 * @see Dithering
	 * @param errorMultiplier
	 *            the error divisor
	 * @param matrix
	 *            multiplier matrix.
	 */
	public FixedMatrixDitheringSpec(final double errorMultiplier, final int[][] matrix) {
		this(errorMultiplier, matrix, TiledOrderedDither.DEFAULT_DITHERING_STRENGTH);
	}

	public FixedMatrixDitheringSpec(final FixedMatrixDitheringSpec spec) {
		this(spec.errorMultiplier, spec.matrix, spec.ditheringStrength);
	}

	@Override
	public FixedMatrixDitheringSpec clone() {
		return new FixedMatrixDitheringSpec(this);
	}

	// @see de.plush.brix.c64graphics.core.pipeline.stages.dithering.matrixBoostSupport#withMatrixBoost(double)
	@Override
	public MatrixBoostSupport withMatrixBoost(final boolean boost) {
		return new FixedMatrixDitheringSpec(errorMultiplier, matrix, boost, ditheringStrength);
	}

	/**
	 * @param ditheringStrength
	 *            should be a value between 0.1 and 1.0, values out of bounds may unexpected effects.
	 *            Default: {@value TiledOrderedDither#DEFAULT_DITHERING_STRENGTH}
	 * @return a new instance with dithering strength set to the given value;
	 * @see de.plush.brix.c64graphics.core.pipeline.stages.dithering.DitheringStrengthSupport#withDitheringStrength(double)
	 */
	@Override
	public FixedMatrixDitheringSpec withDitheringStrength(final double ditheringStrength) {
		return new FixedMatrixDitheringSpec(errorMultiplier, matrix, ditheringStrength);
	}

	@Override
	public Dimension matrixSize() {
		return new Dimension(matrixSize);
	}

	@Override
	public Point centerPoint(final Point globalPosition) {
		throw new UnsupportedOperationException(getClass().getSimpleName() + " objects don't have a center point");
	}

	@Override
	public Point centerPointOffset(final Point matrixPosition, final Point globalPosition) {
		throw new UnsupportedOperationException(getClass().getSimpleName() + " objects don't have a center point");
	}

	private double[][] preMultipliedMatrix() {
		final var preMultiplied = new double[matrix.length][];
		for (int y = 0; y < matrix.length; ++y) {
			preMultiplied[y] = new double[matrix[y].length];
			for (int x = 0; x < matrix[y].length; ++x) {
				if (boost) {
					preMultiplied[y][x] = (matrix[y][x] + 1) * (1.0 / (1.0 / errorMultiplier + 1));
				} else {
					preMultiplied[y][x] = (matrix[y][x] - 0.5) * errorMultiplier;
				}
			}
		}
		return preMultiplied;
	}

	@Override
	public OptionalDouble errorMultiplier(final Point matrixPosition, final Point globalPosition) {
		final double valueAtPos = preMultipliedMatrix[matrixPosition.y][matrixPosition.x];
		return valueAtPos == DitheringSpec.X ? OptionalDouble.empty() //
				: OptionalDouble.of(valueAtPos);
	}

	private static Dimension sizeOf(final int[][] matrix) {
		final int height = matrix.length;
		final int width = matrix[0].length;
		for (int y = 1; y < height; ++y) {
			if (matrix[y].length != width) {
				throw new IllegalArgumentException("Matrix must be uniform: Each line must have the same amount of columns: " + Utils.toString(matrix));
			}
		}
		return new Dimension(width, height);
	}

	@Override
	public Function<BufferedImage, BufferedImage> algorithm(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure) {
		return new TiledOrderedDither(palette, colorDistanceMeasure, this, ditheringStrength);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(matrix);
		result = prime * result + Objects.hash(boost, ditheringStrength, errorMultiplier);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final FixedMatrixDitheringSpec other = (FixedMatrixDitheringSpec) obj;
		return boost == other.boost && Double.doubleToLongBits(ditheringStrength) == Double.doubleToLongBits(other.ditheringStrength) && Double
				.doubleToLongBits(errorMultiplier) == Double.doubleToLongBits(other.errorMultiplier) && Arrays.deepEquals(matrix, other.matrix);
	}

}
