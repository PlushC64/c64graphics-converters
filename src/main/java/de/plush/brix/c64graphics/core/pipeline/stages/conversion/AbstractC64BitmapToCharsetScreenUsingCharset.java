package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.UtilsImage.error;
import static java.util.Comparator.comparingDouble;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.*;

import java.util.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.ColorDistance;

public abstract class AbstractC64BitmapToCharsetScreenUsingCharset implements IPipelineStage<C64Bitmap, C64CharsetScreen> {

	protected final C64Charset charset;

	public AbstractC64BitmapToCharsetScreenUsingCharset(final C64Charset charset) {
		requireNonNull(charset);
		this.charset = new C64Charset(charset);
	}

	public abstract Function<C64Char, PreProcessedCharacterImage> createC64CharToImage();

	@Override
	public C64CharsetScreen apply(final C64Bitmap bitmap) {
		final Map<C64Char, C64Char> bitmapBlockToClosestCharsetBlock = bitmapBlockToClosestCharsetBlock(bitmap);
		C64CharsetScreen charsetScreenImage = null;
		for (int y = 0; y < C64CharsetScreen.HEIGHT_BLOCKS; ++y) {
			for (int x = 0; x < C64CharsetScreen.WIDTH_BLOCKS; ++x) {
				final C64Char character = bitmapBlockToClosestCharsetBlock.get(bitmap.blockAt(x, y));
				if (charsetScreenImage == null) {
					charsetScreenImage = new C64CharsetScreen(charset, character);
				}
				charsetScreenImage.setBlock(x, y, character);
			}
		}
		return charsetScreenImage;
	}

	protected Map<C64Char, C64Char> bitmapBlockToClosestCharsetBlock(final C64Bitmap bitmap) {
		final List<Entry<PreProcessedCharacterImage, C64Char>> charsetImageMappings = imageMappings(charset.uniqueChars());
		return imageMappings(uniqueChars(bitmap)).stream().parallel()//
				.collect(toMap(Entry::getValue, // the bitmap character
						bmapMapping -> charsetImageMappings.stream()//
								.min(comparingDouble(csMapping -> error(bmapMapping.getKey(), csMapping.getKey(), ColorDistance.EUCLIDEAN)))//
								.get()//
								.getValue())); // the charset character
	}

	protected List<Entry<PreProcessedCharacterImage, C64Char>> imageMappings(final Set<C64Char> characters) {
		final Function<C64Char, PreProcessedCharacterImage> chrToImage = createC64CharToImage();
		return characters.parallelStream()//
				.map(character -> new SimpleEntry<>(chrToImage.apply(character), character))//
				.collect(toList());
	}

	protected static Set<C64Char> uniqueChars(final C64Bitmap bitmap) {
		return bitmap.blocksAs(new HashSet<>());
	}
}