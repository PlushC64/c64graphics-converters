/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import static de.plush.brix.c64graphics.core.pipeline.stages.manipulation.ColorBlender.Detect.*;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class ColorBlender implements IPipelineStage<BufferedImage, BufferedImage> {

	public enum Detect {
		/** If observed color differs from pixel color with &lt; colorToleranceRMS, draw pixel color, else draw background color. */
		PixelColor, //
		/** If observed color differs from background color with &lt; colorToleranceRMS, draw background color, else draw pixel color. */
		BackgroundColor,
		/**
		 * Mode to be compatible with first version that was used to convert the dancer video. If red+green+blue &lt; colorToleranceRMS: draw pixel color, else
		 * draw background color.
		 */
		Blackness
	}

	private final Supplier<Color> backgroundSupplier;

	private final Color pixelColor;

	private final double colorToleranceRMS;

	private final Detect detect;

	public ColorBlender(final Supplier<Color> backgroundSupplier, final Color pixelColor, final Detect detect, final double colorToleranceRMS) {
		this.backgroundSupplier = backgroundSupplier;
		this.pixelColor = pixelColor;
		this.detect = detect;
		this.colorToleranceRMS = colorToleranceRMS;
	}

	public ColorBlender(final Supplier<Color> backgroundSupplier, final Color pixelColor, final Detect detect) {
		this(backgroundSupplier, pixelColor, detect, 50);
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());
		// double maxRms = -1;
		// double minRms = Integer.MAX_VALUE;
		final Color backgroundColor = backgroundSupplier.get();
		IntStream.range(0, original.getWidth()).parallel().forEach(x -> {
			for (int y = 0; y < original.getHeight(); ++y) {
				final Color c = new Color(original.getRGB(x, y));

				if (detect == Blackness) {
					if (c.getRed() + c.getGreen() + c.getBlue() < colorToleranceRMS) {
						result.setRGB(x, y, pixelColor.getRGB());
					} else {
						result.setRGB(x, y, backgroundColor.getRGB());
					}
				} else {
					double meanSquareError;
					if (detect == PixelColor) {
						meanSquareError = rms(pixelColor, c);
					} else {
						meanSquareError = rms(backgroundColor, c);
					}
					final Color detectedColorReplacement = detect == Detect.PixelColor ? pixelColor : backgroundColor;
					final Color otherColorReplacement = detect == Detect.PixelColor ? backgroundColor : pixelColor;

					if (meanSquareError < colorToleranceRMS) {
						result.setRGB(x, y, detectedColorReplacement.getRGB());
					} else {
						result.setRGB(x, y, otherColorReplacement.getRGB());
					}
				}
			}
		});
		return result;
	}

	private static double rms(final Color expected, final Color observed) {
		final double r = Math.pow(expected.getRed() - observed.getRed(), 2);
		final double g = Math.pow(expected.getGreen() - observed.getGreen(), 2);
		final double b = Math.pow(expected.getBlue() - observed.getBlue(), 2);
		final double rmse = Math.sqrt(1d / 3 * (r + g + b));
		return rmse;
	}
}