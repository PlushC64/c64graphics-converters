package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;

import java.awt.*;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.Palette;

public class C64BitmapmodePictureHiresToImage implements IPipelineStage<C64BitmapmodePictureHires, BufferedImage> {

	private final Palette palette;

	public C64BitmapmodePictureHiresToImage(final Palette palette) {
		this.palette = palette;
	}

	@Override
	public BufferedImage apply(final C64BitmapmodePictureHires bitmapScreen) {
		final C64Bitmap bitmap = bitmapScreen.bitmap();
		final C64Screen screen = bitmapScreen.screen();

		final BufferedImage result = new BufferedImage(C64Bitmap.WIDTH_PIXELS, C64Bitmap.HEIGHT_PIXELS, BufferedImage.TYPE_INT_RGB);
		final Graphics2D g = result.createGraphics();
		try {
			// jmh testing showed that the simple for-loops seem to be twice as fast as using parallel streams.
			for (int yBlock = 0; yBlock < C64Bitmap.HEIGHT_BLOCKS; ++yBlock) {
				for (int xBlock = 0; xBlock < C64Bitmap.WIDTH_BLOCKS; ++xBlock) {
					final byte screenCode = screen.screenCodeAt(xBlock, yBlock);
					final Color pixelColor = palette.color(toUnsignedByte((screenCode >>> 4) & 0x0F));
					final Color background = palette.color(toUnsignedByte(screenCode & 0x0F));
					final C64Char block = bitmap.blockAt(xBlock, yBlock);
					final BufferedImage blockImage = new C64CharToImageHires(palette, () -> background, pixelColor).apply(block);
					g.drawImage(blockImage, xBlock * C64Char.WIDTH_PIXELS, yBlock * C64Char.HEIGHT_PIXELS, null);
				}
			}
		} finally {
			g.dispose();
		}
		return result;
	}

}
