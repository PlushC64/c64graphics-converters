/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces;

import java.awt.Color;

/**
 * Represents a Color in the "Chroma - Luma" color space for analog PAL/NTSC TV signals from analog sources, such as Antenna, Sat and Cable TV or the analog
 * PAL transmission of Composite Video or S-Video signals.
 * 
 * This must not to be confused with the YpbPr color model that is used for analog video signals from digital sources, such as DVD and DVB, as used in
 * Composite video signals from your DVD player or home computer.
 * 
 * @see <a href="https://en.wikipedia.org/wiki/YUV">see: https://en.wikipedia.org/wiki/YUV</a>
 */
public record ColorYUV(double Y, double U, double V) {

	/**
	 * A constant for color black. Color components are:
	 * 
	 * <pre>
	 *     Y: 0
	 *     U: 0
	 *     V: 0
	 * </pre>
	 */
	public static final ColorYUV BLACK = ColorConversions.convertRGBtoYUV(Color.BLACK);

	/**
	 * A constant for color white. Color components are:
	 *
	 * <pre>
	 * Y: 155.0
	 * U: 0.0
	 * V: 0.0
	 * </pre>
	 */
	public static final ColorYUV WHITE = ColorConversions.convertRGBtoYUV(Color.WHITE);

	@SuppressWarnings("checkstyle:EqualsHashCode") // hashCode can be inherited in this special case
	@Override
	public boolean equals(final Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }

		final ColorYUV colorYUV = (ColorYUV) o;
		if (Double.compare(colorYUV.Y(), Y()) != 0) { return false; }
		if (Double.compare(colorYUV.U(), U()) != 0) { return false; }
		if (Double.compare(colorYUV.V(), V()) != 0) { return false; }

		return true;
	}

}
