/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces;

/**
 * Represents a color in the CMYK color space.
 *
 * <p>
 * Contains the constant values for black, white, red,
 * green, blue, cyan, magenta, and yellow.
 * </p>
 *
 * @see <a href="https://en.wikipedia.org/wiki/CMYK_color_model">https://en.wikipedia.org/wiki/CMYK_color_model</a>
 * @since 1.0-alpha1
 */
public record ColorCmyk(double C, double M, double Y, double K) {

	/**
	 * A constant for color cyan. Color components are:
	 * 
	 * <pre>
	 *     cyan:    100
	 *     magenta: 0
	 *     yellow:  0
	 *     key:     0
	 * </pre>
	 */
	public static final ColorCmyk CYAN = new ColorCmyk(100, 0, 0, 0);

	/**
	 * A constant for color magenta. Color components are:
	 * 
	 * <pre>
	 *     cyan:    0
	 *     magenta: 100
	 *     yellow:  0
	 *     key:     0
	 * </pre>
	 */
	public static final ColorCmyk MAGENTA = new ColorCmyk(0, 100, 0, 0);

	/**
	 * A constant for color yellow. Color components are:
	 * 
	 * <pre>
	 *     cyan:    0
	 *     magenta: 0
	 *     yellow:  100
	 *     key:     0
	 * </pre>
	 */
	public static final ColorCmyk YELLOW = new ColorCmyk(0, 0, 100, 0);

	/**
	 * A constant for color black. Color components are:
	 * 
	 * <pre>
	 *     cyan:    0
	 *     magenta: 0
	 *     yellow:  0
	 *     key:     100
	 * </pre>
	 */
	public static final ColorCmyk BLACK = new ColorCmyk(0, 0, 0, 100);

	/**
	 * A constant for color white. Color components are:
	 * 
	 * <pre>
	 *     cyan:    0
	 *     magenta: 0
	 *     yellow:  0
	 *     key:     0
	 * </pre>
	 */
	public static final ColorCmyk WHITE = new ColorCmyk(0, 0, 0, 0);

	/**
	 * A constant for color red. Color components are:
	 * 
	 * <pre>
	 *     cyan:    0
	 *     magenta: 100
	 *     yellow:  100
	 *     key:     0
	 * </pre>
	 */
	public static final ColorCmyk RED = new ColorCmyk(0, 100, 100, 0);

	/**
	 * A constant for color green. Color components are:
	 * 
	 * <pre>
	 *     cyan:    100
	 *     magenta: 0
	 *     yellow:  100
	 *     key:     0
	 * </pre>
	 */
	public static final ColorCmyk GREEN = new ColorCmyk(100, 0, 100, 0);

	/**
	 * A constant for color blue. Color components are:
	 * 
	 * <pre>
	 *     cyan:    100
	 *     magenta: 100
	 *     yellow:  0
	 *     key:     0
	 * </pre>
	 */
	public static final ColorCmyk BLUE = new ColorCmyk(100, 100, 0, 0);

	@SuppressWarnings("checkstyle:EqualsHashCode") // hashCode can be inherited in this special case
	@Override
	public boolean equals(final Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }

		final ColorCmyk colorCmyk = (ColorCmyk) o;
		if (Double.compare(colorCmyk.C(), C()) != 0) { return false; }
		if (Double.compare(colorCmyk.K(), K()) != 0) { return false; }
		if (Double.compare(colorCmyk.M(), M()) != 0) { return false; }
		if (Double.compare(colorCmyk.Y(), Y()) != 0) { return false; }

		return true;
	}

}
