package de.plush.brix.c64graphics.core.util;

public interface BlendFunction<T> {

	/**
	 *
	 * @param start
	 *            the value to be emitted for the magnitude 0f.
	 * @param magnitude
	 *            the magnitude of blending, from 0f to 1f
	 * @param end
	 *            the value to be emitted for the magnitude 1f.
	 * @return a value between start and end that depends on the magnitude. For a magnitude of 0.5f with a linear blend function, the value is expected to be
	 *         halfway between start and end.
	 */
	T blend(final T start, final double magnitude, final T end);
}
