package de.plush.brix.c64graphics.core.util.collections;

import static de.plush.brix.c64graphics.core.util.Utils.*;
import static de.plush.brix.c64graphics.core.util.functions.BytePredicate.negated;
import static java.util.Arrays.copyOf;
import static java.util.stream.Collectors.joining;

import java.util.*;
import java.util.stream.*;

import de.plush.brix.c64graphics.core.util.Utils;
import de.plush.brix.c64graphics.core.util.functions.*;

public class ByteSet {

	private final boolean[] bytes;

	private int size;

	public ByteSet() {
		bytes = new boolean[0x100];
	}

	public ByteSet(final ByteSet other) {
		bytes = copyOf(other.bytes, other.bytes.length);
		size = other.size;
	}

	public static ByteSet of(final byte... bytes) {
		final var set = new ByteSet();
		set.addAll(bytes);
		return set;
	}

	public static ByteSet ofUnsigned(final int... bytes) {
		final var set = new ByteSet();
		set.addAllUnsigned(bytes);
		return set;
	}

	public static ByteSet empty() {
		return new ByteSet();
	}

	public int size() {
		return size;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public boolean add(final byte b) {
		final var index = toPositiveInt(b);
		final var existed = bytes[index];
		bytes[index] = true;
		size += !existed ? 1 : 0;
		return !existed;
	}

	public boolean addUnsigned(final int b) {
		return add(toUnsignedByte(b));
	}

	public boolean addAll(final ByteSet source) {
		if (source == this || source.isEmpty()) { return false; }
		final int oldSize = size;
		for (int t = 0; t < bytes.length; ++t) {
			final var existed = bytes[t];
			final var toBeAdded = source.bytes[t];
			bytes[t] |= toBeAdded;
			size += !existed && toBeAdded ? 1 : 0;
		}
		return oldSize != size;
	}

	public boolean addAll(final byte... source) {
		final int oldSize = size;
		for (int t = 0; t < source.length; ++t) {
			add(source[t]);
		}
		return oldSize != size;
	}

	public boolean addAllUnsigned(final int... source) {
		final int oldSize = size;
		for (int t = 0; t < source.length; ++t) {
			addUnsigned(source[t]);
		}
		return oldSize != size;
	}

	public boolean remove(final byte b) {
		final var index = toPositiveInt(b);
		final var existed = bytes[index];
		bytes[index] = false;
		size -= existed ? 1 : 0;
		return existed;
	}

	public boolean removeAll(final ByteSet source) {
		if (source.isEmpty()) { return false; }
		final int oldSize = size;
		for (int t = 0; t < bytes.length; ++t) {
			final var existed = bytes[t];
			final var toBeRemoved = source.bytes[t];
			bytes[t] &= !toBeRemoved;
			size -= existed && toBeRemoved ? 1 : 0;
		}
		return oldSize != size;
	}

	public boolean removeAll(final byte[] source) {
		final int oldSize = size;
		for (int t = 0; t < source.length; ++t) {
			remove(source[t]);
		}
		return oldSize != size;
	}

	public boolean contains(final byte b) {
		return bytes[toPositiveInt(b)];
	}

	public ByteSet with(final byte b) {
		final var copy = new ByteSet(this);
		copy.add(b);
		return copy;
	}

	public ByteSet withAll(final ByteSet bytes) {
		final var copy = new ByteSet(this);
		copy.addAll(bytes);
		return copy;
	}

	public ByteSet without(final byte b) {
		final var copy = new ByteSet(this);
		copy.remove(b);
		return copy;
	}

	public ByteSet withoutAll(final ByteSet bytes) {
		final var copy = new ByteSet(this);
		copy.removeAll(bytes);
		return copy;
	}

	public void clear() {
		if (isEmpty()) { return; }
		size = 0;
		for (int t = 0; t < bytes.length; ++t) {
			bytes[t] = false;
		}
	}

	/** Same as {@link #select(BytePredicate)}, but for those who are used to java.util lingo */
	public ByteSet filter(final BytePredicate predicate) {
		return select(predicate);
	}

	/**
	 * Returns a new ByteSet with all of the elements in the ByteSet that return true for the specified predicate.
	 */
	public ByteSet select(final BytePredicate predicate) {
		final var selected = new ByteSet();
		if (isEmpty()) { return selected; }
		for (int t = 0; t < bytes.length; ++t) {
			selected.bytes[t] = bytes[t] && predicate.accept(toUnsignedByte(t));
			selected.size += selected.bytes[t] ? 1 : 0;
		}
		return selected;
	}

	/**
	 * Returns a new ByteSet with all of the elements in the ByteSet that return false for the specified predicate.
	 */
	public ByteSet reject(final BytePredicate predicate) {
		return select(negated(predicate));
	}

	public void forEach(final ByteProcedure procedure) {
		if (isEmpty()) { return; }
		for (int t = 0; t < bytes.length; ++t) {
			if (bytes[t]) {
				procedure.value(toUnsignedByte(t));
			}
		}
	}

	public <T> Stream<T> map(final ByteToObjectFunction<? extends T> function) {
		if (isEmpty()) { return Stream.empty(); }
		final var objects = new HashSet<T>(size);
		forEach(b -> objects.add(function.valueOf(b)));
		return objects.stream();
	}

	public IntStream mapToInt(final ByteToIntFunction function) {
		if (isEmpty()) { return IntStream.empty(); }
		final var array = new int[size];
		int x = -1;
		for (int t = 0; t < bytes.length; ++t) {
			if (bytes[t]) {
				array[++x] = function.valueOf(toUnsignedByte(t));
			}
		}
		return IntStream.of(array);
	}

	public boolean anySatisfy(final BytePredicate predicate) {
		if (isEmpty()) { return false; }
		for (int t = 0; t < bytes.length; ++t) {
			if (bytes[t] && predicate.accept(toUnsignedByte(t))) { return true; }
		}
		return false;
	}

	public boolean allSatisfy(final BytePredicate predicate) {
		if (isEmpty()) { return false; }
		for (int t = 0; t < bytes.length; ++t) {
			if (bytes[t] && !predicate.accept(toUnsignedByte(t))) { return false; }
		}
		return true;
	}

	public boolean noneSatisfy(final BytePredicate predicate) {
		if (isEmpty()) { return true; }
		for (int t = 0; t < bytes.length; ++t) {
			if (bytes[t] && predicate.accept(toUnsignedByte(t))) { return false; }
		}
		return true;
	}

	public int count(final BytePredicate predicate) {
		int count = 0;
		if (isEmpty()) { return count; }
		for (int t = 0; t < bytes.length; ++t) {
			count += bytes[t] && predicate.accept(toUnsignedByte(t)) ? 1 : 0;
		}
		return count;
	}

	public int sum() {
		int sum = 0;
		if (isEmpty()) { return sum; }
		for (int t = 0; t < bytes.length; ++t) {
			sum += bytes[t] ? toUnsignedByte(t) : 0; // 0xff -> -1
		}
		return sum;
	}

	public int sumUnsigned() {
		int sum = 0;
		if (isEmpty()) { return sum; }
		for (int t = 0; t < bytes.length; ++t) {
			sum += bytes[t] ? t : 0; // 0xff -> 255
		}
		return sum;
	}

	public byte min() {
		if (!isEmpty()) {
			for (int t = 0; t < bytes.length; ++t) {
				// as bytes.length (=0x100=256) is a power of 2: X % bytes.length == X & (bytes.length - 1)
				final int index = t + 0x80 & bytes.length - 1; // "negative numbers start at index 0x80 = 128), we want to wrap down
				if (bytes[index]) { return toUnsignedByte(index); }
			}
		}
		throw new NoSuchElementException();
	}

	public byte minIfEmpty(final byte defaultValue) {
		if (isEmpty()) { return defaultValue; }
		return min();
	}

	public byte max() {
		if (!isEmpty()) {
			for (int t = bytes.length - 1; t >= 0; --t) {
				// as bytes.length (=0x100=256) is a power of 2: X % bytes.length == X & (bytes.length - 1)
				final int index = t + 0x80 & bytes.length - 1; // "negative numbers start at index 0x80 = 128), we want to wrap down
				if (bytes[index]) { return toUnsignedByte(index); }
			}
		}
		throw new NoSuchElementException();
	}

	public byte maxIfEmpty(final byte defaultValue) {
		if (isEmpty()) { return defaultValue; }
		return max();
	}

	// TODO: min, max, average, median unsigned

	public double average() {
		if (isEmpty()) { throw new ArithmeticException(); }
		return (double) sum() / size;
	}

	public double median() {
		if (isEmpty()) { throw new ArithmeticException(); }
		final var sortedArray = toSortedArray();
		final int middleIndex = sortedArray.length >> 1;
		if (sortedArray.length > 1 && (sortedArray.length & 1) == 0) {
			final byte left = sortedArray[middleIndex - 1];
			final byte right = sortedArray[middleIndex];
			return (left + right) / 2.0;
		}
		return sortedArray[middleIndex];
	}

	public byte[] toArray() {
		final var array = new byte[size];
		if (isEmpty()) { return array; }
		int x = -1;
		for (int t = 0; t < bytes.length; ++t) {
			if (bytes[t]) {
				array[++x] = toUnsignedByte(t);
			}
		}
		return array;
	}

	public byte[] toSortedArrayUnsigned() {
		return toArray();
	}

	public byte[] toSortedArray() {
		final var array = new byte[size];
		if (isEmpty()) { return array; }
		int x = -1;
		for (int t = 0; t < bytes.length; ++t) {
			// as bytes.length (=0x100=256) is a power of 2: X % bytes.length == X & (bytes.length - 1)
			final int index = t + 0x80 & bytes.length - 1; // "negative numbers start at index 0x80 = 128), we want to wrap down
			if (bytes[index]) {
				array[++x] = toUnsignedByte(index);
			}
		}
		return array;
	}

	@Override
	public String toString() {
		final var byteString = mapToInt(Utils::toPositiveInt)//
				.sorted()//
				.mapToObj(i -> Integer.toString(i, 16))//
				.collect(joining(","));
		return "ByteSet [bytes=" + byteString + ", size=" + size + "]";
	}

	@Override
	public int hashCode() {
		if (isEmpty()) { return 0; }
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(bytes);
		result = prime * result + size;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final ByteSet other = (ByteSet) obj;
		if (size != other.size) { return false; }
		if (!Arrays.equals(bytes, other.bytes)) { return false; }
		return true;
	}

}
