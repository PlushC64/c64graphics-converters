package de.plush.brix.c64graphics.core.emitters;

import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.function.Predicate;

import javax.annotation.*;
import javax.imageio.ImageIO;

import de.plush.brix.c64graphics.core.pipeline.ImageConsumer;

/**
 * Emits the image of a given image file or tries to emit all all files in a given directory as images.
 */
public class ImageEmitter extends AbstractImageEmitter<BufferedImage> {

	private final File fileOrDirectory;
	private final Predicate<File> filter;

	public ImageEmitter(final File fileOrDirectory) {
		this(fileOrDirectory, Collections.emptyList());
	}

	public ImageEmitter(final File fileOrDirectory, final Predicate<File> filter) {
		this(fileOrDirectory, filter, Collections.emptyList());
	}

	public ImageEmitter(final File fileOrDirectory, @WillCloseWhenClosed final ImageConsumer<BufferedImage> consumer) {
		this(fileOrDirectory, Arrays.asList(consumer));
	}

	public ImageEmitter(final File fileOrDirectory, final Predicate<File> filter, @WillCloseWhenClosed final ImageConsumer<BufferedImage> consumer) {
		this(fileOrDirectory, filter, Arrays.asList(consumer));
	}

	public ImageEmitter(final File fileOrDirectory, @WillCloseWhenClosed final Collection<ImageConsumer<BufferedImage>> consumers) {
		this(fileOrDirectory, any -> true, consumers);
	}

	public ImageEmitter(final File fileOrDirectory, final Predicate<File> filter,
			@WillCloseWhenClosed final Collection<ImageConsumer<BufferedImage>> consumers) {
		this.fileOrDirectory = fileOrDirectory;
		this.filter = filter;
		consumers.forEach(this::add);
	}

	@WillClose
	@Override
	public void run() {
		try {
			if (fileOrDirectory.isDirectory()) {
				Arrays.stream(fileOrDirectory.list())//
						.map(name -> Paths.get(fileOrDirectory.getPath(), name))//
						.map(Path::toFile)//
						.filter(filter)//
						.filter(File::isFile)//
						.forEach(this::emitSingle);
			} else {
				emitSingle(fileOrDirectory);
			}
		} finally {
			closeEmission();
		}
	}

	private void emitSingle(final File file) {
		try {
			final BufferedImage image = ImageIO.read(file);
			emit(image);
		} catch (final IOException e) {
			throw new IllegalArgumentException("could not load image " + file, e);
		}
	}

}
