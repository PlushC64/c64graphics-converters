package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces.ColorConversions.*;

import java.awt.Color;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces.ColorDIN99Lab;

/**
 * Color distance according to the DIN99b standard.<br>
 * Note that the DIN99 color space has different iterations of the standard, from DIN99 to DIN99b, ..., DIN99o.
 * 
 * Note that the DIN99 color difference formula is basically the same as the CIE76 and Hunter Lab color difference formula, but operating on a differently
 * formed Lab
 * color space.
 * 
 * @see <a href = "http://juliagraphics.github.io/Colors.jl/stable/colordifferences/">http://juliagraphics.github.io/Colors.jl/stable/colordifferences/</a>
 * @see <a href="de.wikipedia.org/wiki/DIN99-Farbraum">de.wikipedia.org/wiki/DIN99-Farbraum</a>
 */
class ColorDistanceDIN99b implements ColorDistanceMeasure {

	@Override
	public double distanceOf(final Color rgb1, final Color rgb2) {
		final var lab1 = convertCIELabToDIN99bLab(convertXYZtoCIELab(convertRGBtoXYZ(rgb1)));
		final var lab2 = convertCIELabToDIN99bLab(convertXYZtoCIELab(convertRGBtoXYZ(rgb2)));
		return rgb1.equals(rgb2) ? 0.0 : Math.min(1, distanceOf(lab1, lab2) / 101);
	}

	/**
	 * Compares two L*a*b colors and returns the degree of their similarity. The lower the result the more similar are the colors.
	 *
	 * Taken from https://de.wikipedia.org/wiki/DIN99-Farbraum#Farbabstandsformel
	 *
	 * @param lab1
	 *            First color represented in DIN99 L*a*b color space.
	 * @param lab2
	 *            Second color represented in DIN99 L*a*b color space.
	 * @return The degree of similarity between the two input colors according to the CIE76 color-difference formula.
	 */
	private static double distanceOf(final ColorDIN99Lab lab1, final ColorDIN99Lab lab2) {
		return Math.sqrt(Math.pow(lab1.L99() - lab2.L99(), 2) + Math.pow(lab1.a99() - lab2.a99(), 2) + Math.pow(lab1.b99() - lab2.b99(), 2));
	}

}
