package de.plush.brix.c64graphics.core.pipeline.stages.misc;

import static java.lang.Math.min;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.util.CountableInt;
import de.plush.brix.c64graphics.core.util.collections.IntHashBag;

public class FindMostCommonColor implements IPipelineStage<BufferedImage, BufferedImage>, Supplier<Color> {

	public enum Calculate {
		NEW_FOR_EVERY_FRAME, //
		FIRST_FRAME_AND_REUSE;
	}

	private final Calculate calculate;

	private final Optional<Rectangle> boundsToSearch;

	private Optional<Color> mostCommonColor = Optional.empty();

	public FindMostCommonColor(final Calculate calculate) {
		this(calculate, null);
	}

	/**
	 * Gets the most frequent color from the given rectangle
	 */
	public FindMostCommonColor(final Calculate calculate, final Rectangle boundsToSearch) {
		this.calculate = calculate;
		this.boundsToSearch = Optional.ofNullable(boundsToSearch);
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		if (calculate == Calculate.NEW_FOR_EVERY_FRAME || calculate == Calculate.FIRST_FRAME_AND_REUSE && !mostCommonColor.isPresent()) {
			final Rectangle bounds = boundsToSearch.orElse(new Rectangle(0, 0, original.getWidth(), original.getHeight()));
			final int width = min(bounds.width, original.getWidth());
			final int height = min(bounds.height, original.getHeight());
			final var topOccurrences = IntStream.range(0, width * height)//
					.parallel()//
					.map(index -> original.getRGB(index % width, index / width)) //
					.collect(IntHashBag::empty // supplier
							, (bag, value) -> bag.add(value) // accumulator
							, IntHashBag::addAll) // combiner
					.topOccurrences(1);// pair<rgb,count>
			if (!topOccurrences.isEmpty()) {
				mostCommonColor = Optional.of(topOccurrences.get(0))// pair<rgb,count>
						.map(CountableInt::intValue)// rgb value
						.map(Color::new); //
			}
		}
		return original;
	}

	@Override
	public Color get() {
		return mostCommonColor.get();
	}

}