package de.plush.brix.c64graphics.core.pipeline.stages.compression;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;

import java.util.*;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.util.CountableByte;
import de.plush.brix.c64graphics.core.util.collections.ByteArrayList;

/**
 * A stateless, simple RLE-packer, that uses a marker byte to mark runs.
 * <dl>
 * <dt>Run Sequence
 * <dd>[controlByte, count, byte]
 * <dt>Escape Sequence
 * <dd>[controlByte, count, controlByte]
 * <dt>End Sequence
 * <dd>[controlByte, 0]
 * </dl>
 * This is the same packing scheme that Amica Paint uses, where the controlByte is <tt>0xC2</tt>.
 */
public class RLEpacker implements Function<byte[], byte[]> {

	private final byte controlByte;

	public RLEpacker(final byte controlByte) {
		this.controlByte = controlByte;
	}

	@Override
	public byte[] apply(final byte[] input) {
		final byte[] packed = toRLEencodedBinary(createRuns(input));
		System.out.println("raw length: " + input.length + " -> " + packed.length + " (bytes)");
		return packed;
	}

	private byte[] toRLEencodedBinary(final LinkedList<CountableByte> runs) {
		// to RLE encoding:
		final var output = ByteArrayList.empty();
		for (final CountableByte run : runs) {
			if (run.count > 3 || run.byteValue == controlByte) {
				output.add(controlByte);
				output.add(toUnsignedByte((int) run.count));
				output.add(run.byteValue);
			} else {
				for (int t = 0; t < run.count; ++t) {
					output.add(run.byteValue);
				}
			}
		}
		// set endmark:
		output.add(controlByte);
		output.add((byte) 0);
		return output.toArray();
	}

	private static LinkedList<CountableByte> createRuns(final byte[] screenBinary) {
		// create runs of 1:
		final LinkedList<CountableByte> runs = new LinkedList<>();
		for (int t = 0; t < screenBinary.length; ++t) {
			runs.add(CountableByte.ofValue(screenBinary[t]).withCount(1));
		}
		// compact runs:
		final Iterator<CountableByte> iterator = runs.iterator();
		CountableByte head = iterator.hasNext() ? iterator.next() : null;
		while (head != null && iterator.hasNext()) {
			final CountableByte tail = iterator.next();
			if (head.byteValue == tail.byteValue && head.count < 0xff) {
				head.count += 1;
				iterator.remove();
			} else {
				head = tail;
			}
		}
		return runs;
	}

}
