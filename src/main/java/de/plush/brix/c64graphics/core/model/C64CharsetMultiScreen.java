package de.plush.brix.c64graphics.core.model;

import java.awt.*;
import java.util.*;
import java.util.stream.Stream;

import de.plush.brix.c64graphics.core.util.ObjByteToByteFunction;

public class C64CharsetMultiScreen implements IRollBlockwise {

	public static final int WIDTH_BLOCKS = C64Screen.WIDTH_BLOCKS;

	public static final int HEIGHT_BLOCKS = C64Screen.HEIGHT_BLOCKS;

	private final C64Charset charset;
	private final C64Screen[] screens;

	public C64CharsetMultiScreen(final C64CharsetMultiScreen other) throws TooManyCharactersInCharset {
		charset = new C64Charset(other.charset);
		screens = other.screens();
	}

	public C64CharsetMultiScreen(final C64Charset charset, final Collection<C64Screen> screens) {
		this(new C64Charset(charset), screens.toArray(C64Screen[]::new), null);
	}

	public C64CharsetMultiScreen(final C64Charset charset, final Stream<C64Screen> screens) {
		this(new C64Charset(charset), screens.toArray(C64Screen[]::new), null);
	}

	public C64CharsetMultiScreen(final C64Charset charset, final C64Screen[] screens) {
		this.charset = new C64Charset(charset);
		this.screens = screens.clone();
	}

	private C64CharsetMultiScreen(final C64Charset charset, final C64Screen[] screens, @SuppressWarnings("unused") final Void alwaysNull) {
		this.charset = charset;
		this.screens = screens;
	}

	public static Dimension sizePixels() {
		final Dimension dim = C64Screen.sizeBlocks();
		dim.height *= C64Char.HEIGHT_PIXELS;
		dim.width *= C64Char.WIDTH_PIXELS;
		return dim;
	}

	public static Dimension sizeBlocks() {
		return C64Screen.sizeBlocks();
	}

	public byte screenCodeAt(final int x, final int y, final int screenNumber) {
		return screens[screenNumber].screenCodeAt(x, y);
	}

	public void setScreenCode(final int x, final int y, final int screenNumber, final byte code) {
		screens[screenNumber].setScreenCode(x, y, code);
	}

	/**
	 * Replaces the character, but does not modify screen codes.<br>
	 * Will do nothing, if the character to be replaced is not present
	 */
	public void replaceCharacter(final C64Char old, final C64Char replacement) {
		charset.screenCodeOf(old)//
				.ifPresent(screenCode -> charset.putCharacter(screenCode, replacement));
	}

	/**
	 * Will switch the screencodes on every screen and manipulate the character set accordingly. <br>
	 * Will do nothing, if either of the provided screen codes has no character in the charset.
	 */
	public void exchangeScreenCodes(final byte a, final byte b) {
		final var chrA = charset.characterBlockOf(a);
		final var chrB = charset.characterBlockOf(b);
		if (chrA.isPresent() && chrB.isPresent()) {
			final ObjByteToByteFunction<Point> exchangeAandB = (pos, code) -> code == a ? b : code == b ? a : code;
			charset.putCharacter(a, chrB.get());
			charset.putCharacter(b, chrA.get());
			Arrays.stream(screens).parallel().forEach(screen -> {
				screen.modifyEveryScreenCode(exchangeAandB);
			});
		}
	}

	public C64Charset charset() {
		return new C64Charset(charset);
	}

	public C64Screen[] screens() {
		return Arrays.stream(screens).map(C64Screen::new).toArray(C64Screen[]::new);
	}

	public C64Screen screen(final int screenNumber) {
		return new C64Screen(screens[screenNumber]);
	}

	public boolean isCharsetFull() {
		return charset.isFull();
	}

	public int charsetSize() {
		return charset.size();
	}

	public boolean contains(final C64Char chr) {
		return charset.contains(chr);
	}

	@Override
	public void rollLeftBlockwise(final int n) {
		Arrays.stream(screens).forEach(screen -> screen.rollLeftBlockwise(n));
	}

	@Override
	public void rollRightBlockwise(final int n) {
		Arrays.stream(screens).forEach(screen -> screen.rollRightBlockwise(n));
	}

	@Override
	public void rollUpBlockwise(final int n) {
		Arrays.stream(screens).forEach(screen -> screen.rollUpBlockwise(n));
	}

	@Override
	public void rollDownBlockwise(final int n) {
		Arrays.stream(screens).forEach(screen -> screen.rollDownBlockwise(n));
	}

	/*
	 * public byte[] screenBinary() { return screen.toC64Binary(); }
	 */

	public byte[] charsetBinary() {
		return charset.toC64Binary();
	}

	/*
	 * public C64BinaryProvider binaryProviderCharsetFirst() { final byte[] binary = new byte[0x0800 + 0x0400]; final byte[] charsetBinary =
	 * charset.toC64Binary(); final byte[] screenBinary = screen.toC64Binary(); arraycopy(charsetBinary, 0, binary, 0, charsetBinary.length);
	 * arraycopy(screenBinary, 0, binary, 0x800, screenBinary.length); return () -> binary; }
	 * 
	 * public static C64CharsetMultiScreen fromBinaryCharsetFirst(final byte[] binary) { final byte[] charsetBinary = new byte[C64Charset.MAX_CHARACTERS *
	 * C64Char.HEIGHT_PIXELS]; final byte[] screenBinary = new byte[C64Screen.WIDTH_BLOCKS * C64Screen.HEIGHT_BLOCKS]; System.arraycopy(binary, 0,
	 * charsetBinary, 0, charsetBinary.length); System.arraycopy(binary, 0x800, screenBinary, 0, screenBinary.length); final C64Charset charset =
	 * C64Charset.fromBinary(charsetBinary); final C64Screen c64Screen = C64Screen.fromBinary(screenBinary); return new C64CharsetMultiScreen(charset,
	 * c64Screen); }
	 */
}
