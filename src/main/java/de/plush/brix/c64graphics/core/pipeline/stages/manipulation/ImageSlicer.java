package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.Dimension;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class ImageSlicer implements IPipelineStage<BufferedImage, BufferedImage[][]> {

	private final Dimension numSlices;
	private final Dimension sliceDimension;

	public ImageSlicer(final Dimension numSlices, final Dimension sliceDimension) {
		this.numSlices = new Dimension(numSlices);
		this.sliceDimension = new Dimension(sliceDimension);
	}

	@Override
	public BufferedImage[][] apply(final BufferedImage input) {
		final BufferedImage[][] images = new BufferedImage[numSlices.height][numSlices.width];
		for (int y = 0; y < numSlices.height; ++y) {
			for (int x = 0; x < numSlices.width; ++x) {
				images[y][x] = input.getSubimage(x * sliceDimension.width, y * sliceDimension.height, sliceDimension.width, sliceDimension.height);
			}
		}
		return images;
	}

}
