package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.*;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;

/**
 * Takes a uniformly, two-colored image and turns it into a grid of hires sprites.
 * This is very simple yet: No sprite zoom settings, picks up given pixel color and ignores rest.
 */
public class ImageToC64SpriteGridHires implements IPipelineStage<BufferedImage, C64SpriteGrid> {

	private final IPipelineStage<BufferedImage, C64SpriteGrid> pipeline;

	/**
	 * @param pixelColor
	 *            the color to turn into pixels. Everything else will be transparent.
	 * @param positionPixels
	 *            the position of the Sprite grid on the image, grid must not exceed image bounds.
	 * @param sizeSprites
	 *            the number of sprites in x and y direction, more than 8 sprites in x direction make no sense.
	 */
	public ImageToC64SpriteGridHires(final Color pixelColor, final Point positionPixels, final Dimension sizeSprites) {
		final var targetArea = new Rectangle(positionPixels, //
				new Dimension(C64Sprite.WIDTH_PIXELS * sizeSprites.width, C64Sprite.HEIGHT_PIXELS * sizeSprites.height));
		pipeline = new ImageCrop(targetArea)//
				.andThen(new ImageSlicer(sizeSprites, C64Sprite.sizePixels()))//
				.andThen(new ImageSpriteSizedSlicesToSpriteGridHires(pixelColor, sizeSprites));
	}

	@Override
	public C64SpriteGrid apply(final BufferedImage image) {
		return pipeline.apply(image);
	}

}
