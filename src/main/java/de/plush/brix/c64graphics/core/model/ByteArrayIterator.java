package de.plush.brix.c64graphics.core.model;

import java.util.*;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

public final class ByteArrayIterator implements Iterator<Byte> {

	private final byte[] bytes;

	int index = -1;

	@SuppressFBWarnings(value = "EI_EXPOSE_REP2", //
			justification = "Iterating over a copy would not iterate the current state, plus iterating is usually done to avoid explicit copying.")
	public ByteArrayIterator(final byte[] bytes) {
		this.bytes = bytes;
	}

	@Override
	public boolean hasNext() {
		return index + 1 < bytes.length;
	}

	@Override
	public Byte next() {
		try {
			return bytes[++index];
		} catch (final ArrayIndexOutOfBoundsException e) {
			throw new NoSuchElementException();
		}
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("cannot remove line from character");
	}
}