package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.*;

import java.awt.*;
import java.awt.geom.Area;
import java.util.function.*;

import de.plush.brix.c64graphics.core.model.*;

public enum CharsetFormats implements CharsetFormat {
	// common charset sizes:

	/** Common 1x1 charset with $20 as fill char */
	size1x1(new Dimension(C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS)) {
		@Override
		protected C64Screen createScreen(C64Screen screen) {
			screen = screen != null ? screen : new C64Screen((byte) 0x20);
			for (int t = 0; t < 0x100; ++t) {
				screen.setScreenCode(t % C64Screen.WIDTH_BLOCKS, t / C64Screen.WIDTH_BLOCKS, toUnsignedByte(t));
			}
			return screen;
		}
	},

	/** This is more typical of logos, where the fill character is $00 */
	size1x1_withFillChar$00(new Dimension(C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS)) {
		@Override
		protected C64Screen createScreen(C64Screen screen) {
			screen = screen != null ? screen : new C64Screen((byte) 0x00);
			for (int t = 0; t < 0x100; ++t) {
				screen.setScreenCode(t % C64Screen.WIDTH_BLOCKS, t / C64Screen.WIDTH_BLOCKS, toUnsignedByte(t));
			}
			return screen;
		}
	},

	/** This is more typical of logos, where the fill character is $FF */
	size1x1_withFillChar$FF(new Dimension(C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS)) {
		@Override
		protected C64Screen createScreen(C64Screen screen) {
			screen = screen != null ? screen : new C64Screen((byte) 0xFF);
			for (int t = 0; t < 0x100; ++t) {
				screen.setScreenCode(t % C64Screen.WIDTH_BLOCKS, t / C64Screen.WIDTH_BLOCKS, toUnsignedByte(t));
			}
			return screen;
		}
	},

	/**
	 * <code><pre class="tab">A = $01 $41</pre></code>
	 *
	 * @see <a href="http://csdb.dk/release/?id=97150">WOW Charset Master: http://csdb.dk/release/?id=97150</a>
	 */
	size1x2_wowCharMaster(new Dimension(1 * C64Char.WIDTH_PIXELS, 2 * C64Char.HEIGHT_PIXELS)) {

		@Override
		protected C64Screen createScreen(C64Screen screen) {
			screen = screen != null ? screen : new C64Screen((byte) 0x20);
			for (int charIndex = 0; charIndex < 0x100 / 4; ++charIndex) {
				final int x = charIndex % C64Screen.WIDTH_BLOCKS;
				final int y = charIndex / C64Screen.WIDTH_BLOCKS;
				screen.setScreenCode(x, 2 * y, toUnsignedByte(charIndex));
				// shifted char on every second line
				screen.setScreenCode(x, 2 * y + 1, toUnsignedByte(charIndex + 0x40));
			}
			return screen;
		}
	},
	/**
	 * <code><pre class="tab">A = $01<br>    $41</pre></code>
	 *
	 * @see <a href="http://csdb.dk/release/?id=97150">WOW Charset Master: http://csdb.dk/release/?id=97150</a>
	 */
	size2x1_wowCharMaster(new Dimension(2 * C64Char.WIDTH_PIXELS, 1 * C64Char.HEIGHT_PIXELS)) {
		@Override
		protected C64Screen createScreen(C64Screen screen) {
			screen = screen != null ? screen : new C64Screen((byte) 0x20);
			for (int charIndex = 0; charIndex < 0x100 / 4; ++charIndex) {
				final int x = charIndex % (C64Screen.WIDTH_BLOCKS / 2);
				final int y = charIndex / (C64Screen.WIDTH_BLOCKS / 2);
				screen.setScreenCode(2 * x, y, toUnsignedByte(charIndex));
				// shifted char to the right
				screen.setScreenCode(2 * x + 1, y, toUnsignedByte(charIndex + 0x40));
			}
			return screen;
		}
	},
	/**
	 * <code><pre class="tab">A = $01 $41<br>    $81 $c1</pre></code>
	 *
	 * @see <a href="http://csdb.dk/release/?id=97150">WOW Charset Master: http://csdb.dk/release/?id=97150</a>
	 */
	size2x2_wowCharMaster(new Dimension(2 * C64Char.WIDTH_PIXELS, 2 * C64Char.HEIGHT_PIXELS)) {
		@Override
		protected C64Screen createScreen(C64Screen screen) {
			screen = screen != null ? screen : new C64Screen((byte) 0x20);
			for (int charIndex = 0; charIndex < 0x100 / 4; ++charIndex) {
				final int x = charIndex % (C64Screen.WIDTH_BLOCKS / 2);
				final int y = charIndex / (C64Screen.WIDTH_BLOCKS / 2);
				screen.setScreenCode(2 * x + 0, 2 * y + 0, toUnsignedByte(charIndex + 0x00));
				screen.setScreenCode(2 * x + 1, 2 * y + 0, toUnsignedByte(charIndex + 0x40));
				screen.setScreenCode(2 * x + 0, 2 * y + 1, toUnsignedByte(charIndex + 0x80));
				screen.setScreenCode(2 * x + 1, 2 * y + 1, toUnsignedByte(charIndex + 0xc0));
			}
			return screen;
		}
	},

	/**
	 * <code><pre class="tab">@ = $01 $05 $09 $0D<br>    $02 $06 $0A $0E<br>    $03 $07 $0B $0F<br>    $04 $08 $0C $10</pre></code>
	 *
	 * fill char ="@" ($00), no dedicated space character<br>
	 * 
	 * There are not enough 4x4 blocks to cover a full alphabet, but for 16 blocks of 4x4chars
	 */
	size4x4_topDownLeftRight(new Dimension(2 * C64Char.WIDTH_PIXELS, 2 * C64Char.HEIGHT_PIXELS)) {
		@Override
		protected C64Screen createScreen(C64Screen screen) {
			screen = screen != null ? screen : new C64Screen((byte) 0x00);
			for (int charIndex = 0; charIndex < 0x100 / 16; ++charIndex) {
				final int x = charIndex % (C64Screen.WIDTH_BLOCKS / 4);
				final int y = charIndex / (C64Screen.WIDTH_BLOCKS / 4);
				screen.setScreenCode(4 * x + 0, 4 * y + 0, toUnsignedByte(16 * charIndex + 0));
				screen.setScreenCode(4 * x + 0, 4 * y + 1, toUnsignedByte(16 * charIndex + 1));
				screen.setScreenCode(4 * x + 0, 4 * y + 2, toUnsignedByte(16 * charIndex + 2));
				screen.setScreenCode(4 * x + 0, 4 * y + 3, toUnsignedByte(16 * charIndex + 3));

				screen.setScreenCode(4 * x + 1, 4 * y + 0, toUnsignedByte(16 * charIndex + 4));
				screen.setScreenCode(4 * x + 1, 4 * y + 1, toUnsignedByte(16 * charIndex + 5));
				screen.setScreenCode(4 * x + 1, 4 * y + 2, toUnsignedByte(16 * charIndex + 6));
				screen.setScreenCode(4 * x + 1, 4 * y + 3, toUnsignedByte(16 * charIndex + 7));

				screen.setScreenCode(4 * x + 2, 4 * y + 0, toUnsignedByte(16 * charIndex + 8));
				screen.setScreenCode(4 * x + 2, 4 * y + 1, toUnsignedByte(16 * charIndex + 9));
				screen.setScreenCode(4 * x + 2, 4 * y + 2, toUnsignedByte(16 * charIndex + 10));
				screen.setScreenCode(4 * x + 2, 4 * y + 3, toUnsignedByte(16 * charIndex + 11));

				screen.setScreenCode(4 * x + 3, 4 * y + 0, toUnsignedByte(16 * charIndex + 12));
				screen.setScreenCode(4 * x + 3, 4 * y + 1, toUnsignedByte(16 * charIndex + 13));
				screen.setScreenCode(4 * x + 3, 4 * y + 2, toUnsignedByte(16 * charIndex + 14));
				screen.setScreenCode(4 * x + 3, 4 * y + 3, toUnsignedByte(16 * charIndex + 15));
			}
			return screen;
		}
	},

	/**
	 * <code><pre class="tab">@ = $01 $02 $03 $04<br>    $05 $06 $07 $08<br>    $09 $0A $0B $0C<br>    $0D $0E $0F $10</pre></code>
	 *
	 * fill char ="@" ($00), no dedicated space character<br>
	 * 
	 * There are not enough 4x4 blocks to cover a full alphabet, but for 16 blocks of 4x4chars
	 */
	size4x4_leftRightTopDown(new Dimension(2 * C64Char.WIDTH_PIXELS, 2 * C64Char.HEIGHT_PIXELS)) {
		@Override
		protected C64Screen createScreen(C64Screen screen) {
			screen = screen != null ? screen : new C64Screen((byte) 0x00);
			for (int charIndex = 0; charIndex < 0x100 / 16; ++charIndex) {
				final int x = charIndex % (C64Screen.WIDTH_BLOCKS / 4);
				final int y = charIndex / (C64Screen.WIDTH_BLOCKS / 4);
				screen.setScreenCode(4 * x + 0, 4 * y + 0, toUnsignedByte(16 * charIndex + 0));
				screen.setScreenCode(4 * x + 1, 4 * y + 0, toUnsignedByte(16 * charIndex + 1));
				screen.setScreenCode(4 * x + 2, 4 * y + 0, toUnsignedByte(16 * charIndex + 2));
				screen.setScreenCode(4 * x + 3, 4 * y + 0, toUnsignedByte(16 * charIndex + 3));

				screen.setScreenCode(4 * x + 0, 4 * y + 1, toUnsignedByte(16 * charIndex + 4));
				screen.setScreenCode(4 * x + 1, 4 * y + 1, toUnsignedByte(16 * charIndex + 5));
				screen.setScreenCode(4 * x + 2, 4 * y + 1, toUnsignedByte(16 * charIndex + 6));
				screen.setScreenCode(4 * x + 3, 4 * y + 1, toUnsignedByte(16 * charIndex + 7));

				screen.setScreenCode(4 * x + 0, 4 * y + 2, toUnsignedByte(16 * charIndex + 8));
				screen.setScreenCode(4 * x + 1, 4 * y + 2, toUnsignedByte(16 * charIndex + 9));
				screen.setScreenCode(4 * x + 2, 4 * y + 2, toUnsignedByte(16 * charIndex + 10));
				screen.setScreenCode(4 * x + 3, 4 * y + 2, toUnsignedByte(16 * charIndex + 11));

				screen.setScreenCode(4 * x + 0, 4 * y + 3, toUnsignedByte(16 * charIndex + 12));
				screen.setScreenCode(4 * x + 1, 4 * y + 3, toUnsignedByte(16 * charIndex + 13));
				screen.setScreenCode(4 * x + 2, 4 * y + 3, toUnsignedByte(16 * charIndex + 14));
				screen.setScreenCode(4 * x + 3, 4 * y + 3, toUnsignedByte(16 * charIndex + 15));
			}
			return screen;
		}
	},

	// size2x3(new Dimension(2 * C64Char.WIDTH_PIXELS, 3 * C64Char.HEIGHT_PIXELS))//
	// , size3x2(new Dimension(3 * C64Char.WIDTH_PIXELS, 2 * C64Char.HEIGHT_PIXELS))//

	/**
	 * <code><pre class="tab">@ = $01 $04 $07<br>    $02 $05 $08<br>    $03 $06 $09</pre></code>
	 *
	 * space char = "@" ($00)
	 *
	 * @see <a href="http://csdb.dk/release/?id=97150">WOW Charset Master: http://csdb.dk/release/?id=97150</a>
	 */
	size3x3_wowCharMaster(new Dimension(3 * C64Char.WIDTH_PIXELS, 3 * C64Char.HEIGHT_PIXELS)) {
		@Override
		protected C64Screen createScreen(C64Screen screen) {
			screen = screen != null ? screen : new C64Screen((byte) 0x00);
			for (int charIndex = 0; charIndex < 0x100 / 9; ++charIndex) {
				final int x = charIndex % (C64Screen.WIDTH_BLOCKS / 3);
				final int y = charIndex / (C64Screen.WIDTH_BLOCKS / 3);
				screen.setScreenCode(3 * x + 0, 3 * y + 0, toUnsignedByte(9 * charIndex + 0));
				screen.setScreenCode(3 * x + 0, 3 * y + 1, toUnsignedByte(9 * charIndex + 1));
				screen.setScreenCode(3 * x + 0, 3 * y + 2, toUnsignedByte(9 * charIndex + 2));

				screen.setScreenCode(3 * x + 1, 3 * y + 0, toUnsignedByte(9 * charIndex + 3));
				screen.setScreenCode(3 * x + 1, 3 * y + 1, toUnsignedByte(9 * charIndex + 4));
				screen.setScreenCode(3 * x + 1, 3 * y + 2, toUnsignedByte(9 * charIndex + 5));

				screen.setScreenCode(3 * x + 2, 3 * y + 0, toUnsignedByte(9 * charIndex + 6));
				screen.setScreenCode(3 * x + 2, 3 * y + 1, toUnsignedByte(9 * charIndex + 7));
				screen.setScreenCode(3 * x + 2, 3 * y + 2, toUnsignedByte(9 * charIndex + 8));
			}
			return screen;
		}
	},

	// , size3x4(new Dimension(3 * C64Char.WIDTH_PIXELS, 4 * C64Char.HEIGHT_PIXELS))//
	// , size4x3(new Dimension(4 * C64Char.WIDTH_PIXELS, 3 * C64Char.HEIGHT_PIXELS))//
	// , size4x4(new Dimension(4 * C64Char.WIDTH_PIXELS, 4 * C64Char.HEIGHT_PIXELS))//

	/**
	 * <style> #matrix A { background: lightgreen;}</style> <style> #matrix E { background:lightblue;}</style> </style>
	 * <p id="matrix">
	 * GigaCad+ movie frames are charsets <br>
	 * for a screen matrix of dimension 20x12, <i>fillChar = 00</i>, firstCharIndex = <A>01</A>, lastCharIndex = <E>F0</E>
	 * </p>
	 *
	 * <pre id="matrix">
	 * <A>01</A> 02 03 04 ... 12 13 14 <i>00 00 00 ... 00</i>
	 * 15 17 18 19 ... 26 27 28 <i>00 00 00 ... 00</i>
	 * 29 2A 2B 2C ... 2C 2D 3E <i>00 00 00 ... 00</i>
	 * ...
	 * C9 CA CB CC ... DB DC DE <i>00 00 00 ... 00</i>
	 * DD DE DF E1 ... ED EF <E>F0</E> <i>00 00 00 ... 00</i>
	 * <i>00 00 00 00 ... 00 00 00 00 00 00 ... 00
	 * 00 00 00 00 ... 00 00 00 00 00 00 ... 00
	 * ...
	 * 00 00 00 00 ... 00 00 00 00 00 00 ... 00</i>
	 * </pre>
	 */
	size20x12_GigaCadMovieFrame(new Dimension(20 * C64Char.WIDTH_PIXELS, 12 * C64Char.HEIGHT_PIXELS)) {
		@Override
		protected C64Screen createScreen(final C64Screen screen) {
			return MatrixCharsetFormat.createLeftToRightTopToBottomMatrixScreen(dim, (byte) 0x00, (byte) 0x01, (byte) 0xF0, screen);
		}
	},

	;

	final Dimension dim;

	private CharsetFormats(final Dimension dim) {
		this.dim = dim;
	}

	protected abstract C64Screen createScreen(C64Screen screen);

	@Override
	public final C64Screen createScreen() {
		return createScreen(null);
	}

	@Override
	public final Area areaOfBlockPositions() {
		return getAreaBlocks(this::createScreen);
	}

	/**
	 * <style> #matrix A { background: lightgreen;}</style> <style> #matrix E { background:lightblue;}</style> </style>
	 * <p id="matrix">
	 * Example: Screen matrix of dimension 16x16, <i>fillChar = 00</i>, firstCharIndex = <A>00</A>, lastCharIndex = <E>FF</E>
	 * </p>
	 *
	 * <pre id="matrix">
	 * <A>00</A> 01 02 03 ... 0D 0E 0F <i>00 00 00 ... 00</i>
	 * 10 11 12 13 ... 1D 1E 1F <i>00 00 00 ... 00</i>
	 * 20 21 22 23 ... 2D 2E 2F <i>00 00 00 ... 00</i>
	 * ...
	 * E0 E1 E2 E3 ... ED EE EF <i>00 00 00 ... 00</i>
	 * F0 F1 F2 F3 ... FD FE <E>FF</E> <i>00 00 00 ... 00</i>
	 * <i>00 00 00 00 ... 00 00 00 00 00 00 ... 00</i>
	 * <i>00 00 00 00 ... 00 00 00 00 00 00 ... 00</i>
	 * ...
	 * <i>00 00 00 00 ... 00 00 00 00 00 00 ... 00</i>
	 * </pre>
	 */
	public static CharsetFormat withLeftToRightTopToBottomMatrix(final Dimension dim, final byte fillChar, final byte firstCharIndex,
			final byte lastCharIndex) {
		return new MatrixCharsetFormat(dim, fillChar, firstCharIndex, lastCharIndex, MatrixCharsetFormat::createLeftToRightTopToBottomMatrixScreen);
	}

	/**
	 * <style> #matrix A { background: lightgreen;}</style> <style> #matrix E { background:lightblue;}</style> </style>
	 * <p id="matrix">
	 * Example: Screen matrix of dimension 16x16, <i>fillChar = 00</i>, firstCharIndex = <A>00</A>, lastCharIndex = <E>FF</E>
	 * </p>
	 *
	 * <pre id="matrix">
	 * <A>00</A> 10 20 30 ... D0 E0 F0 <i>00 00 00 ... 00</i>
	 * 01 11 21 31 ... D1 E1 F1 <i>00 00 00 ... 00</i>
	 * 02 12 22 32 ... D2 E2 F2 <i>00 00 00 ... 00</i>
	 * ...
	 * 0E 1E 2E 3E ... DE EE FE <i>00 00 00 ... 00</i>
	 * 0F 1F 2F 3F ... DF EF <E>FF</E> <i>00 00 00 ... 00</i>
	 * <i>00 00 00 00 ... 00 00 00 00 00 00 ... 00</i>
	 * <i>00 00 00 00 ... 00 00 00 00 00 00 ... 00</i>
	 * ...
	 * <i>00 00 00 00 ... 00 00 00 00 00 00 ... 00</i>
	 * </pre>
	 */
	public static CharsetFormat withTopToBottomLeftToRightMatrix(final Dimension dim, final byte fillChar, final byte firstCharIndex,
			final byte lastCharIndex) {
		return new MatrixCharsetFormat(dim, fillChar, firstCharIndex, lastCharIndex, MatrixCharsetFormat::createTopToBottomLeftToRightMatrixScreen);
	}

	// I admit this is the code of a mad man. Pretty dirty hack in my book.
	static Area getAreaBlocks(final Consumer<C64Screen> screenCreator) {
		final Area[] area = new Area[] { new Area() }; // may be overwritten during init (be secure if C64Screen implementation changes)
		// subclass/patch setScreenCode to fill the area when a screen code is set. Calling "new" might already do that during init.
		final C64Screen screen = new C64Screen((byte) 0) {
			@Override
			public void setScreenCode(final int x, final int y, final byte screenCode) {
				super.setScreenCode(x, y, screenCode);
				area[0].add(new Area(new Rectangle(x, y, 1, 1)));
			}
		};
		area[0] = new Area(); // reset to make sure that this is blank after C64Screen's initialization (calling "new" above).
		screenCreator.accept(screen); // will fill the area, as the screenCreator is expected to call "setScreenCode".
		return area[0];
	}

	private static class MatrixCharsetFormat implements CharsetFormat {

		@FunctionalInterface
		private interface MatrixCreator {
			C64Screen createMatrix(Dimension dim, byte fillChar, byte firstCharIndex, byte lastCharIndex, C64Screen screen);
		}

		private final Dimension d;

		private final byte fillChar;

		private final byte firstCharIndex;

		private final byte lastCharIndex;

		private final MatrixCreator matrixCreator;

		MatrixCharsetFormat(final Dimension d, final byte fillChar, final byte firstCharIndex, final byte lastCharIndex, final MatrixCreator matrixCreator) {
			this.d = d;
			this.fillChar = fillChar;
			this.firstCharIndex = firstCharIndex;
			this.lastCharIndex = lastCharIndex;
			this.matrixCreator = matrixCreator;
		}

		@Override
		public final C64Screen createScreen() {
			return createScreen(null);
		}

		@Override
		public Area areaOfBlockPositions() {
			return CharsetFormats.getAreaBlocks(this::createScreen);
		}

		private C64Screen createScreen(final C64Screen screen) {
			return matrixCreator.createMatrix(d, fillChar, firstCharIndex, lastCharIndex, screen);
		}

		protected static C64Screen createLeftToRightTopToBottomMatrixScreen(final Dimension dim, final byte fillChar, final byte firstCharIndex,
				final byte lastCharIndex, final C64Screen screen) {
			final int startIndex = toPositiveInt(firstCharIndex);
			final IntUnaryOperator indexToX = (index) -> (index - startIndex) % dim.width;
			final IntUnaryOperator indexToY = (index) -> (index - startIndex) / dim.width;
			return createMatrixScreen(fillChar, firstCharIndex, lastCharIndex, screen, indexToX, indexToY);
		}

		private static C64Screen createTopToBottomLeftToRightMatrixScreen(final Dimension dim, final byte fillChar, final byte firstCharIndex,
				final byte lastCharIndex, final C64Screen screen) {
			final int startIndex = toPositiveInt(firstCharIndex);
			final IntUnaryOperator indexToX = (index) -> (index - startIndex) / dim.width;
			final IntUnaryOperator indexToY = (index) -> (index - startIndex) % dim.width;
			return createMatrixScreen(fillChar, firstCharIndex, lastCharIndex, screen, indexToX, indexToY);
		}

		private static C64Screen createMatrixScreen(final byte fillChar, final byte firstCharIndex, final byte lastCharIndex, C64Screen screen,
				final IntUnaryOperator xFromStartAndIndex, final IntUnaryOperator yFromStartAndIndex) {
			screen = screen != null ? screen : new C64Screen(fillChar);
			final int _firstCharIndex = toPositiveInt(firstCharIndex);
			final int _lastCharIndex = toPositiveInt(lastCharIndex);
			for (int charIndex = _firstCharIndex; charIndex <= _lastCharIndex; ++charIndex) {
				final int x = xFromStartAndIndex.applyAsInt(charIndex);
				final int y = yFromStartAndIndex.applyAsInt(charIndex);
				screen.setScreenCode(x, y, toUnsignedByte(charIndex));
			}
			return screen;
		}
	}

}
