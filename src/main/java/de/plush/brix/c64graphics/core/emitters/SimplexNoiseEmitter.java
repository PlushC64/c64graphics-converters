package de.plush.brix.c64graphics.core.emitters;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.function.*;

public class SimplexNoiseEmitter extends AbstractGeneratedFramesEmitter {

	protected final Dimension dimension;

	private final IntFunction<OpenSimplexNoise> noiseGeneratorFactory;

	private final DoubleFunction<Color> valueToColor;

	private final IntUnaryOperator noiseGeneratorSeed;

	private final ToDoubleBiFunction<Point, Integer> scaleX;

	private final ToDoubleBiFunction<Point, Integer> scaleY;

	private final ToDoubleBiFunction<Point, Integer> scaleZ;

	public SimplexNoiseEmitter(final int frames, final Dimension dimension, final double cutOff, final double xScale,
			final double yScale,
			final double timeScale, final Color background, final Color foreground) {
		this(frames, dimension //
				, (value) -> Math.abs(value) > cutOff ? background : foreground//
				, (frameNr) -> 0 //
				, (pos, frame) -> xScale * pos.x //
				, (pos, frame) -> yScale * pos.y//
				, (pos, frame) -> timeScale * frame//
		);
	}

	public SimplexNoiseEmitter(final int frames,
			final Dimension dimension, //
			final DoubleFunction<Color> valueToColor, //
			final IntUnaryOperator noiseGeneratorSeed, //
			final ToDoubleBiFunction<Point, Integer> scaleX, //
			final ToDoubleBiFunction<Point, Integer> scaleY, //
			final ToDoubleBiFunction<Point, Integer> scaleZ) {
		super(frames);
		this.dimension = new Dimension(dimension);
		this.valueToColor = valueToColor;
		this.noiseGeneratorSeed = noiseGeneratorSeed;
		this.scaleX = scaleX;
		this.scaleY = scaleY;
		this.scaleZ = scaleZ;
		noiseGeneratorFactory = (seed) -> new OpenSimplexNoise(seed);
	}

	@Override
	protected BufferedImage createImage(final int frameNr) {
		final BufferedImage image = new BufferedImage(dimension.width, dimension.height, BufferedImage.TYPE_INT_ARGB);

		final OpenSimplexNoise noiseGenerator = noiseGeneratorFactory.apply(noiseGeneratorSeed.applyAsInt(frameNr));

		for (int y = 0; y < image.getHeight(); ++y) {
			for (int x = 0; x < image.getWidth(); ++x) {
				final var pos = new Point(x, y);
				final var val = noiseGenerator.eval(//
						scaleX.applyAsDouble(pos, frameNr), //
						scaleY.applyAsDouble(pos, frameNr), //
						scaleZ.applyAsDouble(pos, frameNr) //
				);
				final var rgb = valueToColor.apply(val).getRGB();//
				image.setRGB(x, y, rgb);
			}
		}
		return image;
	}
}
