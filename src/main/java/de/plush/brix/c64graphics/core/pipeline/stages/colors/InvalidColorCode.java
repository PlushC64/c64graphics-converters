package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import static de.plush.brix.c64graphics.core.util.Utils.*;

public class InvalidColorCode extends RuntimeException {

	public InvalidColorCode(final byte code, final int numColors) {
		this(code, numColors, "");
	}

	public InvalidColorCode(final byte code, final int numColors, final String message) {
		super(toHexString(toPositiveInt(code)) + " is not a valid color code: Permitted values are 0x00..0x" + toHexString((byte) (numColors - 1))
				+ "(inclusive). "
				+ message);
	}

	public static byte check(final byte colorCode, final int numColors) {
		return check(colorCode, numColors, "");
	}

	public static byte check(final byte colorCode, final int numColors, final String message) {
		if (toPositiveInt(colorCode) >= numColors) { throw new InvalidColorCode(colorCode, numColors, message); }
		return colorCode;
	}

}
