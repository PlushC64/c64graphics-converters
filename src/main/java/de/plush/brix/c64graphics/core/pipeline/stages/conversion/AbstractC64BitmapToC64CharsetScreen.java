package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.model.C64Charset.MAX_CHARACTERS;

import java.util.*;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.ColorDistanceMeasure;
import de.plush.brix.c64graphics.core.util.collections.HashBag;

public abstract class AbstractC64BitmapToC64CharsetScreen implements IPipelineStage<C64Bitmap, C64CharsetScreen> {

	protected final ColorDistanceMeasure distanceMeasure;

	AbstractC64BitmapToC64CharsetScreen(final ColorDistanceMeasure distanceMeasure) {
		this.distanceMeasure = distanceMeasure;
	}

	protected abstract Function<HashBag<C64Char>, List<C64Char[]>> createCharacterReplacer();

	@Override
	public C64CharsetScreen apply(final C64Bitmap bitmap) {

		final HashBag<C64Char> blocks = getBlocks(bitmap);
		final List<C64Char[]> replacements = createReplacements(blocks);

		final var simplifiedBitmap = simplifiedBitmap(bitmap, replacements);
		final HashBag<C64Char> reducedBlocks = getBlocks(simplifiedBitmap);

		final var charset = new C64Charset(reducedBlocks.distinctValues());
		final var screen = createScreen(simplifiedBitmap, charset);
		return new C64CharsetScreen(charset, screen);
	}

	private static C64Bitmap simplifiedBitmap(final C64Bitmap bitmap, final List<C64Char[]> replacements) {
		final var result = new C64Bitmap(bitmap);
		for (final var replacement : replacements) {
			for (int yBlock = 0; yBlock < C64Screen.HEIGHT_BLOCKS; ++yBlock) {
				for (int xBlock = 0; xBlock < C64Screen.WIDTH_BLOCKS; ++xBlock) {
					final var block = bitmap.blockAt(xBlock, yBlock);
					if (replacement[0].equals(block)) {
						result.setBlock(xBlock, yBlock, replacement[1]);
					}
				}
			}
		}
		return result;
	}

	private static C64Screen createScreen(final C64Bitmap bitmap, final C64Charset charset) {
		final var screen = new C64Screen((byte) 0);
		for (int yBlock = 0; yBlock < C64Screen.HEIGHT_BLOCKS; ++yBlock) {
			for (int xBlock = 0; xBlock < C64Screen.WIDTH_BLOCKS; ++xBlock) {
				final var block = bitmap.blockAt(xBlock, yBlock);
				final byte screenCode = charset.screenCodeOf(block)//
						.orElseThrow(() -> new IllegalStateException("char not found in generated charset: \n" + block));
				screen.setScreenCode(xBlock, yBlock, screenCode);
			}
		}
		return screen;
	}

	private List<C64Char[]> createReplacements(final HashBag<C64Char> characters) {
		List<C64Char[]> replacements = new ArrayList<>();
		if (characters.sizeDistinct() > 1) {
			System.out.println("number of characters: " + characters.sizeDistinct() + (characters.sizeDistinct() > MAX_CHARACTERS ? " (reducing...)" : ""));
			if (characters.sizeDistinct() > MAX_CHARACTERS) {
				final var replacer = createCharacterReplacer();
				replacements = replacer.apply(characters);
			}
		}
		return replacements;
	}

	private static HashBag<C64Char> getBlocks(final C64Bitmap bitmap) {
		final HashBag<C64Char> characters = HashBag.empty();
		for (int yBlock = 0; yBlock < C64Screen.HEIGHT_BLOCKS; ++yBlock) {
			for (int xBlock = 0; xBlock < C64Screen.WIDTH_BLOCKS; ++xBlock) {
				final var block = bitmap.blockAt(xBlock, yBlock);
				characters.add(block);
			}
		}
		return characters;
	}

}