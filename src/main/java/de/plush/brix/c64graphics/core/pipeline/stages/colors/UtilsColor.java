package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import java.awt.Color;
import java.util.*;
import java.util.function.ToDoubleFunction;

public class UtilsColor {

	public static final Comparator<Color> SORT_BY_BRIGHTNESS = Comparator
			.<Color> comparingDouble(color -> Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null)[2]);

	public static final ToDoubleFunction<Color> EXTRACT_RED = new ExtractRedFunction();

	public static final ToDoubleFunction<Color> EXTRACT_BLUE = new ExtractGreenFunction();

	public static final ToDoubleFunction<Color> EXTRACT_GREEN = new ExtractBlueFunction();

	public static final ToDoubleFunction<Color> EXTRACT_ALPHA = new ExtractAlphaFunction();

	protected UtilsColor() {
		throw new UnsupportedOperationException();
	}

	public static double rms(final Color expected, final Color observed) {
		final double r = Math.pow(expected.getRed() - observed.getRed(), 2);
		final double g = Math.pow(expected.getGreen() - observed.getGreen(), 2);
		final double b = Math.pow(expected.getBlue() - observed.getBlue(), 2);
		final double a = Math.pow(expected.getAlpha() - observed.getAlpha(), 2);
		return Math.sqrt(1d / 4 * (r + g + b + a));
	}

	public static Color mean(final List<Color> colors) {
		final int redMean = (int) colors.stream().mapToDouble(UtilsColor.EXTRACT_RED).average().orElse(0d);
		final int greenMean = (int) colors.stream().mapToDouble(UtilsColor.EXTRACT_GREEN).average().orElse(0d);
		final int blueMean = (int) colors.stream().mapToDouble(UtilsColor.EXTRACT_BLUE).average().orElse(0d);
		final int alphaMean = (int) colors.stream().mapToDouble(UtilsColor.EXTRACT_ALPHA).average().orElse(0d);
		return new Color(redMean, greenMean, blueMean, alphaMean);
	}

	private static final class ExtractRedFunction implements ToDoubleFunction<Color> {
		@Override
		public double applyAsDouble(final Color c) {
			return c.getRed();
		}
	}

	private static final class ExtractGreenFunction implements ToDoubleFunction<Color> {
		@Override
		public double applyAsDouble(final Color c) {
			return c.getGreen();
		}
	}

	private static final class ExtractBlueFunction implements ToDoubleFunction<Color> {
		@Override
		public double applyAsDouble(final Color c) {
			return c.getBlue();
		}
	}

	private static final class ExtractAlphaFunction implements ToDoubleFunction<Color> {
		@Override
		public double applyAsDouble(final Color c) {
			return c.getAlpha();
		}
	}
}
