package de.plush.brix.c64graphics.core._experimental.wavelet;

import static java.lang.Math.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.function.*;

class MonochromeHaarWaveletImage {

	private final Dimension dim;

	private final double[][] data;

	// private int oddIterationBits;

	private MonochromeHaarWaveletImage nextIteration;

	public MonochromeHaarWaveletImage(final BufferedImage image) {
		this(dataFromImage(image));
	}

	// package private constructor, not for public use
	MonochromeHaarWaveletImage(final double[][] data) {
		dim = new Dimension(data.length, data[0].length);
		this.data = transformed(data);
		if (dim.width > 3 && dim.height > 3) {
			final double[][] subImageData = new double[dim.width / 2][dim.height / 2];
			for (int y = 0; y < dim.height / 2; y++) {
				for (int x = 0; x < dim.width / 2; x++) {
					subImageData[x][y] = this.data[x][y];
				}
			}
			nextIteration = new MonochromeHaarWaveletImage(subImageData);
		}
	}

	public Dimension dimension() {
		return new Dimension(dim);
	}

	public int levels() {
		return nextIteration == null ? 0 : nextIteration.levels() + 1;
	}

	public void forEachCoefficient(final IntPredicate levelMatcher, final DoubleBinaryOperator modifier) {
		forEachCoefficient(0, levelMatcher, modifier);
	}

	private void forEachCoefficient(final int level, final IntPredicate levelMatcher, final DoubleBinaryOperator modifier) {
		if (levelMatcher.test(level)) {
			for (int y = 0; y < dim.height; ++y) {
				for (int x = 0; x < dim.width; ++x) {
					if (x > dim.width / 2 || y > dim.height / 2) {
						data[x][y] = modifier.applyAsDouble(level, data[x][y]);
					}
				}
			}
		}
		if (nextIteration != null) {
			nextIteration.forEachCoefficient(level + 1, levelMatcher, modifier);
		}
	}

	void modifyValueAt(final int x, final int y, final DoubleUnaryOperator op) {
		data[x][y] = op.applyAsDouble(data[x][y]);
	}

	double valueAt(final int x, final int y) {
		return data[x][y];
	}

	// void modifyOddIterationBits(final IntUnaryOperator op) {
	// oddIterationBits = op.applyAsInt(oddIterationBits);
	// }
	//
	// int oddIterationBits() {
	// return oddIterationBits;
	// }

	private static double[][] dataFromImage(final BufferedImage image) {
		if (image.getType() != BufferedImage.TYPE_BYTE_GRAY) { throw new IllegalArgumentException("Expecetd image type BufferedImage.TYPE_BYTE_GRAY"); }
		final Dimension dim = new Dimension(image.getWidth(), image.getHeight());
		final double[][] data = new double[dim.width][dim.height];
		for (int y = 0; y < dim.height; y++) {
			for (int x = 0; x < dim.width; x++) {
				// data[x][y] = image.getRGB(x, y) & 0x000000ff;
				data[x][y] = image.getRaster().getPixel(x, y, new int[4])[0];
			}
		}
		return data;
	}

	private boolean contains(final int x, final int y) {
		return new Rectangle(dim).contains(x, y);
	}

	private double[][] retransformedRecursive() {
		if (nextIteration == null) {
			return retransformed(data); // direct
		}
		// resolve nextIteration, use next Iteration as pivots, add own coefficients
		final double[][] nextIterationRetransformed = nextIteration.retransformedRecursive(); // i.e. 2x2
		final double[][] result = new double[dim.width][dim.height]; // i.e. 4x4
		for (int y = 0; y < dim.height; ++y) {
			for (int x = 0; x < dim.width; ++x) {
				result[x][y] = nextIteration.contains(x, y) ? nextIterationRetransformed[x][y] : data[x][y];
			}
		}
		return retransformed(result); // resolve mix of resolved nextIteration as pivots and own coeffcients
	}

	private double[][] transformed(final double[][] data) {
		return transformedRows(transformedColumns(data));
	}

	private double[][] transformedColumns(final double[][] data) {
		final double[][] result = new double[dim.width][dim.height];
		for (int y = 0; y < dim.height; ++y) {
			for (int x = 0; x < dim.width / 2; ++x) {
				final double left = data[2 * x][y];
				final double right = data[2 * x + 1][y];
				result[x][y] = (left + right) / 2.0;
				result[dim.width / 2 + x][y] = (left - right) / 2.0;
			}
			if (dim.width % 2 != 0) { // odd width: last column needs special treatment (just save coeff to mirror image (left) pixel)
				result[dim.width - 1][y] = (data[dim.width - 1][y] - data[dim.width - 2][y]) / 2.0;
			}
		}
		return result;
	}

	private double[][] transformedRows(final double[][] data) {
		final double[][] result = new double[dim.width][dim.height];
		for (int x = 0; x < dim.width; ++x) {
			for (int y = 0; y < dim.height / 2; ++y) {
				final double top = data[x][2 * y];
				final double bottom = data[x][2 * y + 1];
				result[x][y] = (top + bottom) / 2.0;
				result[x][dim.height / 2 + y] = (top - bottom) / 2.0;
			}
			if (dim.height % 2 != 0) { // odd height: last column needs special treatment (just save coeff to mirror image (upper) pixel)
				result[x][dim.height - 1] = (data[x][dim.height - 1] - data[x][dim.height - 2]) / 2.0;
			}
		}
		return result;
	}

	private double[][] retransformed(final double[][] data) {
		return retransformedColumns(retransformedRows(data));
	}

	private double[][] retransformedRows(final double[][] data) {
		final double[][] result = new double[dim.width][dim.height];
		for (int x = 0; x < dim.width; ++x) {
			for (int y = 0; y < dim.height / 2; ++y) {
				final double pivot = data[x][y];
				final double coeff = data[x][dim.height / 2 + y];
				result[x][2 * y] = pivot + coeff;
				result[x][2 * y + 1] = pivot - coeff;
			}
			if (dim.height % 2 != 0) { // odd height: last column needs special treatment (use coeff to mirror image (upper) pixel)
				final double pivot = result[x][dim.height - 2];
				final double coeff = data[x][dim.height - 1];
				result[x][dim.height - 1] = pivot + coeff * 2;
			}
		}
		return result;
	}

	private double[][] retransformedColumns(final double[][] data) {
		final double[][] result = new double[dim.width][dim.height];
		for (int y = 0; y < dim.height; ++y) {
			for (int x = 0; x < dim.width / 2; ++x) {
				final double pivot = data[x][y];
				final double coeff = data[dim.width / 2 + x][y];
				result[2 * x][y] = pivot + coeff;
				result[2 * x + 1][y] = pivot - coeff;
			}
			if (dim.width % 2 != 0) { // odd height: last column needs special treatment (use coeff to mirror image (upper) pixel)
				final double pivot = result[dim.width - 2][y];
				final double coeff = data[dim.width - 1][y];
				result[dim.width - 1][y] = pivot + coeff * 2;
			}
		}
		return result;
	}

	public BufferedImage toImage() {
		return dataToImage(retransformedRecursive());
	}

	private BufferedImage dataToImage(final double[][] data) {
		final BufferedImage image = new BufferedImage(dim.width, dim.height, BufferedImage.TYPE_BYTE_GRAY);
		for (int y = 0; y < dim.height; ++y) {
			for (int x = 0; x < dim.width; ++x) {
				final int value = clamp((int) data[x][y]);
				checkColor(x, y, value);
				image.getRaster().setPixel(x, y, new int[] { value });
			}
		}
		return image;
	}

	public BufferedImage imageInterpretation() {
		return imageInterpretation(levels());
	}

	public BufferedImage imageInterpretation(int iterations) {
		if (iterations < 0) { return toImage(); }
		final BufferedImage image = new BufferedImage(dim.width, dim.height, BufferedImage.TYPE_BYTE_GRAY);
		final BufferedImage subImage = nextIteration == null
				? null //@formatter:off
													 : iterations < 1 ? nextIteration.toImage() //
													 : nextIteration.imageInterpretation(--iterations); //@formatter:on
		for (int y = 0; y < dim.height; ++y) {
			for (int x = 0; x < dim.width; ++x) {
				final int value;
				if (x < dim.width / 2 && y < dim.height / 2) {
					if (subImage == null) {
						value = clamp((int) data[x][y]);
						checkColor(x, y, value);
					} else {
						value = subImage.getRaster().getPixel(x, y, new int[1])[0];
					}
				} else {
					value = clamp(0x80 + (byte) data[x][y]);
					checkColor(x, y, value);
				}
				image.getRaster().setPixel(x, y, new int[] { value });
			}
		}
		return image;
	}

	private static int clamp(final int value) {
		return clamp(0, value, 0xff);
	}

	private static int clamp(final int min, final int value, final int max) {
		return min(max, max(min, value));
	}

	private static void checkColor(final int x, final int y, final int value) {
		if (value < 0 || value > 0xFF) {
			throw new IllegalArgumentException("Color parameter outside of expected range: " + new Point(x, y) + " -> " + Integer.toHexString(value));
		}

	}

}
