package de.plush.brix.c64graphics.core.util.collections;

import static java.lang.Math.min;
import static java.util.Comparator.naturalOrder;
import static java.util.stream.Collectors.toList;

import java.util.*;
import java.util.stream.Stream;

import de.plush.brix.c64graphics.core.util.*;

public class HashBag<T> {

	private final ObjectIntHashMap<T> itemToCount;

	private int size;

	public HashBag() {
		itemToCount = new ObjectIntHashMap<>();
	}

	public HashBag(final int initialCapacity) {
		itemToCount = new ObjectIntHashMap<>(initialCapacity);
	}

	public static <T> HashBag<T> empty() {
		return new HashBag<>();
	}

	@SuppressWarnings("unchecked")
	public static <T> HashBag<T> of(final T... items) {
		final var bag = new HashBag<T>();
		bag.addAll(items);
		return bag;
	}

	public static <T> HashBag<T> of(final Collection<T> items) {
		final var bag = new HashBag<T>();
		bag.addAll(items);
		return bag;
	}

	public boolean add(final T item) {
		return add(item, 1);
	}

	public boolean add(final CountableObject<T> count) {
		return add(count.objValue, Math.toIntExact(count.count));
	}

	private boolean add(final T item, final int amountToAdd) {
		if (amountToAdd < 0) { throw new IllegalArgumentException("Cannot add a negative amount of items"); }
		if (amountToAdd == 0) { return false; }
		itemToCount.update(item, amountToAdd, currentAmount -> currentAmount + amountToAdd);
		size += amountToAdd;
		return true;
	}

	@SuppressWarnings("unchecked")
	public boolean addAll(final T... items) {
		if (items.length == 0) { return false; }
		for (final var item : items) {
			add(item);
		}
		return true;
	}

	public boolean addAll(final Collection<T> items) {
		if (items.isEmpty()) { return false; }
		items.stream().forEach(this::add);
		return true;
	}

	@SuppressWarnings("unchecked")
	public boolean addAll(final CountableObject<T>... counts) {
		if (counts.length == 0) { return false; }
		final int sizeBefore = size;
		for (final var count : counts) {
			add(count);
		}
		return size != sizeBefore;
	}

	public boolean addAll(final HashBag<T> other) {
		if (other.isEmpty()) { return false; }
		// to be more efficient, don't use any mapping to CountableItem<T> and get+put, instead update the internals directly
		other.itemToCount.entryStream()// assuming backing mask does not contain 0-amout entries
				.forEach(entry -> itemToCount.update(entry.getOne(), entry.getTwo(), currentAmount -> currentAmount + entry.getTwo()));
		size += other.size;
		return true;
	}

	public boolean remove(final T item) {
		return remove(item, 1);
	}

	public boolean remove(final CountableObject<T> count) {
		return remove(count.objValue, (int) min(Integer.MAX_VALUE, count.count));
	}

	public boolean removeAllOccurrences(final T item) {
		return remove(item, Integer.MAX_VALUE);
	}

	private boolean remove(final T item, final int amountToRemove) {
		if (amountToRemove < 0) { throw new IllegalArgumentException("Cannot remove a negative amount of items"); }
		if (amountToRemove == 0) { return false; }
		final var optionalCount = itemToCount.getOptional(item);
		if (optionalCount.isEmpty()) { return false; } // the item has never been in the set
		final var oldCount = optionalCount.getAsInt();
		if (amountToRemove >= oldCount) {
			itemToCount.remove(item);
			size -= oldCount;
		} else {
			itemToCount.put(item, oldCount - amountToRemove);
			size -= amountToRemove;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public boolean removeAll(final T... items) {
		if (items.length == 0) { return false; }
		final int sizeBefore = size;
		for (final var item : items) {
			remove(item);
		}
		return sizeBefore != size;
	}

	@SuppressWarnings("unchecked")
	public boolean removeAll(final CountableObject<T>... counts) {
		if (counts.length == 0) { return false; }
		final int sizeBefore = size;
		for (final var count : counts) {
			remove(count);
		}
		return sizeBefore != size;
	}

	public boolean removeAll(final HashBag<T> other) {
		if (other.isEmpty()) { return false; }
		final int sizeBefore = size;
		// Operating on this itemToCount directly is rather complicated, because on 0-count items must be removed
		// therefore just use the usual remove method, that's clear to read and works reliably
		other.itemToCount.entryStream().forEach(entry -> remove(entry.getOne(), entry.getTwo()));
		return sizeBefore != size;
	}

	public boolean set(final CountableObject<T> count) {
		final var item = count.objValue;
		final var newAmount = Math.toIntExact(count.count);
		if (newAmount < 0) { throw new IllegalArgumentException("Cannot set a negative amount of items"); }
		final int sizeBefore = size;
		remove(item, Integer.MAX_VALUE); // MAX_VALUE saves from looking up old amount twice
		add(item, newAmount);
		return sizeBefore != size;
	}

	public boolean contains(final T item) {
		return itemToCount.containsKey(item);
	}

	/** @return a stream of values in no particilar order, where each value is given exactly once, if it occurs in the bag at least 1 time. */
	public Stream<T> distinctValues() {
		return itemToCount.keyStream();
	}

	/** @return a stream of all values in no particular order, where each value will be repeated as many times as it is in the bag. */
	public Stream<T> values() {
		return itemToCount.entryStream()//
				.flatMap(ic -> Stream.generate(ic::getOne).limit(ic.getTwo()));
	}

	public Stream<CountableObject<T>> occurrences() {
		return itemToCount.entryStream().map(HashBag::toCountableItem);
	}

	public int occurrencesOf(final T item) {
		return itemToCount.getOptional(item).orElse(0);
	}

	public List<CountableObject<T>> bottomOccurrences(final int count) {
		return nOccurrences(count, naturalOrder()); // least amount first
	}

	public List<CountableObject<T>> topOccurrences(final int count) {
		return nOccurrences(count, Comparator.reverseOrder()); // biggest amount first
	}

	private List<CountableObject<T>> nOccurrences(final int count, final Comparator<CountableObject<T>> comparator) {
		if (count < 0) { throw new IllegalArgumentException("Cannot use a value of n < 0"); }
		if (count == 0 || isEmpty()) { return new ArrayList<>(0); }
		final var sorted = itemToCount.entryStream().map(HashBag::toCountableItem).sorted(comparator).collect(toList());
		final int index = Math.min(count, sizeDistinct()) - 1;
		final CountableObject<T> cutOff = sorted.get(index); // we want the top/bottom n counts, this is one of the nTh, if there are more of the same count
		final var amount = cutOff.count;
		int end = index + 1;
		while (end < sorted.size() && sorted.get(end).count == amount) { // find index of first pair having a different amount than our cutOff, or list-end
			++end;
		}
		// we could use a subList view of the original sorted list, but that may be very large, so we give it to the GC and create a new one instead
		return new ArrayList<>(sorted.subList(0, end));
	}

	public int size() {
		return size;
	}

	public int sizeDistinct() {
		return itemToCount.size();
	}

	public void clear() {
		itemToCount.clear();
		size = 0;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public int hashCode() {
		if (isEmpty()) { return 0; }
		final int prime = 31;
		int result = 1;
		result = prime * result + (itemToCount == null ? 0 : itemToCount.hashCode());
		result = prime * result + size;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		@SuppressWarnings("unchecked")
		final HashBag<T> other = (HashBag<T>) obj;
		if (itemToCount == null) {
			if (other.itemToCount != null) { return false; }
		} else if (!itemToCount.equals(other.itemToCount)) { return false; }
		if (size != other.size) { return false; }
		return true;
	}

	private static <T> CountableObject<T> toCountableItem(final ObjectIntPair<T> entry) {
		return CountableObject.ofValue(entry.getOne()).withCount(entry.getTwo());
	}

}
