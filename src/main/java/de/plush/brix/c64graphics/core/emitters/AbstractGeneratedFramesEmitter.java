package de.plush.brix.c64graphics.core.emitters;

import java.awt.image.BufferedImage;
import java.util.*;
import java.util.concurrent.CountDownLatch;

import javax.annotation.WillClose;

public abstract class AbstractGeneratedFramesEmitter extends AbstractImageEmitter<BufferedImage> {

	protected final int frames;
	private volatile Timer timer;
	private volatile CountDownLatch stopSignal;

	public AbstractGeneratedFramesEmitter(final int frames) {
		this.frames = frames;
	}

	protected abstract BufferedImage createImage(final int frame);

	@WillClose
	@Override
	public void run() { // using a timer, because it looks more fancy, if you can see the animation while converting :-)
		stopSignal = new CountDownLatch(1);
		timer = new Timer("FrameEmitter timer", true);
		timer.scheduleAtFixedRate(new TimerTask() {
	
			int frame;
	
			@Override
			public void run() {
				++frame;
				emit(createImage(frame % frames));
				if (frame == frames) {
					stop(timer);
				}
			}
		}, 0, 500 / frames);
	
		try {
			stopSignal.await();
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected void stop(final Timer timer) {
		timer.cancel();
		timer.purge();
		closeEmission();
		stopSignal.countDown();
	}

}