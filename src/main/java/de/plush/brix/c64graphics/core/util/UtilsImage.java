package de.plush.brix.c64graphics.core.util;

import static java.lang.Math.*;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.UnaryOperator;
import java.util.stream.IntStream;

import javax.imageio.ImageIO;

import de.plush.brix.c64graphics.core.model.C64Char;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.util.collections.DoubleArrayList;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

public class UtilsImage {

	protected UtilsImage() {
		throw new UnsupportedOperationException();
	}

	public static boolean equal(final BufferedImage a, final BufferedImage b) {
		return nextUp(0) >= abs(error(b, a, ColorDistance.EUCLIDEAN)); // Euclidean is fastest, no need to be flexible here
	}

	/**
	 * Error function that is fast but not very precise, hamming distance between pixels.
	 * Note that the distance is 0, if pixels are the same color, 1 if te color is any different.
	 */
	public static double error2Colors(final BufferedImage a, final BufferedImage b) {
		if (a.getWidth() != b.getWidth()) { throw new IllegalStateException("Different Width!"); }
		if (a.getHeight() != b.getHeight()) { throw new IllegalStateException("Different Width!"); }
		double err = 0;
		for (int y = 0; y < a.getHeight(); ++y) {
			for (int x = 0; x < a.getWidth(); ++x) {
				final int rgbA = a.getRGB(x, y);
				final int rgbB = b.getRGB(x, y);
				if (rgbA != rgbB) {
					++err;
				}
			}
		}
		final int numPixels = a.getWidth() * a.getHeight();
		err = numPixels == 0 ? 0 : min(1, err / numPixels);
		return err / (a.getHeight() * a.getWidth());
	}

	/**
	 * Error function that is fast but not very precise, based on the hamming distance between pixels, where
	 * each pixel's difference is the difference of color according to the given distance metric.
	 */
	public static double errorFast(final BufferedImage a, final BufferedImage b, final ColorDistanceMeasure distanceMeasure) {
		if (a.getWidth() != b.getWidth()) { throw new IllegalStateException("Different Width!"); }
		if (a.getHeight() != b.getHeight()) { throw new IllegalStateException("Different Width!"); }
		double err = 0;
		for (int y = 0; y < a.getHeight(); ++y) {
			for (int x = 0; x < a.getWidth(); ++x) {
				final int rgbA = a.getRGB(x, y);
				final int rgbB = b.getRGB(x, y);
				final Color colorA = new Color(rgbA);
				final Color colorB = new Color(rgbB);
				err += distanceMeasure.distanceOf(colorA, colorB);
			}
		}
		final int numPixels = a.getWidth() * a.getHeight();
		err = numPixels == 0 ? 0 : min(1, err / numPixels);
		return sqrt(pow(err, 2));
	}

	/**
	 * error function that tries to not look at different kinds of distances.
	 * The hamming distance, the difference of lightness in each quarter of the picture
	 * and the total number of different pixels.
	 */
	public static double error(final BufferedImage a, final BufferedImage b, final ColorDistanceMeasure distanceMeasure) {
		if (a.getWidth() != b.getWidth()) { throw new IllegalStateException("Different Width!"); }
		if (a.getHeight() != b.getHeight()) { throw new IllegalStateException("Different Width!"); }

		final int halftWidth = a.getWidth() / 2;
		final int halfHeight = a.getHeight() / 2;
		// important:clockwise!
		final Rectangle leftTop = new Rectangle(0, 0, halftWidth, halfHeight);
		final Rectangle rightTop = new Rectangle(halftWidth, 0, halftWidth, halfHeight);
		final Rectangle rightBottom = new Rectangle(halftWidth, halfHeight, halftWidth, halfHeight);
		final Rectangle leftBottom = new Rectangle(0, halfHeight, halftWidth, halfHeight);

		final var pixelsPerQuarter = max(9, 1 + a.getWidth() * a.getHeight() / 4); // +1 to avoid premature capacity growth

		// avoiding lists, maps and streams after profiling and tuning this method.
		// indexed array lookup is way faster than map.get();
		final var areaValuesClockwiseA = new DoubleArrayList[] { //
				new DoubleArrayList(pixelsPerQuarter), // initialize with proper size, so the lists don't need to grow in capacity
				new DoubleArrayList(pixelsPerQuarter), //
				new DoubleArrayList(pixelsPerQuarter), //
				new DoubleArrayList(pixelsPerQuarter) //
		};

		final var areaValuesClockwiseB = new DoubleArrayList[] { //
				new DoubleArrayList(pixelsPerQuarter), // initialize with proper size, so the lists don't need to grow in capacity
				new DoubleArrayList(pixelsPerQuarter), //
				new DoubleArrayList(pixelsPerQuarter), //
				new DoubleArrayList(pixelsPerQuarter) //
		};

		int numDifferent = 0;
		double err = 0;
		for (int y = 0; y < a.getHeight(); ++y) {
			for (int x = 0; x < a.getWidth(); ++x) {
				final int rgbA = a.getRGB(x, y);
				final int rgbB = b.getRGB(x, y);
				if (rgbA != rgbB) {
					++numDifferent;
				}
				final Color colorA = new Color(rgbA);
				final Color colorB = new Color(rgbB);
				err += distanceMeasure.distanceOf(colorA, colorB);
				putGrayValuesToAreas(leftTop, rightTop, rightBottom, leftBottom, areaValuesClockwiseA, areaValuesClockwiseB, y, x, colorA, colorB);
			}
		}

		final int numPixels = a.getWidth() * a.getHeight();
		err = numPixels == 0 ? 0 : min(1, err / numPixels);
		final double diff = numPixels == 0 ? 0 : min(1, numDifferent / (double) numPixels);

		// calculate mean gray scale for each square (left top, right top, right bottom, left bottom
		double structuraldiff = 0;

		// to normalized gray scale values:
		final int[] circularComparisonA = circularComparison(areaValuesClockwiseA);
		final int[] circularComparisonB = circularComparison(areaValuesClockwiseB);
		structuraldiff = min(1, IntStream.range(0, 4).map(index -> abs(circularComparisonA[index] - circularComparisonB[index])).sum() / 8.0);

		// System.out.println(Arrays.toString(new double[] { err, diff, structuraldiff }));
		return sqrt(2 * pow(err, 2) + pow(diff, 2) + pow(structuraldiff, 2));
	}

	private static void putGrayValuesToAreas(final Rectangle leftTop, final Rectangle rightTop, final Rectangle rightBottom, final Rectangle leftBottom,
			final DoubleArrayList[] areaValuesClockwiseA, final DoubleArrayList[] areaValuesClockwiseB, final int y, final int x, final Color colorA,
			final Color colorB) {
		// put gray scales to areas
		final var grayScaleA = (colorA.getRed() + colorA.getGreen() + colorA.getBlue()) / 3.0;
		final var grayScaleB = (colorB.getRed() + colorB.getGreen() + colorB.getBlue()) / 3.0;
		final var leftTopIndex = 0;
		final var rightTopIndex = 1;
		final var rightBottomIndex = 2;
		final var leftBottomIndex = 3;
		final Point point = new Point(x, y);
		if (leftTop.contains(point)) {
			areaValuesClockwiseA[leftTopIndex].add(grayScaleA); // the map.get calls are a hot spot, according to CPU sampling
			areaValuesClockwiseB[leftTopIndex].add(grayScaleB);
		} else if (rightTop.contains(point)) {
			areaValuesClockwiseA[rightTopIndex].add(grayScaleA);
			areaValuesClockwiseB[rightTopIndex].add(grayScaleB);
		} else if (rightBottom.contains(point)) {
			areaValuesClockwiseA[rightBottomIndex].add(grayScaleA);
			areaValuesClockwiseB[rightBottomIndex].add(grayScaleB);
		} else if (leftBottom.contains(point)) {
			areaValuesClockwiseA[leftBottomIndex].add(grayScaleA);
			areaValuesClockwiseB[leftBottomIndex].add(grayScaleB);
		}
	}

	private static int[] circularComparison(final DoubleArrayList[] areaValuesClockwise) {
		// avoiding streams. as profiling found them to be slower
		final double[] tsg = new double[] { // create 4 average gray values
				areaValuesClockwise[0].optionalAverage().orElse(0), // average can be empty, if only single line images are compared
				areaValuesClockwise[1].optionalAverage().orElse(0), //
				areaValuesClockwise[2].optionalAverage().orElse(0), //
				areaValuesClockwise[3].optionalAverage().orElse(0), //
		};
		// compare in a circular fashion:
		return new int[] { Double.compare(tsg[0], tsg[1]) //
				, Double.compare(tsg[1], tsg[2]) //
				, Double.compare(tsg[2], tsg[3]) //
				, Double.compare(tsg[3], tsg[0]) //
		};
	}

	public static BufferedImage deepCopy(final BufferedImage image) {
		final ColorModel cm = image.getColorModel();
		final boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		final WritableRaster raster = image.copyData(image.getRaster().createCompatibleWritableRaster());
		return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}

	public static Set<Color> colors(final BufferedImage image) {
		final Set<Color> imageColors = new HashSet<>();
		for (int y = 0; y < image.getHeight(); ++y) {
			for (int x = 0; x < image.getWidth(); ++x) {
				imageColors.add(new Color(image.getRGB(x, y)));
			}
		}
		return imageColors;
	}

	public static void replaceColor(final BufferedImage image, final UnaryOperator<Color> colorChangeFunction) {
		for (int y = 0; y < image.getHeight(); ++y) {
			for (int x = 0; x < image.getWidth(); ++x) {
				final int rgb = image.getRGB(x, y);
				final Color newColor = colorChangeFunction.apply(new Color(rgb));
				image.setRGB(x, y, newColor == null ? rgb : newColor.getRGB());
			}
		}
	}

	@SuppressFBWarnings(value = "DCN_NULLPOINTER_EXCEPTION", //
			justification = "having a null check in place of the NPE-catch would just double the error signalling code")
	public static BufferedImage loadImageResource(final Class<?> clazz, final String imageFilename) {
		try {
			final File imageFile = Paths.get(clazz.getResource(imageFilename).toURI()).toFile();
			return loadImage(imageFile);
		} catch (final URISyntaxException | NullPointerException e) {
			throw new IllegalArgumentException("Could not read image from file " + imageFilename, e);
		}
	}

	public static BufferedImage loadImage(final File imageFile) {
		try {
			return ImageIO.read(imageFile);
		} catch (final IOException e) {
			throw new IllegalArgumentException("Could not read image from file " + imageFile, e);
		}
	}

	public enum ImageFormat {
		PNG, GIF, JPG;
	}

	public static void saveImage(final BufferedImage image, final ImageFormat format, final File imageFile) {
		try {
			ImageIO.write(image, format.toString(), imageFile);
			System.out.println("wrote file " + imageFile);
		} catch (final IOException e) {
			throw new IllegalArgumentException("Could not write image to file " + imageFile + ", canWrite: " + imageFile.canWrite(), e);
		}
	}

	public static BufferedImage repetitiveBlockImage(final BufferedImage partialImage) {
		final int partialImageHeight = partialImage.getHeight();
		if (partialImageHeight > 8) { throw new IllegalArgumentException("line image should be less or equal 8 pixels high, but is " + partialImageHeight); }
		final BufferedImage blockImage = new BufferedImage(C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS, BufferedImage.TYPE_INT_RGB);
		final Graphics g = blockImage.getGraphics();
		try {
			for (int y = 0; y < C64Char.HEIGHT_PIXELS; y += partialImageHeight) {
				g.drawImage(partialImage, 0, y, null);
			}
			return blockImage;
		} finally {
			g.dispose();
		}
	}

}
