package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.PaletteConversion.PaletteConvertedImage;

/** Takes an image and applies a palette. */
public class PaletteConversion implements IPipelineStage<BufferedImage, PaletteConvertedImage> {

	private final Palette palette;

	private final Function<BufferedImage, BufferedImage> dithering;

	/**
	 * Creates a new pipeline step to apply a custom palette to an image.
	 * 
	 * @param palette
	 *            a palette of colors
	 * @param colorDistanceMeasure
	 *            determined how to find the closest palette color to the image color. Predefined color distance measures can be found in the
	 *            {@link ColorDistance} enumeration.
	 * @param ditheringSpec
	 *            determines how the color-mismatch is dealt with. Predefined ditheings can be found in the {@link Dithering} enumeration.
	 */
	public PaletteConversion(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure, final DitheringSpec ditheringSpec) {
		this.palette = palette;
		dithering = ditheringSpec.algorithm(palette, colorDistanceMeasure);
	}

	@Override
	public PaletteConvertedImage apply(final BufferedImage original) {
		return toPaletteConvertedImage(dithering.apply(original));
	}

	private PaletteConvertedImage toPaletteConvertedImage(final BufferedImage original) {
		final PaletteConvertedImage result = new PaletteConvertedImage(palette, original.getWidth(), original.getHeight(), original.getType());
		final Graphics2D g = (Graphics2D) result.getGraphics();
		try {
			g.drawImage(original, 0, 0, null);
			return result;
		} finally {
			g.dispose();
		}
	}

	public static class PaletteConvertedImage extends BufferedImage {

		private final Palette palette;

		public PaletteConvertedImage(final Palette palette, final int width, final int height, final int imageType) {
			super(width, height, imageType);
			this.palette = palette;
		}

		public Palette palette() {
			return palette;
		}

	}
}
