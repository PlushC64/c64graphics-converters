package de.plush.brix.c64graphics.core.pipeline.stages.dithering;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.OptionalDouble;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;

public class NoDitheringSpec implements DitheringSpec {

	public NoDitheringSpec() {}

	@Override
	public NoDitheringSpec clone() {
		return this;
	}

	@Override
	public Point centerPoint(final Point globalPosition) {
		return new Point();
	}

	@Override
	public Point centerPointOffset(final Point matrixPosition, final Point globalPosition) {
		return new Point();
	}

	@Override
	public OptionalDouble errorMultiplier(final Point matrixPosition, final Point globalPosition) {
		return OptionalDouble.empty();
	}

	@Override
	public Dimension matrixSize() {
		return new Dimension();
	}

	@Override
	public Function<BufferedImage, BufferedImage> algorithm(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure) {
		return new NoDithering(palette, colorDistanceMeasure);
	}

}
