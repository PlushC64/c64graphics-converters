package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.Color;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.C64Char;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

public class ImageToC64CharHires implements IPipelineStage<BufferedImage, C64Char> {

	private final Color background;

	private final Color foreground;

	public ImageToC64CharHires(final Color background, final Color foreground) {
		this.background = background;
		this.foreground = foreground;
	}

	@Override
	public C64Char apply(final BufferedImage blockImage) {
		final byte[] bytes = new byte[C64Char.HEIGHT_PIXELS];
		for (int line = 0; line < Math.min(200, blockImage.getHeight()); ++line) {
			bytes[line] = asByte(blockImage.getSubimage(0, line, blockImage.getWidth(), 1), background, foreground);
		}
		return new C64Char(bytes);
	}

	// static to encourage extensive inlining into this method.
	@SuppressFBWarnings(value = "SF_SWITCH_NO_DEFAULT", justification = "false positive, there is a default")
	private static byte asByte(final BufferedImage imageNx1, final Color background, final Color foreground) {
		byte x = 0;
		switch (imageNx1.getWidth()) { // force unrolled jump-table instead of loop
		case 8:
			x |= asBit(imageNx1, 7, background, foreground);
		case 7:
			x |= asBit(imageNx1, 6, background, foreground);
		case 6:
			x |= asBit(imageNx1, 5, background, foreground);
		case 5:
			x |= asBit(imageNx1, 4, background, foreground);
		case 4:
			x |= asBit(imageNx1, 3, background, foreground);
		case 3:
			x |= asBit(imageNx1, 2, background, foreground);
		case 2:
			x |= asBit(imageNx1, 1, background, foreground);
		case 1:
			x |= asBit(imageNx1, 0, background, foreground);
		default: // do nothing
		}
		return x;
	}

	private static byte asBit(final BufferedImage imageNx1, final int x, final Color background, final Color foreground) {
		final int shift = C64Char.WIDTH_PIXELS - 1 - x;
		final var pixelColor = new Color(imageNx1.getRGB(x, 0) & 0xFFFFFF);
		return (byte) (pixelColor.equals(foreground) ? 1 << shift
				: pixelColor.equals(background) ? 0 //
						: error(pixelColor, background, foreground));
	}

	private static byte error(final Color pixel, final Color background, final Color foreground) {
		throw new IllegalArgumentException(pixel + " is not among the given colors for background (" + background + ") and foreground (" + foreground
				+ ") . Did you forget to solve color clashes?");
	}
}
