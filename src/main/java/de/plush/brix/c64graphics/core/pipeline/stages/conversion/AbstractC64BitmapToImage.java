package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

abstract class AbstractC64BitmapToImage implements IPipelineStage<C64Bitmap, BufferedImage> {

	protected abstract Function<C64Char, ? extends BufferedImage> createCharToImage();

	@Override
	public BufferedImage apply(final C64Bitmap bitmap) {
		final BufferedImage result = new BufferedImage(C64Bitmap.WIDTH_PIXELS, C64Bitmap.HEIGHT_PIXELS, BufferedImage.TYPE_INT_RGB);
		final Graphics2D g = result.createGraphics();
		final Function<C64Char, ? extends BufferedImage> c64CharToImage = createCharToImage();
		try {
			// jmh testing showed that the simple for-loops seem to be twice as fast as using parallel streams.
			for (int yBlock = 0; yBlock < C64CharsetScreen.HEIGHT_BLOCKS; ++yBlock) {
				for (int xBlock = 0; xBlock < C64CharsetScreen.WIDTH_BLOCKS; ++xBlock) {
					final C64Char block = bitmap.blockAt(xBlock, yBlock);
					final BufferedImage blockImage = c64CharToImage.apply(block);
					g.drawImage(blockImage, xBlock * C64Char.WIDTH_PIXELS, yBlock * C64Char.HEIGHT_PIXELS, null);
				}
			}
		} finally {
			g.dispose();
		}
		return result;
	}

}