package de.plush.brix.c64graphics.core.pipeline.displays;

import static de.plush.brix.c64graphics.core.util.Utils.defaultIfNull;
import static java.lang.Math.max;

import java.awt.*;
import java.util.function.Supplier;

import javax.swing.*;
import javax.swing.plaf.metal.MetalToolTipUI;

class ImageToolTipUI extends MetalToolTipUI {

	private final Supplier<Image> image;

	public ImageToolTipUI(final Supplier<Image> image) {
		this.image = image;
	}

	@Override
	public void paint(final Graphics g, final JComponent c) {
		final Image image = this.image.get();
		final FontMetrics metrics = c.getFontMetrics(g.getFont());
		g.setColor(c.getForeground());
		final int x = image.getWidth(c) + 5;
		g.drawImage(image, 0, 0, c);

		// if text was drawn at baseline 0, text would be above window with descent showing.
		// loop is first adding the full line height to draw first line, now the text would be a descent too low-
		// Now removing the extra descent, so the text baseline is shown at leading+ascent:
		int y = -metrics.getDescent();
		for (final String line : ((JToolTip) c).getTipText().split("\n")) {
			g.drawString(line, x, y += metrics.getHeight());
		}
	}

	@Override
	public Dimension getPreferredSize(final JComponent c) {
		final Image image = this.image.get();
		final FontMetrics metrics = c.getFontMetrics(c.getFont());
		final String tipText = defaultIfNull(((JToolTip) c).getTipText(), "");
		final String[] lines = tipText.split("\n");
		final int imageAndGapWidth = image.getWidth(c) + 5;
		int width = imageAndGapWidth;
		for (final String line : lines) {
			width = max(width, imageAndGapWidth + SwingUtilities.computeStringWidth(metrics, line));
		}
		// not always the same as metrics.getHeight
		final int height = max(lines.length * metrics.getHeight(), image.getHeight(c)) // space of text lines
				+ metrics.getLeading(); // spacing to bottom
		final Insets i = c.getInsets();
		return new Dimension(width + i.left + i.right, height + i.top + i.bottom);
	}
}