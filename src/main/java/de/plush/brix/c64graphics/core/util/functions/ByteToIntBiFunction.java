package de.plush.brix.c64graphics.core.util.functions;

@FunctionalInterface
public interface ByteToIntBiFunction {

	int apply(byte a, byte b);
}
