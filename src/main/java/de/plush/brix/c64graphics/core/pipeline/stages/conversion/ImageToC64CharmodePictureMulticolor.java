package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;
import static de.plush.brix.c64graphics.core.util.UtilsImage.*;
import static java.util.Arrays.*;
import static java.util.Comparator.*;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.*;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.C64CharToImageMulticolor.Mode;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.CharmodeColorClashResolverMulticolor;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.CharmodeColorClashResolverMulticolor.PreProcessedImage;
import de.plush.brix.c64graphics.core.util.Utils;

public class ImageToC64CharmodePictureMulticolor implements IPipelineStage<BufferedImage, C64CharsetScreenColorRam> {

	private final Palette palette;

	private final ColorDistanceMeasure distanceMeasure;

	private final CharacterReduction characterReduction;

	private final Function<BufferedImage, PreProcessedImage> colorClashResolver;

	public ImageToC64CharmodePictureMulticolor(final Palette palette, final ColorDistanceMeasure distanceMeasure, final CharacterReduction characterReduction) {
		this(palette, distanceMeasure, characterReduction, new CharmodeColorClashResolverMulticolor(palette, distanceMeasure));
	}

	public ImageToC64CharmodePictureMulticolor(final Palette palette, final ColorDistanceMeasure distanceMeasure, final CharacterReduction characterReduction,
			final Function<BufferedImage, PreProcessedImage> colorClashResolver) {
		this.palette = palette;
		this.distanceMeasure = distanceMeasure;
		this.characterReduction = characterReduction;
		this.colorClashResolver = colorClashResolver;
	}

	// found no meaningful way to extract methods that make it more readable
	@SuppressWarnings({ "checkstyle:CyclomaticComplexity", "checkstyle:NPathComplexity" })
	@Override
	public C64CharsetScreenColorRam apply(final BufferedImage image) {
		final PreProcessedImage preProcessedImage = colorClashResolver.apply(image);
		final byte backgroundColorCode = preProcessedImage.backgroundColorCode();
		final byte mc1ColorCode = preProcessedImage.mc1ColorCode();
		final byte mc2ColorCode = preProcessedImage.mc2ColorCode();
		final Color background = palette.color(toUnsignedByte(backgroundColorCode & 0x0F));
		final Color mc1 = palette.color(toUnsignedByte(mc1ColorCode & 0x0F));
		final Color mc2 = palette.color(toUnsignedByte(mc2ColorCode & 0x0F));
		final long start = System.nanoTime();

		final Dimension blocks = C64Bitmap.sizeBlocks();
		final C64Bitmap bitmap = new C64Bitmap();
		final C64ColorRam colorRam = new C64ColorRam(preProcessedImage.colorRam());
		final Map<C64Char, List<Point>> c64CharToPositions = new HashMap<>();
		for (int y = 0; y < blocks.height; ++y) {
			for (int x = 0; x < blocks.width; ++x) {
				final Point blockPos = new Point(x, y);
				final int xPix = x * C64Char.WIDTH_PIXELS;
				final int yPix = y * C64Char.HEIGHT_PIXELS;
				final BufferedImage blockImage = preProcessedImage.getSubimage(xPix, yPix, C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS);
				final byte d800ColorCode = colorRam.colorCodeAt(x, y);

				final Color d800 = palette.visibleColorInMulticolorCharmode(d800ColorCode);
				final C64Char block = palette.isMulticolorBitSetInD800colorCode(d800ColorCode) //
						? new ImageToC64CharMulticolor(background, mc1, mc2, d800).apply(blockImage)
						: new ImageToC64CharHires(background, d800).apply(blockImage);
				c64CharToPositions.computeIfAbsent(block, (k) -> new ArrayList<>(1)).add(blockPos);
				bitmap.setBlock(x, y, block);
			}
		}

		// System.out.println(bitmap);
		if (c64CharToPositions.size() > 1) {
			System.out.println("number of characters: " + c64CharToPositions.size() + (c64CharToPositions.size() > 256 ? " (reducing...)" : ""));

			final Set<C64Char> victimBlacklist = new HashSet<>(); // will be reduced later
			List<Map.Entry<PreProcessedCharacterImage, C64Char>> possibleSurrogates = null;
			List<VictimToSurrogateMapping> priorityQueue = null;
			while (c64CharToPositions.size() > C64Charset.MAX_CHARACTERS) {

				// display.onImage(toImage(blockImageToPositions, background, mc1, mc2, colorRam));

				final int queueSize = c64CharToPositions.size();
				if (possibleSurrogates == null || possibleSurrogates.isEmpty()) {
					possibleSurrogates = createPossibleSurrogates(() -> colors(preProcessedImage), background, mc1, mc2, c64CharToPositions.keySet());
					victimBlacklist.clear();
					// blacklist all single-colored chars (just because it looks like shit if these occur too often)
					addSingleColoredTiles(possibleSurrogates, victimBlacklist);

					if (priorityQueue != null) {
						priorityQueue.clear();
					}
				}
				if (priorityQueue == null || priorityQueue.isEmpty()) {
					priorityQueue = victimToSurrogateMappings(preProcessedImage, c64CharToPositions, possibleSurrogates, victimBlacklist, queueSize);
					if (priorityQueue.isEmpty()) { // e.g. black list contains every character
						victimBlacklist.clear(); // drink the cool aid as last resort
						addSingleColoredTiles(possibleSurrogates, victimBlacklist);
						priorityQueue = victimToSurrogateMappings(preProcessedImage, c64CharToPositions, possibleSurrogates, victimBlacklist, queueSize);
					}
				}
				final VictimToSurrogateMapping vsMapping = reduceNumberOfCharacters(bitmap, colorRam, c64CharToPositions, priorityQueue);

				// cleanup: remove victim and surrogate from data structures
				// a surrogate should not become a victim, so no victim is replaced by a surrogate that is again replaced, raising the error each step:
				victimBlacklist.add(vsMapping.surrogate); // exclude surrogate from the possible list of victims
				// removeIf is not parallel and has to juggle data in place, i.e. in O(n�), avoid by parallel filtering and replacing the original
				possibleSurrogates = possibleSurrogates.stream().parallel()//
						.filter(surrogateImageToChar -> !vsMapping.victim.equals(surrogateImageToChar.getValue())).collect(toList());
				priorityQueue = priorityQueue.stream().parallel()//
						.filter(m -> !(m.surrogate.equals(vsMapping.victim) || m.victim.equals(vsMapping.victim) || m.victim.equals(vsMapping.surrogate)))
						.collect(toList());
			}
		}
		// With less or equal than 256 images, we can build a charset, screen and ColorRam and be done
		final C64Charset charset = new C64Charset();
		final C64Screen screen = new C64Screen((byte) 0);
		for (int y = 0; y < blocks.height; ++y) {
			for (int x = 0; x < blocks.width; ++x) {
				final C64Char block = bitmap.blockAt(x, y);
				charset.addCharacter(block);
				screen.setScreenCode(x, y, charset.screenCodeOf(block).getAsByte());
			}
		}
		final long end = System.nanoTime();
		System.out.println("time (ms):" + TimeUnit.NANOSECONDS.toMillis(end - start));
		return new C64CharsetScreenColorRam(charset, screen, colorRam, backgroundColorCode, mc1ColorCode, mc2ColorCode);
	}

	private static void addSingleColoredTiles(final List<Map.Entry<PreProcessedCharacterImage, C64Char>> possibleSurrogates,
			final Collection<C64Char> victimBlacklist) {
		possibleSurrogates.stream().filter(e -> colors(e.getKey()).size() < 2).map(Map.Entry::getValue).collect(toSet()).forEach(victimBlacklist::add);
	}

	/* keeping this for debugging purposes @formatter:off
	private BufferedImage toImage(final Map<C64Char, List<Point>> blockImageToPositions, final Color bg, final Color mc1, final Color mc2,
			final C64ColorRam cram) {
		final BufferedImage image = new BufferedImage(C64Bitmap.WIDTH_PIXELS, C64Bitmap.HEIGHT_PIXELS, BufferedImage.TYPE_INT_RGB);
		final Graphics2D g = image.createGraphics();
		blockImageToPositions.entrySet().forEach(entry -> {
			entry.getValue().stream()//
					.forEach(pos -> {
						final Color d800 = palette.color(cram.colorCodeAt(pos.x, pos.y));
						final BufferedImage blockImage = new C64CharToImageMulticolor(palette, () -> bg, mc1, mc2, d800, Mode.Char).apply(entry.getKey());
						g.drawImage(blockImage, pos.x * C64Char.WIDTH_PIXELS, pos.y * C64Char.HEIGHT_PIXELS, null);
					});
		});
		// System.out.println(cram);
		return image;
	}
	@formatter:on */

	private List<Map.Entry<PreProcessedCharacterImage, C64Char>> createPossibleSurrogates(final Supplier<Set<Color>> imageColorSupplier, final Color background,
			final Color mc1, final Color mc2, final Set<C64Char> blocks) {
		final long start = System.nanoTime();
		try {
			// for each non-global palette color, create a block-image from each of the characters.
			// don't use just image colors here, as we want to test possible hires/multicolor switches too.
			final List<Color> globalColors = asList(background, mc1, mc2);

			final Set<Color> colors = characterReduction == CharacterReduction.UsingImageColorsOnly ? imageColorSupplier.get()
					: stream(palette.colors()).collect(toSet());
			colors.removeIf(color -> !palette.canBeDisplayedInD800InMulticolorCharmode(color));
			final Set<Color> c800MulticolorVariants = colors.stream()//
					.map(color -> palette.colorThatReplacesThisColorInD800MulticolorCharmode(color))//
					.filter(Optional::isPresent)//
					.map(Optional::get)//
					.collect(toSet());
			colors.addAll(c800MulticolorVariants);
			colors.removeAll(globalColors);
			System.out.println("variant colorCodes (hex): " + colors.stream().map(palette::colorCodeOrException).map(Utils::toHexString).collect(toList()));
			return colors.stream()//
					// return stream(palette.colors())//
					// .filter(color -> !globalColors.contains(color))//
					.map(color -> blocks.stream()//
							.map(block -> new SimpleEntry<>(//
									new C64CharToImageMulticolor(palette, () -> background, mc1, mc2, color, Mode.Char).apply(block), //
									block)))//
					.flatMap(identity())//
					.collect(toList());
		} finally {
			final long end = System.nanoTime();
			System.out.println("createPossibleSurrogates, time: " + TimeUnit.NANOSECONDS.toMillis(end - start));
		}
	}

	private static VictimToSurrogateMapping reduceNumberOfCharacters(final C64Bitmap bitmap, final C64ColorRam colorRam,
			final Map<C64Char, List<Point>> c64CharToPositions, final List<VictimToSurrogateMapping> victimToSurrogateMappings) {
		final VictimToSurrogateMapping victimToSurrogateMapping = victimToSurrogateMappings.remove(victimToSurrogateMappings.size() - 1);
		// System.out.println("surrogateQ: " + victimToSurrogateMappings.size());
		// replace victim by surrogate
		// System.out.println("replacing character by surrogates");
		final C64Char victim = victimToSurrogateMapping.victim;
		// System.out.println("victim: \n" + victim);
		final C64Char surrogate = victimToSurrogateMapping.surrogate;
		// System.out.println("surrogate: \n" + surrogate);
		final PreProcessedCharacterImage surrogateImage = victimToSurrogateMapping.surrogateImage;
		final List<Point> victimPositions = c64CharToPositions.get(victim);
		// System.out.println("victimPositions: \n" + victimPositions);
		victimPositions.stream()//
				.peek(p -> colorRam.setColorCode(p.x, p.y, surrogateImage.d800ColorCode()))//
				.forEach(p -> bitmap.setBlock(p.x, p.y, surrogate));
		c64CharToPositions.get(surrogate).addAll(victimPositions);
		c64CharToPositions.remove(victim);
		return victimToSurrogateMapping;

	}

	private List<VictimToSurrogateMapping> victimToSurrogateMappings(final PreProcessedImage image, final Map<C64Char, List<Point>> c64CharToPositions,
			final List<Map.Entry<PreProcessedCharacterImage, C64Char>> possibleSurrogates, final Set<C64Char> victimBlacklist, final int limit) {
		final long start = System.nanoTime();
		try {
			// for the (n=limit) least occurring characters, look for a surrogate in all images, sort by error:
			final List<VictimToSurrogateMapping> queue = c64CharToPositions.entrySet().stream()//
					.filter(entry -> !victimBlacklist.contains(entry.getKey()))// exclude characters that are already dismissed
					.sorted(comparingInt(charToPos -> charToPos.getValue().size()))// least frequent first
					.limit(limit)//
					.parallel()//
					// .peek(charToPos -> System.out.println("among the 10 most infrequent chars: \n" + charToPos))//
					.map(charToPos -> {
						final C64Char possibleVictim = charToPos.getKey();
						final List<Point> possibleVictimPositions = charToPos.getValue();
						return surrogateMappingsForSingleVictim(possibleVictim, possibleVictimPositions, possibleSurrogates, image);
					}).flatMap(list -> list.stream())// stream of possible possibleVictimToSurrogateMappings (weighted)
					.sorted(comparingDouble(VictimToSurrogateMapping::weight) //
							.thenComparing(vso -> c64CharToPositions.get(vso.victim).size())// least frequentVictims go first, if two are equally good
							.thenComparing(VictimToSurrogateMapping::sortStabilizer))// utilizes stable hash codes
					.collect(toList());
			Collections.reverse(queue); // slow, but a reverse comparator gives strange results and we want to poll from the back of the list for O(1) polling
			return queue;

		} finally {
			final long end = System.nanoTime();
			System.out.println("victimToSurrogateMappings, time: " + TimeUnit.NANOSECONDS.toMillis(end - start));
		}
	}

	private List<VictimToSurrogateMapping> surrogateMappingsForSingleVictim(final C64Char possibleVictim, final List<Point> possibleVictimPositions,
			final List<Map.Entry<PreProcessedCharacterImage, C64Char>> possibleSurrogates, final PreProcessedImage image) {
		return possibleVictimPositions.stream().parallel()//
				// a char (bit pattern) in an image can have different sub images based on coloring at a the given position.
				.map(blockPos -> image.getSubimage(blockPos.x * C64Char.WIDTH_PIXELS, blockPos.y * C64Char.HEIGHT_PIXELS, C64Char.WIDTH_PIXELS,
						C64Char.HEIGHT_PIXELS)) // original image
				.map(possibleVictimImage -> possibleSurrogates.stream() //
						// ignore own modifications:
						.filter(possibleSurrogateEntry -> !possibleVictim.equals(possibleSurrogateEntry.getValue())).map(
								possibleSurrogateEntry -> new VictimToSurrogateMapping(possibleVictim, possibleSurrogateEntry.getValue(), possibleSurrogateEntry
										.getKey(), //
										error(possibleVictimImage, possibleSurrogateEntry.getKey(), distanceMeasure)))).flatMap(Function.identity())//
				.collect(toList()); // important: do no leave a stream here and flatMap later -> speed drops dramatically!
	}

}
