/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import static de.plush.brix.c64graphics.core.model.Direction.Horizontal.Left;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.util.Utils;

/**
 * 
 * <pre>
 * <code>
 * import static de.plush.brix.c64graphics.core.model.Direction.Horizontal.*;
 * import static de.plush.brix.c64graphics.core.model.Direction.Vertical.*;
 * // ...
 * new BlockwiseRoll<>(Down.by(3), Right.by(2))
 * </code>
 * </pre>
 */
public class BlockwiseRoll<T extends IRollBlockwise> implements IPipelineStage<T, T> {

	private Displacement displacement;
	private Displacement otherDisplacement;

	public BlockwiseRoll(final Displacement displacement) {
		this(displacement, Left.by(0));
	}

	public BlockwiseRoll(final Displacement displacement, final Displacement otherDisplacement) {
		this.displacement = displacement;
		this.otherDisplacement = otherDisplacement;
	}

	@Override
	public T apply(final T original) {
		final T copy = Utils.copyFromCopyConstructorOrOriginal(original);
		copy.rollBlockwise(displacement);
		copy.rollBlockwise(otherDisplacement);
		return copy;
	}
}
