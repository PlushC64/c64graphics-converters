package de.plush.brix.c64graphics.core.model;

import static java.lang.System.arraycopy;
import static java.util.Arrays.stream;

import java.awt.Dimension;
import java.net.URI;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.util.*;

/**
 * A class that holds a Bitmap and either 2,4 or 8 screens for the FLI effect on each 4th, 2nd or single line.
 *
 * @author Wanja Gayk
 */
public class C64BitmapmodePictureHiresNscreenFLI implements IRollBlockwise {

	final private static List<Integer> legalScreenCounts = Arrays.asList(2, 4, 8);

	protected final C64Bitmap bitmap;

	protected final C64Screen[] screens;

	protected int expectedScreenCount;

	public C64BitmapmodePictureHiresNscreenFLI(final int expectedScreenCount, final C64Bitmap bitmap, final C64Screen[] screens) {
		this.expectedScreenCount = expectedScreenCount;
		if (expectedScreenCount != screens.length) {
			throw new IllegalArgumentException("Expected " + expectedScreenCount + " screens, but provided were " + screens.length);
		}
		if (!legalScreenCounts.contains(screens.length)) {
			throw new IllegalArgumentException("Legal numbers of screens are " + legalScreenCounts + ", but provided were " + screens.length);
		}
		this.bitmap = new C64Bitmap(bitmap);
		this.screens = stream(screens).map(C64Screen::new).toArray(C64Screen[]::new);
	}

	public C64BitmapmodePictureHiresNscreenFLI(final int expectedScreenCount, final C64BitmapmodePictureHiresNscreenFLI other) {
		this(expectedScreenCount, other.bitmap, other.screens);
	}

	/** Includes 3 FLI-Bug-Blocks in x-direction */
	public Dimension sizeBlocks() {
		return C64Bitmap.sizeBlocks();
	}

	/** Includes 3 FLI-Bug-Blocks in x-direction */
	public Dimension sizePixels() {
		return C64Bitmap.sizePixels();
	}

	public void setBitmapBlock(final int xBlock, final int yBlock, final C64Char chrBlock) {
		bitmap.setBlock(xBlock, yBlock, chrBlock);
	}

	public C64Char bitmapBlockAt(final int xBlock, final int yBlock) {
		return bitmap.blockAt(xBlock, yBlock);
	}

	public void setScreenCode(final int screenNumber, final int x, final int y, final byte screenCode) {
		checkScreenNumber(screenNumber);
		screens[screenNumber].setScreenCode(x, y, screenCode);
	}

	public byte screenCodeAt(final int screenNumber, final int x, final int y) {
		checkScreenNumber(screenNumber);
		return screens[screenNumber].screenCodeAt(x, y);
	}

	public byte[][] screenCodeMatrix(final int screenNumber) {
		checkScreenNumber(screenNumber);
		return screens[screenNumber].screenCodeMatrix();
	}

	/** @return a snapshot of the current bitmap. */
	public C64Bitmap bitmap() {
		return new C64Bitmap(bitmap);
	}

	/** @return a snapshot of the particular screen. */
	public C64Screen screen(final int screenNumber) {
		checkScreenNumber(screenNumber);
		return new C64Screen(screens[screenNumber]);
	}

	/** @return a snapshot of all screens. */
	public C64Screen[] screens() {
		return IntStream.range(0, screens.length)//
				.mapToObj(this::screen)//
				.toArray(C64Screen[]::new);
	}

	public int screenCount() {
		return screens.length;
	}

	private void checkScreenNumber(final int screen) {
		if (screen < 0 || screens.length - 1 < screen) {
			throw new IllegalArgumentException("Screen number must be between 0 and " + (screens.length - 1) + " (inclusive), but was: " + screen);
		}
	}

	@Override
	public void rollLeftBlockwise(final int n) {
		bitmap.rollLeftBlockwise(n);
		Arrays.stream(screens).forEach(screen -> screen.rollLeftBlockwise(n));
	}

	@Override
	public void rollRightBlockwise(final int n) {
		bitmap.rollRightBlockwise(n);
		Arrays.stream(screens).forEach(screen -> screen.rollRightBlockwise(n));
	}

	@Override
	public void rollUpBlockwise(final int n) {
		bitmap.rollUpBlockwise(n);
		Arrays.stream(screens).forEach(screen -> screen.rollUpBlockwise(n));
	}

	@Override
	public void rollDownBlockwise(final int n) {
		bitmap.rollDownBlockwise(n);
		Arrays.stream(screens).forEach(screen -> screen.rollDownBlockwise(n));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (bitmap == null ? 0 : bitmap.hashCode());
		result = prime * result + expectedScreenCount;
		result = prime * result + Arrays.hashCode(screens);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof C64BitmapmodePictureHiresNscreenFLI)) { return false; }
		final C64BitmapmodePictureHiresNscreenFLI other = (C64BitmapmodePictureHiresNscreenFLI) obj;
		if (bitmap == null) {
			if (other.bitmap != null) { return false; }
		} else if (!bitmap.equals(other.bitmap)) { return false; }
		if (expectedScreenCount != other.expectedScreenCount) { return false; }
		if (!Arrays.equals(screens, other.screens)) { return false; }
		return true;
	}

	protected static class C64HiresFliFormat<T extends C64BitmapmodePictureHiresNscreenFLI> {
		protected int loadAddress, endAddress, bitmapAddress, screensAddress;

		protected int length, bitmapOffset, screensOffset;

		protected C64HiresFliFormat<T> withLoadAddress(final int loadAddress, final int endAddress) {
			this.loadAddress = loadAddress;
			this.endAddress = endAddress;
			return this;
		}

		protected C64HiresFliFormat<T> withBitmapAddress(final int bitmapAddress) {
			this.bitmapAddress = bitmapAddress;
			return this;
		}

		protected C64HiresFliFormat<T> withScreensAddress(final int screensAddress) {
			this.screensAddress = screensAddress;
			return this;
		}

		private void calcOffsets() {
			length = endAddress - loadAddress + 1; // $4000 - $4000 is one byte in the codebase64 spec.
			bitmapOffset = bitmapAddress - loadAddress;
			screensOffset = screensAddress - loadAddress;
		}

		protected T load(final byte[] binary, final int screenCount, final BiFunction<C64Bitmap, C64Screen[], T> constuctor) {
			calcOffsets();
			final byte[] bitmapBinary = new byte[C64Bitmap.WIDTH_BLOCKS * C64Bitmap.HEIGHT_BLOCKS * C64Char.HEIGHT_PIXELS];
			System.arraycopy(binary, bitmapOffset, bitmapBinary, 0, bitmapBinary.length);
			final C64Bitmap bitmap = C64Bitmap.binaryToC64Bitmap(bitmapBinary);

			final C64Screen[] screens = IntStream.range(0, screenCount)//
					.mapToObj(line -> {
						final byte[] screenBinary = new byte[C64Screen.WIDTH_BLOCKS * C64Screen.HEIGHT_BLOCKS];
						System.arraycopy(binary, screensOffset + line * 0x400, screenBinary, 0, screenBinary.length);
						return C64Screen.fromBinary(screenBinary);
					}).toArray(C64Screen[]::new);

			return constuctor.apply(bitmap, screens);
		}

		void save(final C64BinaryProvider binaryProvider, final URI uri, final StartAddress startAddress) {
			switch (startAddress) {
			case InFile -> Utils.writeFile(uri, binaryProvider.toC64Binary(), loadAddress);
			case NotInFile -> Utils.writeFile(uri, binaryProvider.toC64Binary());
			}
		}

		protected C64BinaryProvider binaryProvider(final T obj) {
			calcOffsets();
			final byte[] binary = new byte[length];
			IntStream.range(0, obj.screenCount()).forEach(screenNumber -> {
				final byte[] screenBinary = obj.screen(screenNumber).toC64Binary();
				arraycopy(screenBinary, 0, binary, screensOffset + screenNumber * 0x400, screenBinary.length);
			});
			final byte[] bitmapBinary = obj.bitmap().toC64Binary();
			arraycopy(bitmapBinary, 0, binary, bitmapOffset, bitmapBinary.length);
			return () -> binary;
		}
	}
}
