package de.plush.brix.c64graphics.core.util;

import java.util.function.*;

public class UtilExceptions {

	protected UtilExceptions() {
		throw new UnsupportedOperationException();
	}

	/**
	 * wraps throwing an exception, so it can be used in a ternary operator: <br>
	 * {@code x = condition ? foo : throwEx(new IllegalArgumentException("condition not met"))}.
	 */
	public static <T> T throwEx(final RuntimeException x) {
		throw x;
	}

	@FunctionalInterface
	public interface CheckedSupplier<X> {
		X get() throws Throwable;
	}

	@FunctionalInterface
	public interface CheckedConsumer<X> {
		void accept(X x) throws Throwable;
	}

	@FunctionalInterface
	public interface CheckedFunction<X, Y> {
		Y apply(X x) throws Throwable;
	}

	public static <X> Supplier<X> uncheckedSupplier(final CheckedSupplier<X> supplier) {
		return () -> {
			try {
				return supplier.get();
			} catch (final Throwable checkedException) {
				return rethrowUnchecked(checkedException);
			}
		};
	}

	public static <X> Consumer<X> uncheckedConsumer(final CheckedConsumer<X> consumer) {
		return (x) -> {
			try {
				consumer.accept(x);
			} catch (final Throwable checkedException) {
				rethrowUnchecked(checkedException);
			}
		};
	}

	public static <X, Y> Function<X, Y> uncheckedFunction(final CheckedFunction<X, Y> function) {
		return (x) -> {
			try {
				return function.apply(x);
			} catch (final Throwable checkedException) {
				return rethrowUnchecked(checkedException);
			}
		};
	}

	public static <R> R rethrowUnchecked(final Throwable checkedException) {
		return UtilExceptions.<R, RuntimeException> thrownInsteadOf(checkedException);
	}

	@SuppressWarnings("unused")
	public static <T extends Throwable> void declareToThrow(final Class<T> clazz) throws T {
		// do nothing
	}

	@SuppressWarnings("unchecked")
	private static <R, T extends Throwable> R thrownInsteadOf(final Throwable t) throws T {
		throw (T) t;
	}

}