package de.plush.brix.c64graphics.core.util.charpixeldistance;

import static de.plush.brix.c64graphics.core.util.Utils.*;
import static java.lang.Math.min;

import de.plush.brix.c64graphics.core.model.C64Char;

/**
 * Levenshtein distance: Number of insert/replace/delete operations. <br>
 * This class will get the normalized sum of edits per pixel-line to create the same result, i.e a value between 0 (equal) or 1 (64 edits - every pixel needs to
 * be changed).<br>
 * <b>This is rather slow</b> You might prefer the {@link HiresPixelDistanceHamming}.
 *
 * @see <a href ="https://en.wikipedia.org/wiki/Levenshtein_distance">https://en.wikipedia.org/wiki/Levenshtein_distance</a>
 */
public class HiresPixelDistanceLevenshtein implements C64CharHiresPixelDistanceMeasure {

	@Override
	public double distanceOf(final C64Char c1, final C64Char c2) {
		return c1 == c2 || c1.equals(c2) ? 0 : 1d / 64 * sumOf(c1.toC64Binary(), c2.toC64Binary(), HiresPixelDistanceLevenshtein::byteDistance);
	}

	private static int byteDistance(final byte b1, final byte b2) {
		final int len = C64Char.WIDTH_PIXELS + 1;

		// the array of distances
		int[] cost = new int[len];
		int[] newcost = new int[len];

		// initial cost of skipping prefix in String s0
		for (int i = 0; i < len; ++i) {
			cost[i] = i;
		}

		// dynamically computing the array of distances

		// transformation cost for each letter in s1
		for (int j = 1; j < len; ++j) {
			// initial cost of skipping prefix in String s1
			newcost[0] = j;

			// transformation cost for each letter in s0
			for (int i = 1; i < len; ++i) {
				// matching current letters in both strings
				final int match = testBit(b1, i - 1) == testBit(b2, j - 1) ? 0 : 1;

				// computing cost for each transformation
				final int costReplace = cost[i - 1] + match;
				final int costInsert = cost[i] + 1;
				final int costDelete = newcost[i - 1] + 1;

				// keep minimum cost
				newcost[i] = min(costReplace, min(costInsert, costDelete));
			}

			// swap cost/newcost arrays
			final int[] swap = cost;
			cost = newcost;
			newcost = swap;
		}
		// the distance is the cost for transforming all letters in both strings
		return cost[len - 1];
	}

}