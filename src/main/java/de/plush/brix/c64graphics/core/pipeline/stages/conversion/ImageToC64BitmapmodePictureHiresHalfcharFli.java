package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.Color;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.*;

/**
 * Multicolor Bitmap, 2 screens (FLI-effect every 4 lines), ColorRAM.
 *
 * @author Wanja Gayk
 */
public class ImageToC64BitmapmodePictureHiresHalfcharFli implements IPipelineStage<BufferedImage, C64BitmapmodePictureHiresHalfcharFli> {

	private ImageToC64BitmapmodePictureHiresNscreenFli imageToC64BitmapmodePictureHiresNscreenFli;

	/**
	 * Using the standard {@link MulticolorHalfcharFLIColorClashResolver}. <br>
	 * <p>
	 * Use {@link #ImageToC64BitmapmodePictureHiresHalfcharFli(Palette, IHiresFLIColorClashResolver)} if you want to use a custom color clash resolver.
	 * </p>
	 *
	 * @param palette
	 *            the C64 target palette
	 * @param colorDistanceMeasure
	 *            used to measure how similar/dissimilar a color is.
	 */
	public ImageToC64BitmapmodePictureHiresHalfcharFli(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure) {
		this(palette, new HiresHalfcharFLIColorClashResolver(colorDistanceMeasure));
	}

	/**
	 * Allows using a custom color clash resolver.
	 *
	 * @param palette
	 *            the C64 target palette
	 * @param colorClashResolver
	 *            the color clash resolver used for preprocessing.
	 */
	public ImageToC64BitmapmodePictureHiresHalfcharFli(final Palette palette, final IHiresFLIColorClashResolver colorClashResolver) {
		imageToC64BitmapmodePictureHiresNscreenFli = new ImageToC64BitmapmodePictureHiresNscreenFli(C64BitmapmodePictureHiresHalfcharFli.SCREEN_COUNT, palette,
				colorClashResolver);
	}

	public ImageToC64BitmapmodePictureHiresHalfcharFli withPreferredBackgroundColors(final Color... colors) {
		imageToC64BitmapmodePictureHiresNscreenFli = imageToC64BitmapmodePictureHiresNscreenFli.withPreferredBackgroundColors(colors);
		return this;
	}

	public ImageToC64BitmapmodePictureHiresHalfcharFli withPreferredForegroundColors(final Color... colors) {
		imageToC64BitmapmodePictureHiresNscreenFli = imageToC64BitmapmodePictureHiresNscreenFli.withPreferredForegroundColors(colors);
		return this;
	}

	@Override
	public C64BitmapmodePictureHiresHalfcharFli apply(final BufferedImage original) {
		final C64BitmapmodePictureHiresNscreenFLI result = imageToC64BitmapmodePictureHiresNscreenFli.apply(original);
		return new C64BitmapmodePictureHiresHalfcharFli(result.bitmap(), result.screens());
	}

}
