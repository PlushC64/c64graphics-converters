/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.function.UnaryOperator;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class SaturationChange implements IPipelineStage<BufferedImage, BufferedImage> {

	private final ColorReplacement colorReplacement;

	/**
	 * Alter the saturation of the image.
	 * 
	 * @param factor
	 *            the factor to multiply the saturation with. 0 = no saturation, 0.5 = half the saturatiion, 2.0 = double the saturation, etc.
	 */
	public SaturationChange(final double factor) {
		colorReplacement = new ColorReplacement(color -> saturationChange(factor, color));
	}

	private static Color saturationChange(final double factor, final Color color) {
		final float[] hsb = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
		return Color.getHSBColor(hsb[0], (float)(hsb[1]*factor), hsb[2]);
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		return colorReplacement.apply(original);
	}
}
