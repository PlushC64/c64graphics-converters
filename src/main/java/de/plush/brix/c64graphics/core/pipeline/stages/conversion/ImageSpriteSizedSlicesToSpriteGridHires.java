/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.model.*;

public class ImageSpriteSizedSlicesToSpriteGridHires implements Function<BufferedImage[][], C64SpriteGrid> {

	private final ImageToC64SpriteHires imageToC64Sprite;

	private final Dimension numSprites;

	public ImageSpriteSizedSlicesToSpriteGridHires(final Color pixelColor, final Dimension numSprites) {
		this.numSprites = new Dimension(numSprites);
		imageToC64Sprite = new ImageToC64SpriteHires(pixelColor);
	}

	@Override
	public C64SpriteGrid apply(final BufferedImage[][] imageSlices) {
		final C64SpriteGrid grid = new C64SpriteGrid(numSprites);
		for (int y = 0; y < numSprites.height; ++y) {
			for (int x = 0; x < numSprites.width; ++x) {
				final BufferedImage slice = imageSlices[y][x];
				final C64Sprite c64Sprite = imageToC64Sprite.apply(slice);
				grid.setSprite(x, y, c64Sprite);
			}
		}
		return grid;
	}

}