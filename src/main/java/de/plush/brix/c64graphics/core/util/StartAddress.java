package de.plush.brix.c64graphics.core.util;

public enum StartAddress {
	NotInFile(0), InFile(2);
	public final int skipBytes;

	StartAddress(final int skipBytes) {
		this.skipBytes = skipBytes;
	}
}