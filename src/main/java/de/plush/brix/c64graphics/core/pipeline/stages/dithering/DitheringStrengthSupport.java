package de.plush.brix.c64graphics.core.pipeline.stages.dithering;

public interface DitheringStrengthSupport {

	DitheringStrengthSupport withDitheringStrength(double ditheringStrength);

}
