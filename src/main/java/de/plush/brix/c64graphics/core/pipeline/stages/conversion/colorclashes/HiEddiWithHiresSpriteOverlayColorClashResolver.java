package de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes;

import static de.plush.brix.c64graphics.core.util.Utils.subsetStream;
import static de.plush.brix.c64graphics.core.util.UtilsImage.*;
import static java.lang.Math.min;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toSet;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.HiEddiWithHiresSpriteOverlayColorClashResolver.PreProcessedImage;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.Alignment.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.PaletteConversion.PaletteConvertedImage;
import de.plush.brix.c64graphics.core.util.*;
import de.plush.brix.c64graphics.core.util.Utils.WeightedObject;

/**
 * Resolves color clashes for a hires bitmap with uniformly colored sprite layer (bitmap+screen+sprite layer).
 * 
 * @author Wanja Gayk
 */
public class HiEddiWithHiresSpriteOverlayColorClashResolver implements IPipelineStage<BufferedImage, PreProcessedImage> {

	private final ColorDistanceMeasure colorDistanceMeasure;
	private final Point spriteOverlayLocationAtBlock;
	private final C64SpriteGrid c64SpriteGrid;

	/**
	 * @param spriteOverlaySizeSprites
	 *            number of sprites in x and x direction.
	 * @param spriteOverlayLocationAtBlock
	 * @param colorDistanceMeasure
	 */
	public HiEddiWithHiresSpriteOverlayColorClashResolver(final Dimension spriteOverlaySizeSprites, final Point spriteOverlayLocationAtBlock,
			final ColorDistanceMeasure colorDistanceMeasure) {
		this.spriteOverlayLocationAtBlock = new Point(spriteOverlayLocationAtBlock);
		c64SpriteGrid = new C64SpriteGrid(spriteOverlaySizeSprites);
		this.colorDistanceMeasure = colorDistanceMeasure;
	}

	@Override
	public PreProcessedImage apply(final BufferedImage image) {

		final var original = new ResizeCanvas(C64Bitmap.sizePixels(), () -> Color.BLACK, Horizontal.LEFT, Vertical.TOP).apply(image);
		/*
		 * FIXME: "()-> Color.BLACK" might result in odd color clashes on the right size of images that are not sized as multiple of 8 pixels.
		 * it might be a good idea to copy rightmost and lowest pixels to fill it up to a mutiple of 8..
		 */

		// Note that this resolver makes no assumptions about the positions or size of the sprites. It treats the sprite layer like a bitmap-sized layer of one
		// additional color.

		// TODO: check sprite positioning and chose number of colors accordingly

		return UtilsImage.colors(original).stream() // try each background color
				.map(spriteLayerColor -> {
					final PreProcessedImage result = new PreProcessedImage(original.getWidth(), original.getHeight(), original.getType(),
							c64SpriteGrid.sizeSprites(), spriteOverlayLocationAtBlock, spriteLayerColor);
					final Graphics g = result.getGraphics();
					try {
						IntStream.range(0, original.getHeight())//
								.parallel()//
								.filter(y -> y % C64Char.HEIGHT_PIXELS == 0)//
								.forEach(yStart -> {
									for (int xStart = 0; xStart < original.getWidth(); xStart += C64Char.WIDTH_PIXELS) {
										final int restX = min(C64Char.WIDTH_PIXELS, original.getWidth() - xStart);
										final int restY = min(C64Char.HEIGHT_PIXELS, original.getHeight() - yStart);
										final Rectangle blockPix = new Rectangle(xStart, yStart, restX, restY);
										final BufferedImage subimage = original.getSubimage(xStart, yStart, restX, restY);
										final Set<Color> originalColors = UtilsImage.colors(subimage);

										if (!hasColorClash(spriteLayerColor, originalColors, blockPix)) {
											g.drawImage(subimage, xStart, yStart, null);
										} else {
											final Set<Color[]> alternativeColorSets = get2Or3ColorAlternatives(spriteLayerColor, originalColors, blockPix);
											final Optional<WeightedObject<PaletteConvertedImage>> optionalMin = alternativeColorSets.stream()//
													.map(replacementColors -> (Palette) () -> replacementColors)//
													.map(palette -> new PaletteConversion(palette, colorDistanceMeasure, Dithering.None)
															.apply(deepCopy(subimage)))//
													.map(alternativeSubimage -> new Utils.WeightedObject<>(alternativeSubimage,
															error(alternativeSubimage, subimage, colorDistanceMeasure)))//
													.min(comparing((final WeightedObject<PaletteConvertedImage> weightedImage) -> weightedImage.weight));
											if (!optionalMin.isPresent()) {
												System.out.println("waah!");
											}
											final BufferedImage minErrImage = optionalMin.get().object;
											g.drawImage(minErrImage, xStart, yStart, null);
										}
									}
								});
						return result;
					} finally {
						g.dispose();
					}
				}).map(alternativeImage -> new Utils.WeightedObject<>(alternativeImage, error(alternativeImage, original, colorDistanceMeasure)))//
				.min(comparing((final WeightedObject<PreProcessedImage> weightedImage) -> weightedImage.weight))//
				.map(WeightedObject::object)//
				.get();
	}

	private boolean hasColorClash(final Color spriteLayerColor, final Set<Color> colors, final Rectangle blockPosPix) {
		final var maxColors = pixelsFullyInsideSpriteGrid(blockPosPix) ? 3 : 2;
		return colors.size() > maxColors || colors.size() > maxColors - 1 && !colors.contains(spriteLayerColor);
	}

	private Set<Color[]> get2Or3ColorAlternatives(final Color spriteLayerColor, final Set<Color> orginalColors, final Rectangle blockPosPix) {
		return pixelsFullyInsideSpriteGrid(blockPosPix) ? get3ColorAlternatives(spriteLayerColor, orginalColors) : get2ColorAlternatives(orginalColors);
	}

	private static Set<Color[]> get2ColorAlternatives(final Set<Color> orginalColors) {
		assert orginalColors.size() > 2;
		final ArrayList<Color> superSet = new ArrayList<>(orginalColors);
		final Set<Color[]> alternatives = subsetStream(superSet, 2)// all possible 2 color combinations
				.map(set -> set.toArray(new Color[set.size()]))//
				.collect(toSet());
		return alternatives;
	}

	private static Set<Color[]> get3ColorAlternatives(final Color spriteLayerColor, final Set<Color> orginalColors) {
		assert orginalColors.size() > 3;
		final ArrayList<Color> superSet = new ArrayList<>(orginalColors);
		superSet.remove(spriteLayerColor);// will be added later
		final Set<Color[]> alternatives = subsetStream(superSet, 2)// all possible 2 non-spriteLayerColor colors
				.peek(colorSet -> colorSet.add(spriteLayerColor))// plus spriteLayerColor
				.map(set -> set.toArray(new Color[set.size()]))//
				.collect(toSet());
		return alternatives;
	}

	private boolean pixelsFullyInsideSpriteGrid(final Rectangle blockAreaPix) {
		return overlayAreaPix().contains(blockAreaPix);
	}

	private Rectangle overlayAreaPix() {
		return new Rectangle(//
				new Point(spriteOverlayLocationAtBlock.x * C64Char.WIDTH_PIXELS, //
						spriteOverlayLocationAtBlock.y * C64Char.HEIGHT_PIXELS), //
				c64SpriteGrid.sizePixels());
	}

	public static class PreProcessedImage extends BufferedImage {

		private final Color spriteLayerColor;
		private final Dimension spriteOverlaySizeSprites;
		private final Point spriteOverlayLocationAtBlock;

		public PreProcessedImage(final int width, final int height, final int imageType, final Dimension spriteOverlaySizeSprites,
				final Point spriteOverlayLocationAtBlock, final Color spriteLayerColor) {
			super(width, height, imageType);
			this.spriteOverlayLocationAtBlock = new Point(spriteOverlayLocationAtBlock);
			this.spriteOverlaySizeSprites = new Dimension(spriteOverlaySizeSprites);
			this.spriteLayerColor = spriteLayerColor;
		}

		public Color spriteLayerColor() {
			return spriteLayerColor;
		}

		public Dimension spriteOverlaySizeSprites() {
			return new Dimension(spriteOverlaySizeSprites);
		}

		public Point spriteOverlayLocationAtBlock() {
			return new Point(spriteOverlayLocationAtBlock);
		}

	}
}
