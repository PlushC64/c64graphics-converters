package de.plush.brix.c64graphics.core.model;

import java.awt.Dimension;
import java.util.*;

import de.plush.brix.c64graphics.core.util.Utils;

public class C64Bitmap implements C64BinaryProvider, IRollBlockwise {

	public static final int WIDTH_BLOCKS = C64Screen.WIDTH_BLOCKS;

	public static final int HEIGHT_BLOCKS = C64Screen.HEIGHT_BLOCKS;

	public static final int WIDTH_PIXELS = C64Screen.WIDTH_BLOCKS * C64Char.WIDTH_PIXELS;

	public static final int HEIGHT_PIXELS = C64Screen.HEIGHT_BLOCKS * C64Char.HEIGHT_PIXELS;

	private final C64Char[][] bitmap;

	public C64Bitmap() {
		this(C64Char.BLANK);
	}

	public C64Bitmap(final C64Char defaultBlock) {
		bitmap = new C64Char[C64Screen.HEIGHT_BLOCKS][C64Screen.WIDTH_BLOCKS];
		for (final C64Char[] row : bitmap) {
			Arrays.setAll(row, n -> defaultBlock);
		}
	}

	public C64Bitmap(final C64Bitmap other) {
		bitmap = Utils.deepCopy(other.bitmap);
	}

	public static Dimension sizeBlocks() {
		return new Dimension(WIDTH_BLOCKS, HEIGHT_BLOCKS);
	}

	public static Dimension sizePixels() {
		return new Dimension(WIDTH_PIXELS, HEIGHT_PIXELS);
	}

	public void setBlock(final int xBlock, final int yBlock, final C64Char chrBlock) {
		ensureLegalBlockPosition(xBlock, yBlock);
		bitmap[yBlock][xBlock] = chrBlock;
	}

	public C64Char blockAt(final int xBlock, final int yBlock) {
		ensureLegalBlockPosition(xBlock, yBlock);
		return bitmap[yBlock][xBlock];
	}

	private static void ensureLegalBlockPosition(final int x, final int y) {
		ensureLegalBlockPositionX(x);
		ensureLegalBlockPositionY(y);
	}

	public <T extends Collection<? super C64Char>> T blocksAs(final T target) {
		for (int y = 0; y < C64CharsetScreen.HEIGHT_BLOCKS; ++y) {
			for (int x = 0; x < C64CharsetScreen.WIDTH_BLOCKS; ++x) {
				target.add(blockAt(x, y));
			}
		}
		return target;
	}

	private static void ensureLegalBlockPositionX(final int x) {
		if (x < 0 || WIDTH_BLOCKS < x) { throw new IllegalArgumentException("x out if bounds [0 .. " + WIDTH_BLOCKS + "] -> " + x); }
	}

	private static void ensureLegalBlockPositionY(final int y) {
		if (y < 0 || HEIGHT_BLOCKS < y) { throw new IllegalArgumentException("y out if bounds [0 .. " + HEIGHT_BLOCKS + "] -> " + y); }
	}

	@Override
	public void rollLeftBlockwise(final int n) {
		for (final C64Char[] line : bitmap) {
			Utils.rollLeft(line, n);
		}
	}

	@Override
	public void rollRightBlockwise(final int n) {
		for (final C64Char[] line : bitmap) {
			Utils.rollRight(line, n);
		}
	}

	@Override
	public void rollUpBlockwise(final int n) {
		Utils.rollLeft(bitmap, n);
	}

	@Override
	public void rollDownBlockwise(final int n) {
		Utils.rollRight(bitmap, n);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		for (int yPix = 0; yPix < HEIGHT_PIXELS; ++yPix) {
			for (int xBlock = 0; xBlock < WIDTH_BLOCKS; ++xBlock) {
				final C64Char c64Char = bitmap[yPix / C64Char.HEIGHT_PIXELS][xBlock];
				final byte[] charBinary = c64Char.toC64Binary();
				sb.append(Utils.toBinaryString(charBinary[yPix % C64Char.HEIGHT_PIXELS])).append(' ');
			}
			sb.append('\n');
			if ((yPix + 1) % C64Char.HEIGHT_PIXELS == 0) {
				sb.append('\n');
			}
		}
		return sb.toString();
	}

	public static C64Bitmap binaryToC64Bitmap(final byte[] binary) {
		final C64Bitmap bitmap = new C64Bitmap();
		for (int yBlock = 0; yBlock < HEIGHT_BLOCKS; ++yBlock) {
			for (int xBlock = 0; xBlock < WIDTH_BLOCKS; ++xBlock) {
				final byte[] blockBytes = new byte[C64Char.HEIGHT_PIXELS];
				System.arraycopy(binary, C64Char.HEIGHT_PIXELS * (yBlock * WIDTH_BLOCKS + xBlock), blockBytes, 0, C64Char.HEIGHT_PIXELS);
				bitmap.setBlock(xBlock, yBlock, new C64Char(blockBytes));
			}
		}
		return bitmap;
	}

	@Override
	public byte[] toC64Binary() {
		final byte[] binary = new byte[WIDTH_BLOCKS * HEIGHT_BLOCKS * C64Char.HEIGHT_PIXELS];
		for (int yBlock = 0; yBlock < HEIGHT_BLOCKS; ++yBlock) {
			for (int xBlock = 0; xBlock < WIDTH_BLOCKS; ++xBlock) {
				final byte[] blockBytes = blockAt(xBlock, yBlock).toC64Binary();
				System.arraycopy(blockBytes, 0, binary, C64Char.HEIGHT_PIXELS * (yBlock * WIDTH_BLOCKS + xBlock), C64Char.HEIGHT_PIXELS);
			}
		}
		return binary;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(bitmap);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final C64Bitmap other = (C64Bitmap) obj;
		if (!Arrays.deepEquals(bitmap, other.bitmap)) { return false; }
		return true;
	}

}
