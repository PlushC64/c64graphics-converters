/*
 * Copyright (c) 2012, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package de.plush.brix.c64graphics.core.util;

import java.util.*;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.util.functions.*;

/**
 * A container object which may or may not contain a {@code byte} value. If a value is present, {@code isPresent()} will return {@code true} and
 * {@code getAsByte()} will return the value.
 *
 * <p>
 * Additional methods that depend on the presence or absence of a contained value are provided, such as {@link #orElse(byte) orElse()} (return a default value
 * if value not present) and {@link #ifPresent(ByteConsumer) ifPresent()} (execute a block of code if the value is present).
 *
 * <p>
 * This is a <a href="../lang/doc-files/ValueBased.html">value-based</a> class; use of identity-sensitive operations (including reference equality ({@code ==}),
 * identity hash code, or synchronization) on instances of {@code OptionalByte} may have unpredictable results and should be avoided.
 *
 * @since 1.8
 */
public final class OptionalByte {
	/**
	 * Common instance for {@code empty()}.
	 */
	private static final OptionalByte EMPTY = new OptionalByte();

	/**
	 * If true then the value is present, otherwise indicates no value is present
	 */
	private final boolean isPresent;

	private final byte value;

	/**
	 * Construct an empty instance.
	 *
	 * @implNote Generally only one empty instance, {@link OptionalByte#EMPTY}, should exist per VM.
	 */
	private OptionalByte() {
		isPresent = false;
		value = 0;
	}

	/**
	 * Returns an empty {@code OptionalByte} instance. No value is present for this OptionalByte.
	 *
	 * @apiNote Though it may be tempting to do so, avoid testing if an object is empty by comparing with {@code ==} against instances returned by
	 *          {@code Option.empty()}. There is no guarantee that it is a singleton. Instead, use {@link #isPresent()}.
	 *
	 * @return an empty {@code OptionalByte}
	 */
	public static OptionalByte empty() {
		return EMPTY;
	}

	/**
	 * Construct an instance with the value present.
	 *
	 * @param value
	 *            the int value to be present
	 */
	private OptionalByte(final byte value) {
		isPresent = true;
		this.value = value;
	}

	/**
	 * Return an {@code OptionalByte} with the specified value present.
	 *
	 * @param value
	 *            the value to be present
	 * @return an {@code OptionalByte} with the value present
	 */
	public static OptionalByte of(final byte value) {
		return new OptionalByte(value);
	}

	/**
	 * If a value is present in this {@code OptionalByte}, returns the value, otherwise throws {@code NoSuchElementException}.
	 *
	 * @return the value held by this {@code OptionalByte}
	 * @throws NoSuchElementException
	 *             if there is no value present
	 *
	 * @see OptionalByte#isPresent()
	 */
	public byte getAsByte() {
		if (!isPresent) { throw new NoSuchElementException("No value present"); }
		return value;
	}

	/**
	 * Return {@code true} if there is a value present, otherwise {@code false}.
	 *
	 * @return {@code true} if there is a value present, otherwise {@code false}
	 */
	public boolean isPresent() {
		return isPresent;
	}

	/**
	 * Have the specified consumer accept the value if a value is present, otherwise do nothing.
	 *
	 * @param consumer
	 *            block to be executed if a value is present
	 * @throws NullPointerException
	 *             if value is present and {@code consumer} is null
	 */
	public void ifPresent(final ByteConsumer consumer) {
		if (isPresent) {
			consumer.accept(value);
		}
	}

	/**
	 * Return the value if present, otherwise return {@code other}.
	 *
	 * @param other
	 *            the value to be returned if there is no value present
	 * @return the value, if present, otherwise {@code other}
	 */
	public byte orElse(final byte other) {
		return isPresent ? value : other;
	}

	/**
	 * Return the value if present, otherwise invoke {@code other} and return the result of that invocation.
	 *
	 * @param other
	 *            a {@code IntSupplier} whose result is returned if no value is present
	 * @return the value if present otherwise the result of {@code other.getAsInt()}
	 * @throws NullPointerException
	 *             if value is not present and {@code other} is null
	 */
	public byte orElseGet(final ByteSupplier other) {
		return isPresent ? value : other.getAsByte();
	}

	/**
	 * Return the contained value, if present, otherwise throw an exception to be created by the provided supplier.
	 *
	 * @apiNote A method reference to the exception constructor with an empty argument list can be used as the supplier. For example,
	 *          {@code IllegalStateException::new}
	 *
	 * @param <X>
	 *            Type of the exception to be thrown
	 * @param exceptionSupplier
	 *            The supplier which will return the exception to be thrown
	 * @return the present value
	 * @throws X
	 *             if there is no value present
	 * @throws NullPointerException
	 *             if no value is present and {@code exceptionSupplier} is null
	 */
	public <X extends Throwable> byte orElseThrow(final Supplier<X> exceptionSupplier) throws X {
		if (isPresent) { return value; }
		throw exceptionSupplier.get();
	}

	/**
	 * If a value is present, apply the provided mapping function to it, and if the result is non-null, return an {@code OptionalByte} describing the result.
	 * Otherwise return an empty {@code OptionalByte}.
	 *
	 * @param mapper
	 *            a mapping function to apply to the value, if present
	 * @return an {@code OptionalByte} describing the result of applying a mapping function to the value of this {@code OptionalByte}, if a value is present,
	 *         otherwise an empty {@code Optional}
	 * @throws NullPointerException
	 *             if the mapping function is null
	 */
	public OptionalByte map(final ByteToByteFunction mapper) {
		Objects.requireNonNull(mapper);
		if (!isPresent()) { return empty(); }
		return OptionalByte.of(mapper.valueOf(value));
	}

	/**
	 * If a value is present, apply the provided mapping function to it, and if the result is non-null, return an {@code Optional} describing the result.
	 * Otherwise return an empty {@code Optional}.
	 *
	 * @param mapper
	 *            a mapping function to apply to the value, if present
	 * @return an {@code Optional} describing the result of applying a mapping function to the value of this {@code Optional}, if a value is present, otherwise
	 *         an empty {@code Optional}
	 * @throws NullPointerException
	 *             if the mapping function is null
	 */
	public <T> Optional<T> mapToObj(final ByteToObjectFunction<T> mapper) {
		Objects.requireNonNull(mapper);
		if (!isPresent()) { return Optional.empty(); }
		return Optional.ofNullable(mapper.valueOf(value));
	}

	/**
	 * If a value is present, apply the provided {@code OptionalByte}-bearing mapping function to it, return that result, otherwise return an empty
	 * {@code OptionalByte}. This method is similar to {@link #map(ByteToByteFunction)}, but the provided mapper is one whose result is already an
	 * {@code OptionalByte}, and if invoked, {@code flatMap} does not wrap it with an additional {@code OptionalByte}.
	 *
	 * @param mapper
	 *            a mapping function to apply to the value, if present the mapping function
	 * @return the result of applying an {@code OptionalByte}-bearing mapping function to the value of this {@code OptionalByte}, if a value is present,
	 *         otherwise an empty {@code OptionalByte}
	 * @throws NullPointerException
	 *             if the mapping function is null or returns a null result
	 */
	public OptionalByte flatMap(final ByteToObjectFunction<OptionalByte> mapper) {
		Objects.requireNonNull(mapper);
		if (!isPresent()) { return empty(); }
		return Objects.requireNonNull(mapper.valueOf(value));
	}

	/**
	 * Indicates whether some other object is "equal to" this OptionalByte. The other object is considered equal if:
	 * <ul>
	 * <li>it is also an {@code OptionalByte} and;
	 * <li>both instances have no value present or;
	 * <li>the present values are "equal to" each other via {@code ==}.
	 * </ul>
	 *
	 * @param obj
	 *            an object to be tested for equality
	 * @return {code true} if the other object is "equal to" this object otherwise {@code false}
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }

		if (!(obj instanceof OptionalByte)) { return false; }

		final OptionalByte other = (OptionalByte) obj;
		return isPresent && other.isPresent ? value == other.value : isPresent == other.isPresent;
	}

	/**
	 * Returns the hash code value of the present value, if any, or 0 (zero) if no value is present.
	 *
	 * @return hash code value of the present value or 0 if no value is present
	 */
	@Override
	public int hashCode() {
		return isPresent ? Integer.hashCode(value) : 0;
	}

	/**
	 * {@inheritDoc}
	 *
	 * Returns a non-empty string representation of this object suitable for debugging. The exact presentation format is unspecified and may vary between
	 * implementations and versions.
	 *
	 * @implSpec If a value is present the result must include its string representation in the result. Empty and present instances must be unambiguously
	 *           differentiable.
	 *
	 * @return the string representation of this instance
	 */
	@Override
	public String toString() {
		return isPresent ? String.format("OptionalByte[%s]", value) : "OptionalByte.empty";
	}
}
