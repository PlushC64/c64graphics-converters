/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces;

import java.awt.Color;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Represents a Color in the "Chroma - Luma" color space for analog video signals from digital sources, such as DVD and DVB, as used in
 * Composite video signals from DVD players or home computers.
 * 
 * @see <a href="https://en.wikipedia.org/wiki/YPbPr">see: https://en.wikipedia.org/wiki/YPbPr</a>
 */
@SuppressFBWarnings(value = "NM_METHOD_NAMING_CONVENTION", //
		justification = "using the same variable casing as in the given formulas")
public record ColorYPbPr(double Y, double Pb, double Pr) {

	/**
	 * A constant for color black. Color components are:
	 * 
	 * <pre>
	 *     Y: 0.0
	 *     Cb: 0.0
	 *     Cr: 0.0
	 * </pre>
	 */
	public static final ColorYPbPr BLACK = ColorConversions.convertRGBtoYPbPr(Color.BLACK);

	/**
	 * A constant for color white. Color components are:
	 *
	 * <pre>
	 *     Y: 255.0
	 *     Cb: ~0.0
	 *     Cr: ~0.0
	 * </pre>
	 */
	public static final ColorYPbPr WHITE = ColorConversions.convertRGBtoYPbPr(Color.WHITE);

	@SuppressWarnings("checkstyle:EqualsHashCode") // hashCode can be inherited in this special case
	@Override
	public boolean equals(final Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }

		final ColorYPbPr colorYUV = (ColorYPbPr) o;
		if (Double.compare(colorYUV.Y(), Y()) != 0) { return false; }
		if (Double.compare(colorYUV.Pb(), Pb()) != 0) { return false; }
		if (Double.compare(colorYUV.Pr(), Pr()) != 0) { return false; }

		return true;
	}

}
