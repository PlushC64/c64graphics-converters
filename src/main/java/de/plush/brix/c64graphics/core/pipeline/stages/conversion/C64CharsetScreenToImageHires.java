package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;

public class C64CharsetScreenToImageHires extends AbstractCharsetScreenToImage {

	private final Supplier<Color> backgroundSupplier;

	private final Palette palette;

	public C64CharsetScreenToImageHires() {
		this(C64ColorsColodore.PALETTE, () -> BLACK.color, WHITE.color);
	}

	public C64CharsetScreenToImageHires(final Palette palette, final Supplier<Color> backgroundSupplier, final Color pixelColor) {
		this(palette, backgroundSupplier, new C64ColorRam(palette.colorCodeOrException(pixelColor)));
	}

	public C64CharsetScreenToImageHires(final Palette palette, final Supplier<Color> backgroundSupplier, final C64ColorRam colorRAM) {
		super(colorRAM);
		this.palette = palette;
		this.backgroundSupplier = backgroundSupplier;
	}

	@Override
	protected IPipelineStage<C64CharsetScreenColorRam, BufferedImage> createC64CharsetScreenColorRamToImage() {
		return new C64CharmodePictureToImageHires(palette).withForcedBackgroundColor(backgroundSupplier.get());
	}

}
