package de.plush.brix.c64graphics.core.util;

import static java.util.Comparator.*;

import java.util.Comparator;

public class DoubleObjectPair<T> implements Comparable<DoubleObjectPair<T>> {

	private static final Comparator<DoubleObjectPair<?>> ORDER_BY_DOUBLE = nullsFirst(comparingDouble(DoubleObjectPair::getOne));

	private static final Comparator<Object> ORDER_BY_IDENTiTY = comparingInt(System::identityHashCode);

	private static final Comparator<DoubleObjectPair<?>> NATURAL_ORDER = nullsFirst(new Comparator<>() {
		@SuppressWarnings({ "rawtypes", "unchecked" })
		@Override
		public int compare(final DoubleObjectPair<?> o1, final DoubleObjectPair<?> o2) {
			final int c = ORDER_BY_DOUBLE.compare(o1, o2);
			return c != 0 ? c
					: ((Comparator) (o1.two instanceof Comparable && o2.two instanceof Comparable //
							? naturalOrder() //
							: ORDER_BY_IDENTiTY)//
			).compare(o1.two, o2.two);
		}
	});

	private final double one;

	private final T two;

	public DoubleObjectPair(final double newOne, final T newTwo) {
		this.one = newOne;
		this.two = newTwo;
	}

	public static <T> DoubleObjectPair<T> of(final double d, final T o) {
		return new DoubleObjectPair<>(d, o);
	}

	public double getOne() {
		return this.one;
	}

	public T getTwo() {
		return this.two;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(one);
		result = prime * result + (int) (temp ^ temp >>> 32);
		result = prime * result + (two == null ? 0 : two.hashCode());
		return result;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final DoubleObjectPair other = (DoubleObjectPair) obj;
		if (Double.doubleToLongBits(one) != Double.doubleToLongBits(other.one)) { return false; }
		if (two == null) {
			if (other.two != null) { return false; }
		} else if (!two.equals(other.two)) { return false; }
		return true;
	}

	@Override
	public String toString() {
		return this.one + ":" + this.two;
	}

	@Override
	public int compareTo(final DoubleObjectPair<T> that) {
		return NATURAL_ORDER.compare(this, that);
	}

}
