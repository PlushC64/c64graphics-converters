package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toMap;

import java.util.Map;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore;
import de.plush.brix.c64graphics.core.util.charpixeldistance.C64CharHiresPixelDistanceMeasure;

public class C64BitmapToC64CharsetScreenHiresUsingCharset extends AbstractC64BitmapToCharsetScreenUsingCharset {

	private final C64CharHiresPixelDistanceMeasure distanceMeasure;

	/**
	 * Creates a charset screen image, trying to match each block to the given charset
	 *
	 * @param charset
	 *            a charset to use to approximate the image
	 * @param distanceMeasure
	 *            a measure to calculate how close a character is to another one.
	 */
	public C64BitmapToC64CharsetScreenHiresUsingCharset(final C64Charset charset, final C64CharHiresPixelDistanceMeasure distanceMeasure) {
		super(charset);
		requireNonNull(distanceMeasure);
		this.distanceMeasure = distanceMeasure;
	}

	@Override
	public Function<C64Char, PreProcessedCharacterImage> createC64CharToImage() {
		return new C64CharToImageHires(PALETTE, BLACK, C64ColorsColodore.WHITE.color);
	}

	@Override
	protected Map<C64Char, C64Char> bitmapBlockToClosestCharsetBlock(final C64Bitmap bitmap) {
		return uniqueChars(bitmap).stream()//
				.collect(toMap(Function.identity(), //
						bmapBlock -> charset.closestMatch(bmapBlock, distanceMeasure)));
	}

}
