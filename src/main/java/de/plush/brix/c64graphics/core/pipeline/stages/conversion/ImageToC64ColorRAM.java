package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.Alignment.*;

public class ImageToC64ColorRAM implements IPipelineStage<BufferedImage, C64ColorRam> {

	private final Supplier<Color> backgroundColorProvider;

	private final PaletteConversion paletteConversion;

	private final Palette palette;

	public ImageToC64ColorRAM(final Supplier<Color> backgroundColorProvider, final Palette palette, final ColorDistanceMeasure colorDistanceMeasure) {
		this.backgroundColorProvider = backgroundColorProvider;
		this.palette = palette;
		paletteConversion = new PaletteConversion(palette, colorDistanceMeasure, Dithering.None);
	}

	@Override
	public C64ColorRam apply(final BufferedImage image) {
		final var targetSize = C64ColorRam.sizeBlocks();
		final int bgIndex = palette.colorCodeOrException(backgroundColorProvider.get());
		final var rescaledImage = new Rescale(targetSize, backgroundColorProvider).apply(image); // may leave a smaller width or height
		final var imageWithAppliedPalette = paletteConversion.apply(rescaledImage); // make it correct width or height
		final var cRamImage = new ResizeCanvas(C64Bitmap.sizeBlocks(), backgroundColorProvider, Horizontal.CENTER, Vertical.CENTER).apply(
				imageWithAppliedPalette);

		final byte bgColorCode = toUnsignedByte(bgIndex);
		final C64ColorRam cRam = new C64ColorRam(bgColorCode);
		for (int y = 0; y < cRamImage.getHeight(); ++y) {
			for (int x = 0; x < cRamImage.getWidth(); ++x) {
				final Color color = new Color(cRamImage.getRGB(x, y));
				cRam.setColorCode(x, y, palette.colorCode(color).getAsByte()); // palette applied -> color code must be present
			}
		}
		return cRam;
	}

}
