package de.plush.brix.c64graphics.core.util.functions;

@FunctionalInterface
public interface ByteToIntFunction {
	int valueOf(byte byteParameter);
}
