package de.plush.brix.c64graphics.core.pipeline.stages.dithering;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.model.C64Char;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;

/**
 * Holds specs according to various dithering algorithms described in: <a href=
 * "http://www.tannerhelland.com/4660/dithering-eleven-algorithms-source-code/">http://www.tannerhelland.com/4660/dithering-eleven-algorithms-source-code/</a>
 *
 * @author Wanja Gayk
 */
public enum Dithering implements DitheringSpec {

	None(new NoDitheringSpec()), // #
	/**
	 * A dithering that will apply a palette of native colors plus colors that can be mixed from "mixable" clusters of these colors to the target image.The
	 * mix-colors will then be replaced by a simple Pattern of the originating colors.
	 */
	LumaPairing_Scanline(new NoDitheringSpec() {
		@Override
		public Function<BufferedImage, BufferedImage> algorithm(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure) {
			return new LumaPairingDither(palette, ColorClusterIndexes.VIC_II_lenient, ColorMix.HUNTER_Lab, colorDistanceMeasure, new C64Char("""
					11111111
					00000000
					11111111
					00000000
					11111111
					00000000
					11111111
					00000000
					"""));
		}
	}), //
	/**
	 * A dithering that will apply a palette of native colors plus colors that can be mixed from "mixable" clusters of these colors to the target image.The
	 * mix-colors will then be replaced by a simple Pattern of the originating colors.
	 */
	LumaPairing_Hires(new NoDitheringSpec() {
		@Override
		public Function<BufferedImage, BufferedImage> algorithm(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure) {
			return new LumaPairingDither(palette, ColorClusterIndexes.VIC_II_lenient, ColorMix.HUNTER_Lab, colorDistanceMeasure, new C64Char("""
					10101010
					01010101
					10101010
					01010101
					10101010
					01010101
					10101010
					01010101
					"""));
		}
	}), //
	/**
	 * A dithering that will apply a palette of native colors plus colors that can be mixed from "mixable" clusters of these colors to the target image.The
	 * mix-colors will then be replaced by a simple Pattern of the originating colors.
	 */
	LumaPairing_Multicolor(new NoDitheringSpec() {
		@Override
		public Function<BufferedImage, BufferedImage> algorithm(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure) {
			return new LumaPairingDither(palette, ColorClusterIndexes.VIC_II_lenient, ColorMix.HUNTER_Lab, colorDistanceMeasure, new C64Char("""
					11001100
					00110011
					11001100
					00110011
					11001100
					00110011
					11001100
					00110011
					"""));
		}
	}), //
	/** Best known error diffusion dithering. */
	Floyd_Steinberg(new SlidingMatrixDitheringSpec(1.0 / 16, new int[][] { //
			{ 0, X, 7 }, //
			{ 3, 5, 1 } })), //

	/** Error diffusion dithering that tends to leave less lone pixels on uniformly colored areas than Floyd Steinberg. */
	Atkinson(new SlidingMatrixDitheringSpec(1.0 / 8, new int[][] { //
			{ 0, X, 1, 1 }, //
			{ 1, 1, 1, 0 }, //
			{ 0, 1, 0, 0 } })), //

	Jarvis_Judice_Ninke(new SlidingMatrixDitheringSpec(1.0 / 48, new int[][] { //
			{ 0, 0, X, 7, 5 }, //
			{ 3, 5, 7, 5, 3 }, //
			{ 1, 3, 5, 3, 1 } })), //

	Stucki(new SlidingMatrixDitheringSpec(1.0 / 42, new int[][] { //
			{ 0, 0, X, 8, 4 }, //
			{ 2, 4, 8, 4, 2 }, //
			{ 1, 2, 4, 2, 1 } })), //

	Burkes(new SlidingMatrixDitheringSpec(1.0 / 32, new int[][] { //
			{ 0, 0, X, 8, 4 }, //
			{ 2, 4, 8, 4, 2 } })), //

	Sierra(new SlidingMatrixDitheringSpec(1.0 / 32, new int[][] { //
			{ 0, 0, X, 5, 3 }, //
			{ 2, 4, 5, 4, 2 }, //
			{ 0, 2, 3, 2, 0 } })), //

	Sierra_two_row(new SlidingMatrixDitheringSpec(1.0 / 16, new int[][] { //
			{ 0, 0, X, 4, 3 }, //
			{ 1, 2, 3, 2, 1 } })), //

	/** Has a very tiny kernel using powers of two to gain speed. */
	Sierra_Lite(new SlidingMatrixDitheringSpec(1.0 / 4, new int[][] { //
			{ 0, X, 2 }, //
			{ 1, 1, 0 } })), //

	Ordered2x2x2(new FixedMatrixDitheringSpec(1.0 / 2, new int[][] { //
			{ 0, 1 }, //
			{ 1, 0 }, //
	})), //
	Ordered2x2x4(new FixedMatrixDitheringSpec(1.0 / 4, new int[][] { //
			{ 0, 2 }, //
			{ 3, 1 }, //
	})), //
	Ordered4x4x16(new FixedMatrixDitheringSpec(1.0 / 16, new int[][] { //
			{ 0, 8, 2, 10 }, //
			{ 12, 4, 14, 6 }, //
			{ 3, 11, 1, 9 }, //
			{ 15, 7, 13, 5 }, //
	})), //
	Ordered2x2x2Multicolor(new FixedMatrixDitheringSpec(1.0 / 2, new int[][] { //
			{ 0, 0, 1, 1 }, //
			{ 1, 1, 0, 0 }, //
	})), //
	Ordered2x2x4Multicolor(new FixedMatrixDitheringSpec(1.0 / 4, new int[][] { //
			{ 0, 0, 2, 2 }, //
			{ 3, 3, 1, 1 }, //
	})), //
	Ordered2x4x2Multicolor(new FixedMatrixDitheringSpec(1.0 / 2, new int[][] { //
			{ 0, 0, 1, 1 }, //
			{ 0, 0, 1, 1 }, //
			{ 1, 1, 0, 0 }, //
			{ 1, 1, 0, 0 }, //
	})), //
	Ordered4x4x16Multicolor(new FixedMatrixDitheringSpec(1.0 / 16, new int[][] { //
			{ 0, 0, 8, 8, 2, 2, 10, 10 }, //
			{ 12, 12, 4, 4, 14, 14, 6, 6 }, //
			{ 3, 3, 11, 11, 1, 1, 9, 9 }, //
			{ 15, 15, 7, 7, 13, 13, 5, 5 }, //
	})), //
	Ordered1x3(new FixedMatrixDitheringSpec(1.0 / 3, new int[][] { //
			{ 0 }, //
			{ 1 }, //
			{ 2 }, //
	})), //
	Ordered3x1(new FixedMatrixDitheringSpec(1.0 / 3, new int[][] { //
			{ 0, 1, 2 }, //
	})), //
	SlashHires(new FixedMatrixDitheringSpec(1.0 / 4, new int[][] { //
			{ 0, 1, 2, 3 }, // 7
			{ 1, 2, 3, 0 }, // 8
			{ 2, 3, 0, 1 }, // 7
			{ 3, 0, 1, 2 }, // 8
	})), //
	SlashMulticolor(new FixedMatrixDitheringSpec(1.0 / 4, new int[][] { //
			{ 0, 0, 1, 1, 2, 2, 3, 3 }, // 7
			{ 1, 1, 2, 2, 3, 3, 0, 0 }, // 8
			{ 2, 2, 3, 3, 0, 0, 1, 1 }, // 7
			{ 3, 3, 0, 0, 1, 1, 2, 2 }, // 8
	})), //
	BackslashHires(new FixedMatrixDitheringSpec(1.0 / 4, new int[][] { //
			{ 3, 2, 1, 0 }, //
			{ 0, 3, 2, 1 }, //
			{ 1, 0, 3, 2 }, //
			{ 2, 1, 0, 3 }, //
	})), //
	BackslashMulticolor(new FixedMatrixDitheringSpec(1.0 / 4, new int[][] { //
			{ 3, 3, 2, 2, 1, 1, 0, 0 }, //
			{ 0, 0, 3, 3, 2, 2, 1, 1 }, //
			{ 1, 1, 0, 0, 3, 3, 2, 2 }, //
			{ 2, 2, 1, 1, 0, 0, 3, 3 }, //
	})), //

	Circles(new FixedMatrixDitheringSpec(1.0 / 3, new int[][] { //
			{ 0, 0, 1, 1, 1, 0, 0, 2 }, //
			{ 0, 1, 0, 0, 0, 1, 0, 0 }, //
			{ 1, 0, 1, 2, 1, 0, 1, 0 }, //
			{ 1, 0, 2, 1, 2, 0, 1, 0 }, //
			{ 1, 0, 1, 2, 1, 0, 1, 0 }, //
			{ 0, 1, 0, 0, 0, 1, 0, 0 }, //
			{ 0, 0, 1, 1, 1, 0, 0, 2 }, //
			{ 2, 0, 0, 0, 0, 0, 2, 0 }, //
	})), //

	CirclesMulticolor(new FixedMatrixDitheringSpec(1.0 / 3, new int[][] { //
			{ 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 2, 2 }, //
			{ 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0 }, //
			{ 1, 1, 0, 0, 1, 1, 2, 2, 1, 1, 0, 0, 1, 1, 0, 0 }, //
			{ 1, 1, 0, 0, 2, 2, 1, 1, 2, 2, 0, 0, 1, 1, 0, 0 }, //
			{ 1, 1, 0, 0, 1, 1, 2, 2, 1, 1, 0, 0, 1, 1, 0, 0 }, //
			{ 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 2, 2 }, //
			{ 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0 }, //
	})), //

	;

	private final DitheringSpec spec;

	Dithering(final DitheringSpec spec) {
		this.spec = spec;
	}

	public DitheringSpec spec() {
		return spec instanceof final NoDitheringSpec nds ? nds.clone() //
				: spec instanceof final SlidingMatrixDitheringSpec smds ? smds.clone()
						: spec instanceof final FixedMatrixDitheringSpec fms ? fms.clone() //
								: this; // can't be modified -> safe
	}

	public boolean supportsDitheringStrength() {
		return spec instanceof DitheringStrengthSupport;
	}

	@Override
	public Point centerPoint(final Point globalPosition) {
		return new Point(spec.centerPoint(globalPosition)); // don't share the original!
	}

	@Override
	public OptionalDouble errorMultiplier(final Point matrixPosition, final Point globalPosition) {
		return spec.errorMultiplier(matrixPosition, globalPosition);
	}

	@Override
	public Point centerPointOffset(final Point matrixPosition, final Point globalPosition) {
		return new Point(spec.centerPointOffset(matrixPosition, globalPosition)); // don't share the original!
	}

	@Override
	public Dimension matrixSize() {
		return new Dimension(spec.matrixSize()); // don't share the original!
	}

	public static String name(final DitheringSpec spec) {
		return Arrays.stream(values()).filter(v -> v == spec).findFirst()//
				.map(v -> Dithering.class.getSimpleName() + "." + v.name())//
				.orElse(spec.getClass().getName());
	}

	@Override
	public Function<BufferedImage, BufferedImage> algorithm(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure) {
		return spec.algorithm(palette, colorDistanceMeasure);
	}
}
