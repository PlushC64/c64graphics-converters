package de.plush.brix.c64graphics.core.pipeline.stages.dithering;

import static de.plush.brix.c64graphics.core.util.Utils.deepCopy;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.util.Utils;

public class SlidingMatrixDitheringSpec implements DitheringSpec {

	private final double errorMultiplier;

	private final int[][] matrix;

	private final Dimension matrixSize;

	private final Point centerPoint;

	private final double[][] preMultipliedMatrix;

	/**
	 * DIthering Spec for simple matrices with a center point.<br>
	 * Sample setup for a Floyd Steinberg dither: <code>
	 * <pre>
	 *  import static de.plush.brix.c64graphics.core.pipeline.stages.dithering.DitheringSpec.X;
	 *
	 *  final int[][] floydSteinbergMatrix = new int[][]{
	 *   {0,X,7},
	 *   {3,5,1}};
	 *  final double errorMultiplier = 1.0/16;
	 *  final DitheringSpec floydSteinbergSpec = new SlidingMatrixDitheringSpec(errorMultiplier, floydSteinbergMatrix);
	 * </pre>
	 *</code>
	 *
	 * @see Dithering
	 * @param errorMultiplier
	 *            the error divisor
	 * @param matrix
	 *            error distribution matrix, use {@link Integer#MAX_VALUE} for the center point.
	 */
	public SlidingMatrixDitheringSpec(final double errorMultiplier, final int[][] matrix) {
		this.errorMultiplier = errorMultiplier;
		this.matrix = deepCopy(matrix);
		matrixSize = sizeOf(matrix);
		centerPoint = findCenter(matrix);
		preMultipliedMatrix = preMultipliedMatrix();
	}

	public SlidingMatrixDitheringSpec(final SlidingMatrixDitheringSpec spec) {
		this(spec.errorMultiplier, spec.matrix);
	}

	@Override
	public SlidingMatrixDitheringSpec clone() {
		return new SlidingMatrixDitheringSpec(this);
	}

	@Override
	public Dimension matrixSize() {
		return new Dimension(matrixSize);
	}

	@Override
	public Point centerPoint(final Point globalPosition) {
		return new Point(centerPoint);
	}

	@Override
	public Point centerPointOffset(final Point matrixPosition, final Point globalPosition) {
		return new Point(matrixPosition.x - centerPoint.x, matrixPosition.y - centerPoint.y);
	}

	private double[][] preMultipliedMatrix() {
		final var preMultiplied = new double[matrix.length][];
		for (int y = 0; y < matrix.length; ++y) {
			preMultiplied[y] = new double[matrix[y].length];
			for (int x = 0; x < matrix[y].length; ++x) {
				// if (boost) {
				// preMultiplied[y][x] = (matrix[y][x] + 1) * (1.0 / (1.0 / errorMultiplier + 1));
				// } else {
				preMultiplied[y][x] = matrix[y][x] == X ? X : matrix[y][x] * errorMultiplier;
				// }
			}
		}
		return preMultiplied;
	}

	@Override
	public OptionalDouble errorMultiplier(final Point matrixPosition, final Point globalPosition) {
		final double valueAtPos = preMultipliedMatrix[matrixPosition.y][matrixPosition.x];
		return valueAtPos == 0 || valueAtPos == DitheringSpec.X ? OptionalDouble.empty() //
				: OptionalDouble.of(valueAtPos);
	}

	private static Point findCenter(final int[][] matrix) {
		final int height = matrix.length;
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < matrix[y].length; ++x) {
				if (matrix[y][x] == X) {
					return new Point(x, y);
				}
			}
		}
		throw new IllegalArgumentException("Dithering spec matrix has no center point (DitheringSpec.X): " + Utils.toString(matrix));
	}

	private static Dimension sizeOf(final int[][] matrix) {
		final int height = matrix.length;
		final int width = matrix[0].length;
		for (int y = 1; y < height; ++y) {
			if (matrix[y].length != width) {
				throw new IllegalArgumentException("Matrix must be uniform: Each line must have the same amount of columns: " + Utils.toString(matrix));
			}
		}
		return new Dimension(width, height);
	}

	@Override
	public Function<BufferedImage, BufferedImage> algorithm(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure) {
		return new SlidingWindowErrorDiffusionDithering(palette, colorDistanceMeasure, this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(matrix);
		result = prime * result + Objects.hash(centerPoint, errorMultiplier);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final SlidingMatrixDitheringSpec other = (SlidingMatrixDitheringSpec) obj;
		return Objects.equals(centerPoint, other.centerPoint) && Double.doubleToLongBits(errorMultiplier) == Double.doubleToLongBits(other.errorMultiplier)
				&& Arrays.deepEquals(matrix, other.matrix);
	}

}
