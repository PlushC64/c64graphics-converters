package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import java.awt.Color;
import java.util.function.Supplier;

public interface C64Color extends Supplier<Color> {

	/**
	 * Same as {@link Supplier#get()}
	 *
	 * @return the color.
	 */
	default Color color() {
		return get();
	}

	byte colorCode();

	Palette palette();

	default C64Color[] c64Colors() {
		if (!getClass().isEnum()) {
			throw new UnsupportedOperationException(getClass() + " must be an enum for this operation to succeed. See " + C64ColorsColodore.class);
		}
		return getClass().getEnumConstants();
	}

	default C64Color d800ReplacementForMulticolorInCharmode() {
		final C64Color[] c64Colors = c64Colors();
		final byte colorCode = colorCode();
		if (colorCode > 0x07) {
			throw new IllegalArgumentException(
					this + " cannot be displayed in color ram in multicolor charmode, it is displayed as " + c64Colors[colorCode & 0b111] + ".");
		}
		return c64Colors[colorCode | 0b1000];
	}

}
