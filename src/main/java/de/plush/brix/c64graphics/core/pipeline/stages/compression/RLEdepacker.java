package de.plush.brix.c64graphics.core.pipeline.stages.compression;

import java.util.function.Function;

import de.plush.brix.c64graphics.core.util.Utils;
import de.plush.brix.c64graphics.core.util.collections.ByteArrayList;

/**
 * A stateless, simple RLE-depacker, that uses a marker byte to mark runs.
 * <dl>
 * <dt>Run Sequence
 * <dd>[controlByte, count, byte]
 * <dt>Escape Sequence
 * <dd>[controlByte, count, controlByte]
 * <dt>End Sequence
 * <dd>[controlByte, 0]
 * </dl>
 * This is the same packing scheme that Amica Paint uses, where the controlByte is <tt>0xC2</tt>. 
 */
public class RLEdepacker implements Function<byte[], byte[]> {

	private final byte controlByte;

	public RLEdepacker(final byte controlByte) {
		this.controlByte = controlByte;
	}

	@Override
	public byte[] apply(final byte[] input) {
		final byte[] unpacked = unpack(input);
		System.out.println("raw length: " + input.length + " -> " + unpacked.length + " (bytes)");
		return unpacked;
	}

	byte[] unpack(final byte[] packed) {
		final var unpacked = new ByteArrayList(packed.length * 3); // just eyeballing the result length
		for (int t = 0; t < packed.length; ++t) {
			final var b = packed[t];
			if (b == controlByte) {
				final int n = Utils.toPositiveInt(packed[++t]);
				if (n == 0) { return unpacked.toArray(); }
				unpacked.addAll(ByteArrayList.ofNValues(n, packed[++t]));
			} else {
				unpacked.add(b);
			}
		}
		return unpacked.toArray();
	}
}
