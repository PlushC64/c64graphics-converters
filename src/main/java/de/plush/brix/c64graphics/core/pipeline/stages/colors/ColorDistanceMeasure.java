package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import java.awt.Color;
import java.util.Comparator;

/**
 * Implementors are expected to return the distance between to colors as </tt>0.0</tt> if a.equals(b), a distance value from <tt>0.0</tt> to <tt>1.0</tt>
 * (inclusive) otherwise, where <tt>distance(a,b) == distance(b,a)</tt>.
 */
@FunctionalInterface
public interface ColorDistanceMeasure {
	/**
	 * Get the distance between to colors.
	 *
	 * @param a
	 *            some color
	 * @param b
	 *            another color
	 * @return </tt>0.0</tt> if a.equals(b), a distance value from <tt>0.0</tt> to <tt>1.0</tt> (inclusive) otherwise, where
	 *         <tt>distance(a,b) == distance(b,a)</tt>
	 */
	double distanceOf(Color a, Color b);

	default Comparator<Color> comparingDistanceTo(final Color any) {
		return (final Color c1, final Color c2) -> Double.compare(distanceOf(c1, any), distanceOf(c2, any));
	}

}
