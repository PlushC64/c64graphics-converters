package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static java.util.Comparator.*;
import static java.util.stream.Collectors.toList;

import java.util.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Stream;

import de.plush.brix.c64graphics.core.model.C64Char;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.ColorDistanceMeasure;
import de.plush.brix.c64graphics.core.util.*;
import de.plush.brix.c64graphics.core.util.collections.HashBag;

public abstract class AbstractReduceNumberOfUniformlyColoredCharacters implements IPipelineStage<HashBag<C64Char>, List<C64Char[]>> {

	protected final int maxCharacters;

	protected final ColorDistanceMeasure distanceMeasure;

	protected abstract Function<C64Char, PreProcessedCharacterImage> createC64CharToImage();

	public AbstractReduceNumberOfUniformlyColoredCharacters(final int maxCharacters, final ColorDistanceMeasure distanceMeasure) {
		this.maxCharacters = maxCharacters;
		this.distanceMeasure = distanceMeasure;
	}

	/**
	 * Creates a mapping from the given characters to a replacement or itself.
	 * 
	 * @return a map containing all characters of the original set and their replacements (which might be the original character itself).
	 */
	@Override
	public final List<C64Char[]> apply(final HashBag<C64Char> characters) {
		final var replacements = new ArrayList<C64Char[]>();
		reduceToNcharacters(characters, replacements);
		// TODO: reduce replacements: a->b->c = a->c
		return replacements;
	}

	private void reduceToNcharacters(final HashBag<C64Char> bigBag, final List<C64Char[]> replacements) {
		final HashBag<C64Char> smallBag = HashBag.of(bigBag.values().toList());

		final Set<C64Char> victimBlacklist = new HashSet<>(); // will be reduced later
		List<Map.Entry<PreProcessedCharacterImage, C64Char>> possibleSurrogates = null;
		List<VictimToSurrogateMapping> priorityQueue = null;

		do {
			final int queueSize = bigBag.sizeDistinct();// Math.min(bigBag.sizeDistinct(), 256);
			VictimToSurrogateMapping bestSurrogateMapping = null;
			do {
				if (priorityQueue == null || priorityQueue.isEmpty()) {
					possibleSurrogates = createPossibleSurrogates(smallBag.distinctValues());
					System.out.println("PossibleSurrogates: " + possibleSurrogates.size());
					priorityQueue = victimToSurrogateMappings(smallBag, possibleSurrogates, victimBlacklist, queueSize);
					System.out.println("new priorityQueue, length: " + priorityQueue.size());
					if (priorityQueue.isEmpty()) { // e.g. black list contains every character
						victimBlacklist.clear(); // drink the cool aid as last resort
						priorityQueue = victimToSurrogateMappings(smallBag, possibleSurrogates, victimBlacklist, queueSize);
						System.out.println("new priorityQueue, length: " + priorityQueue.size());
					}
				}
				bestSurrogateMapping = priorityQueue.remove(priorityQueue.size() - 1);
			} while (victimBlacklist.contains(bestSurrogateMapping.victim) // won't replace blacklisted
					|| !smallBag.contains(bestSurrogateMapping.victim)// can't replace something that is already gone
					|| !smallBag.contains(bestSurrogateMapping.surrogate)// can't replace with something that is already gone
			);
			replaceCharacter(smallBag, replacements, bestSurrogateMapping);
			victimBlacklist.add(bestSurrogateMapping.surrogate); // exclude surrogate from the possible list of victims
			// System.out.println(smallBag.sizeDistinct());
		} while (smallBag.sizeDistinct() > maxCharacters);
		System.out.println("replacements: " + replacements.size());
	}

	private List<Entry<PreProcessedCharacterImage, C64Char>> createPossibleSurrogates(final Stream<C64Char> characters) {
		final Function<C64Char, PreProcessedCharacterImage> chrToImage = createC64CharToImage();
		return characters.parallel()//
				.map(character -> new SimpleEntry<>(chrToImage.apply(character), character))//
				.collect(toList());
	}

	private List<VictimToSurrogateMapping> victimToSurrogateMappings(final HashBag<C64Char> bag,
			final List<Map.Entry<PreProcessedCharacterImage, C64Char>> possibleSurrogates, final Set<C64Char> victimBlacklist, final int limit) {
		final long start = System.nanoTime();
		try {
			final Function<C64Char, PreProcessedCharacterImage> c64CharToImage = createC64CharToImage();

			final List<VictimToSurrogateMapping> queue = bag.occurrences()//
					.filter(occurrence -> !victimBlacklist.contains(occurrence.objValue))//
					.sorted(comparingLong(CountableObject::count))// least frequent first
					.limit(limit)//
					.parallel()//
					.map(CountableObject::objValue)//
					.map(possibleVictim -> surrogateMappingsForSingleVictim(possibleVictim, possibleSurrogates, c64CharToImage))//
					.flatMap(singleMappings -> singleMappings.stream())// stream of possible possibleVictimToSurrogateMappings (weighted)
					.sorted(comparingDouble(VictimToSurrogateMapping::weight) //
							.thenComparing(vso -> bag.occurrencesOf(vso.victim))// least frequentVictims go first, if two are equally good
							.thenComparing(VictimToSurrogateMapping::sortStabilizer))// utilizes stable hash codes
					.collect(toList());
			Collections.reverse(queue); // slow, but a reverse comparator gives strange results and we want to poll from the back of the list for O(1) polling
			return queue;
		} finally {
			final long end = System.nanoTime();
			System.out.println("victimToSurrogateMappings, time: " + TimeUnit.NANOSECONDS.toMillis(end - start));
		}
	}

	private List<VictimToSurrogateMapping> surrogateMappingsForSingleVictim(final C64Char possibleVictim,
			final List<Map.Entry<PreProcessedCharacterImage, C64Char>> possibleSurrogates, final Function<C64Char, PreProcessedCharacterImage> c64CharToImage) {
		// plain bitmaps, HirRes or Multicolor, are uniformly colored so a given char always has the same image, regardles of position
		final PreProcessedCharacterImage victimImage = c64CharToImage.apply(possibleVictim);
		return possibleSurrogates.stream()//
				.filter(possibleSurrogate -> !possibleVictim.equals(possibleSurrogate.getValue()))//
				.map(surrogate -> {
					final C64Char surrogateChar = surrogate.getValue();
					PreProcessedCharacterImage surrogateImage = surrogate.getKey();
					final double weight = UtilsImage.error(victimImage, surrogateImage, distanceMeasure);
					surrogateImage = null;
					return new VictimToSurrogateMapping(possibleVictim, surrogateChar, surrogateImage, weight);
				})
				// .sorted(Comparator.comparingDouble(VictimToSurrogateMapping::weight) //
				// .thenComparing(VictimToSurrogateMapping::sortStabilizer))// utilizes stable hash codes
				// .limit(32)//
				.collect(toList()); // important: do no leave a stream here and flatMap later -> speed drops dramatically!
	}

	private static void replaceCharacter(final HashBag<C64Char> bag, final List<C64Char[]> replacements, final VictimToSurrogateMapping replacement) {
		// System.out.println("surrogateQ: " + victimToSurrogateMappings.size());
		// replace victim by surrogate
		// System.out.println("replacing character by surrogates");
		final C64Char victim = replacement.victim;
		// System.out.println("victim: \n" + victim);
		final C64Char surrogate = replacement.surrogate;
		if (!victim.equals(surrogate)) { // ..just in case some stupid bug..
			replacements.add(new C64Char[] { victim, surrogate });
			// System.out.println("surrogate: \n" + surrogate);
			final int victimCount = bag.occurrencesOf(victim);
			bag.add(CountableObject.ofValue(surrogate).withCount(victimCount));
			bag.remove(CountableObject.ofValue(victim).withCount(victimCount));
		}
	}

}
