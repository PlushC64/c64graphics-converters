package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class PreProcessedMulticolorImage extends BufferedImage {

	private final Color background;

	public PreProcessedMulticolorImage(final int width, final int height, final int imageType, final Color background) {
		super(width, height, imageType);
		this.background = background;
	}

	public Color background() {
		return background;
	}

}