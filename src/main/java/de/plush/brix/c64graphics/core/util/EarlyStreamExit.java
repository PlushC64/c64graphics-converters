package de.plush.brix.c64graphics.core.util;

public class EarlyStreamExit extends RuntimeException {
	private final Object result;

	public EarlyStreamExit(final Object result) {
		this.result = result;
	}

	@SuppressWarnings("unchecked")
	public <T> T result() {
		return (T) result;
	}
}