package de.plush.brix.c64graphics.core.model;

import java.net.*;

import de.plush.brix.c64graphics.core.util.*;

/**
 * Standard hires bitmap + 2 screen rams<br>
 *
 * @author Wanja Gayk
 */
public class C64BitmapmodePictureHiresHalfcharFli extends C64BitmapmodePictureHiresNscreenFLI {

	public static final int SCREEN_COUNT = 2;

	public C64BitmapmodePictureHiresHalfcharFli(final C64Bitmap bitmap, final C64Screen[] screens) {
		super(SCREEN_COUNT, bitmap, screens);
	}

	public C64BitmapmodePictureHiresHalfcharFli(final C64BitmapmodePictureHiresHalfcharFli other) {
		this(other.bitmap, other.screens);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!super.equals(obj)) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		return true;
	}

	/**
	 * Hires Bitmap, 2 x Screen RAM<br>
	 * <style> #gfxformat th { background:lightgray; text-align: left; width:auto}</style> <style> #gfxformat td.first { width:18ch; }</style><style> #gfxformat
	 * td { width:auto}</style>
	 * <table id="gfxformat">
	 * <th colspan="2">Halfchar AFLI (by Skate/Plush)</th>
	 * <tr>
	 * <td class="first">$4000 - $67fff</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$4000 - $5f3f</td>
	 * <td>Bitmap</td>
	 * <tr>
	 * <td class="first">$6000 - $67ff</td>
	 * <td>Screen RAMs</td>
	 * </table>
	 *
	 * @return A provider that will provide the C64 binary of this image at the time this method was invoked.
	 */
	public C64BinaryProvider binaryProviderPlush() {
		return plushFormat().binaryProvider(this);
	}

	public static C64BitmapmodePictureHiresHalfcharFli loadHiresFLICrest(final URL loc, final StartAddress startAddress) {
		return plushFormat().load(Utils.readFile(loc, startAddress));
	}

	public static C64BitmapmodePictureHiresHalfcharFli loadHiresFLICrest(final URI loc, final StartAddress startAddress) {
		return plushFormat().load(Utils.readFile(loc, startAddress));
	}

	private static Format plushFormat() {
		return (Format) new Format()//
				.withLoadAddress(0x4000, 0x67ff)//
				.withBitmapAddress(0x4000)//
				.withScreensAddress(0x6000);
	}

	private static class Format extends C64HiresFliFormat<C64BitmapmodePictureHiresHalfcharFli> {
		C64BitmapmodePictureHiresHalfcharFli load(final byte[] binary) {
			return super.load(binary, SCREEN_COUNT, C64BitmapmodePictureHiresHalfcharFli::new);
		}
	}
}
