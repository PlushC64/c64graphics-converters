package de.plush.brix.c64graphics.core.pipeline.stages.dithering;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.OptionalDouble;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;

public interface DitheringSpec extends Cloneable {

	/** marks the center point in the matrix. */
	int X = Integer.MAX_VALUE;

	/**
	 * @param globalPosition
	 *            allows switching center points based on global position (even/odd or similar).
	 * @return the center point of the kernel
	 */
	Point centerPoint(final Point globalPosition);

	/**
	 * Gets the position in the kernel matrix, relative to the kernel's center point.
	 *
	 * @param matrixPosition
	 *            the absolute position in the kernel.
	 * @param globalPosition
	 *            allows switching center points based on global position (even/odd or similar).
	 * @return the position inside the kernel, relative to its center point (being left from center creates a negative x-position)
	 */
	Point centerPointOffset(final Point matrixPosition, final Point globalPosition);

	OptionalDouble errorMultiplier(final Point matrixPosition, final Point globalPosition);

	Dimension matrixSize();

	Function<BufferedImage, BufferedImage> algorithm(Palette palette, ColorDistanceMeasure colorDistanceMeasure);

}
