package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import java.awt.Color;

/**
 * Weighted color distance based on a weighted Euclidean metric to take color perception into account. <br>
 * Measures color distance according to this paper:<br>
 * <A href="https://www.compuphase.com/cmetric.htm">https://www.compuphase.com/cmetric.htm</a><br>
 * <p>
 * The proposed algorithm (used by our products EGI, AniSprite and PaletteMaker) is a combination both weighted Euclidean distance functions, where the weight
 * factors depend on how big the "red" component of the colour is. First one calculates the mean level of "red" and then weights the ΔR and ΔB signals as a
 * function of the mean red level. [..] This formula has results that are very close to L*u*v* (with the modified lightness curve) and, more importantly, it is
 * a more stable algorithm: it does not have a range of colours where it suddenly gives far from optimal results. The weights of the formula could be optimized
 * further, but again, the selection of the closest colour is subjective.
 * </p>
 */
class ColorDistanceEuclideanWeighted implements ColorDistanceMeasure {
	@Override
	public double distanceOf(final Color c1, final Color c2) {
		return c1.equals(c2) ? 0.0 : Math.min(1, euclideanWeighted(c1, c2) / 764.83);
	}

	private static double euclideanWeighted(final Color c1, final Color c2) {
		final int red1 = c1.getRed();
		final int red2 = c2.getRed();
		final int rmean = (red1 + red2) >> 1;
		final int r = red1 - red2;
		final int g = c1.getGreen() - c2.getGreen();
		final int b = c1.getBlue() - c2.getBlue();
		return Math.sqrt((((512 + rmean) * r * r) >> 8) + 4 * g * g + (((767 - rmean) * b * b) >> 8));
	}
}
