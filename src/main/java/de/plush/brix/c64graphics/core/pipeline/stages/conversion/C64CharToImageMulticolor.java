package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.pipeline.stages.conversion.C64CharToImageMulticolor.Mode.Char;
import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.model.C64Char;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.Palette;

public class C64CharToImageMulticolor implements IPipelineStage<C64Char, PreProcessedCharacterImage> {

	public enum Mode {
		Char, Bitmap;
	}

	private final Palette palette;

	private final Supplier<Color> backgroundColorProvider;

	// private final Color colorRamColor;

	private final Color mc1_D022;

	private final Color mc2_D023;

	private final Mode mode;

	private final byte cramColorCode;

	private final byte mc1_D022ColorCode;

	private final byte mc2_D023ColorCode;

	/**
	 * <style> ulx > ul { margin-top: 0px; margin-bottom: 0px;} </style>
	 *
	 * @param backgroundColorProvider
	 *            color for 00 bit pairs
	 * @param mc1_D022
	 *            color for 01 bit pairs, all C64 palette colors feasible
	 * @param mc2_D023
	 *            color for 11 bit pairs, all C64 palette colors feasible
	 * @param colorRamColor
	 *            color for 11 bit pairs<br>
	 *            <ulx>
	 *            <li>In {@link Mode#Bitmap Char mode} only be the colors for color codes $00 to $07 (included) are displayed and bit 3 of the color code
	 *            toggles between multicolor and hires (i.e: $07/yellow stays yellow in a hires resolution character, $0F/light gray becomes yellow in a
	 *            multicolor resolution character).<br>
	 *            <li>In {@link Mode#Bitmap Bitmap mode} all palette colors are feasible.</ulx>
	 */
	public C64CharToImageMulticolor(final Palette palette, final Supplier<Color> backgroundColorProvider, final Color mc1_D022, final Color mc2_D023,
			final Color colorRamColor, final Mode mode) {
		this.palette = palette;
		this.backgroundColorProvider = backgroundColorProvider;
		this.mc1_D022 = mc1_D022;
		this.mc2_D023 = mc2_D023;
		this.mode = mode;

		cramColorCode = palette.colorCodeOrException(colorRamColor);
		mc1_D022ColorCode = palette.colorCodeOrException(mc1_D022);
		mc2_D023ColorCode = palette.colorCodeOrException(mc2_D023);

	}

	@Override
	public PreProcessedCharacterImage apply(final C64Char character) {
		// do not get color in constructor, as provider may need to convert an image first (which is after pipeline construction)
		final Color colorRamColor = mode == Char ? palette.visibleColorInMulticolorCharmode(cramColorCode)
				: palette.color(toUnsignedByte(cramColorCode & 0x0F));
		if (mode == Char && !palette.isMulticolorBitSetInD800colorCode(cramColorCode)) {
			return new C64CharToImageHires(palette, backgroundColorProvider, mc1_D022, mc2_D023, colorRamColor).apply(character);
		}
		final Color backgroundColor = backgroundColorProvider.get();
		final byte backgroundColorCode = palette.colorCodeOrException(backgroundColorProvider.get());

		final PreProcessedCharacterImage result = new PreProcessedCharacterImage(C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS, BufferedImage.TYPE_INT_RGB, //
				palette, backgroundColorCode, mc1_D022ColorCode, mc2_D023ColorCode, cramColorCode); // use original color code (correct d800 value)
		int y = -1;
		for (byte b : character) {
			++y;
			for (int x = 0; x < C64Char.WIDTH_PIXELS; x += 2) {
				final boolean d022 = (b & 0xC0) == 0b01 << 6; // $40
				final boolean d023 = (b & 0xC0) == 0b10 << 6; // $80
				final boolean cram = (b & 0xC0) == 0b11 << 6; // $C0

				final Color color = cram
						? colorRamColor //@formatter:off
											: d022 ? mc1_D022//
											: d023 ? mc2_D023//
											: backgroundColor;//@formatter:on

				result.setRGB(x, y, color.getRGB());
				result.setRGB(x + 1, y, color.getRGB());
				b <<= 2;
			}
		}
		return result;
	}

}
