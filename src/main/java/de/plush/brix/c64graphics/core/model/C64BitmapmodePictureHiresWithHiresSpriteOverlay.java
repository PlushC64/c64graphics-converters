package de.plush.brix.c64graphics.core.model;

import static java.lang.System.arraycopy;

import java.awt.*;

import de.plush.brix.c64graphics.core.util.Utils;

/**
 * Standard hires bitmap + screen ram, as used by Hi Eddi, Doodle, Art Studio, Interpaint (Hires), ImageSystem (Hires), etc. plus a Sprite Layer.<br/>
 * Currently the sprite[y][x] matrix describes a continuous plane with its upper left corner given in block-coordinates. Negative block coordinates are allowed,
 * if the sprites should extend into the borders.
 * 
 * @author Wanja Gayk
 */
public class C64BitmapmodePictureHiresWithHiresSpriteOverlay implements IRollBlockwise {

	private final C64BitmapmodePictureHires c64BitmapmodePictureHires;

	private final C64SpriteGrid spriteOverlay;

	private final byte spriteOverlayColorCode;

	private final Point spriteOverlayLocationAtBlock;

	public C64BitmapmodePictureHiresWithHiresSpriteOverlay(final C64BitmapmodePictureHires c64BitmapmodePictureHires, final C64SpriteGrid spriteOverlay,
			final Point spriteOverlayLocationAtBlock) {
		this(c64BitmapmodePictureHires, spriteOverlay, (byte) 0, spriteOverlayLocationAtBlock);
	}

	public C64BitmapmodePictureHiresWithHiresSpriteOverlay(final C64BitmapmodePictureHires c64BitmapmodePictureHires, final C64SpriteGrid spriteOverlay,
			final byte spriteOverlayColorCode, final Point spriteOverlayLocationAtBlock) {
		this.c64BitmapmodePictureHires = new C64BitmapmodePictureHires(c64BitmapmodePictureHires);
		this.spriteOverlay = new C64SpriteGrid(spriteOverlay);
		this.spriteOverlayColorCode = spriteOverlayColorCode;
		this.spriteOverlayLocationAtBlock = new Point(spriteOverlayLocationAtBlock);
	}

	public C64BitmapmodePictureHiresWithHiresSpriteOverlay(final C64BitmapmodePictureHiresWithHiresSpriteOverlay other) {
		c64BitmapmodePictureHires = other.c64BitmapmodePictureHires();
		spriteOverlay = other.spriteOverlay();
		spriteOverlayColorCode = other.spriteOverlayColorCode();
		spriteOverlayLocationAtBlock = other.spriteOverlayLocationAtBlock();
	}

	public Dimension sizeBlocks() {
		return C64Bitmap.sizeBlocks();
	}

	public Dimension sizePixels() {
		return C64Bitmap.sizePixels();
	}

	public C64BitmapmodePictureHires c64BitmapmodePictureHires() {
		return new C64BitmapmodePictureHires(c64BitmapmodePictureHires);
	}

	public C64SpriteGrid spriteOverlay() {
		return new C64SpriteGrid(spriteOverlay);
	}

	public byte spriteOverlayColorCode() {
		return spriteOverlayColorCode;
	}

	/**
	 * @return a snapshot of the top left location of the sprite overlay in block-coordinates. Negative coordinates are allowed, if the sprites should extend
	 *         into the borders.
	 */
	public Point spriteOverlayLocationAtBlock() {
		return new Point(spriteOverlayLocationAtBlock);
	}

	/**
	 * @return a snapshot of the current bitmap
	 */
	public C64Bitmap bitmap() {
		return c64BitmapmodePictureHires.bitmap();
	}

	/**
	 * @return a snapshot of the current screen
	 */
	public C64Screen screen() {
		return c64BitmapmodePictureHires.screen();
	}

	/**
	 * @return a snapshot of the current screenCode matrix
	 */
	public byte[][] screenCodeMatrix() {
		return c64BitmapmodePictureHires.screenCodeMatrix();
	}

	public void setSpriteOverlayLocationToBlock(final Point p) {
		spriteOverlayLocationAtBlock.setLocation(p);
	}

	public void setSpriteOverlayLocationToBlock(final int x, final int y) {
		spriteOverlayLocationAtBlock.setLocation(x, y);
	}

	public void translateSpriteOverlayLocationByBlocks(final int dx, final int dy) {
		spriteOverlayLocationAtBlock.translate(dx, dy);
	}

	public void setBitmapBlock(final int xBlock, final int yBlock, final C64Char chrBlock) {
		c64BitmapmodePictureHires.setBitmapBlock(xBlock, yBlock, chrBlock);
	}

	public C64Char bitmapBlockAt(final int xBlock, final int yBlock) {
		return c64BitmapmodePictureHires.bitmapBlockAt(xBlock, yBlock);
	}

	public void setScreenCode(final int x, final int y, final byte screenCode) {
		c64BitmapmodePictureHires.setScreenCode(x, y, screenCode);
	}

	public byte screenCodeAt(final int x, final int y) {
		return c64BitmapmodePictureHires.screenCodeAt(x, y);
	}

	public Dimension spriteOverlaySizeSprites() {
		return spriteOverlay.sizeSprites();
	}

	public Dimension spriteOverlaySizePixels() {
		return spriteOverlay.sizePixels();
	}

	public C64Sprite spriteAtSpriteGridPos(final Point p) {
		return spriteOverlay.spriteAt(p);
	}

	public C64Sprite spriteAtSpriteGridPos(final int x, final int y) {
		return spriteOverlay.spriteAt(x, y);
	}

	// TODO: we probably need two methods: one relative to the SpriteLayer origin, one relative to the bitmap origin.
	public C64Sprite spriteAtPixelRelativeToSpriteLayerOrigin(final int xPix, final int yPix) {
		return spriteOverlay.spriteAtPixel(xPix, yPix);
	}

	public void setSpriteToSproteGridPos(final Point gridPos, final C64Sprite sprite) {
		spriteOverlay.setSprite(gridPos, sprite);
	}

	public void setSpriteToSProteGridPos(final int x, final int y, final C64Sprite sprite) {
		spriteOverlay.setSprite(x, y, sprite);
	}

	/**
	 * <b><u>Important</u></b>: The position of the sprite layer is just translated along the rolling direction, not actually "rolled". The sprite layer itself
	 * is not modified.
	 */
	@Override
	public void rollLeftBlockwise(final int n) {
		rollBitmapPictureLeftBlockwise(n);
		translateSpriteOverlayLocationByBlocks(-n, 0);
	}

	/**
	 * <b><u>Important</u></b>: The position of the sprite layer is just translated along the rolling direction, not actually "rolled". The sprite layer itself
	 * is not modified.
	 */
	@Override
	public void rollRightBlockwise(final int n) {
		rollBitmapPictureRightBlockwise(n);
		translateSpriteOverlayLocationByBlocks(n, 0);
	}

	/**
	 * <b><u>Important</u></b>: The position of the sprite layer is just translated along the rolling direction, not actually "rolled". The sprite layer itself
	 * is not modified.
	 */
	@Override
	public void rollUpBlockwise(final int n) {
		rollBitmapPictureUpBlockwise(n);
		translateSpriteOverlayLocationByBlocks(0, -n);
	}

	/**
	 * <b><u>Important</u></b>: The position of the sprite layer is just translated along the rolling direction, not actually "rolled". The sprite layer itself
	 * is not modified.
	 */
	@Override
	public void rollDownBlockwise(final int n) {
		rollBitmapPictureDownBlockwise(n);
		translateSpriteOverlayLocationByBlocks(0, n);
	}

	public void rollBitmapPictureBlockwise(final Displacement displacement) {
		c64BitmapmodePictureHires.rollBlockwise(displacement);
	}

	public void rollBitmapPictureLeftBlockwise(final int n) {
		c64BitmapmodePictureHires.rollLeftBlockwise(n);
	}

	public void rollBitmapPictureRightBlockwise(final int n) {
		c64BitmapmodePictureHires.rollRightBlockwise(n);
	}

	public void rollBitmapPictureUpBlockwise(final int n) {
		c64BitmapmodePictureHires.rollUpBlockwise(n);
	}

	public void rollBitmapPictureDownBlockwise(final int n) {
		c64BitmapmodePictureHires.rollDownBlockwise(n);
	}

	/**
	 * ImageSystem/Hi-Eddi followed by sprite data. <br/>
	 * Both Image System and HI-Eddi are the same format, only with different start addresses. Since this method does not include start addresses, just the raw
	 * binary, the data is the same for both formats.
	 * 
	 * <style> #gfxformat th { background:lightgray; text-align: left; width:auto}</style> <style> #gfxformat td.first { width:18ch; }</style><style> #gfxformat
	 * td { width:auto}</style>
	 * <table id="gfxformat">
	 * <th colspan="2">Hi-Eddi (by Markt &amp; Technik) (pc-ext: .hed) + sprite data</th>
	 * <tr>
	 * <td class="first">$2000 - $43ff</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$2000 - $3f3f</td>
	 * <td>Bitmap</td>
	 * <tr>
	 * <td class="first">$4000 - $43e7</td>
	 * <td>Screen RAM</td>
	 * <tr>
	 * <td class="first">$4400 - eof</td>
	 * <td>Sprite data, $40 bytes each.</td>
	 * </table>
	 *
	 * <table id="gfxformat">
	 * <th colspan="2">Image System (Hires) (pc-ext: .ish) + sprite data</th>
	 * <tr>
	 * <td class="first">$4000 - $63e7</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$4000 - $5f3f</td>
	 * <td>Bitmap</td>
	 * <tr>
	 * <td class="first">$6000 - $63e7</td>
	 * <td>Screen RAM</td>
	 * <tr>
	 * <td class="first">$6400 - eof</td>
	 * <td>Sprite data, $40 bytes each.</td>
	 * </table>
	 * </table>
	 * <table id="gfxformat">
	 */
	public C64BinaryProvider binaryProviderHiEddiFollowedBySpriteData() {
		final byte[] paddedBinary = new byte[0x2400];
		final byte[] bitmapAndScreen = c64BitmapmodePictureHires.binaryProviderHiEddi().toC64Binary();
		arraycopy(bitmapAndScreen, 0, paddedBinary, 0, bitmapAndScreen.length);
		final byte[] spriteBinary = spriteOverlay.toC64Binary();
		final byte[] binary = Utils.toByteArray(paddedBinary, spriteBinary);
		return () -> binary;
	}

	/** Redirects to {@link #binaryProviderHiEddiFollowedBySpriteData()}. */
	public C64BinaryProvider binaryProviderImageSystemFollowedBySpriteData() {
		return binaryProviderHiEddiFollowedBySpriteData();
	}
}
