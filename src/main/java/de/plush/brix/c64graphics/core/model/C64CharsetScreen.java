package de.plush.brix.c64graphics.core.model;

import static de.plush.brix.c64graphics.core.util.Utils.*;
import static java.lang.Integer.toHexString;
import static java.lang.System.arraycopy;

import java.awt.*;
import java.util.*;

import de.plush.brix.c64graphics.core.util.*;

public class C64CharsetScreen implements IRollBlockwise {

	public static final int WIDTH_BLOCKS = C64Screen.WIDTH_BLOCKS;

	public static final int HEIGHT_BLOCKS = C64Screen.HEIGHT_BLOCKS;

	private final C64Charset charset;

	protected final C64Char[][] characterMatrix;

	private final C64Screen screen;

	public C64CharsetScreen(final C64CharsetScreen other) throws TooManyCharactersInCharset {
		charset = other.charset();
		characterMatrix = other.characterMatrix();
		screen = other.screen();
	}

	public C64CharsetScreen(final C64Char defaultBlock) throws TooManyCharactersInCharset {
		this(new C64Charset(), defaultBlock);
	}

	public C64CharsetScreen(final C64Charset charset, final C64Char defaultBlock) throws TooManyCharactersInCharset {
		this(charset, new C64Char[HEIGHT_BLOCKS][WIDTH_BLOCKS]);
		charset.addCharacter(defaultBlock);
		for (final C64Char[] line : characterMatrix) {
			Arrays.fill(line, defaultBlock);
		}
		// TODO: Edge case: default block is not in charset
	}

	public C64CharsetScreen(final C64Charset charset, final C64Char[][] characterMatrix) {
		this.charset = new C64Charset(charset);
		this.characterMatrix = deepCopy(characterMatrix);
		screen = createScreen(this.charset, this.characterMatrix);
	}

	public C64CharsetScreen(final C64Charset charset, final C64Screen screen) {
		this.charset = new C64Charset(charset);
		this.screen = new C64Screen(screen);
		characterMatrix = new C64Char[C64Screen.HEIGHT_BLOCKS][C64Screen.WIDTH_BLOCKS];
		for (int y = 0; y < C64Screen.HEIGHT_BLOCKS; ++y) {
			for (int x = 0; x < C64Screen.WIDTH_BLOCKS; ++x) {
				final byte screenCode = screen.screenCodeAt(x, y);
				final int _x = x;
				final int _y = y;
				final Optional<C64Char> optionalScreenCode = charset.characterBlockOf(screenCode);
				characterMatrix[y][x] = optionalScreenCode.orElseThrow(() -> new IllegalArgumentException("Screen code $" + toHexString(toPositiveInt(
						screenCode)) + " at " + new Point(_x, _y) + " has no character matrix in charset."));
			}
		}
	}

	protected static C64Screen createScreen(final C64Charset charset, final C64Char[][] characterMatrix) {
		final C64Screen screen = new C64Screen((byte) 0);
		for (int y = 0; y < characterMatrix.length; ++y) {
			final C64Char[] line = characterMatrix[y];
			for (int x = 0; x < line.length; ++x) {
				final C64Char chr = line[x];
				final int _x = x;
				final int _y = y;
				if (chr != null) {
					final OptionalByte optionalScreenCode = charset.screenCodeOf(chr);
					screen.setScreenCode(x, y, optionalScreenCode.orElseThrow(() -> new IllegalArgumentException("Character Block at " + new Point(_x, _y)
							+ " is not in the given charset (could not find screen code). Character: \n" + chr.toString())));
				}
			}
		}
		return screen;
	}

	public static Dimension sizePixels() {
		final Dimension dim = C64Screen.sizeBlocks();
		dim.height *= C64Char.HEIGHT_PIXELS;
		dim.width *= C64Char.WIDTH_PIXELS;
		return dim;
	}

	public static Dimension sizeBlocks() {
		return C64Screen.sizeBlocks();
	}

	/** will do nothing, if the character to be replaced is not present */
	public void replaceCharacter(final C64Char old, final C64Char replacement) {
		charset.screenCodeOf(old)//
				.ifPresent(screenCode -> charset.putCharacter(screenCode, replacement));
	}

	public void setBlock(final int x, final int y, final C64Char character) throws TooManyCharactersInCharset {
		charset.addCharacter(character);
		characterMatrix[y][x] = character;
		screen.setScreenCode(x, y, charset.screenCodeOf(character).getAsByte());
	}

	public C64Char blockAt(final Point p) {
		return characterMatrix[p.y][p.x];
	}

	public C64Char blockAt(final int x, final int y) {
		return characterMatrix[y][x];
	}

	public byte screenCodeAt(final int x, final int y) {
		return screen.screenCodeAt(x, y);
	}

	public void setScreenCode(final int x, final int y, final byte code) {
		characterMatrix[y][x] = charset.characterBlockOf(code).orElseThrow(() -> new IllegalArgumentException("screen code " + toPositiveInt(code)
				+ " has no corresponding character in charset (" + charset.size() + " chars) "));
		screen.setScreenCode(x, y, code);
	}

	/**
	 * Will switch the screencodes on the screen and manipulate the character set accordingly. <br>
	 * Will do nothing, if either of the provided screen codes has no character in the charset.
	 */
	public void switchScreenCodes(final byte a, final byte b) {
		final var chrA = charset.characterBlockOf(a);
		final var chrB = charset.characterBlockOf(b);
		if (chrA.isPresent() && chrB.isPresent()) {
			final ObjByteToByteFunction<Point> exchangeAandB = (pos, code) -> code == a ? b : code == b ? a : code;
			charset.putCharacter(a, chrB.get());
			charset.putCharacter(b, chrA.get());
			screen.modifyEveryScreenCode(exchangeAandB);
		}
	}

	public Iterable<C64Char> blockIterable() {
		return new Iterable<>() {

			@Override
			public Iterator<C64Char> iterator() {
				return new Iterator<>() {

					int x = -1;

					int y;

					@Override
					public boolean hasNext() {
						return y * WIDTH_BLOCKS + x + 1 < WIDTH_BLOCKS * HEIGHT_BLOCKS;
					}

					@Override
					public C64Char next() {
						if (++x == WIDTH_BLOCKS) {
							x = 0;
							++y;
						}
						return blockAt(x, y);
					}

					@Override
					public void remove() {
						throw new UnsupportedOperationException("cannot remove Character from CharScreenImage");
					}
				};
			}
		};

	}

	public C64Charset charset() {
		return new C64Charset(charset);
	}

	public C64Screen screen() {
		return new C64Screen(screen);
	}

	public C64Char[][] characterMatrix() {
		return Utils.deepCopy(characterMatrix);
	}

	public boolean isCharsetFull() {
		return charset.isFull();
	}

	public int charsetSize() {
		return charset.size();
	}

	public boolean contains(final C64Char chr) {
		return charset.contains(chr);
	}

	@Override
	public void rollLeftBlockwise(final int n) {
		for (final C64Char[] line : characterMatrix) {
			Utils.rollLeft(line, n);
		}
		screen.rollLeftBlockwise(n);
	}

	@Override
	public void rollRightBlockwise(final int n) {
		for (final C64Char[] line : characterMatrix) {
			Utils.rollRight(line, n);
		}
		screen.rollRightBlockwise(n);
	}

	@Override
	public void rollUpBlockwise(final int n) {
		Utils.rollLeft(characterMatrix, n); // rolling "left" the up-to-down array of lines means "rolling up" the lines
		screen.rollUpBlockwise(n);
	}

	@Override
	public void rollDownBlockwise(final int n) {
		Utils.rollRight(characterMatrix, n); // rolling "right" the up-to-down array of lines means "rolling down" the lines
		screen.rollDownBlockwise(n);
	}

	public byte[] screenBinary() {
		return screen.toC64Binary();
	}

	public byte[] charsetBinary() {
		return charset.toC64Binary();
	}

	public C64BinaryProvider binaryProviderCharsetFirst() {
		final byte[] binary = new byte[0x0800 + 0x0400];
		final byte[] charsetBinary = charset.toC64Binary();
		final byte[] screenBinary = screen.toC64Binary();
		arraycopy(charsetBinary, 0, binary, 0, charsetBinary.length);
		arraycopy(screenBinary, 0, binary, 0x800, screenBinary.length);
		return () -> binary;
	}

	public static C64CharsetScreen fromBinaryCharsetFirst(final C64BinaryProvider binaryProvider) {
		return fromBinaryCharsetFirst(binaryProvider.toC64Binary());
	}

	public static C64CharsetScreen fromBinaryCharsetFirst(final byte[] binary) {
		final byte[] charsetBinary = new byte[C64Charset.MAX_CHARACTERS * C64Char.HEIGHT_PIXELS];
		final byte[] screenBinary = new byte[C64Screen.WIDTH_BLOCKS * C64Screen.HEIGHT_BLOCKS];
		System.arraycopy(binary, 0, charsetBinary, 0, charsetBinary.length);
		System.arraycopy(binary, 0x800, screenBinary, 0, screenBinary.length);
		final C64Charset charset = C64Charset.fromBinary(charsetBinary);
		final C64Screen c64Screen = C64Screen.fromBinary(screenBinary);
		return new C64CharsetScreen(charset, c64Screen);
	}

}
