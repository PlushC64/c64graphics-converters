package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import static java.util.stream.Collectors.toUnmodifiableSet;

import java.util.*;

import de.plush.brix.c64graphics.core.util.collections.ByteSet;

/**
 * Color combinations based on the color clusters table by <a href="https://ilesj.wordpress.com">Ilkka Sjöstedt</a><br>
 * <img src="doc-files/c64-luminance-comparison.png"></img><br>
 * The {@link #VIC_II_lenient} enum is based on a 5 cluster model that sits in between the VIC and VIC II luminance cluster model.
 * It's more appropriate for hires graphics.
 * 
 * @see <a href ="https://ilesj.wordpress.com/2016/03/30/old-vic-ii-colors-and-color-blending/">Old VIC-II Colors and Color Blending</a>
 * @see <a href ="https://kodiak64.com/blog/luma-driven-graphics-on-c64">Luma-Driven Graphics on the C64</a>
 */
public enum ColorClusterIndexes implements ColorClusterIndexProvider {
	/** The old VIC chip has 3 luminance clusters. */
	VIC(//
			ByteSet.ofUnsigned(0x2, 0x6, 0x9, 0xB), // RED. BLUE, BROWN, DARK GRAY
			ByteSet.ofUnsigned(0x4, 0x5, 0x8, 0xA, 0xC, 0xE), // PURPLE, GREEN, ORANGE, LIGHT_RED, GRAY, LIGHT_BLUE
			ByteSet.ofUnsigned(0x3, 0x7, 0xD, 0xF)), // CYAN, YELLOW, LIGHT_GREEN, LIGHT GRAY
	/**
	 * This has 5 luminance clusters, it's kind of a mix between the VIC and VIC II luminance levels.
	 * It won't work so well for interlace, but might still be approproiate for hires Graphics.<br>
	 */
	VIC_II_lenient(//
			ByteSet.ofUnsigned(0x2, 0x6, 0x9, 0xB), // RED. BLUE, BROWN, DARK GRAY
			ByteSet.ofUnsigned(0x4, 0x8), // PURPLE, ORANGE
			ByteSet.ofUnsigned(0x5, 0xA, 0xC, 0xE), // GREEN, LIGHT_RED, GRAY, LIGHT_BLUE,
			ByteSet.ofUnsigned(0x3, 0xF), // CYAN, LIGHT_GRAY
			ByteSet.ofUnsigned(0x7, 0xD)), // YELLOW, LIGHT_GREEN
	/** The VIC II chip has 7 luminance clusters. */
	VIC_II(//
			ByteSet.ofUnsigned(0x6, 0x9), // BLUE, BROWN
			ByteSet.ofUnsigned(0x2, 0xB), // RED, DARK_GRAY
			ByteSet.ofUnsigned(0x4, 0x8), // PURPLE, ORANGE
			ByteSet.ofUnsigned(0xC, 0xE), // GRAY, LIGHT_BLUE
			ByteSet.ofUnsigned(0x5, 0xA), // GREEN, LIGHT_RED
			ByteSet.ofUnsigned(0x3, 0xF), // CYAN, LIGHT_GRAY
			ByteSet.ofUnsigned(0x7, 0xD)), // YELLOW. LIGHT_GREEN
	;

	private ByteSet[] clusters;

	ColorClusterIndexes(final ByteSet... clusters) {
		this.clusters = clusters;
	}

	@Override
	public Set<ByteSet> colorIndexClusters() {
		return Arrays.stream(clusters).map(ByteSet::new).collect(toUnmodifiableSet());
	}

}
