package de.plush.brix.c64graphics.core.util.functions;

@FunctionalInterface
public interface ByteProcedure {
	void value(byte each);
}
