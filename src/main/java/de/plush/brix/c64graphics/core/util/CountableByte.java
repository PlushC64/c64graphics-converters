package de.plush.brix.c64graphics.core.util;

import java.util.Comparator;
import java.util.function.LongUnaryOperator;

public final class CountableByte implements Comparable<CountableByte> {

	private static final Comparator<CountableByte> NATURAL_ORDER = Comparator.<CountableByte> //
			comparingLong(CountableByte::count)//
			.thenComparingInt(CountableByte::byteValue);

	public final byte byteValue;

	public long count;

	private CountableByte(final byte byteValue, final long count) {
		this.byteValue = byteValue;
		this.count = count;
	}

	public static WithMissingCount ofValue(final byte value) {
		return (final long count) -> { return new CountableByte(value, count); };
	}

	public static WithMissingValue ofCount(final long count) {
		return (final byte value) -> { return new CountableByte(value, count); };
	}

	public long count() {
		return count;
	}

	public void setCount(final long count) {
		this.count = count;
	}

	public byte byteValue() {
		return byteValue;
	}

	public CountableByte modifyCount(final LongUnaryOperator op) {
		count = op.applyAsLong(count);
		return this;
	}

	@Override
	public int compareTo(final CountableByte o) {
		return NATURAL_ORDER.compare(this, o);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + byteValue;
		result = prime * result + (int) (count ^ count >>> 32);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final CountableByte other = (CountableByte) obj;
		if (byteValue != other.byteValue) { return false; }
		if (count != other.count) { return false; }
		return true;
	}

	@Override
	public String toString() {
		return "byte: " + Utils.toHexString(byteValue) + ", count: " + count;
	}

	public interface WithMissingCount {
		CountableByte withCount(long count);
	}

	public interface WithMissingValue {
		CountableByte withValue(byte value);
	}

}