package de.plush.brix.c64graphics.core.pipeline;

import static java.util.Arrays.asList;

import java.io.Closeable;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.*;

import javax.annotation.WillCloseWhenClosed;

import de.plush.brix.c64graphics.core.util.Utils;

public class PipelineConsumer<In, Out> implements ImageConsumer<In> {
	private final Function<In, Out> pipeline;

	private final List<AutoCloseable> resourcesToClose;

	private final AtomicBoolean closed = new AtomicBoolean();

	private Optional<List<Out>> optionalList = Optional.empty(); // just don't add values, if not necessary

	private Optional<Consumer<? super List<Out>>> optionalPostProcessingStep = Optional.empty();

	public PipelineConsumer(@WillCloseWhenClosed final Function<In, Out> pipeline, @WillCloseWhenClosed final Closeable... resourcesToClose) {
		this(pipeline, asList(resourcesToClose));
	}

	public PipelineConsumer(@WillCloseWhenClosed final Function<In, Out> pipeline, final Consumer<? super List<Out>> postProcessor,
			@WillCloseWhenClosed final Closeable... resourcesToClose) {
		this(pipeline, ArrayList<Out>::new, postProcessor, asList(resourcesToClose));
	}

	public PipelineConsumer(@WillCloseWhenClosed final Function<In, Out> pipeline, @WillCloseWhenClosed final List<AutoCloseable> resourcesToClose) {
		this(pipeline, null, null, resourcesToClose);
	}

	public PipelineConsumer(@WillCloseWhenClosed final Function<In, Out> pipeline, final Consumer<? super List<Out>> postProcessor,
			@WillCloseWhenClosed final List<AutoCloseable> resourcesToClose) {
		this(pipeline, ArrayList<Out>::new, postProcessor, resourcesToClose);
	}

	/**
	 * 
	 * @param <L>
	 *            the type of list in which each pipeline step is stored for post processing
	 * @param pipeline
	 *            the pipeline to process each image
	 * @param postProcessingAssemblyList
	 *            provides the list to store each pipeline result
	 * @param postProcessor
	 *            post processes each pipeline step that was stored in the post processing assembly list
	 * @param resourcesToClose
	 *            a list of resources to be closed if this consumer is closed
	 */
	public <L extends List<Out>> PipelineConsumer(@WillCloseWhenClosed final Function<In, Out> pipeline, final Supplier<L> postProcessingAssemblyList,
			final Consumer<? super L> postProcessor, @WillCloseWhenClosed final List<AutoCloseable> resourcesToClose) {
		this.pipeline = pipeline;
		this.resourcesToClose = new ArrayList<>(resourcesToClose);
		optionalList = Optional.ofNullable(postProcessingAssemblyList).map(Supplier::get);
		@SuppressWarnings("unchecked") // we know it will eat the supplied list from the method signature
		final var _postProcessingStep = (Consumer<? super List<? super Out>>) postProcessor;
		optionalPostProcessingStep = Optional.ofNullable(_postProcessingStep);
	}

	@Override
	public void onImage(final In image) {
		if (!closed.get()) {
			final Out result = pipeline.apply(image);
			optionalList.ifPresent(list -> list.add(result));
		}
	}

	@Override
	public void close() {
		if (closed.compareAndSet(false, true)) { // prohibits double close
			System.out.println("close PipelineConsumer");
			if (pipeline instanceof AutoCloseable) {
				try (AutoCloseable x = (AutoCloseable) pipeline) {
					System.out.println("close pipeline");
					// have pipeline autoclosed
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
			Utils.close(resourcesToClose);
			System.out.println("closes resources");
			optionalPostProcessingStep.ifPresent(post -> post.accept(optionalList.get()));
		}
	}
}