package de.plush.brix.c64graphics.core.model;

import java.net.*;

import de.plush.brix.c64graphics.core.util.*;

/**
 * Standard multicolor bitmap, 2 screen rams, color RAM and background color.<br>
 * E.g. FLI effect once per character line (upper half of character has other screen RAM colors as lower half).
 *
 * @author Wanja Gayk
 */
public class C64BitmapmodePictureMulticolorHalfcharFli extends C64BitmapmodePictureMulticolorNscreenFLI {

	private static final int SCREEN_COUNT = 2;

	public C64BitmapmodePictureMulticolorHalfcharFli(final C64Bitmap bitmap, final C64Screen[] screens, final C64ColorRam colorRam,
			final byte backgroundColorCode) {
		super(SCREEN_COUNT, bitmap, screens, colorRam, backgroundColorCode);
	}

	public C64BitmapmodePictureMulticolorHalfcharFli(final C64BitmapmodePictureMulticolorHalfcharFli other) {
		super(SCREEN_COUNT, other.bitmapScreen, other.colorRam, other.backgroundColorCode);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!super.equals(obj)) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		return true;
	}

	/**
	 * Multicolor Bitmap, 2 x Screen RAM<br>
	 * <style> #gfxformat th { background:lightgray; text-align: left; width:auto}</style> <style> #gfxformat td.first { width:18ch; }</style><style> #gfxformat
	 * td { width:auto}</style>
	 * <table id="gfxformat">
	 * <th colspan="2">Halfchar FLI (by Skate/Plush)</th>
	 * <tr>
	 * <td class="first">$4000 - $6bff</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$4000 - $5f3f</td>
	 * <td>Bitmap</td>
	 * <tr>
	 * <td class="first">$5f40</td>
	 * <td>Background ($d021)</td>
	 * <tr>
	 * <td class="first">$6000 - $67ff</td>
	 * <td>Screen RAMs</td>
	 * <tr>
	 * <td class="first">$6800 - $6bff</td>
	 * <td>Color RAM</td>
	 *
	 * </table>
	 *
	 * @return A provider that will provide the C64 binary of this image at the time this method was invoked.
	 */
	public C64BinaryProvider binaryProviderPlush() {
		return plushFormat().binaryProvider(this);
	}

	public static C64BitmapmodePictureMulticolorHalfcharFli loadPlush(final URL loc, final StartAddress startAddress) {
		return plushFormat().load(Utils.readFile(loc, startAddress));
	}

	public static C64BitmapmodePictureMulticolorHalfcharFli loadPlush(final URI loc, final StartAddress startAddress) {
		return plushFormat().load(Utils.readFile(loc, startAddress));
	}

	public static C64BitmapmodePictureMulticolorHalfcharFli fromPlushBinary(final byte[] binary) {
		return plushFormat().load(binary);
	}

	private static Format plushFormat() {
		return (Format) new Format()//
				.withLoadAddress(0x4000, 0x6bff)//
				.withBitmapAddress(0x4000)//
				.withBackgroundAddress(0x5f40)//
				.withScreensAddress(0x6000)//
				.withCramAddress(0x6800);
	}

	private static class Format extends C64MulticolorFliFormat<C64BitmapmodePictureMulticolorHalfcharFli> {
		C64BitmapmodePictureMulticolorHalfcharFli load(final byte[] binary) {
			return super.load(binary, SCREEN_COUNT, C64BitmapmodePictureMulticolorHalfcharFli::new);
		}
	}

	/**
	 * From <a href=
	 * "http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03">http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03</a>:<br>
	 * <style> #gfxformat th { background:lightgray; text-align: left; width:auto}</style> <style> #gfxformat td.first { width:18ch; }</style><style> #gfxformat
	 * td { width:auto}</style>
	 * <table id="gfxformat">
	 * <th colspan="2">FLI Graph 2.2 (by blackmail) (pc-ext: .bml)</th>
	 * <tr>
	 * <td class="first">3b00 - $7ffe</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$3c00 - $3bc7</td>
	 * <td>$d021 colors (ignored)</td>
	 * <tr>
	 * <td class="first">$3c00 - $3fe7</td>
	 * <td>Color RAM</td>
	 * <tr>
	 * <td class="first">$4000 - $5fe7</td>
	 * <td>Screen RAMs</td>
	 * <tr>
	 * <td class="first">$6000 - $7f3f</td>
	 * <td>Bitmap</td>
	 * </table>
	 *
	 * @return A provider that will provide the C64 binary of this image at the time this method was invoked.
	 */
	public C64BinaryProvider binaryProviderFliGraphBlackmailFormat() {
		final C64BitmapmodePictureMulticolorFli pictureMulticolorFli = new C64BitmapmodePictureMulticolorFli(bitmap(), new C64Screen[] { screen(0), screen(0),
				screen(0), screen(0), screen(1), screen(1), screen(1), screen(1) }, colorRam, backgroundColorCode);
		return pictureMulticolorFli.binaryProviderFliGraphBlackmail();
	}
}
