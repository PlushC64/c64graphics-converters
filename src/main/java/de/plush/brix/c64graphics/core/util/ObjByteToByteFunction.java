package de.plush.brix.c64graphics.core.util;

@FunctionalInterface
public interface ObjByteToByteFunction<T> {
	byte apply(T p, byte b);
}
