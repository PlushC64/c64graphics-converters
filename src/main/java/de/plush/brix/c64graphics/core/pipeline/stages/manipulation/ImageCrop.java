/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.*;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class ImageCrop implements IPipelineStage<BufferedImage, BufferedImage> {

	private final Rectangle targetArea;
	private final Insets insets;

	public ImageCrop(final Rectangle targetArea) {
		this.targetArea = new Rectangle(targetArea);
		insets = null;
	}

	public ImageCrop(final Insets insets) {
		targetArea = null;
		this.insets = new Insets(insets.top, insets.left, insets.bottom, insets.right);
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		if (targetArea != null) {
			return original.getSubimage(targetArea.x, targetArea.y, targetArea.width, targetArea.height);
		} else if (insets != null) {
			return original.getSubimage(insets.left, insets.top, //
					original.getWidth() - insets.left - insets.right, original.getHeight() - insets.top - insets.bottom);
		} else {
			return original;
		}
	}
}