/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.misc;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class FrameCount<T> implements IPipelineStage<T, T> {

	private int t;

	@Override
	public T apply(final T anything) {
		System.out.println("frame: " + (++t));
		return anything;
	}
}