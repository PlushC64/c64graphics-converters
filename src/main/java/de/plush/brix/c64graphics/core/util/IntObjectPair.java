package de.plush.brix.c64graphics.core.util;

import static java.util.Comparator.*;

import java.util.Comparator;

public class IntObjectPair<T> implements Comparable<IntObjectPair<T>> {

	private static final Comparator<IntObjectPair<?>> ORDER_BY_INT = nullsFirst(comparingLong(IntObjectPair::getOne));

	private static final Comparator<Object> ORDER_BY_IDENTiTY = comparingInt(System::identityHashCode);

	private static final Comparator<IntObjectPair<?>> NATURAL_ORDER = nullsFirst(new Comparator<>() {
		@SuppressWarnings({ "rawtypes", "unchecked" })
		@Override
		public int compare(final IntObjectPair<?> o1, final IntObjectPair<?> o2) {
			final int c = ORDER_BY_INT.compare(o1, o2);
			return c != 0 ? c
					: ((Comparator) (o1.two instanceof Comparable && o2.two instanceof Comparable //
							? naturalOrder() //
							: ORDER_BY_IDENTiTY)//
			).compare(o1.two, o2.two);
		}
	});

	private final int one;

	private final T two;

	public IntObjectPair(final int newOne, final T newTwo) {
		this.one = newOne;
		this.two = newTwo;
	}

	public static <T> IntObjectPair<T> of(final int i, final T o) {
		return new IntObjectPair<>(i, o);
	}

	public int getOne() {
		return this.one;
	}

	public T getTwo() {
		return this.two;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + one;
		result = prime * result + (two == null ? 0 : two.hashCode());
		return result;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final IntObjectPair other = (IntObjectPair) obj;
		if (one != other.one) { return false; }
		if (two == null) {
			if (other.two != null) { return false; }
		} else if (!two.equals(other.two)) { return false; }
		return true;
	}

	@Override
	public String toString() {
		return this.one + ":" + this.two;
	}

	@Override
	public int compareTo(final IntObjectPair<T> that) {
		return NATURAL_ORDER.compare(this, that);
	}

}
