package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.*;
import java.awt.geom.Area;
import java.awt.image.*;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

abstract class AbstractImageToC64Charset implements IPipelineStage<BufferedImage, C64Charset> {

	protected final C64Screen screen;

	protected final Area areaOfBlockPositions;

	AbstractImageToC64Charset(final CharsetFormat grid) {
		screen = grid.createScreen();
		areaOfBlockPositions = grid.areaOfBlockPositions();
	}

	protected abstract Function<BufferedImage, C64Char> createImageToC64Char();

	@Override
	public C64Charset apply(final BufferedImage image) {
		final Function<BufferedImage, C64Char> imageToC64Char = createImageToC64Char();
		final C64Charset charset = new C64Charset(); // will be the target charset, may not be full
		for (int yBlock = 0; yBlock < C64Screen.HEIGHT_BLOCKS; ++yBlock) {
			for (int xBlock = 0; xBlock < C64Screen.WIDTH_BLOCKS; ++xBlock) {
			 final Point blockPosition = new Point(xBlock, yBlock); 
				if (areaOfBlockPositions.contains(blockPosition)) {
					try {
						final BufferedImage blockImage = image.getSubimage(xBlock * C64Char.WIDTH_PIXELS, yBlock * C64Char.WIDTH_PIXELS, //
								C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS);
						final C64Char chr = imageToC64Char.apply(blockImage);
						final byte screenCode = screen.screenCodeAt(xBlock, yBlock);
						charset.putCharacter(screenCode, chr);
					} catch (final IllegalArgumentException e) {
						throw new IllegalArgumentException("At block xBlock: " + xBlock + ", yBlock: " + yBlock, e);
					} catch (final RasterFormatException e) {
						throw new RuntimeException("blockPosition: " + blockPosition + " image size: " + new Dimension(image.getWidth(), image.getHeight()), e);
					}
				}
			}
		}
		return charset;
	}

}