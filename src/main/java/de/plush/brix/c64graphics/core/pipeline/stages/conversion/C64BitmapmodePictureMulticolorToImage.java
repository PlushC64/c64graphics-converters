package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;

import java.awt.*;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.Palette;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.C64CharToImageMulticolor.Mode;

public class C64BitmapmodePictureMulticolorToImage implements IPipelineStage<C64BitmapmodePictureMulticolor, BufferedImage> {

	private final Palette palette;

	public C64BitmapmodePictureMulticolorToImage(final Palette palette) {
		this.palette = palette;
	}

	@Override
	public BufferedImage apply(final C64BitmapmodePictureMulticolor bitmapScreenColorRam) {
		final BufferedImage result = new BufferedImage(C64Bitmap.WIDTH_PIXELS, C64Bitmap.HEIGHT_PIXELS, BufferedImage.TYPE_INT_RGB);
		final Graphics2D g = result.createGraphics();
		try {
			final C64Bitmap bitmap = bitmapScreenColorRam.bitmap();
			final C64Screen screen = bitmapScreenColorRam.screen();
			final C64ColorRam colorRam = bitmapScreenColorRam.colorRam();
			final Color background = palette.color(toUnsignedByte(bitmapScreenColorRam.backgroundColorCode() & 0x0F));

			// jmh testing showed that the simple for-loops seem to be twice as fast as using parallel streams.
			for (int yBlock = 0; yBlock < C64Bitmap.HEIGHT_BLOCKS; ++yBlock) {
				for (int xBlock = 0; xBlock < C64Bitmap.WIDTH_BLOCKS; ++xBlock) {
					final Point blockPos = new Point(xBlock, yBlock);
					final BufferedImage blockImage = blockImage(blockPos, bitmap, screen, colorRam, background);
					g.drawImage(blockImage, blockPos.x * C64Char.WIDTH_PIXELS, blockPos.y * C64Char.HEIGHT_PIXELS, null);
				}
			}
		} finally {
			g.dispose();
		}
		return result;
	}

	public BufferedImage blockImage(final Point blockPos, final C64Bitmap bitmap, final C64Screen screen, final C64ColorRam colorRam, final Color background) {
		final byte screenCode = screen.screenCodeAt(blockPos.x, blockPos.y);
		final Color mc1 = palette.color(toUnsignedByte(screenCode >>> 4 & 0x0F));
		final Color mc2 = palette.color(toUnsignedByte(screenCode & 0x0F));
		final Color cram = palette.color(toUnsignedByte(colorRam.colorCodeAt(blockPos.x, blockPos.y) & 0x0F));
		final C64Char block = bitmap.blockAt(blockPos.x, blockPos.y);
		return new C64CharToImageMulticolor(palette, () -> background, mc1, mc2, cram, Mode.Bitmap).apply(block);
	}

}
