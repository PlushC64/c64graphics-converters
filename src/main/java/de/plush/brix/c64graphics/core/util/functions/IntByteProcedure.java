package de.plush.brix.c64graphics.core.util.functions;

@FunctionalInterface
public interface IntByteProcedure {
	void value(int argument1, byte argument2);
}
