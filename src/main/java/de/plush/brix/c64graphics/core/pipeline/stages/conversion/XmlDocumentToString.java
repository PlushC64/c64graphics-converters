/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.io.StringWriter;

import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class XmlDocumentToString implements IPipelineStage<Document, String> {

	private final TransformerFactory transformerFactory = TransformerFactory.newInstance();

	@Override
	public String apply(final Document doc) {
		try {
			final Transformer transformer = transformerFactory.newTransformer();
			/// transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			final StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			return writer.getBuffer().toString();
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

}