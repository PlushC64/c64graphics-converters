package de.plush.brix.c64graphics.core.util.charpixeldistance;

import static java.lang.Math.*;
import static java.util.Arrays.stream;

import java.util.Comparator;

import de.plush.brix.c64graphics.core.model.C64Char;

/**
 * Interface for measures to get the distance between to C64Char instances, based on raw pixel data (strictly hires)
 *
 * @author Wanja Gayk
 */
public interface C64CharHiresPixelDistanceMeasure {

	/** @returns a value between 0 and 1(inclusive). Length of the vector of all measurements. */
	public static C64CharHiresPixelDistanceMeasure combined(final C64CharHiresPixelDistanceMeasure... measures) {
		return new C64CharHiresPixelDistanceMeasure() {
			@Override
			public double distanceOf(final C64Char c1, final C64Char c2) {
				// have a vector of length 0..1 of all measuring components (be a bit prudent in case someone uses values from -1 to 1);
				return sqrt(stream(measures)// for 9 measures sqrt(sum) is between 0 and 3, for 4 measures between 0 and 2. etc.
						.mapToDouble(m -> m.distanceOf(c1, c2))// d is expected to be a value from 0 to 1.
						.map(d -> pow(d, 2))// pow(1,2) = 1 max
						.sum()) / sqrt(measures.length); // for 9 measures is (0..3) / 4, for 4 measures is (0..2) / 2 -> a value between 0 and 1
			}
		};
	}

	/**
	 * Creates a comparator to compare characters against a fixed C64Char.
	 *
	 * @param ref
	 *            a reference character to compare against.
	 * @return a comparator implementation
	 */
	public default Comparator<C64Char> compareByDistanceTo(final C64Char ref) {
		return (c1, c2) -> c1 == c2 || c1.equals(c2) ? 0 : Double.compare(distanceOf(ref, c1), distanceOf(ref, c2));
	}

	/**
	 * Get the distance between to C64Char instances,based on raw pixel data (strictly hires)
	 *
	 * @param c1
	 *            some character
	 * @param c2
	 *            another character
	 * @return </tt>0.0</tt> if a.equals(b), a distance value from <tt>0.0</tt> to <tt>1.0</tt> (inclusive) otherwise, where
	 *         <tt>distance(a,b) == distance(b,a)</tt>
	 */
	public abstract double distanceOf(final C64Char c1, final C64Char c2);
}
