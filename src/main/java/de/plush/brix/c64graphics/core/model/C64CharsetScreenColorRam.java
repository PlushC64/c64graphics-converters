package de.plush.brix.c64graphics.core.model;

import static java.lang.System.arraycopy;

import java.awt.Dimension;
import java.util.Arrays;

public class C64CharsetScreenColorRam extends C64CharsetScreen implements C64BinaryProvider {

	@SuppressWarnings("hiding")
	public static final int WIDTH_BLOCKS = C64CharsetScreen.WIDTH_BLOCKS;

	@SuppressWarnings("hiding")
	public static final int HEIGHT_BLOCKS = C64CharsetScreen.HEIGHT_BLOCKS;

	private final C64ColorRam colorRam;

	private byte backgroundColorCode;

	private byte mc1_D022ColorCode;

	private byte mc2_D023ColorCode;

	public C64CharsetScreenColorRam(final C64CharsetScreenColorRam other) {
		this(other.charset(), other.screen(), other.colorRam(), other.backgroundColorCode(), other.mc1_D022ColorCode(), other.mc2_D023ColorCode());
	}

	public C64CharsetScreenColorRam(final C64Char defaultBlock, final byte defaultColorCode) throws TooManyCharactersInCharset {
		this(new C64Charset(), defaultBlock, new C64ColorRam(defaultColorCode));
	}

	public C64CharsetScreenColorRam(final C64Char defaultBlock, final C64ColorRam colorRam) throws TooManyCharactersInCharset {
		this(new C64Charset(), defaultBlock, colorRam);
	}

	public C64CharsetScreenColorRam(final C64Charset charset, final C64Char defaultBlock, final byte defaultColorCode) throws TooManyCharactersInCharset {
		this(charset, defaultBlock, new C64ColorRam(defaultColorCode));
	}

	public C64CharsetScreenColorRam(final C64Charset charset, final C64Char defaultBlock, final C64ColorRam colorRam) throws TooManyCharactersInCharset {
		this(charset, new C64Char[HEIGHT_BLOCKS][WIDTH_BLOCKS], colorRam);
		charset.addCharacter(defaultBlock);
		for (final C64Char[] line : characterMatrix) { // TODO: it's probably better to replace inheritance by composition
			Arrays.fill(line, defaultBlock);
		}
	}

	public C64CharsetScreenColorRam(final C64Charset charset, final C64Char[][] characterMatrix, final C64ColorRam colorRam) {
		this(charset, createScreen(charset, characterMatrix), colorRam);
	}

	public C64CharsetScreenColorRam(final C64Charset charset, final C64Screen screen, final C64ColorRam colorRam) {
		this(charset, screen, colorRam, (byte) 0);
	}

	public C64CharsetScreenColorRam(final C64Charset charset, final C64Screen screen, final C64ColorRam colorRam, final byte backgroundColorCode) {
		this(charset, screen, colorRam, backgroundColorCode, backgroundColorCode, backgroundColorCode);
	}

	public C64CharsetScreenColorRam(final C64Charset charset, final C64Screen screen, final C64ColorRam colorRam, final byte backgroundColorCode,
			final byte mc1_D022ColorCode, final byte mc2_D023ColorCode) {
		super(charset, screen);
		this.colorRam = new C64ColorRam(colorRam);
		this.mc1_D022ColorCode = mc1_D022ColorCode;
		this.mc2_D023ColorCode = mc2_D023ColorCode;
		this.backgroundColorCode = backgroundColorCode;
	}

	public C64CharsetScreenColorRam(final C64CharsetScreen charScreenImage, final C64ColorRam colorRam) {
		this(charScreenImage, colorRam, (byte) 0);
	}

	public C64CharsetScreenColorRam(final C64CharsetScreen charScreenImage, final C64ColorRam colorRam, final byte backgroundColorCode) {
		super(charScreenImage);
		this.colorRam = new C64ColorRam(colorRam);
		setBackgroundColorCode(backgroundColorCode);
	}

	public static Dimension sizePixels() {
		return C64CharsetScreen.sizePixels();
	}

	public static Dimension sizeBlocks() {
		return C64CharsetScreen.sizeBlocks();
	}

	public void setColorCode(final int x, final int y, final byte screenCode) {
		colorRam.setColorCode(x, y, screenCode);
	}

	public byte colorCodeAt(final int x, final int y) {
		return colorRam.colorCodeAt(x, y);
	}

	public byte[][] colorCodeMatrix() {
		return colorRam.colorCodeMatrix();
	}

	public C64ColorRam colorRam() {
		return new C64ColorRam(colorRam);
	}

	public byte backgroundColorCode() {
		return backgroundColorCode;
	}

	public void setBackgroundColorCode(final byte backgroundColorCode) {
		this.backgroundColorCode = backgroundColorCode;
	}

	public byte mc1_D022ColorCode() {
		return mc1_D022ColorCode;
	}

	public void setMc1_D022ColorCode(final byte mc1_D022ColorCode) {
		this.mc1_D022ColorCode = mc1_D022ColorCode;
	}

	public byte mc2_D023ColorCode() {
		return mc2_D023ColorCode;
	}

	public void setMc2_D023ColorCode(final byte mc2_D023ColorCode) {
		this.mc2_D023ColorCode = mc2_D023ColorCode;
	}

	@Override
	public void rollLeftBlockwise(final int n) {
		super.rollLeftBlockwise(n);
		colorRam.rollLeftBlockwise(n);
	}

	@Override
	public void rollRightBlockwise(final int n) {
		super.rollRightBlockwise(n);
		colorRam.rollRightBlockwise(n);
	}

	@Override
	public void rollUpBlockwise(final int n) {
		super.rollUpBlockwise(n);
		colorRam.rollUpBlockwise(n);
	}

	@Override
	public void rollDownBlockwise(final int n) {
		super.rollDownBlockwise(n);
		colorRam.rollDownBlockwise(n);
	}

	public byte[] colorRamBinary() {
		return colorRam.toC64Binary();
	}

	/**
	 * @return 0xFF9 bytes as follows: <br/>
	 *         0x800 bytes charset <br/>
	 *         0x400 bytes screen <br/>
	 *         0x3E8 bytes color ram <br/>
	 *         1 byte background color code
	 */
	@Override
	public byte[] toC64Binary() {
		final byte[] binary = new byte[0x0800 + 0x0400 + 0x03E8 + 1];
		final byte[] charsetBinary = charsetBinary();
		final byte[] screenBinary = screenBinary();
		final byte[] colorRamBinary = colorRamBinary();
		arraycopy(charsetBinary, 0, binary, 0, charsetBinary.length);
		arraycopy(screenBinary, 0, binary, 0x0800, screenBinary.length);
		arraycopy(colorRamBinary, 0, binary, 0x0c00, colorRamBinary.length);
		binary[0x0FE8] = backgroundColorCode;
		return binary;
	}

	public static C64CharsetScreenColorRam fromC64Binary(final byte[] binary) {
		final byte[] charsetBinary = new byte[C64Charset.MAX_CHARACTERS * C64Char.HEIGHT_PIXELS];
		final byte[] screenBinary = new byte[C64Screen.WIDTH_BLOCKS * C64Screen.HEIGHT_BLOCKS];
		final byte[] colorRamBinary = new byte[C64Screen.WIDTH_BLOCKS * C64Screen.HEIGHT_BLOCKS];
		System.arraycopy(binary, 0, charsetBinary, 0, charsetBinary.length);
		System.arraycopy(binary, 0x0800, screenBinary, 0, screenBinary.length);
		System.arraycopy(binary, 0x0c00, colorRamBinary, 0, colorRamBinary.length);
		final C64Charset charset = C64Charset.fromBinary(charsetBinary);
		final C64Screen c64Screen = C64Screen.fromBinary(screenBinary);
		final C64ColorRam c64ColorRam = C64ColorRam.binaryToC64ColorRam(colorRamBinary);
		final byte backgroundColorCode = binary[0x0FE8];
		return new C64CharsetScreenColorRam(charset, c64Screen, c64ColorRam, backgroundColorCode);
	}

}
