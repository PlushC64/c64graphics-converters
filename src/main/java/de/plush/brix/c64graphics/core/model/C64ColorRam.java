package de.plush.brix.c64graphics.core.model;

import static de.plush.brix.c64graphics.core.util.Utils.*;

import java.awt.*;
import java.net.URI;
import java.util.Arrays;

import de.plush.brix.c64graphics.core.util.*;
import de.plush.brix.c64graphics.core.util.functions.ByteToByteFunction;

/**
 * Commodore 64 ColorRAM model. <br>
 * This class does not enforce values &lt;= $0f, to not sabotage conversions to interlace color tables (that might store a secondary color in the high nibble).
 *
 * @author Wanja Gayk
 */
public class C64ColorRam implements C64BinaryProvider, IRollBlockwise {
	public static final int WIDTH_BLOCKS = 40;

	public static final int HEIGHT_BLOCKS = 25;

	private final byte[][] colorCodes;

	public C64ColorRam(final C64ColorRam other) {
		this(other.colorCodes);
	}

	public C64ColorRam(final byte[][] colorCodes) {
		this.colorCodes = deepCopy(colorCodes);
	}

	public C64ColorRam(final byte defaultColorCode) {
		colorCodes = new byte[HEIGHT_BLOCKS][WIDTH_BLOCKS];
		for (final byte[] line : colorCodes) {
			Arrays.fill(line, defaultColorCode);
		}
	}

	public static Dimension sizeBlocks() {
		return new Dimension(WIDTH_BLOCKS, HEIGHT_BLOCKS);
	}

	public void setColorCode(final int x, final int y, final byte colorCode) {
		colorCodes[y][x] = colorCode;
	}

	public byte colorCodeAt(final int x, final int y) {
		return colorCodes[y][x];
	}

	public byte[][] colorCodeMatrix() {
		return Utils.deepCopy(colorCodes);
	}

	public void modifyColorCodeAt(final Point p, final ByteToByteFunction modifier) {
		modifyColorCodeAt(p.x, p.y, modifier);
	}

	public void modifyColorCodeAt(final int x, final int y, final ByteToByteFunction modifier) {
		final byte code = colorCodeAt(y, x);
		setColorCode(y, x, modifier.valueOf(code));
	}

	/**
	 * @param modifier
	 *            a function that takes the current position and color code and returns the color code that should be set at that position.<br>
	 *            Note that there are no guarantees about the iteration order.
	 */
	public void modifyEveryColorCode(final ObjByteToByteFunction<Point> modifier) {
		for (int x = 0; x < C64ColorRam.HEIGHT_BLOCKS; ++x) {
			for (int y = 0; y < C64ColorRam.WIDTH_BLOCKS; ++y) {
				final byte code = colorCodeAt(y, x);
				setColorCode(y, x, modifier.apply(new Point(x, y), code));
			}
		}
	}

	@Override
	public void rollLeftBlockwise(final int n) {
		for (final byte[] line : colorCodes) {
			Utils.rollLeft(line, n);
		}
	}

	@Override
	public void rollRightBlockwise(final int n) {
		for (final byte[] line : colorCodes) {
			Utils.rollRight(line, n);
		}
	}

	@Override
	public void rollUpBlockwise(final int n) {
		Utils.rollLeft(colorCodes, n);
	}

	@Override
	public void rollDownBlockwise(final int n) {
		Utils.rollRight(colorCodes, n);
	}

	public static C64ColorRam loadColorRam(final URI uri, final StartAddress startAddress) {
		return binaryToC64ColorRam(Utils.readFile(uri, startAddress));
	}

	public static C64ColorRam binaryToC64ColorRam(final byte[] bytes) {
		final byte[][] colorCodes = new byte[HEIGHT_BLOCKS][WIDTH_BLOCKS];
		for (int t = 0; t < bytes.length; ++t) {
			final int x = t % WIDTH_BLOCKS;
			final int y = t / WIDTH_BLOCKS;
			colorCodes[y][x] = bytes[t];
		}
		return new C64ColorRam(colorCodes);
	}

	@Override
	public byte[] toC64Binary() {
		final byte[] binary = new byte[WIDTH_BLOCKS * HEIGHT_BLOCKS];
		for (int index = 0; index < binary.length; ++index) {
			binary[index] = colorCodeAt(index % WIDTH_BLOCKS, index / WIDTH_BLOCKS);
		}
		return binary;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(colorCodes);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final C64ColorRam other = (C64ColorRam) obj;
		if (!Arrays.deepEquals(colorCodes, other.colorCodes)) { return false; }
		return true;
	}

	@Override
	public String toString() {
		final String intro = "C64ColorRam \n";
		final StringBuilder sb = new StringBuilder(1000 * 3 + intro.length());
		sb.append(intro);
		for (int y = 0; y < C64ColorRam.HEIGHT_BLOCKS; ++y) {
			for (int x = 0; x < C64ColorRam.WIDTH_BLOCKS; ++x) {
				sb.append(toHexString(colorCodeAt(x, y)));
				if (x + 1 < C64ColorRam.WIDTH_BLOCKS) {
					sb.append(',');
				}
			}
			sb.append('\n');
		}
		return sb.toString();
	}

}
