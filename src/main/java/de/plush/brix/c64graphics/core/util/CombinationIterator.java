package de.plush.brix.c64graphics.core.util;

import java.util.*;
import java.util.function.IntFunction;
import java.util.stream.*;

/**
 * @see <a href="https://stackoverflow.com/a/14848763/6198746">https://stackoverflow.com/a/14848763/6198746</a>
 *
 * @author kharole
 * @author Wanja Gayk (added collection producer)
 *
 * @param <C>
 *            the type of collection for each combination
 * @param <T>
 *            the element type
 */
class CombinationIterator<C extends Collection<T>, T> implements Iterator<C> {

	private final int[] indices;

	private final List<T> elements;

	private boolean hasNext = true;

	private final IntFunction<C> combinationCollectionProducer;

	public CombinationIterator(final List<T> elements, final int combinationSize, final IntFunction<C> combinationCollectionProducer)
			throws IllegalArgumentException {
		this.elements = elements;
		this.combinationCollectionProducer = combinationCollectionProducer;
		this.indices = new int[combinationSize];
		for (int i = 0; i < combinationSize; i++) {
			indices[i] = combinationSize - 1 - i;
		}
	}

	public static <C extends Collection<T>, T> Stream<C> asStream(final List<T> elements, final int k, final IntFunction<C> collectionProducer) {
		final Iterable<C> iterable = () -> new CombinationIterator<>(elements, k, collectionProducer);
		return StreamSupport.stream(iterable.spliterator(), false);
	}

	@Override
	public boolean hasNext() {
		return hasNext;
	}

	private int inc(final int[] indices, final int maxIndex, final int depth) throws IllegalStateException {
		if (depth == indices.length) { throw new IllegalStateException("depth == indices.length"); }
		if (indices[depth] < maxIndex) {
			indices[depth] = indices[depth] + 1;
		} else {
			indices[depth] = inc(indices, maxIndex - 1, depth + 1) + 1;
		}
		return indices[depth];
	}

	private boolean inc() {
		try {
			inc(indices, elements.size() - 1, 0);
			return true;
		} catch (final IllegalStateException e) {
			return false;
		}
	}

	@Override
	public C next() {
		final C result = combinationCollectionProducer.apply(indices.length);
		for (int index = indices.length - 1; index >= 0; --index) {
			result.add(elements.get(indices[index]));
		}
		hasNext = inc();
		return result;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}