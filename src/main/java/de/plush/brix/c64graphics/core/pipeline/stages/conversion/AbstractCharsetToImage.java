package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.image.BufferedImage;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.util.Utils;

abstract class AbstractCharsetToImage implements IPipelineStage<C64Charset, BufferedImage> {

	private final CharsetFormat grid;

	private final Supplier<IPipelineStage<C64CharsetScreen, BufferedImage>> charsetScreenToImageRef = Utils.lazy(this::createCharsetScreenToImage);

	public AbstractCharsetToImage(final CharsetFormat grid) {
		this.grid = grid;
	}

	protected abstract IPipelineStage<C64CharsetScreen, BufferedImage> createCharsetScreenToImage();

	@Override
	public BufferedImage apply(C64Charset charset) {
		final C64Screen screen = grid.createScreen();
		charset = new C64Charset(charset);
		charset.addBlanksForForMissingScreenCodes(screen.uniqueScreenCodes());
		return charsetScreenToImageRef.get().apply(new C64CharsetScreen(charset, screen));
	}
}
