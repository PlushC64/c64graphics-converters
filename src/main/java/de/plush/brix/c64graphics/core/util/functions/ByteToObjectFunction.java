package de.plush.brix.c64graphics.core.util.functions;

@FunctionalInterface
public interface ByteToObjectFunction<V> {
	V valueOf(byte byteParameter);
}
