package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

public class Alignment {

	public static enum Horizontal {
		LEFT {
			@Override
			public int offset(final int widthCanvas, final int widthObject) {
				return 0;
			}
		},
		CENTER {
			@Override
			public int offset(final int widthCanvas, final int widthObject) {
				return (widthCanvas - widthObject) / 2;
			}
		},
		RIGHT {
			@Override
			public int offset(final int widthCanvas, final int widthObject) {
				return widthCanvas - widthObject;
			}
		};
		public abstract int offset(int lenCanvas, int lenObject);
	}

	public static enum Vertical {
		TOP {
			@Override
			public int offset(final int heightCanvas, final int heightObject) {
				return 0;
			}
		},
		CENTER {
			@Override
			public int offset(final int heightCanvas, final int heightObject) {
				return (heightCanvas - heightObject) / 2;
			}
		},
		BOTTOM {
			@Override
			public int offset(final int heightCanvas, final int heightObject) {
				return heightCanvas - heightObject;
			}
		};
		public abstract int offset(int heightCanvas, int heightObject);
	}
}
