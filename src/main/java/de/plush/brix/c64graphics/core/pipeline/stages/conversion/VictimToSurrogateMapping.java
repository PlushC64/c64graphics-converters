package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import de.plush.brix.c64graphics.core.model.C64Char;

public class VictimToSurrogateMapping {

	public final C64Char victim;
	public final C64Char surrogate;
	public final PreProcessedCharacterImage surrogateImage;
	public final double weight;

	public VictimToSurrogateMapping(final C64Char victim, final C64Char surrogate, final PreProcessedCharacterImage surrogateImage, final double weight) {
		this.victim = victim;
		this.surrogate = surrogate;
		this.surrogateImage = surrogateImage;
		this.weight = weight;
	}

	public C64Char victim() {
		return victim;
	}

	public C64Char surrogate() {
		return surrogate;
	}

	public PreProcessedCharacterImage surrogateImage() {
		return surrogateImage;
	}

	public double weight() {
		return weight;
	}

	public int sortStabilizer() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (surrogate == null ? 0 : surrogate.hashCode());
		result = prime * result + (victim == null ? 0 : victim.hashCode());
		return result;
	}

}
