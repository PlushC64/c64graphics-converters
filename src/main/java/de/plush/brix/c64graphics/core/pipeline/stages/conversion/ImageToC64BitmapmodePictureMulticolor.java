package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;
import static de.plush.brix.c64graphics.core.util.UtilsImage.colors;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.function.*;
import java.util.stream.Stream;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.KoalaColorClashResolver;

public class ImageToC64BitmapmodePictureMulticolor implements IPipelineStage<BufferedImage, C64BitmapmodePictureMulticolor> {

	private final IPipelineStage<BufferedImage, PreProcessedMulticolorImage> colorClashResolver;

	private final Palette palette;

	private List<Color> preferredMc1_d022Colors = emptyList();

	private List<Color> preferredMc2_d023Colors = emptyList();

	private List<Color> preferredCramColors = emptyList();

	/**
	 * Using the standard {@link KoalaColorClashResolver}. <br>
	 * <p>
	 * Use {@link #ImageToC64BitmapmodePictureMulticolor(Palette, IPipelineStage)} if you want to use a custom color clash resolver.
	 * </p>
	 * 
	 * @param palette
	 *            the C64 target palette
	 * @param backgroundSupplier
	 *            provides a C64 palette background color.
	 * @param colorDistanceMeasure
	 *            used to measure how similar/dissimilar a color is.
	 */
	public ImageToC64BitmapmodePictureMulticolor(final Palette palette, final Supplier<Color> backgroundSupplier,
			final ColorDistanceMeasure colorDistanceMeasure) {
		this(palette, new KoalaColorClashResolver(backgroundSupplier, colorDistanceMeasure));
	}

	/**
	 * Allows using a custom color clash resolver.
	 *
	 * @param palette
	 *            the C64 target palette
	 * @param colorClashResolver
	 *            the color clash resolver used for preprocessing.
	 */
	public ImageToC64BitmapmodePictureMulticolor(final Palette palette, final IPipelineStage<BufferedImage, PreProcessedMulticolorImage> colorClashResolver) {
		this.palette = palette;
		this.colorClashResolver = colorClashResolver;
	}

	public ImageToC64BitmapmodePictureMulticolor withPreferredMulticolor1D022Colors(final Color... colors) {
		preferredMc1_d022Colors = asList(colors);
		preferredMc1_d022Colors.forEach(palette::colorCodeOrException);
		checkSettingsConsistency();
		return this;
	}

	public ImageToC64BitmapmodePictureMulticolor withPreferredMulticolor2D023Colors(final Color... colors) {
		preferredMc2_d023Colors = asList(colors);
		preferredMc2_d023Colors.forEach(palette::colorCodeOrException);
		checkSettingsConsistency();
		return this;
	}

	public ImageToC64BitmapmodePictureMulticolor withPreferredColorRamD800Colors(final Color... colors) {
		preferredCramColors = asList(colors);
		preferredCramColors.forEach(palette::colorCodeOrException);
		checkSettingsConsistency();
		return this;
	}

	private void checkSettingsConsistency() {
		crossCheck(preferredMc1_d022Colors, preferredMc2_d023Colors, x -> "You can't force these colors to be in both mc1_d022 and mc2_d022: " + x);
		crossCheck(preferredMc1_d022Colors, preferredCramColors, x -> "You can't force these colors to be in both mc1_d022 and cram_d800: " + x);
		crossCheck(preferredMc2_d023Colors, preferredCramColors, x -> "You can't force these colors to be in both mc2_d022 and cram_d800: " + x);
	}

	private static void crossCheck(final Collection<Color> a, final Collection<Color> b, final Function<Collection<Color>, String> messageProvider) {
		final Set<Color> x = a.stream().filter(b::contains).collect(toSet());
		if (!x.isEmpty()) {
			throw new IllegalArgumentException(messageProvider.apply(x));
		}
	}

	// found no meaningful way to extract methods that make it more readable
	@SuppressWarnings({ "checkstyle:CyclomaticComplexity", "checkstyle:NPathComplexity" })
	@Override
	public C64BitmapmodePictureMulticolor apply(final BufferedImage original) {
		final PreProcessedMulticolorImage sanitized = colorClashResolver.apply(original);
		final Color background = sanitized.background();

		final C64Bitmap bitmap = new C64Bitmap();
		final C64Screen screen = new C64Screen((byte) 0);
		final C64ColorRam colorRam = new C64ColorRam((byte) 0);

		final Dimension blocks = C64Bitmap.sizeBlocks();
		for (int y = 0; y < blocks.height; ++y) {
			for (int x = 0; x < blocks.width; ++x) {
				final int xPix = x * C64Char.WIDTH_PIXELS;
				final int yPix = y * C64Char.HEIGHT_PIXELS;
				final BufferedImage blockImage = sanitized.getSubimage(xPix, yPix, C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS);
				final List<Color> colors = colors(blockImage).stream().filter(c -> !c.equals(background)).collect(toList());
				while (colors.size() < 3) {
					colors.add(palette.color((byte) 0)); // default
				}
				colors.sort(UtilsColor.SORT_BY_BRIGHTNESS); // to get some predictable patterns
				Color mc1 = preferredMc1_d022Colors.stream().filter(colors::contains).findFirst().orElse(null);
				Color mc2 = preferredMc2_d023Colors.stream().filter(colors::contains).findFirst().orElse(null);
				Color d800 = preferredCramColors.stream().filter(colors::contains).findFirst().orElse(null);
				Stream.of(mc1, mc2, d800).filter(c -> c != null).forEach(colors::remove);
				// list of colors has no preferred color left, from here we can assign colors that are not preferred by anyone to empty slots
				final Iterator<Color> colorsLeft = colors.iterator();
				mc1 = mc1 == null ? colorsLeft.hasNext() ? colorsLeft.next() : background : mc1;
				mc2 = mc2 == null ? colorsLeft.hasNext() ? colorsLeft.next() : background : mc2;
				d800 = d800 == null ? colorsLeft.hasNext() ? colorsLeft.next() : background : d800;
				Stream.of(mc1, mc2, d800).forEach(colors::remove);
				if (!colors.isEmpty()) {
					throw new IllegalStateException(
							"unexpectedly found more than 4 colors in block (x=" + x + ", y=" + y + "). Have you solved all color clashes?");
				}
				final C64Char block = new ImageToC64CharMulticolor(background, mc1, mc2, d800).apply(blockImage);
				bitmap.setBlock(x, y, block);
				screen.setScreenCode(x, y, toUnsignedByte(palette.colorCodeOrException(mc1) << 4 | palette.colorCodeOrException(mc2)));
				colorRam.setColorCode(x, y, palette.colorCodeOrException(d800));
			}
		}
		return new C64BitmapmodePictureMulticolor(bitmap, screen, colorRam, palette.colorCodeOrException(background));
	}

}
