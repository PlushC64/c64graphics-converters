package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.image.BufferedImage;
import java.util.HashMap;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import jankovicsandras.imagetracer.ImageTracer;
import jankovicsandras.imagetracer.ImageTracer.ImageData;

/**
 * @author Wanja Gayk
 */
public class ImageToSvgString implements IPipelineStage<BufferedImage, String> {

	private final HashMap<String, Float> options;

	public ImageToSvgString() {
		this(new HashMap<>());
	}

	private ImageToSvgString(final HashMap<String, Float> options) {
		this.options = ImageTracer.checkoptions(options);
	}

	@Override
	public String apply(final BufferedImage original) {
		try {
			final ImageData imageData = ImageTracer.loadImageData(original);
			final byte[][] palette = ImageTracer.getPalette(original, options);
			return ImageTracer.imagedataToSVG(imageData, options, palette);
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @param threshold
	 *            Default: 1f
	 */
	public ImageToSvgString withErrorTresholdForStraightLines(final float threshold) {
		return withKeyValue("ltres", threshold);
	}

	/**
	 * Using very small qtres, e. g. 0.0001 will practically force straight lines.
	 * 
	 * @param threshold
	 *            Default: 1f
	 */
	public ImageToSvgString withErrorTresholdForQuadraticLines(final float threshold) {
		return withKeyValue("qtres", threshold);
	}

	/**
	 * Edge node paths shorter than this will be discarded for noise reduction.
	 * 
	 * @param length
	 *            Default: 8f
	 */
	public ImageToSvgString withEdgeNodePathsShorterThanThisDiscardedForNoiseReduction(final float length) {
		return withKeyValue("pathomit", length);
	}

	public ImageToSvgString withColorSamplingEnabled(final boolean enabled) {
		return withKeyValue("colorsampling", enabled ? 1f : 0f);
	}

	/**
	 * Number of colors to use on palette if pal object is not defined.
	 * 
	 * @param colors
	 *            Default: 16
	 */
	public ImageToSvgString withNumberOfColors(final int colors) {
		return withKeyValue("numberofcolors", colors);
	}

	/**
	 * Color quantization will randomize a color if fewer pixels than (total pixels*ratio) has it. <br>
	 * 
	 * @param ratio
	 *            Default: 0.02f
	 */
	public ImageToSvgString withMinColorsRatioForQuantization(final float ratio) {
		return withKeyValue("mincolorratio", ratio);
	}

	/**
	 * Color quantization will be repeated this many times.
	 * 
	 * @param colorquantcycles
	 *            Default: 3
	 */
	public ImageToSvgString withColorQuantizationCycles(final int colorquantcycles) {
		return withKeyValue("colorquantcycles", colorquantcycles);
	}

	/**
	 * Set this to 1f..5f for selective Gaussian blur preprocessing.
	 * 
	 * @param blurradius
	 *            Default. 0f
	 */
	public ImageToSvgString withBlurRadius(final float blurradius) {
		return withKeyValue("blurradius", blurradius);
	}

	/**
	 * @param blurdelta
	 *            Default; 20f
	 */
	public ImageToSvgString withRgbaDeltaTresholdForSelectiveGaussianBlurPreprocessing(final float blurdelta) {
		return withKeyValue("blurdelta", blurdelta);
	}

	/**
	 * 1 means rounded to 1 decimal place like 7.3 ; 3 means rounded to 3 places, like 7.356
	 * 
	 * @param roundcoords
	 *            default: 1
	 */
	public ImageToSvgString withCoordinatesRoundedToDecimalPlace(final int roundcoords) {
		return withKeyValue("roundcoords", roundcoords);
	}

	/**
	 * Enable or disable SVG viewBox
	 * 
	 * @param enabled
	 *            Default: false
	 */
	public ImageToSvgString withViewboxEnabled(final boolean enabled) {
		return withKeyValue("viewbox", enabled ? 1f : 0f);
	}

	/**
	 * Enable or disable SVG descriptions
	 * 
	 * @param enabled
	 *            Default: true
	 */
	public ImageToSvgString withSvgDesciptionsEnabled(final boolean enabled) {
		return withKeyValue("desc", enabled ? 1f : 0f);
	}

	private ImageToSvgString withKeyValue(final String key, final float value) {
		final HashMap<String, Float> options = new HashMap<>(this.options);
		options.put(key, value);
		return new ImageToSvgString(options);
	}

	/**
	 * If this is greater than zero, small circles will be drawn in the SVG. Do not use this for big/complex images.
	 * 
	 * @param lcpr
	 *            Default: 0f
	 */
	public ImageToSvgString withStraightLineControlPointRadius(final float lcpr) {
		return withKeyValue("lcpr", lcpr);
	}

	/**
	 * If this is greater than zero, small circles and lines will be drawn in the SVG. Do not use this for big/complex images.
	 * 
	 * @param qcpr
	 *            Default: 0f
	 */
	public ImageToSvgString withQuadraticSplineControlPointRadius(final float qcpr) {
		return withKeyValue("qcpr", qcpr);
	}

}
