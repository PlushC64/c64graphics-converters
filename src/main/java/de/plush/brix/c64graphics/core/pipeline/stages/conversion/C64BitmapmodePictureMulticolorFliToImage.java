package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;

import java.awt.*;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.Palette;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.C64CharToImageMulticolor.Mode;

public class C64BitmapmodePictureMulticolorFliToImage implements IPipelineStage<C64BitmapmodePictureMulticolorFli, PreProcessedMulticolorFliImage> {

	private final Palette palette;

	public C64BitmapmodePictureMulticolorFliToImage(final Palette palette) {
		this.palette = palette;
	}

	@Override
	public PreProcessedMulticolorFliImage apply(final C64BitmapmodePictureMulticolorFli c64MulticolorFLI) {
		final byte backgroundColorCode = c64MulticolorFLI.backgroundColorCode();
		final Color background = palette.color(toUnsignedByte(backgroundColorCode & 0x0F));
		final PreProcessedMulticolorFliImage result = new PreProcessedMulticolorFliImage(C64Bitmap.WIDTH_PIXELS, C64Bitmap.HEIGHT_PIXELS,
				BufferedImage.TYPE_INT_RGB, backgroundColorCode);
		final Graphics2D g = result.createGraphics();
		try {
			final C64Bitmap bitmap = c64MulticolorFLI.bitmap();
			final C64Screen[] screens = c64MulticolorFLI.screens();
			final C64ColorRam colorRam = c64MulticolorFLI.colorRam();

			for (int yBlock = 0; yBlock < C64Bitmap.HEIGHT_BLOCKS; ++yBlock) {
				for (int xBlock = 0; xBlock < C64Bitmap.WIDTH_BLOCKS; ++xBlock) {
					for (int line = 0; line < C64Char.HEIGHT_PIXELS; ++line) {
						final Point blockPos = new Point(xBlock, yBlock);
						final BufferedImage blockImage = blockImage(blockPos, bitmap, screens[line], colorRam, background);
						final BufferedImage lineImage = blockImage.getSubimage(0, line, C64Char.WIDTH_PIXELS, 1);
						g.drawImage(lineImage, blockPos.x * C64Char.WIDTH_PIXELS, blockPos.y * C64Char.HEIGHT_PIXELS + line, null);
					}
				}
			}
		} finally {
			g.dispose();
		}
		return result;
	}

	public BufferedImage blockImage(final Point blockPos, final C64Bitmap bitmap, final C64Screen screen, final C64ColorRam colorRam, final Color background) {
		final byte screenCode = screen.screenCodeAt(blockPos.x, blockPos.y);
		final Color mc1 = palette.color(toUnsignedByte(screenCode >>> 4 & 0x0F));
		final Color mc2 = palette.color(toUnsignedByte(screenCode & 0x0F));
		final Color cram = palette.color(toUnsignedByte(colorRam.colorCodeAt(blockPos.x, blockPos.y) & 0x0F));
		final C64Char block = bitmap.blockAt(blockPos.x, blockPos.y);
		return new C64CharToImageMulticolor(palette, () -> background, mc1, mc2, cram, Mode.Bitmap).apply(block);
	}

}
