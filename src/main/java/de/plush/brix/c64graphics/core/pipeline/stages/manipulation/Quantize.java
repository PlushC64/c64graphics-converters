/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import static java.lang.Math.max;

import java.awt.Shape;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;

public class Quantize implements IPipelineStage<BufferedImage, BufferedImage> {

	public final int colors;

	private Quantize standard;
	// private final ColorQuantizerType type;

	private final ColorDistanceMeasure colorDistanceMeasure;

	/**
	 * This is an algorithm-dependent parameter.
	 * <ul>
	 * <li>For the median-cut color quantization, it's the maximum size of the three-dimensional histogram, if <tt>null</tt> defaults to 32768.
	 * <li>For the neu-quant color quantization, it's the number of cycles, if <tt>null</tt> defaults to 100.
	 * <li>For the oct-tree color quantization, it's the maximum size of the oct-tree, if <tt>null</tt> defaults to 65536.
	 * </ul>
	 */
	private final Integer upperBound;

	private final int subSampleRateX; // Defaults to 1, if null.

	private final int subSampleRateY; // Defaults to 1, if null.

	/**
	 * Default color quantizer.<br>
	 * Uses the {@link #withNeuQuantQuantization() NeuQuant} algorithm with a {@link ColorDistance#EUCLIDEAN euclidean} color
	 * distance metric. In case you want to customize the options, explore this api.
	 */
	public Quantize(final int colors) {
		this(ColorDistance.EUCLIDEAN, colors, null, null, null);
		standard = withNeuQuantQuantization();
	}

	private Quantize(final ColorDistanceMeasure colorDistanceMeasure, final int colors, final Integer upperBound, final Integer subSampleRateX,
			final Integer subSampleRateY) {
		this.colorDistanceMeasure = colorDistanceMeasure;
		this.colors = colors;
		this.upperBound = upperBound;
		this.subSampleRateX = subSampleRateX == null ? 1 : max(1, subSampleRateX.intValue());
		this.subSampleRateY = subSampleRateY == null ? 1 : max(1, subSampleRateY.intValue());
	}

	/**
	 * @return a median-cut color quantization that computes the 3D color histogram first, then chooses and divides the largest color cube (in number of pixels)
	 *         along the median, until the required number of clusters is obtained or all the cubes are not separable.<br>
	 *         This is usually the fastest option
	 */
	public MedianCutQuantize withMedianCutQuantization() {
		return new MedianCutQuantize(colorDistanceMeasure, colors, upperBound, subSampleRateX, subSampleRateY);
	}

	/**
	 * @return an Oct-Tree color quantization that constructs an oct-tree of the color histogram, then repeatedly merges the offspring into the parent if they
	 *         contain a number of pixels smaller than a threshold.<br>
	 *         This is usually the option that uses the most memory.
	 */
	public OcttreeQuantize withOctreeQuantization() {
		return new OcttreeQuantize(colorDistanceMeasure, colors, upperBound, subSampleRateX, subSampleRateY);
	}

	/**
	 * @return a NeuQuant color quantization that creates the cluster centers using Kohonen's self-organizing neural network.<br>
	 *         This is usually the slowest option, but gives very good results.
	 */
	public NeuQuantQuantize withNeuQuantQuantization() {
		return new NeuQuantQuantize(colorDistanceMeasure, colors, upperBound, subSampleRateX, subSampleRateY);
	}

	public final class MedianCutQuantize extends Quantize {
		private MedianCutQuantize(final ColorDistanceMeasure colorDistanceMeasure, final int colors, final Integer upperBound, final Integer subSampleRateX,
				final Integer subSampleRateY) {
			super(colorDistanceMeasure, colors, upperBound, subSampleRateX, subSampleRateY);
		}

		/** Defaults to {@link ColorDistance#EUCLIDEAN} */
		public MedianCutQuantize withColorDistanceMeasure(final ColorDistanceMeasure colorDistanceMeasure) {
			return new MedianCutQuantize(colorDistanceMeasure, colors, upperBound, subSampleRateX, subSampleRateY);
		}

		/** defaults to 32768. */
		public MedianCutQuantize with3DHistogramSizeLimit(final int upperBound) {
			return new MedianCutQuantize(colorDistanceMeasure, colors, upperBound, subSampleRateX, subSampleRateY);
		}

		/**
		 * Higher sub sampling rates (i.e. skipped pixels) speed up operation, but come at a cost of less precison.
		 * 
		 * @param subSampleRateX
		 *            defaults to 1, if <tt>null</tt>
		 * @param subSampleRateY
		 *            defaults to 1, if <tt>null</tt>
		 */
		public MedianCutQuantize withSubSampleRates(final Integer subSampleRateX, final Integer subSampleRateY) {
			return new MedianCutQuantize(colorDistanceMeasure, colors, upperBound, subSampleRateX, subSampleRateY);
		}

		@Override
		public BufferedImage apply(final BufferedImage original) {
			final Shape regionOfInterest = null; // May be null to evaluate the whole image
			return new MedianCutQuantizeImpl(colorDistanceMeasure, colors, upperBound, regionOfInterest, subSampleRateX, subSampleRateY).apply(original);
		}
	}

	public final class OcttreeQuantize extends Quantize {
		private OcttreeQuantize(final ColorDistanceMeasure colorDistanceMeasure, final int colors, final Integer upperBound, final Integer subSampleRateX,
				final Integer subSampleRateY) {
			super(colorDistanceMeasure, colors, upperBound, subSampleRateX, subSampleRateY);
		}

		/** Defaults to {@link ColorDistance#EUCLIDEAN} */
		public OcttreeQuantize withColorDistanceMeasure(final ColorDistanceMeasure colorDistanceMeasure) {
			return new OcttreeQuantize(colorDistanceMeasure, colors, upperBound, subSampleRateX, subSampleRateY);
		}

		/** defaults to 65536. */
		public OcttreeQuantize withOctreeSizeLimit(final int upperBound) {
			return new OcttreeQuantize(colorDistanceMeasure, colors, upperBound, subSampleRateX, subSampleRateY);
		}

		/**
		 * Higher sub sampling rates (i.e. skipped pixels) speed up operation, but come at a cost of less precison.
		 * 
		 * @param subSampleRateX
		 *            defaults to 1, if <tt>null</tt>
		 * @param subSampleRateY
		 *            defaults to 1, if <tt>null</tt>
		 */
		public OcttreeQuantize withSubSampleRates(final Integer subSampleRateX, final Integer subSampleRateY) {
			return new OcttreeQuantize(colorDistanceMeasure, colors, upperBound, subSampleRateX, subSampleRateY);
		}

		@Override
		public BufferedImage apply(final BufferedImage original) {
			final Shape regionOfInterest = null; // May be null to evaluate the whole image
			return new OcttreeQuantizeImpl(colorDistanceMeasure, colors, upperBound, regionOfInterest, subSampleRateX, subSampleRateY).apply(original);
		}
	}

	public final class NeuQuantQuantize extends Quantize {
		private NeuQuantQuantize(final ColorDistanceMeasure colorDistanceMeasure, final int colors, final Integer upperBound, final Integer subSampleRateX,
				final Integer subSampleRateY) {
			super(colorDistanceMeasure, colors, upperBound, subSampleRateX, subSampleRateY);
		}

		/** Defaults to {@link ColorDistance#EUCLIDEAN} */
		public NeuQuantQuantize withColorDistanceMeasure(final ColorDistanceMeasure colorDistanceMeasure) {
			return new NeuQuantQuantize(colorDistanceMeasure, colors, upperBound, subSampleRateX, subSampleRateY);
		}

		/** default to 100 */
		public NeuQuantQuantize withMaxCyclesLimit(final int upperBound) {
			return new NeuQuantQuantize(colorDistanceMeasure, colors, upperBound, subSampleRateX, subSampleRateY);
		}

		/**
		 * Higher sub sampling rates (i.e. skipped pixels) speed up operation, but come at a cost of less precison.
		 * 
		 * @param subSampleRateX
		 *            defaults to 1, if <tt>null</tt>
		 * @param subSampleRateY
		 *            defaults to 1, if <tt>null</tt>
		 */
		public NeuQuantQuantize withSubSampleRates(final Integer subSampleRateX, final Integer subSampleRateY) {
			return new NeuQuantQuantize(colorDistanceMeasure, colors, upperBound, subSampleRateX, subSampleRateY);
		}

		@Override
		public BufferedImage apply(final BufferedImage original) {
			final Shape regionOfInterest = null; // May be null to evaluate the whole image
			return new NeuQuantImpl(colorDistanceMeasure, colors, upperBound, regionOfInterest, subSampleRateX, subSampleRateY).apply(original);
		}
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		return standard.apply(original);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [colors=" + colors + ", standard=" + standard + ", colorDistanceMeasure=" + colorDistanceMeasure + ", upperBound="
				+ upperBound + ", subSampleRateX=" + subSampleRateX + ", subSampleRateY=" + subSampleRateY + "]";
	}

}
