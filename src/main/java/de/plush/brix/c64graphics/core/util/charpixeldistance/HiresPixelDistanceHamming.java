package de.plush.brix.c64graphics.core.util.charpixeldistance;

import static de.plush.brix.c64graphics.core.util.Utils.sumOf;

import de.plush.brix.c64graphics.core.model.C64Char;
import de.plush.brix.c64graphics.core.util.Utils;

/** Hamming distance: Number of different bits (very fast), normalized to a value from 0.0 to 1.0 (inclusive). */
public class HiresPixelDistanceHamming implements C64CharHiresPixelDistanceMeasure {

	@Override
	public double distanceOf(final C64Char c1, final C64Char c2) {
		return c1 == c2 || c1.equals(c2) ? 0 : 1d / 64 * sumOf(c1.toC64Binary(), c2.toC64Binary(), HiresPixelDistanceHamming::byteDistance);
	}

	private static int byteDistance(final byte a, final byte b) {
		if (a == b) { return 0; }
		return Utils.bitCount((byte) (a ^ b));
	}

}