package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;
import static de.plush.brix.c64graphics.core.util.UtilsImage.colors;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.HiEddiColorClashResolver;

public class ImageToC64BitmapmodePictureHires implements IPipelineStage<BufferedImage, C64BitmapmodePictureHires> {

	private final Function<BufferedImage, BufferedImage> colorClashResolver;

	private final Palette palette;

	private List<Color> preferredBackgroundColors = emptyList();

	private List<Color> preferredForegroundColors = emptyList();

	public ImageToC64BitmapmodePictureHires(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure) {
		this.palette = palette;
		colorClashResolver = new HiEddiColorClashResolver(colorDistanceMeasure);
	}

	public ImageToC64BitmapmodePictureHires(final Palette palette, final Function<BufferedImage, BufferedImage> colorClashResolver) {
		this.palette = palette;
		this.colorClashResolver = colorClashResolver;
	}

	public ImageToC64BitmapmodePictureHires withPreferredBackgroundColors(final Color... colors) {
		preferredBackgroundColors = asList(colors);
		preferredBackgroundColors.forEach(palette::colorCodeOrException);
		checkSettingsConsistency();
		return this;
	}

	public ImageToC64BitmapmodePictureHires withPreferredForegroundColors(final Color... colors) {
		preferredForegroundColors = asList(colors);
		preferredForegroundColors.forEach(palette::colorCodeOrException);
		checkSettingsConsistency();
		return this;
	}

	private void checkSettingsConsistency() {
		final Set<Color> x = preferredBackgroundColors.stream().filter(preferredForegroundColors::contains).collect(toSet());
		if (!x.isEmpty()) {
			throw new IllegalArgumentException("You can't force these colors to be both background and foreground: " + x);
		}
	}

	@Override
	public C64BitmapmodePictureHires apply(final BufferedImage original) {
		final BufferedImage sanitized = colorClashResolver.apply(original);

		final C64Bitmap bitmap = new C64Bitmap();
		final C64Screen screen = new C64Screen((byte) 0);

		final Dimension blocks = C64Bitmap.sizeBlocks();
		for (int y = 0; y < blocks.height; ++y) {
			for (int x = 0; x < blocks.width; ++x) {
				final int xPix = x * C64Char.WIDTH_PIXELS;
				final int yPix = y * C64Char.HEIGHT_PIXELS;
				final BufferedImage blockImage = sanitized.getSubimage(xPix, yPix, C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS);

				final List<Color> colors = colors(blockImage).stream().collect(toList());
				if (colors.size() == 1) {
					colors.add(palette.color((byte) 0)); // default
				}
				colors.sort(UtilsColor.SORT_BY_BRIGHTNESS); // to get some predictable patterns
				Color background = preferredBackgroundColors.stream().filter(colors::contains).findFirst().orElse(null);
				Color foreground = preferredForegroundColors.stream().filter(colors::contains).findFirst().orElse(null);
				Stream.of(background, foreground).filter(c -> c != null).forEach(colors::remove);
				// list of colors has no preferred color left, from here we can assign colors that are not preferred by anyone to empty slots
				final Iterator<Color> colorsLeft = colors.iterator();
				background = background == null ? colorsLeft.hasNext() ? colorsLeft.next() : palette.color((byte) 0) : background;
				foreground = foreground == null ? colorsLeft.hasNext() ? colorsLeft.next() : palette.color((byte) 0) : foreground;
				Stream.of(background, foreground).forEach(colors::remove);
				if (!colors.isEmpty()) {
					throw new IllegalStateException(
							"unexpectedly found more than 2 colors in block (x=" + x + ", y=" + y + "). Have you solved all color clashes?");
				}

				final C64Char block = new ImageToC64CharHires(background, foreground).apply(blockImage);
				bitmap.setBlock(x, y, block);
				screen.setScreenCode(x, y, toUnsignedByte(palette.colorCodeOrException(foreground) << 4 | palette.colorCodeOrException(background)));
			}
		}
		return new C64BitmapmodePictureHires(bitmap, screen);
	}

}
