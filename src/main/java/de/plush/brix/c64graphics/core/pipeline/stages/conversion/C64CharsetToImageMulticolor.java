package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.model.C64CharsetScreen;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.C64CharToImageMulticolor.Mode;

public class C64CharsetToImageMulticolor extends AbstractCharsetToImage {

	private final Supplier<Color> backgroundColorProvider;

	private final Palette palette;

	private final Color colorRamColor;

	private final Color mc1_D022;

	private final Color mc2_D023;

	public C64CharsetToImageMulticolor(final CharsetFormat grid) {
		this(grid, C64ColorsColodore.PALETTE, () -> BLACK.color, LIGHT_GRAY.color, GRAY.color, WHITE.d800ReplacementForMulticolorInCharmode().color());
	}

	/**
	 * <style> ulx > ul { margin-top: 0px; margin-bottom: 0px; } </style>
	 *
	 * @param grid
	 *            screen matrix generator to display different charsets.
	 * @param backgroundColorProvider
	 *            color for 00 bit pairs
	 * @param mc1_D022
	 *            color for 01 bit pairs, all C64 palette colors feasible
	 * @param mc2_D023
	 *            color for 11 bit pairs, all C64 palette colors feasible
	 * @param colorRamColor
	 *            color for 11 bit pairs<br>
	 *            <ulx>
	 *            <li>In {@link Mode#Bitmap Char mode} only be the colors for color codes $00 to $07 (included) are displayed and bit 3 of the color code
	 *            toggles between multicolor and hires (i.e: $07/yellow stays yellow in a hires resolution character, $0F/light gray becomes yellow in a
	 *            multicolor resolution character).<br>
	 *            </ulx>
	 */
	public C64CharsetToImageMulticolor(final CharsetFormat grid, final Palette palette, final Supplier<Color> backgroundColorProvider, final Color mc1_D022,
			final Color mc2_D023, final Color colorRamColor) {
		super(grid);
		this.palette = palette;
		this.backgroundColorProvider = backgroundColorProvider;
		this.colorRamColor = palette.colorThatReplacesThisColorInD800MulticolorCharmode(colorRamColor)
				.orElseThrow(() -> new IllegalArgumentException("the colorRam (d800) color " + colorRamColor
						+ " cannot be displayed in multicolor charmode. Only the first 8 colors of the palette are permitted."));
		this.mc1_D022 = mc1_D022;
		this.mc2_D023 = mc2_D023;
	}

	@Override
	protected IPipelineStage<C64CharsetScreen, BufferedImage> createCharsetScreenToImage() {
		return new C64CharsetScreenToImageMulticolor(palette, backgroundColorProvider, mc1_D022, mc2_D023, colorRamColor);
	}
}
