package de.plush.brix.c64graphics.core.util.collections;

import static de.plush.brix.c64graphics.core.util.functions.BytePredicate.negated;
import static java.lang.Math.*;
import static java.util.stream.Collectors.toList;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.*;

import de.plush.brix.c64graphics.core.util.*;
import de.plush.brix.c64graphics.core.util.functions.*;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

public class ByteArrayList {

	private static final int MAX_ARRAY_LENGTH = Integer.MAX_VALUE - 8;

	private static final int DEFAULT_CAPACITY = 10;

	private byte[] elementData;

	int size;

	public ByteArrayList() {
		this(DEFAULT_CAPACITY);
	}

	public ByteArrayList(final int initialCapacity) {
		elementData = new byte[initialCapacity];
	}

	public ByteArrayList(final byte... bytes) {
		elementData = Arrays.copyOf(bytes, bytes.length);
		size = bytes.length;
	}

	public ByteArrayList(final ByteArrayList bytes) {
		// we can't delegate to the "byte..." constructor, as the array may be larger than the actual size of the list
		elementData = Arrays.copyOf(bytes.elementData, bytes.size);
		size = bytes.size;
	}

	public static ByteArrayList empty() {
		return new ByteArrayList();
	}

	public static ByteArrayList of(final byte... bytes) {
		return new ByteArrayList(bytes);
	}

	public static ByteArrayList of(final ByteArrayList bytes) {
		return new ByteArrayList(bytes);
	}

	public static ByteArrayList ofNValues(final int n, final byte value) {
		final var list = new ByteArrayList(n);
		Arrays.fill(list.elementData, value);
		list.size = n;
		return list;
	}

	public byte get(final int index) {
		Objects.checkIndex(index, size);
		return uncheckedGet(index);
	}

	public byte set(final int index, final byte element) {
		Objects.checkIndex(index, size);
		final byte oldValue = uncheckedGet(index);
		uncheckedSet(index, element);
		return oldValue;
	}

	byte uncheckedGet(final int index) {
		return elementData[index];
	}

	void uncheckedSet(final int index, final byte element) {
		elementData[index] = element;
	}

	public boolean add(final byte e) {
		ensureCapacityForOneMore();
		elementData[size++] = e;
		return true;
	}

	public void add(final int index, final byte element) {
		if (index == size) {
			add(element);
		} else {
			Objects.checkIndex(index, size + 1);
			ensureCapacityForOneMore();
			System.arraycopy(elementData, index, elementData, index + 1, size - index);
			elementData[index] = element;
			++size;
		}
	}

	public void addAll(final byte... elements) {
		ensureCapacityOfAtLeast(size + elements.length);
		System.arraycopy(elements, 0, elementData, size, elements.length);
		size += elements.length;
	}

	public void addAll(final int index, final byte... elements) {
		Objects.checkIndex(index, size + 1);
		if (index == size) {
			addAll(elements);
		}
		ensureCapacityOfAtLeast(size + elements.length);
		System.arraycopy(elementData, index, elementData, index + elements.length, size - index);
		System.arraycopy(elements, 0, elementData, index, elements.length);
		size += elements.length;
	}

	public void addAll(final ByteArrayList other) {
		addAll(other.toArray());
		// TODO: inefficient, as it copies once too often, but ok for the moment
	}

	public void addAll(final int index, final ByteArrayList other) {
		addAll(index, other.toArray());
		// TODO: inefficient, as it copies once too often, but ok for the moment
	}

	public byte removeLast() {
		if (size == 0) { throw new ArrayIndexOutOfBoundsException("Cannot remove from empty list"); }
		return elementData[--size];
	}

	public void removeLastN(final int amount) {
		if (size == 0) { throw new ArrayIndexOutOfBoundsException("Cannot remove from empty list"); }
		if (size < amount) { throw new ArrayIndexOutOfBoundsException("Cannot remove more elements than the list contains"); }
		size -= amount;
	}

	public byte remove(final int index) {
		if (index == size - 1) { return removeLast(); }
		if (size == 0) { throw new ArrayIndexOutOfBoundsException("Cannot remove from empty list"); }
		Objects.checkIndex(index, size);
		final byte removed = elementData[index];
		System.arraycopy(elementData, index + 1, elementData, index, --size - index);
		return removed;
	}

	public void removeN(final int index, final int amount) {
		if (index + amount == size) {
			removeLastN(amount);
			return;
		}
		if (size < amount) { throw new ArrayIndexOutOfBoundsException("Cannot remove more elements than the list contains"); }
		Objects.checkFromToIndex(index, index + amount, size);
		System.arraycopy(elementData, index + amount, elementData, index, (size -= amount) - index);
	}


	public boolean contains(final byte b) {
		return anySatisfy(candidate -> candidate == b);
	}

	/** Same as {@link #select(BytePredicate)}, but for those who are used to java.util lingo */
	public ByteArrayList filter(final BytePredicate predicate) {
		return select(predicate);
	}

	/**
	 * Returns a new ByteArrayList with all of the elements in the ByteArrayList that return true for the specified predicate.
	 */
	public ByteArrayList select(final BytePredicate predicate) {
		final var result = new ByteArrayList(size());
		if (isEmpty()) { return result; }
		indexStream().filter(index -> predicate.accept(uncheckedGet(index)))//
				.forEach(index -> result.add(uncheckedGet(index)));
		// there's probably a more efficient way, by making bulk-adds, but this should be sufficient for a start
		return result;
	}

	/**
	 * @returns a new {@link List} formed from {@link ByteArrayList} and another {@link List} by combining corresponding elements in pairs.
	 *          If one of the two Lists is longer than the other, its remaining elements are ignored.
	 */
	public <T> List<ByteObjectPair<T>> zip(final List<T> other) {
		final var otherIndex = new AtomicInteger(-1);
		return indexStream()//
				.limit(min(size(), other.size())) //
				.mapToObj(index -> ByteObjectPair.of(uncheckedGet(index), other.get(otherIndex.incrementAndGet())))//
				.collect(toList());
	}

	/**
	 * Returns a new ByteArrayList with all of the elements in the ByteArrayList that return false for the specified predicate.
	 */
	public ByteArrayList reject(final BytePredicate predicate) {
		return select(negated(predicate));
	}

	public void forEach(final ByteProcedure procedure) {
		indexStream().forEach(index -> procedure.value(uncheckedGet(index)));
	}

	public IntStream mapToInt(final ByteToIntFunction function) {
		return indexStream().map(index -> function.valueOf(uncheckedGet(index)));
	}

	public <T> Stream<T> mapToObj(final ByteToObjectFunction<? extends T> function) {
		return indexStream().mapToObj(index -> function.valueOf(uncheckedGet(index)));
	}

	public boolean anySatisfy(final BytePredicate predicate) {
		if (isEmpty()) { return false; }
		return indexStream().anyMatch(index -> predicate.accept(uncheckedGet(index)));//
	}

	public boolean allSatisfy(final BytePredicate predicate) {
		if (isEmpty()) { return false; }
		return indexStream().allMatch(index -> predicate.accept(uncheckedGet(index)));//
	}

	public boolean noneSatisfy(final BytePredicate predicate) {
		if (isEmpty()) { return true; }
		return indexStream().noneMatch(index -> predicate.accept(uncheckedGet(index)));//
	}

	public int size() {
		return size;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + size;
		result = prime * result + Utils.hashCodeOfRegion(elementData, 0, size);
		return result;
	}

	@SuppressFBWarnings(value = "EQ_GETCLASS_AND_CLASS_CONSTANT", justification = "Just incorporate Sublist and nothing else")
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (obj.getClass() != ByteArrayList.class && obj.getClass() != ByteSubList.class) { return false; }
		final ByteArrayList other = (ByteArrayList) obj;
		if (!equalsRange(other, 0, size)) { return false; }
		return true;
	}

	public byte[] toArray() {
		return Arrays.copyOfRange(elementData, 0, size);
	}

	public void sort() {
		Arrays.sort(elementData, 0, size);
	}

	public void sortUnsigend() {
		Utils.sortUnsigned(elementData, 0, size);
	}

	@Override
	public String toString() {
		final var s = mapToObj(Utils::toHexString).collect(Collectors.joining(","));
		return "ByteArrayList [bytes=" + s + ", size=" + size + "]";
	}

	public ByteArrayList subList(final int fromIndex, final int toIndex) {
		Objects.checkFromToIndex(fromIndex, toIndex, size);
		return new ByteSubList(this, fromIndex, toIndex);
	}

	IntStream indexStream() {
		return IntStream.range(0, size);
	}

	boolean equalsRange(final ByteArrayList other, final int from, final int to) {
		if (other instanceof ByteSubList) { return equalsRange((ByteSubList) other, from, to); }
		if (other.size != to - from) { return false; }
		Objects.checkFromToIndex(from, to, size);
		return Arrays.equals(elementData, from, to, other.elementData, 0, to - from);
	}

	boolean equalsRange(final ByteSubList other, final int from, final int to) {
		if (other.size != to - from) { return false; }
		Objects.checkFromToIndex(from, to, size);
		return Arrays.equals(elementData, from, to, other.backingList.elementData, other.offset, other.offset + to - from);
	}

	void ensureCapacityForOneMore() {
		ensureCapacityOfAtLeast(size + 1);
	}

	void ensureCapacityOfAtLeast(final int minCapacity) {
		if (minCapacity > elementData.length) {
			final int oldCapacity = elementData.length;
			final var minGrowth = minCapacity - oldCapacity;
			final var preferredGrowth = oldCapacity >> 1;
			final int newCapacity = newCapacity(oldCapacity, minGrowth, preferredGrowth);
			// System.out.println("capacity: " + newCapacity + " = " + String.format("%.2f %%", (double) newCapacity / (double) Integer.MAX_VALUE * 100.0));
			elementData = Arrays.copyOf(elementData, newCapacity);
		}
	}

	private static int newCapacity(final int currentCapacity, final int minGrowth, final int preferredGrowth) {
		int shifts = 0;
		do {
			try {
				return StrictMath.addExact(max(minGrowth, preferredGrowth >> shifts), currentCapacity);
			} catch (final ArithmeticException e1) {
				shifts += 1;
			}
		} while (shifts <= 2);
		if (currentCapacity >= MAX_ARRAY_LENGTH) { throw new OutOfMemoryError("Array list full."); }
		return MAX_ARRAY_LENGTH;
	}

	// attention inheritance will inherit null array. Any get/set/add/remove/grow must be redirected to backing list.
	@SuppressFBWarnings(value = "EQ_DOESNT_OVERRIDE_EQUALS", //
			justification = "The superclass equals method is tailored to work its own class and this class only")
	private static final class ByteSubList extends ByteArrayList {

		private final ByteArrayList backingList;

		private final ByteSubList parent;

		private final int offset;

		@SuppressWarnings("unused")
		@Deprecated
		private byte[] elementData; // shadowing is intentional

		/** SubList backed by ByteArrayList. */
		private ByteSubList(final ByteArrayList backingList, final int fromIndex, final int toIndex) {
			this.backingList = backingList;
			parent = null;
			offset = fromIndex;
			size = toIndex - fromIndex;
		}

		/** SubList backed by another Sublist. */
		private ByteSubList(final ByteSubList parent, final int fromIndex, final int toIndex) {
			backingList = parent.backingList;
			this.parent = parent;
			offset = parent.offset + fromIndex;
			size = toIndex - fromIndex;
		}

		@Override
		public byte get(final int index) {
			Objects.checkIndex(index, size);
			return uncheckedGet(offset + index);
		}

		@Override
		public byte set(final int index, final byte element) {
			Objects.checkIndex(index, size);
			final byte oldValue = uncheckedGet(offset + index);
			uncheckedSet(offset + index, element);
			return oldValue;
		}

		@Override
		byte uncheckedGet(final int index) {
			return backingList.elementData[index];
		}

		@Override
		void uncheckedSet(final int index, final byte element) {
			backingList.elementData[index] = element;
		}

		@Override
		public boolean add(final byte element) {
			add(size, element);
			return true;
		}

		@Override
		public void add(final int index, final byte element) {
			Objects.checkIndex(index, size + 1);
			backingList.add(offset + index, element);
			updateSizeCount(1);
		}

		@Override
		public void addAll(final byte... elements) {
			backingList.addAll(offset + size, elements);
			updateSizeCount(elements.length);
		}

		@Override
		public void addAll(final int index, final byte... elements) {
			Objects.checkIndex(index, size + 1);
			backingList.addAll(offset + index, elements);
			updateSizeCount(elements.length);
		}

		@Override
		public byte removeLast() {
			return remove(size - 1);
		}

		@Override
		public byte remove(final int index) {
			Objects.checkIndex(index, size);
			final byte result = backingList.remove(offset + index);
			updateSizeCount(-1);
			return result;
		}

		@Override
		public byte[] toArray() {
			return Arrays.copyOfRange(backingList.elementData, offset, offset + size);
		}

		@Override
		public void sort() {
			Arrays.sort(backingList.elementData, offset, offset + size);
		}

		@Override
		public void sortUnsigend() {
			Utils.sortUnsigned(backingList.elementData, offset, offset + size);
		}

		@Override
		public ByteArrayList subList(final int fromIndex, final int toIndex) {
			Objects.checkFromToIndex(fromIndex, toIndex, size);
			return new ByteSubList(this, fromIndex, toIndex);
		}

		@SuppressWarnings("checkstyle:EqualsHashCode") // equals can be inherited in this special case
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + size;
			result = prime * result + Utils.hashCodeOfRegion(backingList.elementData, offset, offset + size);
			return result;
		}

		@Override
		IntStream indexStream() {
			return IntStream.range(offset, offset + size);
		}

		@Override
		boolean equalsRange(final ByteArrayList other, final int from, final int to) {
			if (other instanceof ByteSubList) { return equalsRange((ByteSubList) other, from, to); }
			if (other.size != to - from) { return false; }
			Objects.checkFromToIndex(from, to, size);
			return Arrays.equals(backingList.elementData, from + offset, to + offset, other.elementData, 0, to - from);
		}

		@Override
		boolean equalsRange(final ByteSubList other, final int from, final int to) {
			if (other.size != to - from) { return false; }
			Objects.checkFromToIndex(from, to, size);
			return Arrays.equals(backingList.elementData, from + offset, to + offset, //
					other.backingList.elementData, other.offset, other.offset + to - from);
		}

		private void updateSizeCount(final int sizeChange) {
			ByteSubList subList = this;
			do {
				subList.size += sizeChange;
				subList = subList.parent;
			} while (subList != null);
		}

	}

}
