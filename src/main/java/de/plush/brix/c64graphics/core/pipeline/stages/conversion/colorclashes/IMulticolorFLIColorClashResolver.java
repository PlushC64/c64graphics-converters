package de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes;

import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.PreProcessedMulticolorFliImage;

public interface IMulticolorFLIColorClashResolver extends IPipelineStage<BufferedImage, PreProcessedMulticolorFliImage>, D800PreferenceSupplier {
	// interface to combine single interfaces
}