package de.plush.brix.c64graphics.core.model;

import java.net.*;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.util.*;

/**
 * This is what makes up a typical Fli Image, as used by FBI Crews FLI Designer.
 *
 * @author Wanja Gayk
 */
public class C64BitmapmodePictureMulticolorFli extends C64BitmapmodePictureMulticolorNscreenFLI {

	private static final int SCREEN_COUNT = C64Char.HEIGHT_PIXELS;

	public C64BitmapmodePictureMulticolorFli(final C64Bitmap bitmap, final C64Screen[] screens) {
		this(bitmap, screens, new C64ColorRam((byte) 0));
	}

	public C64BitmapmodePictureMulticolorFli(final C64BitmapmodePictureMulticolor bitmapModePic, final C64ColorRam colorRam) {
		this(bitmapModePic.bitmap(), //
				IntStream.range(0, SCREEN_COUNT)//
						.mapToObj(__ -> bitmapModePic.screen())//
						.toArray(C64Screen[]::new), colorRam, //
				bitmapModePic.backgroundColorCode());
	}

	public C64BitmapmodePictureMulticolorFli(final C64BitmapmodePictureHiresFli bitmapScreens, final C64ColorRam colorRam) {
		this(bitmapScreens, colorRam, (byte) 0);
	}

	public C64BitmapmodePictureMulticolorFli(final C64BitmapmodePictureHiresFli bitmapScreens, final C64ColorRam colorRam, final byte backgroundColorCode) {
		this(bitmapScreens.bitmap(), bitmapScreens.screens(), colorRam, backgroundColorCode);
	}

	public C64BitmapmodePictureMulticolorFli(final C64Bitmap bitmap, final C64Screen[] screens, final C64ColorRam colorRam) {
		this(bitmap, screens, colorRam, (byte) 0);
	}

	public C64BitmapmodePictureMulticolorFli(final C64Bitmap bitmap, final C64Screen[] screens, final C64ColorRam colorRam, final byte backgroundColorCode) {
		super(SCREEN_COUNT, bitmap, screens, colorRam, backgroundColorCode);
	}

	public C64BitmapmodePictureMulticolorFli(final C64BitmapmodePictureMulticolorFli other) {
		super(SCREEN_COUNT, other.bitmapScreen, other.colorRam, other.backgroundColorCode);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!super.equals(obj)) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		return true;
	}

	public static C64BitmapmodePictureMulticolorFli loadFliDesignerFbiCrew(final URL loc, final StartAddress startAddress) {
		return fliDesignerFbiCrewFormat().load(Utils.readFile(loc, startAddress));
	}

	public void saveFliDesignerFbiCrew(final URI loc, final StartAddress startAddress) {
		fliDesignerFbiCrewFormat().save(binaryProviderFliDesignerFbiCrew(), loc, startAddress);
	}

	public static C64BitmapmodePictureMulticolorFli loadFliDesignerFbiCrew(final URI loc, final StartAddress startAddress) {
		return fliDesignerFbiCrewFormat().load(Utils.readFile(loc, startAddress));
	}

	public static C64BitmapmodePictureMulticolorFli fromFliDesignerFbiCrewBinary(final byte[] binary) {
		return fliDesignerFbiCrewFormat().load(binary);
	}

	/**
	 * From <a href=
	 * "http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03">http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03</a>:<br>
	 * <style> #gfxformat th { background:lightgray; text-align: left; width:auto}</style> <style> #gfxformat td.first { width:18ch; }</style><style> #gfxformat
	 * td { width:auto}</style>
	 * <table id="gfxformat">
	 * <th colspan="2">FLI Designer 1.1 &amp; 2.0 (by FBI Crew) (pc-ext: .fd2)</th>
	 * <tr>
	 * <td class="first">3c00 - $7ffe</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$3c00 - $3fe7</td>
	 * <td>Color RAM</td>
	 * <tr>
	 * <td class="first">$4000 - $5fe7</td>
	 * <td>Screen RAMs</td>
	 * <tr>
	 * <td class="first">$6000 - $7f3f</td>
	 * <td>Bitmap</td>
	 * </table>
	 *
	 * @return A provider that will provide the C64 binary of this image at the time this method was invoked.
	 */
	public C64BinaryProvider binaryProviderFliDesignerFbiCrew() {
		return fliDesignerFbiCrewFormat().binaryProvider(this);
	}

	private static Format fliDesignerFbiCrewFormat() {
		return (Format) new Format()//
				.withLoadAddress(0x3c00, 0x7ffe)//
				.withCramAddress(0x3c00) //
				.withScreensAddress(0x4000) //
				.withBitmapAddress(0x6000);
	}

	public static C64BitmapmodePictureMulticolorFli loadFliGraphBlackmail(final URL loc, final StartAddress startAddress) {
		return fliGraphBlackmailFormat().load(Utils.readFile(loc, startAddress));
	}

	public static C64BitmapmodePictureMulticolorFli loadFliGraphBlackmail(final URI loc, final StartAddress startAddress) {
		return fliGraphBlackmailFormat().load(Utils.readFile(loc, startAddress));
	}

	public void saveFliGraphBlackmail(final URI loc, final StartAddress startAddress) {
		fliGraphBlackmailFormat().save(binaryProviderFliGraphBlackmail(), loc, startAddress);
	}

	public static C64BitmapmodePictureMulticolorFli fromFliGraphBlackmailBinary(final byte[] binary) {
		return fliGraphBlackmailFormat().load(binary);
	}

	/**
	 * From <a href=
	 * "http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03">http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03</a>:<br>
	 * <style> #gfxformat th { background:lightgray; text-align: left; width:auto}</style> <style> #gfxformat td.first { width:18ch; }</style><style> #gfxformat
	 * td { width:auto}</style>
	 * <table id="gfxformat">
	 * <th colspan="2">FLI Graph 2.2 (by blackmail) (pc-ext: .bml)</th>
	 * <tr>
	 * <td class="first">3b00 - $7ffe</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$3c00 - $3bc7</td>
	 * <td>$d021 colors (ignored)</td>
	 * <tr>
	 * <td class="first">$3c00 - $3fe7</td>
	 * <td>Color RAM</td>
	 * <tr>
	 * <td class="first">$4000 - $5fe7</td>
	 * <td>Screen RAMs</td>
	 * <tr>
	 * <td class="first">$6000 - $7f3f</td>
	 * <td>Bitmap</td>
	 * </table>
	 *
	 * @return A provider that will provide the C64 binary of this image at the time this method was invoked.
	 */
	public C64BinaryProvider binaryProviderFliGraphBlackmail() {
		return fliGraphBlackmailFormat().binaryProvider(this);
	}

	private static Format fliGraphBlackmailFormat() {
		return (Format) new Format()//
				.withLoadAddress(0x3b00, 0x7f3f)//
				.withCramAddress(0x3c00) //
				.withScreensAddress(0x4000) //
				.withBitmapAddress(0x6000);
	}

	public static C64BitmapmodePictureMulticolorFli loadFliDesigner2(final URL loc, final StartAddress startAddress) {
		return fliDesigner2Format().load(Utils.readFile(loc, startAddress));
	}

	public static C64BitmapmodePictureMulticolorFli loadFliDesigner2(final URI loc, final StartAddress startAddress) {
		return fliDesigner2Format().load(Utils.readFile(loc, startAddress));
	}

	public void saveFliDesigner2(final URI loc, final StartAddress startAddress) {
		fliDesigner2Format().save(binaryProviderFliDesigner2(), loc, startAddress);
	}

	public static C64BitmapmodePictureMulticolorFli fromFliDesigner2Binary(final byte[] binary) {
		return fliDesigner2Format().load(binary);
	}

	/**
	 * From <a href=
	 * "http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03">http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03</a>:<br>
	 * <style> #gfxformat th { background:lightgray; text-align: left; width:auto}</style> <style> #gfxformat td.first { width:18ch; }</style><style> #gfxformat
	 * td { width:auto}</style>
	 * <table id="gfxformat">
	 * <th colspan="2">FLI Designer 2 (pc-ext: .fli)</th>
	 * <tr>
	 * <td class="first">$3ff0 - $83ee</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$3ff0 - $43d7</td>
	 * <td>Color RAM</td>
	 * <tr>
	 * <td class="first">$43f0 - $63d7</td>
	 * <td>Screen RAMs</td>
	 * <tr>
	 * <td class="first">$63f0 - $832f</td>
	 * <td>Bitmap</td>
	 * </table>
	 *
	 * @return A provider that will provide the C64 binary of this image at the time this method was invoked.
	 */
	public C64BinaryProvider binaryProviderFliDesigner2() {
		return fliDesigner2Format().binaryProvider(this);
	}

	private static Format fliDesigner2Format() {
		return (Format) new Format()//
				.withLoadAddress(0x3ff0, 0x83ee)//
				.withCramAddress(0x3ff0) //
				.withScreensAddress(0x43f0) //
				.withBitmapAddress(0x63f0);
	}

	private static class Format extends C64MulticolorFliFormat<C64BitmapmodePictureMulticolorFli> {
		C64BitmapmodePictureMulticolorFli load(final byte[] binary) {
			return super.load(binary, SCREEN_COUNT, C64BitmapmodePictureMulticolorFli::new);
		}
	}
}
