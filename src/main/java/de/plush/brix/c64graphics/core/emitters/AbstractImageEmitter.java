package de.plush.brix.c64graphics.core.emitters;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.WillCloseWhenClosed;

import de.plush.brix.c64graphics.core.pipeline.ImageConsumer;
import de.plush.brix.c64graphics.core.util.Utils;

public abstract class AbstractImageEmitter<T> implements Runnable, AutoCloseable {

	private final List<ImageConsumer<? super T>> consumers = new LinkedList<>();

	private final AtomicBoolean closed = new AtomicBoolean();

	/**
	 * This method
	 * <ol>
	 * <li>Is expected to block the current thread until all images are emitted.
	 * <li>is expected to close the emission after all images are emitted.
	 * </ol>
	 */
	@Override
	public abstract void run();

	public void add(@WillCloseWhenClosed final ImageConsumer<? super T> consumer) {
		consumers.add(consumer);
	}

	protected final void emit(final T image) {
		if (!closed.get()) {
			for (final ImageConsumer<? super T> consumer : consumers) {
				consumer.onImage(image);
			}
		}
	}

	protected final void closeEmission() {
		if (closed.compareAndSet(false, true)) { // prohibits double close
			Utils.close(consumers);
		}
	}

	@Override
	public final void close() {
		if (closed.compareAndSet(false, true)) { // prohibits double close
			closeEmission();
		}
	}

}
