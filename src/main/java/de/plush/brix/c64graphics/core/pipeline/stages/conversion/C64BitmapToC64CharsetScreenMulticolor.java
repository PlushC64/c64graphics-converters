package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.util.List;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.ColorDistanceMeasure;
import de.plush.brix.c64graphics.core.util.collections.HashBag;

public class C64BitmapToC64CharsetScreenMulticolor extends AbstractC64BitmapToC64CharsetScreen {

	public C64BitmapToC64CharsetScreenMulticolor(final ColorDistanceMeasure distanceMeasure) {
		super(distanceMeasure);
	}

	// TODO: use real colors for better precision
	@Override
	protected Function<HashBag<C64Char>, List<C64Char[]>> createCharacterReplacer() {
		return new ReduceNumberOfUniformlyColoredCharactersMulticolor(C64Charset.MAX_CHARACTERS, distanceMeasure);
	}

}
