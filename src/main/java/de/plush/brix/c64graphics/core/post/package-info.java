
/**
 * This package contains processing steps that are not part of the usual pipeline, but are meant to happen when an emitter has closed emitting frames.<br>
 * They typically work on a number of frames rather than on a single one.
 */
package de.plush.brix.c64graphics.core.post;
