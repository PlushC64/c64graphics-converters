package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import static java.lang.Math.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.function.*;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.ColorDistanceMeasure;

/**
 * <p>
 * An efficient color quantization algorithm using OctTrees. The pixels for
 * an image are placed into an oct tree. The oct tree is reduced in
 * size, and the pixels from the original image are reassigned to the
 * nodes in the reduced tree.
 * </p>
 * <p>
 * Based upon the LightZone sources.
 * Added a feature to inject own color distance measures.
 * </p>
 * 
 * @author Wanja Gayk
 * 
 * @see
 *      <a href=
 *      "https://github.com/AntonKast/LightZone/blob/master/lightcrafts/extsrc/com/lightcrafts/media/jai/opimage/OctTreeOpImage.java">OctTreeOpImage.java
 *      at LightZone</a>
 * @implNote
 *           See additional copyright notices at the end of this source code.
 * 
 */
class OcttreeQuantizeImpl implements Function<BufferedImage, BufferedImage> {

	private final int maxColors;

	private final Shape roi;

	private final int xPeriod;

	private final int yPeriod;

	/** The size of the histogram. */
	private final int treeSize;

	private final int maxTreeDepth = 8;

	private final ColorDistanceMeasure colorDistanceMeasure;

	OcttreeQuantizeImpl(final ColorDistanceMeasure colorDistanceMeasure, final int maxColorNum, final Integer maxTreeSize, final Shape roi,
			final int xSampleRate, final int ySampleRate) {
		this.colorDistanceMeasure = colorDistanceMeasure;
		maxColors = maxColorNum;
		treeSize = maxTreeSize == null ? 2 << 16 : maxTreeSize.intValue(); // 16 bits
		this.roi = roi;
		xPeriod = xSampleRate;
		yPeriod = ySampleRate;
	}

	@Override
	public BufferedImage apply(final BufferedImage originalImage) {

		final OctTree octTree = new OctTree(originalImage);
		final var colorSet = octTree.colors();
		final var result = new BufferedImage(originalImage.getWidth(), originalImage.getHeight(), originalImage.getType());
		for (int y = 0; y < result.getHeight(); ++y) {
			for (int x = 0; x < result.getWidth(); ++x) {
				final var sourceColor = new Color(originalImage.getRGB(x, y));
				final Color closestColor = Arrays.stream(colorSet).min(colorDistanceMeasure.comparingDistanceTo(sourceColor)).get();
				result.setRGB(x, y, closestColor.getRGB());
			}
		}
		return result;
	}

	class OctTree {
		private final BufferedImage originalImage;

		private final Node root;

		private int depth; // counter for the number of levels in the tree

		private int nodes;

		// counter for the (current) number of colors in the cube. This gets recalculated while the tree is being built.
		private int colors;

		private Color[] colormap;

		OctTree(final BufferedImage originalImage) {
			this.originalImage = originalImage;
			depth = (int) (log(maxColors) / log(2)); // tree depth = log2(max_colors)
			depth = max(2, min(depth, maxTreeDepth)); // clamp value

			root = constructTree();
			reduce(root);
			buildColorMap(root);
		}

		Node constructTree() {
			final Rectangle imageBounds = new Rectangle(originalImage.getWidth(), originalImage.getHeight());
			var roi = OcttreeQuantizeImpl.this.roi;
			if (roi == null) {
				roi = imageBounds;
			}
			// Process if and only if within ROI bounds.
			if (roi.intersects(imageBounds)) {
				final var intersection = imageBounds.intersection(roi.getBounds());
				return constructTree(intersection);
			}
			throw new IllegalArgumentException("Region of interest " + roi + " does not intersect with image bounds " + imageBounds);
		}

		/*
		 * Procedure Classification begins by initializing a color
		 * description tree of sufficient depth to represent each
		 * possible input color in a leaf. However, it is impractical
		 * to generate a fully-formed color description tree in the
		 * classification phase for realistic values of cmax. If
		 * colors components in the input image are quantized to k-bit
		 * precision, so that cmax= 2k-1, the tree would need k levels
		 * below the root node to allow representing each possible
		 * input color in a leaf. This becomes prohibitive because the
		 * tree's total number of nodes is 1 + sum(i=1,k,8k).
		 * A complete tree would require 19,173,961 nodes for k = 8,
		 * cmax = 255. Therefore, to avoid building a fully populated
		 * tree, QUANTIZE: (1) Initializes data structures for nodes
		 * only as they are needed; (2) Chooses a maximum depth for
		 * the tree as a function of the desired number of colors in
		 * the output image (currently log2(colormap size)).
		 * For each pixel in the input image, classification scans
		 * downward from the root of the color description tree. At
		 * each level of the tree it identifies the single node which
		 * represents a cube in RGB space containing It updates the
		 * following data for each such node:
		 * number_pixels : Number of pixels whose color is contained
		 * in the RGB cube which this node represents;
		 * unique : Number of pixels whose color is not represented
		 * in a node at lower depth in the tree; initially, n2 = 0
		 * for all nodes except leaves of the tree.
		 * total_red/green/blue : Sums of the red, green, and blue
		 * component values for all pixels not classified at a lower
		 * depth. The combination of these sums and n2 will
		 * ultimately characterize the mean color of a set of pixels
		 * represented by this node.
		 */
		private Node constructTree(final Shape roi) {
			final Node root = new Node(this);
			final var rect = roi.getBounds();
			for (int y = rect.y; y < rect.height; y += yPeriod) {
				for (int x = rect.x; x < rect.width; x += xPeriod) {
					if (roi.contains(x, y)) {
						final Color color = new Color(originalImage.getRGB(x, y));
						final int red = color.getRed();
						final int green = color.getGreen();
						final int blue = color.getBlue();
						// a hard limit on the number of nodes in the tree
						if (nodes > treeSize) {
							root.pruneLowestLevel();
							--depth;
						}
						// walk the tree to depth, increasing the
						// number_pixels count for each node
						Node node = root;
						for (int level = 1; level <= depth; ++level) {
							final int id = (red > node.mid_red ? 1 : 0) | (green > node.mid_green ? 1 : 0) << 1 | (blue > node.mid_blue ? 1 : 0) << 2;
							if (node.child[id] == null) {
								node = new Node(node, id, level);
							} else {
								node = node.child[id];
							}
							node.number_pixels++;
						}

						++node.unique;
						node.total_red += red;
						node.total_green += green;
						node.total_blue += blue;
					}
				}
			}
			return root;
		}

		/*
		 * reduction repeatedly prunes the tree until the number of
		 * nodes with unique > 0 is less than or equal to the maximum
		 * number of colors allowed in the output image.
		 * When a node to be pruned has offspring, the pruning
		 * procedure invokes itself recursively in order to prune the
		 * tree from the leaves upward. The statistics of the node
		 * being pruned are always added to the corresponding data in
		 * that node's parent. This retains the pruned node's color
		 * characteristics for later averaging.
		 */
		void reduce(final Node root) {
			final int totalSamples = (originalImage.getWidth() + xPeriod - 1) / xPeriod * (originalImage.getHeight() + yPeriod - 1) / yPeriod;
			int threshold = Math.max(1, totalSamples / (maxColors * 8));
			while (colors > maxColors) {
				colors = 0; // start color index at 0 for counting the number of colors
				threshold = root.reduce(threshold, Integer.MAX_VALUE); // sets this Octtree's color counter
			}
		}

		/*
		 * Procedure assignment generates the output image from the
		 * pruned tree. The output image consists of two parts: (1) A
		 * color map, which is an array of color descriptions (RGB
		 * triples) for each color present in the output image; (2) A
		 * pixel array, which represents each pixel as an index into
		 * the color map array.
		 * First, the assignment phase makes one pass over the pruned
		 * color description tree to establish the image's color map.
		 * For each node with n2 > 0, it divides Sr, Sg, and Sb by n2.
		 * This produces the mean color of all pixels that classify no
		 * lower than this node. Each of these colors becomes an entry
		 * in the color map.
		 * Finally, the assignment phase reclassifies each pixel in
		 * the pruned tree to identify the deepest node containing the
		 * pixel's color. The pixel's value in the pixel array becomes
		 * the index of this node's mean color in the color map.
		 */
		void buildColorMap(final Node root) {
			colormap = new Color[colors]; // using number of colors after reduction
			colors = 0; // start color index at 0 for filling the color map.
			root.colormap();
		}

		Color[] colors() {
			return colormap;
			// return IntStream.range(0, colormap[0].length)//
			// .mapToObj(index -> new Color(toUnsignedInt(colormap[0][index]), toUnsignedInt(colormap[1][index]), toUnsignedInt(colormap[2][index])))//
			// .toArray(Color[]::new);
		}

		/**
		 * A single Node in the tree.
		 */
		class Node {
			OctTree octTree;

			// parent node
			Node parent;

			// child nodes
			Node[] child;

			int nchild;

			// our index within our parent
			int id;

			// our level within the tree
			int level;

			// our color midpoint
			int mid_red;

			int mid_green;

			int mid_blue;

			// the pixel count for this node and all children
			int number_pixels;

			// the pixel count for this node
			int unique;

			// the sum of all pixels contained in this node
			int total_red;

			int total_green;

			int total_blue;

			// used to build the colormap
			int color_number;

			Node(final OctTree cube) {
				octTree = cube;
				parent = this;
				child = new Node[8];
				id = 0;
				level = 0;

				number_pixels = Integer.MAX_VALUE;

				mid_red = maxColors + 1 >> 1;
				mid_green = maxColors + 1 >> 1;
				mid_blue = maxColors + 1 >> 1;
			}

			Node(final Node parent, final int id, final int level) {
				octTree = parent.octTree;
				this.parent = parent;
				child = new Node[8];
				this.id = id;
				this.level = level;

				// add to the cube
				++octTree.nodes;
				if (level == octTree.depth) {
					++octTree.colors;
				}

				// add to the parent
				++parent.nchild;
				parent.child[id] = this;

				// figure out our midpoint
				final int bi = 1 << maxTreeDepth - level >> 1;
				mid_red = parent.mid_red + ((id & 1) > 0 ? bi : -bi);
				mid_green = parent.mid_green + ((id & 2) > 0 ? bi : -bi);
				mid_blue = parent.mid_blue + ((id & 4) > 0 ? bi : -bi);
			}

			/**
			 * Remove this child node, and make sure our parent
			 * absorbs our pixel statistics.
			 */
			void pruneThis() {
				--parent.nchild;
				parent.unique += unique;
				parent.total_red += total_red;
				parent.total_green += total_green;
				parent.total_blue += total_blue;
				parent.child[id] = null;
				--octTree.nodes;
				octTree = null;
				parent = null;
			}

			/**
			 * Prune the lowest layer of the tree.
			 */
			void pruneLowestLevel() {
				forEachChild(Node::pruneLowestLevel);
				if (level == octTree.depth) {
					pruneThis();
				}
			}

			/**
			 * Remove any nodes that have fewer than threshold
			 * pixels. Also, as long as we're walking the tree:
			 *
			 * - figure out the color with the fewest pixels
			 * - recalculate the total number of colors in the tree
			 */
			int reduce(final int threshold, final int nextThreshold) {
				final var next_threshold = new int[] { nextThreshold };
				forEachChild(child -> next_threshold[0] = child.reduce(threshold, next_threshold[0]));
				if (number_pixels <= threshold) {
					pruneThis();
				} else {
					if (unique != 0) {
						++octTree.colors;
					}
					if (number_pixels < next_threshold[0]) {
						next_threshold[0] = number_pixels;
					}
				}
				return next_threshold[0];
			}

			void forEachChild(final Consumer<Node> action) {
				if (nchild != 0) {
					for (int id = 0; id < 8; ++id) {
						if (child[id] != null) {
							action.accept(child[id]);
						}
					}
				}
			}

			/*
			 * colormap traverses the color cube tree and notes each
			 * colormap entry. A colormap entry is any node in the
			 * color cube tree where the number of unique colors is
			 * not zero.
			 */
			void colormap() {
				forEachChild(Node::colormap);
				if (unique != 0) {
					final Color c = new Color(//
							(total_red + (unique >> 1)) / unique, //
							(total_green + (unique >> 1)) / unique, //
							(total_blue + (unique >> 1)) / unique //
					);//
					octTree.colormap[octTree.colors] = c;
					color_number = octTree.colors++;
				}
			}
		}
	}
}
/*
 * An efficient color quantization algorithm, adapted from the C++
 * implementation quantize.c in <a
 * href="http://www.imagemagick.org/">ImageMagick</a>.
 * <p>
 * Here is the copyright notice from ImageMagick:
 * <pre>
 * %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 * % Permission is hereby granted, free of charge, to any person obtaining a %
 * % copy of this software and associated documentation files ("ImageMagick"), %
 * % to deal in ImageMagick without restriction, including without limitation %
 * % the rights to use, copy, modify, merge, publish, distribute, sublicense, %
 * % and/or sell copies of ImageMagick, and to permit persons to whom the %
 * % ImageMagick is furnished to do so, subject to the following conditions: %
 * % %
 * % The above copyright notice and this permission notice shall be included in %
 * % all copies or substantial portions of ImageMagick. %
 * % %
 * % The software is provided "as is", without warranty of any kind, express or %
 * % implied, including but not limited to the warranties of merchantability, %
 * % fitness for a particular purpose and noninfringement. In no event shall %
 * % E. I. du Pont de Nemours and Company be liable for any claim, damages or %
 * % other liability, whether in an action of contract, tort or otherwise, %
 * % arising from, out of or in connection with ImageMagick or the use or other %
 * % dealings in ImageMagick. %
 * % %
 * % Except as contained in this notice, the name of the E. I. du Pont de %
 * % Nemours and Company shall not be used in advertising or otherwise to %
 * % promote the sale, use or other dealings in ImageMagick without prior %
 * % written authorization from the E. I. du Pont de Nemours and Company. %
 * % %
 * %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 * </pre>
 */
