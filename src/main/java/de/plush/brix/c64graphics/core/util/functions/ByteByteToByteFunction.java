
package de.plush.brix.c64graphics.core.util.functions;

@FunctionalInterface
public interface ByteByteToByteFunction {
	byte valueOf(byte left, byte right);
}
