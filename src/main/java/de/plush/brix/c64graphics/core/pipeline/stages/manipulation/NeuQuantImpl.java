package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import static java.util.Arrays.stream;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.ColorDistanceMeasure;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * NeuQuant Neural-Net Quantization Algorithm.
 * <p>
 * NEUQUANT Neural-Net quantization algorithm by Anthony Dekker, 1994. See
 * "Kohonen neural networks for optimal colour quantization" in "Network:
 * Computation in Neural Systems" Vol. 5 (1994) pp 351-367. for a discussion of
 * the algorithm.
 * </p>
 * 
 * @author Anthony Dekker, Copyright (c) 1994, Original algorithm
 * 
 * @author K Weiner, Ported to Java 12/00
 * @author Wanja Gayk, added BufferedImage recolorization using custom color distance measures
 * 
 * @see <a href=
 *      "https://www.researchgate.net/publication/2499219_Optimal_Colour_Quantization_using_Kohonen_Neural_Networks">"Optimal Colour Quantization using Kohonen Neural Networks"
 *      Original Paper by Anghony Henry Dekker.</a>
 * @see <a href="https://scientificgems.wordpress.com/stuff/neuquant-fast-high-quality-image-quantization/">NeuQuant: Fast High-Quality Image Quantization
 *      (Scientific Gems)</a>
 * @see <a href="https://github.com/bumptech/glide/blob/master/third_party/gif_encoder/src/main/java/com/bumptech/glide/gifencoder/NeuQuant.java">NeuQuant
 *      source code at
 *      bumptech</a>
 * @see <a href=
 *      "https://github.com/AntonKast/LightZone/blob/7e4212dcc6aeddc3eed76254e7ed1b48bbc53ec4/lightcrafts/extsrc/com/lightcrafts/media/jai/opimage/NeuQuantOpImage.java">NeuQuant
 *      source code at LightZone</a>
 * @implNote
 *           See additional copyright notices at the end of this source code.
 */
class NeuQuantImpl implements Function<BufferedImage, BufferedImage> {

	/** no. of learning cycles */
	private final int ncycles;

	private final int maxColorNum;

	private final int xPeriod;

	private final int yPeriod;

	private final Shape roi;

	private final ColorDistanceMeasure colorDistanceMeasure;

	/* radpower for precomputation */

	/*
	 * Initialise network in range (0,0,0) to (255,255,255) and set parameters
	 */
	public NeuQuantImpl(final ColorDistanceMeasure colorDistanceMeasure, final int maxColorNum, final Integer nCycles, final Shape roi, final int xSampleRate,
			final int ySampleRate) {
		this.colorDistanceMeasure = colorDistanceMeasure;
		this.maxColorNum = maxColorNum;
		this.roi = roi;
		ncycles = nCycles == null ? 100 : nCycles.intValue();
		xPeriod = xSampleRate;
		yPeriod = ySampleRate;

	}

	@Override
	public BufferedImage apply(final BufferedImage originalImage) {
		final var neuQuant = new NeuQuant(originalImage);
		final var colorSet = neuQuant.colors();
		final var result = new BufferedImage(originalImage.getWidth(), originalImage.getHeight(), originalImage.getType());
		for (int y = 0; y < result.getHeight(); ++y) {
			for (int x = 0; x < result.getWidth(); ++x) {
				final var sourceColor = new Color(originalImage.getRGB(x, y));
				final Color closestColor = stream(colorSet).min(colorDistanceMeasure.comparingDistanceTo(sourceColor)).get();
				result.setRGB(x, y, closestColor.getRGB());
			}
		}
		return result;
	}

	class NeuQuant {

		/* four primes near 500 - assume no image has a length so large */
		/* that it is divisible by all four primes */
		private static final int prime1 = 499;

		private static final int prime2 = 491;

		private static final int prime3 = 487;

		private static final int prime4 = 503;

		/** minimum size for input image */
		private static final int minpicturebytes = 3 * prime4;

		// Network Definitions

		private final int maxnetpos = maxColorNum - 1;

		/** bias for colour values */
		private static final int netbiasshift = 4;

		// defs for freq and bias

		/** bias for fractions */
		private static final int intbiasshift = 16;

		private static final int intbias = 1 << intbiasshift;

		private static final int gammashift = 10; /* gamma = 1024 */

		@SuppressWarnings("unused")
		private static final int gamma = 1 << gammashift;

		private static final int betashift = 10;

		private static final int beta = intbias >> betashift; // beta = 1/1024

		private static final int betagamma = intbias << gammashift - betashift;

		// defs for decreasing radius factor

		private final int initrad = maxColorNum >> 3; // for 256 cols, radius starts

		private static final int radiusbiasshift = 6; // at 32.0 biased by 6 bits

		private static final int radiusbias = 1 << radiusbiasshift;

		private final int initradius = initrad * radiusbias; // and decreases by a

		private static final int radiusdec = 30; // factor of 1/30 each cycle

		// defs for decreasing alpha factor

		private static final int alphabiasshift = 10; // alpha starts at 1.0

		private static final int initalpha = 1 << alphabiasshift;

		private int alphadec; // biased by 10 bits

		/* radbias and alpharadbias used for radpower calculation */
		private static final int radbiasshift = 8;

		private static final int radbias = 1 << radbiasshift;

		private static final int alpharadbshift = alphabiasshift + radbiasshift;

		private static final int alpharadbias = 1 << alpharadbshift;

		/**
		 * the network itself - [netsize][4]<br>
		 * typedef int pixel[4]; // BGRc
		 */
		private int[][] network;

		/** for network lookup - really 256 */
		private final int[] netindex = new int[256];

		/** bias and freq arrays for learning */
		private final int[] bias = new int[maxColorNum];

		private final int[] freq = new int[maxColorNum];

		/** radpower for precomputation */
		private final int[] radpower = new int[initrad];

		private final BufferedImage originalImage; /* the input image itself */

		private Color[] colors;

		public NeuQuant(final BufferedImage originalImage) {
			this.originalImage = originalImage;
			process();
		}

		private void process() {
			final Rectangle imageBounds = new Rectangle(originalImage.getWidth(), originalImage.getHeight());
			var roi = NeuQuantImpl.this.roi;
			if (roi == null) {
				roi = imageBounds;
			}
			// Process if and only if within ROI bounds.
			if (!roi.intersects(imageBounds)) {
				throw new IllegalArgumentException("Region of interest " + roi + " does not intersect with image bounds " + imageBounds);
			}
			final var intersection = imageBounds.intersection(roi.getBounds());
			initNetwork();
			learn(intersection);
			unbiasnet();
			inxbuild();
			buildColorMap();
		}

		Color[] colors() {
			return colors;
		}

		private void initNetwork() {
			// intialize the network
			network = new int[maxColorNum][];
			for (int i = 0; i < maxColorNum; ++i) {
				network[i] = new int[4];
				final int[] p = network[i];
				p[0] = p[1] = p[2] = (i << netbiasshift + 8) / maxColorNum;
				freq[i] = intbias / maxColorNum; /* 1/maxColorNum */
				bias[i] = 0;
			}
		}

		@SuppressWarnings({ "checkstyle:cyclomaticComplexity", "checkstyle:NPathComplexity" }) // keeping it close to the original code
		/** Main Learning Loop */
		void learn(final Shape roi) {
			final var rect = roi.getBounds(); // TODO: Adapt to non-rectangular Regions of interest.

			int samplefac = xPeriod * yPeriod;
			final int startX = rect.x / xPeriod;
			final int startY = rect.y / yPeriod;
			final int offsetX = rect.x % xPeriod;
			final int offsetY = rect.y % yPeriod;
			final int pixelsPerLine = (rect.width - 1) / xPeriod + 1;
			final int numSamples = pixelsPerLine * ((rect.height - 1) / yPeriod + 1);

			if (numSamples < minpicturebytes) {
				samplefac = 1;
			}

			alphadec = 30 + (samplefac - 1) / 3;
			int pix = 0;

			final int delta = numSamples / ncycles;
			int alpha = initalpha;
			int radius = initradius;

			int rad = radius >> radiusbiasshift;
			if (rad <= 1) {
				rad = 0;
			}
			for (int i = 0; i < rad; ++i) {
				radpower[i] = alpha * ((rad * rad - i * i) * radbias / (rad * rad));
			}

			final int step = findStep(numSamples);

			Color color = Color.BLACK;
			for (int i = 0; i < numSamples;) {
				final int y = (pix / pixelsPerLine + startY) * yPeriod + offsetY;
				final int x = (pix % pixelsPerLine + startX) * xPeriod + offsetX;

				try {
					color = new Color(originalImage.getRGB(x, y));
				} catch (final Exception e) {
					continue;
				}

				final int b = color.getBlue() << netbiasshift;
				final int g = color.getGreen() << netbiasshift;
				final int r = color.getRed() << netbiasshift;

				int j = contest(b, g, r);

				altersingle(alpha, j, b, g, r);
				if (rad != 0) {
					alterneigh(rad, j, b, g, r); /* alter neighbours */
				}

				pix += step;
				if (pix >= numSamples) {
					pix -= numSamples;
				}

				if (++i % delta == 0) {
					alpha -= alpha / alphadec;
					radius -= radius / radiusdec;
					rad = radius >> radiusbiasshift;
					if (rad <= 1) {
						rad = 0;
					}
					for (j = 0; j < rad; ++j) {
						radpower[j] = alpha * ((rad * rad - j * j) * radbias / (rad * rad));
					}
				}
			}
		}

		private int findStep(final int numSamples) {
			int step;
			if (numSamples < minpicturebytes) {
				step = 3;
			} else if (numSamples % prime1 != 0) {
				step = 3 * prime1;
			} else {
				if (numSamples % prime2 != 0) {
					step = 3 * prime2;
				} else {
					if (numSamples % prime3 != 0) {
						step = 3 * prime3;
					} else {
						step = 3 * prime4;
					}
				}
			}
			return step;
		}

		/** Insertion sort of network and building of netindex[0..255] (to do after unbias) */
		public void inxbuild() {
			int previouscol = 0;
			int startpos = 0;
			for (int i = 0; i < maxColorNum; i++) {
				final int[] p = network[i];
				int smallpos = i;
				int smallval = p[1]; /* index on g */
				/* find smallest in i..maxColorNum-1 */
				int j;
				for (j = i + 1; j < maxColorNum; j++) {
					final int[] q = network[j];
					if (q[1] < smallval) { /* index on g */
						smallpos = j;
						smallval = q[1]; /* index on g */
					}
				}
				final int[] q = network[smallpos];
				/* swap p (i) and q (smallpos) entries */
				if (i != smallpos) {
					j = q[0];
					q[0] = p[0];
					p[0] = j;
					j = q[1];
					q[1] = p[1];
					p[1] = j;
					j = q[2];
					q[2] = p[2];
					p[2] = j;
					j = q[3];
					q[3] = p[3];
					p[3] = j;
				}
				/* smallval entry is now in position i */
				if (smallval != previouscol) {
					final int spi = startpos + i; // the code cleanup will change " (startpos + i)>>>1" to "startpos + i >>>1" which is wrong
					netindex[previouscol] = spi >>> 1;
					for (j = previouscol + 1; j < smallval; j++) {
						netindex[j] = i;
					}
					previouscol = smallval;
					startpos = i;
				}
			}
			final int sm = startpos + maxnetpos; // teh code clenaup will always remove the () in (startpos + maxnetpos) >>> 1, yielding a wrong result
			netindex[previouscol] = sm >>> 1;
			for (int j = previouscol + 1; j < 256; j++) {
				netindex[j] = maxnetpos; /* really 256 */
			}
		}

		/**
		 * Unbias network to give byte values 0..255 and record
		 * position i to prepare for sort.
		 */
		public void unbiasnet() {
			for (int i = 0; i < maxColorNum; i++) {
				network[i][0] >>= netbiasshift;
				network[i][1] >>= netbiasshift;
				network[i][2] >>= netbiasshift;
				network[i][3] = i; /* record colour no */
			}
		}

		@SuppressFBWarnings(value = "DE_MIGHT_IGNORE", //
				justification = "for some odd reason this catch will prevent a compiler bug")
		/**
		 * Move adjacent neurons by precomputed
		 * alpha*(1-((i-j)^2/[r]^2)) in radpower[|i-j|]
		 */
		private void alterneigh(final int rad, final int i, final int b, final int g, final int r) {
			int lo = i - rad;
			if (lo < -1) {
				lo = -1;
			}
			int hi = i + rad;
			if (hi > maxColorNum) {
				hi = maxColorNum;
			}

			int j = i + 1;
			int k = i - 1;
			int m = 1;
			while (j < hi || k > lo) {
				final int a = radpower[m++];
				if (j < hi) {
					final int[] p = network[j++];
					try {
						p[0] -= a * (p[0] - b) / alpharadbias;
						p[1] -= a * (p[1] - g) / alpharadbias;
						p[2] -= a * (p[2] - r) / alpharadbias;
					} catch (final Exception e) {/* prevents 1.3 miscompilation */}
				}
				if (k > lo) {
					final int[] p = network[k--];
					try {
						p[0] -= a * (p[0] - b) / alpharadbias;
						p[1] -= a * (p[1] - g) / alpharadbias;
						p[2] -= a * (p[2] - r) / alpharadbias;
					} catch (final Exception e) {/* prevents 1.3 miscompilation */}
				}
			}
		}

		/** Move neuron i towards biased (b,g,r) by factor alpha. */
		private void altersingle(final int alpha, final int i, final int b, final int g, final int r) {
			/* alter hit neuron */
			final int[] n = network[i];
			n[0] -= alpha * (n[0] - b) / initalpha;
			n[1] -= alpha * (n[1] - g) / initalpha;
			n[2] -= alpha * (n[2] - r) / initalpha;
		}

		/**
		 * Search for biased BGR values.
		 * <p>
		 * Finds closest neuron (min dist) and updates freq
		 * finds best neuron (min dist-bias) and returns position
		 * for frequently chosen neurons, freq[i] is high and bias[i] is negative
		 * bias[i] = gamma*((1/netsize)-freq[i])
		 * </p>
		 */
		private int contest(final int b, final int g, final int r) {
			/*
			 * finds closest neuron (min dist) and updates freq
			 * finds best neuron (min dist-bias) and returns position
			 * for frequently chosen neurons, freq[i] is high and bias[i] is negative
			 * bias[i] = gamma*((1/maxColorNum)-freq[i])
			 */
			int bestd = ~(1 << 31);
			int bestbiasd = bestd;
			int bestpos = -1;
			int bestbiaspos = bestpos;

			for (int i = 0; i < maxColorNum; i++) {
				final int[] n = network[i];
				int dist = n[0] - b;
				if (dist < 0) {
					dist = -dist;
				}
				int a = n[1] - g;
				if (a < 0) {
					a = -a;
				}
				dist += a;
				a = n[2] - r;
				if (a < 0) {
					a = -a;
				}
				dist += a;
				if (dist < bestd) {
					bestd = dist;
					bestpos = i;
				}
				final int biasdist = dist - (bias[i] >> intbiasshift - netbiasshift);
				if (biasdist < bestbiasd) {
					bestbiasd = biasdist;
					bestbiaspos = i;
				}
				final int betafreq = freq[i] >> betashift;
				freq[i] -= betafreq;
				bias[i] += betafreq << gammashift;
			}
			freq[bestpos] += beta;
			bias[bestpos] -= betagamma;
			return bestbiaspos;
		}

		private void buildColorMap() {
			colors = new Color[maxColorNum];
			final int[] index = new int[maxColorNum];
			for (int i = 0; i < maxColorNum; i++) {
				index[network[i][3]] = i;
			}
			int k = -1;
			for (int i = 0; i < maxColorNum; i++) {
				final int j = index[i];
				colors[++k] = new Color(network[j][2], network[j][1], network[j][0]);
			}
		}
	}

}
/*
 * Any party obtaining a copy of these files from the author, directly or
 * indirectly, is granted, free of charge, a full and unrestricted irrevocable,
 * world-wide, paid up, royalty-free, nonexclusive right and license to deal in
 * this software and documentation files (the "Software"), including without
 * limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons who
 * receive copies from any such party to do so, with the only requirement being
 * that this copyright notice remain intact.
 */
