package de.plush.brix.c64graphics.core.model;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;
import static java.lang.Byte.toUnsignedInt;
import static java.util.Comparator.comparingInt;

import java.io.IOException;
import java.net.*;
import java.nio.file.*;
import java.util.*;
import java.util.function.*;

import com.jsoniter.JsonIterator;

import de.plush.brix.c64graphics.core.util.functions.ByteToCharFunction;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * @see <A href="https://sta.c64.org/cbmdocs.html">https://sta.c64.org/cbmdocs.html</a>
 * @see <a href=
 *      "https://github.com/excess-c64/lib1541img/blob/master/src/lib/1541img/petscii.c">https://github.com/excess-c64/lib1541img/blob/master/src/lib/1541img/petscii.c</a>
 */
public final class PETSCII {

	private PETSCII() { /* no public constructor */}

	public enum Case {
		Lower, Upper
	}

	public enum Reverse {
		On, Off
	}

	//@formatter:off
	// TODO: I might actually use therse values, e.g. for the color
//	@SuppressWarnings("unused")
//	@SuppressFBWarnings(value = "UUF_UNUSED_FIELD", justification = "shut the fridge!")
	private static final class Symbol {
		int petscii;
		int type;
		int color;
		int sc_rof_ug;
		int sc_rof_lu;
		int sc_ron_ug;
		int sc_ron_lu;
		int ucp_rof_ug;
		int ucp_rof_lu;
		int ucp_ron_ug;
		int ucp_ron_lu;
		int ucpe0_rof_ug;
		int ucpe0_rof_lu;
		int ucpe0_ron_ug;
		int ucpe0_ron_lu;
		int ucpee_rof_ug;
		int ucpee_rof_lu;
		int ucpee_ron_ug;
		int ucpee_ron_lu;
		/* unip: [{ v: 0 }], */
		int ascii;
		int cp1252;
		int macroman;
		String html;
	}
	//@formatter:on

	private static final Comparator<Symbol> ORDER_BY_PETSCII_CODE = comparingInt(symbol -> symbol.petscii);

	private static final Symbol[] symbolArray;
	static {
		try {
			final URL url = PETSCII.class.getResource("resources/petscii.json");
			final byte[] bytes = Files.readAllBytes(Path.of(url.toURI()));
			symbolArray = JsonIterator.deserialize(bytes, Symbol[].class);
			Arrays.sort(symbolArray, ORDER_BY_PETSCII_CODE);
		} catch (IOException | URISyntaxException e) {
			throw new IllegalStateException("Could not deserialize petscii.json", e);
		}
	}

	private static Map<Character, Byte> charC64ProMonoStyleToScreenCodeLower = new HashMap<>();
	static {
		for (int sc = 0; sc <= 0xFF; ++sc) {
			final byte b = (byte) sc;
			final char c = screencodeToCharC64ProMonoStyle(b, Case.Lower);
			charC64ProMonoStyleToScreenCodeLower.put(c, b);
		}
	}

	private static Map<Character, Byte> charC64ProMonoStyleToScreenCodeUpper = new HashMap<>();
	static {
		for (int sc = 0; sc <= 0xFF; ++sc) {
			final byte b = (byte) sc;
			final char c = screencodeToCharC64ProMonoStyle(b, Case.Upper);
			charC64ProMonoStyleToScreenCodeUpper.put(c, b);
		}
	}

	/**
	 * Warning: one-directional conversion, for display only.
	 * 
	 * @return the screen code to display if the given petscii code is quoted in print-statements.
	 * @see <A href="https://sta.c64.org/cbm64pettoscr.html">https://sta.c64.org/cbm64pettoscr.html</a>
	 */
	@SuppressWarnings({ "checkstyle:cyclomaticComplexity" })
	public static byte petsciiQuotedToScreenCode(final byte petsciiCode) {
		final int p = toUnsignedInt(petsciiCode);
		//@formatter:off
		final byte change = //
		  p >= 0x00 && 0x1F >= p ? (byte) 0x80 //
		: p >= 0x20 && 0x3F >= p ? (byte) 0x00 //
		: p >= 0x40 && 0x5F >= p ? (byte) 0xC0 //
		: p >= 0x60 && 0x7F >= p ? (byte) 0xE0 //
		: p >= 0x80 && 0x8F >= p ? (byte) 0x40 //
		: p >= 0xA0 && 0xBF >= p ? (byte) 0xC0 //
		: p >= 0xC0 && 0xDF >= p ? (byte) 0x80 //
		: p >= 0xE0 && 0xFE >= p ? (byte) 0x80 //
		: /* p == 0xFF  */ (byte) 0x5F; // $FF is the BASIC token of the pi-symbol. It is converted internally to screen code $5E when printed onto the screen.
		//@formatter:on
		return (byte) (petsciiCode + change);
	}

	/**
	 * Warning: Not suitable for display, but or bidirectional conversion. This has never been implemented by Commodore.
	 * 
	 * @return a screen-code equivalent to a given petscii code for bidirection conversion.
	 * @see <A href="https://sta.c64.org/cbm64pettoscrext.html">https://sta.c64.org/cbm64pettoscrext.html</a>
	 */
	@SuppressWarnings({ "checkstyle:cyclomaticComplexity" })
	@SuppressFBWarnings("DB_DUPLICATE_BRANCHES")
	public static byte petsciiExtendedToScreencode(final byte petsciiCode) {
		final int p = toUnsignedInt(petsciiCode);
		//@formatter:off
		final byte change = //
		  p >= 0x00 && 0x1F >= p ? (byte) 0x80 //
		: p >= 0x20 && 0x3F >= p ? (byte) 0x00 //
		: p >= 0x40 && 0x5F >= p ? (byte) 0xC0 //
	    : p >= 0x60 && 0x7F >= p ? (byte) 0x40 // NOTE: differs from table above!
		: p >= 0x80 && 0x8F >= p ? (byte) 0x40 //
		: p >= 0xA0 && 0xBF >= p ? (byte) 0xC0 //
		: p >= 0xC0 && 0xDF >= p ? (byte) 0x80 //
		: p >= 0xE0 && 0xFF >= p ? (byte) 0x00 // NOTE: differs from table above!
		: /* p == 0xFF  */ (byte) 0x00; // never used, just to satisfy statement
		//@formatter:on
		return (byte) (petsciiCode - change);
	}

	/**
	 * Warning: Not suitable for display, but for bidirectional conversion. This has never been implemented by Commodore.
	 * 
	 * @return a petscii equivalent to a given screen code for bidirection conversion.
	 * @see <A href="https://sta.c64.org/cbm64pettoscrext.html">https://sta.c64.org/cbm64pettoscrext.html</a>
	 */
	@SuppressWarnings({ "checkstyle:cyclomaticComplexity" })
	@SuppressFBWarnings(value = "DB_DUPLICATE_BRANCHES", justification = "intentional, there are several cases where the same change is necessary")
	public static byte screenCodeToPetsciiExtended(final byte screenCode) {
		final int s = toUnsignedInt(screenCode);
		//@formatter:off
		final byte change = //
		  s >= 0x80 && 0x9F >= s ? (byte) 0x80 //
		: s >= 0x20 && 0x3F >= s ? (byte) 0x00 //
		: s >= 0x00 && 0x1F >= s ? (byte) 0xC0 //
	    : s >= 0xA0 && 0xBF >= s ? (byte) 0x40 //
		: s >= 0xC0 && 0xDF >= s ? (byte) 0x40 //
		: s >= 0x60 && 0x7F >= s ? (byte) 0xC0 //
		: s >= 0x40 && 0x5F >= s ? (byte) 0x80 //
		: s >= 0xE0 && 0xFF >= s ? (byte) 0x00 //
		: /* p == 0xFF  */ (byte) 0x00; // never used, just to satisfy statement
		//@formatter:on
		return (byte) (screenCode - change);
	}

	/**
	 * Warning: one-directional conversion, for display only.
	 */
	public static String petsciiQuotedToString(final byte[] petsciiCodes, final Case charCase) {
		return bytesToString(petsciiCodes, (b) -> petsciiQuotedToChar(b, charCase));
	}

	/**
	 * Warning: one-directional conversion, for display only.
	 */
	public static String petsciiToString(final byte[] petsciiCodes, final Case charCase, final Reverse reverse) {
		return bytesToString(petsciiCodes, (b) -> petsciiToChar(b, charCase, reverse));
	}

	public static String petsciiToStringC64ProMonoStyle(final byte[] petsciiCodes, final Case charCase, final Reverse reverse) {
		return bytesToString(petsciiCodes, (b) -> petsciiToCharC64ProMonoStyle(b, charCase, reverse));
	}

	// TODO: petscii to string as printed

	/**
	 * Warning: one-directional conversion, for display only.
	 */
	public static char petsciiQuotedToChar(final byte petsciiCode, final Case charCase) {
		return screencodeToChar(petsciiQuotedToScreenCode(petsciiCode), charCase);
	}

	public static char petsciiToChar(final byte petsciiCode, final Case charCase, final Reverse reverse) {
		final int pet = Byte.toUnsignedInt(petsciiCode);
		if (pet >= 0 && pet <= 0xFF) {
			final var symbol = symbolArray[pet];
			switch (charCase) {
			case Lower:
				switch (reverse) {
				case Off:
					return (char) symbol.ucp_rof_lu;
				case On:
					return (char) symbol.ucp_ron_lu;
				default:
					throw new IllegalStateException("unknown enum case " + reverse);
				}
			case Upper:
				switch (reverse) {
				case Off:
					return (char) symbol.ucp_rof_ug;
				case On:
					return (char) symbol.ucp_ron_ug;
				default:
					throw new IllegalStateException("unknown enum case " + reverse);
				}
			default:
				throw new IllegalArgumentException("unknown case" + charCase);
			}
		}
		throw new IllegalArgumentException(String.format("can't map petscii code %d to unicode", pet));
	}

	public static char petsciiToCharC64ProMonoStyle(final byte petsciiCode, final Case charCase, final Reverse reverse) {
		final int pet = Byte.toUnsignedInt(petsciiCode);
		if (pet >= 0 && pet <= 0xFF) {
			final var symbol = symbolArray[pet];
			switch (charCase) {
			case Lower:
				switch (reverse) {
				case Off:
					return (char) symbol.ucpe0_rof_lu;
				case On:
					return (char) symbol.ucpe0_ron_lu;
				default:
					throw new IllegalStateException("unknown enum case " + reverse);
				}
			case Upper:
				switch (reverse) {
				case Off:
					return (char) symbol.ucpe0_rof_ug;
				case On:
					return (char) symbol.ucpe0_ron_ug;
				default:
					throw new IllegalStateException("unknown enum case " + reverse);
				}
			default:
				throw new IllegalArgumentException("unknown case" + charCase);
			}
		}
		throw new IllegalArgumentException(String.format("can't map petscii code %d to unicode", pet));
	}

	/**
	 * Maps a screencode to common unicode. Not all screencodes have a similar unicode character. This is a one-way operation for text/console display purposes
	 * only.
	 * 
	 * see: <a href="https://style64.org/petscii/">https://style64.org/petscii/</a>
	 */
	public static byte charToScreencode(final char chr, final Case charCase) {
		final int ucp = chr;
		// final var reverse = sc < 0x80 ? Reverse.Off : Reverse.On;
		final Predicate<Symbol> findRof;
		final ToIntFunction<Symbol> mapToRof;
		final Predicate<Symbol> findRon;
		final ToIntFunction<Symbol> mapToRon;
		switch (charCase) {
		case Lower:
			findRof = symbol -> symbol.ucp_rof_lu == ucp;
			mapToRof = symbol -> symbol.sc_rof_lu;
			findRon = symbol -> symbol.ucp_ron_lu == ucp;
			mapToRon = symbol -> symbol.sc_ron_lu;
			break;
		case Upper:
			findRof = symbol -> symbol.ucp_rof_ug == ucp;
			mapToRof = symbol -> symbol.sc_rof_ug;
			findRon = symbol -> symbol.ucp_ron_ug == ucp;
			mapToRon = symbol -> symbol.sc_ron_ug;
			break;
		default:
			throw new IllegalArgumentException("unknown case" + charCase);
		}
		final var optSymbolRof = Arrays.stream(symbolArray).filter(findRof).findAny();
		if (optSymbolRof.isPresent()) { return toUnsignedByte(mapToRof.applyAsInt(optSymbolRof.get())); }
		final var symbolRon = Arrays.stream(symbolArray).filter(findRon).findAny()//
				.orElseThrow(() -> new IllegalArgumentException(String.format("can't map unicode %d to screen code", ucp)));
		return toUnsignedByte(mapToRon.applyAsInt(symbolRon));
	}

	public static byte[] stringToScreenCodes(final String string, final Case charCase) {
		int x = 0;
		try {
			final byte[] scs = new byte[string.length()];
			for (; x < string.length(); ++x) {
				scs[x] = charToScreencode(string.charAt(x), charCase);
			}
			return scs;
		} catch (final Exception e) {
			throw new IllegalArgumentException("Char '" + string.charAt(x) + "' at position " + x + " in string \"" + string
					+ "\" could not be converted to screencode.", e);
		}
	}

	/**
	 * Maps a screencode to common unicode. Not all screencodes have a similar unicode character. This is a one-way operation for text/console display purposes
	 * only.
	 * 
	 * see: <a href="https://style64.org/petscii/">https://style64.org/petscii/</a>
	 */
	public static char screencodeToChar(final byte screencode, final Case charCase) {
		final var sc = Byte.toUnsignedInt(screencode);
		final var reverse = sc < 0x80 ? Reverse.Off : Reverse.On;
		final Predicate<Symbol> find;
		final ToIntFunction<Symbol> mapTo;
		switch (charCase) {
		case Lower:
			switch (reverse) {
			case Off:
				find = symbol -> symbol.sc_rof_lu == sc;
				mapTo = symbol -> symbol.ucp_rof_lu;
				break;
			case On:
				find = symbol -> symbol.sc_ron_lu == sc;
				mapTo = symbol -> symbol.ucp_ron_lu;
				break;
			default:
				throw new IllegalStateException("unknown enum case " + reverse);
			}
			break;
		case Upper:
			switch (reverse) {
			case Off:
				find = symbol -> symbol.sc_rof_ug == sc;
				mapTo = symbol -> symbol.ucp_rof_ug;
				break;
			case On:
				find = symbol -> symbol.sc_ron_ug == sc;
				mapTo = symbol -> symbol.ucp_ron_ug;
				break;
			default:
				throw new IllegalStateException("unknown enum case " + reverse);
			}
			break;
		default:
			throw new IllegalArgumentException("unknown case" + charCase);
		}
		final var symbol = Arrays.stream(symbolArray) //
				.filter(find)//
				.findAny()//
				.orElseThrow(() -> new IllegalArgumentException(String.format("can't map screen code %d to unicode", sc)));
		return (char) mapTo.applyAsInt(symbol);
	}

	/**
	 * Maps a screencode to the unicode codepoints in the C64 TrueType v1.2.1 font by The Wiz/Style Since every screen code has a code point in this font, there
	 * is a reverse operation to this. Note that the code points in this font do not necessarily have a proper look in other installed fonts, if any.
	 * 
	 * see: <a href="https://style64.org/petscii/">https://style64.org/petscii/</a>
	 */
	public static char screencodeToCharC64ProMonoStyle(final byte screencode, final Case charCase) {
		final var sc = Byte.toUnsignedInt(screencode);
		final var reverse = sc < 0x80 ? Reverse.Off : Reverse.On;
		final Predicate<Symbol> find;
		final ToIntFunction<Symbol> mapTo;
		switch (charCase) {
		case Lower:
			switch (reverse) {
			case Off:
				find = symbol -> symbol.sc_rof_lu == sc;
				mapTo = symbol -> symbol.ucpe0_rof_lu;
				break;
			case On:
				find = symbol -> symbol.sc_ron_lu == sc;
				mapTo = symbol -> symbol.ucpe0_ron_lu;
				break;
			default:
				throw new IllegalStateException("unknown enum case " + reverse);
			}
			break;
		case Upper:
			switch (reverse) {
			case Off:
				find = symbol -> symbol.sc_rof_ug == sc;
				mapTo = symbol -> symbol.ucpe0_rof_ug;
				break;
			case On:
				find = symbol -> symbol.sc_ron_ug == sc;
				mapTo = symbol -> symbol.ucpe0_ron_ug;
				break;
			default:
				throw new IllegalStateException("unknown enum case " + reverse);
			}
			break;
		default:
			throw new IllegalArgumentException("unknown case" + charCase);
		}
		final var symbol = Arrays.stream(symbolArray) //
				.filter(find)//
				.findAny()//
				.orElseThrow(() -> new IllegalArgumentException(String.format("can't map screen code %d to unicode/C64 TrueType", sc)));
		return (char) mapTo.applyAsInt(symbol);
	}

	/**
	 * Maps a unicode codepoint in the C64 TrueType v1.2.1 font by The Wiz/Style to a screen screen code. Note that depending on the character case, a unicode
	 * code point may not have a corresponding character. You can't have a screen code for a lower case "a" from in an all-uppercae screencode-set.
	 * 
	 * see: <a href="https://style64.org/petscii/">https://style64.org/petscii/</a>
	 */
	public static byte charC64ProMonoStyleToScreenCode(final char chr, final Case charCase) {
		final Byte b;
		switch (charCase) {
		case Lower:
			b = charC64ProMonoStyleToScreenCodeLower.get(chr);
			break;
		case Upper:
			b = charC64ProMonoStyleToScreenCodeUpper.get(chr);
			break;
		default:
			throw new IllegalArgumentException("unknown case" + charCase);
		}
		if (b == null) { throw new IllegalArgumentException(String.format("cannot translate character '%c' to petscii in case %s", chr, charCase)); }
		return b;
	}

	private static String bytesToString(final byte[] bytes, final ByteToCharFunction transformation) {
		final var builder = new StringBuilder();
		for (final byte b : bytes) {
			builder.append(transformation.apply(b));
		}
		return builder.toString();
	}

}
