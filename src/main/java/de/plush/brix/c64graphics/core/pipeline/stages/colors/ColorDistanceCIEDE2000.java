package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces.ColorConversions.*;
import static java.lang.Math.min;

import java.awt.Color;
import java.util.function.DoubleBinaryOperator;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces.ColorCieLab;
import de.plush.brix.c64graphics.core.util.Utils;

/**
 * http://www.color.org/events/colorimetry/Melgosa_CIEDE2000_Workshop-July4.pdf
 */
class ColorDistanceCIEDE2000 implements ColorDistanceMeasure {

	private DoubleBinaryOperator atan2 = Math::atan2;

	ColorDistanceCIEDE2000 fast() {
		atan2 = (y, x) -> Utils.fastAtan2((float) y, (float) x);
		return this;
	}

	@Override
	public double distanceOf(final Color rgb1, final Color rgb2) {
		return rgb1.equals(rgb2) ? 0.0 : min(1, distanceOf(convertXYZtoCIELab(convertRGBtoXYZ(rgb1)), convertXYZtoCIELab(convertRGBtoXYZ(rgb2))) / 100);
	}

	/**
	 * Compares two L*a*b colors and returns the degree of their similarity. The lower the result the more similar are the colors.
	 *
	 * Taken from https://github.com/StanfordHCI/c3/blob/master/java/src/edu/stanford/vis/color/LAB.java
	 *
	 * @param lab1
	 *            First color represented in L*a*b color space.
	 * @param lab2
	 *            Second color represented in L*a*b color space.
	 * @return The degree of similarity between the two input colors according to the CIEDE2000 color-difference formula.
	 */
	private double distanceOf(final ColorCieLab lab1, final ColorCieLab lab2) {
		// adapted from Sharma et al's MATLAB implementation at
		// http://www.ece.rochester.edu/~gsharma/ciede2000/
		// parametric factors, use defaults
		final double kl = 1, kc = 1, kh = 1;
		// compute terms
		final double L1 = lab1.L();
		final double a1 = lab1.a();
		final double b1 = lab1.b();

		final double L2 = lab2.L();
		final double a2 = lab2.a();
		final double b2 = lab2.b();

		// Cab = sqrt(a^2 + b^2)
		final double Cab1 = Math.sqrt(a1 * a1 + b1 * b1);
		final double Cab2 = Math.sqrt(a2 * a2 + b2 * b2);

		// CabAvg = (Cab1 + Cab2) / 2
		final double CabAvg = 0.5 * (Cab1 + Cab2);
		final double G = 1 + 0.5 * (1 - Math.sqrt(Math.pow(CabAvg, 7) / (Math.pow(CabAvg, 7) + Math.pow(25, 7))));

		final double ap1 = G * a1;
		final double ap2 = G * a2;

		// Cp = sqrt(ap^2 + b^2)
		final double Cp1 = Math.sqrt(ap1 * ap1 + b1 * b1);
		final double Cp2 = Math.sqrt(ap2 * ap2 + b2 * b2);

		// CpProd = (Cp1 * Cp2)
		final double CpProd = Cp1 * Cp2;

		// NOTE: Other implementations use a precalculated value with more digits for pi*2, but:
		// Math.PI * 2.0 == 6.283185307179586
		// Math.PI * 2.0 == 6.283185307179586476925286766559
		// --> the extra digits don't make a difference

		// hp1 = atan2(b1, ap1)
		double hp1 = atan2.applyAsDouble(b1, ap1);
		// ensure hue is between 0 and 2pi
		if (hp1 < 0) {
			hp1 += 2 * Math.PI;
		}

		// hp2 = atan2(b2, ap2)
		double hp2 = atan2.applyAsDouble(b2, ap2);
		// ensure hue is between 0 and 2pi
		if (hp2 < 0) {
			hp2 += 2 * Math.PI;
		}

		double dL = L2 - L1;
		double dC = Cp2 - Cp1;

		// computation of hue difference
		double dhp = 0.0;
		// keep hue difference at zero if the product of chromas is zero
		if (CpProd != 0) { // TODO: really do double == double with no wiggle room?
			dhp = hp2 - hp1;
			if (dhp > +Math.PI) {
				dhp -= 2 * Math.PI;
			}
			if (dhp < -Math.PI) {
				dhp += 2 * Math.PI;
			}
		}

		// Note that the defining equations actually need signed Hue and chroma
		// differences which is different from prior color difference formulae
		double dH = 2 * Math.sqrt(CpProd) * Math.sin(dhp / 2);

		// Weighting functions
		final double Lp = 0.5 * (L1 + L2);

		final double Cp = 0.5 * (Cp1 + Cp2);
		// Average Hue Computation. This is equivalent to that in the paper but
		// simpler programmatically. Average hue is computed in radians and
		// converted to degrees where needed
		double hp = 0.5 * (hp1 + hp2);

		// Identify positions for which abs hue diff exceeds 180 degrees
		if (Math.abs(hp1 - hp2) > Math.PI) {
			hp -= Math.PI;
		}
		// ensure hue is between 0 and 2pi
		if (hp < 0) {
			hp += 2 * Math.PI;
		}

		// Check if one of the chroma values is zero, in which case set mean hue
		// to the sum which is equivalent to other value
		if (CpProd == 0) {
			hp = hp1 + hp2;
		}
		final double Lpm502 = (Lp - 50) * (Lp - 50);
		final double Sl = 1 + 0.015 * Lpm502 / Math.sqrt(20 + Lpm502);

		final double Sc = 1 + 0.045 * Cp;

		final double T = 1 - 0.17 * Math.cos(hp - Math.PI / 6) + 0.24 * Math.cos(2 * hp) + 0.32 * Math.cos(3 * hp + Math.PI / 30) - 0.20 * Math.cos(4 * hp - 63
				* Math.PI / 180);

		final double Sh = 1 + 0.015 * Cp * T;

		final double ex = (180 / Math.PI * hp - 275) / 25;
		final double deltaThetaRad = 30 * Math.PI / 180 * Math.exp(-1 * (ex * ex));

		final double Rc = 2 * Math.sqrt(Math.pow(Cp, 7) / (Math.pow(Cp, 7) + Math.pow(25, 7)));

		final double RT = -Math.sin(2 * deltaThetaRad) * Rc;
		dL = dL / (kl * Sl);
		dC = dC / (kc * Sc);
		dH = dH / (kh * Sh);
		// The CIED200 color difference
		return Math.sqrt(dL * dL + dC * dC + dH * dH + RT * dC * dH);
	}

}
