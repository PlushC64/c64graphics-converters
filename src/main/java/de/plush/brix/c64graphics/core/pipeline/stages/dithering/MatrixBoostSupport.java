package de.plush.brix.c64graphics.core.pipeline.stages.dithering;

public interface MatrixBoostSupport {

	/**
	 * @param boost
	 *            if set true, a matrix like {{0,1,2}} will be transformed to {{1,2,3}} and the multiplier will be changed from 1.0/3 to 1.0/4, also the matrix
	 *            normalization (-0.5) will be removed.<br>
	 *            This will usually lead to a dithering will less dark
	 *            streaks/spots, but an overall brighter picture.
	 * @return a new instance with boost set to the given value;
	 */
	MatrixBoostSupport withMatrixBoost(boolean boost);

}