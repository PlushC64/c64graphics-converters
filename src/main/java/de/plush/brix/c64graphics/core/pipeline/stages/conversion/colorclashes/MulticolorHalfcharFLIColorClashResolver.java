package de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes;

import static de.plush.brix.c64graphics.core.util.Utils.subsetStream;
import static de.plush.brix.c64graphics.core.util.UtilsImage.*;
import static java.lang.Math.min;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toSet;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.PreProcessedMulticolorFliImage;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.Alignment.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.PaletteConversion.PaletteConvertedImage;
import de.plush.brix.c64graphics.core.util.Utils;
import de.plush.brix.c64graphics.core.util.Utils.WeightedObject;

/**
 * Resolves color clashes in Multicolor Bitmaps + 2 Screen RAMs + colorRAM + background color
 *
 * @author Wanja Gayk
 */
public class MulticolorHalfcharFLIColorClashResolver implements IMulticolorFLIColorClashResolver {

	private final Palette palette;

	private final Supplier<Color> backgroundSupplier;

	private final ColorDistanceMeasure colorDistanceMeasure;

	private List<Color> preferredCramColors = emptyList();

	private final int partialImageHeight;

	public MulticolorHalfcharFLIColorClashResolver(final Palette palette, final Supplier<Color> backgroundSupplier,
			final ColorDistanceMeasure colorDistanceMeasure) {
		this.palette = palette;
		this.backgroundSupplier = backgroundSupplier;
		this.colorDistanceMeasure = colorDistanceMeasure;
		partialImageHeight = C64Char.HEIGHT_PIXELS / 2;
	}

	public MulticolorHalfcharFLIColorClashResolver withPreferredColorRamD800Colors(final Color... colors) {
		preferredCramColors = asList(colors);
		preferredCramColors.forEach(palette::colorCodeOrException);
		return this;
	}

	// @see de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.D800PreferenceSupplier#preferredColorRamD800Colors()
	@Override
	public List<Color> preferredColorRamD800Colors() {
		return new ArrayList<>(preferredCramColors);
	}

	@Override
	public PreProcessedMulticolorFliImage apply(final BufferedImage image) {
		final Color background = backgroundSupplier.get();
		final var original = new ResizeCanvas(C64Bitmap.sizePixels(), backgroundSupplier, Horizontal.LEFT, Vertical.TOP).apply(image);

		final PreProcessedMulticolorFliImage result = new PreProcessedMulticolorFliImage(original.getWidth(), original.getHeight(), original.getType(),
				palette.colorCodeOrException(background));
		final Graphics g = result.getGraphics();
		try {
			IntStream.range(0, original.getHeight())//
					.parallel()//
					.filter(yPix -> yPix % C64Char.HEIGHT_PIXELS == 0) // cursor block
					.forEach(yPixBlockStart -> {
						for (int xPixBlockStart = 0; xPixBlockStart < original.getWidth(); xPixBlockStart += C64Char.WIDTH_PIXELS) {
							final int restX = min(C64Char.WIDTH_PIXELS, original.getWidth() - xPixBlockStart);
							final int restY = min(C64Char.HEIGHT_PIXELS, original.getHeight() - yPixBlockStart);
							final BufferedImage blockImage = original.getSubimage(xPixBlockStart, yPixBlockStart, restX, restY);

							final Set<Color> colors = colors(blockImage);
							final LinkedHashSet<Color> preferredOccurrences = new LinkedHashSet<>(preferredCramColors);
							preferredOccurrences.retainAll(colors);
							final Collection<Color> cramColorCandidates = !preferredOccurrences.isEmpty() ? preferredOccurrences : colors;

							final PreProcessedBlockImage minErrImage = cramColorCandidates.stream()//
									.map(cramColor -> alternativeBlockImage(blockImage, background, cramColor))
									.map(alternativeBlockImage -> new Utils.WeightedObject<>(alternativeBlockImage,
											error(alternativeBlockImage, blockImage, colorDistanceMeasure)))//
									.min(comparing(weightedImage -> weightedImage.weight))//
									.get().object;
							g.drawImage(minErrImage, xPixBlockStart, yPixBlockStart, null);
							result.setColorCode(xPixBlockStart / 8, yPixBlockStart / 8, minErrImage.cramColorCode);
						}
					});
		} finally {
			g.dispose();
		}
		return result;
	}

	private PreProcessedBlockImage alternativeBlockImage(final BufferedImage blockImage, final Color background, final Color cramColor) {
		final PreProcessedBlockImage blockImageCandidate = new PreProcessedBlockImage(palette.colorCodeOrException(cramColor));

		final Graphics2D gCandidate = blockImageCandidate.createGraphics();
		try {
			for (int line = 0; line < C64Char.HEIGHT_PIXELS; line += partialImageHeight) {
				final BufferedImage partialImage = blockImage.getSubimage(0, line, blockImage.getWidth(), partialImageHeight);
				final Set<Color> halfImageColors = colors(partialImage);
				if (!hasColorClash(background, cramColor, halfImageColors)) {
					gCandidate.drawImage(partialImage, 0, line, null);
				} else {
					final BufferedImage minErrLineImage = alternativePartialImage(partialImage, halfImageColors, background, cramColor);
					gCandidate.drawImage(minErrLineImage, 0, line, null);
				}
			}
			return blockImageCandidate;
		} finally {
			gCandidate.dispose();
		}
	}

	private BufferedImage alternativePartialImage(final BufferedImage partialImage, final Set<Color> partialImageColors, final Color background,
			final Color cramColor) {
		final Set<Color[]> alternativeColorSets = get4ColorAlternatives(background, cramColor, partialImageColors);
		final Optional<WeightedObject<PaletteConvertedImage>> optionalMin = alternativeColorSets.stream()//
				.map(replacementColors -> (Palette) () -> replacementColors)//
				.map(palette -> new PaletteConversion(palette, colorDistanceMeasure, Dithering.None).apply(deepCopy(partialImage)))//
				.map(alternativePartialImage -> new Utils.WeightedObject<>(alternativePartialImage,
						error(alternativePartialImage, partialImage, colorDistanceMeasure)))//
				.min(comparing(weightedImage -> weightedImage.weight));
		if (!optionalMin.isPresent()) {
			System.out.println("could not find min err line image");
		}
		return optionalMin.get().object;
	}

	private static boolean hasColorClash(final Color background, final Color cramColor, final Set<Color> colors) {
		final List<Color> blockGlobal = Arrays.asList(background, cramColor);
		return colors.stream()// We may have no more than 2 non-block-global colors per line
				.filter(color -> !blockGlobal.contains(color))//
				.count() > 2;
	}

	private static Set<Color[]> get4ColorAlternatives(final Color background, final Color cramColor, final Set<Color> orginalColors) {
		final ArrayList<Color> superSet = new ArrayList<>(orginalColors);
		superSet.remove(background);// will be added later
		superSet.remove(cramColor);// will be added later
		final Set<Color[]> alternatives = subsetStream(superSet, 2)// all possible 2 non-background, non colorRam colors
				.peek(colorSet -> colorSet.add(background))// plus background
				.peek(colorSet -> colorSet.add(cramColor))// plus cramColor
				.map(set -> set.toArray(new Color[set.size()]))//
				.collect(toSet());
		return alternatives;
	}

	private static class PreProcessedBlockImage extends BufferedImage {

		private final byte cramColorCode;

		public PreProcessedBlockImage(final byte cramColorCode) {
			super(C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS, BufferedImage.TYPE_INT_RGB);
			this.cramColorCode = cramColorCode;
		}

	}

}
