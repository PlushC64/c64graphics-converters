package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.Alignment.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.ResizeCanvas;

public class ImageToC64BitmapMulticolor implements IPipelineStage<BufferedImage, C64Bitmap> {

	private final Supplier<Color> backgroundColorProvider;

	private final Color mc1D022;

	private final Color mc2D023;

	private final Color cramD800;

	public ImageToC64BitmapMulticolor() {
		this(() -> C64ColorsColodore.BLACK.color, C64ColorsColodore.BLUE.color, C64ColorsColodore.LIGHT_BLUE.color, C64ColorsColodore.CYAN.color);
	}

	public ImageToC64BitmapMulticolor(final Supplier<Color> backgroundColorProvider, final Color mc1D022, final Color mc2D023, final Color cramD800) {
		this.backgroundColorProvider = backgroundColorProvider;
		this.mc1D022 = mc1D022;
		this.mc2D023 = mc2D023;
		this.cramD800 = cramD800;
	}

	@Override
	public C64Bitmap apply(final BufferedImage original) {
		final Color background = backgroundColorProvider.get();
		final var image = new ResizeCanvas(C64Bitmap.sizePixels(), backgroundColorProvider, Horizontal.LEFT, Vertical.TOP).apply(original);

		final ImageToC64CharMulticolor imageToC64CharMulticolor = new ImageToC64CharMulticolor(background, mc1D022, mc2D023, cramD800);

		final C64Bitmap bitmap = new C64Bitmap();
		final Dimension blocks = C64Bitmap.sizeBlocks();
		for (int yBlock = 0; yBlock < blocks.height; ++yBlock) {
			for (int xBlock = 0; xBlock < blocks.width; ++xBlock) {
				final int xPix = xBlock * C64Char.WIDTH_PIXELS;
				final int yPix = yBlock * C64Char.HEIGHT_PIXELS;
				final BufferedImage blockImage = image.getSubimage(xPix, yPix, C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS);
				final C64Char block = imageToC64CharMulticolor.apply(blockImage);
				bitmap.setBlock(xBlock, yBlock, block);
			}
		}
		return bitmap;
	}

}
