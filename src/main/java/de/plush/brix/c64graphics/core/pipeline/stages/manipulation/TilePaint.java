/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Optional;
import java.util.function.BiConsumer;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class TilePaint implements IPipelineStage<BufferedImage, BufferedImage> {

	private final Dimension tileSize;

	private final BiConsumer<Rectangle, Graphics2D> painter;

	private final Optional<Rectangle> optionalBounds;

	/**
	 * Paints a pattern onto the result image.
	 *
	 * @param bounds
	 *            the bounds of the effect, i.e. the painting area, <tt>null</tt> to fill the entire frame
	 * @param tileSize
	 *            the size of the tile that will be painted
	 * @param painter
	 *            the function to paint the tile, it will receive a new Rectangle describing the tile bounds and a new Graphics object for every call.
	 */
	public TilePaint(final Rectangle bounds, final Dimension tileSize, final BiConsumer<Rectangle, Graphics2D> painter) {
		optionalBounds = Optional.ofNullable(bounds);
		this.tileSize = new Dimension(tileSize);
		this.painter = painter;
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		final Graphics2D g = (Graphics2D) result.getGraphics();
		try {
			g.drawImage(original, 0, 0, null);
			optionalBounds.ifPresent(g::setClip);
			for (int y = 0; y < result.getHeight(); y += tileSize.height) {
				for (int x = 0; x < result.getWidth(); x += tileSize.width) {
					final Graphics2D gTile = (Graphics2D) g.create();
					try {
						painter.accept(new Rectangle(x, y, tileSize.width, tileSize.height), gTile);
					} finally {
						gTile.dispose();
					}
				}
			}
		} finally {
			g.dispose();
		}
		return result;
	}
}