package de.plush.brix.c64graphics.core.model;

import java.net.*;

import de.plush.brix.c64graphics.core.util.*;

/**
 * Standard hires bitmap + 8 screen rams, as used by Topaz AFLI editor, etc. <br>
 *
 * @see #binaryProviderAFLITopaz()
 * @see #binaryProviderHiresFLICrest()
 * @author Wanja Gayk
 */
public class C64BitmapmodePictureHiresFli extends C64BitmapmodePictureHiresNscreenFLI {

	private static final int SCREEN_COUNT = C64Char.HEIGHT_PIXELS;

	public C64BitmapmodePictureHiresFli(final C64Bitmap bitmap, final C64Screen[] screens) {
		super(SCREEN_COUNT, bitmap, screens);
	}

	public C64BitmapmodePictureHiresFli(final C64BitmapmodePictureHiresFli other) {
		this(other.bitmap, other.screens);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!super.equals(obj)) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		return true;
	}

	/**
	 * Hires Bitmap, 8 x Screen RAM<br>
	 * From <a href=
	 * "http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03">http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03</a>:<br>
	 * <style> #gfxformat th { background:lightgray; text-align: left; width:auto}</style> <style> #gfxformat td.first { width:18ch; }</style><style> #gfxformat
	 * td { width:auto}</style>
	 * <table id="gfxformat">
	 * <th colspan="2">Hires FLI (by Crest) (pc-ext: .hfc)</th>
	 * <tr>
	 * <td class="first">$4000 - $7fff</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$4000 - $5f3f</td>
	 * <td>Bitmap</td>
	 * <tr>
	 * <td class="first">$6000 - $7fe7</td>
	 * <td>Screen RAMs</td>
	 * </table>
	 *
	 * @return A provider that will provide the C64 binary of this image at the time this method was invoked.
	 */
	public C64BinaryProvider binaryProviderHiresFLICrest() {
		return hiresFLICrestFormat().binaryProvider(this);
	}

	public static C64BitmapmodePictureHiresFli loadHiresFLICrest(final URL loc, final StartAddress startAddress) {
		return hiresFLICrestFormat().load(Utils.readFile(loc, startAddress));
	}

	public static C64BitmapmodePictureHiresFli loadHiresFLICrest(final URI loc, final StartAddress startAddress) {
		return hiresFLICrestFormat().load(Utils.readFile(loc, startAddress));
	}

	public void saveHiresFLICrest(final URI loc, final StartAddress startAddress) {
		hiresFLICrestFormat().save(binaryProviderHiresFLICrest(), loc, startAddress);
	}

	public static C64BitmapmodePictureHiresFli fromHiresFLICrestBinary(final byte[] binary) {
		return hiresFLICrestFormat().load(binary);
	}

	private static Format hiresFLICrestFormat() {
		return (Format) new Format()//
				.withLoadAddress(0x4000, 0x7fff)//
				.withBitmapAddress(0x4000)//
				.withScreensAddress(0x6000);
	}

	/**
	 * Hires Bitmap, 8 x Screen RAM<br>
	 * From <a href=
	 * "http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03">http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03</a>:<br>
	 * <style> #gfxformat th { background:lightgray; text-align: left; width:auto}</style> <style> #gfxformat td.first { width:18ch; }</style><style> #gfxformat
	 * td { width:auto}</style>
	 * <table id="gfxformat">
	 * <th colspan="2">AFLI-editor v2.0 (by Topaz Beerline) (pc-ext: .afl)</th>
	 * <tr>
	 * <td class="first">$4000 - $7fff</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$4000 - $5fe7</td>
	 * <td>Screen RAMs</td>
	 * <tr>
	 * <td class="first">$6000 - $7f3f</td>
	 * <td>Bitmap</td>
	 * </table>
	 *
	 * @return A provider that will provide the C64 binary of this image at the time this method was invoked.
	 */
	public C64BinaryProvider binaryProviderAFLITopaz() {
		return aFLITopazFormat().binaryProvider(this);
	}

	public static C64BitmapmodePictureHiresFli loadAFLITopaz(final URL loc, final StartAddress startAddress) {
		return aFLITopazFormat().load(Utils.readFile(loc, startAddress));
	}

	public static C64BitmapmodePictureHiresFli loadAFLITopaz(final URI loc, final StartAddress startAddress) {
		return aFLITopazFormat().load(Utils.readFile(loc, startAddress));
	}

	public void saveAFLITopaz(final URI loc, final StartAddress startAddress) {
		aFLITopazFormat().save(binaryProviderAFLITopaz(), loc, startAddress);
	}

	public static C64BitmapmodePictureHiresFli fromAFLITopazBinary(final byte[] binary) {
		return aFLITopazFormat().load(binary);
	}

	private static Format aFLITopazFormat() {
		return (Format) new Format()//
				.withLoadAddress(0x4000, 0x7fff)//
				.withScreensAddress(0x4000) //
				.withBitmapAddress(0x6000);
	}

	private static class Format extends C64HiresFliFormat<C64BitmapmodePictureHiresFli> {
		C64BitmapmodePictureHiresFli load(final byte[] binary) {
			return super.load(binary, SCREEN_COUNT, C64BitmapmodePictureHiresFli::new);
		}
	}
}
