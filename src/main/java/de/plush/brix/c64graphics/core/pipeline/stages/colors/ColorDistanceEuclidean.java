package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import static java.lang.Math.*;

import java.awt.Color;

/**
 * Compared colors to their simple Euclidean distance.<br>
 * This is usually not a very good measure, as it does not take color perception into account.
 */
class ColorDistanceEuclidean implements ColorDistanceMeasure {
	@Override
	public double distanceOf(final Color c1, final Color c2) {
		return c1.equals(c2) ? 0.0 : min(1, euclidean(c1, c2) / 441.67);
	}

	private static double euclidean(final Color c1, final Color c2) {
		return sqrt(pow(c1.getRed() - c2.getRed(), 2) + pow(c1.getGreen() - c2.getGreen(), 2) + pow(c1.getBlue() - c2.getBlue(), 2));
	}
}
