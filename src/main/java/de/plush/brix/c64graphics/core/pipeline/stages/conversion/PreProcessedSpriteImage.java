package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.Palette;

public class PreProcessedSpriteImage extends BufferedImage {

	private final Palette palette;

	private final byte spriteColorCode;

	private final byte mc1ColorCode;

	private final byte mc2ColorCode;


	public PreProcessedSpriteImage(final int width, final int height, final int imageType, final Palette palette, final byte spriteColorCode,
			final byte mc1ColorCode, final byte mc2ColorCode) {
		super(width, height, imageType);
		this.palette = palette;
		this.spriteColorCode = spriteColorCode;
		this.mc1ColorCode = mc1ColorCode;
		this.mc2ColorCode = mc2ColorCode;
	}

	public Palette palette() {
		return palette;
	}

	public byte spriteColorCode() {
		return spriteColorCode;
	}

	public byte mc1ColorCode() {
		return mc1ColorCode;
	}

	public byte mc2ColorCode() {
		return mc2ColorCode;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [backgroundColorCode=" + spriteColorCode + ", mc1ColorCode=" + mc1ColorCode + ", mc2ColorCode=" + mc2ColorCode
				+ "]";
	}

}