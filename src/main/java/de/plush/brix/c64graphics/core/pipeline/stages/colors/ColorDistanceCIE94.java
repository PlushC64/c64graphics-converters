package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces.ColorConversions.*;
import static java.lang.Math.*;

import java.awt.Color;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces.ColorCieLab;

/**
 * Color distance according to <a href="https://en.wikipedia.org/wiki/Color_difference#CIE94">CIE94</a>.
 * The 1976 definition was extended to address perceptual non-uniformities, while retaining the CIELAB color space, by the introduction of application-specific
 * weights derived from an automotive paint test's tolerance data.
 * 
 * ΔE (1994) is defined in the L*C*h* color space with differences in lightness, chroma and hue calculated from L*a*b* coordinates.
 */
class ColorDistanceCIE94 implements ColorDistanceMeasure {

	private final Application application;

	ColorDistanceCIE94(final Application application) {
		this.application = application;
	}

	@Override
	public double distanceOf(final Color rgb1, final Color rgb2) {
		final ColorCieLab lab1 = convertXYZtoCIELab(convertRGBtoXYZ(rgb1));
		final ColorCieLab lab2 = convertXYZtoCIELab(convertRGBtoXYZ(rgb2));
		return rgb1.equals(rgb2) ? 0.0 : min(1, distanceOf(lab1, lab2) / 350);
	}

	/**
	 * 
	 * Compares two L*c*h colors and returns the degree of their similarity. The lower the result the more similar are the colors.
	 *
	 * TODO: I reverted back to Lab colors, to fix it. I tried several mehods to compare stuff in the Lch space, but that didnt work out.
	 * The implementation is based on https://github.com/Boris-Em/ColorKit/blob/master/ColorKit/ColorKit/Difference.swift with an additional bug fix for roots
	 * of negative values.
	 * 
	 * @param lch1
	 *            First color represented in L*c*h color space.
	 * @param lch2
	 *            Second color represented in L*c*h color space.
	 * 
	 * @return The degree of similarity between the two input colors according to the CIE94 color-difference formula.
	 */
	private double distanceOf(final ColorCieLab lab1, final ColorCieLab lab2) {

		// kC and kH are usually both unity, and the weighting factors kL, K1 and K2 depend on the application
		final double kC = 1.0;
		final double kH = 1.0;
		final double kL = application.Kl;
		final double k1 = application.K1;
		final double k2 = application.K2;

		final double c1 = sqrt(pow(lab1.a(), 2) + pow(lab1.b(), 2));
		final double c2 = sqrt(pow(lab2.a(), 2) + pow(lab2.b(), 2));
		final double deltaCab = c1 - c2;

		final double sL = 1.0;
		final double sC = 1 + k1 * c1;
		final double sH = 1 + k2 * c1;

		final double deltaL = lab1.L() - lab2.L();
		final double deltaA = lab1.a() - lab2.a();
		final double deltaB = lab1.b() - lab2.b();

		final double deltaHab = sqrt(max(0, pow(deltaA, 2) + pow(deltaB, 2) - pow(deltaCab, 2))); // https://www.internationalcircle.net/wp-content/uploads/2022/01/ICJ_06_2013_02_069-1.pdf

		final double p1 = pow(deltaL / (kL * sL), 2);
		final double p2 = pow(deltaCab / (kC * sC), 2);
		final double p3 = pow(deltaHab / (kH * sH), 2);
		return sqrt(p1 + p2 + p3);
	}

	enum Application {
		GraphicsArts(1.0, .045, .015), //
		Textiles(2.0, .048, .014)//
		;

		final double Kl;

		final double K1;

		final double K2;

		Application(final double Kl, final double K1, final double K2) {
			this.Kl = Kl;
			this.K1 = K1;
			this.K2 = K2;
		}

	}

}
