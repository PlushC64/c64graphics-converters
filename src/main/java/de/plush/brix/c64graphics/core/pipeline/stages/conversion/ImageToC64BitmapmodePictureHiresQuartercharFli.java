package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.Color;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.*;

/**
 * Multicolor Bitmap, 2 screens (FLI-effect every 4 lines), ColorRAM.
 *
 * @author Wanja Gayk
 */
public class ImageToC64BitmapmodePictureHiresQuartercharFli implements IPipelineStage<BufferedImage, C64BitmapmodePictureHiresQuartercharFli> {

	private ImageToC64BitmapmodePictureHiresNscreenFli delegate;

	/**
	 * Using the standard {@link MulticolorHalfcharFLIColorClashResolver}. <br>
	 * <p>
	 * Use {@link #ImageToC64BitmapmodePictureHiresQuartercharFli(Palette, IHiresFLIColorClashResolver)} if you want to use a custom color clash resolver.
	 * </p>
	 *
	 * @param palette
	 *            the C64 target palette
	 * @param colorDistanceMeasure
	 *            used to measure how similar/dissimilar a color is.
	 */
	public ImageToC64BitmapmodePictureHiresQuartercharFli(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure) {
		this(palette, new HiresQuartercharFLIColorClashResolver(colorDistanceMeasure));
	}

	/**
	 * Allows using a custom color clash resolver.
	 *
	 * @param palette
	 *            the C64 target palette
	 * @param colorClashResolver
	 *            the color clash resolver used for preprocessing.
	 */
	public ImageToC64BitmapmodePictureHiresQuartercharFli(final Palette palette, final IHiresFLIColorClashResolver colorClashResolver) {
		delegate = new ImageToC64BitmapmodePictureHiresNscreenFli(C64BitmapmodePictureHiresQuartercharFli.SCREEN_COUNT, palette, colorClashResolver);
	}

	public ImageToC64BitmapmodePictureHiresQuartercharFli withPreferredBackgroundColors(final Color... colors) {
		delegate = delegate.withPreferredBackgroundColors(colors);
		return this;
	}

	public ImageToC64BitmapmodePictureHiresQuartercharFli withPreferredForegroundColors(final Color... colors) {
		delegate = delegate.withPreferredForegroundColors(colors);
		return this;
	}

	@Override
	public C64BitmapmodePictureHiresQuartercharFli apply(final BufferedImage original) {
		final C64BitmapmodePictureHiresNscreenFLI result = delegate.apply(original);
		return new C64BitmapmodePictureHiresQuartercharFli(result.bitmap(), result.screens());
	}

}
