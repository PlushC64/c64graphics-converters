package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.Palette;
import de.plush.brix.c64graphics.core.util.functions.ByteSupplier;

public class C64BitmapScreenToImageMulticolor implements IPipelineStage<C64BitmapmodePictureHires, BufferedImage> {

	private final Palette palette;

	private final C64ColorRam colorRam;

	private final ByteSupplier backgroundColorCodeProvider;

	public C64BitmapScreenToImageMulticolor(final Palette palette, final byte d800ColorCode) {
		this(palette, new C64ColorRam(d800ColorCode), () -> (byte) 0);
	}

	public C64BitmapScreenToImageMulticolor(final Palette palette, final C64ColorRam colorRam) {
		this(palette, colorRam, () -> (byte) 0);
	}

	public C64BitmapScreenToImageMulticolor(final Palette palette, final C64ColorRam colorRam, final Supplier<Color> backgroundColorProvider) {
		this(palette, colorRam, () -> palette.colorCodeOrException(backgroundColorProvider.get()));
	}

	public C64BitmapScreenToImageMulticolor(final Palette palette, final C64ColorRam colorRam, final ByteSupplier backgroundColorCodeProvider) {
		this.palette = palette;
		this.colorRam = new C64ColorRam(colorRam);
		this.backgroundColorCodeProvider = backgroundColorCodeProvider;
	}

	@Override
	public BufferedImage apply(final C64BitmapmodePictureHires bitmapScreen) {
		return new C64BitmapmodePictureMulticolorToImage(palette).apply(new C64BitmapmodePictureMulticolor(bitmapScreen, colorRam, backgroundColorCodeProvider.getAsByte()));
	}

}
