package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.*;
import static java.awt.image.BufferedImage.TYPE_INT_RGB;
import static java.lang.Integer.toHexString;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.model.C64Charset.ROM;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.Palette;

public class C64ColorRAMToImage implements IPipelineStage<C64ColorRam, BufferedImage> {

	private final Supplier<Color> backgroundColorProvider;

	private final Color[] colors;

	private final Palette palette;

	private final C64Char character;

	public C64ColorRAMToImage(final Palette palette) {
		this(palette, C64Charset.load(ROM.C64_UpperCase).characterBlockOf(toUnsignedByte(0x51)).get());
	}

	public C64ColorRAMToImage(final Palette palette, final C64Char character) {
		this(palette, () -> Color.BLACK, character);
	}

	public C64ColorRAMToImage(final Supplier<Color> backgroundColorProvider, final Palette palette) {
		this(palette, backgroundColorProvider, C64Charset.load(ROM.C64_UpperCase).characterBlockOf(toUnsignedByte(0x51)).get());
	}

	public C64ColorRAMToImage(final Palette palette, final Supplier<Color> backgroundColorProvider, final C64Char character) {
		this.palette = palette;
		this.backgroundColorProvider = backgroundColorProvider;
		this.character = character;
		colors = palette.colors();
	}

	@Override
	public BufferedImage apply(final C64ColorRam cRam) {
		final BufferedImage result = new BufferedImage(C64ColorRam.WIDTH_BLOCKS * C64Char.WIDTH_PIXELS, C64ColorRam.HEIGHT_BLOCKS * C64Char.HEIGHT_PIXELS,
				TYPE_INT_RGB);
		final Graphics2D g = result.createGraphics();
		try {
			for (int yBlock = 0; yBlock < C64ColorRam.HEIGHT_BLOCKS; ++yBlock) {
				for (int xBlock = 0; xBlock < C64ColorRam.WIDTH_BLOCKS; ++xBlock) {
					final byte colorCode = cRam.colorCodeAt(xBlock, yBlock);
					final int paletteIndex = toPositiveInt(colorCode);
					if (paletteIndex >= colors.length) {
						throw new IllegalArgumentException(
								"color code $" + toHexString(paletteIndex) + " is not contained in palette (max index " + (colors.length - 1) + ").");
					}
					final Color color = colors[paletteIndex];
					final BufferedImage blockImage = new C64CharToImageHires(palette, backgroundColorProvider, color).apply(character);
					g.drawImage(blockImage, xBlock * C64Char.WIDTH_PIXELS, yBlock * C64Char.HEIGHT_PIXELS, null);
				}
			}
			return result;
		} finally {
			g.dispose();
		}
	}

}
