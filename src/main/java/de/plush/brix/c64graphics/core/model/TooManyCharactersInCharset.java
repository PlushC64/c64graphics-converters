package de.plush.brix.c64graphics.core.model;

@SuppressWarnings("serial")
public class TooManyCharactersInCharset extends RuntimeException {

	public TooManyCharactersInCharset() {}

	public TooManyCharactersInCharset(final String arg0, final Throwable arg1) {
		super(arg0, arg1);
	}

	public TooManyCharactersInCharset(final String arg0) {
		super(arg0);
	}

	public TooManyCharactersInCharset(final Throwable arg0) {
		super(arg0);
	}

}
