package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Optional;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.model.C64Char;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.Palette;

public class C64CharToImageHires implements IPipelineStage<C64Char, PreProcessedCharacterImage> {

	private final Supplier<Color> backgroundColorProvider;

	private final Color pixelColor;

	private final Palette palette;

	private final byte d800ColorCode;

	private final Optional<Color> mc1_D022;

	private final Optional<Color> mc2_D023;

	public C64CharToImageHires(final Palette palette, final Supplier<Color> backgroundColorProvider, final Color pixelColor) {
		this(palette, backgroundColorProvider, null, null, pixelColor);
	}

	/**
	 * This constructor is for internal use by {@link C64CharToImageMulticolor} only, it serves to preserve the original d022 and d023 colors for hires-chars in
	 * multicolor char mode. Using the normal constructor these values would be the same as the background color.
	 */
	C64CharToImageHires(final Palette palette, final Supplier<Color> backgroundColorProvider, final Color mc1_D022, final Color mc2_D023,
			final Color pixelColor) {
		this.palette = palette;
		this.backgroundColorProvider = backgroundColorProvider;
		this.pixelColor = pixelColor;
		d800ColorCode = palette.colorCodeOrException(pixelColor);
		this.mc1_D022 = Optional.ofNullable(mc1_D022);
		this.mc2_D023 = Optional.ofNullable(mc2_D023);
	}

	@Override
	public PreProcessedCharacterImage apply(final C64Char character) {
		// do not get color in constructor, as provider may need to convert an image first (which is after pipeline construction)
		final Color backgroundColor = backgroundColorProvider.get();
		final byte backgroundColorCode = palette.colorCodeOrException(backgroundColor);
		final byte mc1_D022ColorCode = palette.colorCodeOrException(mc1_D022.orElse(backgroundColor));
		final byte mc2_D023ColorCode = palette.colorCodeOrException(mc2_D023.orElse(backgroundColor));

		final PreProcessedCharacterImage result = new PreProcessedCharacterImage(C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS, BufferedImage.TYPE_INT_RGB,
				palette, backgroundColorCode, mc1_D022ColorCode, mc2_D023ColorCode, d800ColorCode);
		for (int y = 0; y < C64Char.HEIGHT_PIXELS; ++y) {
			for (int x = 0; x < C64Char.WIDTH_PIXELS; ++x) {
				final Color color = character.isPixelSet(x, y) ? pixelColor : backgroundColor;
				result.setRGB(x, y, color.getRGB());
			}
		}
		return result;
	}

}
