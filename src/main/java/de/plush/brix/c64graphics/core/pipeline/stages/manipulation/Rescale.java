/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import static java.util.Objects.requireNonNull;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.Alignment.*;

public class Rescale implements IPipelineStage<BufferedImage, BufferedImage> {

	public static class Tweaks {
		// will stretch the input some extra, but not change the target pixel size
		protected int extraStretchPixelsY;
	}

	private final Tweaks tweaks;

	private final Dimension targetSizePixels;

	private final Supplier<Color> backgroundSupplier;

	private final Horizontal horizontalAlignment;

	private final Vertical verticalAlignment;

	public Rescale(final Dimension targetSizePixels, final Supplier<Color> backgroundSupplier) {
		this(targetSizePixels, backgroundSupplier, Horizontal.CENTER, Vertical.CENTER, new Tweaks());
	}

	public Rescale(final int targetSizePixelsX, final int targetSizePixelsY, final Supplier<Color> backgroundSupplier) {
		this(new Dimension(targetSizePixelsX, targetSizePixelsY), backgroundSupplier);
	}

	private Rescale(final Dimension targetSizePixels, final Supplier<Color> backgroundSupplier, final Horizontal horizontalAlignment,
			final Vertical verticalAlignment, final Tweaks tweaks) {
		this.targetSizePixels = new Dimension(targetSizePixels);
		this.backgroundSupplier = requireNonNull(backgroundSupplier);
		this.horizontalAlignment = requireNonNull(horizontalAlignment);
		this.verticalAlignment = requireNonNull(verticalAlignment);
		this.tweaks = requireNonNull(tweaks);
	}

	public Rescale withAlignment(final Horizontal horizontalAlignment) {
		return new Rescale(targetSizePixels, backgroundSupplier, horizontalAlignment, verticalAlignment, tweaks);
	}

	public Rescale withAlignment(final Vertical verticalAlignment) {
		return new Rescale(targetSizePixels, backgroundSupplier, horizontalAlignment, verticalAlignment, tweaks);
	}

	public Rescale withTweaks(final Tweaks tweaks) {
		return new Rescale(targetSizePixels, backgroundSupplier, horizontalAlignment, verticalAlignment, tweaks);
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final double ratioX = (double) targetSizePixels.width / original.getWidth();
		final double ratioY = (double) targetSizePixels.height / original.getHeight();
		final double ratio = Math.min(ratioX, ratioY);

		final BufferedImage result = new BufferedImage(targetSizePixels.width, targetSizePixels.height, BufferedImage.TYPE_3BYTE_BGR);
		final Graphics2D resultGfx = result.createGraphics();
		resultGfx.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		resultGfx.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		resultGfx.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
		resultGfx.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);

		try {
			resultGfx.setColor(backgroundSupplier.get());
			resultGfx.fillRect(0, 0, result.getWidth(), result.getHeight());

			final int scaledWidth = (int) (original.getWidth() * ratio);
			final int scaledHeight = (int) (original.getHeight() * ratio) + tweaks.extraStretchPixelsY;
			final int centerOffsetX = horizontalAlignment.offset(targetSizePixels.width, scaledWidth);
			final int centerOffsetY = verticalAlignment.offset(targetSizePixels.height, scaledHeight);
			resultGfx.drawImage(original, 0 + centerOffsetX, 0 + centerOffsetY, scaledWidth, scaledHeight, null);
			return result;
		} finally {
			resultGfx.dispose();
		}
	}
}