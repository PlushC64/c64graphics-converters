package de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes;

import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public interface IHiresFLIColorClashResolver extends IPipelineStage<BufferedImage, BufferedImage> {
	// interface to indicate usage
}