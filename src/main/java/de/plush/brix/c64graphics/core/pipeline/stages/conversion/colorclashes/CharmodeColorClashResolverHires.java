package de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes;

import static de.plush.brix.c64graphics.core.util.UtilsImage.*;
import static java.lang.Math.min;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toSet;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.CharmodeColorClashResolverHires.PreProcessedImage;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.Alignment.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.PaletteConversion.PaletteConvertedImage;
import de.plush.brix.c64graphics.core.util.*;
import de.plush.brix.c64graphics.core.util.Utils.WeightedObject;

/**
 * Resolves color clashes for Hires charmode (charset+screen+colorram)
 *
 * @author Wanja Gayk
 */
public class CharmodeColorClashResolverHires implements IPipelineStage<BufferedImage, PreProcessedImage> {

	private final ColorDistanceMeasure colorDistanceMeasure;

	public CharmodeColorClashResolverHires(final ColorDistanceMeasure colorDistanceMeasure) {
		this.colorDistanceMeasure = colorDistanceMeasure;
	}

	@Override
	public PreProcessedImage apply(final BufferedImage image) {
		return UtilsImage.colors(image).stream() // try each background color
				.map(background -> {
					final var original = new ResizeCanvas(C64Bitmap.sizePixels(), () -> Color.BLACK, Horizontal.LEFT, Vertical.TOP).apply(image);
					final PreProcessedImage result = new PreProcessedImage(original.getWidth(), original.getHeight(), original.getType(), background);
					final Graphics g = result.getGraphics();
					try {
						IntStream.range(0, original.getHeight())//
								.parallel()//
								.filter(y -> y % C64Char.HEIGHT_PIXELS == 0)//
								.forEach(yStart -> {
									for (int xStart = 0; xStart < original.getWidth(); xStart += C64Char.WIDTH_PIXELS) {
										final int restX = min(C64Char.WIDTH_PIXELS, original.getWidth() - xStart);
										final int restY = min(C64Char.HEIGHT_PIXELS, original.getHeight() - yStart);
										final BufferedImage subimage = original.getSubimage(xStart, yStart, restX, restY);
										final Set<Color> originalColors = UtilsImage.colors(subimage);

										if (!hasColorClash(background, originalColors)) {
											g.drawImage(subimage, xStart, yStart, null);
										} else {
											final Set<Color[]> alternativeColorSets = get2ColorAlternatives(background, originalColors);
											final Optional<WeightedObject<PaletteConvertedImage>> optionalMin = alternativeColorSets.stream()//
													.map(replacementColors -> (Palette) () -> replacementColors)//
													.map(palette -> new PaletteConversion(palette, colorDistanceMeasure, Dithering.None)
															.apply(deepCopy(subimage)))//
													.map(alternativeSubimage -> new Utils.WeightedObject<>(alternativeSubimage,
															error(alternativeSubimage, subimage, colorDistanceMeasure)))//
													.min(comparing((final WeightedObject<PaletteConvertedImage> weightedImage) -> weightedImage.weight));
											if (!optionalMin.isPresent()) {
												System.out.println("waah!");
											}
											final BufferedImage minErrImage = optionalMin.get().object;
											g.drawImage(minErrImage, xStart, yStart, null);
										}
									}
								});
						return result;
					} finally {
						g.dispose();
					}
				}).map(alternativeImage -> new Utils.WeightedObject<>(alternativeImage, error(alternativeImage, image, colorDistanceMeasure)))//
				.min(comparing((final WeightedObject<PreProcessedImage> weightedImage) -> weightedImage.weight))//
				.map(WeightedObject::object)//
				.get();
	}

	private static boolean hasColorClash(final Color background, final Set<Color> colors) {
		return colors.size() > 2 || colors.size() > 1 && !colors.contains(background);
	}

	private static Set<Color[]> get2ColorAlternatives(final Color background, final Set<Color> orginalColors) {
		assert orginalColors.size() > 2;
		return orginalColors.stream()//
				.filter(color -> !color.equals(background))//
				.map(color -> new Color[] { background, color })//
				.collect(toSet());
	}

	public static class PreProcessedImage extends BufferedImage {

		private final Color background;

		public PreProcessedImage(final int width, final int height, final int imageType, final Color background) {
			super(width, height, imageType);
			this.background = background;
		}

		public Color background() {
			return background;
		}

	}
}
