package de.plush.brix.c64graphics.core.pipeline.displays;

import java.awt.Image;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

/** Displays a value using a given conversion function and a given ImageDisplay */
public class Display<T> implements IPipelineStage<T, T> {

	private final ImageDisplay<Image> imageDisplay;
	private final IPipelineStage<T, ? extends Image> conversionForView;

	public Display(final IPipelineStage<T, ? extends Image> conversionForView, final ImageDisplay<Image> imageDisplay) {
		this.imageDisplay = imageDisplay;
		this.conversionForView = conversionForView;
	}

	@Override
	public T apply(final T value) {
		imageDisplay.apply(conversionForView.apply(value));
		return value;
	}
}
