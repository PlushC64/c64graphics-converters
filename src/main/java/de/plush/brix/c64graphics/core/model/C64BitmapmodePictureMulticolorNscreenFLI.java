package de.plush.brix.c64graphics.core.model;

import static java.lang.System.arraycopy;

import java.awt.Dimension;
import java.net.URI;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.util.*;

public class C64BitmapmodePictureMulticolorNscreenFLI implements IRollBlockwise {

	protected final C64BitmapmodePictureHiresNscreenFLI bitmapScreen;

	protected final C64ColorRam colorRam;

	protected final byte backgroundColorCode;

	public C64BitmapmodePictureMulticolorNscreenFLI(final int expectedScreenCount, final C64Bitmap bitmap, final C64Screen[] screens,
			final C64ColorRam colorRam, final byte backgroundColorCode) {
		bitmapScreen = new C64BitmapmodePictureHiresNscreenFLI(expectedScreenCount, bitmap, screens);
		this.colorRam = new C64ColorRam(colorRam);
		this.backgroundColorCode = backgroundColorCode;
	}

	public C64BitmapmodePictureMulticolorNscreenFLI(final int expectedScreenCount, final C64BitmapmodePictureHiresNscreenFLI bitmapScreen,
			final C64ColorRam colorRam, final byte backgroundColorCode) {
		this.bitmapScreen = new C64BitmapmodePictureHiresNscreenFLI(expectedScreenCount, bitmapScreen);
		this.colorRam = new C64ColorRam(colorRam);
		this.backgroundColorCode = backgroundColorCode;
	}

	public Dimension sizeBlocks() {
		return C64Bitmap.sizeBlocks();
	}

	public Dimension sizePixels() {
		return C64Bitmap.sizePixels();
	}

	public void setBitmapBlock(final int xBlock, final int yBlock, final C64Char chrBlock) {
		bitmapScreen.setBitmapBlock(xBlock, yBlock, chrBlock);
	}

	public C64Char bitmapBlockAt(final int xBlock, final int yBlock) {
		return bitmapScreen.bitmapBlockAt(xBlock, yBlock);
	}

	public void setScreenCode(final int screenNumber, final int x, final int y, final byte screenCode) {
		bitmapScreen.setScreenCode(screenNumber, x, y, screenCode);
	}

	public byte screenCodeAt(final int screenNumber, final int x, final int y) {
		return bitmapScreen.screenCodeAt(screenNumber, x, y);
	}

	public byte[][] screenCodeMatrix(final int screenNumber) {
		return bitmapScreen.screenCodeMatrix(screenNumber);
	}

	public void setColorCode(final int x, final int y, final byte colorCode) {
		colorRam.setColorCode(x, y, colorCode);
	}

	public byte colorCodeAt(final int x, final int y) {
		return colorRam.colorCodeAt(x, y);
	}

	public byte[][] colorCodeMatrix() {
		return colorRam.colorCodeMatrix();
	}

	/** @return a snapshot of the current bitmap. */
	public C64Bitmap bitmap() {
		return bitmapScreen.bitmap();
	}

	/** @return a snapshot of the particular screen. */
	public C64Screen screen(final int screenNumber) {
		return bitmapScreen.screen(screenNumber);
	}

	/** @return a snapshot of all screens. */
	public C64Screen[] screens() {
		return bitmapScreen.screens();
	}

	/** @return the number of screens. */
	public int screenCount() {
		return bitmapScreen.screenCount();
	}

	/** @return a snapshot of the current colorRam. */
	public C64ColorRam colorRam() {
		return new C64ColorRam(colorRam);
	}

	public byte backgroundColorCode() {
		return backgroundColorCode;
	}

	@Override
	public void rollLeftBlockwise(final int n) {
		bitmapScreen.rollLeftBlockwise(n);
		colorRam.rollLeftBlockwise(n);
	}

	@Override
	public void rollRightBlockwise(final int n) {
		bitmapScreen.rollRightBlockwise(n);
		colorRam.rollRightBlockwise(n);
	}

	@Override
	public void rollUpBlockwise(final int n) {
		bitmapScreen.rollUpBlockwise(n);
		colorRam.rollUpBlockwise(n);
	}

	@Override
	public void rollDownBlockwise(final int n) {
		bitmapScreen.rollDownBlockwise(n);
		colorRam.rollDownBlockwise(n);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + backgroundColorCode;
		result = prime * result + (bitmapScreen == null ? 0 : bitmapScreen.hashCode());
		result = prime * result + (colorRam == null ? 0 : colorRam.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (!(obj instanceof C64BitmapmodePictureMulticolorNscreenFLI)) { return false; }
		final C64BitmapmodePictureMulticolorNscreenFLI other = (C64BitmapmodePictureMulticolorNscreenFLI) obj;
		if (backgroundColorCode != other.backgroundColorCode) { return false; }
		if (bitmapScreen == null) {
			if (other.bitmapScreen != null) { return false; }
		} else if (!bitmapScreen.equals(other.bitmapScreen)) { return false; }
		if (colorRam == null) {
			if (other.colorRam != null) { return false; }
		} else if (!colorRam.equals(other.colorRam)) { return false; }
		return true;
	}

	@FunctionalInterface
	protected interface MulticolorFliConstructor<R> {
		R apply(C64Bitmap bitmap, C64Screen[] screens, C64ColorRam cram, byte backgroundColorCode);
	}

	protected static class C64MulticolorFliFormat<T extends C64BitmapmodePictureMulticolorNscreenFLI> {
		protected int loadAddress, endAddress, bitmapAddress, screensAddress, cramAddress, backgroundAddress = -1;

		protected int length, bitmapOffset, screensOffset, cramOffset, backgroundOffset = -1;

		protected C64MulticolorFliFormat<T> withLoadAddress(final int loadAddress, final int endAddress) {
			this.loadAddress = loadAddress;
			this.endAddress = endAddress;
			return this;
		}

		protected C64MulticolorFliFormat<T> withBitmapAddress(final int bitmapAddress) {
			this.bitmapAddress = bitmapAddress;
			return this;
		}

		protected C64MulticolorFliFormat<T> withScreensAddress(final int screensAddress) {
			this.screensAddress = screensAddress;
			return this;
		}

		protected C64MulticolorFliFormat<T> withCramAddress(final int cramAddress) {
			this.cramAddress = cramAddress;
			return this;
		}

		C64MulticolorFliFormat<T> withBackgroundAddress(final int backgroundAddress) {
			this.backgroundAddress = backgroundAddress;
			return this;
		}

		private void calcOffsets() {
			length = endAddress - loadAddress + 1; // $4000 - $4000 is one byte in the codebase64 spec.
			bitmapOffset = bitmapAddress - loadAddress;
			screensOffset = screensAddress - loadAddress;
			cramOffset = cramAddress - loadAddress;
			backgroundOffset = backgroundAddress < 0 ? -1 : backgroundAddress - loadAddress;
		}

		protected T load(final byte[] binary, final int screenCount, final MulticolorFliConstructor<T> constuctor) {
			calcOffsets();
			final byte[] bitmapBinary = new byte[C64Bitmap.WIDTH_BLOCKS * C64Bitmap.HEIGHT_BLOCKS * C64Char.HEIGHT_PIXELS];
			System.arraycopy(binary, bitmapOffset, bitmapBinary, 0, bitmapBinary.length);
			final C64Bitmap bitmap = C64Bitmap.binaryToC64Bitmap(bitmapBinary);

			final byte[] cramBinary = new byte[C64Screen.WIDTH_BLOCKS * C64Screen.HEIGHT_BLOCKS];
			System.arraycopy(binary, cramOffset, cramBinary, 0, cramBinary.length);
			final C64ColorRam cram = C64ColorRam.binaryToC64ColorRam(cramBinary);

			final C64Screen[] screens = IntStream.range(0, screenCount)//
					.mapToObj(line -> {
						final byte[] screenBinary = new byte[C64Screen.WIDTH_BLOCKS * C64Screen.HEIGHT_BLOCKS];
						System.arraycopy(binary, screensOffset + line * 0x400, screenBinary, 0, screenBinary.length);
						return C64Screen.fromBinary(screenBinary);
					}).toArray(C64Screen[]::new);

			final byte backgroundColorCode = backgroundOffset < 0 ? (byte) 0 : binary[backgroundOffset];
			return constuctor.apply(bitmap, screens, cram, backgroundColorCode);
		}

		void save(final C64BinaryProvider binaryProvider, final URI uri, final StartAddress startAddress) {
			switch (startAddress) {
			case InFile -> Utils.writeFile(uri, binaryProvider.toC64Binary(), loadAddress);
			case NotInFile -> Utils.writeFile(uri, binaryProvider.toC64Binary());
			}
		}

		protected C64BinaryProvider binaryProvider(final T obj) {
			calcOffsets();
			final byte[] binary = new byte[length];
			IntStream.range(0, obj.screenCount()).forEach(screenNumber -> {
				final byte[] screenBinary = obj.screen(screenNumber).toC64Binary();
				arraycopy(screenBinary, 0, binary, screensOffset + screenNumber * 0x400, screenBinary.length);
			});
			final byte[] bitmapBinary = obj.bitmap().toC64Binary();
			arraycopy(bitmapBinary, 0, binary, bitmapOffset, bitmapBinary.length);
			final byte[] cramBinary = obj.colorRam().toC64Binary();
			arraycopy(cramBinary, 0, binary, cramOffset, cramBinary.length);
			if (backgroundOffset >= 0) {
				binary[backgroundOffset] = obj.backgroundColorCode;
			}
			return () -> binary;
		}
	}
}