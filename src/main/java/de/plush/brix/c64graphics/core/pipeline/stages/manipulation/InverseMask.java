/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.*;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class InverseMask implements IPipelineStage<BufferedImage, BufferedImage> {

	private final Area nonMaskedArea;
	private final Color color;

	public InverseMask(final Color color, final Shape nonMaskedShape) {
		this.color = color;
		nonMaskedArea = new Area(nonMaskedShape);
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		final Graphics2D g = (Graphics2D) result.getGraphics();
		try {
			g.drawImage(original, 0, 0, null);
			final Area outer = new Area(new Rectangle(0, 0, original.getWidth(), original.getHeight()));
			outer.subtract(nonMaskedArea);
			g.setColor(color);
			g.fill(nonMaskedArea);
			return result;
		} finally {
			g.dispose();
		}

	}
}