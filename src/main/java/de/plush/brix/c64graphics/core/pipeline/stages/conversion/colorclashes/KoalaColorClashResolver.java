package de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes;

import static de.plush.brix.c64graphics.core.util.Utils.subsetStream;
import static de.plush.brix.c64graphics.core.util.UtilsImage.*;
import static java.lang.Math.min;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toSet;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.PreProcessedMulticolorImage;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.Alignment.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.PaletteConversion.PaletteConvertedImage;
import de.plush.brix.c64graphics.core.util.Utils;
import de.plush.brix.c64graphics.core.util.Utils.WeightedObject;

public class KoalaColorClashResolver implements IPipelineStage<BufferedImage, PreProcessedMulticolorImage> {

	private final Supplier<Color> backgroundSupplier;

	private final ColorDistanceMeasure colorDistanceMeasure;

	public KoalaColorClashResolver(final Supplier<Color> backgroundSupplier, final ColorDistanceMeasure colorDistanceMeasure) {
		this.backgroundSupplier = backgroundSupplier;
		this.colorDistanceMeasure = colorDistanceMeasure;
	}

	@Override
	public PreProcessedMulticolorImage apply(final BufferedImage image) {
		final Color background = backgroundSupplier.get();
		final var original = new ResizeCanvas(C64Bitmap.sizePixels(), backgroundSupplier, Horizontal.LEFT, Vertical.TOP).apply(image);

		final PreProcessedMulticolorImage result = new PreProcessedMulticolorImage(original.getWidth(), original.getHeight(), original.getType(), background);
		final Graphics g = result.getGraphics();
		try {
			IntStream.range(0, original.getHeight())//
					.parallel()//
					.filter(y -> y % C64Char.HEIGHT_PIXELS == 0)//
					.forEach(yStart -> {
						for (int xStart = 0; xStart < original.getWidth(); xStart += C64Char.WIDTH_PIXELS) {
							final int restX = min(C64Char.WIDTH_PIXELS, original.getWidth() - xStart);
							final int restY = min(C64Char.HEIGHT_PIXELS, original.getHeight() - yStart);
							final BufferedImage subimage = original.getSubimage(xStart, yStart, restX, restY);
							final Set<Color> originalColors = colors(subimage);
							if (!hasColorClash(background, originalColors)) {
								g.drawImage(subimage, xStart, yStart, null);
							} else {
								final Set<Color[]> alternativeColorSets = get4ColorAlternatives(background, originalColors);
								final Optional<WeightedObject<PaletteConvertedImage>> optionalMin = alternativeColorSets.stream()//
										.map(replacementColors -> (Palette) () -> replacementColors)//
										.map(palette -> new PaletteConversion(palette, colorDistanceMeasure, Dithering.None).apply(deepCopy(subimage)))//
										.map(alternativeSubimage -> new Utils.WeightedObject<>(alternativeSubimage, error(alternativeSubimage, subimage,
												colorDistanceMeasure)))//
										.min(comparing(weightedImage -> weightedImage.weight));
								if (!optionalMin.isPresent()) {
									System.out.println("waah!");
								}
								final BufferedImage minErrImage = optionalMin.get().object;
								g.drawImage(minErrImage, xStart, yStart, null);
							}
						}
					});
		} finally {
			g.dispose();
		}
		return result;
	}

	private static boolean hasColorClash(final Color background, final Set<Color> colors) {
		return colors.size() > 4 || !colors.contains(background) && colors.size() > 3;
	}

	private static Set<Color[]> get4ColorAlternatives(final Color background, final Set<Color> orginalColors) {
		assert orginalColors.size() > 4 || !orginalColors.contains(background) && orginalColors.size() > 3;

		final ArrayList<Color> superSet = new ArrayList<>(orginalColors);
		superSet.remove(background);// will be added later
		final Set<Color[]> alternatives = subsetStream(superSet, 3)// all possible 3 non-background colors
				.peek(colorSet -> colorSet.add(background))// plus background
				.map(set -> set.toArray(new Color[set.size()]))//
				.collect(toSet());
		return alternatives;
	}

}
