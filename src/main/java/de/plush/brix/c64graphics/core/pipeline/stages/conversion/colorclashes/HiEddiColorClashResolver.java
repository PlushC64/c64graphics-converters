package de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes;

import static de.plush.brix.c64graphics.core.util.Utils.subsetStream;
import static de.plush.brix.c64graphics.core.util.UtilsImage.*;
import static java.lang.Math.min;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toSet;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.Alignment.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.PaletteConversion.PaletteConvertedImage;
import de.plush.brix.c64graphics.core.util.*;
import de.plush.brix.c64graphics.core.util.Utils.WeightedObject;

/**
 * Resolves color clashes in Hires-Bitmaps + Screen RAM (HiEddi, Artstudio, Doodle, etc).
 *
 * @author Wanja Gayk
 */
public class HiEddiColorClashResolver implements IPipelineStage<BufferedImage, BufferedImage> {

	private final ColorDistanceMeasure colorDistanceMeasure;

	public HiEddiColorClashResolver(final ColorDistanceMeasure colorDistanceMeasure) {
		this.colorDistanceMeasure = colorDistanceMeasure;
	}

	@Override
	public BufferedImage apply(final BufferedImage image) {
		final var original = new ResizeCanvas(C64Bitmap.sizePixels(), () -> Color.BLACK, Horizontal.LEFT, Vertical.TOP).apply(image);
		/*
		 * FIXME: "()-> Color.BLACK" might result in odd color clashes on the right size of images that are not sized as multiple of 8 pixels.
		 * it might be a good idea to copy rightmost and lowest pixels to fill it up to a mutiple of 8..
		 */
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());
		final Graphics g = result.getGraphics();
		try {
			IntStream.range(0, original.getHeight())//
					.parallel()//
					.filter(y -> y % C64Char.HEIGHT_PIXELS == 0)//
					.forEach(yStart -> {
						for (int xStart = 0; xStart < original.getWidth(); xStart += C64Char.WIDTH_PIXELS) {
							final int restX = min(C64Char.WIDTH_PIXELS, original.getWidth() - xStart);
							final int restY = min(C64Char.HEIGHT_PIXELS, original.getHeight() - yStart);
							final BufferedImage subimage = original.getSubimage(xStart, yStart, restX, restY);
							final Set<Color> originalColors = UtilsImage.colors(subimage);
							if (!hasColorClash(originalColors)) {
								g.drawImage(subimage, xStart, yStart, null);
							} else {
								final Set<Color[]> alternativeColorSets = get2ColorAlternatives(originalColors);
								final Optional<WeightedObject<PaletteConvertedImage>> optionalMin = alternativeColorSets.stream()//
										.map(replacementColors -> (Palette) () -> replacementColors)//
										.map(palette -> new PaletteConversion(palette, colorDistanceMeasure, Dithering.None).apply(deepCopy(subimage)))//
										.map(alternativeSubimage -> new Utils.WeightedObject<>(alternativeSubimage,
												error(alternativeSubimage, subimage, colorDistanceMeasure)))//
										.min(comparing((final WeightedObject<PaletteConvertedImage> weightedImage) -> weightedImage.weight));
								if (!optionalMin.isPresent()) {
									System.out.println("waah!");
								}
								final BufferedImage minErrImage = optionalMin.get().object;
								g.drawImage(minErrImage, xStart, yStart, null);
							}
						}
					});
		} finally {
			g.dispose();
		}
		return result;
	}

	private static boolean hasColorClash(final Set<Color> colors) {
		return colors.size() > 2;
	}

	private static Set<Color[]> get2ColorAlternatives(final Set<Color> orginalColors) {
		assert orginalColors.size() > 2;
		final ArrayList<Color> superSet = new ArrayList<>(orginalColors);
		final Set<Color[]> alternatives = subsetStream(superSet, 2)// all possible 2 color combinations
				.map(set -> set.toArray(new Color[set.size()]))//
				.collect(toSet());
		return alternatives;
	}

}
