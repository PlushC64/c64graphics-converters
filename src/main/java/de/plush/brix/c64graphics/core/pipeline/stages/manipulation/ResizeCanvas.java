/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.Alignment.*;

public class ResizeCanvas implements IPipelineStage<BufferedImage, BufferedImage> {

	private final Supplier<Color> backgroundSupplier;

	private final int targetWidth;

	private final int targetHeight;

	private final Horizontal horizontal;

	private final Vertical vertical;

	public ResizeCanvas(final Dimension targetSize, final Supplier<Color> backgroundSupplier, final Horizontal horizontalAlignment,
			final Vertical verticalAlignment) {
		this(targetSize.width, targetSize.height, backgroundSupplier, horizontalAlignment, verticalAlignment);
	}

	public ResizeCanvas(final int targetWidth, final int targetHeight, final Supplier<Color> backgroundSupplier, final Horizontal horizontalAlignment,
			final Vertical verticalAlignment) {
		this.targetWidth = targetWidth;
		this.targetHeight = targetHeight;
		this.backgroundSupplier = backgroundSupplier;
		horizontal = horizontalAlignment;
		vertical = verticalAlignment;
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage result = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_3BYTE_BGR);
		final Graphics resultGfx = result.getGraphics();
		try {
			resultGfx.setColor(backgroundSupplier.get());
			resultGfx.fillRect(0, 0, targetWidth, targetHeight);
			final int offsetX = horizontal.offset(targetWidth, original.getWidth());
			final int offsetY = vertical.offset(targetHeight, original.getHeight());
			resultGfx.drawImage(original, 0 + offsetX, 0 + offsetY, null);
			return result;
		} finally {
			resultGfx.dispose();
		}
	}
}