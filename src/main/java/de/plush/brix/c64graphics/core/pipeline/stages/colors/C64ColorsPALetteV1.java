package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import static de.plush.brix.c64graphics.core.util.Utils.lazy;
import static java.util.Arrays.stream;

import java.awt.Color;

import de.plush.brix.c64graphics.core.util.functions.ByteSupplier;

/**
 * Based on collaborate work on a Forum64 thread:
 * <a href =
 * "https://www.forum64.de/index.php?thread/124965-neue-rgb-palette-für-den-c64-palette">https://www.forum64.de/index.php?thread/124965-neue-rgb-palette-für-den-c64-palette</a>
 * by the users Tobias, mathop, silverdr, Arndt Dettke, et al.
 * 
 * @see <a href="https://www.godot64.de/">https://www.godot64.de/</a>
 * @see <a href =
 *      "https://www.forum64.de/index.php?thread/124965-neue-rgb-palette-für-den-c64-palette">https://www.forum64.de/index.php?thread/124965-neue-rgb-palette-für-den-c64-palette</a>
 * @author Wanja Gayk
 */
public enum C64ColorsPALetteV1 implements C64Color {

	//@formatter:off
	/**0*/BLACK(0, 0, 0), 				
	/**1*/WHITE(255,255,255),
	/**2*/RED(140,50,61),
	/**3*/CYAN(102,191,179),
	/**4*/PURPLE(142,54,161),
	/**5*/GREEN(74,166,72),
	/**6*/BLUE(50,45,171),
	/**7*/YELLOW(205,210,86),
	//
	/**8*/ORANGE(143,80,26),
	/**9*/BROWN(83,61,0),
	/**A*/LIGHT_RED(189,99,110),
	/**B*/DARK_GRAY(78,78,78),
	/**C*/GRAY(118,118,118),	
	/**D*/LIGHT_GREEN(140,233,139),
	/**E*/LIGHT_BLUE(107,102,228),
	/**F*/LIGHT_GRAY(163,163,163);
	//@formatter:on

	public static final PaletteC64Full PALETTE = new PaletteC64Full() {

		private Color[] colors;

		@Override
		public Color[] colors() {
			if (colors == null) {
				colors = stream(values()).map(C64ColorsPALetteV1::color).toArray(Color[]::new);
			}
			return colors.clone();
		}

		@Override
		public String toString() {
			return C64ColorsPALetteV1.class.getSimpleName();
		}
	};

	public final Color color;

	private final ByteSupplier colorCode = lazy(() -> palette().colorCodeOrException(get()));

	C64ColorsPALetteV1(final int r, final int g, final int b) {
		color = new Color(r, g, b);
	}

	@Override
	public Color get() {
		return color;
	}

	@Override
	public byte colorCode() {
		return colorCode.getAsByte(); // can't do this in the constructor (init phase)
	}

	@Override
	public Palette palette() {
		return PALETTE;
	}

}
