package de.plush.brix.c64graphics.core.util;

import static java.util.Comparator.*;

import java.util.Comparator;
import java.util.function.LongUnaryOperator;

public final class CountableObject<T> implements Comparable<CountableObject<T>> {

	private static final Comparator<CountableObject<?>> ORDER_BY_COUNT = comparingLong(CountableObject::count);

	private static final Comparator<Object> ORDER_BY_IDENTiTY = comparingInt(System::identityHashCode);

	private static final Comparator<CountableObject<?>> NATURAL_ORDER = new Comparator<>() {
		@SuppressWarnings({ "rawtypes", "unchecked" })
		@Override
		public int compare(final CountableObject<?> o1, final CountableObject<?> o2) {
			final int c = ORDER_BY_COUNT.compare(o1, o2);
			return c != 0 ? c
					: nullsFirst((Comparator) (o1.objValue instanceof Comparable && o2.objValue instanceof Comparable //
							? naturalOrder() //
							: ORDER_BY_IDENTiTY)//
			).compare(o1.objValue, o2.objValue);
		}
	};

	public final T objValue;

	public long count;

	private CountableObject(final T item, final long count) {
		this.objValue = item;
		this.count = count;
	}

	public static <T> WithMissingCount<T> ofValue(final T value) {
		return (final long count) -> { return new CountableObject<>(value, count); };
	}

	public static <T> WithMissingValue<T> ofCount(final long count) {
		return (final T value) -> { return new CountableObject<>(value, count); };
	}

	public CountableObject<T> modifyCount(final LongUnaryOperator op) {
		count = op.applyAsLong(count);
		return this;
	}

	public T objValue() {
		return objValue;
	}

	public long count() {
		return count;
	}

	public void setCount(final long count) {
		this.count = count;
	}

	@Override
	public int compareTo(final CountableObject<T> o) {
		return NATURAL_ORDER.compare(this, o);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (count ^ count >>> 32);
		result = prime * result + (objValue == null ? 0 : objValue.hashCode());
		return result;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final CountableObject other = (CountableObject) obj;
		if (count != other.count) { return false; }
		if (objValue == null) {
			if (other.objValue != null) { return false; }
		} else if (!objValue.equals(other.objValue)) { return false; }
		return true;
	}

	@Override
	public String toString() {
		return "item: " + (objValue == null ? "null" : objValue.toString()) + ", count: " + count;
	}

	public interface WithMissingCount<T> {
		CountableObject<T> withCount(long count);
	}

	public interface WithMissingValue<T> {
		CountableObject<T> withValue(T value);
	}
}