package de.plush.brix.c64graphics.core.model;

import static java.lang.System.arraycopy;

import java.awt.Dimension;
import java.net.*;

import de.plush.brix.c64graphics.core.util.*;

/**
 * Standard hires bitmap + screen ram, as used by Hi Eddi, Doodle, Art Studio, Interpaint (Hires), ImageSystem (Hires), etc <br>
 *
 * @see #binaryProviderArtStudio()
 * @see #binaryProviderHiEddi()
 * @see #binaryProviderDoodle()
 * @author Wanja Gayk
 */
public class C64BitmapmodePictureHires implements IRollBlockwise {
	private final C64Bitmap bitmap;

	private final C64Screen screen;

	public C64BitmapmodePictureHires(final C64Bitmap bitmap, final C64Screen screen) {
		this.bitmap = new C64Bitmap(bitmap);
		this.screen = new C64Screen(screen);
	}

	public C64BitmapmodePictureHires(final C64BitmapmodePictureHires other) {
		bitmap = new C64Bitmap(other.bitmap);
		screen = new C64Screen(other.screen);
	}

	public Dimension sizeBlocks() {
		return C64Bitmap.sizeBlocks();
	}

	public Dimension sizePixels() {
		return C64Bitmap.sizePixels();
	}

	public void setBitmapBlock(final int xBlock, final int yBlock, final C64Char chrBlock) {
		bitmap.setBlock(xBlock, yBlock, chrBlock);
	}

	public C64Char bitmapBlockAt(final int xBlock, final int yBlock) {
		return bitmap.blockAt(xBlock, yBlock);
	}

	public void setScreenCode(final int x, final int y, final byte screenCode) {
		screen.setScreenCode(x, y, screenCode);
	}

	public byte screenCodeAt(final int x, final int y) {
		return screen.screenCodeAt(x, y);
	}

	public byte[][] screenCodeMatrix() {
		return screen.screenCodeMatrix();
	}

	/** @return a snapshot of the current bitmap. */
	public C64Bitmap bitmap() {
		return new C64Bitmap(bitmap);
	}

	/** @return a snapshot of the current screen. */
	public C64Screen screen() {
		return new C64Screen(screen);
	}

	@Override
	public void rollLeftBlockwise(final int n) {
		bitmap.rollLeftBlockwise(n);
		screen.rollLeftBlockwise(n);
	}

	@Override
	public void rollRightBlockwise(final int n) {
		bitmap.rollRightBlockwise(n);
		screen.rollRightBlockwise(n);
	}

	@Override
	public void rollUpBlockwise(final int n) {
		bitmap.rollUpBlockwise(n);
		screen.rollUpBlockwise(n);
	}

	@Override
	public void rollDownBlockwise(final int n) {
		bitmap.rollDownBlockwise(n);
		screen.rollDownBlockwise(n);
	}

	/**
	 * The binaries for HiEddi and ImageSystem look almost the same, they only differ by their load address, which is not included in the binary itself. <br>
	 * From <a href=
	 * "http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03">http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03</a>:<br>
	 * <style> #gfxformat th { background:lightgray; text-align: left; width:auto}</style> <style> #gfxformat td.first { width:18ch; }</style><style> #gfxformat
	 * td { width:auto}</style>
	 * <table id="gfxformat">
	 * <th colspan="2">Hi-Eddi (by Markt &amp; Technik) (pc-ext: .hed)</th>
	 * <tr>
	 * <td class="first">$2000 - $43ff</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$2000 - $3f3f</td>
	 * <td>Bitmap</td>
	 * <tr>
	 * <td class="first">$4000 - $43e7</td>
	 * <td>Screen RAM</td>
	 * </table>
	 *
	 * <table id="gfxformat">
	 * <th colspan="2">Image System (Hires) (pc-ext: .ish)</th>
	 * <tr>
	 * <td class="first">$4000 - $63e7</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$4000 - $5f3f</td>
	 * <td>Bitmap</td>
	 * <tr>
	 * <td class="first">$6000 - $63e7</td>
	 * <td>Screen RAM</td>
	 * </table>
	 * <table id="gfxformat">
	 *
	 * @return A provider that will provide the C64 binary of this image at the time this method was invoked.
	 */
	public C64BinaryProvider binaryProviderHiEddi() {
		return hiEddiFormat().binaryProvider(this);
	}

	public static C64BitmapmodePictureHires loadHiEddi(final URL url, final StartAddress startAddress) {
		return hiEddiFormat().load(Utils.readFile(url, startAddress));
	}

	public static C64BitmapmodePictureHires loadHiEddi(final URI uri, final StartAddress startAddress) {
		return hiEddiFormat().load(Utils.readFile(uri, startAddress));
	}

	public void saveHiEddi(final URI uri, final StartAddress startAddress) {
		hiEddiFormat().save(binaryProviderHiEddi(), uri, startAddress);
	}

	public static C64BitmapmodePictureHires fromHiEddiBinary(final byte[] binary) {
		return hiEddiFormat().load(binary);
	}

	private static C64HiresFormat hiEddiFormat() {
		return new C64HiresFormat()//
				.withLoadAddress(0x2000, 0x43FF)//
				.withBitmapAddress(0x2000)//
				.withScreenAddress(0x4000);
	}

	/** Redirects to {@link #binaryProviderHiEddi()}. */
	public C64BinaryProvider binaryProviderImageSystem() {
		return binaryProviderHiEddi();
	}

	/** just delegates to the hiEddi format, as they are the same. */
	public static C64BitmapmodePictureHires loadImageSystem(final URL url, final StartAddress startAddress) {
		return loadHiEddi(url, startAddress);
	}

	/** just delegates to the hiEddi format, as they are the same. */
	public static C64BitmapmodePictureHires loadImageSystem(final URI uri, final StartAddress startAddress) {
		return loadHiEddi(uri, startAddress);
	}

	/** just delegates to the hiEddi format, as they are the same. */
	public void saveImageSystem(final URI uri, final StartAddress startAddress) {
		saveHiEddi(uri, startAddress);
	}

	/** just delegates to the hiEddi format, as they are the same. */
	public static C64BitmapmodePictureHires fromImageSystemBinary(final byte[] binary) {
		return hiEddiFormat().load(binary);
	}

	/**
	 * The binaries for Interpaint Hires and ArtStudio (hires) look almost the same, they mainly differ by their load address, which is not included in the
	 * binary itself and the end address. Art Studio features a border color that is not part of this binary, so it defaults to 0 (black). <br>
	 * From <a href=
	 * "http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03">http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03</a>:<br>
	 * <style> #gfxformat th { background:lightgray; text-align: left; width:auto}</style> <style> #gfxformat td.first { width:18ch; }</style><style> #gfxformat
	 * td { width:auto}</style>
	 * <table id="gfxformat">
	 * <th colspan="2">Art Studio 1.0-1.1 (by OCP) (pc-ext: .aas;.art;.hpi)</th>
	 * <tr>
	 * <td class="first">$2000 - $432E</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$2000 - $3F3F</td>
	 * <td>Bitmap</td>
	 * <tr>
	 * <td class="first">$3F40 - $4327</td>
	 * <td>Screen RAM</td>
	 * <tr>
	 * <td class="first">$4328</td>
	 * <td>Border Color</td>
	 * </table>
	 * <table id="gfxformat">
	 * <th colspan="2">Interpaint Hires (pc-ext: .ip64h)</th>
	 * <tr>
	 * <td class="first">$4000 - $6327</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$4000 - $5F3F</td>
	 * <td>Bitmap</td>
	 * <tr>
	 * <td class="first">$5F40 - $6327</td>
	 * <td>Screen RAM</td>
	 * </table>
	 *
	 * @return A provider that will provide the C64 binary of this image at the time this method was invoked.
	 */
	public C64BinaryProvider binaryProviderArtStudio() {
		return artStudioFormat().binaryProvider(this);
	}

	public static C64BitmapmodePictureHires loadArtStudio(final URL url, final StartAddress startAddress) {
		return artStudioFormat().load(Utils.readFile(url, startAddress));
	}

	public static C64BitmapmodePictureHires loadArtStudio(final URI uri, final StartAddress startAddress) {
		return artStudioFormat().load(Utils.readFile(uri, startAddress));
	}

	public void saveArtStudio(final URI uri, final StartAddress startAddress) {
		artStudioFormat().save(binaryProviderArtStudio(), uri, startAddress);
	}

	public static C64BitmapmodePictureHires fromArtStudioBinary(final byte[] binary) {
		return artStudioFormat().load(binary);
	}

	private static C64HiresFormat artStudioFormat() {
		return new C64HiresFormat()//
				.withLoadAddress(0x2000, 0x432E)//
				.withBitmapAddress(0x2000)//
				.withScreenAddress(0x3F40);
	}

	public static C64BitmapmodePictureHires loadInterPaint(final URL url, final StartAddress startAddress) {
		return interPaintFormat().load(Utils.readFile(url, startAddress));
	}

	public static C64BitmapmodePictureHires loadInterPaint(final URI uri, final StartAddress startAddress) {
		return interPaintFormat().load(Utils.readFile(uri, startAddress));
	}

	public void saveInterPaint(final URI uri, final StartAddress startAddress) {
		interPaintFormat().save(binaryProviderInterPaint(), uri, startAddress);
	}

	public static C64BitmapmodePictureHires fromInterPaintBinary(final byte[] binary) {
		return interPaintFormat().load(binary);
	}

	/** {@link #binaryProviderArtStudio()} */
	public C64BinaryProvider binaryProviderInterPaint() {
		return interPaintFormat().binaryProvider(this);
	}

	private static C64HiresFormat interPaintFormat() {
		return new C64HiresFormat()//
				.withLoadAddress(0x4000, 0x6327)//
				.withBitmapAddress(0x4000)//
				.withScreenAddress(0x5F40);
	}

	/**
	 * From <a href=
	 * "http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03">http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03</a>:<br>
	 * <style> #gfxformat th { background:lightgray; text-align: left; width:auto}</style> <style> #gfxformat td.first { width:18ch; }</style><style> #gfxformat
	 * td { width:auto}</style>
	 * <table id="gfxformat">
	 * <th colspan="2">Doodle (by OMNI) (pc-ext: .dd;.ddl;.jj)</th>
	 * <tr>
	 * <td class="first">$5c00 - $7fff</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$5c00 - $5fe7</td>
	 * <td>Screen RAM</td>
	 * <tr>
	 * <td class="first">$7000 - $7f3f</td>
	 * <td>Bitmap</td>
	 * </table>
	 * Doodle images might also load at $1c00-$3fff, and $5c00-$807f. (For more information on the Doodle format, see C= Hacking issue 11). Doodle images with
	 * extension .jj are RLE compressed. Escape code is $FE and is followed by one byte indicating the value to repeat and another byte indicating how many
	 * repetitions of that byte are to be generated.
	 *
	 * @return A provider that will provide the C64 binary of this image at the time this method was invoked.<br>
	 *         <b>Note</b> this uses the uncompressed format from $1c00-$3fff.
	 */
	public C64BinaryProvider binaryProviderDoodle() {
		return doodleFormat().binaryProvider(this);
	}

	// TODO: support compressed formats, see C64BitmapmodePictureMulticolor#binaryProviderAmicaPaint()

	public static C64BitmapmodePictureHires loadDoodle(final URL url, final StartAddress startAddress) {
		return doodleFormat().load(Utils.readFile(url, startAddress));
	}

	public static C64BitmapmodePictureHires loadDoodle(final URI uri, final StartAddress startAddress) {
		return doodleFormat().load(Utils.readFile(uri, startAddress));
	}

	public void saveDoodle(final URI uri, final StartAddress startAddress) {
		doodleFormat().save(binaryProviderDoodle(), uri, startAddress);
	}

	public static C64BitmapmodePictureHires fromDoodleBinary(final byte[] binary) {
		return doodleFormat().load(binary);
	}

	private static C64HiresFormat doodleFormat() {
		return new C64HiresFormat()//
				.withLoadAddress(0x1c00, 0x3fff)//
				.withScreenAddress(0x1c00)//
				.withBitmapAddress(0x2000);
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (bitmap == null ? 0 : bitmap.hashCode());
		result = prime * result + (screen == null ? 0 : screen.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final C64BitmapmodePictureHires other = (C64BitmapmodePictureHires) obj;
		if (bitmap == null) {
			if (other.bitmap != null) { return false; }
		} else if (!bitmap.equals(other.bitmap)) { return false; }
		if (screen == null) {
			if (other.screen != null) { return false; }
		} else if (!screen.equals(other.screen)) { return false; }
		return true;
	}

	private static class C64HiresFormat {
		int loadAddress, endAddress, bitmapAddress, screenAddress;

		int length, bitmapOffset, screenOffset;

		protected C64HiresFormat withLoadAddress(final int loadAddress, final int endAddress) {
			this.loadAddress = loadAddress;
			this.endAddress = endAddress;
			return this;
		}

		C64HiresFormat withBitmapAddress(final int bitmapAddress) {
			this.bitmapAddress = bitmapAddress;
			return this;
		}

		C64HiresFormat withScreenAddress(final int screensAddress) {
			screenAddress = screensAddress;
			return this;
		}

		void calcOffsets() {
			length = endAddress - loadAddress + 1; // $4000 - $4000 is one byte in the codebase64 spec.
			bitmapOffset = bitmapAddress - loadAddress;
			screenOffset = screenAddress - loadAddress;
		}

		C64BitmapmodePictureHires load(final byte[] binary) {
			calcOffsets();
			final byte[] bitmapBinary = new byte[C64Bitmap.WIDTH_BLOCKS * C64Bitmap.HEIGHT_BLOCKS * C64Char.HEIGHT_PIXELS];
			System.arraycopy(binary, bitmapOffset, bitmapBinary, 0, bitmapBinary.length);
			final C64Bitmap bitmap = C64Bitmap.binaryToC64Bitmap(bitmapBinary);

			final byte[] screenBinary = new byte[C64Screen.WIDTH_BLOCKS * C64Screen.HEIGHT_BLOCKS];
			System.arraycopy(binary, screenOffset, screenBinary, 0, screenBinary.length);
			final C64Screen screen = C64Screen.fromBinary(screenBinary);

			return new C64BitmapmodePictureHires(bitmap, screen);
		}

		void save(final C64BinaryProvider binaryProvider, final URI uri, final StartAddress startAddress) {
			switch (startAddress) {
			case InFile -> Utils.writeFile(uri, binaryProvider.toC64Binary(), loadAddress);
			case NotInFile -> Utils.writeFile(uri, binaryProvider.toC64Binary());
			}
		}

		C64BinaryProvider binaryProvider(final C64BitmapmodePictureHires obj) {
			calcOffsets();
			final byte[] binary = new byte[length];

			final byte[] screenBinary = obj.screen().toC64Binary();
			arraycopy(screenBinary, 0, binary, screenOffset, screenBinary.length);
			final byte[] bitmapBinary = obj.bitmap().toC64Binary();
			arraycopy(bitmapBinary, 0, binary, bitmapOffset, bitmapBinary.length);

			return () -> binary;
		}
	}
}
