package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.image.BufferedImage;
import java.io.StringReader;
import java.net.URI;

import com.kitfox.svg.SVGUniverse;
import com.kitfox.svg.app.beans.SVGIcon;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

/**
 * @author Wanja Gayk
 */
public class SvgStringToImage implements IPipelineStage<String, BufferedImage> {

	@Override
	public BufferedImage apply(final String original) {
		final SVGUniverse svgUniverse = new SVGUniverse();
		final URI uri = svgUniverse.loadSVG(new StringReader(original), "temp");
		// SVGDiagram diagram = svgUniverse.getDiagram(uri);
		// diagram.render(g);
		final SVGIcon icon = new SVGIcon();
		icon.setSvgUniverse(svgUniverse);
		icon.setSvgURI(uri);
		return (BufferedImage) icon.getImage();
	}

}
