package de.plush.brix.c64graphics.core;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.emitters.ImageEmitter;
import de.plush.brix.c64graphics.core.model.C64CharsetScreen;
import de.plush.brix.c64graphics.core.pipeline.PipelineConsumer;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay;
import de.plush.brix.c64graphics.core.pipeline.displays.ImageDisplay.Grid;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.HiEddiWithHiresSpriteOverlayColorClashResolver;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.FindMostCommonColor;
import de.plush.brix.c64graphics.core.pipeline.stages.misc.FindMostCommonColor.Calculate;

/** Main class used for starting the conversion. */
public final class ImageConverter {

	private ImageConverter() {}

	final static String dataDir;
	static {
		try {
			dataDir = new File(".").getCanonicalPath() + "/target/classes/data/";
		} catch (final IOException e) {
			throw new IllegalStateException(e);
		}
	}

	public static void main(final String[] args) {
		daniOliver();
	}

	private static void daniOliver() {

		final String picName = "Geisha-by-Luca-Tarlazzi-513x640.jpg";
		// final String picName = "geisha-lovely-woman-face-1.jpg";
		// final String picName = "lena.bmp";
		// final String picName = "godfather.jpg";
		// final String charsetFile = "src/data/out/charset_" + methodName + ".bin";
		// final String screensFile = "src/data/out/screens_" + methodName + ".rle.bin";

		final File imageFile = new File(dataDir + picName);

		// final Supplier<Color> backgroundColor = () -> Color.WHITE;
		// final Color pixelColor = Color.BLACK;

		// final File fileOut = new File("src/data/out/ahaFile.bin");

		final List<AutoCloseable> resourcesToClose = new ArrayList<>();

		final Palette palette = C64ColorsColodore.PALETTE;
		final ColorDistance colorDistanceMeasure = ColorDistance.EUCLIDEAN_WEIGHTED;
		final Dithering dithering = Dithering.Atkinson;

		final FindMostCommonColor backgroundProviderRaw = new FindMostCommonColor(Calculate.FIRST_FRAME_AND_REUSE);
		final FindMostCommonColor backgroundProviderC64 = new FindMostCommonColor(Calculate.FIRST_FRAME_AND_REUSE);

		@SuppressWarnings("resource")
		final Function<BufferedImage, BufferedImage> pipeline //
				= new ImageDisplay<BufferedImage>(1, "original", resourcesToClose::add) //
						.andThen(backgroundProviderRaw)//
						.andThen(new Rescale(C64CharsetScreen.sizePixels(), backgroundProviderRaw))//
						// .andThen(new ImageDisplay<BufferedImage>(2, "scaled", closeThisInTheEnd))//
						.andThen(new PaletteConversion(palette, colorDistanceMeasure, dithering))//
						.andThen(new ImageDisplay<>(2.85f, "applied " + palette + " " + colorDistanceMeasure + " " + dithering, Grid.sizeC64Char,
								resourcesToClose::add))//
						// .andThen(new Pixelize(new Dimension(2, 1)))//
						//.andThen(new PaletteConversion(palette, colorDistanceMeasure, Dithering.None))//
						
						// .andThen(new ImageDisplay<BufferedImage>(2, "pixelized", closeThisInTheEnd))//

						.andThen(backgroundProviderC64)//

						// .andThen(new ImageToC64CharsetScreenColorRamHires(palette, colorDistanceMeasure, CharacterReduction.UsingImageColorsOnly,
						// new CharmodeColorClashResolverHires(colorDistanceMeasure)))//
						// .andThen(new C64CharsetScreenColorRamToImage(palette))//

						// .andThen(new MulticolorFLIColorClashResolver(palette, backgroundProviderC64, colorDistanceMeasure))//
						// .andThen(new ImageDisplay<>(2.85f, "clashes removed", Grid.sizeC64Char, resourcesToClose::add))//
						// .andThen(new ImageToC64BitmapmodePictureMulticolorFli(palette,
						// new MulticolorFLIColorClashResolver(palette, backgroundProviderC64, colorDistanceMeasure)))
						// .andThen(new C64BitmapmodePictureMulticolorFliToImage(palette))

						// .andThen(new ImageToC64BitmapmodePictureHiresFli(palette, new HiresFLIColorClashResolver(colorDistanceMeasure)))
						// .andThen(new C64BitmapmodePictureHiresFliToImage(palette))

						// .andThen(new ImageToC64CharmodePictureMulticolor(palette, colorDistanceMeasure, CharacterReduction.UsingImageColorsOnly,
						// new CharmodeColorClashResolverMulticolor(palette, colorDistanceMeasure)//
						// .withForcedMulticolorBlocksAt(new Point(20, 8), new Point(20, 9), new Point(20, 10))//
						// .andThen(new ImageDisplay<>(2.85f, "clashes removed", Grid.Invisible, resourcesToClose::add))))//
						// .andThen(new C64CharmodePictureToImageMulticolor(palette))//

						// .andThen(new ImageToC64BitmapmodePictureMulticolor(palette, new KoalaColorClashResolver(backgroundProviderC64,
						// colorDistanceMeasure))//
						// // .withPreferredMulticolor1D022Colors(C64ColorsColodore.BLACK.color)//
						// )//
						// .andThen(new C64BitmapmodePictureMulticolorToImage(palette))//

						// .andThen(new ImageToC64BitmapmodePictureHires(palette, new HiEddiColorClashResolver(colorDistanceMeasure))//
						// .withPreferredBackgroundColors(C64ColorsColodore.WHITE.color, C64ColorsColodore.DARK_GRAY.color))//
						// .andThen(new C64BitmapmodePictureHiresToImage(palette))//

						.andThen(new ImageToC64BitmapmodePictureHiresWithHiresSpriteOverlay(palette,
								new HiEddiWithHiresSpriteOverlayColorClashResolver(new Dimension(6, 9), new Point(12, 1), colorDistanceMeasure)
						// .andThen(new ImageDisplay<>(2.85f, "clashes removed", Grid.sizeC64Char, resourcesToClose::add))
						))//
						.andThen(new C64BitmapmodePictureHiresWithHiresSpriteOverlayToImage(palette)) //

						// .andThen(new HiEddiColorClashResolver(colorDistanceMeasure))//
						// .andThen(new CharmodeColorClashResolverMulticolor(palette, colorDistanceMeasure))//
						.andThen(new ImageDisplay<>(3.2f, "result " + palette + " " + colorDistanceMeasure, Grid.Invisible, resourcesToClose::add))//
		// .andThen(new ImageToC64Bitmap())//

		;

		final var consumer = new PipelineConsumer<>(pipeline, resourcesToClose);
		final ImageEmitter emitter = new ImageEmitter(imageFile, consumer);
		try (emitter; consumer) {
			emitter.run();
		}
		System.exit(0);
	}

}
