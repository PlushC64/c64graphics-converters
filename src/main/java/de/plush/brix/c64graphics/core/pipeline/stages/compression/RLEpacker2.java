package de.plush.brix.c64graphics.core.pipeline.stages.compression;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;

import java.util.*;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.util.collections.ByteArrayList;

/**
 * A stateless, simple RLE-packer that uses a "header+data" scheme instead of marker bytes.
 * 
 * <dl>
 * <dt>Run Sequence
 * <dd>[(count-1), byte], where 1 &lt; count &lt;= 128
 * <dt>Copy Sequence
 * <dd>[(count-1) &amp; 0x080, byte0, ...., byte n], where 0 &lt; count &lt;= 128
 * <dt>End Sequence
 * <dd>[0]
 * </dl>
 * Example:<br/>
 * 0x01,0x01,0x01,0x82,0x03,0x04,0x05,0x00 unpacks to: 1,1,2,3,4,5<br>
 * Explanation:<br/>
 * 0x01, 0x01 -&gt; unfold 2x: 1<br>
 * 0x80, 0x02 -&gt; literal 1x: 2<br>
 * 0x82, 0x03, 0x04, 0x05 -> literal 3x: 3,4,5<br>
 * 0x00 -&gt; endmark<br>
 * 
 */
public class RLEpacker2 implements Function<byte[], byte[]> {

	public RLEpacker2() {}

	@Override
	public byte[] apply(final byte[] input) {
		final byte[] packed = toRLEencodedBinary(createRuns(input));
		System.out.println("raw length: " + input.length + " -> " + packed.length + " (bytes)");
		return packed;
	}

	private static byte[] toRLEencodedBinary(final Iterable<Run> runs) {
		final ByteArrayList output = ByteArrayList.empty();
		for (final Run run : runs) {
			if (run.ofEqualChars) {
				output.add(toUnsignedByte(run.count - 1));
			} else {
				output.add(toUnsignedByte(run.count - 1 | 0x80));
			}
			output.addAll(run.bytes);
		}
		// set endmark:
		output.add((byte) 0);
		return output.toArray();
	}

	private static List<Run> createRuns(final byte[] screenBinary) {
		final List<Run> runs = createRunsOfOne(screenBinary);
		compactRunsOfEqualCharacters(runs);
		compactRunsOfIndividualCharacters(runs);
		return runs;
	}

	private static void compactRunsOfEqualCharacters(final Iterable<Run> runs) {
		final Iterator<Run> iterator = runs.iterator();
		Run head = iterator.hasNext() ? iterator.next() : null;
		while (head != null && iterator.hasNext()) {
			final Run tail = iterator.next();
			if (head.bytes.get(0) == tail.bytes.get(0) && head.count < 0x7f) {
				assert tail.count == 1 : "tail count is expected to be 1";
				head.ofEqualChars = true;
				head.count += 1;
				iterator.remove();
			} else {
				head = tail;
			}
		}
	}

	private static void compactRunsOfIndividualCharacters(final Iterable<Run> runs) {
		final Iterator<Run> iterator = runs.iterator();
		Run head = iterator.hasNext() ? iterator.next() : null;
		while (head != null && iterator.hasNext()) {
			final Run tail = iterator.next();
			if (!head.ofEqualChars && !tail.ofEqualChars && head.count < 0x7f) {
				assert tail.count == 1 : "tail count is expected to be 1, if individual character";
				head.count += 1;
				head.bytes.addAll(tail.bytes);
				iterator.remove();
			} else {
				head = tail;
			}
		}
	}

	private static LinkedList<Run> createRunsOfOne(final byte[] screenBinary) {
		final LinkedList<Run> runs = new LinkedList<>();
		for (int t = 0; t < screenBinary.length; ++t) {
			final var run = new Run();
			run.count = 1;
			run.bytes = ByteArrayList.of(screenBinary[t]);
			runs.add(run);
		}
		return runs;
	}

	private static class Run {
		boolean ofEqualChars;

		int count;

		ByteArrayList bytes;
	}

	public void printSampleDepackerSource() {
		//@formatter:off
		System.out.println( 
"""

.const target = $1000 // where the depacked code should start 
.pc= $0810
            lda #<target
            sta unfold+1
            lda #>target
            sta unfold+2

unpack:     jsr getbyte // get header (bit 7 = switch, rest: run length)
            beq done    // zero header: bail out
            asl         // check upper bit switch in carry, if clear: unfold, if set copy
            ldx #unfold-branch-2  // set branch for unfold
            bcc setbranch
            ldx #copy-branch-2    // set branch for copy
setbranch:  stx branch+1 
            lsr // upper bit cleared, we have the run length
            tax
copy:       jsr getbyte
unfold:     sta $ffff
            inc unfold+1
            bne wb_over
            inc unfold+2
wb_over:    dex
branch:     bpl unfold // bpl branches on 0, so we get 128 bytes max
            bmi unpack
getbyte:    inc getpos+1
            bne getpos
            inc getpos+2 
getpos:     lda compressed-1
done:       rts
compressed:
            .byte $00 | $01, 1          // unfold 2x 1
            .byte $80 | $00, 2          // literal 1x, "2"
            .byte $80 | $02, 3, 4, 5    // literal 3x, "3,4,5"
            .byte 0 // end mark

""");//@formatter:on
	}

}
