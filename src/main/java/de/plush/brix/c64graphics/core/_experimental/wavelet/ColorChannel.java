package de.plush.brix.c64graphics.core._experimental.wavelet;

public enum ColorChannel {
	Alpha(24), //
	Red(16), //
	Green(8), //
	Blue(0);

	private int shift;

	private int mask;

	private int inverseMask;

	ColorChannel(final int shift) {
		this.shift = shift;
		mask = 0xff << shift;
		inverseMask = ~mask;
	}

	public int filterRgb(final int rgb) {
		return rgb & mask;
	}

	public int toRgb(final int gray) {
		return (gray & 0xFF) << shift;
	}

	public int fromRgb(final int rgb) {
		return rgb >> shift & 0xFF;
	}

	public int replace(final int rgb, final int containsReplacement) {
		return rgb & inverseMask | containsReplacement & mask;
	}

	public int mask() {
		return mask;
	}

	public int inverseMask() {
		return inverseMask;
	}

}