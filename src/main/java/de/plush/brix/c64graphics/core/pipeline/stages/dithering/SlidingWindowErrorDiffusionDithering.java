package de.plush.brix.c64graphics.core.pipeline.stages.dithering;

import static java.lang.Math.min;
import static java.lang.Runtime.getRuntime;
import static java.lang.Thread.currentThread;
import static java.util.Arrays.stream;
import static java.util.concurrent.Executors.newFixedThreadPool;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.util.collections.LRUMap;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

final class SlidingWindowErrorDiffusionDithering implements IPipelineStage<BufferedImage, BufferedImage> {

	private final DitheringSpec ditheringSpec;

	private final Dimension kernelSize;

	private ExecutorService threadPool;

	private final Object threadPoolLock = new Object();

	private volatile boolean poolBusy;

	private final ConcurrentHashMap<Thread, Map<Color, Color>> caches = new ConcurrentHashMap<>(getRuntime().availableProcessors() * 2 + 1, 0.75f, getRuntime()
			.availableProcessors());

	private final Function<Color, Color> computeClosestColorFunction;

	SlidingWindowErrorDiffusionDithering(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure, final DitheringSpec ditheringSpec) {
		this.ditheringSpec = ditheringSpec;
		kernelSize = ditheringSpec.matrixSize();

		final var targetPaletteColors = palette.colors();
		computeClosestColorFunction = originalColor -> stream(targetPaletteColors)//
				.min(colorDistanceMeasure.comparingDistanceTo(originalColor))//
				.get();
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());
		final Graphics2D g = (Graphics2D) result.getGraphics();
		try {
			poolBusy = true;
			initThreadPool();
			// dithering works "in place", so original colors must be in result image prior to error distribution
			g.drawImage(original, 0, 0, null);

			// For small source pictures, like 320x200 with 16 colors, single threaded dithering is actually a lot faster.
			// But as soon as you've got like 320x200 with 256 colors, or 1024x768 with 16 colors, multithreaded wins according to jmh tests.
			LineRunner leader = null;
			Future<?> lastLine = null;
			for (int y = 0; y < original.getHeight(); ++y) {
				leader = new LineRunner(y, result, leader); // first leader is null (expected)
				lastLine = threadPool.submit(leader);
			}
			if (lastLine != null) {
				lastLine.get();
			}
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		} finally {
			g.dispose();
			poolBusy = false;
		}
		return result;
	}

	/**
	 * @implNote removed clunky synchronized wait/notify in favor of busy waiting, since the time slices are too small to have an effect.
	 */
	private class LineRunner implements Runnable {

		private final LineRunner leader;

		private final BufferedImage result;

		private final int y;

		// Since it is increasing monotonously, this is actually not even be needed to use volatile to avoid confusion.
		// On one hand volatile increases memory access overhead, on the other hand it helps other threads to see a result earlier.
		// I did not fully explore the tradeoff, but using volatile is the more reliable choice.
		@SuppressFBWarnings(value = "VO_VOLATILE_INCREMENT", justification = "Only one thread is increasing 'x'. It's okay that the increase of x is not atomic")
		volatile int x;

		volatile boolean finished;

		LineRunner(final int y, final BufferedImage result, final LineRunner leader) {
			this.y = y;
			this.result = result;
			this.leader = leader;
		}

		@Override
		public void run() {
			try {
				for (x = 0; x < result.getWidth(); ++x) {
					if (leader != null) {
						waitForLeader();
					}
					final Color originalColor = new Color(result.getRGB(x, y)); // do not use "original.getRGB()" or dithering will fail
					final Color newColor = clostestColorTo(originalColor);
					result.setRGB(x, y, newColor.getRGB());
					errorDistributionDithering(result, new Point(x, y), originalColor, newColor, kernelSize);
				}
			} finally {
				finished = true;
			}
		}

		private void waitForLeader() {
			while (!canContinue()) {
				Thread.yield(); // just hint the scheduler that it may interrupt the thread here
			}
		}

		private boolean canContinue() {
			final Point globalPos = new Point(x, y);
			final Point kernelCenter = ditheringSpec.centerPoint(globalPos);
			final int myRightSlidingWindowEdge = x + kernelSize.width - kernelCenter.x;
			final int leaderLeftSlidingWindowEdge = leader.x - kernelCenter.x;
			return leader.finished || myRightSlidingWindowEdge < leaderLeftSlidingWindowEdge;
		}

		private void errorDistributionDithering(final BufferedImage result, final Point globalPos, final Color originalColor, final Color newColor,
				final Dimension kernelSize) {
			final int redErr = originalColor.getRed() - newColor.getRed();
			final int greenErr = originalColor.getGreen() - newColor.getGreen();
			final int blueErr = originalColor.getBlue() - newColor.getBlue();

			for (int yKernel = 0; yKernel < kernelSize.height; ++yKernel) {
				for (int xKernel = 0; xKernel < kernelSize.width; ++xKernel) {
					final Point kernelRelative = ditheringSpec.centerPointOffset(new Point(xKernel, yKernel), globalPos);
					final Point kernelPos = new Point(xKernel, yKernel);
					final OptionalDouble errorMultiplier = ditheringSpec.errorMultiplier(kernelPos, globalPos);
					if (errorMultiplier.isPresent() // no distribution -> skip
							&& !outOfBounds(result.getWidth(), globalPos.x, kernelRelative.x) && !outOfBounds(result.getHeight(), globalPos.y,
									kernelRelative.y)) {
						final int x = globalPos.x + kernelRelative.x;
						final int y = globalPos.y + kernelRelative.y;
						final Color current = new Color(result.getRGB(x, y));

						final double weight = errorMultiplier.getAsDouble();
						final int newRed = min(0xFF, Math.max(0, (int) (current.getRed() + redErr * weight)));
						final int newGreen = min(0xFF, Math.max(0, (int) (current.getGreen() + greenErr * weight)));
						final int newBlue = min(0xFF, Math.max(0, (int) (current.getBlue() + blueErr * weight)));
						final Color colorWithErr = new Color(newRed, newGreen, newBlue);
						result.setRGB(x, y, colorWithErr.getRGB());
					}
				}
			}
		}
	}

	static boolean outOfBounds(final int max, final int center, final int offset) {
		return center + offset < 0 || center + offset > max - 1;
	}

	/*
	 * Note: I tried a common cache and a synchronized block, that was extremely slow a common cache guarded by a spin lock was also significantly slower
	 * than
	 * using one cache for each thread.
	 * Having no cache at all was slightly better for multitreaded dithering on a rich color set, but significantly harms straight application of a palette
	 * on
	 * an already reduced palette.
	 */
	private Color clostestColorTo(final Color originalColor) {
		final Map<Color, Color> closestColorsCache = caches.computeIfAbsent(currentThread(), thread -> newCache());
		return closestColorsCache.computeIfAbsent(originalColor, computeClosestColorFunction);
	}

	private static LinkedHashMap<Color, Color> newCache() {
		return new LRUMap<>(1024 * 64); // cache most frequently used 64.000 colors:
	}

	/**
	 * Create a thread pool that shuts itself down after some idle time (shutdown-hook will not fire while this pool's threads are running!). Just returns
	 * the last thread pool if it's not yet shut down.
	 */
	private void initThreadPool() {
		synchronized (threadPoolLock) {
			if (threadPool == null || threadPool.isTerminated()) {
				threadPool = newFixedThreadPool(getRuntime().availableProcessors() + 1);
				getRuntime().addShutdownHook(new Thread(threadPool::shutdownNow));
				threadPool.submit(() -> {
					synchronized (threadPoolLock) {
						while (poolBusy && !Thread.currentThread().isInterrupted()) {
							try {
								TimeUnit.SECONDS.timedWait(threadPoolLock, 5);
							} catch (final InterruptedException e) {
								// we don't care if the shutter is shut while it's waiting to shut
							}
						}
						threadPool.shutdown();
						// Important: pool.isTerminated() does not suffice, it may not be terminated while this operation is still running.
						threadPool = null;
					}
				});
			}
		}
	}

}