package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.pipeline.stages.manipulation.Alignment.Horizontal.LEFT;
import static de.plush.brix.c64graphics.core.pipeline.stages.manipulation.Alignment.Vertical.TOP;
import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;
import static de.plush.brix.c64graphics.core.util.UtilsImage.colors;
import static java.lang.Math.*;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.HiEddiWithHiresSpriteOverlayColorClashResolver.PreProcessedImage;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;

public class ImageToC64BitmapmodePictureHiresWithHiresSpriteOverlay implements IPipelineStage<BufferedImage, C64BitmapmodePictureHiresWithHiresSpriteOverlay> {

	private final Function<BufferedImage, PreProcessedImage> colorClashResolver;
	private final Palette palette;
	private List<Color> preferredBackgroundColors = emptyList();
	private List<Color> preferredForegroundColors = emptyList();

	public ImageToC64BitmapmodePictureHiresWithHiresSpriteOverlay(final Palette palette, final Function<BufferedImage, PreProcessedImage> colorClashResolver) {
		this.palette = palette;
		this.colorClashResolver = colorClashResolver;
	}

	public ImageToC64BitmapmodePictureHiresWithHiresSpriteOverlay withPreferredBackgroundColors(final Color... colors) {
		preferredBackgroundColors = asList(colors);
		preferredBackgroundColors.forEach(palette::colorCodeOrException);
		checkSettingsConsistency();
		return this;
	}

	public ImageToC64BitmapmodePictureHiresWithHiresSpriteOverlay withPreferredForegroundColors(final Color... colors) {
		preferredForegroundColors = asList(colors);
		preferredForegroundColors.forEach(palette::colorCodeOrException);
		checkSettingsConsistency();
		return this;
	}

	private void checkSettingsConsistency() {
		final Set<Color> x = preferredBackgroundColors.stream().filter(preferredForegroundColors::contains).collect(toSet());
		if (!x.isEmpty()) {
			throw new IllegalArgumentException("You can't force these colors to be both background and foreground: " + x);
		}
	}

	@Override
	public C64BitmapmodePictureHiresWithHiresSpriteOverlay apply(final BufferedImage original) {
		final PreProcessedImage sanitized = colorClashResolver.apply(original);

		final Dimension overlaySizeSprites = sanitized.spriteOverlaySizeSprites();
		final Point spriteOverlayLocationAtBlock = sanitized.spriteOverlayLocationAtBlock();
		final Rectangle overlayAreaPix = overlayAreaPix(overlaySizeSprites, spriteOverlayLocationAtBlock);
		final Color spriteColor = sanitized.spriteLayerColor();
		final byte spriteOverlayColorCode = palette.colorCodeOrException(spriteColor);

		final C64SpriteGrid spriteOverlay = spriteGridFromOverlayArea(sanitized, overlaySizeSprites, overlayAreaPix, spriteColor);
		final C64BitmapmodePictureHires c64BitmapmodePictureHires = hiresPicFromWholeImage(sanitized, overlayAreaPix, spriteColor);

		return new C64BitmapmodePictureHiresWithHiresSpriteOverlay(c64BitmapmodePictureHires, //
				spriteOverlay, spriteOverlayColorCode, spriteOverlayLocationAtBlock);
	}

	@SuppressWarnings("checkstyle:CyclomaticComplexity") // found no meaningful way to extract methods that make it more readable
	private C64BitmapmodePictureHires hiresPicFromWholeImage(final PreProcessedImage sanitized, final Rectangle overlayAreaPix, final Color spriteColor) {
		final C64Bitmap bitmap = new C64Bitmap();
		final C64Screen screen = new C64Screen((byte) 0);

		final Dimension blocks = C64Bitmap.sizeBlocks();
		for (int y = 0; y < blocks.height; ++y) {
			for (int x = 0; x < blocks.width; ++x) {
				final int xPix = x * C64Char.WIDTH_PIXELS;
				final int yPix = y * C64Char.HEIGHT_PIXELS;
				BufferedImage blockImage = sanitized.getSubimage(xPix, yPix, C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS);
				final var blockAreaPix = new Rectangle(xPix, yPix, C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS);
				final boolean insideOverlay = overlayAreaPix.contains(blockAreaPix);

				final List<Color> bitmapColors = colors(blockImage).stream() //
						.filter(color -> !(insideOverlay && color.equals(spriteColor))) // only remove sprite color, if inside overlay
						.collect(toList());
				bitmapColors.sort(UtilsColor.SORT_BY_BRIGHTNESS); // to get some predictable patterns
				// note that the sprite background color is not considered:
				Color background = preferredBackgroundColors.stream().filter(bitmapColors::contains).findFirst().orElse(null);
				Color foreground = preferredForegroundColors.stream().filter(bitmapColors::contains).findFirst().orElse(null);
				Stream.of(background, foreground).filter(c -> c != null).forEach(bitmapColors::remove);
				// list of colors has no preferred color left, from here we can assign colors that are not preferred by anyone to empty slots
				final Iterator<Color> colorsLeft = bitmapColors.iterator();
				background = background == null ? colorsLeft.hasNext() ? colorsLeft.next() : palette.color((byte) 0) : background;
				foreground = foreground == null ? colorsLeft.hasNext() ? colorsLeft.next() : palette.color((byte) 0) : foreground;
				if (insideOverlay) {
					final var _background = background; // for use in function.
					blockImage = new ColorReplacement(color -> color.equals(spriteColor) ? _background : color).apply(blockImage);
				}
				Stream.of(background, foreground).forEach(bitmapColors::remove);
				if (!bitmapColors.isEmpty()) {
					throw new IllegalStateException(
							"unexpectedly found too many colors in block (x=" + x + ", y=" + y + "). Have you solved all color clashes?");
				}
				final C64Char block = new ImageToC64CharHires(background, foreground).apply(blockImage);
				bitmap.setBlock(x, y, block);
				screen.setScreenCode(x, y, toUnsignedByte(palette.colorCodeOrException(foreground) << 4 | palette.colorCodeOrException(background)));
			}
		}
		return new C64BitmapmodePictureHires(bitmap, screen);
	}

	private static C64SpriteGrid spriteGridFromOverlayArea(final PreProcessedImage sanitized, final Dimension overlaySizeSprites,
			final Rectangle overlayAreaPix, final Color spriteColor) {
		final BufferedImage overlayImage = sanitized.getSubimage(//
				max(0, overlayAreaPix.x), //
				max(0, overlayAreaPix.y), //
				min(overlayAreaPix.width, sanitized.getWidth() - overlayAreaPix.x), //
				min(overlayAreaPix.height, sanitized.getHeight() - overlayAreaPix.y));
		final Color nonSpriteColor = Arrays.stream(C64ColorsColodore.PALETTE.colors()).filter(color -> !color.equals(spriteColor)).findFirst().get();

		final BufferedImage rightSizedOverlayImage = new ResizeCanvas(overlayAreaPix.getSize(), () -> nonSpriteColor, LEFT, TOP).apply(overlayImage);
		// new ImageDisplay<>(3f, Grid.sizeC64Sprite).show(rightSizedOverlayImage, "overlay");
		final BufferedImage[][] slices = new ImageSlicer(overlaySizeSprites, C64Sprite.sizePixels()).apply(rightSizedOverlayImage);
		return new ImageSpriteSizedSlicesToSpriteGridHires(spriteColor, overlaySizeSprites).apply(slices);
	}

	private static Rectangle overlayAreaPix(final Dimension overlaySizeSprites, final Point spriteOverlayLocationAtBlock) {
		final Dimension overlaySizePixels = new Dimension(overlaySizeSprites.width * C64Sprite.WIDTH_PIXELS,
				overlaySizeSprites.height * C64Sprite.HEIGHT_PIXELS);
		return new Rectangle(//
				new Point(spriteOverlayLocationAtBlock.x * C64Char.WIDTH_PIXELS, //
						spriteOverlayLocationAtBlock.y * C64Char.HEIGHT_PIXELS), //
				overlaySizePixels);
	}
}
