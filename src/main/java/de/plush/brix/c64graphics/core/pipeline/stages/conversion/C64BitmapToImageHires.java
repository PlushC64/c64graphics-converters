package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.function.*;

import de.plush.brix.c64graphics.core.model.C64Char;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;

public class C64BitmapToImageHires extends AbstractC64BitmapToImage {

	private final Palette palette;

	private final Supplier<Color> backgroundColorProvider;

	private final Color pixelColor;

	public C64BitmapToImageHires() {
		this(C64ColorsColodore.PALETTE, () -> C64ColorsColodore.BLACK.color, C64ColorsColodore.WHITE.color);
	}

	public C64BitmapToImageHires(final Palette palette) {
		this(palette, () -> palette.color((byte) 0), palette.color((byte) 1));
	}

	public C64BitmapToImageHires(final Palette palette, final Supplier<Color> backgroundColorProvider, final Color pixelColor) {
		this.palette = palette;
		this.backgroundColorProvider = backgroundColorProvider;
		this.pixelColor = pixelColor;
	}

	@Override
	protected Function<C64Char, ? extends BufferedImage> createCharToImage() {
		return new C64CharToImageHires(palette, backgroundColorProvider, pixelColor);
	}

}
