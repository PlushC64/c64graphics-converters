package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

abstract class AbstractCharmodePictureToImage implements IPipelineStage<C64CharsetScreenColorRam, BufferedImage> {

	protected abstract IPipelineStage<C64Char, PreProcessedCharacterImage> createC64CharToImage(final byte backgroundColorCode, final byte mc1colorCode,
			final byte mc2colorCode, final byte d800ColorCode);

	@Override
	public BufferedImage apply(final C64CharsetScreenColorRam input) {
		final byte backgroundColorCode = input.backgroundColorCode();
		final byte mc1colorCode = input.mc1_D022ColorCode();
		final byte mc2colorCode = input.mc2_D023ColorCode();
		final int xPix = C64CharsetScreen.WIDTH_BLOCKS * C64Char.WIDTH_PIXELS;
		final int yPix = C64CharsetScreen.HEIGHT_BLOCKS * C64Char.HEIGHT_PIXELS;
		final BufferedImage result = new BufferedImage(xPix, yPix, BufferedImage.TYPE_INT_RGB);
		final Graphics2D g = result.createGraphics();
		try {
			for (int yBlock = 0; yBlock < C64CharsetScreen.HEIGHT_BLOCKS; ++yBlock) {
				for (int xBlock = 0; xBlock < C64CharsetScreen.WIDTH_BLOCKS; ++xBlock) {
					final IPipelineStage<C64Char, PreProcessedCharacterImage> c64CharToImage = createC64CharToImage(backgroundColorCode, mc1colorCode,
							mc2colorCode, input.colorCodeAt(xBlock, yBlock));
					final BufferedImage blockImage = c64CharToImage.apply(input.blockAt(xBlock, yBlock));
					g.drawImage(blockImage, xBlock * C64Char.WIDTH_PIXELS, yBlock * C64Char.HEIGHT_PIXELS, null);
				}
			}
		} finally {
			g.dispose();
		}
		return result;
	}
}
