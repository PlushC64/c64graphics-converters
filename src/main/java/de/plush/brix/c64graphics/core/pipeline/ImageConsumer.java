package de.plush.brix.c64graphics.core.pipeline;

/**
 * Before implementing this, have a look at {@link PipelineConsumer}, it probably covers your needs.
 * 
 * Make sure that any implementation fulfills this basic contract: <strong>Calling any method on a closed consumer, including the close method itself, has no
 * effect.</strong>
 * 
 * <code><pre>
 * class ExampleImageConsumer<T> extends ImageConsumer<T> {
 *   private final AtomicBoolean closed = new AtomicBoolean();
 *   
 *   &#64;Override
 *   public void onImage(final T image) {
 *     if (!closed.get()) {
 *       // do something
 *     }
 *   }
 * 
 *   &#64;Override
 *   public void close() {
 *     if (closed.compareAndSet(false, true)) { // prohibits double close�
 *       // do something else
 *     }
 * }<pre>
 * </code>
 * 
 * @author Brix
 *
 * @param <T>
 *            the type of image to consume
 */
public interface ImageConsumer<T> extends AutoCloseable {

	/**
	 * Called by an emitter, if an image is emitted. Make sure this method does nothing, if the emitter has already been closed;
	 * 
	 * @param image
	 *            the image to be processed.
	 */
	void onImage(T image);

	/**
	 * Called arbitrary number of times after a resource is closed. Make sure subsequent calls ona closed resource do nothing.
	 */
	@Override
	void close();

}
