package de.plush.brix.c64graphics.core.util.collections;

import static de.plush.brix.c64graphics.core.util.IntIntPair.pair;
import static java.lang.Math.max;
import static java.util.stream.Collectors.joining;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

import de.plush.brix.c64graphics.core.util.*;

/**
 * A HashMap to store primitive int to int mappings.
 * 
 * @author Mikhail Vorontsov (base implementation);
 * @author Wanja Gayk (additional methods, refactoring)
 * @see <a href=
 *      "http://java-performance.info/implementing-world-fastest-java-int-to-int-hash-map/">http://java-performance.info/implementing-world-fastest-java-int-to-int-hash-map/</a>
 * @implNote Some speed has been sacrificed to allow lazy initialization of the backing map.
 *           This is useful, if a map is created with a large capacity, but stays unused.
 *           Also some speed has been sacrificed to speed up subsequent hashCode() calls.
 * @implNote
 *           Tests using jmh have shown that changing FREE_KEY to an 0/1 int and using branchless techniques based
 *           on its value led to worse performance.
 *           jmh has also shown that refactoring by extracting methods did not create a noticeable performance difference,
 *           despite the rehash skipping an if-condition and an invalidateHashCode() call.
 */
public class IntIntHashMap {

	/**
	 * The default initial capacity - MUST be a power of two.
	 */
	static final int DEFAULT_INITIAL_CAPACITY = 1 << 4; // aka 16

	/**
	 * The maximum capacity, used if a higher value is implicitly specified
	 * by either of the constructors with arguments.
	 * MUST be a power of two <= 1<<29.
	 */
	static final int MAXIMUM_CAPACITY = 1 << 29;

	/* Must be a power of two. */
	static final int MINIUMUM_CAPACITY = 1 << 3;

	/**
	 * The load factor used when none specified in constructor.
	 */
	static final float DEFAULT_LOAD_FACTOR = 0.75f;

	/* package private visibility for unit tests */
	static final int FREE_KEY = 0;

	/** package private visibility for unit test */
	static final int DEFAULT_NO_VALUE = Integer.MIN_VALUE + 1;

	public final int no_value;

	// private int[] m_data;

	/** Keys and values */
	private Supplier<int[]> data;

	/** Do we have 'free' key in the map? */
	private boolean m_hasFreeKey;

	/** Value of 'free' key */
	private int m_freeValue;

	/** Fill factor, must be between (0 and 1) */
	private final float m_fillFactor;

	/** We will resize a map once it reaches this size */
	private int m_threshold;

	/** Current map size */
	private int m_size;

	/** Mask to calculate the original position */
	private int m_mask;

	private int m_mask2;

	public IntIntHashMap() {
		this(DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR); // same as java.util.HashMap
	}

	public IntIntHashMap(final int initialCapacity) {
		this(initialCapacity, DEFAULT_LOAD_FACTOR);
	}

	public IntIntHashMap(final int initialCapacity, final float fillFactor) {
		this(initialCapacity, fillFactor, DEFAULT_NO_VALUE);
	}

	/**
	 * In Lambdas, where you pass like "IntINtHashMap::new, a single integer constructor usually stands for the initial size/capacity.
	 * This constrtuctor uses an unused parameter that can only be null to allow the distinct.
	 * 
	 * @param no_value
	 *            the integer to return, if a key has no value, see: {@link #no_value}
	 * @param justPassNull
	 *            unused, always null
	 */
	public IntIntHashMap(final int no_value, final Void justPassNull) {
		this(DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR, no_value);
	}

	public IntIntHashMap(final int initialCapacity, final float fillFactor, final int no_value) {
		if (MAXIMUM_CAPACITY < initialCapacity) { throw new IllegalArgumentException("initial capacity: " + initialCapacity); }
		if (fillFactor <= 0 || fillFactor >= 1) { throw new IllegalArgumentException("FillFactor must be in (0, 1)"); }
		this.no_value = no_value;
		final int arraySize = arraySize(max(MINIUMUM_CAPACITY, initialCapacity), fillFactor);
		m_mask = arraySize - 1;
		m_mask2 = arraySize * 2 - 1;
		m_fillFactor = fillFactor;
		data = () -> (data = Utils.supplierOf(new int[arraySize * 2])).get(); // lazy init without subsequent null-checks
		m_threshold = (int) (arraySize * fillFactor);
	}

	public static IntIntHashMap of(final int key, final int value) {
		final var map = new IntIntHashMap();
		map.put(key, value);
		return map;
	}

	public static IntIntHashMap of(final int key0, final int value0, final int key1, final int value1) {
		final var map = IntIntHashMap.of(key0, value0);
		map.put(key1, value1);
		return map;
	}

	public static IntIntHashMap of(final int key0, final int value0, final int key1, final int value1, final int key2, final int value2) {
		final var map = IntIntHashMap.of(key0, value0, key1, value1);
		map.put(key2, value2);
		return map;
	}

	public static IntIntHashMap of(final int key0, final int value0, final int key1, final int value1, final int key2, final int value2, final int key3,
			final int value3) {
		final var map = IntIntHashMap.of(key0, value0, key1, value1, key2, value2);
		map.put(key3, value3);
		return map;
	}

	public static IntIntHashMap of(final int key0, final int value0, final int key1, final int value1, final int key2, final int value2, final int key3,
			final int value3, final int key4, final int value4) {
		final var map = IntIntHashMap.of(key0, value0, key1, value1, key2, value2, key3, value3);
		map.put(key4, value4);
		return map;
	}

	public static IntIntHashMap of(final int key0, final int value0, final int key1, final int value1, final int key2, final int value2, final int key3,
			final int value3, final int key4, final int value4, final int key5, final int value5) {
		final var map = IntIntHashMap.of(key0, value0, key1, value1, key2, value2, key3, value3, key4, value4);
		map.put(key5, value5);
		return map;
	}

	public static IntIntHashMap of(final int key0, final int value0, final int key1, final int value1, final int key2, final int value2, final int key3,
			final int value3, final int key4, final int value4, final int key5, final int value5, final int key6, final int value6) {
		final var map = IntIntHashMap.of(key0, value0, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5);
		map.put(key6, value6);
		return map;
	}

	public static IntIntHashMap of(final int key0, final int value0, final int key1, final int value1, final int key2, final int value2, final int key3,
			final int value3, final int key4, final int value4, final int key5, final int value5, final int key6, final int value6, final int key7,
			final int value7) {
		final var map = IntIntHashMap.of(key0, value0, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5, key6, value6);
		map.put(key7, value7);
		return map;
	}

	public static IntIntHashMap of(final int key0, final int value0, final int key1, final int value1, final int key2, final int value2, final int key3,
			final int value3, final int key4, final int value4, final int key5, final int value5, final int key6, final int value6, final int key7,
			final int value7, final int key8, final int value8) {
		final var map = IntIntHashMap.of(key0, value0, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5, key6, value6, key7, value7);
		map.put(key8, value8);
		return map;
	}

	public static IntIntHashMap of(final int key0, final int value0, final int key1, final int value1, final int key2, final int value2, final int key3,
			final int value3, final int key4, final int value4, final int key5, final int value5, final int key6, final int value6, final int key7,
			final int value7, final int key8, final int value8, final int key9, final int value9) {
		final var map = IntIntHashMap.of(key0, value0, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5, key6, value6, key7, value7, key8,
				value8);
		map.put(key9, value9);
		return map;
	}

	public static IntIntHashMap of(final IntIntPair... values) {
		final var map = new IntIntHashMap(max(values.length, MINIUMUM_CAPACITY));
		for (final IntIntPair value : values) {
			map.put(value.getOne(), value.getTwo());
		}
		return map;
	}

	public int get(final int key) {
		if (key == FREE_KEY) { return m_hasFreeKey ? m_freeValue : no_value; }
		final var m_data = data.get();
		final int ptr = probe(key, m_data);
		return m_data[ptr] == FREE_KEY ? no_value : m_data[ptr + 1];
	}

	public OptionalInt getOptional(final int key) {
		if (key == FREE_KEY) { return m_hasFreeKey ? OptionalInt.of(m_freeValue) : OptionalInt.empty(); }
		final var m_data = data.get();
		final int ptr = probe(key, m_data);
		return m_data[ptr] == FREE_KEY ? OptionalInt.empty() : OptionalInt.of(m_data[ptr + 1]);
	}

	public boolean containsKey(final int key) {
		if (key == FREE_KEY) { return m_hasFreeKey; }
		final var m_data = data.get();
		return m_data[probe(key, m_data)] == key;
	}

	public int put(final int key, final int value) {
		// probably not worth to invalidate the hashCode only if new and old are different. Puts usually come in bunches and branch misses are always costly
		invalidateHashCode();
		if (key == FREE_KEY) { return putToFreeKey(value); }
		return putNoFreeKey(key, value);
	}

	private int putNoFreeKey(final int key, final int value) { // reused by rehash (instead of put).
		final var m_data = data.get();
		int ptr = startingSlot(key);
		int k = m_data[ptr];
		do {
			if (k == FREE_KEY) { // test case expected to be more common first, with put we usually expect free keys
				return putToFreeSlot(key, value, m_data, ptr);
			} else if (k == key) { // we check FREE prior to this call
				return putToOccupiedSlot(value, m_data, ptr);
			}
			ptr = nextSlot(ptr);
			k = m_data[ptr];
		} while (true);
	}

	/**
	 * @param key
	 *            the key of the value to update
	 * @param defaultValue
	 *            the value to put if there is no value
	 * @param update
	 *            a function {@code (key, currentValue) -> newValue} to update the old value
	 * @return the previous value or {@link #no_value}
	 */
	public int update(final int key, final int defaultValue, final IntBinaryOperator update) {
		// probably not worth to invalidate the hashCode only if new and old are different. Puts usually come in bunches and branch misses are always costly
		invalidateHashCode();
		if (key == FREE_KEY) { return updateFreeKey(key, defaultValue, update); }
		final var m_data = data.get();
		int ptr = startingSlot(key);
		int k = m_data[ptr];
		do {
			if (k == key) { // test case expected to be more common first, with update we expect a key to be there
				return updateOccupiedSlot(key, update, m_data, ptr);
			} else if (k == FREE_KEY) { //
				return putToFreeSlot(key, defaultValue, m_data, ptr);
			}
			ptr = nextSlot(ptr);
			k = m_data[ptr];
		} while (true);
	}

	public int remove(final int key) {
		if (key == FREE_KEY) { return removeFreeKey(); }
		final var m_data = data.get();
		int ptr = startingSlot(key);
		int k = m_data[ptr];
		do {
			if (k == key) { // test case expected to be more common first, with remove we expect a key to be there
				return removeOccupiedSlot(m_data, ptr);
			} else if (k == FREE_KEY) { //
				return no_value;
			}
			ptr = nextSlot(ptr);
			k = m_data[ptr];
		} while (true);
	}

	public void clear() {
		final var length = data.get().length;
		data = () -> (data = Utils.supplierOf(new int[length])).get(); // lazy init without subsequent null-checks
		invalidateHashCode();
		m_size = 0;
		m_hasFreeKey = false;
		m_freeValue = no_value; // unnecessary, but leaving it may create a distraction when debugging
	}

	public int size() {
		return m_size;
	}

	public boolean isEmpty() {
		return m_size == 0;
	}

	public IntStream keyStream() {
		if (isEmpty()) { return IntStream.empty(); }
		final var m_data = data.get();
		final var freeKey = m_hasFreeKey ? IntStream.of(FREE_KEY) : IntStream.empty();
		final var mapKeys = keyIndexStream().map(index -> m_data[index]);
		return IntStream.concat(freeKey, mapKeys);
	}

	public IntStream valueStream() {
		if (isEmpty()) { return IntStream.empty(); }
		final var m_data = data.get();
		final var freeKeyValue = m_hasFreeKey ? IntStream.of(m_freeValue) : IntStream.empty();
		final var mapValues = keyIndexStream().map(index -> m_data[index + 1]);
		return IntStream.concat(freeKeyValue, mapValues);
	}

	public Stream<IntIntPair> entryStream() {
		if (isEmpty()) { return Stream.empty(); }
		final var m_data = data.get();
		final var freeEntry = m_hasFreeKey ? Stream.of(pair(FREE_KEY, m_freeValue)) : Stream.<IntIntPair> empty();
		final var mapEntries = keyIndexStream().mapToObj(index -> pair(m_data[index], m_data[index + 1]));
		return Stream.concat(freeEntry, mapEntries);
	}

	public IntIntPair[] toArray() {
		return entryStream().toArray(IntIntPair[]::new);
	}

	public IntIntPair[] toSortedArray() {
		return entryStream().sorted().toArray(IntIntPair[]::new);
	}

	private int hashCode;

	private void invalidateHashCode() {
		hashCode = 0;
	}

	@Override
	public int hashCode() {
		if (hashCode != 0) { return hashCode; }
		if (isEmpty()) { return 0; }
		final int prime = 31;
		int result = 1;
		result = prime * result + m_size;
		result = prime * result + Arrays.hashCode(toSortedArray());
		hashCode = result;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final IntIntHashMap other = (IntIntHashMap) obj;
		if (m_size != other.m_size) { return false; }
		if (!Arrays.equals(toSortedArray(), other.toSortedArray())) { return false; }
		return true;
	}

	@Override
	public String toString() {
		return "IntIntHashMap [no_value=" + no_value + ", m_size=" + m_size //
				+ ", entries=" + entryStream().map(IntIntPair::toString).collect(joining(",")) + "]";
	}

	// --- internals ---

	private int putToFreeKey(final int value) {
		final int previous = m_freeValue;
		m_freeValue = value;
		if (!m_hasFreeKey) { // going branchless using 0/1 for m_hasFreeKey did not prove to be faster.
			m_hasFreeKey = true;
			++m_size;
		}
		return previous;
	}

	private int updateFreeKey(final int key, final int defaultValue, final IntBinaryOperator update) {
		if (m_hasFreeKey) {
			m_freeValue = update.applyAsInt(key, m_freeValue);
			return m_freeValue;
		}
		m_freeValue = defaultValue;
		m_hasFreeKey = true;
		++m_size;
		return no_value;
	}

	private int removeFreeKey() {
		if (!m_hasFreeKey) { return no_value; }
		invalidateHashCode();
		m_hasFreeKey = false;
		--m_size;
		return m_freeValue; // value does not need to be cleaned
	}

	private int putToFreeSlot(final int key, final int value, final int[] m_data, final int ptr) {
		m_data[ptr] = key;
		m_data[ptr + 1] = value;
		++m_size;
		if (m_size >= m_threshold) {
			rehash(m_data.length * 2, m_data); // size is set inside
		}
		return no_value;
	}

	private static int putToOccupiedSlot(final int value, final int[] m_data, final int ptr) {
		final int previous = m_data[ptr + 1];
		m_data[ptr + 1] = value;
		return previous;
	}

	private static int updateOccupiedSlot(final int key, final IntBinaryOperator update, final int[] m_data, final int ptr) {
		final var previous = m_data[ptr + 1];
		m_data[ptr + 1] = update.applyAsInt(key, previous);
		return previous;
	}

	private int removeOccupiedSlot(final int[] m_data, final int ptr) {
		invalidateHashCode();
		final int previous = m_data[ptr + 1];
		shiftKeys(ptr, m_data);
		--m_size;
		return previous;
	}

	private int probe(final int key, final int[] m_data) {
		int ptr = startingSlot(key);
		int k = m_data[ptr];
		while (k != key && k != FREE_KEY) { // expecting the key to be found on first try to be a common case for less loaded maps
			ptr = nextSlot(ptr); // that's next index
			k = m_data[ptr];
		}
		return ptr;
	}

	private int startingSlot(final int key) {
		return (phiMix(key) & m_mask) << 1;
	}

	private int nextSlot(int ptr) {
		ptr = ptr + 2 & m_mask2;
		return ptr;
	}

	private int shiftKeys(int ptr, final int[] m_data) {
		// Shift entries with the same hash.
		int last, slot;
		int k;
		do {
			last = ptr;
			ptr = nextSlot(last = ptr);
			do {
				if ((k = m_data[ptr]) == FREE_KEY) {
					m_data[last] = FREE_KEY;
					return last;
				}
				slot = startingSlot(k); // calculate the starting slot for the current key
				if (last <= ptr ? last >= slot || slot > ptr : last >= slot && slot > ptr) {
					break;
				}
				ptr = nextSlot(ptr);
			} while (true); // evaluate condition last, as it allows an early return
			m_data[last] = k;
			m_data[last + 1] = m_data[ptr + 1];
		} while (true); // evaluate condition last, as it allows an early return
	}

	private int[] rehash(final int newCapacity, final int[] m_data) {
		if (m_data.length == Integer.MAX_VALUE) { throw new ArrayIndexOutOfBoundsException("Map full"); }
		m_threshold = (int) (newCapacity / 2f * m_fillFactor); // 2f is important, see Findbugs ICAST_IDIV_CAST_TO_DOUBLE
		m_mask = newCapacity / 2 - 1;
		m_mask2 = newCapacity - 1;

		final int oldCapacity = m_data.length;
		final int[] oldData = m_data;

		final var newData = new int[newCapacity * 2];
		data = Utils.supplierOf(newData); // map is in use, don't need to be lazy anymore
		m_size = m_hasFreeKey ? 1 : 0;

		for (int i = 0; i < oldCapacity; i += 2) {
			final int oldKey = oldData[i];
			if (oldKey != FREE_KEY) {
				putNoFreeKey(oldKey, oldData[i + 1]);
			}
		}
		return newData;
	}

	/**
	 * Returns the least power of two smaller than or equal to 2<sup>30</sup> and larger than or equal to <code>Math.ceil( expected / f )</code>.
	 *
	 * @param expected
	 *            the expected number of elements in a hash table.
	 * @param f
	 *            the load factor.
	 * @return the minimum possible size for a backing array.
	 * @throws IllegalArgumentException
	 *             if the necessary size is larger than 2<sup>30</sup>.
	 */
	private static int arraySize(final int expected, final float f) {
		final long s = Math.max(2, Utils.nextPowerOfTwo((long) Math.ceil(expected / f)));
		if (s > 1 << 30) { throw new IllegalArgumentException("Too large (" + expected + " expected elements with load factor " + f + ")"); }
		return (int) s;
	}

	// taken from FastUtil
	private static final int INT_PHI = 0x9E3779B9;

	private static int phiMix(final int x) {
		final int h = x * INT_PHI;
		return h ^ h >> 16;
	}

	private IntStream keyIndexStream() {
		final var m_data = data.get();
		return IntStream.range(0, m_data.length)//
				.filter(index -> (index & 1) == 0 && m_data[index] != FREE_KEY);
	}

}
