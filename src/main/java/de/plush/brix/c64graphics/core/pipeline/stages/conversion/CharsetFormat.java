package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.geom.Area;

import de.plush.brix.c64graphics.core.model.C64Screen;

public interface CharsetFormat {

	C64Screen createScreen();

	/** a charset might not fill the whole screen, this is the area that all charset blocks occupy on the screen. */
	Area areaOfBlockPositions();

}