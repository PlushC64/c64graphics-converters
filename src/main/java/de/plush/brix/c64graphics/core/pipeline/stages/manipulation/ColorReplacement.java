/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import static de.plush.brix.c64graphics.core.util.UtilsImage.replaceColor;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.function.UnaryOperator;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class ColorReplacement implements IPipelineStage<BufferedImage, BufferedImage> {

	private final UnaryOperator<Color> colorChangeFunction;

	public ColorReplacement(final UnaryOperator<Color> colorChangeFunction) {
		this.colorChangeFunction = colorChangeFunction;
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final var result = new BufferedImage(original.getWidth(), original.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		final Graphics2D g = result.createGraphics();
		try {
			g.drawImage(original, 0, 0, null);
			replaceColor(result, colorChangeFunction);
		} finally {
			g.dispose();
		}
		return result;
	}
}