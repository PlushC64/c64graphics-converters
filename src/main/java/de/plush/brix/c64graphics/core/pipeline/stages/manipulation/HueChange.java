/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.Color;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class HueChange implements IPipelineStage<BufferedImage, BufferedImage> {

	private final ColorReplacement colorReplacement;

	/**
	 * Alter the brightness of the image.
	 * 
	 * @param rotationFraction
	 *            a value between 0 and 1 for the rotation on the hue circle.
	 *            1.0 for a full circle (== 0.0), 0.75 for 270", 0.5 for 180°, 0.25 for 90°, etc.
	 */
	public HueChange(final double rotationFraction) {
		colorReplacement = new ColorReplacement(color -> brightnessChange(rotationFraction, color));
	}

	private static Color brightnessChange(final double factor, final Color color) {
		final float[] hsb = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
		return Color.getHSBColor(hsb[0], hsb[1], (float) (hsb[2] * factor));
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		return colorReplacement.apply(original);
	}
}
