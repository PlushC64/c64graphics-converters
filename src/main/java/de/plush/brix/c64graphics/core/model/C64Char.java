package de.plush.brix.c64graphics.core.model;

import static de.plush.brix.c64graphics.core.util.Utils.*;
import static java.lang.Integer.parseInt;
import static java.util.Arrays.*;

import java.awt.*;
import java.math.BigInteger;
import java.util.*;

import javax.annotation.concurrent.Immutable;

import de.plush.brix.c64graphics.core.util.Utils;
import de.plush.brix.c64graphics.core.util.functions.IntByteProcedure;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@Immutable
public final class C64Char implements C64BinaryProvider, Comparable<C64Char>, Iterable<Byte> {

	public static final C64Char BLANK = new C64Char(new byte[8]);

	public static final C64Char FULL = new C64Char(new byte[] //
	{ (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff });

	public static final int WIDTH_PIXELS = 8;

	public static final int HEIGHT_PIXELS = 8;

	final byte[] bytes;

	@SuppressFBWarnings(value = "JCIP_FIELD_ISNT_FINAL_IN_IMMUTABLE_CLASS", //
			justification = "cached value, since we're immutable, it's not important to be thread safe. Multiple versions of the same value may exist")
	private BigInteger value;

	@SuppressFBWarnings(value = "JCIP_FIELD_ISNT_FINAL_IN_IMMUTABLE_CLASS", //
			justification = "cached hashCode, since we're immutable, it's not important to be thread safe. Multiple versions of the same value may exist")
	private int hashCode = -1;

	/**
	 * Creates a new Character from a String.<br>
	 * 
	 * Example:
	 * <code><pre>
	 * var frameChar = new C64Char("""
	 * 		11111111
	 * 		10000001
	 * 		10000001
	 * 		10000001
	 * 		10000001
	 * 		10000001
	 * 		10000001
	 * 		11111111
	 * 		""");</pre></code>
	 * 
	 */
	public C64Char(final String zeroOrOnes) {
		this(bytesFromString(zeroOrOnes));
	}

	private static byte[] bytesFromString(final String zeroOrOnes) {
		final String[] lines = zeroOrOnes.split("\n");
		if (lines.length > HEIGHT_PIXELS) { throw new IllegalArgumentException("wrong number of bytes (" + lines.length + "). Expected: <=" + HEIGHT_PIXELS); }
		final var bytes = new byte[8];
		for (int t = 0; t < HEIGHT_PIXELS; ++t) {
			bytes[t] = t >= lines.length ? 0 : toUnsignedByte(parseInt(lines[t], 2));
		}
		return bytes;
	}

	public C64Char(final byte[] bytes) {
		if (bytes.length != HEIGHT_PIXELS) { throw new IllegalArgumentException("wrong number of bytes (" + bytes.length + "). Expected: " + HEIGHT_PIXELS); }
		this.bytes = Arrays.copyOf(bytes, bytes.length);
	}

	public static Dimension sizePixels() {
		return new Dimension(WIDTH_PIXELS, HEIGHT_PIXELS);
	}

	/** returns a new instance with all pixels reversed. */
	public C64Char reversed() {
		return xor(FULL);
	}

	/** returns a new instance with all pixels rolled up n lines, what leaves at the top, comed back in at the bottom. */
	public C64Char rolledPixelsUp(final int lines) {
		if (lines % HEIGHT_PIXELS == 0) { return this; }
		final byte[] copy = copyOf(bytes, bytes.length);
		Utils.rollLeft(copy, lines);
		return new C64Char(copy);
	}

	/** returns a new instance with all pixels rolled down n lines, what leaves ath the bottom comes back in at the top. */
	public C64Char rolledPixelsDown(final int lines) {
		if (lines % HEIGHT_PIXELS == 0) { return this; }
		final byte[] copy = copyOf(bytes, bytes.length);
		Utils.rollRight(copy, lines);
		return new C64Char(copy);
	}

	/** returns a new instance with all pixels rolled left n columns. What leaves on the left, comes back in at the right. */
	public C64Char rolledPixelsLeft(final int columns) {
		return columns % WIDTH_PIXELS == 0 ? this : new C64Char(transformedCopy(bytes, b -> rollBitsLeft(b, columns)));
	}

	/** returns a new instance with all pixels rolled right n columns. What leaves on the right comes back in on the left. */
	public C64Char rolledPixelsRight(final int columns) {
		return columns % WIDTH_PIXELS == 0 ? this : new C64Char(transformedCopy(bytes, b -> rollBitsRight(b, columns)));
	}

	/** returns a new instance with all pixels shifted up n lines. */
	public C64Char shiftedPixelsUp(final int lines) {
		if (lines == 0) { return this; }
		if (lines >= 8) { return BLANK; }
		if (lines < 0) { return shiftedPixelsDown(-lines); }
		final byte[] shifted = copyOfRange(bytes, lines, bytes.length);
		return new C64Char(copyOf(shifted, HEIGHT_PIXELS));
	}

	/** returns a new instance with all pixels shifted down n lines. */
	public C64Char shiftedPixelsDown(final int lines) {
		if (lines == 0) { return this; }
		if (lines >= 8) { return BLANK; }
		if (lines < 0) { return shiftedPixelsUp(-lines); }
		final byte[] shifted = copyOfRange(bytes, 0, HEIGHT_PIXELS - lines);
		return new C64Char(Utils.toByteArray(new byte[lines], shifted));
	}

	/** returns a new instance with all pixels shifted left n columns. */
	public C64Char shiftedPixelsLeft(final int columns) {
		if (columns == 0) { return this; }
		if (columns >= 8) { return BLANK; }
		if (columns < 0) { return shiftedPixelsRight(-columns); }
		return columns % WIDTH_PIXELS == 0 ? this : new C64Char(transformedCopy(bytes, b -> toUnsignedByte(toPositiveInt(b) << columns)));
	}

	/** returns a new instance with all pixels shifted right n columns. */
	public C64Char shiftedPixelsRight(final int columns) {
		if (columns == 0) { return this; }
		if (columns >= 8) { return BLANK; }
		if (columns < 0) { return shiftedPixelsLeft(-columns); }
		return columns % WIDTH_PIXELS == 0 ? this : new C64Char(transformedCopy(bytes, b -> toUnsignedByte(toPositiveInt(b) >>> columns)));
	}

	/** Flipped around the y-axis. What's left will appear right. */
	public C64Char flippedHorizontally() {
		return new C64Char(transformedCopy(bytes, Utils::reverseBitOrder));
	}

	/** Flipped around the x-axis. What's on top will appear at the bottom. */
	public C64Char flippedVertically() {
		return new C64Char(reversedCopy(bytes));
	}

	// TODO: rotated90DegreesClockwise(n)
	// TODO: rotated90DegreesAnticlockwise(n)

	public C64Char and(final C64Char other) {
		if (other.equals(BLANK)) { return BLANK; }
		if (other.equals(FULL)) { return this; }
		final C64Char newChar = new C64Char(Utils.and(bytes, other.bytes));
		return newChar.equals(BLANK) ? BLANK //
				: newChar.equals(this) ? this //
						: newChar;
	}

	public C64Char or(final C64Char other) {
		if (other.equals(BLANK)) { return this; }
		final C64Char newChar = new C64Char(Utils.or(bytes, other.bytes));
		return newChar.equals(FULL) ? FULL//
				: newChar.equals(this) ? this //
						: newChar;
	}

	public C64Char xor(final C64Char other) {
		if (other.equals(BLANK)) { return this; }
		final var newChar = new C64Char(Utils.xor(bytes, other.bytes));
		return newChar.equals(FULL) ? FULL
				: newChar.equals(BLANK) ? BLANK //
						: newChar.equals(this) ? this //
								: newChar;
	}

	public C64Char withPixelSet(final Point p) {
		return withPixelSet(p.x, p.y);
	}

	public C64Char withPixelSet(final int x, final int y) {
		if (0 > x || x >= WIDTH_PIXELS) { throw new IllegalArgumentException("x coordinate out of bounds 0..7: " + x); }
		if (0 > y || y >= HEIGHT_PIXELS) { throw new IllegalArgumentException("y coordinate out of bounds 0..7: " + y); }
		if (this == C64Char.FULL) { return this; }
		final int mask = 0x80 >> x;
		final var newBytes = bytes.clone();
		newBytes[y] = Utils.toUnsignedByte(bytes[y] | mask);
		final var newChar = new C64Char(newBytes);
		return newChar.equals(C64Char.FULL) ? C64Char.FULL //
				: newChar.equals(this) ? this //
						: newChar;

	}

	public C64Char withPixelDeleted(final Point p) {
		return withPixelDeleted(p.x, p.y);
	}

	public C64Char withPixelDeleted(final int x, final int y) {
		if (0 > x || x >= WIDTH_PIXELS) { throw new IllegalArgumentException("x coordinate out of bounds 0..7: " + x); }
		if (0 > y || y >= HEIGHT_PIXELS) { throw new IllegalArgumentException("y coordinate out of bounds 0..7: " + y); }
		if (this == C64Char.BLANK) { return this; }
		final int mask = 0x80 >> x;
		final var newBytes = bytes.clone();
		newBytes[y] = Utils.toUnsignedByte(bytes[y] & ~mask);
		final C64Char newChar = new C64Char(newBytes);
		return newChar.equals(C64Char.BLANK) ? C64Char.BLANK //
				: newChar.equals(this) ? this //
						: newChar;
	}

	public C64Char withPixelFlipped(final Point p) {
		return withPixelFlipped(p.x, p.y);
	}

	public C64Char withPixelFlipped(final int x, final int y) {
		if (0 > x || x >= WIDTH_PIXELS) { throw new IllegalArgumentException("x coordinate out of bounds 0..7: " + x); }
		if (0 > y || y >= HEIGHT_PIXELS) { throw new IllegalArgumentException("y coordinate out of bounds 0..7: " + y); }
		final int mask = 0x80 >> x;
		final var newBytes = bytes.clone();
		newBytes[y] = Utils.toUnsignedByte(bytes[y] ^ mask);
		final C64Char newChar = new C64Char(newBytes);
		return newChar.equals(C64Char.BLANK) ? C64Char.BLANK //
				: newChar.equals(C64Char.FULL) ? C64Char.FULL //
						: newChar.equals(this) ? this //
								: newChar;
	}

	/** x=0 is the leftmost pixel, i.e. 0x80. */
	public boolean isPixelSet(final Point p) {
		return isPixelSet(p.x, p.y);
	}

	/** x=0 is the leftmost pixel, i.e. 0x80. */
	public boolean isPixelSet(final int x, final int y) {
		if (0 > x || x >= WIDTH_PIXELS) { throw new IllegalArgumentException("x coordinate out of bounds 0..7: " + x); }
		if (0 > y || y >= HEIGHT_PIXELS) { throw new IllegalArgumentException("y coordinate out of bounds 0..7: " + y); }
		final int mask = 0x80 >> x;
		return (bytes[y] & mask) != 0;
	}

	@Override
	public Iterator<Byte> iterator() {
		return new ByteArrayIterator(bytes);
	}

	public void forEachIndexed(final IntByteProcedure indexByteConsumer) {
		for (int i = 0; i < bytes.length; ++i) {
			indexByteConsumer.value(i, bytes[i]);
		}
	}

	@Override
	public int compareTo(final C64Char other) {
		return other == null ? 1 : value().compareTo(other.value());
	}

	@Override
	public int hashCode() {
		if (hashCode == -1) {
			hashCode = value().hashCode();
		}
		return hashCode;
	}

	@Override
	public boolean equals(final Object other) {
		return other == this //
				|| other != null //
						&& getClass().equals(other.getClass()) //
						&& value().equals(((C64Char) other).value());
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder(WIDTH_PIXELS * HEIGHT_PIXELS);
		for (final Byte b : bytes) {
			sb.append(Utils.toBinaryString(b)).append('\n');
		}
		return sb.toString();
	}

	@Override
	public byte[] toC64Binary() {
		return Arrays.copyOf(bytes, bytes.length);
	}

	@SuppressWarnings("boxing")
	private BigInteger value() {
		if (value == null) {
			value = new BigInteger(new byte[] { //
					(byte) 0 // sign always positive
					, bytes[0] //
					, bytes[1] //
					, bytes[2] //
					, bytes[3] //
					, bytes[4] //
					, bytes[5] //
					, bytes[6] //
					, bytes[7] //
			}//
			);
		}
		return value;
	}

}
