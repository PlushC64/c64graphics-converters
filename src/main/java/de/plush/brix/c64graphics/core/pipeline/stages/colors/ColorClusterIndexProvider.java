package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import java.util.Set;

import de.plush.brix.c64graphics.core.util.collections.ByteSet;

public interface ColorClusterIndexProvider {

	Set<ByteSet> colorIndexClusters();
}
