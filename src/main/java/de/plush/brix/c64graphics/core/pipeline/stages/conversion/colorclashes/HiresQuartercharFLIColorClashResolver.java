package de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes;

import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.ColorDistanceMeasure;

/**
 * Resolves color clashes in Hires-Bitmaps + 4 Screen RAMs
 *
 * @author Wanja Gayk
 */
public class HiresQuartercharFLIColorClashResolver implements IHiresFLIColorClashResolver {

	private final HiresNscreenFLIColorClashResolver hiresNScreenFLIColorClashResolver;

	public HiresQuartercharFLIColorClashResolver(final ColorDistanceMeasure colorDistanceMeasure) {
		final int screens = 4;
		hiresNScreenFLIColorClashResolver = new HiresNscreenFLIColorClashResolver(screens, colorDistanceMeasure);
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		return hiresNScreenFLIColorClashResolver.apply(original);
	}
}
