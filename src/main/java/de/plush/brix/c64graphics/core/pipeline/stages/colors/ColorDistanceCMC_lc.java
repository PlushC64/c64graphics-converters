package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces.ColorConversions.*;

import java.awt.Color;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces.ColorCieLab;

/**
 * In 1984, the Colour Measurement Committee of the Society of Dyers and Colourists defined a difference measure, also based on the L*C*h color model. Named
 * after the developing committee, their metric is called CMC l:c. The quasimetric has two parameters: lightness (l) and chroma (c), allowing the users to
 * weight the difference based on the ratio of l:c that is deemed appropriate for the application.
 * 
 * @See <a href="https://en.wikipedia.org/wiki/Color_difference#CMC_l:c_(1984)">https://en.wikipedia.org/wiki/Color_difference#CMC_l:c_(1984)</a>
 */
class ColorDistanceCMC_lc implements ColorDistanceMeasure {

	public static final double DefaultLightness = 2.0;

	public static final double DefaultChroma = 1.0;

	// The quasimetric has two parameters: lightness (l) and chroma (c), allowing the users to weight the difference based on the ratio of l:c that is
	// deemed appropriate for the application.
	// Commonly used l:c values are 2:1 for acceptability and 1:1 for the threshold of imperceptibility.
	private final double l; // lightness

	private final double c; // chrome

	ColorDistanceCMC_lc() {
		l = DefaultLightness;
		c = DefaultChroma;
	}

	ColorDistanceCMC_lc(final double lightness, final double chroma) {
		l = lightness;
		c = chroma;
	}

	@Override
	public double distanceOf(final Color rgb1, final Color rgb2) {
		final var lab1 = convertXYZtoCIELab(convertRGBtoXYZ(rgb1));
		final var lab2 = convertXYZtoCIELab(convertRGBtoXYZ(rgb2));
		return rgb1.equals(rgb2) ? 0.0 : Math.min(1, distanceOfAlt(lab1, lab2) / 207);

		// final ColorCieLch lch1 = convertCIELabtoCIELCH(lab1);
		// final ColorCieLch lch2 = convertCIELabtoCIELCH(lab2);
		// return rgb1.equals(rgb2) ? 0.0 : Math.min(1, distanceOf(lch1, lch2, lab1, lab2) / 550);
	}

	/**
	 * Compares two L*c*h colors and returns the degree of their similarity. The lower the result the more similar are the colors.
	 * Taken from
	 * https://github.com/lessthanoptimal/BoofCV/blob/aa9675da1f68f7318db9e9224e6e1acac61225fa/main/boofcv-ip/src/main/java/boofcv/alg/color/ColorDifference.java
	 */
	private double distanceOfAlt(final ColorCieLab lab1, final ColorCieLab lab2) {

		// double C1 = Math.sqrt(Math.pow(a1,2) + Math.pow(b1,2));
		final double C1 = Math.sqrt(lab1.a() * lab1.a() + lab1.b() * lab1.b());
		// double C2 = Math.sqrt(Math.pow(a2,2) + Math.pow(b2,2));
		final double C2 = Math.sqrt(lab2.a() * lab2.a() + lab2.b() * lab2.b());
		final double deltaC = C1 - C2;
		final double deltaL = lab1.L() - lab2.L();
		final double deltaA = lab1.a() - lab2.a();
		final double deltaB = lab1.b() - lab2.b();
		// Changed to avoid NaN due limited arithmetic precision
		// final double deltaH = Math.sqrt(Math.pow(deltaA, 2) + Math.pow(deltaB, 2) - Math.pow(deltaC, 2));
		final double deltaH = Math.pow(deltaA, 2) + Math.pow(deltaB, 2) - Math.pow(deltaC, 2);

		double SL = 0.511;
		if (lab1.L() >= 16.0) {
			SL = 0.040975 * lab1.L() / (1.0 + 0.01765 * lab1.L());
		}
		final double SC = 0.0638 * C1 / (1 + 0.0131 * C1) + 0.638;
		final double H = Math.toDegrees(Math.atan2(lab1.b(), lab1.a()));
		double H1 = H;
		if (H < 0) {
			H1 += 360;
		}
		// double F = Math.sqrt(Math.pow(C1,4)/(Math.pow(C1,4) + 1900.0));
		final double p = C1 * C1 * C1 * C1;
		final double F = Math.sqrt(p / (p + 1900.0));
		double T;
		if (H1 <= 345.0 && H1 >= 164.0) {
			T = 0.56 + Math.abs(0.2 * Math.cos(Math.toRadians(H1 + 168.0)));
		} else {
			T = 0.36 + Math.abs(0.4 * Math.cos(Math.toRadians(H1 + 35.0)));
		}
		final double SH = SC * (F * T + 1.0 - F);
		// Changed to avoid NaN due limited arithmetic precision
		// return(Math.sqrt(Math.pow(deltaL/(l*SL),2) + Math.pow(deltaC/(c*SC),2) + Math.pow(deltaH/SH,2)));
		final double p1 = deltaL / (l * SL);
		final double p2 = deltaC / (c * SC);
		final double p3 = 1 / SH;
		return Math.sqrt(p1 * p1 + p2 * p2 + deltaH * p3 * p3);
	}

	/**
	 * // FIXME: There still seems to be a problem with this implementation
	 * Compares two L*c*h colors and returns the degree of their similarity. The lower the result the more similar are the colors.
	 *
	 * Taken from https://github.com/muak/ColorMinePortable/blob/master/ColorMinePortable/ColorSpaces/Comparisons/CmcComparison.cs
	 * 
	 * @param lch1
	 *            First color represented in L*c*h color space.
	 * @param lch2
	 *            Second color represented in L*c*h color space.
	 * @param lab2
	 * @param lab1
	 * 
	 * @return The degree of similarity between the two input colors according to the CMC l:c color-difference formula.
	 *
	 *         private double distanceOf(final ColorCieLch lch1, final ColorCieLch lch2, final ColorCieLab lab1, final ColorCieLab lab2) {
	 * 
	 *         final double C1 = lch1.C;
	 *         final double L1 = lch1.L;
	 *         final double h1 = lch1.h;
	 * 
	 *         final double F = Math.sqrt(Math.pow(C1, 4) / (Math.pow(C1, 4) + 1900.0));
	 * 
	 *         final var T = 164 <= h1 && h1 <= 345 //
	 *         ? 0.56 + Math.abs(0.2 * Math.cos(Math.toRadians(h1 + 168.0))) //
	 *         : 0.36 + Math.abs(0.4 * Math.cos(Math.toRadians(h1 + 35.0)));
	 * 
	 *         final var Sc = 0.0638 * C1 / (1 + 0.0131 * C1) + 0.638;
	 * 
	 *         final var Sh = Sc * (F * T + 1 - F);
	 * 
	 *         final var Sl = L1 < 16 //
	 *         ? 0.511 //
	 *         : 0.040975 * L1 / (1.0 + .01765 * L1);
	 * 
	 *         final double deltaL = lch1.L - lch2.L;
	 *         final double deltaC = lch1.C - lch2.C;
	 * 
	 *         // Usually this metric should be able to work on the lch color space alone, but something's not quite right.
	 * 
	 *         // see: https://www.hdm-stuttgart.de/international_circle/circular/issues/13_01/ICJ_06_2013_02_069.pdf -> gives very bad results
	 *         // final double deltaH = lch1.h - lch2.h;
	 * 
	 *         // Taken from https://github.com/muak/ColorMinePortable/blob/master/ColorMinePortable/ColorSpaces/Comparisons/CmcComparison.cs
	 *         final var deltaH = Math.sqrt(Math.pow(lab1.a - lab2.a, 2) + Math.pow(lab1.b - lab2.b, 2) - Math.pow(lch1.C - lch2.C, 2));
	 * 
	 *         return Math.sqrt(Math.pow(deltaL / l * Sl, 2) + Math.pow(deltaC / c * Sc, 2) + Math.pow(deltaH / Sh, 2));
	 *         }
	 */
}
