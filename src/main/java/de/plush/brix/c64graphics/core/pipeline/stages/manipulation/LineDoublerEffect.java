/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.image.BufferedImage;
import java.util.function.IntPredicate;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class LineDoublerEffect implements IPipelineStage<BufferedImage, BufferedImage> {

	private final IntPredicate lineDoublingTrigger;

	public LineDoublerEffect() {
		this(n -> n % 2 == 0);
	}

	public LineDoublerEffect(final IntPredicate lineDoublingTrigger) {
		this.lineDoublingTrigger = lineDoublingTrigger;
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		for (int y = 0; y < result.getHeight(); ++y) {
			copyLine(original, result, y, y);
			if (lineDoublingTrigger.test(y)) {
				copyLine(original, result, y, y + 1);
				++y;
			}

		}
		return result;
	}

	private static void copyLine(final BufferedImage original, final BufferedImage result, final int y, final int y2) {
		for (int x = 0; x < result.getWidth(); ++x) {
			final int rgb = original.getRGB(x, y);
			result.setRGB(x, y2, rgb);
		}
	}
}