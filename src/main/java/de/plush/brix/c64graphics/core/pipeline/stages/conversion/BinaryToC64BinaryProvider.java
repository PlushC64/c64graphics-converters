package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import de.plush.brix.c64graphics.core.model.C64BinaryProvider;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class BinaryToC64BinaryProvider implements IPipelineStage<byte[], C64BinaryProvider> {

	@Override
	public C64BinaryProvider apply(final byte[] binary) {
		return new C64BinaryProvider() {
			@Override
			public byte[] toC64Binary() {
				return binary;
			}
		};
	}
}
