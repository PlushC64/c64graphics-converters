package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import static de.plush.brix.c64graphics.core.util.Utils.lazy;

import java.awt.Color;
import java.util.Arrays;

import de.plush.brix.c64graphics.core.util.functions.ByteSupplier;

public enum C64ColorsPepto implements C64Color {

	//@formatter:off
	/**0*/BLACK(0, 0, 0),
	/**1*/WHITE(255,255,255),
	/**2*/RED(104,55,43),
	/**3*/CYAN(112,164,178),
	/**4*/PURPLE(111,61,134),
	/**5*/GREEN(88,141,67),
	/**6*/BLUE(53,40,121),
	/**7*/YELLOW(184,199,111),
	//
	/**8*/ORANGE(111,79,37),
	/**9*/BROWN(67,57,0),
	/**A*/LIGHT_RED(154,103,89),
	/**B*/DARK_GRAY(68,68,68),
	/**C*/GRAY(108,108,108),
	/**D*/LIGHT_GREEN(154,210,132),
	/**E*/LIGHT_BLUE(108,94,181),
	/**F*/LIGHT_GRAY(149,149,149);
	//@formatter:on

	public static final PaletteC64Full PALETTE = new PaletteC64Full() {

		private Color[] colors;

		@Override
		public Color[] colors() {
			if (colors == null) {
				colors = Arrays.stream(values()).map(C64ColorsPepto::color).toArray(Color[]::new);
			}
			return colors.clone();
		}

		@Override
		public String toString() {
			return C64ColorsPepto.class.getSimpleName();
		}

	};

	public final Color color;

	private final ByteSupplier colorCode = lazy(() -> palette().colorCodeOrException(get()));

	C64ColorsPepto(final int r, final int g, final int b) {
		color = new Color(r, g, b);
	}

	@Override
	public Color get() {
		return color;
	}

	@Override
	public byte colorCode() {
		return colorCode.getAsByte(); // can't do this in the constructor (init phase)
	}

	@Override
	public Palette palette() {
		return PALETTE;
	}

}
