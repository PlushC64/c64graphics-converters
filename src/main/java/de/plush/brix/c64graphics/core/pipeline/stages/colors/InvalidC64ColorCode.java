package de.plush.brix.c64graphics.core.pipeline.stages.colors;

public class InvalidC64ColorCode extends InvalidColorCode {

	public InvalidC64ColorCode(final byte code) {
		this(code, "");
	}

	public InvalidC64ColorCode(final byte code, final String message) {
		super(code, 16, message);
	}

	public static byte check(final byte colorCode) {
		return check(colorCode, "");
	}

	public static byte check(final byte colorCode, final String message) {
		if ((colorCode & 0xF0) != 0) { throw new InvalidC64ColorCode(colorCode, message); }
		return colorCode;
	}

}
