package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayDeque;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;

public class FloodFill implements IPipelineStage<BufferedImage, BufferedImage> {

	private final Point floodStart;

	private final Color targetColor;

	private final double tolerance;

	private final ColorDistanceMeasure distanceMeasure;

	public FloodFill(final Point floodStart, final Color targetColor) {
		this(floodStart, targetColor, 1.0 / (2 ^ 32), ColorDistance.EUCLIDEAN);
	}

	/**
	 * @param floodStart
	 *            the point at which to start the flood fill
	 * @param targetColor
	 *            the color to fill
	 * @param tolerance
	 *            a value between 0.0 and 1.0 (inclusive)
	 * @param distanceMeasure
	 *            the object to measure the distance between two colors.
	 */
	public FloodFill(final Point floodStart, final Color targetColor, final double tolerance, final ColorDistanceMeasure distanceMeasure) {
		this.floodStart = new Point(floodStart);
		this.targetColor = targetColor;
		this.tolerance = tolerance;
		this.distanceMeasure = distanceMeasure;
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		final Graphics2D g = (Graphics2D) result.getGraphics();
		try {
			g.drawImage(original, 0, 0, null);
			floodFillImage(result, targetColor);
			return result;
		} finally {
			g.dispose();
		}

	}

	public void floodFillImage(final BufferedImage image, final Color targetColor) {
		final Color srcColor = new Color(image.getRGB(floodStart.x, floodStart.y));

		final var toBeFilled = new ArrayDeque<Point>(image.getWidth() * image.getHeight() * 3 / 4); // one out, four in
		final var alreadyFilled = new boolean[image.getWidth()][image.getHeight()];
		toBeFilled.add(floodStart);

		while (!toBeFilled.isEmpty()) {
			final Point pos = toBeFilled.remove();
			if (mustFill(image, alreadyFilled, pos, srcColor)) {

				image.setRGB(pos.x, pos.y, targetColor.getRGB());
				alreadyFilled[pos.x][pos.y] = true;

				toBeFilled.add(new Point(pos.x + 1, pos.y));
				toBeFilled.add(new Point(pos.x, pos.y - 1));
				toBeFilled.add(new Point(pos.x - 1, pos.y));
				toBeFilled.add(new Point(pos.x, pos.y + 1));
			}
		}
	}

	private boolean mustFill(final BufferedImage image, final boolean[][] alreadyFilled, final Point point, final Color srcColor) {
		return new Rectangle(0, 0, image.getWidth(), image.getHeight()).contains(point) //
				&& !alreadyFilled[point.x][point.y] //
				&& distanceMeasure.distanceOf(srcColor, targetColor) <= tolerance;
	}

	// TODO: try another algorithm: http://www.adammil.net/blog/v126_A_More_Efficient_Flood_Fill.html

}
