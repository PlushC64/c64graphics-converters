/**
 * Color clash resolvers in this package generally expect an image that has already been applied a Commodore target palette and resized to the correct size
 * (usually 320x200 pixels). For multicolor-only formats, like Koala painter, it is okay for the color clash resolver to already expect a pixelated image
 * (double-wide pixels). For multi-resolution-pictures (such as multicolor charmode), they are expected to handle regional resolution-reduction themselves. More
 * generally: Color clash resolvers should expect to get an image of an appropriate resolution to do their job, which is to create an image that can be
 * converted to the targeted C64 graphics format with no conflicting color information.<br>
 * <p>
 * Though converter stages ideally do little more than to create a C64 graphics format from the data they get from the color clash resolvers, this is not set in
 * stone. The converter stage can well cover some conversion aspects, e.g. which bit pair gets assigned which color. Things like this should be handled where
 * appropriate. Converters and color clash resolvers are tightly coupled classes. Since color clash resolving can be a very complicated task in itself, the
 * converter should not expect the clash resolver to handle everything related to color. Ideally creating color information is delayed to when it's needed and
 * forwarded to the next stage that needs it, so it isn't lost.
 * <p>
 * As an example: A multicolor charmode color clash resolver needs to gather information about what color is assigned to what Color-RAM position to do its job,
 * as this determines if a block is best represented as a hires or multicolor char. It will attach this information to the image it returns, so the
 * charmode-converter can pick it up.<br>
 * A Koalapainter clash resolver on the other hand just needs to find the best background color for the image, since for each character block it is not
 * important what color ends up in the Screen- or Color-RAM. It only needs to care about the number of colors in each block and about that one global background
 * color, so this is all it attaches to its output. It is therefore the job of the converter to assign the remaining non-background colors to the Screen- or
 * Color-RAM.
 * <p>
 * See also:
 * <ul>
 * <li>{@link de.plush.brix.c64graphics.core.pipeline.stages.manipulation.PaletteConversion}
 * <li>{@link de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore}
 * <li>{@link de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsPepto}
 * <ul>
 * </p>
 */
package de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes;