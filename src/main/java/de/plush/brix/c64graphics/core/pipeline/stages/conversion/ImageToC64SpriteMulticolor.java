package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static java.lang.Math.min;

import java.awt.Color;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class ImageToC64SpriteMulticolor implements IPipelineStage<BufferedImage, C64Sprite> {

	private final Color pixelColor;

	private final Color mc1D025;

	private final Color mc2D026;

	private final Color background;

	public ImageToC64SpriteMulticolor(final Color background, final Color mc1D025, final Color mc2D026, final Color pixelColor) {
		this.background = background;
		this.mc1D025 = mc1D025;
		this.mc2D026 = mc2D026;
		this.pixelColor = pixelColor;
	}

	@Override
	public C64Sprite apply(final BufferedImage image) {
		final int pixelsPerByte = 8;
		final int bytesPerLine = C64Sprite.WIDTH_PIXELS / pixelsPerByte;
		final byte[] spriteBytes = new byte[bytesPerLine * C64Sprite.HEIGHT_PIXELS];
		for (int y = 0; y < min(C64Sprite.HEIGHT_PIXELS, image.getHeight()); ++y) {
			for (int x = 0; x < min(C64Sprite.WIDTH_PIXELS, image.getWidth()); x += pixelsPerByte) {
				final Byte b = asByte(image.getSubimage(x, y, pixelsPerByte, 1), background, mc1D025, mc2D026, pixelColor);
				final int index = x / pixelsPerByte + y * bytesPerLine;
				spriteBytes[index] = b;
			}
		}
		return new C64Sprite(spriteBytes);
	}

	// TODO: same as ImageToC64CharMulticolor -> refactor!
	private static Byte asByte(final BufferedImage image8x1, final Color background, final Color mc1, final Color mc2, final Color pixelColor) {
		final StringBuilder b = new StringBuilder(C64Char.WIDTH_PIXELS);
		for (int x = 0; x < C64Char.WIDTH_PIXELS; x += 2) {
			final Color pixel = new Color(image8x1.getRGB(x, 0) & 0xFFFFFF); // ignore alpha channel
			b.append(pixel.equals(background)
					? "00" //@formatter:off
						: pixel.equals(mc1) ? "01" //d022
						: pixel.equals(mc2) ? "10" //d023
						: pixel.equals(pixelColor) ? "11" //d023
						: error(pixel)
			);//@formatter:on
		}
		return Byte.valueOf((byte) (Integer.parseInt(b.toString(), 2) & 0xFF));
	}

	private static String error(final Color pixel) {
		throw new IllegalArgumentException(pixel
				+ " is not among the given colors for background, mc1, mc2 and colorRam. Did you forget to solve color clashes?");
	}
}
