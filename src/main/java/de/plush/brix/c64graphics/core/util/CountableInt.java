package de.plush.brix.c64graphics.core.util;

import java.util.Comparator;
import java.util.function.LongUnaryOperator;

public final class CountableInt implements Comparable<CountableInt> {
	private static final Comparator<CountableInt> NATURAL_ORDER = Comparator.<CountableInt> //
			comparingLong(CountableInt::count)//
			.thenComparingInt(CountableInt::intValue);

	public final int intValue;

	public long count;

	private CountableInt(final int intValue, final long count) {
		this.intValue = intValue;
		this.count = count;
	}

	public static WithMissingCount ofValue(final int value) {
		return (final long count) -> { return new CountableInt(value, count); };
	}

	public static WithMissingValue ofCount(final long count) {
		return (final int value) -> { return new CountableInt(value, count); };
	}

	public CountableInt modifyCount(final LongUnaryOperator op) {
		count = op.applyAsLong(count);
		return this;
	}

	public long count() {
		return count;
	}

	public void setCount(final long count) {
		this.count = count;
	}

	public int intValue() {
		return intValue;
	}

	@Override
	public int compareTo(final CountableInt o) {
		return NATURAL_ORDER.compare(this, o);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (count ^ count >>> 32);
		result = prime * result + intValue;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final CountableInt other = (CountableInt) obj;
		if (count != other.count) { return false; }
		if (intValue != other.intValue) { return false; }
		return true;
	}

	@Override
	public String toString() {
		return "int: " + intValue + ", count: " + count;
	}

	public interface WithMissingCount {
		CountableInt withCount(long count);
	}

	public interface WithMissingValue {
		CountableInt withValue(int value);
	}

}