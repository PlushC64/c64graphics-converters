package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.UtilsImage.*;
import static java.util.Arrays.stream;
import static java.util.Comparator.*;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.CharmodeColorClashResolverHires.PreProcessedImage;

public class ImageToC64CharmodePictureHires implements IPipelineStage<BufferedImage, C64CharsetScreenColorRam> {

	private final Palette palette;

	private final ColorDistanceMeasure distanceMeasure;

	private final CharacterReduction characterReduction;

	private final Function<BufferedImage, PreProcessedImage> colorClashResolver;

	public ImageToC64CharmodePictureHires(final Palette palette, final ColorDistanceMeasure distanceMeasure, final CharacterReduction characterReduction,
			final Function<BufferedImage, PreProcessedImage> colorClashResolver) {
		this.palette = palette;
		this.distanceMeasure = distanceMeasure;
		this.characterReduction = characterReduction;
		this.colorClashResolver = colorClashResolver;
	}

	// found no meaningful way to extract methods that make it more readable
	@SuppressWarnings({ "checkstyle:CyclomaticComplexity", "checkstyle:NPathComplexity" })
	@Override
	public C64CharsetScreenColorRam apply(final BufferedImage image) {
		final PreProcessedImage preProcessedImage = colorClashResolver.apply(image);
		final Color background = preProcessedImage.background();
		final long start = System.nanoTime();
		// new ImageDisplay<>(3f, "clashes removed").onImage(preProcessedImage);

		final Map<C64Char, List<Point>> blockImageToPositions = new HashMap<>();
		final C64Bitmap bitmap = new C64Bitmap();
		final C64ColorRam colorRam = new C64ColorRam(palette.colorCodeOrException(background));
		final Dimension blocks = C64Bitmap.sizeBlocks();
		for (int y = 0; y < blocks.height; ++y) {
			for (int x = 0; x < blocks.width; ++x) {
				final Point blockPos = new Point(x, y);
				final int xPix = x * C64Char.WIDTH_PIXELS;
				final int yPix = y * C64Char.HEIGHT_PIXELS;
				final BufferedImage blockImage = preProcessedImage.getSubimage(xPix, yPix, C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS);
				final Color pixelColor = pixelColor(background, blockImage, blockPos);
				final C64Char block = new ImageToC64CharHires(background, pixelColor).apply(blockImage);
				blockImageToPositions.computeIfAbsent(block, (k) -> new ArrayList<>(1)).add(blockPos);
				bitmap.setBlock(x, y, block);
				colorRam.setColorCode(x, y, palette.colorCodeOrException(pixelColor));
			}
		}

		if (blockImageToPositions.size() > 1) {
			System.out.println("number of characters: " + blockImageToPositions.size() + (blockImageToPositions.size() > 256 ? " (reducing...)" : ""));

			final Set<C64Char> victimBlacklist = new HashSet<>(); // will be reduced later
			List<Map.Entry<PreProcessedCharacterImage, C64Char>> possibleSurrogates = null;
			List<VictimToSurrogateMapping> priorityQueue = null;
			while (blockImageToPositions.size() > 256) {
				final int queueSize = blockImageToPositions.size();
				if (possibleSurrogates == null || possibleSurrogates.isEmpty()) {
					possibleSurrogates = createPossibleSurrogates(preProcessedImage, background, blockImageToPositions);
					victimBlacklist.clear();
					addSingleColoredTiles(possibleSurrogates, victimBlacklist);
					if (priorityQueue != null) {
						priorityQueue.clear();
					}
				}
				if (priorityQueue == null || priorityQueue.isEmpty()) {
					priorityQueue = victimToSurrogateMappings(preProcessedImage, blockImageToPositions, possibleSurrogates, victimBlacklist, queueSize);
					if (priorityQueue.isEmpty()) { // e.g. black list contains every character
						victimBlacklist.clear(); // drink the cool aid as last resort
						addSingleColoredTiles(possibleSurrogates, victimBlacklist);
						priorityQueue = victimToSurrogateMappings(preProcessedImage, blockImageToPositions, possibleSurrogates, victimBlacklist, queueSize);
					}
				}
				final VictimToSurrogateMapping vsMapping = reduceNumberOfCharacters(background, bitmap, colorRam, blockImageToPositions, priorityQueue);

				// cleanup: remove victim and surrogate from data structures
				// a surrogate should not become a victim, so no victim is replaced by a surrogate that is again replaced, raising the error each step:
				victimBlacklist.add(vsMapping.surrogate); // exclude surrogate from the possible list of victims
				// removeIf is not parallel and has to juggle data in place, i.e. in O(n�), avoid by parallel filtering and replacing the original
				possibleSurrogates = possibleSurrogates.stream().parallel()//
						.filter(surrogateImageToChar -> !vsMapping.victim.equals(surrogateImageToChar.getValue())).collect(toList());
				priorityQueue = priorityQueue.stream().parallel()//
						.filter(m -> !(m.surrogate.equals(vsMapping.victim) || m.victim.equals(vsMapping.victim) || m.victim.equals(vsMapping.surrogate)))
						.collect(toList());
			}
		}
		// With less or equal than 256 images, we can build a charset, screen and ColorRam and be done
		final C64Charset charset = new C64Charset();
		final C64Screen screen = new C64Screen((byte) 0);
		for (int y = 0; y < blocks.height; ++y) {
			for (int x = 0; x < blocks.width; ++x) {
				final C64Char block = bitmap.blockAt(x, y);
				charset.addCharacter(block);
				screen.setScreenCode(x, y, charset.screenCodeOf(block).getAsByte());
			}
		}
		final long end = System.nanoTime();
		System.out.println("time (ms):" + TimeUnit.NANOSECONDS.toMillis(end - start));
		return new C64CharsetScreenColorRam(charset, screen, colorRam, palette.colorCodeOrException(background));
	}

	private static void addSingleColoredTiles(final List<Map.Entry<PreProcessedCharacterImage, C64Char>> possibleSurrogates,
			final Collection<C64Char> victimBlacklist) {
		possibleSurrogates.stream().filter(e -> colors(e.getKey()).size() < 2).map(Map.Entry::getValue).collect(toSet()).forEach(victimBlacklist::add);
	}

	private List<Map.Entry<PreProcessedCharacterImage, C64Char>> createPossibleSurrogates(final PreProcessedImage image, final Color background,
			final Map<C64Char, List<Point>> blockImageToPositions) {
		final long start = System.nanoTime();
		try {
			// for each non-background image color, create a block-image from each of the characters: max 16.000 images
			final Set<Color> colors = characterReduction == CharacterReduction.UsingImageColorsOnly ? colors(image) : stream(palette.colors()).collect(toSet());
			return colors.stream()//
					.filter(color -> !background.equals(color))//
					.map(color -> blockImageToPositions.keySet().stream()//
							.map(block -> new SimpleEntry<>(new C64CharToImageHires(palette, () -> background, color).apply(block), block)))//
					.flatMap(identity())//
					.collect(toList());
		} finally {
			final long end = System.nanoTime();
			System.out.println("createPossibleSurrogates, time: " + TimeUnit.NANOSECONDS.toMillis(end - start));
		}
	}

	private VictimToSurrogateMapping reduceNumberOfCharacters(final Color background, final C64Bitmap bitmap, final C64ColorRam colorRam,
			final Map<C64Char, List<Point>> blockImageToPositions, final List<VictimToSurrogateMapping> victimToSurrogateMappings) {
		final VictimToSurrogateMapping victimToSurrogateMapping = victimToSurrogateMappings.remove(victimToSurrogateMappings.size() - 1);
		// System.out.println("surrogateQ: " + victimToSurrogateMappings.size());
		// replace victim by surrogate
		// System.out.println("replacing character by surrogates");
		final C64Char victim = victimToSurrogateMapping.victim;
		// System.out.println("victim: \n" + victim);
		final C64Char surrogate = victimToSurrogateMapping.surrogate;
		// System.out.println("surrogate: \n" + surrogate);
		final BufferedImage surrogateImage = victimToSurrogateMapping.surrogateImage;
		final byte pixelColorCode = palette.colorCodeOrException(pixelColor(background, surrogateImage));

		final List<Point> victimPositions = blockImageToPositions.get(victim);
		// System.out.println("victimPositions: \n" + victimPositions);
		victimPositions.stream()//
				.peek(p -> colorRam.setColorCode(p.x, p.y, pixelColorCode))//
				.forEach(p -> bitmap.setBlock(p.x, p.y, surrogate));
		blockImageToPositions.get(surrogate).addAll(victimPositions);
		blockImageToPositions.remove(victim);
		return victimToSurrogateMapping;

	}

	private List<VictimToSurrogateMapping> victimToSurrogateMappings(final PreProcessedImage image, final Map<C64Char, List<Point>> blockImageToPositions,
			final List<Map.Entry<PreProcessedCharacterImage, C64Char>> possibleSurrogates, final Set<C64Char> victimBlacklist, final int limit) {
		final long start = System.nanoTime();
		try {
			// for the (n=limit) least occurring characters, look for a surrogate in all images, sort by error:
			final List<VictimToSurrogateMapping> queue = blockImageToPositions.entrySet().stream()//
					.parallel()//
					.filter(entry -> !victimBlacklist.contains(entry.getKey()))// exclude characters that are already dismissed
					.sorted(comparingInt(charToPos -> charToPos.getValue().size()))// occurrence counts
					.limit(limit)//
					// .peek(charToPos -> System.out.println("among the 10 most infrequent chars: \n" + charToPos))//
					.map(charToPos -> {
						final C64Char possibleVictim = charToPos.getKey();
						final List<Point> possibleVictimPositions = charToPos.getValue();
						return surrogateMappingsForSingleVictim(possibleVictim, possibleVictimPositions, possibleSurrogates, image);
					}).flatMap(list -> list.stream())// stream of possible possibleVictimToSurrogateMappings (weighted)
					.sorted(comparingDouble(VictimToSurrogateMapping::weight) //
							.thenComparing(vso -> blockImageToPositions.get(vso.victim).size())// least frequentVictims go first, if two are equally good
							.thenComparing(VictimToSurrogateMapping::sortStabilizer))// utilizes stable hash codes
					.collect(toList());
			Collections.reverse(queue); // slow, but a reverse comparator gives strange results and we want to poll from the back of the list for O(1) polling
			return queue;

		} finally {
			final long end = System.nanoTime();
			System.out.println("victimToSurrogateMappings, time: " + TimeUnit.NANOSECONDS.toMillis(end - start));
		}
	}

	private List<VictimToSurrogateMapping> surrogateMappingsForSingleVictim(final C64Char possibleVictim, final List<Point> possibleVictimPositions,
			final List<Map.Entry<PreProcessedCharacterImage, C64Char>> possibleSurrogates, final PreProcessedImage image) {
		return possibleVictimPositions.stream().parallel()//
				// a char (bit pattern) in an image can have different sub images based on coloring at a the given position.
				.map(blockPos -> image.getSubimage(blockPos.x * C64Char.WIDTH_PIXELS, blockPos.y * C64Char.HEIGHT_PIXELS, C64Char.WIDTH_PIXELS,
						C64Char.HEIGHT_PIXELS)) // original image
				.map(possibleVictimImage -> possibleSurrogates.stream() //
						// ignore own modifications:
						.filter(possibleSurrogateEntry -> !possibleVictim.equals(possibleSurrogateEntry.getValue()))
						.map(possibleSurrogateEntry -> new VictimToSurrogateMapping(possibleVictim, possibleSurrogateEntry.getValue(),
								possibleSurrogateEntry.getKey(), //
								error(possibleVictimImage, possibleSurrogateEntry.getKey(), distanceMeasure))))
				.flatMap(Function.identity())//
				.collect(toList()); // important: do no leave a stream here and flatMap later -> speed drops dramatically!
	}

	private static Color pixelColor(final Color background, final BufferedImage blockImage, final Point... positions) {
		final Set<Color> colors = colors(blockImage);
		if (colors.size() > 2) {
			throw new IllegalStateException(
					"unexpected number of colors (" + colors.size() + ") at " + Arrays.toString(positions) + ". Have you removed color clashes?");
		}
		colors.remove(background);
		if (colors.size() > 1) {
			throw new IllegalStateException("more than one non-background color at " + Arrays.toString(positions)
					+ ". Have you removed color clashes? (Colors: " + colors + " background: " + background);
		}
		return colors.size() > 0 ? colors.iterator().next() : background;
	}

	private static class VictimToSurrogateMapping {
		final C64Char victim;

		final C64Char surrogate;

		final PreProcessedCharacterImage surrogateImage;

		final double weight;

		public VictimToSurrogateMapping(final C64Char victim, final C64Char surrogate, final PreProcessedCharacterImage surrogateImage, final double weight) {
			this.victim = victim;
			this.surrogate = surrogate;
			this.surrogateImage = surrogateImage;
			this.weight = weight;
		}

		public double weight() {
			return weight;
		}

		public int sortStabilizer() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (surrogate == null ? 0 : surrogate.hashCode());
			result = prime * result + (victim == null ? 0 : victim.hashCode());
			return result;
		}

	}

}
