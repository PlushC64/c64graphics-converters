package de.plush.brix.c64graphics.core.model;

import static de.plush.brix.c64graphics.core.util.Utils.*;
import static java.util.Arrays.copyOfRange;

import java.io.File;
import java.net.*;
import java.util.*;
import java.util.function.UnaryOperator;
import java.util.stream.*;

import de.plush.brix.c64graphics.core.util.*;
import de.plush.brix.c64graphics.core.util.charpixeldistance.C64CharHiresPixelDistanceMeasure;
import de.plush.brix.c64graphics.core.util.collections.ByteSet;

public class C64Charset implements C64BinaryProvider {

	public static final int MAX_CHARACTERS = 256;

	private final List<C64Char> chars = new ArrayList<>();

	public C64Charset() {}

	public C64Charset(final C64Char... initialChars) {
		this(Arrays.stream(initialChars));
	}

	public C64Charset(final Stream<C64Char> initialChars) {
		initialChars.forEach(chars::add);
	}

	public C64Charset(final C64Charset original) {
		chars.addAll(original.chars);
	}

	/**
	 * Opposed to subList, the returned charset is not backed by the original charset, instead it is a new, independent instance.
	 * 
	 * @return a new charset, that only contains the characters from the given codes. The characters will be assigned new codes in the order of appearance.
	 *         i.e.:
	 * 
	 *         <pre>
	 *         <code class="java">
	 *         var b = cs.fromScreenCodes((byte) 10,(byte) 20,(byte) 30);
	 *         assert b.characterBlockOf((byte)0) == cs.characterBlockOf((byte)10);
	 *         assert b.characterBlockOf((byte)1) == cs.characterBlockOf((byte)20);
	 *         assert b.characterBlockOf((byte)2) == cs.characterBlockOf((byte)30);
	 *         </code>
	 *         </pre>
	 */
	public C64Charset subCharsetFromScreenCodes(final byte... codes) {
		final C64Charset cs = new C64Charset();
		for (final byte code : codes) {
			characterBlockOf(code).ifPresent(cs::addCharacter);
		}
		return cs;
	}

	/** @return a new charset, where each character is reverse, see: {@link C64Char#reversed()} */
	public C64Charset reversed() {
		return transformed(C64Char::reversed);
	}

	/** @return a new charset, where each character is flipped horizintally, see: {@link C64Char#flippedHorizontally()} */
	public C64Charset flippedHorizontally() {
		return transformed(C64Char::flippedHorizontally);
	}

	/** @return a new charset, where each character is flipped vertically, see: {@link C64Char#flippedVertically()} */
	public C64Charset flippedVertically() {
		return transformed(C64Char::flippedVertically);
	}

	/** @return a new charset, where each character is rolled n lines up, see: {@link C64Char#rolledPixelsUp(int)} */
	public C64Charset rolledPixelsUp(final int lines) {
		return transformed(c -> c.rolledPixelsUp(lines));
	}

	/** @return a new charset, where each character is rolled n lines down, see: {@link C64Char#rolledPixelsDown(int)} */
	public C64Charset rolledPixelsDown(final int lines) {
		return transformed(c -> c.rolledPixelsDown(lines));
	}

	/** @return a new charset, where each character is rolled n columns left, see: {@link C64Char#rolledPixelsLeft(int)} */
	public C64Charset rolledPixelsLeft(final int columns) {
		return transformed(c -> c.rolledPixelsLeft(columns));
	}

	/** @return a new charset, where each character is rolled n columns right, see: {@link C64Char#rolledPixelsRight(int)} */
	public C64Charset rolledPixelsRight(final int columns) {
		return transformed(c -> c.rolledPixelsRight(columns));
	}

	/** @return a new charset, where each character is shifted n lines up, see: {@link C64Char#shiftedPixelsUp(int)} */
	public C64Charset shiftedPixelsUp(final int lines) {
		return transformed(c -> c.shiftedPixelsUp(lines));
	}

	/** @return a new charset, where each character is shifted n lines down, see: {@link C64Char#shiftedPixelsDown(int)} */
	public C64Charset shiftedPixelsDown(final int lines) {
		return transformed(c -> c.shiftedPixelsDown(lines));
	}

	/** @return a new charset, where each character is shifted n columns left, see: {@link C64Char#shiftedPixelsLeft(int)} */
	public C64Charset shiftedPixelsLeft(final int columns) {
		return transformed(c -> c.shiftedPixelsLeft(columns));
	}

	/** @return a new charset, where each character is shifted n columns right, see: {@link C64Char#shiftedPixelsRight(int)} */
	public C64Charset shiftedPixelsRight(final int columns) {
		return transformed(c -> c.shiftedPixelsRight(columns));
	}

	/** @return a new charset, where each character is and'ed with the given character, see: {@link C64Char#and(C64Char)} */
	public C64Charset and(final C64Char mask) {
		return transformed(c -> c.and(mask));
	}

	/** @return a new charset, where each character is or'ed with the given character, see: {@link C64Char#or(C64Char)} */
	public C64Charset or(final C64Char mask) {
		return transformed(c -> c.or(mask));
	}

	/** @return a new charset, where each character is xor'ed with the given character, see: {@link C64Char#xor(C64Char)} */
	public C64Charset xor(final C64Char mask) {
		return transformed(c -> c.xor(mask));
	}

	/**
	 * @param transformation
	 *            an operator to transform a character. the operator may fail on <tt>null</t>, as it will never be calles for <tt>null</tt> .
	 * @return a new charset, where each character is transformed by the given transformation operator.
	 */
	public C64Charset transformed(final UnaryOperator<C64Char> transformation) {
		return new C64Charset(charStream().map(nullable(transformation)));
	}

	public boolean isFull() {
		return chars.size() == MAX_CHARACTERS;
	}

	public boolean isEmpty() {
		return chars.isEmpty();
	}

	public boolean contains(final C64Char chr) {
		return chars.contains(chr);
	}

	public OptionalByte screenCodeOf(final C64Char character) {
		final int index = chars.indexOf(character);
		return index < 0 ? OptionalByte.empty() : OptionalByte.of(toUnsignedByte(index));
	}

	public Optional<C64Char> characterBlockOf(final byte code) {
		final int index = toPositiveInt(code);
		return 0 <= index && index < chars.size() ? Optional.of(chars.get(index)) : Optional.empty();
	}

	public boolean addAllCharacters(final Collection<C64Char> chars) throws TooManyCharactersInCharset {
		return addAllCharacters(chars.stream());
	}

	public boolean addAllCharacters(final Stream<C64Char> chars) throws TooManyCharactersInCharset {
		final var before = size();
		chars.forEach(this::addCharacter);
		return size() != before;
	}

	public boolean addCharacter(final C64Char chr) throws TooManyCharactersInCharset {
		final boolean isNew = !chars.contains(chr);
		if (isNew) {
			addCharacterEvenIfAlreadyPresent(chr);
		}
		return isNew;
	}

	public boolean addAllCharactersEvenIfAlreadyPresent(final Collection<C64Char> chars) throws TooManyCharactersInCharset {
		return addAllCharactersEvenIfAlreadyPresent(chars.stream());
	}

	public boolean addAllCharactersEvenIfAlreadyPresent(final Stream<C64Char> chars) throws TooManyCharactersInCharset {
		return chars.filter(this::addCharacterEvenIfAlreadyPresent).count() > 0; // findAny() will short circuit on the firstz finding and not work as expected
	}

	public boolean addCharacterEvenIfAlreadyPresent(final C64Char chr) throws TooManyCharactersInCharset {
		if (isFull()) { throw new TooManyCharactersInCharset("charset full"); }
		return chars.add(chr);
	}

	public void addBlanksForForMissingScreenCodes(final ByteSet screenCodes) {
		screenCodes.reject(b -> characterBlockOf(b).isPresent())//
				.forEach(b -> putCharacter(b, C64Char.BLANK));
	}

	/**
	 * Puts a given character to the given character code, if the character set has less characters than the given code, fills up the character set up to the
	 * given character code with with
	 * {@link C64Char#BLANK blanks}.
	 * 
	 * @param code
	 *            the character code (0x00 - 0xff)
	 * @param chr
	 *            the character to store for that character code
	 */
	public void putCharacter(final byte code, final C64Char chr) {
		putCharacter(code, chr, C64Char.BLANK);
	}

	/**
	 * Puts a given character to the given character code, if the character set has less characters than the given code, fills up the character set up to the
	 * given character code with the given filler character this comes handy, if you don't want to fill with blanks, but with a fixed pattern or reverdes
	 * 
	 * @param code
	 *            the character code (0x00 - 0xff)
	 * @param chr
	 *            the character to store for that character code
	 * 
	 */
	public void putCharacter(final byte code, final C64Char chr, final C64Char gapFiller) {
		final int index = toPositiveInt(code);
		while (chars.size() <= index) {
			chars.add(gapFiller);
		}
		chars.set(index, chr);
	}

	public Set<C64Char> uniqueChars() {
		return new HashSet<>(chars);
	}

	public Stream<C64Char> charStream() {
		return chars.stream();
	}

	public <T extends Collection<? super C64Char>> T addAllCharsTo(final T target) {
		target.addAll(chars);
		return target;
	}

	public C64Char closestMatch(final C64Char character, final C64CharHiresPixelDistanceMeasure distanceMeasure) {
		return contains(character) ? character : Collections.min(chars, distanceMeasure.compareByDistanceTo(character));
	}

	public int size() {
		return chars.size();
	}

	@Override
	public String toString() {
		return chars.stream().map(C64Char::toString).collect(Collectors.joining("\n"));
	}

	public static C64Charset loadCharset(final File file, final StartAddress startAddress) {
		return loadCharset(file.toURI(), startAddress);
	}

	public static C64Charset loadCharset(final URI uri, final StartAddress startAddress) {
		return fromBinary(readFileInto(uri, new byte[0x0800], startAddress));
	}

	public static C64Charset loadCharset(final URL url, final StartAddress startAddress) {
		return fromBinary(readFileInto(url, new byte[0x0800], startAddress));
	}

	public static C64Charset load(final ROM romCharset) {
		return fromBinary(romCharset.load());
	}

	public static C64Charset fromBinary(final byte[] charsetBinary) {
		final C64Charset c64Charset = new C64Charset();
		for (int t = 0; t < 0x0800; t += C64Char.HEIGHT_PIXELS) {
			final C64Char character = new C64Char(copyOfRange(charsetBinary, t, t + C64Char.HEIGHT_PIXELS));
			c64Charset.chars.add(character); // allow duplicates!
		}
		if (c64Charset.size() != 0x100) { throw new IllegalStateException("file does not contain a full charset"); }
		return c64Charset;
	}

	@Override
	public byte[] toC64Binary() {
		return toByteArray(chars.stream().map(C64Char::toC64Binary));
	}

	/**
	 * Various types of C64/VIC-20/PET/C128 ROM charsets.
	 * This enumeration is by no means meant to be complete.
	 * 
	 * @see <a href ="https://www.pagetable.com/c64ref/charset/">https://www.pagetable.com/c64ref/charset/</a>
	 * @see <a href ="http://www.zimmers.net/anonftp/pub/cbm/firmware/characters/">http://www.zimmers.net/anonftp/pub/cbm/firmware/characters/</a>
	 */
	public enum ROM {

		// ------- STANDARD (US) CHARSETS -------

		/** Official C64 ROM charset, used in standard non-localized (US) Commodore 64 computers. */
		C64_LowerCase("c64ROMCharset.bin", 0x0800), //
		/** Official C64 ROM charset, used in standard non-localized (US) Commodore 64, TED and C128 computers. */
		C64_UpperCase("c64ROMCharset.bin", 0), //
		/**
		 * Official PET ROM charset, used in standard non-localized (US) computers.
		 */
		PET_LowerCase("pet_us_lower.bin", 0), //
		/**
		 * Official PET ROM charset, used in standard non-localized (US) computers.
		 */
		PET_UpperCase("pet_us_upper.bin", 0), //
		/**
		 * Official VIC-20 ROM charset, used in standard non-localized (US) computers. This is the same as the PET charset, only with the British Pound (£)
		 * symbol instead of a backslash (\).
		 */
		VIC20_LowerCase("vic-20_us_lower.bin", 0), //
		/**
		 * Official VIC-20 ROM charset, used in standard non-localized (US) computers. This is the same as the PET charset, only with the British Pound (£)
		 * symbol instead of a backslash (\).
		 */
		VIC20_UpperCase("vic-20_us_upper.bin", 0), //
		/**
		 * Official TED charset. This is a refinement of the Standard C64 lower charset with only a few different pixels for the letters b, c, d, e, f, h, i, j,
		 * k, l, m, n, s, t, u, w, and y.
		 */
		TED_LowerCase("ted_us_lower.bin", 0),
		/** Official TED charset, same as {@link #C64_UpperCase} */
		TED_UpperCase(C64_UpperCase.filename, C64_UpperCase.fileOffset), //
		/** Official C128 charset, used in standard non-localized C-128 computers. Same as {@link #C64_UpperCase} */
		C128_UpperCase(C64_UpperCase.filename, C64_UpperCase.fileOffset), //
		/**
		 * Official C128 charset, used in standard non-localized C-128 computers.
		 * This Is a refinement of the TED lower case charset with only a few pixels changed for the letter m that looked rather funny on the TED.
		 * This one looks like a small uppercase M though, hardly an improvement.
		 */
		C128_LowerCase("c128_us_lower.bin", 0), //
		/**
		 * Commodore 65 alternative character set, taken from a C65 ROM with
		 * the checksum $CAFF. This is an exact copy of the Commodore 128 character
		 * set.
		 */
		C65_CAFF_LowerCase(C128_LowerCase.filename, C128_LowerCase.fileOffset), //
		/**
		 * Commodore 65 alternative character set, taken from a C65 ROM with
		 * the checksum $CAFF. This is an exact copy of the Commodore 128 character
		 * set.
		 */
		C65_CAFF_UpperCase(C128_UpperCase.filename, C128_UpperCase.fileOffset), //

		// ------- LOCALIZED CHARSETS -------

		/**
		 * Official Commodore 64 Swedish/Finnish character set with the å, ä and ö
		 * characters. This is the debugged version from <a href ="https://www.pagetable.com/c64ref/charset/">https://www.pagetable.com/c64ref/charset/</a>.
		 */
		C64_Swedish_LowerCase("c64_swedish_lower.bin", 0), //
		/**
		 * Official Commodore 64 Swedish/Finnish character set with the å, ä and ö
		 * characters. This is the debugged version from <a href ="https://www.pagetable.com/c64ref/charset/">https://www.pagetable.com/c64ref/charset/</a>.
		 */
		C64_Swedish_UpperCase("c64_swedish_upper.bin", 0), //
		/**
		 * Official Japanese character set used in the VIC-1001 (the japanese VIC-20). The British pound (£) has
		 * been replaced with a Japanese yen (¥) symbol, and the lowercase/uppercase
		 * set has been replaced with an uppercase/Kanji set of glyphs.
		 * 
		 * @see <a href="https://en.wikipedia.org/wiki/Commodore_VIC-20">https://en.wikipedia.org/wiki/Commodore_VIC-20</a>
		 */
		VIC20_JapaneseUpper("pet_japanese_upper.bin", 0),
		/**
		 * Official Japanese character set used in the VIC-1001 (the japanese VIC-20). This is the "lowercase/uppercase"
		 * set, which has been replaced with an uppercase/Kanji set of glyphs.
		 * 
		 * @see <a href="https://en.wikipedia.org/wiki/Commodore_VIC-20">https://en.wikipedia.org/wiki/Commodore_VIC-20</a>
		 */
		VIC20_JapaneseUpperKanji("pet_vic-20_japanese_upper-kanji.bin", 0),
		/**
		 * Greek character generator ROM. Some Latin characters have been replaced
		 * with Greek ones. This font is not very consistent: for instance, the
		 * two copies of the @ sign at code positions $00 and $80, as do the
		 * two copies of the capital Phi at $06 and $c6. This character generator
		 * was found on a 4-kilobyte chip, whose first half was identical with
		 * the 901447-10 character generator.
		 */
		PET_GreekUpper("pet_greek_upper.bin", 0),
		/**
		 * Greek character generator ROM. Some Latin characters have been replaced
		 * with Greek ones. This font is not very consistent: for instance, the
		 * two copies of the @ sign at code positions $00 and $80, as do the
		 * two copies of the capital Phi at $06 and $c6. This character generator
		 * was found on a 4-kilobyte chip, whose first half was identical with
		 * the 901447-10 character generator.
		 */
		PET_GreekLower("pet_greek_lower.bin", 0),
		/**
		 * A German character set modelled after the Vic-20 character set. Also used
		 * in the German Commodore 128 (top part of the 315079-01 ROM) and in the
		 * German 64DX (C65) prototype.
		 */
		C128_German_LowerCase("c128_german_lower.bin", 0), //
		/**
		 * A German character set modelled after the Vic-20 character set. Also used
		 * in the German Commodore 128 (top part of the 315079-01 ROM) and in the
		 * German 64DX (C65) prototype.
		 */
		C128_German_UpperCase("c128_german_upper.bin", 0), //
		/**
		 * A Norwegian/Danish character set modelled after the Vic-20 character set.
		 * Used in the Norwegian/Danish Commodore 128. The characters @, !, /, &lt;, &gt;
		 * and the up arrow are different, and so are many graphics characters. The
		 * characters [£] have been replaced with ÆØÅ.
		 */
		C128_Norwegian_LowerCase("c128_norwegian_lower.bin", 0), //
		/**
		 * A Norwegian/Danish character set modelled after the Vic-20 character set.
		 * Used in the Norwegian/Danish Commodore 128. The characters @, !, /, &lt;, &gt;
		 * and the up arrow are different, and so are many graphics characters. The
		 * characters [£] have been replaced with ÆØÅ.
		 */
		C128_Norwegian_UpperCase("c128_norwegian_lower.bin", 0), //

		/**
		 * Used in the German 64DX (C65) prototype.
		 * A German character set modelled after the Vic-20 character set. Also used
		 * in the German Commodore 128 (top part of the 315079-01 ROM).
		 */
		C65_C64DX_German_LowerCase(C128_German_LowerCase.filename, C128_German_LowerCase.fileOffset),
		/**
		 * Used in the German 64DX (C65) prototype.
		 * A German character set modelled after the Vic-20 character set. Also used
		 * in the German Commodore 128 (top part of the 315079-01 ROM).
		 */
		C65_C64DX_German_UpperCase(C128_German_UpperCase.filename, C128_German_UpperCase.fileOffset),
		//
		;

		private final String filename;

		private final int fileOffset;

		ROM(final String filename, final int fileOffset) {
			this.filename = filename;
			this.fileOffset = fileOffset;
		}

		byte[] load() {
			return readFileInto(getClass().getResource("resources/" + filename), fileOffset, new byte[0x0800], StartAddress.NotInFile);
		}

	}
}
