package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static java.lang.Math.min;

import java.awt.Color;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

public class ImageToC64SpriteHires implements IPipelineStage<BufferedImage, C64Sprite> {

	private final Color pixelColor;

	public ImageToC64SpriteHires(final Color pixelColor) {
		this.pixelColor = pixelColor;
	}

	@Override
	public C64Sprite apply(final BufferedImage image) {
		final int bytesPerLine = C64Sprite.WIDTH_PIXELS / C64Char.WIDTH_PIXELS;
		final byte[] spriteBytes = new byte[bytesPerLine * C64Sprite.HEIGHT_PIXELS];
		for (int y = 0; y < min(C64Sprite.HEIGHT_PIXELS, image.getHeight()); ++y) {
			for (int x = 0; x < min(C64Sprite.WIDTH_PIXELS, image.getWidth()); x += C64Char.WIDTH_PIXELS) {
				final byte b = asByte(image.getSubimage(x, y, min(C64Char.WIDTH_PIXELS, image.getWidth() - x), 1), pixelColor);
				final int index = x / C64Char.WIDTH_PIXELS + y * bytesPerLine;
				spriteBytes[index] = b;
			}
		}
		return new C64Sprite(spriteBytes);
	}

	// static to encourage extensive inlining into this method.
	@SuppressFBWarnings(value = "SF_SWITCH_NO_DEFAULT", justification = "false positive, there is a default")
	private static byte asByte(final BufferedImage imageNx1, final Color pixelColor) {
		byte x = 0;
		switch (imageNx1.getWidth()) { // force unrolled jump-table instead of loop
		case 8:
			x |= asBit(imageNx1, 7, pixelColor);
		case 7:
			x |= asBit(imageNx1, 6, pixelColor);
		case 6:
			x |= asBit(imageNx1, 5, pixelColor);
		case 5:
			x |= asBit(imageNx1, 4, pixelColor);
		case 4:
			x |= asBit(imageNx1, 3, pixelColor);
		case 3:
			x |= asBit(imageNx1, 2, pixelColor);
		case 2:
			x |= asBit(imageNx1, 1, pixelColor);
		case 1:
			x |= asBit(imageNx1, 0, pixelColor);
		default: // do nothing
		}
		return x;
	}

	private static byte asBit(final BufferedImage imageNx1, final int x, final Color pixelColor) {
		final int shift = C64Char.WIDTH_PIXELS - 1 - x;
		return (byte) (new Color(imageNx1.getRGB(x, 0) & 0xFFFFFF).equals(pixelColor) ? 1 << shift : 0);
	}

}
