package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.util.Arrays;
import java.util.function.*;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.*;
import de.plush.brix.c64graphics.core.pipeline.stages.compression.RLEpacker;
import de.plush.brix.c64graphics.core.util.Utils;

public class C64CharsetMultiScreenToPackedCharsetScreenBinary implements IPipelineStage<C64CharsetMultiScreen, PackedCharsetScreenBinary> {

	private final Supplier<Function<byte[], byte[]>> packerProvider;

	/** Defaults to using an RLE packer with the given controlByte. */
	public C64CharsetMultiScreenToPackedCharsetScreenBinary(final byte controlByte) {
		this(new RLEpacker(controlByte));
	}

	/** Uses the given packer instance for all screens. <u>Important</u>: The packer must be stateless. as parallel packing might occur.. */
	public C64CharsetMultiScreenToPackedCharsetScreenBinary(final Function<byte[], byte[]> packer) {
		this(() -> packer);
	}

	/**
	 * Calls the given provider for each single screen in order to obtain an instance of the packer. <br>
	 * <u>Important</u>: If the provider returns the same instance on each call, the packer must be stateless, as parallel packing might occur.
	 */
	public C64CharsetMultiScreenToPackedCharsetScreenBinary(final Supplier<Function<byte[], byte[]>> packerProvider) {
		this.packerProvider = packerProvider;
	}

	@Override
	public PackedCharsetScreenBinary apply(final C64CharsetMultiScreen charMultiScreen) {
		return new PackedCharsetScreenBinary(charMultiScreen.charsetBinary(), packed(charMultiScreen.screens()));
	}

	private byte[] packed(final C64Screen[] screens) {
		return Utils.toByteArray(Arrays.stream(screens).parallel()//
				.map(C64Screen::toC64Binary)//
				.map(packerProvider.get()::apply));
	}

}
