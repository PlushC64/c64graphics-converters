package de.plush.brix.c64graphics.core.util.collections;

import static java.lang.Math.*;
import static java.util.stream.Collectors.toList;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.*;
import java.util.stream.*;

import de.plush.brix.c64graphics.core.util.*;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

public class DoubleArrayList {

	private static final int MAX_ARRAY_LENGTH = Integer.MAX_VALUE - 8;

	private static final int DEFAULT_CAPACITY = 10;

	private final int initialCapacity;

	private double[] elementData;

	int size;

	public DoubleArrayList() {
		this(DEFAULT_CAPACITY);
	}

	public DoubleArrayList(final int initialCapacity) {
		this.initialCapacity = initialCapacity;
		elementData = new double[initialCapacity];
	}

	public DoubleArrayList(final double... doubles) {
		elementData = Arrays.copyOf(doubles, doubles.length);
		initialCapacity = doubles.length;
		size = doubles.length;
	}

	public DoubleArrayList(final DoubleArrayList doubles) {
		// we can't delegate to the "double..." constructor, as the array may be larger than the actual size of the list
		elementData = Arrays.copyOf(doubles.elementData, doubles.size);
		initialCapacity = doubles.size;
		size = doubles.size;
	}

	public static DoubleArrayList empty() {
		return new DoubleArrayList();
	}

	public static DoubleArrayList of(final double... doubles) {
		return new DoubleArrayList(doubles);
	}

	public static DoubleArrayList of(final DoubleArrayList doubles) {
		return new DoubleArrayList(doubles);
	}

	public static DoubleArrayList ofNValues(final int n, final double value) {
		final var list = new DoubleArrayList(n);
		Arrays.fill(list.elementData, value);
		list.size = n;
		return list;
	}

	public double get(final int index) {
		Objects.checkIndex(index, size);
		return uncheckedGet(index);
	}

	public double set(final int index, final double element) {
		Objects.checkIndex(index, size);
		final double oldValue = uncheckedGet(index);
		uncheckedSet(index, element);
		return oldValue;
	}

	double uncheckedGet(final int index) {
		return elementData[index];
	}

	void uncheckedSet(final int index, final double element) {
		elementData[index] = element;
	}

	public boolean add(final double e) {
		ensureCapacityForOneMore();
		elementData[size++] = e;
		return true;
	}

	public void add(final int index, final double element) {
		if (index == size) {
			add(element);
		} else {
			Objects.checkIndex(index, size + 1);
			ensureCapacityForOneMore();
			System.arraycopy(elementData, index, elementData, index + 1, size - index);
			elementData[index] = element;
			++size;
		}
	}

	public void addAll(final double... elements) {
		ensureCapacityOfAtLeast(size + elements.length);
		System.arraycopy(elements, 0, elementData, size, elements.length);
		size += elements.length;
	}

	public void addAll(final int index, final double... elements) {
		Objects.checkIndex(index, size + 1);
		if (index == size) {
			addAll(elements);
		}
		ensureCapacityOfAtLeast(size + elements.length);
		System.arraycopy(elementData, index, elementData, index + elements.length, size - index);
		System.arraycopy(elements, 0, elementData, index, elements.length);
		size += elements.length;
	}

	public void addAll(final DoubleArrayList other) {
		addAll(other.toArray());
		// TODO: inefficient, as it copies once too often, but ok for the moment
	}

	public void addAll(final int index, final DoubleArrayList other) {
		addAll(index, other.toArray());
		// TODO: inefficient, as it copies once too often, but ok for the moment
	}

	public double removeLast() {
		if (size == 0) { throw new ArrayIndexOutOfBoundsException("Cannot remove from empty list"); }
		return elementData[--size];
	}

	public double remove(final int index) {
		if (index == size - 1) { return removeLast(); }
		if (size == 0) { throw new ArrayIndexOutOfBoundsException("Cannot remove from empty list"); }
		Objects.checkIndex(index, size);
		final double removed = elementData[index];
		System.arraycopy(elementData, index + 1, elementData, index, --size - index);
		return removed;
	}

	public void clear() {
		elementData = new double[initialCapacity];
		size = 0;
	}

	public boolean contains(final double b) {
		return anySatisfy(candidate -> candidate == b);
	}

	/** Same as {@link #select(DoublePredicate)}, but for those who are used to java.util lingo */
	public DoubleArrayList filter(final DoublePredicate predicate) {
		return select(predicate);
	}

	/**
	 * Returns a new DoubleArrayList with all of the elements in the DoubleArrayList that return true for the specified predicate.
	 */
	public DoubleArrayList select(final DoublePredicate predicate) {
		final var result = new DoubleArrayList(size());
		if (isEmpty()) { return result; }
		indexStream().filter(index -> predicate.test(uncheckedGet(index)))//
				.forEach(index -> result.add(uncheckedGet(index)));
		// there's probably a more efficient way, by making bulk-adds, but this should be sufficient for a start
		return result;
	}

	/**
	 * @returns a new {@link List} formed from {@link DoubleArrayList} and another {@link List} by combining corresponding elements in pairs.
	 *          If one of the two Lists is longer than the other, its remaining elements are ignored.
	 */
	public <T> List<DoubleObjectPair<T>> zip(final List<T> other) {
		final var otherIndex = new AtomicInteger(-1);
		return indexStream()//
				.limit(min(size(), other.size())) //
				.mapToObj(index -> DoubleObjectPair.of(uncheckedGet(index), other.get(otherIndex.incrementAndGet())))//
				.collect(toList());
	}

	/**
	 * Returns a new DoubleArrayList with all of the elements in the DoubleArrayList that return false for the specified predicate.
	 */
	public DoubleArrayList reject(final DoublePredicate predicate) {
		return select(predicate.negate());
	}

	public void forEach(final DoubleConsumer procedure) {
		indexStream().forEach(index -> procedure.accept(uncheckedGet(index)));
	}

	public DoubleStream stream() {
		return indexStream().mapToDouble(index -> uncheckedGet(index));
	}

	public DoubleStream map(final DoubleUnaryOperator function) {
		return stream().map(function::applyAsDouble);
	}

	public IntStream mapToInt(final DoubleToIntFunction function) {
		return indexStream().map(index -> function.applyAsInt(uncheckedGet(index)));
	}

	public <T> Stream<T> mapToObj(final DoubleFunction<? extends T> function) {
		return indexStream().mapToObj(index -> function.apply(uncheckedGet(index)));
	}

	public boolean anySatisfy(final DoublePredicate predicate) {
		if (isEmpty()) { return false; }
		return indexStream().anyMatch(index -> predicate.test(uncheckedGet(index)));//
	}

	public boolean allSatisfy(final DoublePredicate predicate) {
		if (isEmpty()) { return false; }
		return indexStream().allMatch(index -> predicate.test(uncheckedGet(index)));//
	}

	public boolean noneSatisfy(final DoublePredicate predicate) {
		if (isEmpty()) { return true; }
		return indexStream().noneMatch(index -> predicate.test(uncheckedGet(index)));//
	}

	public double sum() {
		double sum = 0;
		for (int t = 0; t < size; ++t) {
			sum += elementData[t];
		}
		return sum;
	}

	public double average() {
		if (isEmpty()) { throw new ArithmeticException(); }
		return sum() / size;
	}

	public OptionalDouble optionalAverage() {
		return isEmpty() ? OptionalDouble.empty() : OptionalDouble.of(sum() / size);
	}

	public int size() {
		return size;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + size;
		result = prime * result + Utils.hashCodeOfRegion(elementData, 0, size);
		return result;
	}

	@SuppressFBWarnings(value = "EQ_GETCLASS_AND_CLASS_CONSTANT", justification = "Just incorporate Sublist and nothing else")
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (obj.getClass() != DoubleArrayList.class && obj.getClass() != DoubleSubList.class) { return false; }
		final DoubleArrayList other = (DoubleArrayList) obj;
		if (!equalsRange(other, 0, size)) { return false; }
		return true;
	}

	public double[] toArray() {
		return Arrays.copyOfRange(elementData, 0, size);
	}

	public void sort() {
		Arrays.sort(elementData, 0, size);
	}

	@Override
	public String toString() {
		final var s = mapToObj(Double::toString).collect(Collectors.joining(","));
		return "DoubleArrayList [doubles=" + s + ", size=" + size + "]";
	}

	public DoubleArrayList subList(final int fromIndex, final int toIndex) {
		Objects.checkFromToIndex(fromIndex, toIndex, size);
		return new DoubleSubList(this, fromIndex, toIndex);
	}

	IntStream indexStream() {
		return IntStream.range(0, size);
	}

	boolean equalsRange(final DoubleArrayList other, final int from, final int to) {
		if (other instanceof DoubleSubList) { return equalsRange((DoubleSubList) other, from, to); }
		if (other.size != to - from) { return false; }
		Objects.checkFromToIndex(from, to, size);
		return Arrays.equals(elementData, from, to, other.elementData, 0, to - from);
	}

	boolean equalsRange(final DoubleSubList other, final int from, final int to) {
		if (other.size != to - from) { return false; }
		Objects.checkFromToIndex(from, to, size);
		return Arrays.equals(elementData, from, to, other.backingList.elementData, other.offset, other.offset + to - from);
	}

	void ensureCapacityForOneMore() {
		ensureCapacityOfAtLeast(size + 1);
	}

	void ensureCapacityOfAtLeast(final int minCapacity) {
		if (minCapacity > elementData.length) {
			final int oldCapacity = elementData.length;
			final var minGrowth = minCapacity - oldCapacity;
			final var preferredGrowth = oldCapacity >> 1;
			final int newCapacity = newCapacity(oldCapacity, minGrowth, preferredGrowth);
			// System.out.println("capacity: " +oldCapacity+" -> "+ newCapacity + " = " + String.format("%.2f %%", (double) newCapacity / (double)
			// Integer.MAX_VALUE * 100.0));
			elementData = Arrays.copyOf(elementData, newCapacity);
		}
	}

	private static int newCapacity(final int currentCapacity, final int minGrowth, final int preferredGrowth) {
		int shifts = 0;
		do {
			try {
				return StrictMath.addExact(max(minGrowth, preferredGrowth >> shifts), currentCapacity);
			} catch (final ArithmeticException e1) {
				shifts += 1;
			}
		} while (shifts <= 2);
		if (currentCapacity >= MAX_ARRAY_LENGTH) { throw new OutOfMemoryError("Array list full."); }
		return MAX_ARRAY_LENGTH;
	}

	// attention inheritance will inherit null array. Any get/set/add/remove/grow must be redirected to backing list.
	@SuppressFBWarnings(value = "EQ_DOESNT_OVERRIDE_EQUALS", //
			justification = "The superclass equals method is tailored to work its own class and this class only")
	private static final class DoubleSubList extends DoubleArrayList {

		private final DoubleArrayList backingList;

		private final DoubleSubList parent;

		private final int offset;

		@SuppressWarnings("unused")
		@Deprecated
		private double[] elementData; // shadowing is intentional

		/** SubList backed by DoubleArrayList. */
		private DoubleSubList(final DoubleArrayList backingList, final int fromIndex, final int toIndex) {
			this.backingList = backingList;
			parent = null;
			offset = fromIndex;
			size = toIndex - fromIndex;
		}

		/** SubList backed by another Sublist. */
		private DoubleSubList(final DoubleSubList parent, final int fromIndex, final int toIndex) {
			backingList = parent.backingList;
			this.parent = parent;
			offset = parent.offset + fromIndex;
			size = toIndex - fromIndex;
		}

		@Override
		public double get(final int index) {
			Objects.checkIndex(index, size);
			return uncheckedGet(offset + index);
		}

		@Override
		public double set(final int index, final double element) {
			Objects.checkIndex(index, size);
			final double oldValue = uncheckedGet(offset + index);
			uncheckedSet(offset + index, element);
			return oldValue;
		}

		@Override
		double uncheckedGet(final int index) {
			return backingList.elementData[index];
		}

		@Override
		void uncheckedSet(final int index, final double element) {
			backingList.elementData[index] = element;
		}

		@Override
		public boolean add(final double element) {
			add(size, element);
			return true;
		}

		@Override
		public void add(final int index, final double element) {
			Objects.checkIndex(index, size + 1);
			backingList.add(offset + index, element);
			updateSizeCount(1);
		}

		@Override
		public void addAll(final double... elements) {
			backingList.addAll(offset + size, elements);
			updateSizeCount(elements.length);
		}

		@Override
		public void addAll(final int index, final double... elements) {
			Objects.checkIndex(index, size + 1);
			backingList.addAll(offset + index, elements);
			updateSizeCount(elements.length);
		}

		@Override
		public double removeLast() {
			return remove(size - 1);
		}

		@Override
		public double remove(final int index) {
			Objects.checkIndex(index, size);
			final double result = backingList.remove(offset + index);
			updateSizeCount(-1);
			return result;
		}

		@Override
		public double[] toArray() {
			return Arrays.copyOfRange(backingList.elementData, offset, offset + size);
		}

		@Override
		public void sort() {
			Arrays.sort(backingList.elementData, offset, offset + size);
		}

		@Override
		public DoubleArrayList subList(final int fromIndex, final int toIndex) {
			Objects.checkFromToIndex(fromIndex, toIndex, size);
			return new DoubleSubList(this, fromIndex, toIndex);
		}

		@SuppressWarnings("checkstyle:EqualsHashCode") // equals can be inherited in this special case
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + size;
			result = prime * result + Utils.hashCodeOfRegion(backingList.elementData, offset, offset + size);
			return result;
		}

		@Override
		IntStream indexStream() {
			return IntStream.range(offset, offset + size);
		}

		@Override
		boolean equalsRange(final DoubleArrayList other, final int from, final int to) {
			if (other instanceof DoubleSubList) { return equalsRange((DoubleSubList) other, from, to); }
			if (other.size != to - from) { return false; }
			Objects.checkFromToIndex(from, to, size);
			return Arrays.equals(backingList.elementData, from + offset, to + offset, other.elementData, 0, to - from);
		}

		@Override
		boolean equalsRange(final DoubleSubList other, final int from, final int to) {
			if (other.size != to - from) { return false; }
			Objects.checkFromToIndex(from, to, size);
			return Arrays.equals(backingList.elementData, from + offset, to + offset, //
					other.backingList.elementData, other.offset, other.offset + to - from);
		}

		private void updateSizeCount(final int sizeChange) {
			DoubleSubList subList = this;
			do {
				subList.size += sizeChange;
				subList = subList.parent;
			} while (subList != null);
		}

	}

}
