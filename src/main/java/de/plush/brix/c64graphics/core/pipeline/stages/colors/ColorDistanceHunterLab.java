package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces.ColorConversions.*;
import static java.lang.Math.min;

import java.awt.Color;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces.ColorHunterLab;

/**
 * Color distance according to the
 * <a href=
 * "https://support.hunterlab.com/hc/en-us/articles/203023559-Brief-Explanation-of-delta-E-or-delta-E-">Hunter Lab ∆E formula</a>.
 * In 1948, R. S. Hunter proposed a color space more uniform in the perception, and the formula for ∆E
 * and the values readable directly from a photoelectric colorimeter. The formula evolved in the 1950s
 * and 1960s, assuming the current shape in 1966. CIEXYZ values were transformed into Hunter L, a, b
 * coordinates with the following arrangements:
 * <ul>
 * <li>L coordinate for representing brightness following in 0≤ L ≤ 100, L=0 for black, L=100 for a
 * perfectly reflecting diffuser;
 * <li>A coordinate for representing colors on the red-green axis, with positive values for red, and negative for green;
 * <li>B coordinate for representing colors on the blue-yellow color axis, with positive values for yellow,
 * and negative for blue.
 * </ul>
 * 
 * Note that the Hunter Lab color difference formula is basically the same as the CIE76 and DIN99 color difference formula, but operating on a differently
 * formed Lab color space.
 * 
 * @see <a href="https://wisotop.de/assets/2017/DeltaE-%20Survey-2.pdf">https://wisotop.de/assets/2017/DeltaE-%20Survey-2.pdf (4.4. The Hunter color system
 *      and the ∆EH formula)</a>
 */
class ColorDistanceHunterLab implements ColorDistanceMeasure {

	@Override
	public double distanceOf(final Color rgb1, final Color rgb2) {
		// return rgb1.equals(rgb2) ? 0.0 : distanceOf(convertXYZtoHunterLab(convertRGBtoXYZ(rgb1)), convertXYZtoHunterLab(convertRGBtoXYZ(rgb2)));
		return rgb1.equals(rgb2) ? 0.0 : min(1, distanceOf(convertXYZtoHunterLab(convertRGBtoXYZ(rgb1)), convertXYZtoHunterLab(convertRGBtoXYZ(rgb2))) / 145);
	}

	/**
	 * Compares two L*a*b colors and returns the degree of their similarity. The lower the result the more similar are the colors.
	 *
	 * Taken from <a href="https://www.lovibond.com/en/PC/Colour-Values/Delta-Hunter-Lab-colour-difference/Delta-Hunter-Lab-colour-difference">Hunter Lab colour
	 * difference</a>
	 *
	 * @param lab1
	 *            First color represented in L*a*b color space.
	 * @param lab2
	 *            Second color represented in L*a*b color space.
	 * @return The degree of similarity between the two input colors according to the Hunter Lab color-difference formula.
	 */
	private static double distanceOf(final ColorHunterLab Hlab1, final ColorHunterLab Hlab2) {
		return Math.sqrt(Math.pow(Hlab2.L() - Hlab1.L(), 2) + Math.pow(Hlab2.a() - Hlab1.a(), 2) + Math.pow(Hlab2.b() - Hlab1.b(), 2));
	}
}
