package de.plush.brix.c64graphics.core.model;

import static de.plush.brix.c64graphics.core.model.Direction.Horizontal.*;
import static de.plush.brix.c64graphics.core.model.Direction.Vertical.*;

public interface IRollBlockwise {

	void rollLeftBlockwise(int n);

	void rollRightBlockwise(int n);

	void rollUpBlockwise(int n);

	void rollDownBlockwise(int n);

	default void rollBlockwise(final Displacement displacement) {
		if (Up.equals(displacement.direction)) {
			rollUpBlockwise(displacement.value);
		} else if (Down.equals(displacement.direction)) {
			rollDownBlockwise(displacement.value);
		} else if (Left.equals(displacement.direction)) {
			rollLeftBlockwise(displacement.value);
		} else if (Right.equals(displacement.direction)) {
			rollRightBlockwise(displacement.value);
		} else {
			throw new IllegalArgumentException("Unsupported direction: " + displacement.direction);
		}

	}
}
