package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import static java.lang.Byte.toUnsignedInt;
import static java.lang.Math.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.ColorDistanceMeasure;
import de.plush.brix.c64graphics.core.util.collections.IntHashBag;

/**
 * Color Quantization based on the median-cut algorithm.
 * <p>
 * Based upon the LightZone sources.
 * Added a feature to inject own color distance measures.
 * </p>
 * 
 * @author Wanja Gayk
 * 
 * @see
 *      <a href=
 *      "https://github.com/AntonKast/LightZone/blob/master/lightcrafts/extsrc/com/lightcrafts/media/jai/opimage/MedianCutOpImage.java">MedianCutOpImage.java
 *      at LightZone</a>
 * 
 * 
 */
public class MedianCutQuantizeImpl implements Function<BufferedImage, BufferedImage> {

	private final int maxColorNum;

	private final Shape roi;

	private final int xPeriod;

	private final int yPeriod;

	/** The size of the histogram. */
	private final int histogramSize;

	private final ColorDistanceMeasure colorDistanceMeasure;

	/**
	 * Constructs an <code>MedianCutOpImage</code>.
	 */
	public MedianCutQuantizeImpl(final ColorDistanceMeasure colorDistanceMeasure, final int maxColorNum, final Integer upperBound, final Shape roi,
			final int xSampleRate, final int ySampleRate) {
		this.colorDistanceMeasure = colorDistanceMeasure;
		this.maxColorNum = maxColorNum;
		histogramSize = upperBound == null ? 2 << 15 : upperBound.intValue(); // 15 bits
		this.roi = roi;
		xPeriod = xSampleRate;
		yPeriod = ySampleRate;
	}

	@Override
	public BufferedImage apply(final BufferedImage originalImage) {
		final MedianCut medianCut = new MedianCut(originalImage);
		final var colorSet = medianCut.colors();
		final var result = new BufferedImage(originalImage.getWidth(), originalImage.getHeight(), originalImage.getType());
		for (int y = 0; y < result.getHeight(); ++y) {
			for (int x = 0; x < result.getWidth(); ++x) {
				final var sourceColor = new Color(originalImage.getRGB(x, y));
				final Color closestColor = Arrays.stream(colorSet).min(colorDistanceMeasure.comparingDistanceTo(sourceColor)).get();
				result.setRGB(x, y, closestColor.getRGB());
			}
		}
		return result;
	}

	private class MedianCut {

		// /** The counts of the colors. */
		// private int[] counts;
		//
		// /** The colors for the color histogram. */
		// private int[] colors;

		/** The partition of the RGB color space. Each cube contains one cluster. */
		private Cube[] cubes;

		/** The maximum number of bits to have 32768 colors. */
		private static final int bits = 8;

		/** The mask to generate the low bits colors from the original colors. */
		private int mask;

		private byte[][] colormap;

		MedianCut(final BufferedImage originalImage) {
			mask = 255 << 8 - bits & 255;
			mask = mask | mask << 8 | mask << 16;

			final IntHashBag histogram = computeHistogram(originalImage);
			medianCut(histogram);
		}

		private IntHashBag computeHistogram(final BufferedImage originalImage) {
			final Rectangle imageBounds = new Rectangle(originalImage.getWidth(), originalImage.getHeight());
			var roi = MedianCutQuantizeImpl.this.roi;
			if (roi == null) {
				roi = imageBounds;
			}
			// Process if and only if within ROI bounds.
			if (roi.intersects(imageBounds)) {
				final var intersection = imageBounds.intersection(roi.getBounds());
				return _computeHistogram(originalImage, intersection);
			}
			throw new IllegalArgumentException("Region of interest " + roi + " does not intersect with image bounds " + imageBounds);
		}

		private IntHashBag _computeHistogram(final BufferedImage originalImage, final Shape roi) {
			final var histogram = new IntHashBag(histogramSize);
			final var rect = roi.getBounds();
			for (int y = rect.y; y < rect.height; y += yPeriod) {
				for (int x = rect.x; x < rect.width; x += xPeriod) {
					if (roi.contains(x, y)) {
						histogram.add(originalImage.getRGB(x, y) & mask);
					}
				}
			}
			return histogram;
		}

		/**
		 * Applies the Heckbert's median-cut algorithm to partition the color
		 * space into <code>maxcubes</code> cubes. The centroids
		 * of each cube are are used to create a color table.
		 * 
		 * @param histogram
		 */
		@SuppressWarnings({ "checkstyle:cyclomaticComplexity", "checkstyle:NPathComplexity" }) // keeping it close to the original code
		public void medianCut(final IntHashBag histogram) {
			final int[] counts = new int[histogram.sizeDistinct()];
			final int[] colors = new int[histogram.sizeDistinct()];
			final int[] index = new int[1];
			histogram.occurrences().forEach(ci -> { colors[index[0]] = ci.intValue; counts[index[0]++] = (int) ci.count; });

			// Creates the first color cube
			cubes = new Cube[maxColorNum];
			int numCubes = 0;
			Cube cube = new Cube(colors);
			for (int color = 0; color < counts.length; ++color) {
				cube.count = cube.count + counts[color];
			}

			cube.lower = 0;
			cube.upper = counts.length - 1;
			cube.level = 0;
			cube.shrink();
			cubes[numCubes++] = cube;

			// Partition the cubes until the expected number of cubes are reached, or
			// cannot further partition
			while (numCubes < maxColorNum) {
				// Search the list of cubes for next cube to split, the lowest level cube
				int level = 255;
				int splitableCube = -1;

				for (int c = 0; c < numCubes; ++c) {
					if (cubes[c].lower != cubes[c].upper && cubes[c].level < level) {
						level = cubes[c].level;
						splitableCube = c;
					}
				}

				// no more cubes to split
				if (splitableCube == -1) {
					break;
				}

				// Find longest dimension of this cube: 0 - red, 1 - green, 2 - blue
				cube = cubes[splitableCube];
				level = cube.level;
				// Cube color channel dimensions weighted by luminosities
				final int lr = 77 * (cube.rmax - cube.rmin);
				final int lg = 150 * (cube.gmax - cube.gmin);
				final int lb = 29 * (cube.bmax - cube.bmin);

				int colorChannelMask = 0;
				// note: using "if" instead of "if/else" makes blue dominant, in case all are equal.
				if (lr >= lg && lr >= lb) { // if red dimension is dominant, sort cube colors by reds
					colorChannelMask = 0xFF0000;
				}
				if (lg >= lr && lg >= lb) { // if green dimension is dominant, sort cube colors by greens
					colorChannelMask = 0xFF00;
				}
				if (lb >= lr && lb >= lg) { // if blue dimension is dominant, sort cube colors by blues
					colorChannelMask = 0xFF;
				}
				quickSort(colors, counts, cube.lower, cube.upper, colorChannelMask);

				// Find median of the colors (sorted by weighted largest dimension)
				int count = 0;
				int median = cube.lower;
				for (; count < cube.count / 2 && median <= cube.upper - 1; ++median) {
					count = count + counts[median];
				}

				// Now split "cube" at the median and add the two new
				// cubes to the list of cubes.
				final var cubeA = new Cube(colors);
				cubeA.lower = cube.lower;
				cubeA.upper = median - 1;
				cubeA.count = count;
				cubeA.level = cube.level + 1;
				cubeA.shrink();
				cubes[splitableCube] = cubeA; // add in old slot

				final var cubeB = new Cube(colors);
				cubeB.lower = median;
				cubeB.upper = cube.upper;
				cubeB.count = cube.count - count;
				cubeB.level = cube.level + 1;
				cubeB.shrink();
				cubes[numCubes++] = cubeB; // add in new slot */
			}

			// creates the lookup table and the inverse mapping
			buildColorMap(numCubes, counts, colors);
		}

		/** Creates the lookup table and computes the inverse mapping. */
		private void buildColorMap(final int ncubes, final int[] counts, final int[] colors) {
			final float scale = 255.0f / (mask & 255);

			colormap = new byte[3][ncubes];
			for (int c = 0; c < ncubes; ++c) {
				final Cube cube = cubes[c];
				float rsum = 0.0f, gsum = 0.0f, bsum = 0.0f;

				for (int col = cube.lower; col <= cube.upper; ++col) {
					final Color color = new Color(colors[col]);
					rsum += color.getRed() * (float) counts[col];
					gsum += color.getGreen() * (float) counts[col];
					bsum += color.getBlue() * (float) counts[col];
				}

				// Create the color map
				colormap[0][c] = (byte) (rsum / cube.count * scale);
				colormap[1][c] = (byte) (gsum / cube.count * scale);
				colormap[2][c] = (byte) (bsum / cube.count * scale);
			}
		}

		Color[] colors() {
			return IntStream.range(0, colormap[0].length)//
					.mapToObj(index -> new Color(toUnsignedInt(colormap[0][index]), toUnsignedInt(colormap[1][index]), toUnsignedInt(colormap[2][index])))//
					.toArray(Color[]::new);
		}

	}

	private static void quickSort(final int[] colors, final int[] counts, final int start, final int end, final int colorChannelMask) {
		// Based on the QuickSort method by James Gosling from Sun's SortDemo applet
		int lo = start;
		int hi = end;
		int mid, t;

		if (end > start) {
			// note: eclipse will remove brackets on "(start+end)>>>1" and mess up the maths.
			mid = colors[start + (end - start) / 2] & colorChannelMask; // See: https://duckduckgo.com/?q=nearly+all+binary+searchs+are+broken
			while (lo <= hi) {
				while (lo < end && (colors[lo] & colorChannelMask) < mid) {
					++lo;
				}
				while (hi > start && (colors[hi] & colorChannelMask) > mid) {
					--hi;
				}
				if (lo <= hi) {
					t = colors[lo];
					colors[lo] = colors[hi];
					colors[hi] = t;

					t = counts[lo];
					counts[lo] = counts[hi];
					counts[hi] = t;

					++lo;
					--hi;
				}
			}
			if (start < hi) {
				quickSort(colors, counts, start, hi, colorChannelMask);
			}
			if (lo < end) {
				quickSort(colors, counts, lo, end, colorChannelMask);
			}
		}
	}

	private static class Cube { // structure for a cube in color space

		private final int[] colors; // contains all of the histogram's colors

		int lower; // one corner's index in histogram

		int upper; // another corner's index in histogram

		int count; // cube's histogram count

		int level; // cube's level

		int rmin, rmax;

		int gmin, gmax;

		int bmin, bmax;

		public Cube(final int[] colors) {
			this.colors = colors;
		}

		/**
		 * Shrinks the provided <code>Cube</code> to a smallest contains
		 * the same colors defined in the histogram.
		 */
		void shrink() {
			rmin = 255;
			gmin = 255;
			bmin = 255;
			rmax = 0;
			gmax = 0;
			bmax = 0;
			for (int x = lower; x <= upper; ++x) {
				final var color = new Color(colors[x]);
				rmax = max(color.getRed(), rmax);
				rmin = min(color.getRed(), rmin);
				gmax = max(color.getGreen(), gmax);
				gmin = min(color.getGreen(), gmin);
				bmax = max(color.getBlue(), bmax);
				bmin = min(color.getBlue(), bmin);
			}
		}
	}

}