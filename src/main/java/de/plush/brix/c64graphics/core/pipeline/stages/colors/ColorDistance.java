package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import java.awt.Color;
import java.util.Arrays;

/**
 * Enumeration of different color distance measures.
 */
public enum ColorDistance implements ColorDistanceMeasure {
	/**
	 * Compares colors to their simple Euclidean distance in the RGB color space..<br>
	 * This is a fast measure, that does not take color perception into account.
	 */
	EUCLIDEAN(new ColorDistanceEuclidean()), //
	/**
	 * Weighted color distance based on a weighted Euclidean metric in the RGB color space to take color perception into account. <br>
	 * Measures color distance according to this paper:<br>
	 * <A href="https://www.compuphase.com/cmetric.htm">https://www.compuphase.com/cmetric.htm</a><br>
	 * <p>
	 */
	EUCLIDEAN_WEIGHTED(new ColorDistanceEuclideanWeighted()), //
	/**
	 * In 1948, R. S. Hunter proposed a color space more uniform in the perception, and the formula for ∆E
	 * and the values readable directly from a photoelectric colorimeter. The formula evolved in the 1950s
	 * and 1960s, assuming the current shape in 1966. CIEXYZ values were transformed into Hunter L, a, b
	 * coordinates.
	 * 
	 * @see <a href="https://wisotop.de/assets/2017/DeltaE-%20Survey-2.pdf">https://wisotop.de/assets/2017/DeltaE-%20Survey-2.pdf</a>
	 */
	HUNTER_Lab(new ColorDistanceHunterLab()),
	/**
	 * Color distance according to <a href="https://en.wikipedia.org/wiki/Color_difference#CIE76">CIE76</a>.
	 * The 1976 formula is the first formula that related a measured color difference to a known set of CIELAB coordinates. This formula has been succeeded by
	 * the 1994 and 2000 formulas because the CIELAB space turned out to be not as perceptually uniform as intended, especially in the saturated regions. This
	 * means that this formula rates these colors too highly as opposed to other colors.
	 */
	CIE79(new ColorDistanceCIE76()), //
	/**
	 * Color distance according to <a href="https://en.wikipedia.org/wiki/Color_difference#CIE94">CIE94</a>.
	 * The 1976 definition was extended to address perceptual non-uniformities, while retaining the CIELAB color space, by the introduction of
	 * application-specific weights derived from an automotive paint test's tolerance data.
	 */
	CIE94_GraphicsArts(new ColorDistanceCIE94(ColorDistanceCIE94.Application.GraphicsArts)), //
	/**
	 * Color distance according to <a href="https://en.wikipedia.org/wiki/Color_difference#CIE94">CIE94</a>.
	 * The 1976 definition was extended to address perceptual non-uniformities, while retaining the CIELAB color space.
	 * This comparison uses application-specific weights specific to Textiles.
	 */
	CIE94_Textiles(new ColorDistanceCIE94(ColorDistanceCIE94.Application.Textiles)), //
	/**
	 * CMC l:c. A quasimetric with weights on lightness (l) and chroma (c), equal weights.<br>
	 * 
	 * @implNote
	 *           Note that this is a quasimetric, i.e. it violates symmetry. Therefore, distanceOf(a, b) may not equal distanceOf(b, a), contrary to the
	 *           contract
	 *           of the {@link ColorDistanceMeasure} interface. Handle with care.
	 */
	CMC_lc_11(new ColorDistanceCMC_lc(1, 1)),
	/**
	 * CMC l:c. A quasimetric with weights on lightness (l) and chroma (c), weighted towards luma.<br>
	 * 
	 * @implNote
	 *           Note that this is a quasimetric, i.e. it violates symmetry. Therefore, distanceOf(a, b) may not equal distanceOf(b, a), contrary to the
	 *           contract
	 *           of the {@link ColorDistanceMeasure} interface. Handle with care.
	 */
	CMC_lc_21(new ColorDistanceCMC_lc(2, 1)), //
	/** Color difference according to the <a href="https://en.wikipedia.org/wiki/Color_difference#CIEDE2000">CIEDE 2000 standard</a>. */
	CIEDE_2000(new ColorDistanceCIEDE2000()), //
	/**
	 * Color difference according to the <a href="https://en.wikipedia.org/wiki/Color_difference#CIEDE2000">CIEDE 2000 standard</a>.
	 * Uses a fastAtan2 algorithm that is quicker, but not as accurate. However, it gives slighty different results that
	 * might be more agreeable.
	 */
	CIEDE_2000_FAST(new ColorDistanceCIEDE2000().fast()), //
	/**
	 * Color difference according to the DIN99b standard. Note that the DIN99 color space has different iterations of the standard, from DIN99 to DIN99b, ...,
	 * DIN99o.
	 * 
	 * @see <a href = "http://juliagraphics.github.io/Colors.jl/stable/colordifferences/">http://juliagraphics.github.io/Colors.jl/stable/colordifferences/</a>
	 */
	DIN99b(new ColorDistanceDIN99b()), //
	/**
	 * Color difference according to the DIN99o standard. Note that the DIN99 color space has different iterations of the standard, from DIN99 to DIN99b, ...,
	 * DIN99o.
	 * 
	 * @see <a href = "http://juliagraphics.github.io/Colors.jl/stable/colordifferences/">http://juliagraphics.github.io/Colors.jl/stable/colordifferences/</a>
	 */
	DIN99o(new ColorDistanceDIN99o()), //
	;

	private final ColorDistanceMeasure measure;

	ColorDistance(final ColorDistanceMeasure measure) {
		this.measure = measure;
	}

	@Override
	public double distanceOf(final Color a, final Color b) {
		return measure.distanceOf(a, b);
	}

	public static String name(final ColorDistanceMeasure cm) {
		return Arrays.stream(values()).filter(v -> v == cm).findFirst()//
				.map(v -> ColorDistance.class.getSimpleName() + "." + v.name())//
				.orElse(cm.getClass().getName());
	}

}
