/**
 * This package contains classes dealing with color science and C64-palettes.
 * 
 * @see <a href = "https://wisotop.de/assets/2017/DeltaE-%20Survey-2.pdf">https://wisotop.de/assets/2017/DeltaE-%20Survey-2.pdf</a>
 * @see <a href ="https://www.pepto.de/projects/colorvic/">https://www.pepto.de/projects/colorvic/</a>
 * @see <a href = "https://lospec.com/palette-list/colodore">https://lospec.com/palette-list/colodore</a>
 */
package de.plush.brix.c64graphics.core.pipeline.stages.colors;