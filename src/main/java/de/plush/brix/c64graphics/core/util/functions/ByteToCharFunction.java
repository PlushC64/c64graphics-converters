package de.plush.brix.c64graphics.core.util.functions;

@FunctionalInterface
public interface ByteToCharFunction {
	char apply(byte b);
}
