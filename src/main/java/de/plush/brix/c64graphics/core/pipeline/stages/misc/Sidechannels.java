package de.plush.brix.c64graphics.core.pipeline.stages.misc;

import java.util.function.Function;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class Sidechannels<T> implements IPipelineStage<T, T> {

	private final Function<? super T, ?>[] channels;

	/**
	 * Executes the listed Pipeline-stages/functions in the given order but will not use their result.
	 * Returns the original object.
	 * This is useful for running differnt pipelines on the same data without
	 * post-processing, e.g. extracting different parts of data and writing them to disc separately.
	 */
	@SafeVarargs
	public Sidechannels(final Function<? super T, ?>... channels) {
		this.channels = channels;
	}

	@Override
	public T apply(final T original) {
		for (final var channel : channels) {
			channel.apply(original);
		}
		return original;
	}

}
