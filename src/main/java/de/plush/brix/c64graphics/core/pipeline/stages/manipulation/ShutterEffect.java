/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class ShutterEffect implements IPipelineStage<BufferedImage, BufferedImage> {

	private final Supplier<Color> backgroundSupplier;

	public ShutterEffect(final Supplier<Color> backgroundSupplier) {
		this.backgroundSupplier = backgroundSupplier;
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		Color background = backgroundSupplier.get();
		for (int y = 0; y < result.getHeight(); y += 2) {
			for (int x = 0; x < result.getWidth(); ++x) {
				result.setRGB(x, y, original.getRGB(x, y));
				result.setRGB(x, y + 1, background.getRGB());
			}
		}
		return result;
	}
}