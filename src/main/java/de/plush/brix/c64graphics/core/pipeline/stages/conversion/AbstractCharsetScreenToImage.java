package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

abstract class AbstractCharsetScreenToImage implements IPipelineStage<C64CharsetScreen, BufferedImage> {

	private final C64ColorRam colorRam;

	public AbstractCharsetScreenToImage(final C64ColorRam colorRAM) {
		colorRam = colorRAM;
	}

	protected abstract IPipelineStage<C64CharsetScreenColorRam, BufferedImage> createC64CharsetScreenColorRamToImage();

	@Override
	public BufferedImage apply(final C64CharsetScreen input) {
		return createC64CharsetScreenColorRamToImage().apply(new C64CharsetScreenColorRam(input, colorRam));
	}
}
