package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;
import static de.plush.brix.c64graphics.core.util.UtilsImage.*;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.stream.*;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.*;

public class ImageToC64BitmapmodePictureHiresFli implements IPipelineStage<BufferedImage, C64BitmapmodePictureHiresFli> {

	private final IHiresFLIColorClashResolver colorClashResolver;

	private final Palette palette;

	private final List<Color>[] preferredBackgroundColors = IntStream.range(0, 8).mapToObj(n -> Collections.emptyList()).toArray(List[]::new);

	private final List<Color>[] preferredForegroundColors = IntStream.range(0, 8).mapToObj(n -> Collections.emptyList()).toArray(List[]::new);

	public ImageToC64BitmapmodePictureHiresFli(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure) {
		this(palette, new HiresFLIColorClashResolver(colorDistanceMeasure));
	}

	public ImageToC64BitmapmodePictureHiresFli(final Palette palette, final IHiresFLIColorClashResolver colorClashResolver) {
		this.palette = palette;
		this.colorClashResolver = colorClashResolver;
	}

	public ImageToC64BitmapmodePictureHiresFli withPreferredBackgroundColors(final int screenNumber, final Color... colors) {
		preferredBackgroundColors[screenNumber] = asList(colors);
		preferredBackgroundColors[screenNumber].forEach(palette::colorCodeOrException);
		checkSettingsConsistency();
		return this;
	}

	public ImageToC64BitmapmodePictureHiresFli withPreferredForegroundColors(final int screenNumber, final Color... colors) {
		preferredForegroundColors[screenNumber] = asList(colors);
		preferredForegroundColors[screenNumber].forEach(palette::colorCodeOrException);
		checkSettingsConsistency();
		return this;
	}

	private void checkSettingsConsistency() {
		final Set<Color> bgs = new HashSet<>();
		final Set<Color> fgs = new HashSet<>();
		for (int screenNumber = 0; screenNumber < 8; ++screenNumber) {
			bgs.addAll(preferredBackgroundColors[screenNumber]);
			fgs.addAll(preferredForegroundColors[screenNumber]);
		}
		final Set<Color> x = bgs.stream().filter(fgs::contains).collect(toSet());
		if (!x.isEmpty()) { throw new IllegalArgumentException("You can't force these colors to be both background and foreground: " + x); }
	}

	@Override
	public C64BitmapmodePictureHiresFli apply(final BufferedImage original) {
		final BufferedImage sanitized = colorClashResolver.apply(original);

		final C64Bitmap bitmap = new C64Bitmap();
		final C64Screen[] screens = IntStream.range(0, C64Char.HEIGHT_PIXELS).mapToObj(__ -> new C64Screen((byte) 0)).toArray(C64Screen[]::new);

		final Dimension blocks = C64Bitmap.sizeBlocks();
		for (int y = 0; y < blocks.height; ++y) {
			for (int x = 2; x < blocks.width; ++x) { // start with 3rd block (FLI-Bug)
				final byte[] blockBytes = new byte[C64Char.HEIGHT_PIXELS];
				for (int line = 0; line < 8; ++line) {
					final int xPix = x * C64Char.WIDTH_PIXELS;
					final int yPix = y * C64Char.HEIGHT_PIXELS + line;
					final BufferedImage lineImage = sanitized.getSubimage(xPix, yPix, C64Char.WIDTH_PIXELS, 1);

					final List<Color> colors = colors(lineImage).stream().collect(toList());
					if (colors.size() == 1) {
						colors.add(palette.color((byte) 0)); // default
					}
					colors.sort(UtilsColor.SORT_BY_BRIGHTNESS); // to get some predictable patterns
					Color background = preferredBackgroundColors[line].stream().filter(colors::contains).findFirst().orElse(null);
					Color foreground = preferredForegroundColors[line].stream().filter(colors::contains).findFirst().orElse(null);
					Stream.of(background, foreground).filter(c -> c != null).forEach(colors::remove);
					// list of colors has no preferred color left, from here we can assign colors that are not preferred by anyone to empty slots
					final Iterator<Color> colorsLeft = colors.iterator();
					background = background == null ? colorsLeft.hasNext() ? colorsLeft.next() : palette.color((byte) 0) : background;
					foreground = foreground == null ? colorsLeft.hasNext() ? colorsLeft.next() : palette.color((byte) 0) : foreground;
					Stream.of(background, foreground).forEach(colors::remove);

					final C64Char block = new ImageToC64CharHires(background, foreground).apply(repetitiveBlockImage(lineImage));
					blockBytes[line] = block.toC64Binary()[line];
					try {
						screens[line].setScreenCode(x, y, toUnsignedByte(palette.colorCodeOrException(foreground) << 4 | palette.colorCodeOrException(
								background)));
					} catch (final Exception e) {
						throw new IllegalStateException("in block (x=" + x + ", y=" + y + ", global " + new Point(xPix, yPix) + ")", e);
					}
					if (!colors.isEmpty()) {
						throw new IllegalStateException("unexpectedly found more than 2 colors in block (x=" + x + ", y=" + y + ", line=" + line
								+ "). Have you solved all color clashes?");
					}
				}
				bitmap.setBlock(x, y, new C64Char(blockBytes));
			}
		}
		return new C64BitmapmodePictureHiresFli(bitmap, screens);
	}

}
