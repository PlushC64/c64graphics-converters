package de.plush.brix.c64graphics.core.pipeline.stages.dithering;

import static de.plush.brix.c64graphics.core.util.Utils.subsetStream;
import static java.util.stream.Collectors.toMap;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.function.BinaryOperator;

import de.plush.brix.c64graphics.core.model.C64Char;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.PaletteConversion;
import de.plush.brix.c64graphics.core.util.Utils;

/**
 * A dithering that will apply a palette of native colors plus colors that can be mixed from "mixable" clusters of these colors to the target image.The
 * mix-colors will then be replaced by a simple Pattern of the originating colors.
 * 
 * See <A href = "https://kodiak64.com/blog/luma-driven-graphics-on-c64">https://kodiak64.com/blog/luma-driven-graphics-on-c64</a> for the basic idea.
 */
final class LumaPairingDither implements IPipelineStage<BufferedImage, BufferedImage> {

	private final C64Char pattern;

	private final PaletteConversion noDither;

	private final Map<Color, Color[]> unmixMap;

	/**
	 * A dithering that will apply a palette of native colors plus colors that can be mixed from "mixable" clusters of these colors to the target image.
	 * The mix-colors will then be replaced by a simple Pattern of the originating colors.</br>
	 * </br>
	 * E.g. it will apply {@link C64ColorsColodore} colors plus a mix of {@link C64ColorsColodore#BLUE} and {@link C64ColorsColodore#BROWN}, as defined ni
	 * {@link ColorClusterIndexes#VIC_II}, mixed by a {@link ColorMix#HUNTER_Lab} mixer and then replace that mix-color with alternating lines of BLUE and
	 * BROWN.</br>
	 * </br>
	 * If using a custom {@link Palette} other than the Frameworks palettes like {@link C64ColorsColodore} or {@link C64ColorsPepto}, it must implement the
	 * {@link Palette#colorClusters(ColorClusterIndexProvider)} method to return proper colors to all the provided <tt>mixableColorIndexes</tt>. The default
	 * implementation will just return an empty set of color clusters.<br>
	 * 
	 * @param palette
	 *            see: {@link C64ColorsColodore} or {@link C64ColorsPepto}
	 * @param mixableColorIndexes
	 *            see {@link ColorClusterIndexes}
	 * @param colorMixer
	 *            see {@link ColorMix}
	 * @param colorDistanceMeasure
	 *            see {@link ColorDistance}
	 */
	LumaPairingDither(final Palette palette, final ColorClusterIndexProvider mixableColorIndexes, final BinaryOperator<Color> colorMixer,
			final ColorDistanceMeasure colorDistanceMeasure) {
		this(palette, mixableColorIndexes, colorMixer, colorDistanceMeasure, //
				new C64Char("""
						11111111
						00000000
						11111111
						00000000
						11111111
						00000000
						11111111
						00000000
						"""));
	}

	/**
	 * A dithering that will apply a palette of native colors plus colors that can be mixed from "mixable" clusters of these colors to the target image.
	 * The mix-colors will then be replaced by a simple Pattern of the originating colors.</br>
	 * </br>
	 * E.g. it will apply {@link C64ColorsColodore} colors plus a mix of {@link C64ColorsColodore#BLUE} and {@link C64ColorsColodore#BROWN}, as defined ni
	 * {@link ColorClusterIndexes#VIC_II}, mixed by a {@link ColorMix#HUNTER_Lab} mixer and then replace that mix-color with alternating lines of BLUE and
	 * BROWN.</br>
	 * </br>
	 * If using a custom {@link Palette} other than the Frameworks palettes like {@link C64ColorsColodore} or {@link C64ColorsPepto}, it must implement the
	 * {@link Palette#colorClusters(ColorClusterIndexProvider)} method to return proper colors to all the provided <tt>mixableColorIndexes</tt>. The default
	 * implementation will just return an empty set of color clusters.<br>
	 * 
	 * @param palette
	 *            see: {@link C64ColorsColodore} or {@link C64ColorsPepto}
	 * @param mixableColorIndexes
	 *            see {@link ColorClusterIndexes}
	 * @param colorMixer
	 *            see {@link ColorMix}
	 * @param colorDistanceMeasure
	 *            see {@link ColorDistance}
	 * @param pattern
	 *            a pattern for the "mixed" color decomposition.
	 */
	LumaPairingDither(final Palette palette, final ColorClusterIndexProvider mixableColorIndexes, final BinaryOperator<Color> colorMixer,
			final ColorDistanceMeasure colorDistanceMeasure, final C64Char pattern) {

		final Set<Set<Color>> colorClusters = palette.colorClusters(mixableColorIndexes);
		unmixMap = colorClusters.stream()//
				.flatMap(cluster -> subsetStream(cluster, 2).map(set -> set.toArray(Color[]::new)))//
				.collect(toMap(cp -> colorMixer.apply(cp[0], cp[1]), cp -> cp));

		final var paletteColors = palette.colors();
		final var mixColors = unmixMap.keySet().toArray(Color[]::new);
		final var allColors = Utils.toArray(paletteColors, mixColors); // evauate beforehand

		final Palette combinedPalette = () -> allColors; // use constant in lambda function to not recreate it everytime

		noDither = new PaletteConversion(combinedPalette, colorDistanceMeasure, Dithering.None);
		this.pattern = pattern;
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final var imageWithMixColors = noDither.apply(original);
		for (int y = 0; y < imageWithMixColors.getHeight(); ++y) {
			final int tileY = y % 8;
			for (int x = 0; x < imageWithMixColors.getWidth(); ++x) {
				final int tileX = x % 8;
				final Color[] unmix = unmixMap.get(new Color(imageWithMixColors.getRGB(x, y)));
				if (unmix != null) {
					final Color c = pattern.isPixelSet(tileX, tileY) ? unmix[0] : unmix[1];
					imageWithMixColors.setRGB(x, y, c.getRGB());
				}
			}
		}
		return imageWithMixColors;
	}

}