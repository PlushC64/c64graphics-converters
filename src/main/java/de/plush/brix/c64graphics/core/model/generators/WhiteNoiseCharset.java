package de.plush.brix.c64graphics.core.model.generators;

import static de.plush.brix.c64graphics.core.util.Utils.deepClone;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.stream.*;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.ImageToC64CharHires;

public class WhiteNoiseCharset {

	private final List<Point> allPoints;
	private final int steps;
	private final Random random;
	private final C64Charset charset;

	public WhiteNoiseCharset() {
		this(new C64Charset(), 0xFF, new Random(0xFF));
	}

	private WhiteNoiseCharset(final C64Charset charset, final int steps, final Random random) {
		allPoints = Collections.unmodifiableList(IntStream.range(0, C64Char.WIDTH_PIXELS * C64Char.HEIGHT_PIXELS)//
				.mapToObj(n -> new Point(n % C64Char.WIDTH_PIXELS, n / C64Char.HEIGHT_PIXELS))//
				.collect(Collectors.toList()));
		this.charset = charset;
		this.steps = steps;
		this.random = random;
	}

	/**
	 * If this is given, will the returned generator will produce a copy of the given character set with added characters. Make sure there are enough characters
	 * free.
	 */
	public WhiteNoiseCharset withCharset(final C64Charset charset) {
		return new WhiteNoiseCharset(charset, steps, random);
	}

	public WhiteNoiseCharset withSteps(final int steps) {
		return new WhiteNoiseCharset(charset, steps, random);
	}

	public WhiteNoiseCharset withRandom(final Random random) {
		return new WhiteNoiseCharset(charset, steps, random);
	}

	/** creates a sequence of randomly dithered characters with the given amount of pixels each. **/
	public C64Charset uniformWithPixelsPerChar(final int pixels) {
		final C64Charset charset = new C64Charset(this.charset);
		int s = 0;
		while (s <= steps) {
			int retries = 10; // maybe char was already present, try a few times to generate another random one.
			while (!charset.addCharacter(generateRandomCharacter(pixels, random)) && retries > 0) {
				if (--retries == 0) {
					System.out.println(WhiteNoiseCharset.class.getSimpleName() + ": Retries exhausted. Adding char, even if already present.");
					charset.addCharacterEvenIfAlreadyPresent(generateRandomCharacter(pixels, random));
				}
			}
			++s;
		}
		return charset;
	}

	/** Creates a sequence of randomly dithered characters, starting with minPIxels, ending with maxPixels. **/
	public C64Charset ditheredFromMinToMaxPixels(final int minPixels, final int maxPixels) {
		final C64Charset charset = new C64Charset(this.charset);
		double pixels = minPixels;
		int s = 0;
		while (pixels <= maxPixels || s <= steps) {
			int retries = 10; // maybe char was already present, try a few times to generate another random one.
			while (!charset.addCharacter(generateRandomCharacter((int) Math.round(pixels), random)) && retries > 0) {
				if (--retries == 0) {
					System.out.println(WhiteNoiseCharset.class.getSimpleName() + ": Retries exhausted. Adding char, even if already present.");
					charset.addCharacterEvenIfAlreadyPresent(generateRandomCharacter((int) Math.round(pixels), random));
				}
			}
			pixels += (maxPixels - minPixels) / (double) steps;
			++s;
		}
		return charset;
	}

	/** Creates a sequence of randomly dithered characters, starting with minPIxels, ending with maxPixels. **/
	public C64Charset ditheredFromMinToMaxPixelsRetainingPositions(final int minPixels, final int maxPixels) {
		final C64Charset charset = new C64Charset(this.charset);
		double pixels = minPixels;
		int s = 0;
		while (pixels <= maxPixels || s <= steps) {
			// pattern will always be the same, no need to retry.
			charset.addCharacterEvenIfAlreadyPresent(generateRandomCharacter((int) Math.round(pixels), deepClone(random)));
			pixels += (maxPixels - minPixels) / (double) steps;
			++s;
		}
		return charset;
	}

	private C64Char generateRandomCharacter(final int numPixels, final Random random) {
		final List<Point> remainingPoints = new ArrayList<>(allPoints);
		if (numPixels > remainingPoints.size()) {
			throw new IllegalArgumentException(
					"Number of pixels per char (" + numPixels + ") must not be larger than number of pixels in a character ("
							+ C64Char.WIDTH_PIXELS * C64Char.HEIGHT_PIXELS + ")");
		}
		final BufferedImage charImage = new BufferedImage(C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS, BufferedImage.TYPE_INT_RGB);
		IntStream.range(0, numPixels)//
				.mapToObj(n -> remainingPoints.remove(random.nextInt(remainingPoints.size())))//
				.forEach(p -> charImage.setRGB(p.x, p.y, Color.WHITE.getRGB()));
		return new ImageToC64CharHires(Color.BLACK, Color.WHITE).apply(charImage);
	}

}
