package de.plush.brix.c64graphics.core.model;

import static de.plush.brix.c64graphics.core.util.Utils.toByteArray;
import static java.util.Arrays.stream;

import java.awt.*;

import de.plush.brix.c64graphics.core.util.Utils;

// note: I think we should prefer to have a new class like C64SPriteGridZoomed, instead of adding parameters to this.
// as for special formats
public class C64SpriteGrid implements C64BinaryProvider {

	private final C64Sprite[][] grid;

	private final Dimension sizeBlocks;

	public C64SpriteGrid(final Dimension sizeSprites) {
		checkSize(sizeSprites);
		sizeBlocks = new Dimension(sizeSprites);
		grid = new C64Sprite[sizeSprites.height][sizeSprites.width];
		for (int y = 0; y < sizeSprites.height; ++y) {
			for (int x = 0; x < sizeSprites.width; ++x) {
				grid[y][x] = new C64Sprite();
			}
		}
	}

	public C64SpriteGrid(final C64SpriteGrid other) {
		grid = Utils.deepCopy(other.grid);
		sizeBlocks = new Dimension(other.sizeBlocks);
	}

	private static void checkSize(final Dimension sizeBlocks) {
		if (sizeBlocks.width > 8) {
			System.err.println("Warn: Seriously, have you ever seen more than 8 sprites in a row by anyone else but Crossbow/Crest?");
		}
	}

	public Dimension sizeSprites() {
		return new Dimension(sizeBlocks);
	}

	public Dimension sizePixels() {
		return new Dimension(sizeBlocks.width * C64Sprite.WIDTH_PIXELS, sizeBlocks.height * C64Sprite.HEIGHT_PIXELS);
	}

	public C64Sprite spriteAt(final Point p) {
		return spriteAt(p.x, p.y);
	}

	public C64Sprite spriteAtPixel(final int xPix, final int yPix) {
		return spriteAt(xPix / C64Sprite.WIDTH_PIXELS, yPix / C64Sprite.HEIGHT_PIXELS);
	}

	public C64Sprite spriteAt(final int x, final int y) {
		checkPosition(x, y);
		return grid[y][x];
	}

	public void setSprite(final Point gridPos, final C64Sprite sprite) {
		setSprite(gridPos.x, gridPos.y, sprite);
	}

	public void setSprite(final int x, final int y, final C64Sprite sprite) {
		checkPosition(x, y);
		grid[y][x] = sprite;
	}

	private void checkPosition(final int x, final int y) {
		if (x < 0 || x > sizeBlocks.width || y < 0 || y > sizeBlocks.height) {
			throw new IllegalArgumentException("position " + new Point(x, y) + " out of bounds " + sizeBlocks);
		}
	}

	@Override
	public byte[] toC64Binary() {
		return toByteArray(stream(grid).flatMap(line -> stream(line)).map(C64Sprite::toC64Binary));
	}

	public byte[] toC64BinaryIgnoreEmptySprites() {
		final var empty = new C64Sprite();
		return toByteArray(stream(grid).flatMap(line -> stream(line)).filter(sprite -> !sprite.equals(empty)).map(C64Sprite::toC64Binary));
	}

}
