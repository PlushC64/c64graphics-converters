package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.UtilsColor.*;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

/**
 *
 */
public class IsolatedPixelRemoval implements IPipelineStage<BufferedImage, BufferedImage> {

	private final double isolatedPixelColorDiffRMS;

	/**
	 * @param isolatedPixelColorDiffRMS
	 *            a value between 0 and 255, where 0 means no difference and 255 means maximum difference.
	 */
	public IsolatedPixelRemoval(final double isolatedPixelColorDiffRMS) {
		this.isolatedPixelColorDiffRMS = isolatedPixelColorDiffRMS;
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());
		IntStream.range(0, original.getHeight()).parallel().forEach(y -> {
			final int yTop = y > 0 ? y - 1 : 1;// assume top color out of bounds is same as bottom color
			final int yBottom = y < original.getHeight() - 1 ? y + 1 : original.getHeight() - 2;// assume bottom color out of bounds is same as top color
			for (int x = 0; x < original.getWidth(); ++x) {
				final int xLeft = x > 0 ? x - 1 : 1;// assume left color out of bounds is same as right color
				final int xRight = x < original.getWidth() - 1 ? x + 1 : original.getWidth() - 2;// assume right color out of bounds is same as left color

				final Color topLeft = new Color(original.getRGB(xLeft, yTop));
				final Color top = new Color(original.getRGB(x, yTop));
				final Color topRight = new Color(original.getRGB(xRight, yTop));
				final Color left = new Color(original.getRGB(xLeft, y));
				final Color center = new Color(original.getRGB(x, y));
				final Color right = new Color(original.getRGB(xRight, y));
				final Color bottomLeft = new Color(original.getRGB(xLeft, yBottom));
				final Color bottom = new Color(original.getRGB(x, yBottom));
				final Color bottomRight = new Color(original.getRGB(xRight, yBottom));

				final List<Color> surroundingColors = Arrays.asList(topLeft, top, topRight, left, right, bottomLeft, bottom, bottomRight);
				final boolean isIsolated = !surroundingColors.stream().anyMatch(new TestForAPixelColoredLike(center));
				result.setRGB(x, y, isIsolated ? mean(surroundingColors).getRGB() : center.getRGB());
			}
		});
		return result;
	}

	private final class TestForAPixelColoredLike implements Predicate<Color> {
		private final Color color;

		public TestForAPixelColoredLike(final Color color) {
			this.color = color;
		}

		@Override
		public boolean test(final Color color) {
			return rms(this.color, color) < isolatedPixelColorDiffRMS;
		}
	}

}