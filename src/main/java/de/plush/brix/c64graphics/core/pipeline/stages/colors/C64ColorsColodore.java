package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import static de.plush.brix.c64graphics.core.util.Utils.lazy;
import static java.util.Arrays.stream;

import java.awt.Color;

import de.plush.brix.c64graphics.core.util.functions.ByteSupplier;

public enum C64ColorsColodore implements C64Color {

	//@formatter:off
	/**0*/BLACK(0, 0, 0), 				
	/**1*/WHITE(255,255,255),
	/**2*/RED(129,51,56),
	/**3*/CYAN(117,206,200),
	/**4*/PURPLE(142,60,151),
	/**5*/GREEN(86,172,77),
	/**6*/BLUE(46,44,155),
	/**7*/YELLOW(237,241,113),
	//
	/**8*/ORANGE(142,80,41),
	/**9*/BROWN(85,56,0),
	/**A*/LIGHT_RED(196,108,113),
	/**B*/DARK_GRAY(74,74,74),
	/**C*/GRAY(123,123,123),	
	/**D*/LIGHT_GREEN(169,255,159),
	/**E*/LIGHT_BLUE(112,109,235),
	/**F*/LIGHT_GRAY(178,178,178);
	//@formatter:on

	public static final PaletteC64Full PALETTE = new PaletteC64Full() {

		private Color[] colors;

		@Override
		public Color[] colors() {
			if (colors == null) {
				colors = stream(values()).map(C64ColorsColodore::color).toArray(Color[]::new);
			}
			return colors.clone();
		}

		@Override
		public String toString() {
			return C64ColorsColodore.class.getSimpleName();
		}
	};

	public final Color color;

	private final ByteSupplier colorCode = lazy(() -> palette().colorCodeOrException(get()));

	C64ColorsColodore(final int r, final int g, final int b) {
		color = new Color(r, g, b);
	}

	@Override
	public Color get() {
		return color;
	}

	@Override
	public byte colorCode() {
		return colorCode.getAsByte(); // can't do this in the constructor (init phase)
	}

	@Override
	public Palette palette() {
		return PALETTE;
	}

}
