package de.plush.brix.c64graphics.core.util.collections;

import static java.lang.Math.min;
import static java.util.Comparator.naturalOrder;
import static java.util.stream.Collectors.toList;

import java.util.*;
import java.util.stream.*;

import de.plush.brix.c64graphics.core.util.*;

public class IntHashBag {

	private final IntIntHashMap itemToCount;

	private int size;

	public IntHashBag() {
		itemToCount = new IntIntHashMap();
	}

	public IntHashBag(final int initialCapacity) {
		itemToCount = new IntIntHashMap(initialCapacity);
	}

	public static IntHashBag empty() {
		return new IntHashBag();
	}

	public static IntHashBag of(final int... items) {
		final var bag = new IntHashBag();
		bag.addAll(items);
		return bag;
	}

	public boolean add(final int item) {
		return add(item, 1);
	}

	public boolean add(final CountableInt count) {
		return add(count.intValue, Math.toIntExact(count.count));
	}

	private boolean add(final int item, final int amountToAdd) {
		if (amountToAdd < 0) { throw new IllegalArgumentException("Cannot add a negative amount of items"); }
		if (amountToAdd == 0) { return false; }
		itemToCount.update(item, amountToAdd, (itm, currentAmount) -> currentAmount + amountToAdd);
		size += amountToAdd;
		return true;
	}

	public boolean addAll(final int... items) {
		if (items.length == 0) { return false; }
		for (final var item : items) {
			add(item);
		}
		return true;
	}

	public boolean addAll(final IntArrayList items) {
		if (items.isEmpty()) { return false; }
		items.stream().forEach(this::add);
		return true;
	}

	public boolean addAll(final CountableInt... counts) {
		if (counts.length == 0) { return false; }
		final int sizeBefore = size;
		for (final var count : counts) {
			add(count);
		}
		return size != sizeBefore;
	}

	public boolean addAll(final IntHashBag other) {
		if (other.isEmpty()) { return false; }
		// to be more efficient, don't use any mapping to CountableInt and get+put, instead update the internals directly
		other.itemToCount.entryStream()// assuming backing mask does not contain 0-amout entries
				.forEach(entry -> itemToCount.update(entry.getOne(), entry.getTwo(), (i, c) -> c + entry.getTwo()));
		size += other.size;
		return true;
	}

	public boolean remove(final int item) {
		return remove(item, 1);
	}

	public boolean remove(final CountableInt count) {
		return remove(count.intValue, (int) min(Integer.MAX_VALUE, count.count));
	}

	public boolean removeAllOccurrences(final int item) {
		return remove(item, Integer.MAX_VALUE);
	}

	private boolean remove(final int item, final int amountToRemove) {
		if (amountToRemove < 0) { throw new IllegalArgumentException("Cannot remove a negative amount of items"); }
		if (amountToRemove == 0) { return false; }
		final var optionalCount = itemToCount.getOptional(item);
		if (optionalCount.isEmpty()) { return false; } // the item has never been in the set
		final var oldCount = optionalCount.getAsInt();
		if (amountToRemove >= oldCount) {
			itemToCount.remove(item);
			size -= oldCount;
		} else {
			itemToCount.put(item, oldCount - amountToRemove);
			size -= amountToRemove;
		}
		return true;
	}

	public boolean removeAll(final int... items) {
		if (items.length == 0) { return false; }
		final int sizeBefore = size;
		for (final var item : items) {
			remove(item);
		}
		return sizeBefore != size;
	}

	public boolean removeAll(final CountableInt... counts) {
		if (counts.length == 0) { return false; }
		final int sizeBefore = size;
		for (final var count : counts) {
			remove(count);
		}
		return sizeBefore != size;
	}

	public boolean removeAll(final IntHashBag other) {
		if (other.isEmpty()) { return false; }
		final int sizeBefore = size;
		// Operating on this itemToCount directly is rather complicated, because on 0-count items must be removed
		// therefore just use the usual remove method, that's clear to read and works reliably
		other.itemToCount.entryStream().forEach(entry -> remove(entry.getOne(), entry.getTwo()));
		return sizeBefore != size;
	}

	public boolean set(final CountableInt count) {
		final var item = count.intValue;
		final var newAmount = Math.toIntExact(count.count);
		if (newAmount < 0) { throw new IllegalArgumentException("Cannot set a negative amount of items"); }
		final int sizeBefore = size;
		remove(item, Integer.MAX_VALUE); // MAX_VALUE saves from looking up old amount twice
		add(item, newAmount);
		return sizeBefore != size;
	}

	public boolean contains(final int item) {
		return itemToCount.containsKey(item);
	}

	/** @return a stream of values in no particilar order, where each value is given exactly once, if it occurs in the bag at least 1 time. */
	public IntStream distinctValues() {
		return itemToCount.keyStream();
	}

	/** @return a stream of all values in no particular order, where each value will be repeated as many times as it is in the bag. */
	public IntStream values() {
		return itemToCount.entryStream()//
				.flatMapToInt(ic -> IntStream.generate(ic::getOne).limit(ic.getTwo()));
	}

	public Stream<CountableInt> occurrences() {
		return itemToCount.entryStream().map(IntHashBag::toCountableInt);
	}

	public int occurrencesOf(final int item) {
		return itemToCount.getOptional(item).orElse(0);
	}

	public List<CountableInt> bottomOccurrences(final int count) {
		return nOccurrences(count, naturalOrder()); // least amount first
	}

	public List<CountableInt> topOccurrences(final int count) {
		return nOccurrences(count, Comparator.reverseOrder()); // biggest amount first
	}

	private List<CountableInt> nOccurrences(final int count, final Comparator<CountableInt> comparator) {
		if (count < 0) { throw new IllegalArgumentException("Cannot use a value of n < 0"); }
		if (count == 0 || isEmpty()) { return new ArrayList<>(0); }
		final var sorted = itemToCount.entryStream().map(IntHashBag::toCountableInt).sorted(comparator).collect(toList());
		final int index = Math.min(count, sizeDistinct()) - 1;
		final CountableInt cutOff = sorted.get(index); // we want the top/bottom n counts, this is one of the nTh, if there are more of the same count
		final var amount = cutOff.count;
		int end = index + 1;
		while (end < sorted.size() && sorted.get(end).count == amount) { // find index of first pair having a different amount than our cutOff, or list-end
			++end;
		}
		// we could use a subList view of the original sorted list, but that may be very large, so we give it to the GC and create a new one instead
		return new ArrayList<>(sorted.subList(0, end));
	}

	public int size() {
		return size;
	}

	public int sizeDistinct() {
		return itemToCount.size();
	}

	public void clear() {
		itemToCount.clear();
		size = 0;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public int hashCode() {
		if (isEmpty()) { return 0; }
		final int prime = 31;
		int result = 1;
		result = prime * result + (itemToCount == null ? 0 : itemToCount.hashCode());
		result = prime * result + size;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final IntHashBag other = (IntHashBag) obj;
		if (itemToCount == null) {
			if (other.itemToCount != null) { return false; }
		} else if (!itemToCount.equals(other.itemToCount)) { return false; }
		if (size != other.size) { return false; }
		return true;
	}

	private static CountableInt toCountableInt(final IntIntPair entry) {
		return CountableInt.ofValue(entry.getOne()).withCount(entry.getTwo());
	}

}
