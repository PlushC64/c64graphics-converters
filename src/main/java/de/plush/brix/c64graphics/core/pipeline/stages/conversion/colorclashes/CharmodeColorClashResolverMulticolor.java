package de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes;

import static de.plush.brix.c64graphics.core.util.Utils.*;
import static de.plush.brix.c64graphics.core.util.UtilsImage.*;
import static de.plush.brix.c64graphics.core.util.UtilsImage.deepCopy;
import static java.lang.Math.*;
import static java.util.Arrays.*;
import static java.util.Comparator.*;
import static java.util.stream.Collectors.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.*;
import java.util.stream.*;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.CharmodeColorClashResolverMulticolor.PreProcessedImage;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.Alignment.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.PaletteConversion.PaletteConvertedImage;
import de.plush.brix.c64graphics.core.util.*;
import de.plush.brix.c64graphics.core.util.Utils.WeightedObject;

public class CharmodeColorClashResolverMulticolor implements IPipelineStage<BufferedImage, PreProcessedImage> {

	private final Palette palette;

	private final Palette d800SubPalette;

	private final ColorDistanceMeasure colorDistanceMeasure;

	private final Set<Point> forcedHiresPositions = new HashSet<>();

	private final Set<Point> forcedMulticolorPositions = new HashSet<>();

	private Optional<Color> forcedBg = Optional.empty();

	private Optional<Color> forcedMc1 = Optional.empty();

	private Optional<Color> forcedMc2 = Optional.empty();

	public CharmodeColorClashResolverMulticolor(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure) {
		this.palette = palette;
		d800SubPalette = palette.d800MulticolorCharmodeSubPalette();
		this.colorDistanceMeasure = colorDistanceMeasure;
	}

	public CharmodeColorClashResolverMulticolor withForcedMulticolorBlocksAt(final Point... positions) {
		forcedMulticolorPositions.addAll(asList(positions));
		checkSettingsConsistency();
		return this;
	}

	public CharmodeColorClashResolverMulticolor withForcedHiresBlocksAt(final Point... positions) {
		forcedHiresPositions.addAll(asList(positions));
		checkSettingsConsistency();
		return this;
	}

	public CharmodeColorClashResolverMulticolor withForcedBackgroundColor(final Color background) {
		forcedBg = Optional.ofNullable(background);
		return this;
	}

	public CharmodeColorClashResolverMulticolor withForcedMulticolor1D022(final Color mc1_D022) {
		forcedMc1 = Optional.ofNullable(mc1_D022);
		return this;
	}

	public CharmodeColorClashResolverMulticolor withForcedMulticolor2D023(final Color mc2_D023) {
		forcedMc2 = Optional.ofNullable(mc2_D023);
		return this;
	}

	private void checkSettingsConsistency() {
		final HashSet<Point> x = new HashSet<>(forcedHiresPositions);
		x.retainAll(forcedMulticolorPositions);
		if (!x.isEmpty()) { throw new IllegalArgumentException("You can't force these block positions to be both hiores and multicolor: " + x); }
	}

	@Override
	public PreProcessedImage apply(final BufferedImage original) {
		final long time = System.nanoTime();
		try {
			final Set<Color> imageColors = colors(original);
			System.out.println("image colors: " + imageColors.size());
			assert asList(palette.colors()).containsAll(imageColors) : "all colors in image must be in palette " + palette;
			// runtime protection: images with too many colors cause too many subsets, if the color range is too large, simply use all of the target palette
			final Stream<List<Color>> subsets //
					= imageColors.size() <= 16 ? subsetStream(imageColors, 3, ArrayList::new) //
							: subsetStream(asList(palette.colors()), 3, ArrayList::new);
			final List<List<Color>> ssList = subsets.collect(toList()); // for some stupid reason, using the stream directly takes 10 times as long!
			addPermutationsForDifferentBackgroundColors(ssList);
			System.out.println("[bg,mc1,mc2] color combinations: " + ssList.size());
			// System.out.println("[bg,mc1,mc2] color combinations: " + ssList);
			removeIllegalCombinations(ssList);
			System.out.println("[bg,mc1,mc2] legal color combinations: " + ssList.size());
			return ssList.stream()// all possible combinations of 3 image colors for background, mc1 and mc2
					.parallel()//
					.map(bgmc1mc2 -> {
						final Color a = bgmc1mc2.get(0);
						final Color b = bgmc1mc2.size() > 1 ? bgmc1mc2.get(1) : a;
						final Color c = bgmc1mc2.size() > 2 ? bgmc1mc2.get(2) : a;
						return toImage(original, a, b, c);
					}).map(reducedImage -> new WeightedObject<>(reducedImage, error(reducedImage, original, colorDistanceMeasure)))// don't calc per comparison
					// .peek(this::printStats)//
					.peek(wo -> { if (wo.weight > -3 * ulp(wo.weight) && wo.weight < 3 * ulp(wo.weight)) { throw new EarlyStreamExit(wo); } })//
					.sorted(comparing(WeightedObject::weight))//
					.min(comparing(WeightedObject::weight))//
					.map(this::printStats)//
					.map(WeightedObject::object).get();//
		} catch (final EarlyStreamExit e) {
			final WeightedObject<PreProcessedImage> result = e.result();
			printStats(result);
			return result.object;
		} finally {
			System.out.println(TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - time) + "ms");
		}
	}

	private static void addPermutationsForDifferentBackgroundColors(final List<List<Color>> ssList) {
		ssList.addAll(ssList.stream().map(bgmc1mc2 -> {
			final Color a = bgmc1mc2.get(0);
			final Color b = bgmc1mc2.size() > 1 ? bgmc1mc2.get(1) : a;
			final Color c = bgmc1mc2.size() > 2 ? bgmc1mc2.get(2) : a;
			return Stream.<List<Color>> of(//
					new ArrayList<>(asList(a, b, c)), // from the set of 3 colors, try each one as background for hires characters
					new ArrayList<>(asList(b, a, c)), //
					new ArrayList<>(asList(c, a, b)));
		}).flatMap(Function.identity()).collect(toList()));
	}

	private void removeIllegalCombinations(final List<List<Color>> ssList) {
		ssList.removeIf(bgmc1mc2 -> {
			return forcedBg.isPresent() && !forcedBg.get().equals(bgmc1mc2.get(0))//
					|| forcedMc1.isPresent() && bgmc1mc2.size() > 1 && !forcedMc1.get().equals(bgmc1mc2.get(1))//
					|| forcedMc2.isPresent() && bgmc1mc2.size() > 2 && !forcedMc2.get().equals(bgmc1mc2.get(2));//
		});
	}

	private PreProcessedImage toImage(final BufferedImage image, final Color background, final Color mc1, final Color mc2) {
		// background, mc1 and mc2 are global colors
		final var original = new ResizeCanvas(C64Bitmap.sizePixels(), () -> background, Horizontal.LEFT, Vertical.TOP).apply(image);
		final PreProcessedImage result = new PreProcessedImage(original.getWidth(), original.getHeight(), original.getType(), //
				palette.colorCodeOrException(background), palette.colorCodeOrException(mc1), palette.colorCodeOrException(mc2));
		final Graphics g = result.getGraphics();
		try {
			IntStream.range(0, original.getHeight())//
					.filter(y -> y % C64Char.HEIGHT_PIXELS == 0)//
					// do not parallelize. The higher level needs the threads
					.forEach(yStart -> {
						for (int xStart = 0; xStart < original.getWidth(); xStart += C64Char.WIDTH_PIXELS) {
							final Point blockPos = new Point(xStart / C64Char.WIDTH_PIXELS, yStart / C64Char.HEIGHT_PIXELS);
							final int restX = min(C64Char.WIDTH_PIXELS, original.getWidth() - xStart);
							final int restY = min(C64Char.HEIGHT_PIXELS, original.getHeight() - yStart);
							final BufferedImage subimage = original.getSubimage(xStart, yStart, restX, restY);
							final Set<Color> originalColors = colors(subimage);
							final Optional<Color> perfectD800ColorMatch = perfectD800ColorMatch(background, mc1, mc2, originalColors);
							// perfect color matches can disrupt enforcing multicolor (finds colorCodes like 0). So we skip this case for enforced MC positions
							if (perfectD800ColorMatch.isPresent() && !forcedMulticolorPositions.contains(blockPos)) {
								result.setColorCode(blockPos.x, blockPos.y, palette.colorCodeOrException(perfectD800ColorMatch.get()));
								g.drawImage(subimage, xStart, yStart, null);
							} else {
								result.clashes.add(blockPos);
								// possible hires conversions: Each Color[] has d800 as first value
								final Set<Color[]> hiresColorPairs = get2ColorAlternatives(background, originalColors);
								final Stream<WeightedObject<PaletteConvertedImage>> hiresCharacterImages //
								= forcedMulticolorPositions.contains(blockPos) ? Stream.empty() //
										: hiresColorPairs.stream()//
												.map(replacementColors -> (Palette) () -> replacementColors)//
												.map(palette -> new PaletteConversion(palette, colorDistanceMeasure, Dithering.None).apply(deepCopy(subimage)))//
												.map(alternativeSubimage -> new WeightedObject<>(alternativeSubimage, error(alternativeSubimage, subimage,
														colorDistanceMeasure)));
								// possible multicolor conversions: Each Color[] has d800 as first value
								final Set<Color[]> alternativeColorSets = get4ColorAlternatives(background, mc1, mc2, originalColors);
								final Stream<WeightedObject<PaletteConvertedImage>> multicolorCharacterImages //
								= forcedHiresPositions.contains(blockPos) ? Stream.empty() //
										: alternativeColorSets.stream()//
												.map(replacementColors -> (Palette) () -> replacementColors)//
												.map(palette -> new PaletteConversion(palette, colorDistanceMeasure, Dithering.None).apply(//
														new Pixelize(new Dimension(2, 1)).apply(//
																deepCopy(subimage))))//
												.map(alternativeSubimage -> new WeightedObject<>(alternativeSubimage, error(alternativeSubimage, subimage,
														colorDistanceMeasure)));

								final PaletteConvertedImage minErrImage = Stream.concat(hiresCharacterImages, multicolorCharacterImages)//
										.min(comparing((final Utils.WeightedObject<PaletteConvertedImage> weightedImage) -> weightedImage.weight))//
										.map(WeightedObject::object)//
										.orElseThrow(() -> new IllegalStateException("No minimum found at block " + blockPos + ".\noriginalColors:"
												+ originalColors + "\nbg=" + background + "\nmc1=" + mc1 + "\nmc2=" + mc2));
								// reduced images palette has d800 as first value:
								final Palette reducedPalette = minErrImage.palette();
								byte d800colorCode = palette.colorCodeOrException(reducedPalette.color((byte) 0));
								if (reducedPalette.colors().length > 2) { // multicolor conversion?
									d800colorCode = palette.colorCodeWithMulticolorBitSetOrException(d800colorCode); // set multicolor bit in colorRam
								}
								result.setColorCode(blockPos.x, blockPos.y, d800colorCode);
								g.drawImage(minErrImage, xStart, yStart, null);
							}
						}
					});
		} finally {
			g.dispose();
		}
		return result;
	}

	/**
	 * @return a non-empty optional, if the character block can be drawn as-is, with no pixelization and a known d800 value
	 */
	private Optional<Color> perfectD800ColorMatch(final Color background, final Color mc1, final Color mc2, final Set<Color> blockColors) {
		if (blockColors.size() == 1) { // special case: only one color in block -> even in multicolor mode no pixel stretching is needed
			final Color color = blockColors.iterator().next();
			if (color.equals(background)) { // -> no d800 needed
				return Optional.of(palette.color((byte) 0x00)); // chose hires black ($00) as default
			} else if (color.equals(mc1) || color.equals(mc2)) { // -> d800 needed to switch to multicolor only
				return Optional.of(palette.color((byte) 0x08)); // chose multicolor black ($08) as default
			} else if (palette.canBeDisplayedInD800InMulticolorCharmode(color)) { // non global color, but d800 capable
				return Optional.of(color);
			}
		}
		// so we have more than 1 colors in the block, or the only color cannot be displayed in d800Mode:
		if (blockColors.size() == 2) { // possible hires mode
			final Predicate<Color> noBackground = c -> !background.equals(c);
			final List<Color> nonBackgroundColors = blockColors.stream().filter(noBackground).collect(toList());
			if (nonBackgroundColors.size() == 1) { // test for hires mode (background+character color)
				final Color d800Candidate = nonBackgroundColors.get(0);
				if (palette.canBeDisplayedInD800InMulticolorCharmode(d800Candidate)) {
					return Optional.of(d800Candidate); // background+character color -> hires possible!
				}
			}
		}
		return Optional.empty(); // either multicolor mode that needs pixel stretching or color clash must be resolved
	}

	/**
	 * Gets a list of colors that contain the global colors and one of the original colors capable of being used in D800 (color ram)
	 *
	 * @return Set of Color[] { d800candidate, background }
	 */
	private Set<Color[]> get2ColorAlternatives(final Color background, final Set<Color> originalColors) {
		final List<Color> possibleD800ColorCandidates = d800Candidates(asList(background), originalColors);
		return possibleD800ColorCandidates.stream().map(d800candidate -> new Color[] { d800candidate, background }).collect(toSet());
	}

	/** Gets a list of colors that contain the global colors and one of the original colors capable of being used in D800 (color ram) */
	private Set<Color[]> get4ColorAlternatives(final Color background, final Color mc1, final Color mc2, final Set<Color> originalColors) {
		final List<Color> possibleD800ColorCandidates = d800Candidates(asList(background, mc1, mc2), originalColors);
		return possibleD800ColorCandidates.stream().map(d800candidate -> new Color[] { d800candidate, background, mc1, mc2 }).collect(toSet());
	}

	private List<Color> d800Candidates(final List<Color> globalColors, final Set<Color> originalColors) {
		final Predicate<Color> noGlobalColor = c -> !globalColors.contains(c);
		final List<Color> possibleD800ColorCandidates = originalColors.stream()//
				.filter(noGlobalColor)//
				.filter(palette::canBeDisplayedInD800InMulticolorCharmode)//
				.collect(toList());
		if (!possibleD800ColorCandidates.isEmpty()) {
			return possibleD800ColorCandidates; // character already has colors that can be used in d800, return all of them.
		}
		// bad! character contains no color that could be used in d800
		// getting the 3 colors from the d800 sub palette that are closest to any of the original colors
		return stream(d800SubPalette.colors())//
				.map(d800 -> originalColors.stream().map(oc -> new WeightedObject<>(d800, colorDistanceMeasure.distanceOf(d800, oc))))//
				.flatMap(stream -> stream.sorted(comparingDouble(WeightedObject::weight)))//
				.map(WeightedObject::object)//
				.distinct()//
				.limit(4)//
				.collect(toList());
	}

	private WeightedObject<PreProcessedImage> printStats(final WeightedObject<PreProcessedImage> wo) {
		System.out.println("[bg, mc1, mc2]: " + asList(//
				toHexString(wo.object.backgroundColorCode), //
				toHexString(wo.object.mc1ColorCode), //
				toHexString(wo.object.mc2ColorCode)//
		) + " weight: " + wo.weight);
		return wo;
	}

	public static class PreProcessedImage extends BufferedImage {

		private final byte backgroundColorCode;

		private final byte mc1ColorCode;

		private final byte mc2ColorCode;

		private final C64ColorRam colorRam = new C64ColorRam((byte) 0);

		private final Collection<Point> clashes = new HashSet<>();

		// TODO: store palette with image
		public PreProcessedImage(final int width, final int height, final int imageType, final byte backgroundColorCode, final byte mc1ColorCode,
				final byte mc2ColorCode) {
			super(width, height, imageType);
			this.backgroundColorCode = backgroundColorCode;
			this.mc1ColorCode = mc1ColorCode;
			this.mc2ColorCode = mc2ColorCode;
		}

		public C64ColorRam colorRam() {
			return new C64ColorRam(colorRam);
		}

		private void setColorCode(final int x, final int y, final byte colorCode) {
			colorRam.setColorCode(x, y, colorCode);
		}

		public byte backgroundColorCode() {
			return backgroundColorCode;
		}

		public byte mc1ColorCode() {
			return mc1ColorCode;
		}

		public byte mc2ColorCode() {
			return mc2ColorCode;
		}

		@Override
		public String toString() {
			return getClass().getSimpleName() + " [backgroundColorCode=" + backgroundColorCode + ", mc1ColorCode=" + mc1ColorCode + ", mc2ColorCode="
					+ mc2ColorCode + ", colorRam=" + colorRam + ", clashes=" + clashes + "]";
		}

	}

}
