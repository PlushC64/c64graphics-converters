package de.plush.brix.c64graphics.core.model;

import java.awt.Dimension;
import java.math.BigInteger;
import java.util.*;

import javax.annotation.concurrent.Immutable;

import de.plush.brix.c64graphics.core.util.Utils;

@Immutable
public final class C64Sprite implements C64BinaryProvider, Comparable<C64Sprite>, Iterable<Byte> {

	public static final C64Sprite BLANK = new C64Sprite(new byte[C64Sprite.WIDTH_PIXELS / 8 * C64Sprite.HEIGHT_PIXELS]);

	public static final int WIDTH_PIXELS = 24;
	public static final int HEIGHT_PIXELS = 21;

	private final byte[] bytes;
	private volatile BigInteger value;

	public C64Sprite() {
		this(new byte[0x40]);
	}

	public C64Sprite(final byte[] bytes) {
		if (bytes.length > 0x40 || bytes.length < 0x3f) { // sprites actually have 0x40 bytes, the last byte is not used
			throw new IllegalArgumentException("wrong number of bytes (" + bytes.length + "). Expected: " + 0x40 + " or " + 0x3f);
		}
		this.bytes = Arrays.copyOf(bytes, Math.max(bytes.length, 0x40));
	}

	public static Dimension sizePixels() {
		return new Dimension(WIDTH_PIXELS, HEIGHT_PIXELS);
	}

	@Override
	public Iterator<Byte> iterator() {
		return new ByteArrayIterator(bytes);
	}

	@Override
	public int compareTo(final C64Sprite other) {
		return other == null ? 1 : value().compareTo(other.value());
	}

	@Override
	public int hashCode() {
		return value().hashCode();
	}

	@Override
	public boolean equals(final Object other) {
		return other != null //
				&& getClass().equals(other.getClass()) //
				&& value().equals(((C64Sprite) other).value());
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder(WIDTH_PIXELS * HEIGHT_PIXELS);
		final int widthBytes = WIDTH_PIXELS / 8;
		for (int y = 0; y < HEIGHT_PIXELS; ++y) {
			for (int x = 0; x < widthBytes; ++x) {
				sb.append(Utils.toBinaryString(bytes[y * widthBytes + x]));
			}
			sb.append('\n');
		}
		return sb.toString();
	}

	@Override
	public byte[] toC64Binary() {
		return Arrays.copyOf(bytes, bytes.length);
	}

	@SuppressWarnings("boxing")
	private BigInteger value() {
		if (value == null) {
			final byte[] bytes = new byte[this.bytes.length + 1]; // bytes[0] contains the sign, which is always positive
			System.arraycopy(this.bytes, 0, bytes, 1, this.bytes.length);
			value = new BigInteger(bytes);
		}
		return value;
	}

}
