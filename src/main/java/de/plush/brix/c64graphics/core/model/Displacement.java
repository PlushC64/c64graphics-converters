package de.plush.brix.c64graphics.core.model;

import static de.plush.brix.c64graphics.core.model.Direction.Horizontal.*;
import static de.plush.brix.c64graphics.core.model.Direction.Vertical.*;

import java.awt.Point;

/**
 * 
 * <pre>
 * <code>
 * import static de.plush.brix.c64graphics.core.model.Direction.Horizontal.*;
 * import static de.plush.brix.c64graphics.core.model.Direction.Vertical.*;
 * // ...
 * Displacement x = Left.by(3);
 * Displacement y = Down.by(2);
 * </code>
 * </pre>
 */
public class Displacement {
	public final Direction direction;
	public final int value;

	public Displacement(final Direction direction, final int value) {
		this.direction = direction;
		this.value = value;
	}

	/**
	 * @return a Point in the Java2D coordinate system: Origin (0/0) is top left. <br/>
	 *         Left/Up directions will yield negative values for p.x and p.y.
	 **/
	public Point asPoint() {
		final var p = new Point();
		if (Up.equals(direction)) {
			p.x = 0;
			p.y = -value;
		} else if (Down.equals(direction)) {
			p.x = 0;
			p.y = value;
		} else if (Left.equals(direction)) {
			p.x = -value;
			p.y = 0;
		} else if (Right.equals(direction)) {
			p.x = value;
			p.y = 0;
		} else {
			throw new IllegalArgumentException("Unsupported direction: " + direction);
		}
		return p;
	}

	/**
	 * Translates a point according to the Java2D coordinate system: Origin (0/0) is top left. <br/>
	 * Left/Up directions will subtract values from p.x and p.y.
	 */
	public void displace(final Point p) {
		final var d = asPoint();
		p.translate(d.x, d.y);
	}
}
