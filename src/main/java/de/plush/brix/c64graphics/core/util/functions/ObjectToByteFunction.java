package de.plush.brix.c64graphics.core.util.functions;

@FunctionalInterface
public interface ObjectToByteFunction<T> {
	byte apply(T object);
}
