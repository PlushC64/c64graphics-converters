/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class Border implements IPipelineStage<BufferedImage, BufferedImage> {

	private final Supplier<Color> colorSupplier;

	private final Insets insets;

	public Border(final Supplier<Color> colorSupplier, final Insets insets) {
		this.colorSupplier = colorSupplier;
		this.insets = new Insets(insets.top, insets.left, insets.bottom, insets.right);
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		final Graphics2D g = (Graphics2D) result.getGraphics();
		try {
			g.drawImage(original, 0, 0, null);
			g.setColor(colorSupplier.get());
			g.fillRect(0, 0, insets.left, result.getHeight());
			g.fillRect(result.getWidth() - insets.right, 0, insets.right, result.getHeight());
			g.fillRect(0, 0, result.getWidth(), insets.top);
			g.fillRect(0, result.getHeight() - insets.bottom, result.getWidth(), insets.bottom);
			return result;
		} finally {
			g.dispose();
		}

	}
}