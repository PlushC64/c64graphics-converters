package de.plush.brix.c64graphics.core.util.collections;

import static java.lang.Math.max;
import static java.util.stream.Collectors.joining;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

import de.plush.brix.c64graphics.core.util.ObjectIntPair;
import de.plush.brix.c64graphics.core.util.functions.ObjectIntToIntFunction;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/* Losely Based on org.eclipse.collections. */
public class ObjectIntHashMap<K> {
	static final int DEFAULT_NO_VALUE = Integer.MIN_VALUE + 1;

	private static final int DEFAULT_INITIAL_CAPACITY = 8;

	private Object[] keys;

	private int[] values;

	private int occupiedWithData;

	private int occupiedWithSentinels;

	public final int no_value;

	public ObjectIntHashMap() {
		this(DEFAULT_INITIAL_CAPACITY);
	}

	public ObjectIntHashMap(final int initialCapacity) {
		this(initialCapacity, DEFAULT_NO_VALUE);
	}

	/**
	 * @param no_value
	 *            the valkue to represent no_value
	 * @param unused
	 *            always null.
	 */
	public ObjectIntHashMap(final int no_value, final Void unused) {
		this(DEFAULT_INITIAL_CAPACITY, no_value);
	}

	public ObjectIntHashMap(final int initialCapacity, final int no_value) {
		if (initialCapacity < 0) { throw new IllegalArgumentException("initial capacity cannot be less than 0"); }
		final int capacity = smallestPowerOfTwoGreaterThan(fastCeil(initialCapacity << 1));
		allocateTable(capacity);
		this.no_value = no_value;
	}

	public ObjectIntHashMap(final ObjectIntHashMap<? extends K> map) {
		this(Math.max(map.size(), DEFAULT_INITIAL_CAPACITY), map.no_value);
		putAll(map);
	}

	public static <T> ObjectIntHashMap<T> of(final T key, final int value) {
		final var map = new ObjectIntHashMap<T>();
		map.put(key, value);
		return map;
	}

	public static <T> ObjectIntHashMap<T> of(final T key0, final int value0, final T key1, final int value1) {
		final var map = ObjectIntHashMap.of(key0, value0);
		map.put(key1, value1);
		return map;
	}

	public static <T> ObjectIntHashMap<T> of(final T key0, final int value0, final T key1, final int value1, final T key2, final int value2) {
		final var map = ObjectIntHashMap.of(key0, value0, key1, value1);
		map.put(key2, value2);
		return map;
	}

	public static <T> ObjectIntHashMap<T> of(final T key0, final int value0, final T key1, final int value1, final T key2, final int value2, final T key3,
			final int value3) {
		final var map = ObjectIntHashMap.of(key0, value0, key1, value1, key2, value2);
		map.put(key3, value3);
		return map;
	}

	public static <T> ObjectIntHashMap<T> of(final T key0, final int value0, final T key1, final int value1, final T key2, final int value2, final T key3,
			final int value3, final T key4, final int value4) {
		final var map = ObjectIntHashMap.of(key0, value0, key1, value1, key2, value2, key3, value3);
		map.put(key4, value4);
		return map;
	}

	public static <T> ObjectIntHashMap<T> of(final T key0, final int value0, final T key1, final int value1, final T key2, final int value2, final T key3,
			final int value3, final T key4, final int value4, final T key5, final int value5) {
		final var map = ObjectIntHashMap.of(key0, value0, key1, value1, key2, value2, key3, value3, key4, value4);
		map.put(key5, value5);
		return map;
	}

	public static <T> ObjectIntHashMap<T> of(final T key0, final int value0, final T key1, final int value1, final T key2, final int value2, final T key3,
			final int value3, final T key4, final int value4, final T key5, final int value5, final T key6, final int value6) {
		final var map = ObjectIntHashMap.of(key0, value0, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5);
		map.put(key6, value6);
		return map;
	}

	public static <T> ObjectIntHashMap<T> of(final T key0, final int value0, final T key1, final int value1, final T key2, final int value2, final T key3,
			final int value3, final T key4, final int value4, final T key5, final int value5, final T key6, final int value6, final T key7, final int value7) {
		final var map = ObjectIntHashMap.of(key0, value0, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5, key6, value6);
		map.put(key7, value7);
		return map;
	}

	public static <T> ObjectIntHashMap<T> of(final T key0, final int value0, final T key1, final int value1, final T key2, final int value2, final T key3,
			final int value3, final T key4, final int value4, final T key5, final int value5, final T key6, final int value6, final T key7, final int value7,
			final T key8, final int value8) {
		final var map = ObjectIntHashMap.of(key0, value0, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5, key6, value6, key7, value7);
		map.put(key8, value8);
		return map;
	}

	public static <T> ObjectIntHashMap<T> of(final T key0, final int value0, final T key1, final int value1, final T key2, final int value2, final T key3,
			final int value3, final T key4, final int value4, final T key5, final int value5, final T key6, final int value6, final T key7, final int value7,
			final T key8, final int value8, final T key9, final int value9) {
		final var map = ObjectIntHashMap.of(key0, value0, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5, key6, value6, key7, value7,
				key8, value8);
		map.put(key9, value9);
		return map;
	}

	@SafeVarargs
	public static <T> ObjectIntHashMap<T> of(final ObjectIntPair<T>... values) {
		final var map = new ObjectIntHashMap<T>(max(values.length, DEFAULT_INITIAL_CAPACITY));
		for (final ObjectIntPair<T> value : values) {
			map.put(value.getOne(), value.getTwo());
		}
		return map;
	}

	public int get(final Object key) {
		return getIfAbsent(key, no_value);
	}

	public int getIfAbsent(final Object key, final int ifAbsent) {
		final int index = probe(key);
		if (isOccupied(keys[index]) && Objects.equals(toNonSentinel(keys[index]), key)) { return values[index]; }
		return ifAbsent;
	}

	public OptionalInt getOptional(final Object key) {
		final int index = probe(key);
		if (isOccupied(keys[index]) && Objects.equals(toNonSentinel(keys[index]), key)) { return OptionalInt.of(values[index]); }
		return OptionalInt.empty();
	}

	public int getIfAbsentPut(final K key, final int value) {
		final int index = probe(key);
		if (isOccupied(keys[index]) && Objects.equals(toNonSentinel(keys[index]), key)) { return values[index]; }
		addKeyValueAtIndex(key, value, index);
		return value;
	}

	public int getIfAbsentPut(final K key, final IntSupplier function) {
		final int index = probe(key);
		if (isOccupied(keys[index]) && Objects.equals(toNonSentinel(keys[index]), key)) { return values[index]; }
		final int value = function.getAsInt();
		addKeyValueAtIndex(key, value, index);
		return value;
	}

	public int getIfAbsentPutWithKey(final K key, final ToIntFunction<? super K> function) {
		final int index = probe(key);
		if (isOccupied(keys[index]) && Objects.equals(toNonSentinel(keys[index]), key)) { return values[index]; }
		final int value = function.applyAsInt(key);
		addKeyValueAtIndex(key, value, index);
		return value;
	}

	public boolean containsKey(final Object key) {
		final int index = probe(key);
		return isOccupied(keys[index]) && Objects.equals(toNonSentinel(keys[index]), key);
	}

	public void put(final K key, final int value) {
		invalidateHashCode();
		final int index = probe(key);

		if (isOccupied(keys[index]) && Objects.equals(toNonSentinel(keys[index]), key)) {
			// key already present in map
			values[index] = value;
			return;
		}

		addKeyValueAtIndex(key, value, index);
	}

	public void putAll(final ObjectIntHashMap<? extends K> map) {
		map.forEachEntry(this::put);
	}

	public int update(final K key, final int initialValueIfAbsent, final IntUnaryOperator function) {
		final int index = probe(key);
		if (isOccupied(keys[index]) && Objects.equals(toNonSentinel(keys[index]), key)) {
			values[index] = function.applyAsInt(values[index]);
			return values[index];
		}
		final int value = initialValueIfAbsent;
		addKeyValueAtIndex(key, value, index);
		return value;
	}

	public void updateValues(final ObjectIntToIntFunction<? super K> function) {
		invalidateHashCode();
		for (int i = 0; i < keys.length; i++) {
			if (isOccupied(keys[i])) {
				values[i] = function.valueOf(toNonSentinel(keys[i]), values[i]);
			}
		}
	}

	public void removeKey(final K key) {
		final int index = probe(key);
		removeKeyAtIndex(key, index);
	}

	@SuppressWarnings("unchecked")
	public void remove(final Object key) {
		removeKey((K) key);
	}

	public int removeKeyIfAbsent(final K key, final int value) {
		invalidateHashCode();
		final int index = probe(key);
		if (isOccupied(keys[index]) && Objects.equals(toNonSentinel(keys[index]), key)) {
			keys[index] = REMOVED_KEY;
			final int oldValue = values[index];
			values[index] = no_value;
			occupiedWithData--;
			occupiedWithSentinels++;
			return oldValue;
		}
		return value;
	}

	public Stream<K> keyStream() {
		return Arrays.stream(keys).filter(ObjectIntHashMap::isOccupied).map(this::toNonSentinel);
	}

	public IntStream valueStream() {
		return indexStream().map(index -> values[index]);
	}

	public Stream<ObjectIntPair<K>> entryStream() {
		return indexStream().mapToObj(index -> ObjectIntPair.of(toNonSentinel(keys[index]), values[index]));
	}

	private IntStream indexStream() {
		return IntStream.range(0, keys.length).filter(index -> isOccupied(keys[index]));
	}

	public void forEachKey(final Consumer<? super K> procedure) {
		for (int i = 0; i < keys.length; ++i) {
			if (isOccupied(keys[i])) {
				procedure.accept(toNonSentinel(keys[i]));
			}
		}
	}

	public void forEachEntry(final ObjIntConsumer<? super K> procedure) {
		for (int i = 0; i < keys.length; ++i) {
			if (isOccupied(keys[i])) {
				procedure.accept(toNonSentinel(keys[i]), values[i]);
			}
		}
	}

	/**
	 * Rehashes every element in the set into a new backing table of the smallest possible size and eliminating removed sentinels.
	 */
	public void compact() {
		rehash(ObjectIntHashMap.smallestPowerOfTwoGreaterThan(size()));
	}

	public void clear() {
		invalidateHashCode();
		occupiedWithData = 0;
		occupiedWithSentinels = 0;
		Arrays.fill(keys, null);
		Arrays.fill(values, no_value);
	}

	public int size() {
		return occupiedWithData;
	}

	public boolean isEmpty() {
		return size() == 0;
	}

	public ObjectIntPair<K>[] toArray() {
		return entryStream().toArray(ObjectIntPair[]::new);
	}

	public ObjectIntPair<K>[] toSortedArray() {
		return entryStream().sorted().toArray(ObjectIntPair[]::new);
	}

	private int hashCode;

	private void invalidateHashCode() {
		hashCode = 0;
	}

	@Override
	public int hashCode() {
		if (hashCode != 0) { return hashCode; }
		if (isEmpty()) { return 0; }
		final int prime = 31;
		int result = 1;
		result = prime * result + size();
		result = prime * result + Arrays.hashCode(toSortedArray());
		hashCode = result;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		@SuppressWarnings("unchecked")
		final ObjectIntHashMap<K> other = (ObjectIntHashMap<K>) obj;
		if (size() != other.size()) { return false; }
		if (!Arrays.equals(toSortedArray(), other.toSortedArray())) { return false; }
		return true;
	}

	@Override
	public String toString() {
		return "IntIntHashMap [no_value=" + no_value + ", size()=" + size() //
				+ ", entries=" + entryStream().map(ObjectIntPair::toString).collect(joining()) + "]";
	}

	// -- bunch of internal stuff follows

	private void addKeyValueAtIndex(final K key, final int value, final int index) {
		invalidateHashCode();
		if (keys[index] == REMOVED_KEY) {
			--occupiedWithSentinels;
		}
		keys[index] = toSentinelIfNull(key);
		values[index] = value;
		++occupiedWithData;
		if (occupiedWithData + occupiedWithSentinels > maxOccupiedWithData()) {
			rehashAndGrow();
		}
	}

	private void removeKeyAtIndex(final K key, final int index) {
		invalidateHashCode();
		if (isOccupied(keys[index]) && Objects.equals(toNonSentinel(keys[index]), key)) {
			keys[index] = REMOVED_KEY;
			values[index] = no_value;
			occupiedWithData--;
			occupiedWithSentinels++;
		}
	}

	private int probe(final Object element) {
		final int index = spread(element);

		int removedIndex = -1;
		if (isRemovedKey(keys[index])) {
			removedIndex = index;
		}

		else if (keys[index] == null || Objects.equals(toNonSentinel(keys[index]), element)) { return index; }

		int nextIndex = index;
		int probe = 17;

		// loop until an empty slot is reached
		while (true) {
			// Probe algorithm: 17*n*(n+1)/2 where n = no. of collisions
			nextIndex += probe;
			probe += 17;
			nextIndex &= keys.length - 1;

			if (isRemovedKey(keys[nextIndex])) {
				if (removedIndex == -1) {
					removedIndex = nextIndex;
				}
			} else if (Objects.equals(toNonSentinel(keys[nextIndex]), element)) {
				return nextIndex;
			} else if (keys[nextIndex] == null) { return removedIndex == -1 ? nextIndex : removedIndex; }
		}
	}

	private int spread(final Object element) {
		// This function ensures that hashCodes that differ only by
		// constant multiples at each bit position have a bounded
		// number of collisions (approximately 8 at default load factor).
		int h = element == null ? 0 : element.hashCode();
		h ^= h >>> 20 ^ h >>> 12;
		h ^= h >>> 7 ^ h >>> 4;
		return h & keys.length - 1;
	}

	private static int smallestPowerOfTwoGreaterThan(final int n) {
		return n > 1 ? Integer.highestOneBit(n - 1) << 1 : 1;
	}

	private static int fastCeil(final float v) {
		int possibleResult = (int) v;
		if (v - possibleResult > 0.0F) {
			possibleResult++;
		}
		return possibleResult;
	}

	private void rehashAndGrow() {
		final int max = maxOccupiedWithData();
		int newCapacity = Math.max(max, smallestPowerOfTwoGreaterThan(occupiedWithData + 1 << 1));
		if (occupiedWithSentinels > 0 && (max >> 1) + (max >> 2) < occupiedWithData) {
			newCapacity <<= 1;
		}
		rehash(newCapacity);
	}

	private void rehash(final int newCapacity) {
		final int oldLength = keys.length;
		final Object[] old = keys;
		final int[] oldValues = values;
		allocateTable(newCapacity);
		occupiedWithData = 0;
		occupiedWithSentinels = 0;

		for (int i = 0; i < oldLength; i++) {
			if (isOccupied(old[i])) {
				put(toNonSentinel(old[i]), oldValues[i]);
			}
		}
	}

	protected void allocateTable(final int sizeToAllocate) {
		keys = new Object[sizeToAllocate];
		values = new int[sizeToAllocate];
		Arrays.fill(values, no_value);
	}

	private int maxOccupiedWithData() {
		final int capacity = keys.length;
		// need at least one free slot for open addressing
		return Math.min(capacity - 1, capacity >> 1);
	}

	private static <K> boolean isOccupied(final K key) {
		return key != null && !isRemovedKey(key);
	}

	private static boolean isRemovedKey(final Object key) {
		return key == REMOVED_KEY;
	}

	@SuppressWarnings("unchecked")
	private K toNonSentinel(final Object key) {
		return key == NULL_KEY ? null : (K) key;
	}

	private static Object toSentinelIfNull(final Object key) {
		return key == null ? NULL_KEY : key;
	}

	private static final Object NULL_KEY = new Object() {

		@SuppressFBWarnings(value = "EQ_UNUSUAL", justification = "catches possible corruption.")
		@Override
		public boolean equals(final Object obj) {
			throw new RuntimeException("Possible corruption through unsynchronized concurrent modification.");
		}

		@Override
		public int hashCode() {
			throw new RuntimeException("Possible corruption through unsynchronized concurrent modification.");
		}

		@Override
		public String toString() {
			return "ObjectIntHashMap.NULL_KEY";
		}
	};

	private static final Object REMOVED_KEY = new Object() {

		@SuppressFBWarnings(value = "EQ_UNUSUAL", justification = "catches possible corruption.")
		@Override
		public boolean equals(final Object obj) {
			throw new RuntimeException("Possible corruption through unsynchronized concurrent modification.");
		}

		@Override
		public int hashCode() {
			throw new RuntimeException("Possible corruption through unsynchronized concurrent modification.");
		}

		@Override
		public String toString() {
			return "ObjectIntHashMap.REMOVED_KEY";
		}
	};

}
