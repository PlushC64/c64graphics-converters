package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import static de.plush.brix.c64graphics.core.util.Utils.toPositiveInt;
import static java.util.stream.Collectors.toSet;

import java.awt.Color;
import java.util.Set;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

public interface PaletteC64Full extends Palette {

	@Override
	default Color color(final byte colorCode) {
		return colors()[toPositiveInt(InvalidC64ColorCode.check(colorCode))];
	}

	@SuppressFBWarnings(value = "SA_LOCAL_SELF_COMPARISON", //
			justification = "False positive, clusters can implement a ColorClusterIndexProvider, but not be a ColorClusterIndexes enum.")
	@Override
	default Set<Set<Color>> colorClusters(final ColorClusterIndexProvider clusters) {
		if (clusters instanceof final ColorClusterIndexes cci) { // I hate to sy this, but I think Checkstyle is wrong here.
			switch (cci) {
			case VIC, VIC_II_lenient, VIC_II:
				return cci.colorIndexClusters().stream()//
						.map(bs -> bs.map(this::color).collect(toSet())//
						).collect(toSet());
			}
		}
		System.err.println("Unknown ColorClusterIndexProvider " + clusters + ". System may not find the correct color at the index provided.");
		return Palette.super.colorClusters(clusters);
	}
}
