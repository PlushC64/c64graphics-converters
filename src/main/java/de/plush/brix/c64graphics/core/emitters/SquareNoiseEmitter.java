package de.plush.brix.c64graphics.core.emitters;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;
import java.util.function.IntUnaryOperator;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

public class SquareNoiseEmitter extends AbstractGeneratedFramesEmitter {

	protected final Dimension dimension;

	private final IntUnaryOperator randomSeedGenerator;

	private final IntUnaryOperator numberOfRectangles;

	private final Dimension minRect;

	private final Dimension maxRect;

	private final Color background;

	private final Color foreground;

	public SquareNoiseEmitter(final int frames, final Dimension dimension, final IntUnaryOperator randomSeedGenerator, final IntUnaryOperator numberOfRectangles,
			final Dimension minRect, final Dimension maxRect, final Color background, final Color foreground) {
		super(frames);
		this.dimension = new Dimension(dimension);
		this.randomSeedGenerator = randomSeedGenerator;
		this.numberOfRectangles = numberOfRectangles;
		this.minRect = new Dimension(minRect);
		this.maxRect = new Dimension(maxRect);
		this.background = background;
		this.foreground = foreground;

	}

	@SuppressFBWarnings(value = "DMI_RANDOM_USED_ONLY_ONCE", //
			justification = "false positive, this random object is not used once, but in a for-loop.")
	@Override
	protected BufferedImage createImage(final int frameNr) {
		final BufferedImage image = new BufferedImage(dimension.width, dimension.height, BufferedImage.TYPE_INT_ARGB);
		final Graphics2D g = image.createGraphics();
		g.setColor(background);
		g.fillRect(0, 0, dimension.width, dimension.height);
		g.setColor(foreground);
		try {
			final var deltaW = maxRect.width - minRect.width;
			final var deltaH = maxRect.height - minRect.height;

			final var random = new Random(randomSeedGenerator.applyAsInt(frameNr));
			final var rectangles = numberOfRectangles.applyAsInt(frameNr);
			for (int t = 0; t < rectangles; ++t) {
				final int x = random.nextInt(dimension.width) - minRect.width;
				final int y = random.nextInt(dimension.height) - minRect.height;
				final int w = random.nextInt(deltaW) + minRect.width;
				final int h = random.nextInt(deltaH) + minRect.height;
				final var lr = random.nextBoolean();
				final var ud = random.nextBoolean();
				final var rect = new Rectangle(x, y, w, h);
				snapToGrid(lr, ud, rect);
				g.draw(rect);
			}
			return image;
		} finally {
			g.dispose();
		}
	}

	private static void snapToGrid(final boolean lr, final boolean ud, final Rectangle rect) {
		if (rect.x % 2 != 0) {
			rect.x = rect.x + (lr ? 1 : -1);
		}
		if ((rect.x - 1 + rect.width - 1) % 2 != 0) {
			rect.width = rect.width + (lr ? 1 : -1);
		}
		if (rect.y % 2 != 0) {
			rect.y = rect.y + (ud ? 1 : -1);
		}
		if ((rect.y - 1 + rect.height - 1) % 2 != 0) {
			rect.height = rect.height + (ud ? 1 : -1);
		}
	}
}
