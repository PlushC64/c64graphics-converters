/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces;

import java.awt.Color;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Represents a Color in the "Chroma - Luma" color space for ditital PAL/NTSC TV, JPEG/MPEG (DVD, VideoCD) according to
 * the ITU-R BT 601 Standard, aka CCIR-601.
 * 
 * Derived from digital RGB (8 bits per sample, each using the full range with zero representing black and 255 representing white)
 * 
 * @see <a href="https://en.wikipedia.org/wiki/YCbCr">see: https://en.wikipedia.org/wiki/YCbCr</a>
 * @see <a href="https://de.wikipedia.org/wiki/ITU-R_BT_601">see: https://de.wikipedia.org/wiki/ITU-R_BT_601</a>
 */
@SuppressFBWarnings(value = "NM_METHOD_NAMING_CONVENTION", //
		justification = "using the same variable casing as in the given formulas")
public record ColorYCbCr(double Y, double Cb, double Cr) {

	/**
	 * A constant for color black. Color components are:
	 * 
	 * <pre>
	 *     Y: 16.0
	 *     U: 128.0
	 *     V: 128.0
	 * </pre>
	 */
	public static final ColorYCbCr BLACK = ColorConversions.convertRGBtoYCbCrJPEG(Color.BLACK);

	/**
	 * A constant for color white. Color components are:
	 *
	 * <pre>
	 * Y: 236.0
	 * U: 128.0
	 * V: 128.0
	 * </pre>
	 */
	public static final ColorYCbCr WHITE = ColorConversions.convertRGBtoYCbCrJPEG(Color.WHITE);

	@SuppressWarnings("checkstyle:EqualsHashCode") // hashCode can be inherited in this special case
	@Override
	public boolean equals(final Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }

		final ColorYCbCr colorYUV = (ColorYCbCr) o;
		if (Double.compare(colorYUV.Y(), Y()) != 0) { return false; }
		if (Double.compare(colorYUV.Cb(), Cb()) != 0) { return false; }
		if (Double.compare(colorYUV.Cr(), Cr()) != 0) { return false; }

		return true;
	}

}
