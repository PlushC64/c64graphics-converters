/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.Color;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class BrightnessChange implements IPipelineStage<BufferedImage, BufferedImage> {

	private final ColorReplacement colorReplacement;

	/**
	 * Alter the brightness of the image.
	 * 
	 * @param factor
	 *            the factor to multiply the bright with. 0 = pitch black, 0.5 = half the brightness, 2.0 = double the brightness, etc.
	 */
	public BrightnessChange(final double factor) {
		colorReplacement = new ColorReplacement(color -> brightnessChange(factor, color));
	}

	private static Color brightnessChange(final double factor, final Color color) {
		final float[] hsb = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
		return Color.getHSBColor(hsb[0], hsb[1], (float) (hsb[2] * factor));
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		return colorReplacement.apply(original);
	}
}
