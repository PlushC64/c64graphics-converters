package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import static de.plush.brix.c64graphics.core.util.Utils.*;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toSet;

import java.awt.Color;
import java.util.*;

import de.plush.brix.c64graphics.core.util.OptionalByte;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

public interface Palette {
	Color[] colors();

	static <T extends Enum<T> & C64Color> Palette of(final Class<T> enumClass) {
		return enumClass.getEnumConstants()[0].palette();
	}

	default boolean contains(final Color c) {
		return asList(colors()).contains(c);
	}

	default OptionalByte colorCode(final Color color) {
		final int index = asList(colors()).indexOf(color);
		return index < 0 ? OptionalByte.empty() : OptionalByte.of(toUnsignedByte(index));
	}

	default byte colorCodeOrException(final Color color) {
		return colorCode(color).orElseThrow(() -> new IllegalArgumentException("Color " + color + " is not in palette."));
	}

	default Color color(final byte colorCode) {
		final var colors = colors();
		return colors[toPositiveInt(InvalidColorCode.check(colorCode, colors.length))];
	}

	default Palette d800MulticolorCharmodeSubPalette() {
		final Color[] colors = Arrays.copyOf(colors(), 8);
		return new Palette() {
			@Override
			public Color[] colors() {
				return Arrays.copyOf(colors, 8); // always return a copy -> keep it immutable!
			}
		};
	}

	default boolean canBeDisplayedInD800InMulticolorCharmode(final Color color) {
		return d800MulticolorCharmodeSubPalette().contains(color);
	}

	default boolean isMulticolorBitSetInD800colorCode(final byte colorCode) {
		return colorCode > 7;
	}

	default OptionalByte colorCodeWithMulticolorBitSet(final byte colorCode) {
		try {
			return OptionalByte.of(colorCodeWithMulticolorBitSetOrException(colorCode));
		} catch (final Exception e) {
			return OptionalByte.empty();
		}
	}

	default byte colorCodeWithMulticolorBitSetOrException(final byte colorCode) {
		final Color[] colors = colors();
		if (colorCode > 0x07) {
			throw new IllegalArgumentException(this + " cannot be displayed in color ram in multicolor charmode, it is displayed as " + colors[colorCode
					& 0b111] + ".");
		}
		return colorCodeOrException(colors[colorCode | 0b1000]);
	}

	default Color visibleColorInMulticolorCharmode(final byte colorCode) {
		final Color[] colors = colors();
		return colors[colorCode & 0b0111];
	}

	default Optional<Color> colorThatReplacesThisColorInD800MulticolorCharmode(final Color original) {
		return colorCode(original).flatMap(this::colorCodeWithMulticolorBitSet).mapToObj(this::color);
	}

	@SuppressFBWarnings(value = "DM_NEW_FOR_GETCLASS", //
			justification = "Spotbugs is right, but doesn't get that deriving an object in a method creates an anonymous class that features a 'getEnclosingMethod' result.")
	default Set<Set<Color>> colorClusters(final ColorClusterIndexProvider clusters) {
		try {
			return clusters.colorIndexClusters().stream()//
					.map(bs -> bs.map(this::color).collect(toSet()))//
					.collect(toSet());
		} catch (final InvalidColorCode e) {
			final var methodName = new Object() {/**/}.getClass().getEnclosingMethod().getName();
			System.err.println("ColorClusterIndexProvider " + clusters + " provided a color index that is not"//
					+ " found in this palette " + toString() + ": " + e.getMessage() + ". You might want to override the " + methodName
					+ " method of this palette.");
			return Set.of();
		}
	}

}
