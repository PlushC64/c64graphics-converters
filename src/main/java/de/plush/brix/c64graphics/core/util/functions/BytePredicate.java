package de.plush.brix.c64graphics.core.util.functions;

@FunctionalInterface
public interface BytePredicate {
	boolean accept(byte value);

	static BytePredicate negated(final BytePredicate predicate) {
		return (value) -> !predicate.accept(value);
	}
}
