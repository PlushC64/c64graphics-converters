package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.Palette;

public class PreProcessedCharacterImage extends BufferedImage {

	private final Palette palette;

	private final byte backgroundColorCode;

	private final byte mc1ColorCode;

	private final byte mc2ColorCode;

	private final byte d800ColorCode;

	public PreProcessedCharacterImage(final int width, final int height, final int imageType, final Palette palette, final byte backgroundColorCode,
			final byte mc1ColorCode, final byte mc2ColorCode, final byte d800ColorCode) {
		super(width, height, imageType);
		this.palette = palette;
		this.backgroundColorCode = backgroundColorCode;
		this.mc1ColorCode = mc1ColorCode;
		this.mc2ColorCode = mc2ColorCode;
		this.d800ColorCode = d800ColorCode;
	}

	public Palette palette() {
		return palette;
	}

	public byte backgroundColorCode() {
		return backgroundColorCode;
	}

	public byte mc1ColorCode() {
		return mc1ColorCode;
	}

	public byte mc2ColorCode() {
		return mc2ColorCode;
	}

	public byte d800ColorCode() {
		return d800ColorCode;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [backgroundColorCode=" + backgroundColorCode + ", mc1ColorCode=" + mc1ColorCode + ", mc2ColorCode="
				+ mc2ColorCode + ", d800ColorCode=" + d800ColorCode + "]";
	}

}