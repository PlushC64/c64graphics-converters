package de.plush.brix.c64graphics.core.pipeline.displays;

import java.awt.Image;
import java.util.function.Supplier;

import javax.swing.JToolTip;

class ImageToolTip extends JToolTip {
	public ImageToolTip(final Supplier<Image> image) {
		setUI(new ImageToolTipUI(image));
	}
}
