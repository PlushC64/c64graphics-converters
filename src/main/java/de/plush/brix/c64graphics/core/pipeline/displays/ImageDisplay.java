/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.displays;

import static java.lang.Math.round;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.image.*;
import java.lang.reflect.InvocationTargetException;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.function.*;

import javax.swing.*;

import de.plush.brix.c64graphics.core.emitters.ImageEmitter;
import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.ImageConsumer;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class ImageDisplay<T extends Image> implements ImageConsumer<T>, IPipelineStage<T, T> {

	/** Grids of common 64 graphic blocks */
	public enum Grid {
		Invisible(null)
		// common charset sizes:
		, sizeC64Char(new Dimension(C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS))//
		, sizeC64Char1x2(new Dimension(1 * C64Char.WIDTH_PIXELS, 2 * C64Char.HEIGHT_PIXELS))//
		, sizeC64Char2x1(new Dimension(2 * C64Char.WIDTH_PIXELS, 1 * C64Char.HEIGHT_PIXELS))//
		, sizeC64Char2x2(new Dimension(2 * C64Char.WIDTH_PIXELS, 2 * C64Char.HEIGHT_PIXELS))//
		, sizeC64Char2x3(new Dimension(2 * C64Char.WIDTH_PIXELS, 3 * C64Char.HEIGHT_PIXELS))//
		, sizeC64Char3x2(new Dimension(3 * C64Char.WIDTH_PIXELS, 2 * C64Char.HEIGHT_PIXELS))//
		, sizeC64Char3x3(new Dimension(3 * C64Char.WIDTH_PIXELS, 3 * C64Char.HEIGHT_PIXELS))//
		, sizeC64Char3x4(new Dimension(3 * C64Char.WIDTH_PIXELS, 4 * C64Char.HEIGHT_PIXELS))//
		, sizeC64Char4x3(new Dimension(4 * C64Char.WIDTH_PIXELS, 3 * C64Char.HEIGHT_PIXELS))//
		, sizeC64Char4x4(new Dimension(4 * C64Char.WIDTH_PIXELS, 4 * C64Char.HEIGHT_PIXELS))//
		// common canvas sizes (symmetric blocks of 256 chars)
		, sizeC64Char16x16(new Dimension(16 * C64Char.WIDTH_PIXELS, 16 * C64Char.HEIGHT_PIXELS))//
		, sizeC64Char32x8(new Dimension(32 * C64Char.WIDTH_PIXELS, 8 * C64Char.HEIGHT_PIXELS))//
		// common sprite sizes (logos, strips)
		, sizeC64Sprite(new Dimension(C64Sprite.WIDTH_PIXELS, C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite1x2(new Dimension(1 * C64Sprite.WIDTH_PIXELS, 2 * C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite1x3(new Dimension(1 * C64Sprite.WIDTH_PIXELS, 3 * C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite1x4(new Dimension(1 * C64Sprite.WIDTH_PIXELS, 4 * C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite1x5(new Dimension(1 * C64Sprite.WIDTH_PIXELS, 5 * C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite1x6(new Dimension(1 * C64Sprite.WIDTH_PIXELS, 6 * C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite1x7(new Dimension(1 * C64Sprite.WIDTH_PIXELS, 7 * C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite1x8(new Dimension(1 * C64Sprite.WIDTH_PIXELS, 8 * C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite2x1(new Dimension(2 * C64Sprite.WIDTH_PIXELS, 1 * C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite2x2(new Dimension(2 * C64Sprite.WIDTH_PIXELS, 2 * C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite2x3(new Dimension(2 * C64Sprite.WIDTH_PIXELS, 3 * C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite2x4(new Dimension(2 * C64Sprite.WIDTH_PIXELS, 4 * C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite3x1(new Dimension(3 * C64Sprite.WIDTH_PIXELS, 1 * C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite3x2(new Dimension(3 * C64Sprite.WIDTH_PIXELS, 2 * C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite3x3(new Dimension(3 * C64Sprite.WIDTH_PIXELS, 3 * C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite4x1(new Dimension(4 * C64Sprite.WIDTH_PIXELS, 1 * C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite4x2(new Dimension(4 * C64Sprite.WIDTH_PIXELS, 2 * C64Sprite.HEIGHT_PIXELS))//
		, sizeC64Sprite8x1(new Dimension(8 * C64Sprite.WIDTH_PIXELS, 1 * C64Sprite.HEIGHT_PIXELS))//
		;

		private final Dimension d;

		Grid(final Dimension d) {
			this.d = d;
		}

		void paint(final Graphics2D g, final int width, final int height, final float zoom, final int _n) {
			if (this != Invisible) {
				g.setColor(new Color(0, 255, 0, 180));
				int nY = _n;
				for (float y = 0; y < height; y += d.height * zoom) {
					nY ^= 0x7F_FFFF; // flip lower 32 bits (direction) every line
					// vary stroke with each line, to have less global flicker/pattern effect (easier on theeyes)
					g.setStroke(new BasicStroke(1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 1f, new float[] { 11f, 5.3f }, nY + y + 11));
					g.drawLine(0, round(y), width, round(y));
				}

				int nX = _n;
				for (float x = 0; x < width; x += d.width * zoom) {
					if (round(x) % 3 == 0) { // change direction every third line
						nX ^= 0x7F_FFFF; // flip lower 32 bits
					}
					g.setStroke(new BasicStroke(1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 1f, new float[] { 9f, 5f }, nX + x + 13));
					g.drawLine(round(x), 0, round(x), height);
				}
			}
		}
	}

	private volatile JFrame frame;

	private final float zoom;

	private final String title;

	private Grid grid;

	public ImageDisplay() {
		this(1, null, Grid.Invisible, null);
	}

	public ImageDisplay(final Consumer<AutoCloseable> closerRegistry) {
		this(1, null, Grid.Invisible, closerRegistry);
	}

	public ImageDisplay(final String title) {
		this(1, title);
	}

	public ImageDisplay(final String title, final Consumer<AutoCloseable> closerRegistry) {
		this(1f, title, Grid.Invisible, closerRegistry);
	}

	public ImageDisplay(final String title, final Grid grid, final Consumer<AutoCloseable> closerRegistry) {
		this(1f, title, grid, closerRegistry);
	}

	public ImageDisplay(final float zoom) {
		this(zoom, null, Grid.Invisible, null);
	}

	public ImageDisplay(final float zoom, final Grid grid) {
		this(zoom, null, grid, null);
	}

	public ImageDisplay(final float zoom, final Consumer<AutoCloseable> closerRegistry) {
		this(zoom, null, Grid.Invisible, closerRegistry);
	}

	public ImageDisplay(final float zoom, final Grid grid, final Consumer<AutoCloseable> closerRegistry) {
		this(zoom, null, grid, closerRegistry);
	}

	public ImageDisplay(final float zoom, final String title) {
		this(zoom, title, Grid.Invisible, null);
	}

	public ImageDisplay(final float zoom, final String title, final Grid grid) {
		this(zoom, title, grid, null);
	}

	public ImageDisplay(final float zoom, final String title, final Consumer<AutoCloseable> closerRegistry) {
		this(zoom, title, Grid.Invisible, closerRegistry);
	}

	public ImageDisplay(final float zoom, final String title, final Grid grid, final Consumer<AutoCloseable> closerRegistry) {
		this.zoom = zoom;
		this.title = title;
		this.grid = grid;
		if (closerRegistry != null) {
			closerRegistry.accept(this);
		}

		if (GraphicsEnvironment.isHeadless()) {
			System.err.println("GraphicsEnvironment is headless: Not showing graphics.");
			return;
		}
	}

	@Override
	public void close() {
		dispose();
	}

	public void dispose() {
		if (frame != null) {
			frame.dispose();
		}
		frame = null;
	}

	/**
	 * Shows the image. <br>
	 * This is to implement the {@link ImageConsumer} interface, so this display can be added to an {@link ImageEmitter} via
	 * {@link ImageEmitter#add(ImageConsumer)}.
	 */
	@Override
	public void onImage(final Image image) {
		show(image, "");
	}

	/**
	 * Shows and returns the image. <br>
	 * This is so implement the {@link Function} interface, which is a handy way to insert this display into pipelines built using
	 * {@link Function#andThen(Function)}. From a functional point of view this method works like {@link Function#identity}.
	 */
	@Override
	public T apply(final T image) {
		show(image, "");
		return image;
	}

	/**
	 * Shows the image and overrides the title.<br>
	 * This can be used, if it is necessary to provide some ad-hoc extra information, like a frame counter.
	 */
	@SuppressWarnings("unchecked")
	public void show(final Image image, final String title) {
		if (GraphicsEnvironment.isHeadless()) { return; }

		if (frame != null) {
			((ImagePanel) frame.getContentPane()).updateImage(image);
		} else {
			try {
				EventQueue.invokeAndWait(new Runnable() {
					@Override
					public void run() {
						@SuppressWarnings("serial")
						final JPanel imagePanel = new ImagePanel(image, zoom, grid);
						frame = new JFrame(ImageDisplay.this.title != null ? ImageDisplay.this.title : title);
						frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
						frame.setContentPane(imagePanel);
						frame.pack();
						frame.setVisible(true);
					}
				});
			} catch (

			final InterruptedException e) {
				e.printStackTrace();
			} catch (

			final InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}

	public static final class ImagePanel extends JPanel {
		private Supplier<Image> image;

		private final float zoom;

		private final Grid grid;

		int n; // some rolling timing value to mae the grid glow.

		private JToolTip currentToolTip;

		private Dimension zoomWindowPixels = new Dimension(10, 10);

		private float tooltipExtraZoom = 3;

		public ImagePanel(final Image image) {
			this(image, 1);
		}

		public ImagePanel(final Image image, final float zoom) {
			this(image, zoom, Grid.Invisible);
		}

		public ImagePanel(final Image image, final Grid grid) {
			this(image, 1, grid);
		}

		public ImagePanel(final Image image, final float zoom, final Grid grid) {
			this(() -> image, zoom, grid);
		}

		public ImagePanel(final Supplier<Image> imageSupplier) {
			this(imageSupplier, 1f, Grid.Invisible);
		}

		public ImagePanel(final Supplier<Image> imageSupplier, final float zoom) {
			this(imageSupplier, zoom, Grid.Invisible);
		}

		public ImagePanel(final Supplier<Image> imageSupplier, final Grid grid) {
			this(imageSupplier, 1, grid);
		}

		public ImagePanel(final Supplier<Image> imageSupplier, final float zoom, final Grid grid) {
			image = imageSupplier;
			this.zoom = zoom;
			this.grid = grid;

			if (GraphicsEnvironment.isHeadless()) {
				System.err.println("GraphicsEnvironment is headless: Not showing graphics.");
				return;
			}

			// if (grid != Grid.Invisible) {
			new java.util.Timer("ImageDisplay tooltip refresh timer", true).scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					n = (int) (System.currentTimeMillis() / 100 & 0x7F_FFFF); // avoid negative, use just 23 bits (float fraction)
					repaint();
					final JToolTip tt = currentToolTip; // don't query twice (race condition)
					if (tt != null) {
						tt.repaint();
					}
				}
			}, 0L, 50);
			// }
			ToolTipManager.sharedInstance().setDismissDelay((int) TimeUnit.SECONDS.toMillis(30));
			ToolTipManager.sharedInstance().setInitialDelay(0);
			ToolTipManager.sharedInstance().setReshowDelay(0);
			ToolTipManager.sharedInstance().setLightWeightPopupEnabled(true);
			setToolTipText("..."); // just to activate tooltip, text is recreated dynamically anyway
		}

		public void updateImage(final Image image) {
			this.image = () -> image;
			if (GraphicsEnvironment.isHeadless()) { return; }
			repaint();
		}

		/**
		 * Sets the extra zoom for tool tip's image.
		 * Use <tt>1f</tt> for no extra zoom.
		 * 
		 * @param tooltipExtraZoom
		 *            default: <tt>3f</tt>.
		 */
		public ImagePanel withTooltipExtraZoom(final float tooltipExtraZoom) {
			this.tooltipExtraZoom = tooltipExtraZoom;
			return this;
		}

		/**
		 * Sets how many pixels of the unzoomed image the tool tip image should show.
		 * These pixels will be zoomed according to the overall zoom settings
		 * plus the tool tip's {@link #withTooltipExtraZoom(float) extra zoom}.
		 * 
		 * @param zoomWindowPixels
		 *            default: <tt>Dimension(10,10)</tt>.
		 */
		public ImagePanel withTooltipShowingZoomedPixels(final Dimension zoomWindowPixels) {
			this.zoomWindowPixels = new Dimension(zoomWindowPixels);
			return this;
		}

		private Image image() {
			return image.get();
		}

		@Override
		protected void paintComponent(final Graphics _g) {
			super.paintComponent(_g);
			final Graphics2D g = (Graphics2D) _g.create();
			final ImageObserver waitingForImageLoad = this;
			try {
				// shift painting of image and grid to respect border.:
				final var border = getInsets();
				g.translate(border.left, border.top);
				final int zoomedWidth = (int) (image().getWidth(waitingForImageLoad) * zoom);
				final int zoomedHeight = (int) (image().getHeight(waitingForImageLoad) * zoom);
				g.drawImage(image(), 0, 0, zoomedWidth, zoomedHeight, waitingForImageLoad);
				grid.paint(g, zoomedWidth, zoomedHeight, zoom, n);
			} finally {
				g.dispose();
			}
		}

		@Override
		public Dimension getMinimumSize() {
			return zoomedSizeWithBorders();
		}

		@Override
		public Dimension getPreferredSize() {
			return zoomedSizeWithBorders();
		}

		@Override
		public Dimension getMaximumSize() {
			return zoomedSizeWithBorders();
		}

		private Dimension zoomedSizeWithBorders() {
			final ImageObserver waitingForImageLoad = this;
			final var border = getInsets();
			return new Dimension(round(image().getWidth(waitingForImageLoad) * zoom) + border.left + border.right//
					, round(image().getHeight(waitingForImageLoad) * zoom) + border.top + border.bottom);
		}

		@Override
		public JToolTip createToolTip() {
			try {
				final Robot robot = new Robot();
				return currentToolTip = new ImageToolTip(() -> createToolTipImage(robot));
			} catch (final AWTException e) {
				currentToolTip = null;
				return super.createToolTip();
			}
		}

		@Override
		public Point getToolTipLocation(final MouseEvent event) {
			final Point point = event.getPoint();
			point.translate((int) (10 * zoom), (int) (5 * zoom));
			return point;
		}

		private Image createToolTipImage(final Robot robot) {
			// final long t = System.nanoTime();
			// final var waitForImageLoad = this;
			// final Dimension zoomedPixels = new Dimension(//
			// (int) min(image().getWidth(waitForImageLoad) * zoom, zoomWindowPixels.width * zoom) //
			// , (int) min(image().getHeight(waitForImageLoad) * zoom, zoomWindowPixels.height * zoom)//
			// );
			final Dimension zoomedPixels = new Dimension(//
					(int) (zoomWindowPixels.width * zoom) //
					, (int) (zoomWindowPixels.height * zoom)//
			);
			final Point location = MouseInfo.getPointerInfo().getLocation();
			location.translate(-zoomedPixels.width / 2, -zoomedPixels.height / 2);
			final BufferedImage image = robot.createScreenCapture(new Rectangle(location, zoomedPixels));
			final Dimension zoomed = new Dimension((int) (zoomedPixels.width * tooltipExtraZoom), (int) (zoomedPixels.height * tooltipExtraZoom));
			final BufferedImage zoomedmage = new BufferedImage(zoomed.width, zoomed.height, BufferedImage.TYPE_INT_RGB);
			final Graphics2D g = zoomedmage.createGraphics();
			try {
				g.drawImage(image, 0, 0, zoomed.width, zoomed.height, null);
				g.setColor(Color.GREEN);
				final int crossHair = 5;
				g.drawLine(zoomed.width / 2 - crossHair, zoomed.height / 2 - crossHair, //
						zoomed.width / 2 + crossHair, zoomed.height / 2 + crossHair);
				g.drawLine(zoomed.width / 2 - crossHair, zoomed.height / 2 + crossHair, //
						zoomed.width / 2 + crossHair, zoomed.height / 2 - crossHair);
				// System.out.println(TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - t));
				return zoomedmage;
			} finally {
				g.dispose();
			}
		}

		@Override
		public String getToolTipText(final MouseEvent event) {
			final var ttt = createToolTipText();
			putClientProperty(TOOL_TIP_TEXT_KEY, ttt);
			return ttt;
		}

		private String createToolTipText() {
			final var mouseOnScreen = MouseInfo.getPointerInfo().getLocation();
			final var mouseOnPanel = new Point(mouseOnScreen);
			SwingUtilities.convertPointFromScreen(mouseOnPanel, this);
			String colorString = "";
			try {
				colorString += new Robot().getPixelColor(mouseOnScreen.x, mouseOnScreen.y);
				colorString = colorString.substring(Color.class.getPackageName().length() + 1);
			} catch (final AWTException e) {
				// ignore
			}
			final Point pPix = componentToPixelPos(mouseOnPanel, zoom);
			final Point pBlock = pixelToBlockPos(pPix, C64Char.sizePixels());
			return "PixelPos: x=" + pPix.x + ", y = " + pPix.y//
					+ "\n" + "BlockPos: x=" + pBlock.x + ", y = " + pBlock.y//
					+ "\n" + colorString;
		}

		private static Point componentToPixelPos(final Point p, final float zoom) {
			final int pixX = (int) (p.x / zoom);
			final int pixY = (int) (p.y / zoom);
			return new Point(pixX, pixY);
		}

		private static Point pixelToBlockPos(final Point pPix, final Dimension blockDimensions) {
			final int blockX = pPix.x / blockDimensions.width;
			final int blockY = pPix.y / blockDimensions.height;
			return new Point(blockX, blockY);
		}
	}

}