package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import static java.lang.Math.min;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class HiddenLineHorizontal implements IPipelineStage<BufferedImage, BufferedImage> {

	private final double[][] kernel;

	private final Dimension kernelDimension;

	private final int linegap;

	private final int rise;

	/**
	 * @param kernelDimension
	 *            a dimension of odd width and height for blurring, e.g. 3x3 or 5x5, denotes the area of pixels around an pixels that is used to determine its
	 *            color.
	 * @param sigma
	 *            Blurriness factor, e.g. 1.5. The larger the blurriness factor, the more it makes sense to have a larger kernel dimension
	 * @param linegap
	 *            a number &gt;= 0
	 * 
	 */
	public HiddenLineHorizontal(final Dimension kernelDimension, final double sigma, final int linegap, final int rise) {
		if (linegap < 0) { throw new IllegalArgumentException("parameter linegap " + linegap + " < 0, must be >= 0"); }
		if (rise < 0) { throw new IllegalArgumentException("parameter rise " + rise + " < 0, must be >= 0"); }
		kernel = createKernel(kernelDimension, sigma);
		this.linegap = linegap;
		this.kernelDimension = new Dimension(kernelDimension);
		this.rise = rise;
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());
		final var g = result.createGraphics();
		try {
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, original.getWidth(), original.getHeight());

			final int xKernelOffset = kernelDimension.width / 2;
			final int yKernelOffset = kernelDimension.height / 2;

			final var minY = new int[original.getWidth()]; // x can safely run in parallel, y needs to be sequential
			Arrays.parallelSetAll(minY, any -> Integer.MAX_VALUE);

			IntStream.range(0, original.getWidth()).parallel().forEach(xCenter -> {
				for (int yCenter = original.getHeight() - 1; yCenter >= 0; yCenter -= linegap + 1) {
					int sumR = 0;
					int sumG = 0;
					int sumB = 0;
					for (int xKernelRelative = -xKernelOffset; xKernelRelative <= xKernelOffset; ++xKernelRelative) {
						for (int yKernelRelative = -yKernelOffset; yKernelRelative <= yKernelOffset; ++yKernelRelative) {
							final int xKernel = xKernelRelative + xKernelOffset;
							final int yKernel = yKernelRelative + yKernelOffset;
							final double weight = kernel[xKernel][yKernel];

							final int x = xCenter + (outOfBounds(original.getWidth(), xCenter, xKernelRelative) ? -xKernelRelative : xKernelRelative);
							final int y = yCenter + (outOfBounds(original.getHeight(), yCenter, yKernelRelative) ? -yKernelRelative : yKernelRelative);
							final Color color = new Color(original.getRGB(x, y));
							sumR += color.getRed() * weight;
							sumG += color.getGreen() * weight;
							sumB += color.getBlue() * weight;
						}
					}
					final var col = new Color(sumR, sumG, sumB);



					final int y = yCenter - (int) ((col.getRed() + col.getGreen() + col.getBlue()) / (3 * 255.0) * rise);
					if (y < minY[xCenter] && y >= 0) {
						result.setRGB(xCenter, y, Color.BLACK.getRGB());
					}
					minY[xCenter] = min(minY[xCenter], y);

				}
			});
			return result;
		} finally {
			g.dispose();
		}
	}

	private static boolean outOfBounds(final int max, final int center, final int offset) {
		return center + offset < 0 || center + offset > max - 1;
	}

	/**
	 * Creates a Gaussian kernel.<br>
	 *
	 * @see <a href=
	 *      "http://www.swageroo.com/wordpress/how-to-program-a-gaussian-blur-without-using-3rd-party-libraries/">http://www.swageroo.com/wordpress/how-to-program-a-gaussian-blur-without-using-3rd-party-libraries/</a>
	 *
	 * @param sigma
	 *            Blurriness factor, e.g. 1.5
	 * @param kernelDimension
	 *            Width and Height of kernel in pixels
	 * @return A matrix of weights to apply to neighboring pixels. Format is double[x][y]
	 */
	public static double[][] createKernel(final Dimension kernelDimension, final double sigma) {
		checkKernelDimensionsAreOdd(kernelDimension);

		final double[][] result = new double[kernelDimension.width][kernelDimension.height];
		final int xoffset = kernelDimension.width / 2;
		final int yoffset = kernelDimension.height / 2;

		double sum = 0; // used to normalize later
		final double base = 1 / (2 * Math.PI * sigma * sigma); // height of peak
		// Calculate all the weights
		for (int y = -yoffset; y <= yoffset; ++y) {
			for (int x = -xoffset; x <= xoffset; ++x) {
				final double weight = gauss(x, y, base, sigma);
				result[x + xoffset][y + yoffset] = weight;
				sum += weight;
			}
		}

		// Normalize all the weights so the sum is 1
		for (int x = 0; x < kernelDimension.width; ++x) {
			for (int y = 0; y < kernelDimension.height; ++y) {
				result[x][y] = result[x][y] * (1d / sum);
			}
		}
		return result;
	}

	private static void checkKernelDimensionsAreOdd(final Dimension kernelDimension) {
		if (kernelDimension.width % 2 == 0) {
			throw new IllegalArgumentException("Kernel dimension width (" + kernelDimension.width + ") is not an odd number");
		}
		if (kernelDimension.height % 2 == 0) {
			throw new IllegalArgumentException("Kernel dimension height (" + kernelDimension.height + ") is not an odd number");
		}
	}

	/**
	 *
	 * @param x
	 *            x-coordinate (-n to n)
	 * @param y
	 *            y-coordinate (-n to n)
	 * @param baseHeight
	 *            height of the bell curve's peak
	 * @param sigma
	 *            the base RMS value, that stretches the curve's width, (e.g.: 0.2 for a steep curve, 5.0 for a very flat one)
	 * @see: https://dzone.com/articles/gaussian-function
	 * @return the height of the gaussian plane at the respective x/y coordinate
	 */
	private static double gauss(final int x, final int y, final double baseHeight, final double sigma) {
		final double distanceSq = x * x + y * y;
		/*
		 * This is the "complicated" way to do it:
		 *
		 * final double centerCurve = 0.0; //@formatter:off
		 * final double distanceFromCenter = Math.sqrt(distanceSq);
		 * final double xMinusB = Math.pow(distanceFromCenter - centerCurve, 2);
		 * final double weight = base * Math.exp(-(xMinusB / (2 * sigma * sigma))); //@formatter:on
		 *
		 * however, we can replace the pow(sqrt(distanceSq)-centerCurve),2) with distanceSq, since the center (peak) of the bell curve is at 0:
		*/
		final double weight = baseHeight * Math.exp(-(distanceSq / (2 * sigma * sigma)));
		return weight;
	}

}
