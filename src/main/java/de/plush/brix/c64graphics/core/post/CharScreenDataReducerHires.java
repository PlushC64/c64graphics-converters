package de.plush.brix.c64graphics.core.post;

import static de.plush.brix.c64graphics.core.util.Utils.subsetStream;
import static java.util.stream.Collectors.toCollection;

import java.util.*;
import java.util.stream.*;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.util.charpixeldistance.C64CharHiresPixelDistanceMeasure;
import de.plush.brix.c64graphics.core.util.collections.HashBag;

public class CharScreenDataReducerHires extends AbstractCharScreenDataReducerHires {

	private static final double BLACKLIST_LOAD_FACTOR = 0.80;

	public CharScreenDataReducerHires(final C64CharHiresPixelDistanceMeasure distanceMeasure, final Prefer impact) {
		this(distanceMeasure, impact, new C64Charset());
	}

	protected CharScreenDataReducerHires(final C64CharHiresPixelDistanceMeasure distanceMeasure, final Prefer impact, final C64Charset protectedChars) {
		super(distanceMeasure, impact, protectedChars);
	}

	public CharScreenDataReducerHires withProtectedChars(final C64Charset protectedChars) {
		return new CharScreenDataReducerHires(distanceMeasure, impact, protectedChars);
	}

	public CharScreenDataReducerHires withProtectedChars(final C64Char... chars) {
		return new CharScreenDataReducerHires(distanceMeasure, impact, new C64Charset(chars));
	}

	@SuppressWarnings("checkstyle:CyclomaticComplexity") // found no meaningful way to extract methods that make it more readable
	@Override
	public C64CharsetMultiScreen reduce(final Collection<C64CharsetScreen> _charScreens) {
		// make sure the original data is not changed:
		final Collection<C64CharsetScreen> charScreens = _charScreens.stream().map(C64CharsetScreen::new).collect(Collectors.toList());

		final Map<C64Char, Locator> chrToLocations = allCharacterLocations(charScreens);
		System.out.println("chrToLocations size " + chrToLocations.size());
		final HashBag<C64Char> occurrenceCounts = occurrenceCounts(chrToLocations);
		if (chrToLocations.size() > C64Charset.MAX_CHARACTERS) {
			System.out.println("building chrToImageMap...");
			final Map<C64Char, PreProcessedCharacterImage> chrToImageMap = createChrToImageMap(chrToLocations.keySet());
			System.out.println("reducing...");
			do {
				final Set<C64Char> victimBlacklist = new HashSet<>();
				protectedChars.addAllCharsTo(victimBlacklist);
				System.out.println("new blacklist");

				final List<C64Char> topOccurring = new ArrayList<>();
				// final int percentile = 0;
				int percentile = (int) (BLACKLIST_LOAD_FACTOR * 100) / 2; // the blacklist should have some leeway before overloading
				var topPercentile = occurrenceCounts.topOccurrences(chrToLocations.size() * percentile / 100);
				// to make sure that when every character is equally likely, we don't block every one:
				while (topPercentile.size() >= chrToLocations.size()) {
					percentile = percentile * 2 / 3;
					topPercentile = occurrenceCounts.topOccurrences(chrToLocations.size() * percentile / 100);
				}
				System.out.println("top percentile for blacklist: " + percentile);
				topPercentile.forEach(oip -> topOccurring.add(oip.objValue()));
				victimBlacklist.addAll(topOccurring);
				System.out.println("most frequent characters not considered for replacement: " + victimBlacklist.size());
				do {
					final List<VictimToSurrogateMapping> queue = buildQueue(chrToImageMap, victimBlacklist, occurrenceCounts);
					System.out.println("built queue of size: " + queue.size());
					// We now have a couple of items in the queue sorted from best to worst match.
					// Note that there could be better matches among more frequent items, but to keep the overall impact low
					// a worse match that is rare is preferred to a better match that is very common.
					do {
						// the reason why we don't substitute the whole queue is that
						// there could be way better, almost equally rare matches higher up the occurrence count
						// So we eventually want to pull in new candidates.

						// for some (not all) items of the queue:
						final VictimToSurrogateMapping substitution = pickFromQueue(queue, chrToLocations, victimBlacklist);
						final Locator victimLocator = chrToLocations.remove(substitution.victim);

						if (victimLocator == null) {
							System.out.println("no locations found for victim char:\n" + substitution.victim);
							statsForChar(charScreens, chrToLocations, queue, occurrenceCounts, chrToImageMap, substitution.victim);
						} else {
							replaceCharacterEverywhere(substitution, victimLocator, chrToLocations);
							cleanupCollections(substitution, chrToImageMap, victimLocator, victimBlacklist, occurrenceCounts);
						}
						if (chrToLocations.size() % 10 == 0) {
							// while running, the occurrence counts change. Once in a while sort the queue anew to cater for
							// the changed occurrence numbers
							// queue.sort(substitutionOrder(occurrenceCounts));
							System.out.print('.');
						}
					} while (chrToLocations.size() > C64Charset.MAX_CHARACTERS //
							&& chrToLocations.size() > victimBlacklist.size() / BLACKLIST_LOAD_FACTOR//
							&& !queue.isEmpty()); //
					System.out.println("characters: " + chrToLocations.size());
					System.out.println("blacklist overloaded or queue drained.");
				} while (chrToLocations.size() > C64Charset.MAX_CHARACTERS //
						&& chrToLocations.size() > victimBlacklist.size() / BLACKLIST_LOAD_FACTOR); //
				System.out.println("blacklist overloaded.");
			} while (chrToLocations.size() > C64Charset.MAX_CHARACTERS); // still too many characters? replace some!
			System.out.println("finished.");
		}

		System.out.println("characters: " + chrToLocations.size());
		final C64Charset charset = reconstructCharset(chrToLocations);
		final C64Screen[] c64Screens = reconstructScreens(chrToLocations, charScreens, charset);
		return new C64CharsetMultiScreen(charset, c64Screens);
	}

	@Override
	protected List<VictimToSurrogateMapping> buildQueue(final Map<C64Char, PreProcessedCharacterImage> chrToImageMap, final Set<C64Char> victimBlacklist,
			final HashBag<C64Char> occurrenceCounts) {
		final List<VictimToSurrogateMapping> queue = subsetStream(chrToImageMap.keySet(), 2, ArrayList::new)//
				.parallel()//
				.flatMap(pair -> {
					final var a = pair.get(0);
					final var b = pair.get(1);
					final PreProcessedCharacterImage imageA = chrToImageMap.get(a);
					final PreProcessedCharacterImage imageB = chrToImageMap.get(b);
					final double weight = distanceMeasure.distanceOf(a, b);
					return Stream.of(new VictimToSurrogateMapping(a, b, imageB, weight), new VictimToSurrogateMapping(b, a, imageA, weight));
				})//
				.filter(vso -> !victimBlacklist.contains(vso.victim))//
				.sorted(substitutionOrder(occurrenceCounts, impact)) //
				.collect(toCollection(ArrayList::new)); // queue is sorted, so the last item is the best recplacement
		return queue;
	}

}
