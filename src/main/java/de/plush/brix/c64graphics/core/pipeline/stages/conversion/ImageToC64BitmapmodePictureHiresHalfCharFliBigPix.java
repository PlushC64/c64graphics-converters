package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.*;

import java.awt.*;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.dithering.Dithering;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.*;

/**
 * Fixed hires Bitmap, 2 screens (FLI-effect every 4 lines).<br>
 * 
 * <pre>
 * Each 8x8-pixel bitmap block looks like this:
 * <code>
 * 00001111 (colors from screen 1)
 * 00001111 ...
 * 00001111 ...
 * 00001111 ...
 * 00001111 (colors from screen 2)
 * 00001111 ...
 * 00001111 ...
 * 00001111 ...</code>
 * </pre>
 * 
 * @author Wanja Gayk
 */
public class ImageToC64BitmapmodePictureHiresHalfCharFliBigPix implements IPipelineStage<BufferedImage, C64BitmapmodePictureHiresHalfcharFli> {

	private final Palette palette;
	private final PaletteConversion paletteConversion;

	/**
	 * @param palette
	 *            the C64 target palette
	 */
	public ImageToC64BitmapmodePictureHiresHalfCharFliBigPix(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure) {
		this.palette = palette;
		paletteConversion = new PaletteConversion(palette, colorDistanceMeasure, Dithering.None);
	}

	@Override
	public C64BitmapmodePictureHiresHalfcharFli apply(final BufferedImage original) {
		final var bitmap = new C64Bitmap(new C64Char(new byte[] { //
				(byte) 0b00001111, //
				(byte) 0b00001111, //
				(byte) 0b00001111, //
				(byte) 0b00001111, //
				// ------------------------
				(byte) 0b00001111, //
				(byte) 0b00001111, //
				(byte) 0b00001111, //
				(byte) 0b00001111,//
		}));
		final var screens = new C64Screen[] { new C64Screen((byte) 0), new C64Screen((byte) 0) };

		final Dimension targetSize4PixelPerBlock = new Dimension(original.getWidth() / C64Char.WIDTH_PIXELS * 4,
				original.getHeight() / C64Char.HEIGHT_PIXELS * 4);
		final BufferedImage shrinkedImage = new Rescale(targetSize4PixelPerBlock, () -> Color.BLACK).apply(original);
		final BufferedImage shrinkedImageWithAppliedPalette = paletteConversion.apply(shrinkedImage);
		for (int yPix = 0; yPix < shrinkedImageWithAppliedPalette.getHeight(); ++yPix) {
			for (int xPix = 0; xPix < shrinkedImageWithAppliedPalette.getWidth(); ++xPix) {
				final int xBlock = xPix / 4, yBlock = yPix / 4;
				final boolean foregroundColor = xPix % 2 != 0;
				final var screen = yPix % 2 == 0 ? screens[0] : screens[1];
				final Color color = new Color(shrinkedImageWithAppliedPalette.getRGB(xPix, yPix));
				final int colorCode = toPositiveInt(palette.colorCodeOrException(color));
				screen.modifyScreenCodeAt(xBlock, yBlock, screenCode -> toUnsignedByte(screenCode //
						& (foregroundColor ? 0b11110000 : 0b00001111) //
						| (foregroundColor ? colorCode << 4 : colorCode)));
			}
		}
		return new C64BitmapmodePictureHiresHalfcharFli(bitmap, screens);
	}

	// TODO: Create Example
}
