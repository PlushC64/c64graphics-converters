package de.plush.brix.c64graphics.core.pipeline.stages.misc;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.*;

import de.plush.brix.c64graphics.core.util.UtilsImage;
import de.plush.brix.c64graphics.core.util.UtilsImage.ImageFormat;

public class WriteImageToFile<T extends BufferedImage> implements Function<T, T> {

	private final AtomicInteger number = new AtomicInteger();

	private final ImageFormat format;

	private final IntFunction<String> fileNameGenerator;

	/**
	 * @param fileNameGenerator
	 *            a function that maps the current binary provider's number to a path. <br/>
	 *            The function's parameter value starts with <tt>0</tt> and increases with every call to @link {@link #apply(BufferedImage)}.
	 */
	public WriteImageToFile(final UtilsImage.ImageFormat format, final IntFunction<String> fileNameGenerator) {
		this.format = format;
		this.fileNameGenerator = fileNameGenerator;
	}

	/**
	 * Writes the file and returns the parameter value.
	 */
	@Override
	public T apply(final T image) {
		final int n = number.getAndIncrement();
		final String filePath = fileNameGenerator.apply(n);
		UtilsImage.saveImage(image, format, new File(filePath));
		return image;
	}

	public void resetNumber() {
		while (number.compareAndSet(number.get(), 0)) {
			Thread.yield();
		}
	}
}
