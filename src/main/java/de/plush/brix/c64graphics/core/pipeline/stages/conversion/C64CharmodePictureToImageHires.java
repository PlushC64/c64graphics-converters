package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;

import java.awt.Color;
import java.util.Optional;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.model.C64CharsetScreenColorRam;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;

public class C64CharmodePictureToImageHires extends AbstractCharmodePictureToImage {

	private final Palette palette;

	private Optional<Supplier<Color>> forcedBg = Optional.empty();

	/**
	 * Uses the {@link C64ColorsColodore} palette
	 */
	public C64CharmodePictureToImageHires() {
		this(C64ColorsColodore.PALETTE);
	}

	/**
	 * Uses a zoom factor of 1.
	 *
	 * @param palette
	 *            see {@link C64ColorsColodore},{@link C64ColorsPepto},
	 */
	public C64CharmodePictureToImageHires(final Palette palette) {
		this.palette = palette;
	}

	/**
	 *
	 * @param background
	 *            the color of 0 bits, overrides the value in the {@link C64CharsetScreenColorRam}
	 * @return this
	 */
	public C64CharmodePictureToImageHires withForcedBackgroundColor(final Color background) {
		forcedBg = Optional.ofNullable(background).map(c -> () -> c);
		return this;
	}

	@Override
	protected C64CharToImageHires createC64CharToImage(final byte backgroundColorCode, final byte mc1_D022ColorCode, final byte mc2_D023ColorCode,
			final byte d800ColorCode) {
		return new C64CharToImageHires(palette, forcedBg.orElse(() -> palette.color(toUnsignedByte(backgroundColorCode & 0x0F)))//
				, palette.color(toUnsignedByte(d800ColorCode & 0x0F)));
	}

}
