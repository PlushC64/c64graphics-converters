package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.Alignment.*;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.ResizeCanvas;

public class ImageToC64BitmapHires implements IPipelineStage<BufferedImage, C64Bitmap> {

	private final Supplier<Color> backgroundColorProvider;

	private final Color pixelColor;

	public ImageToC64BitmapHires() {
		this(() -> Color.BLACK, Color.WHITE);
	}

	public ImageToC64BitmapHires(final Supplier<Color> backgroundColorProvider, final Color pixelColor) {
		this.backgroundColorProvider = backgroundColorProvider;
		this.pixelColor = pixelColor;
	}

	@Override
	public C64Bitmap apply(final BufferedImage original) {
		final Color background = backgroundColorProvider.get();
		final var image = new ResizeCanvas(C64Bitmap.sizePixels(), backgroundColorProvider, Horizontal.LEFT, Vertical.TOP).apply(original);

		final ImageToC64CharHires imageToC64CharHires = new ImageToC64CharHires(background, pixelColor);

		final C64Bitmap bitmap = new C64Bitmap();
		final Dimension blocks = C64Bitmap.sizeBlocks();
		for (int y = 0; y < blocks.height; ++y) {
			for (int x = 0; x < blocks.width; ++x) {
				final int xPix = x * C64Char.WIDTH_PIXELS;
				final int yPix = y * C64Char.HEIGHT_PIXELS;
				final BufferedImage blockImage = image.getSubimage(xPix, yPix, C64Char.WIDTH_PIXELS, C64Char.HEIGHT_PIXELS);
				final C64Char block = imageToC64CharHires.apply(blockImage);
				bitmap.setBlock(x, y, block);
			}
		}
		return bitmap;
	}

}
