package de.plush.brix.c64graphics.core.post;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;
import static java.lang.Runtime.getRuntime;
import static java.util.Collections.emptySet;
import static java.util.Comparator.comparingDouble;
import static java.util.stream.Collectors.toMap;

import java.awt.Point;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.*;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;
import de.plush.brix.c64graphics.core.util.CountableObject;
import de.plush.brix.c64graphics.core.util.charpixeldistance.C64CharHiresPixelDistanceMeasure;
import de.plush.brix.c64graphics.core.util.collections.HashBag;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

public abstract class AbstractCharScreenDataReducerHires implements Function<Collection<C64CharsetScreen>, C64CharsetMultiScreen> {

	public enum Prefer {
		SmallLocalImpact, SmallGlobalImpact
	}

	private static final int CONCURRENCY_LEVEL = getRuntime().availableProcessors();

	protected final C64CharHiresPixelDistanceMeasure distanceMeasure;

	protected final Prefer impact;

	protected final C64Charset protectedChars;

	public AbstractCharScreenDataReducerHires(final C64CharHiresPixelDistanceMeasure distanceMeasure, final Prefer impact) {
		this(distanceMeasure, impact, new C64Charset());
	}

	protected AbstractCharScreenDataReducerHires(final C64CharHiresPixelDistanceMeasure distanceMeasure, final Prefer impact, final C64Charset protectedChars) {
		this.distanceMeasure = distanceMeasure;
		this.impact = impact;
		this.protectedChars = new C64Charset(protectedChars);
	}

	public abstract C64CharsetMultiScreen reduce(final Collection<C64CharsetScreen> _charScreens);

	protected abstract List<VictimToSurrogateMapping> buildQueue(final Map<C64Char, PreProcessedCharacterImage> chrToImageMap,
			final Set<C64Char> victimBlacklist, final HashBag<C64Char> occurrenceCounts);

	@Override
	public final C64CharsetMultiScreen apply(final Collection<C64CharsetScreen> charScreens) {
		return reduce(charScreens);
	}

	protected C64Charset reconstructCharset(final Map<C64Char, Locator> chrToLocations) {
		final C64Charset charset = new C64Charset();
		chrToLocations.keySet().forEach(charset::addCharacterEvenIfAlreadyPresent);
		return charset;
	}

	protected C64Screen[] reconstructScreens(final Map<C64Char, Locator> chrToLocations, final Collection<C64CharsetScreen> charScreens,
			final C64Charset charset) {
		final Map<C64CharsetScreen, C64Screen> csToScreen = charScreens.stream()//
				.collect(toMap(cs -> cs, cs -> new C64Screen((byte) 0), (u, v) -> { throw new IllegalStateException("Duplicate key " + u); },
						LinkedHashMap::new));
		chrToLocations.values().stream().forEach(locator -> {
			final byte screenCode = charset.screenCodeOf(locator.chr).getAsByte();
			locator.locations.entrySet().forEach(entry -> {
				final C64Screen screen = csToScreen.get(entry.getKey());
				final Set<Point> points = entry.getValue();
				points.forEach(point -> screen.setScreenCode(point.x, point.y, screenCode));
			});
		});
		return charScreens.stream().map(csToScreen::get).toArray(C64Screen[]::new);
	}

	protected Map<C64Char, Locator> allCharacterLocations(final Collection<C64CharsetScreen> charScreens) {
		final var protectedCharLocators = protectedChars.charStream()//
				.map(chr -> new LinkedHashMap<>(Map.of(chr, new Locator(chr))));
		final var regularCharLocators = charScreens.stream()//
				.map(AbstractCharScreenDataReducerHires::createLocators);
		return Stream.concat(protectedCharLocators, regularCharLocators)//
				.sequential() // to avoid concurrentModificationException in mergeLocatorMaps
				.reduce(new LinkedHashMap<>(), AbstractCharScreenDataReducerHires::mergeLocatorMaps);
	}

	static Map<C64Char, Locator> createLocators(final C64CharsetScreen charsetScreen) {
		return IntStream.range(0, C64Screen.WIDTH_BLOCKS * C64Screen.HEIGHT_BLOCKS)//
				.mapToObj(xy -> new Point(xy % C64Screen.WIDTH_BLOCKS, xy / C64Screen.WIDTH_BLOCKS))//
				.map(p -> createLocator(p, charsetScreen))//
				.collect(toMap(loc -> loc.chr, loc -> loc, (l1, l2) -> l1.merge(l2), LinkedHashMap::new));
	}

	static Locator createLocator(final Point p, final C64CharsetScreen charsetScreen) {
		final Locator loc = new Locator(charsetScreen.blockAt(p.x, p.y));
		loc.locations.computeIfAbsent(charsetScreen, (k) -> new LinkedHashSet<>(10)).add(p);
		return loc;
	}

	static <M extends Map<C64Char, Locator>> Map<C64Char, Locator> mergeLocatorMaps(final M m1, final M m2) {
		// final var combined = Stream.concat(m1.entrySet().stream(), m1.entrySet().stream());
		// return combined.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (l1, l2) -> l1.merge(l2)));
		m2.forEach((chr, locator) -> m1.merge(chr, locator, (loc1, loc2) -> loc1.merge(loc2)));
		return m1;
	}

	protected HashBag<C64Char> occurrenceCounts(final Map<C64Char, Locator> locations) {
		System.out.println("locations values size " + locations.size());
		return locations.values().stream()//
				.parallel()//
				.map((final Locator loc) -> {
					final HashBag<C64Char> bag = HashBag.<C64Char> empty();
					bag.add(CountableObject.ofValue(loc.chr).withCount(numberOfLocations(loc)));
					return bag;
				})//
				.sequential()//
				.reduce(HashBag.empty(), (b1, b2) -> { //
					b1.addAll(b2);
					return b1;
				});
	}

	protected int numberOfLocations(final Locator loc) {
		return loc.locations//
				.values().stream()//
				.mapToInt(Set::size)//
				.sum();
	}

	protected Map<C64Char, PreProcessedCharacterImage> createChrToImageMap(final Collection<C64Char> characters) {
		final var union = new ArrayList<>(characters);
		protectedChars.addAllCharsTo(union);
		return _createChrToImageMap(union);
	}

	private static Map<C64Char, PreProcessedCharacterImage> _createChrToImageMap(final Collection<C64Char> characters) {
		final Function<C64Char, PreProcessedCharacterImage> c64CharToImage = createC64CharToImage();
		return characters.stream()//
				.parallel()//
				.collect(Collectors.toConcurrentMap(chr -> chr, //
						chr -> c64CharToImage.apply(chr), //
						(i1, i2) -> i1, //
						() -> new ConcurrentHashMap<>(characters.size(), 0.75f, CONCURRENCY_LEVEL)));
	}

	private static Function<C64Char, PreProcessedCharacterImage> createC64CharToImage() {
		return new C64CharToImageHires(PALETTE, BLACK, C64ColorsColodore.WHITE.color);
	}

	protected void replaceCharacterEverywhere(final VictimToSurrogateMapping substitution, final Locator victimLocator,
			final Map<C64Char, Locator> chrToLocations) {
		// don't use "locatons.forEach", so we can be sure we're not working on the same charScreen in parallel
		final Locator surrogateLocator = chrToLocations.get(substitution.surrogate);
		chrToLocations.remove(substitution.victim);
		victimLocator.charScreens().stream()//
				// .parallel() // doesn't work
				.forEach(charScreen -> {
					/* WE DON'T NEED THIS. screens get fully reconstructed from chrToLocations-map: */
					// replace char first, so "setBlock" does not add a new character and we don't need to clean up char sets.
					// charScreen.replaceCharacter(substitution.victim, substitution.surrogate);
					// victimLocator.blockPositions(charScreen).forEach(point -> {
					// charScreen.setBlock(point.x, point.y, substitution.surrogate);
					// });
					surrogateLocator.locations.computeIfAbsent(charScreen, (cs) -> new LinkedHashSet<>())//
							.addAll(victimLocator.blockPositions(charScreen));
				});
	}

	protected VictimToSurrogateMapping pickFromQueue(final List<VictimToSurrogateMapping> queue, final Map<C64Char, Locator> chrToLocations,
			final Set<C64Char> victimBlacklist) {
		VictimToSurrogateMapping substitution;
		do {
			substitution = queue.remove(queue.size() - 1);
		} while (invalidQueueEntry(substitution, chrToLocations, victimBlacklist));
		return substitution;
	}

	/**
	 * Replacing a replaced item will increase the error twice, which we should not do. So the image should not be in the queue. <br>
	 * However: Just rejecting an entry picked from the queue is a lot faster, than to walk through the whole queue and clean it up.
	 */
	protected boolean invalidQueueEntry(final VictimToSurrogateMapping substitution, final Map<C64Char, Locator> chrToLocations,
			final Set<C64Char> victimBlacklist) {
		return !chrToLocations.containsKey(substitution.victim) // already replaced
				|| !chrToLocations.containsKey(substitution.surrogate) // already replaced
				|| victimBlacklist.contains(substitution.victim); // blacklisted
	}

	protected void cleanupCollections(final VictimToSurrogateMapping substitution, final Map<C64Char, ?> chrToImageMap, final Locator victimLocator,
			final Set<C64Char> victimBlacklist, final HashBag<C64Char> occurrenceCounts) {

		chrToImageMap.remove(substitution.victim);
		victimBlacklist.add(substitution.victim);

		final int victimCount = victimLocator.charScreens().parallelStream() //
				.mapToInt(charScreen -> victimLocator.blockPositions(charScreen).size()) //
				.sum();
		occurrenceCounts.removeAllOccurrences(substitution.victim);
		occurrenceCounts.add(CountableObject.ofValue(substitution.surrogate).withCount(victimCount));
	}

	protected Comparator<VictimToSurrogateMapping> substitutionOrder(final HashBag<C64Char> occurrenceCounts, final Prefer impact) {
		Comparator<VictimToSurrogateMapping> compareByImpact;
		switch (impact) {
		case SmallLocalImpact:
			compareByImpact = comparingDouble((final VictimToSurrogateMapping vso) -> vso.weight);
			break;
		case SmallGlobalImpact:
			compareByImpact = comparingDouble((final VictimToSurrogateMapping vso) -> vso.weight() * occurrenceCounts.occurrencesOf(vso.victim));
			break;
		default:
			throw new IllegalArgumentException("unknown enum constant " + impact);
		}
		return compareByImpact.thenComparingInt(vso -> occurrenceCounts.occurrencesOf(vso.victim)) // least frequentVictims go first, if impact is equal
				.thenComparingInt(vso -> -occurrenceCounts.occurrencesOf(vso.surrogate)) // more frequent surrogates go first, to have less error accumulation
				.thenComparing(VictimToSurrogateMapping::sortStabilizer)// everything being equal, sort by a hash code that is stable between runs
				.reversed();
	}

	protected void statsForChar(final Collection<C64CharsetScreen> charScreens, final Map<C64Char, Locator> chrToLocations,
			final List<VictimToSurrogateMapping> queue, final HashBag<C64Char> occurrenceCounts, final Map<C64Char, PreProcessedCharacterImage> chrToImageMap,

			final C64Char problemchild) {
		System.out.println("in queue as victim " + queue.stream().filter(q -> q.victim.equals(problemchild)).count());
		System.out.println("in queue as surrogate " + queue.stream().filter(q -> q.surrogate.equals(problemchild)).count());

		System.out.println("remains in charsets: " + charScreens.stream().filter(cs -> cs.contains(problemchild)).count());
		System.out.println("remains in screens: " + charScreens.stream()//
				.filter(cs -> StreamSupport.stream(cs.blockIterable().spliterator(), false).anyMatch(problemchild::equals))//
				.count());
		System.out.println("as key in chrToLocations: " + chrToLocations.containsKey(problemchild));
		System.out.println("value mapped in chrToLocations: " + chrToLocations.get(problemchild));
		System.out.println("in occurrencecounts: " + occurrenceCounts.occurrencesOf(problemchild));
		System.out.println("in chrToIMageMap: " + chrToImageMap.containsKey(problemchild));
	}

	@SuppressFBWarnings(value = "DB_DUPLICATE_BRANCHES", //
			justification = "shortcut for returning the result set without using the return statement in a lambda")
	public static class Locator {
		public final C64Char chr;

		final Map<C64CharsetScreen, Set<Point>> locations = new LinkedHashMap<>();

		public Locator(final C64Char chr) {
			this.chr = chr;
		}

		Locator merge(final Locator l2) {
			if (chr != l2.chr && !chr.equals(l2.chr)) {
				throw new UnsupportedOperationException("can't merge locators with different chars: " + chr + " \n\n" + l2.chr);
			}
			l2.locations.forEach((key, value) -> locations.merge(key, value, (v1, v2) -> v1.addAll(v2) ? v1 : v1)); // v1 both is important
			return this;
		}

		Set<C64CharsetScreen> charScreens() {
			return locations.keySet();
		}

		Set<Point> blockPositions(final C64CharsetScreen charScreen) {
			return locations.getOrDefault(charScreen, emptySet());
		}
	}

}
