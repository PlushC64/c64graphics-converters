package de.plush.brix.c64graphics.core.util.charpixeldistance;

import static de.plush.brix.c64graphics.core.util.Utils.*;
import static java.lang.Integer.bitCount;
import static java.lang.Math.*;
import static java.util.Arrays.copyOfRange;

import java.util.Arrays;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.model.C64Char;

/**
 * Compares characters by separating them into different regions and evaluating the change in pixel density in a circular fashion.<br>
 * This is meant as a secondary or complementary sort criteria, for it only values very rough regional density differences changes.
 * <p>
 * Example: The following character is split into a left top, a right top, a right bottom and a left bottom square.<br>
 * In the left picture, each square is given a number for its pixel density:<br>
 * -1 = low ( 0-4 pixels set), 0 = medium (5 to 11 pixels set) 1 = high (12 to 16 pixels set): <br>
 * <img src="doc-files/RoughCirular1.png"/><br>
 * In the right picture, starting with the left top in a circular fashion the change in pixel density is determined:<br>
 * -1 = falling, 0 = no change, 1 = raising.<br>
 * For this image we get "1, 1, -1, 0". <br>
 * For a comparison see a different, but still rather similar character:<br>
 * <img src="doc-files/RoughCirular2.png"/><br>
 * The second character is also classified as: "1, 1, -1, 0", so despite the different pixels, is is considered equal.<br>
 * This third character looks a bit more different:<br>
 * <img src="doc-files/RoughCirular3.png"/><br>
 * It is classified as "0, 1, -1, 0", it differs to the others by 1, which is because on the way from left top to right top, the density does not rise, as it
 * does in the first two examples. However, the value "1" is not what you'll get as a result, as the distance between these two characters is calculated by
 * normalizing the sum of differences, so you'll get 0.125:<br>
 * <code>min(1, IntStream.range(0, 4).map(index -&gt; abs(a[index] - b[index])).sum() / 8.0)</code> <br>
 * Watching this third character will demonstrate, how a character that looks almost like a 180� rotation of the first, is also classified as more
 * different:<br>
 * <img src="doc-files/RoughCirular4.png"/><br>
 * This one is classified as "-1, 0, 1, 1", compared to the first and second character ("1, 1, -1, 0") the difference comes out as "0.75".
 * </p>
 *
 * @author Wanja Gayk
 *
 */
public class HiresPixelDistanceCircularDensityChangeRough implements C64CharHiresPixelDistanceMeasure {

	@Override
	public double distanceOf(final C64Char c1, final C64Char c2) {
		return c1 == c2 || c1.equals(c2) ? 0 : circularPixelDensity(c1, c2);
	}

	private static double circularPixelDensity(final C64Char a, final C64Char b) {
		final int[] circularComparisonA = circularComparison(areaCountsClockwise(a.toC64Binary()));
		final int[] circularComparisonB = circularComparison(areaCountsClockwise(b.toC64Binary()));
		return normalizedSumOfDifferences(circularComparisonA, circularComparisonB);
	}

	private static double normalizedSumOfDifferences(final int[] circularComparisonA, final int[] circularComparisonB) {
		return min(1, IntStream.range(0, 4).map(index -> abs(circularComparisonA[index] - circularComparisonB[index])).sum() / 8.0);
	}

	private static int[] areaCountsClockwise(final byte[] xs) {
		final int leftTop = sumOf(copyOfRange(xs, 0, 4), x -> bitCount(toPositiveInt(highNibble(x))));
		final int rightTop = sumOf(copyOfRange(xs, 0, 4), x -> bitCount(toPositiveInt(lowNibble(x))));
		final int rightBottom = sumOf(copyOfRange(xs, 4, 8), x -> bitCount(toPositiveInt(lowNibble(x))));
		final int leftBottom = sumOf(copyOfRange(xs, 4, 8), x -> bitCount(toPositiveInt(highNibble(x))));
		return new int[] { leftTop, rightTop, rightBottom, leftBottom };
	}

	private static int[] circularComparison(final int[] pixelCounts) {
		// create 3 stage average (high, medium, low)
		final int[] tristage = asNegZeroPos(pixelCounts);
		// compare in a circular fashion:
		return new int[] { Double.compare(tristage[0], tristage[1]) //
				, Double.compare(tristage[1], tristage[2]) //
				, Double.compare(tristage[2], tristage[3]) //
				, Double.compare(tristage[3], tristage[0]) //
		};
	}

	static int[] asNegZeroPos(final int[] pixelCounts) {
		return Arrays.stream(pixelCounts).map(x -> min(1, max(-1, (-8 + x) / 4))).toArray();
	}

	public static void main(final String[] args) {
		// System.out.println(Arrays.toString(asNegZeroPos(new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 })));
		//
		// final C64Char half = new C64Char(new byte[] { (byte) 0xaa, (byte) 0xaa, (byte) 0xaa, (byte) 0xaa, (byte) 0xaa, (byte) 0xaa, (byte) 0xaa, (byte) 0xaa
		// });
		// final C64Char full = new C64Char(new byte[] { (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff
		// });
		// final double a = new CircularPixelDensity().distanceOf(C64Char.BLANK, C64Char.BLANK);
		// final double b = new CircularPixelDensity().distanceOf(C64Char.BLANK, half);
		// final double c = new CircularPixelDensity().distanceOf(C64Char.BLANK, full);
		//
		// System.out.println(a);
		// System.out.println(b);
		// System.out.println(c);

		System.out.println(normalizedSumOfDifferences(new int[] { 1, 1, -1, 0 }, new int[] { 1, 1, -1, 0 }));
		System.out.println(normalizedSumOfDifferences(new int[] { 1, 1, -1, 0 }, new int[] { 0, 1, -1, 0 }));
		System.out.println(normalizedSumOfDifferences(new int[] { 1, 1, -1, 0 }, new int[] { -1, 0, 1, 1 }));

	}
}
