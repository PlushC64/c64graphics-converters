package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.util.Utils;

public class PackedCharsetScreenBinaryToImage implements IPipelineStage<PackedCharsetScreenBinary, List<Image>> {

	@Override
	public List<Image> apply(final PackedCharsetScreenBinary binary) {
		final int xPix = C64CharsetScreen.WIDTH_BLOCKS * C64Char.WIDTH_PIXELS;
		final int yPix = C64CharsetScreen.HEIGHT_BLOCKS * C64Char.HEIGHT_PIXELS;

		final List<Image> images = new ArrayList<>();
		new RLEdecoder(binary.screens) {

			BufferedImage image = new BufferedImage(xPix, yPix, BufferedImage.TYPE_INT_RGB);

			int xBlock;

			int yBlock;

			@Override
			void onByte(final byte screenCode) {
				int y = -1;
				for (byte b : charsetCharacterBlockOf(screenCode)) {
					++y;
					for (int x = 0; x < C64Char.WIDTH_PIXELS; ++x) {
						if ((b & 0x80) != 0) {
							image.setRGB(xBlock * C64Char.WIDTH_PIXELS + x, yBlock * C64Char.HEIGHT_PIXELS + y, Color.WHITE.getRGB());
						}
						b <<= 1;
					}
				}
				if (++xBlock == C64CharsetScreen.WIDTH_BLOCKS) {
					xBlock = 0;
					++yBlock;
				}

			}

			@Override
			void onEndSequence() {
				images.add(image);
				image = new BufferedImage(xPix, yPix, BufferedImage.TYPE_INT_RGB);

				final int bytesWritten = yBlock * C64CharsetScreen.WIDTH_BLOCKS + xBlock;

				if (bytesWritten > 1000) {
					throw new IllegalStateException("unpacked more that 1000 bytes per frame");
				}

				xBlock = 0;
				yBlock = 0;
			}

			private byte[] charsetCharacterBlockOf(final byte screenCode) {
				final int index = Utils.toPositiveInt(screenCode);
				final byte[] block = new byte[8];
				block[0] = binary.charset[8 * index];
				block[1] = binary.charset[8 * index + 1];
				block[2] = binary.charset[8 * index + 2];
				block[3] = binary.charset[8 * index + 3];
				block[4] = binary.charset[8 * index + 4];
				block[5] = binary.charset[8 * index + 5];
				block[6] = binary.charset[8 * index + 6];
				block[7] = binary.charset[8 * index + 7];
				return block;
			}

		}.run();

		return images;
	}

	private static abstract class RLEdecoder {

		private final byte[] packedBytes;

		RLEdecoder(final byte[] packedBytes) {
			this.packedBytes = packedBytes;
		}

		public void run() {
			int pos = -1;
			while (++pos < packedBytes.length) {
				byte b = packedBytes[pos];
				if (b != 0) {
					onByte(b);
				} else {
					int n = Utils.toPositiveInt(packedBytes[++pos]);
					if (n == 0) {
						onEndSequence();
					} else {
						b = packedBytes[++pos];
						while (--n >= 0) {
							onByte(b);
						}
					}
				}
			}
		}

		abstract void onByte(byte b);

		abstract void onEndSequence();

	}

}
