package de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes;

import java.awt.Color;
import java.util.List;

public interface D800PreferenceSupplier {

	List<Color> preferredColorRamD800Colors();

}