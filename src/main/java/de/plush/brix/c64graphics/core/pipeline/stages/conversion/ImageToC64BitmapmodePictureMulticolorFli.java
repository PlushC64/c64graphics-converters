package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;
import static de.plush.brix.c64graphics.core.util.UtilsImage.*;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.function.*;
import java.util.stream.*;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.*;

public class ImageToC64BitmapmodePictureMulticolorFli implements IPipelineStage<BufferedImage, C64BitmapmodePictureMulticolorFli> {

	private final IPipelineStage<BufferedImage, PreProcessedMulticolorFliImage> colorClashResolver;

	private final Palette palette;

	private List<Color> preferredMc1_d022Colors = emptyList();

	private List<Color> preferredMc2_d023Colors = emptyList();

	private final List<Color> preferredCramColors;

	/**
	 * Using the standard {@link MulticolorFLIColorClashResolver}. <br>
	 * <p>
	 * Use {@link #ImageToC64BitmapmodePictureMulticolorFli(Palette, IMulticolorFLIColorClashResolver)} if you want to use a custom color clash resolver.
	 * </p>
	 * 
	 * @param palette
	 *            the C64 target palette
	 * @param backgroundSupplier
	 *            provides a C64 palette background color.
	 * @param colorDistanceMeasure
	 *            used to measure how similar/dissimilar a color is.
	 */
	public ImageToC64BitmapmodePictureMulticolorFli(final Palette palette, final Supplier<Color> backgroundSupplier,
			final ColorDistanceMeasure colorDistanceMeasure) {
		this(palette, new MulticolorFLIColorClashResolver(palette, backgroundSupplier, colorDistanceMeasure));
	}

	/**
	 * Allows using a custom color clash resolver.
	 *
	 * @param palette
	 *            the C64 target palette
	 * @param colorClashResolver
	 *            the color clash resolver used for preprocessing.
	 */
	public ImageToC64BitmapmodePictureMulticolorFli(final Palette palette, final IMulticolorFLIColorClashResolver colorClashResolver) {
		this.palette = palette;
		this.colorClashResolver = colorClashResolver;
		preferredCramColors = colorClashResolver.preferredColorRamD800Colors();
	}

	public ImageToC64BitmapmodePictureMulticolorFli withPreferredMulticolor1D022Colors(final Color... colors) {
		preferredMc1_d022Colors = asList(colors);
		preferredMc1_d022Colors.forEach(palette::colorCodeOrException);
		checkSettingsConsistency();
		return this;
	}

	public ImageToC64BitmapmodePictureMulticolorFli withPreferredMulticolor2D023Colors(final Color... colors) {
		preferredMc2_d023Colors = asList(colors);
		preferredMc2_d023Colors.forEach(palette::colorCodeOrException);
		checkSettingsConsistency();
		return this;
	}

	private void checkSettingsConsistency() {
		crossCheck(preferredMc1_d022Colors, preferredMc2_d023Colors, x -> "You can't force these colors to be in both mc1_d022 and mc2_d022: " + x);
		crossCheck(preferredMc1_d022Colors, preferredCramColors, x -> "You can't force these colors to be in both mc1_d022 and cram_d800: " + x);
		crossCheck(preferredMc2_d023Colors, preferredCramColors, x -> "You can't force these colors to be in both mc2_d022 and cram_d800: " + x);
	}

	private static void crossCheck(final Collection<Color> a, final Collection<Color> b, final Function<Collection<Color>, String> messageProvider) {
		final Set<Color> x = a.stream().filter(b::contains).collect(toSet());
		if (!x.isEmpty()) {
			throw new IllegalArgumentException(messageProvider.apply(x));
		}
	}

	@SuppressWarnings("checkstyle:CyclomaticComplexity") // found no meaningful way to extract methods that make it more readable
	@Override
	public C64BitmapmodePictureMulticolorFli apply(final BufferedImage original) {
		final PreProcessedMulticolorFliImage sanitized = colorClashResolver.apply(original);
		final Color background = palette.color(toUnsignedByte(sanitized.backgroundColorCode() & 0x0F));

		final C64Bitmap bitmap = new C64Bitmap();
		final C64Screen[] screens = IntStream.range(0, C64Char.HEIGHT_PIXELS).mapToObj(__ -> new C64Screen((byte) 0)).toArray(C64Screen[]::new);
		final C64ColorRam colorRam = new C64ColorRam((byte) 0);

		final Dimension blocks = C64Bitmap.sizeBlocks();
		for (int y = 0; y < blocks.height; ++y) {
			for (int x = 3; x < blocks.width; ++x) {
				final byte[] blockBytes = new byte[C64Char.HEIGHT_PIXELS];
				final byte preProcessedCramColorCode = sanitized.colorCodeAt(x, y);
				final Color d800 = palette.color(toUnsignedByte(preProcessedCramColorCode & 0x0F));

				for (int line = 0; line < 8; ++line) {
					try {
						final int xPix = x * C64Char.WIDTH_PIXELS;
						final int yPix = y * C64Char.HEIGHT_PIXELS + line;
						final BufferedImage lineImage = sanitized.getSubimage(xPix, yPix, C64Char.WIDTH_PIXELS, 1);

						final Set<Color> lineColors = colors(lineImage);
						if (lineColors.size() > 4) {
							throw new IllegalStateException("unexpectedly found more than 4 colors. Have you solved all color clashes?");
						}
						final List<Color> colors = lineColors.stream()//
								.filter(c -> !c.equals(background) && !c.equals(d800))//
								.collect(toList());
						colors.sort(UtilsColor.SORT_BY_BRIGHTNESS); // to get somewhat predictable patterns
						// while (colors.size() < 3) { // if I sort later, a "filler" color takes precedence over a real color
						// colors.add(palette.color((byte) 0)); // default
						// }
						Color mc1 = preferredMc1_d022Colors.stream().filter(colors::contains).findFirst().orElse(null);
						Color mc2 = preferredMc2_d023Colors.stream().filter(colors::contains).findFirst().orElse(null);
						// Color d800 = preferredCramColors.stream().filter(colors::contains).findFirst().orElse(null);

						Stream.of(mc1, mc2).filter(c -> c != null).forEach(colors::remove);
						// list of colors has no preferred color left, from here we can assign colors that are not preferred by anyone to empty slots
						final Iterator<Color> colorsLeft = colors.iterator();
						mc1 = mc1 == null ? colorsLeft.hasNext() ? colorsLeft.next() : background : mc1;
						mc2 = mc2 == null ? colorsLeft.hasNext() ? colorsLeft.next() : background : mc2;
						// d800 = d800 == null ? preProcessedCramColor : d800;
						Stream.of(mc1, mc2, d800).forEach(colors::remove);
						final C64Char block = new ImageToC64CharMulticolor(background, mc1, mc2, d800).apply(repetitiveBlockImage(lineImage));
						blockBytes[line] = block.toC64Binary()[line];
						screens[line].setScreenCode(x, y, toUnsignedByte(palette.colorCodeOrException(mc1) << 4 | palette.colorCodeOrException(mc2)));
						colorRam.setColorCode(x, y, palette.colorCodeOrException(d800));
					} catch (final Exception e) {
						throw new IllegalStateException("In Block x=" + x + ", y=" + y + ", line=" + line, e);
					}
				}
				bitmap.setBlock(x, y, new C64Char(blockBytes));
			}
		}
		return new C64BitmapmodePictureMulticolorFli(bitmap, screens, colorRam, palette.colorCodeOrException(background));
	}

}
