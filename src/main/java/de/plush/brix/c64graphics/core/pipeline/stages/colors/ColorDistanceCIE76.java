package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces.ColorConversions.*;
import static java.lang.Math.min;

import java.awt.Color;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces.ColorCieLab;

/**
 * Color distance according to <a href="https://en.wikipedia.org/wiki/Color_difference#CIE76">CIE76</a>.
 * The 1976 formula is the first formula that related a measured color difference to a known set of CIELAB coordinates. This formula has been succeeded by the
 * 1994 and 2000 formulas because the CIELAB space turned out to be not as perceptually uniform as intended, especially in the saturated regions. This means
 * that this formula rates these colors too highly as opposed to other colors.
 * 
 * Note that the CIE76 color difference formula is basically the same as the Hunter Lab and DIN99 color difference formula, but operating on a differently
 * formed Lab color space.
 */
class ColorDistanceCIE76 implements ColorDistanceMeasure {

	@Override
	public double distanceOf(final Color rgb1, final Color rgb2) {
		return rgb1.equals(rgb2) ? 0.0 : min(1, distanceOf(convertXYZtoCIELab(convertRGBtoXYZ(rgb1)), convertXYZtoCIELab(convertRGBtoXYZ(rgb2))) / 150);
	}

	/**
	 * Compares two L*a*b colors and returns the degree of their similarity. The lower the result the more similar are the colors.
	 *
	 * Taken from https://en.wikipedia.org/wiki/Color_difference
	 *
	 * @param lab1
	 *            First color represented in L*a*b color space.
	 * @param lab2
	 *            Second color represented in L*a*b color space.
	 * @return The degree of similarity between the two input colors according to the CIE76 color-difference formula.
	 */
	private static double distanceOf(final ColorCieLab lab1, final ColorCieLab lab2) {
		return Math.sqrt(Math.pow(lab2.L() - lab1.L(), 2) + Math.pow(lab2.a() - lab1.a(), 2) + Math.pow(lab2.b() - lab1.b(), 2));
	}
}
