package de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes;

import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.ColorDistanceMeasure;

/**
 * Resolves color clashes in Hires-Bitmaps + 8 Screen RAMs (AFLI/Topaz or Hires FLI/Crest)
 *
 * @author Wanja Gayk
 */
public class HiresFLIColorClashResolver implements IHiresFLIColorClashResolver {

	private final HiresNscreenFLIColorClashResolver hiresNScreenFLIColorClashResolver;

	public HiresFLIColorClashResolver(final ColorDistanceMeasure colorDistanceMeasure) {
		final int screens = 8;
		hiresNScreenFLIColorClashResolver = new HiresNscreenFLIColorClashResolver(screens, colorDistanceMeasure);
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		return hiresNScreenFLIColorClashResolver.apply(original);
	}

}
