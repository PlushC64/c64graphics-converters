
package de.plush.brix.c64graphics.core.util;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;
import static java.util.Comparator.*;

import java.util.Comparator;

public class ByteBytePair implements Comparable<ByteBytePair> {

	public static final Comparator<ByteBytePair> ORDER_UNSIGNED = //
			nullsFirst(comparingInt((final ByteBytePair bbp) -> Byte.toUnsignedInt(bbp.getOne()))//
					.thenComparingInt(bbp2 -> Byte.toUnsignedInt(bbp2.getTwo())));

	private final byte one;

	private final byte two;

	ByteBytePair(final byte one, final byte two) {
		this.one = one;
		this.two = two;
	}

	/**
	 * @param one
	 *            an integer between 0 and 0xFF (inclusive)
	 * @param two
	 *            an integer between 0 and 0xFF (inclusive)
	 * @throws IllegalArgumentException
	 *             if values are out of range.
	 */
	public static ByteBytePair pairUnsigned(final int one, final int two) {
		checkRange(one);
		checkRange(two);
		return new ByteBytePair(toUnsignedByte(one), toUnsignedByte(two));
	}

	public static ByteBytePair pair(final byte one, final byte two) {
		return new ByteBytePair(one, two);
	}

	/** same as "pair", may read better depending on the context */
	public static ByteBytePair of(final byte one, final byte two) {
		return new ByteBytePair(one, two);
	}

	private static void checkRange(final int one) {
		if (one < 0 || one > 0xFF) { throw new IllegalArgumentException("value " + one + " out of range for a byte."); }
	}

	public byte getOne() {
		return one;
	}

	public byte getTwo() {
		return two;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + one;
		result = prime * result + two;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final ByteBytePair other = (ByteBytePair) obj;
		if (one != other.one) { return false; }
		if (two != other.two) { return false; }
		return true;
	}

	@Override
	public String toString() {
		return one + ":" + two;
	}

	@Override
	public int compareTo(final ByteBytePair that) {
		final int i = one < that.getOne() ? -1 : one > that.getOne() ? 1 : 0;
		if (i != 0) { return i; }
		return two < that.getTwo() ? -1 : two > that.getTwo() ? 1 : 0;
	}
}
