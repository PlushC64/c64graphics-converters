package de.plush.brix.c64graphics.core.post;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import de.plush.brix.c64graphics.core.model.C64Sprite;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.ImageToC64SpriteHires;
import de.plush.brix.c64graphics.core.pipeline.stages.manipulation.ImageSlicer;
import de.plush.brix.c64graphics.core.util.Utils;

public class ImagesToSpriteAnimation implements Consumer<List<BufferedImage>> {

	private final ImageSlicer slicer;

	private final ImageToC64SpriteHires imageToC64Sprite;

	private final Dimension numSprites;

	private final File fileOut;

	public ImagesToSpriteAnimation(final Dimension numSprites, final Color pixelColor, final File fileOut) {
		this.numSprites = new Dimension(numSprites);
		this.fileOut = fileOut;
		slicer = new ImageSlicer(numSprites, C64Sprite.sizePixels());
		imageToC64Sprite = new ImageToC64SpriteHires(pixelColor);
	}

	@Override
	public void accept(final List<BufferedImage> images) {
		final List<C64Sprite> sprites = new LinkedList<>();

		for (final var image : images) {
			final BufferedImage[][] slicedImages = slicer.apply(image);
			for (int y = 0; y < slicedImages.length; ++y) {
				final BufferedImage[] line = slicedImages[y];
				for (int x = 0; x < numSprites.width; ++x) {
					final BufferedImage cutImage = line[x];
					final C64Sprite c64Sprite = imageToC64Sprite.apply(cutImage);
					sprites.add(c64Sprite);
				}
			}
		}

		final byte[] spriteAnimationBinary = new byte[sprites.size() * 0x40];
		for (int s = 0; s < sprites.size(); ++s) {
			final C64Sprite sprite = sprites.get(s);
			System.arraycopy(sprite.toC64Binary(), 0, spriteAnimationBinary, s * 0x40, 0x40);
		}
		Utils.writeFile(fileOut.getPath(), spriteAnimationBinary);
	}

}
