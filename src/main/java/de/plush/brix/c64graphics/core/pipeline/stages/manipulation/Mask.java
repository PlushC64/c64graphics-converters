/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.*;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class Mask implements IPipelineStage<BufferedImage, BufferedImage> {

	private final Shape maskArea;
	private final Color color;

	public Mask(final Color color, final Shape maskArea) {
		this.color = color;
		this.maskArea = maskArea;
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		final Graphics2D g = (Graphics2D) result.getGraphics();
		try {
			g.drawImage(original, 0, 0, null);
			g.setColor(color);
			g.fill(maskArea);
			return result;
		} finally {
			g.dispose();
		}

	}
}