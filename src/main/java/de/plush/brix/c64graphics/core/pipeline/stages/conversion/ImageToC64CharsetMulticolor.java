package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.function.*;

import de.plush.brix.c64graphics.core.model.C64Char;

public class ImageToC64CharsetMulticolor extends AbstractImageToC64Charset {

	final Supplier<Color> backgroundColorProvider;

	private final Color mc1D022;

	private final Color mc2D023;

	private final Color cramD800;

	public ImageToC64CharsetMulticolor(final CharsetFormat grid, final Supplier<Color> backgroundColorProvider, final Color mc1D022, final Color mc2D023,
			final Color cramD800) {
		super(grid);
		this.backgroundColorProvider = backgroundColorProvider;
		this.mc1D022 = mc1D022;
		this.mc2D023 = mc2D023;
		this.cramD800 = cramD800;
	}

	@Override
	protected Function<BufferedImage, C64Char> createImageToC64Char() {
		final Color background = backgroundColorProvider.get(); // don't get in constructor, the value may be determined after construction.
		return new ImageToC64CharMulticolor(background, mc1D022, mc2D023, cramD800);
	}

}
