package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;

import java.awt.Color;
import java.util.function.*;

import de.plush.brix.c64graphics.core.model.C64Char;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.C64CharToImageMulticolor.Mode;

public class ReduceNumberOfUniformlyColoredCharactersMulticolor extends AbstractReduceNumberOfUniformlyColoredCharacters {

	private final Palette palette;

	private final Supplier<Color> background;

	private final Color mc1_D022;

	private final Color mc2_D023;

	private final Color colorRamColor;

	public ReduceNumberOfUniformlyColoredCharactersMulticolor(final int maxCharacters, final ColorDistanceMeasure distanceMeasure) {
		this(maxCharacters, PALETTE, distanceMeasure, BLACK, DARK_GRAY.color, GRAY.color, WHITE.color);
	}

	/**
	 * Allows for more precision, by considering the real colors
	 */
	public ReduceNumberOfUniformlyColoredCharactersMulticolor(final int maxCharacters, //
			final Palette palette, final ColorDistanceMeasure distanceMeasure, //
			final Supplier<Color> background, final Color mc1_D022, final Color mc2_D023, final Color colorRamColor) {
		super(maxCharacters, distanceMeasure);
		this.palette = palette;
		this.background = background;
		this.mc1_D022 = mc1_D022;
		this.mc2_D023 = mc2_D023;
		this.colorRamColor = colorRamColor;
	}

	@Override
	public Function<C64Char, PreProcessedCharacterImage> createC64CharToImage() {
		return new C64CharToImageMulticolor(palette, background, mc1_D022, mc2_D023, colorRamColor, Mode.Bitmap);
	}

}
