package de.plush.brix.c64graphics.core.emitters;

import static java.nio.file.StandardOpenOption.READ;
import static java.util.concurrent.TimeUnit.NANOSECONDS;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URI;
import java.nio.file.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import javax.annotation.WillClose;
import javax.imageio.*;
import javax.imageio.metadata.*;

import org.w3c.dom.*;

public class GifEmitter extends AbstractImageEmitter<BufferedImage> {
	private static final Color DEFAULT_BACKGROUND = new Color(0, 0, 0, 0);

	private volatile Timer timer;

	private volatile CountDownLatch stopSignal;

	private final Path file;

	private final Color background;

	public GifEmitter(final File file) {
		this(file.toURI(), DEFAULT_BACKGROUND);
	}

	public GifEmitter(final URI file) {
		this(file, DEFAULT_BACKGROUND);
	}

	public GifEmitter(final Path file) {
		this(file, DEFAULT_BACKGROUND);
	}

	public GifEmitter(final File file, final Color defaultBackground) {
		this(file.toURI(), defaultBackground);
	}

	public GifEmitter(final URI file, final Color defaultBackground) {
		this(Paths.get(file), defaultBackground);
	}

	public GifEmitter(final Path file, final Color defaultBackground) {
		this.file = file;
		background = defaultBackground;
	}

	@WillClose
	@Override
	public void run() { // using a timer, because it looks more fancy, if you can see the animation while converting :-)
		stopSignal = new CountDownLatch(1);

		ImageReader reader = null;
		try (final var fileInputStream = Files.newInputStream(file, READ); final var imageInputStream = ImageIO.createImageInputStream(fileInputStream)) {
			reader = ImageIO.getImageReadersByFormatName("gif").next();
			reader.setInput(imageInputStream);

			final ImageFrame[] frames = readGIF(reader);
			timer = new Timer("GifEmitter timer", true);
			timer.schedule(new EmitFrameTask(frames, 0), 0);
			try {
				stopSignal.await();
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		} catch (final IOException e) {
			throw new IllegalStateException(e);
		} finally {
			if (reader != null) {
				reader.dispose();
			}
		}
	}

	private final class EmitFrameTask extends TimerTask {
		private final ImageFrame[] frames;

		private final int frameIndex;

		private EmitFrameTask(final ImageFrame[] frames, final int frameIndex) {
			this.frames = frames;
			this.frameIndex = frameIndex;
		}

		@Override
		public void run() {
			final ImageFrame imageFrame = frames[frameIndex];
			final long start = System.nanoTime();
			emit(imageFrame.image);
			final long pipelineMillis = NANOSECONDS.toMillis(System.nanoTime() - start);
			if (frameIndex + 1 < frames.length) {
				final long frameDelayMillis = imageFrame.delay * 10L; // delay = n/100 seconds. For 10L see Findbugs: ICAST_INTEGER_MULTIPLY_CAST_TO_LONG
				final long delayMillis = Math.max(0L, frameDelayMillis - pipelineMillis);
				timer.schedule(new EmitFrameTask(frames, frameIndex + 1), delayMillis);
			} else {
				stop(timer);
			}
		}

		private void stop(final Timer timer) {
			timer.cancel();
			closeEmission();
			stopSignal.countDown();
		}
	}

	protected ImageFrame[] readGIF(final ImageReader reader) throws IOException {
		final ArrayList<ImageFrame> frames = new ArrayList<>(2);
		Graphics2D masterGraphics = null;
		try {
			BufferedImage masterImage = null;

			for (int frameIndex = 0;; frameIndex++) {
				final BufferedImage image = reader.read(frameIndex);
				final IIOMetadataNode root = (IIOMetadataNode) reader.getImageMetadata(frameIndex).getAsTree("javax_imageio_gif_image_1.0");
				final IIOMetadataNode gce = (IIOMetadataNode) root.getElementsByTagName("GraphicControlExtension").item(0);
				final int delay = Integer.parseInt(gce.getAttribute("delayTime"));
				final String disposal = gce.getAttribute("disposalMethod");

				final Point imagePosition;
				if (masterImage == null) {
					final var dimension = imageDimensionFromMetaData(reader).orElse(new Dimension(image.getWidth(), image.getHeight()));
					masterImage = new BufferedImage(dimension.width, dimension.height, BufferedImage.TYPE_INT_ARGB);
					masterGraphics = masterImage.createGraphics();
					masterGraphics.setBackground(background);
					imagePosition = new Point(0, 0);
				} else {
					imagePosition = childPosition(root).orElse(new Point(0, 0));
				}
				assert masterGraphics != null : "masterGrahpics must be set with master";
				masterGraphics.drawImage(image, imagePosition.x, imagePosition.y, null);

				final BufferedImage copy = new BufferedImage(masterImage.getColorModel(), masterImage.copyData(null), masterImage.isAlphaPremultiplied(), null);
				frames.add(new ImageFrame(copy, delay, disposal));

				if ("restoreToPrevious".equals(disposal)) {
					final BufferedImage from = previousImage(frames, frameIndex).orElse(copy);
					masterImage = new BufferedImage(from.getColorModel(), from.copyData(null), from.isAlphaPremultiplied(), null);
					masterGraphics.dispose();
					masterGraphics = masterImage.createGraphics();
					masterGraphics.setBackground(background);
				} else if ("restoreToBackgroundColor".equals(disposal)) {
					masterGraphics.clearRect(imagePosition.x, imagePosition.y, image.getWidth(), image.getHeight());
				}
			}
		} catch (final IndexOutOfBoundsException io) {
			// expected when last image is read. The GIF format does not provide a maximum number of images..
		} finally {
			if (masterGraphics != null) {
				masterGraphics.dispose();
			}
		}
		return frames.toArray(new ImageFrame[frames.size()]);
	}

	private static Optional<BufferedImage> previousImage(final List<ImageFrame> frames, final int frameIndex) {
		for (int i = frameIndex - 1; i >= 0; i--) {
			if (!frames.get(i).disposal.equals("restoreToPrevious") || frameIndex == 0) { return Optional.of(frames.get(i).image); }
		}
		return Optional.empty();
	}

	private static Optional<Point> childPosition(final IIOMetadataNode root) {
		final NodeList children = root.getChildNodes();
		for (int nodeIndex = 0; nodeIndex < children.getLength(); nodeIndex++) {
			final Node nodeItem = children.item(nodeIndex);
			if (nodeItem.getNodeName().equals("ImageDescriptor")) {
				final NamedNodeMap map = nodeItem.getAttributes();
				return Optional.of(new Point(//
						Integer.parseInt(map.getNamedItem("imageLeftPosition").getNodeValue()), //
						Integer.parseInt(map.getNamedItem("imageTopPosition").getNodeValue())));
			}
		}
		return Optional.empty();
	}

	private static Optional<Dimension> imageDimensionFromMetaData(final ImageReader reader) throws IOException {
		final IIOMetadata metadata = reader.getStreamMetadata();
		if (metadata != null) {
			final IIOMetadataNode globalRoot = (IIOMetadataNode) metadata.getAsTree(metadata.getNativeMetadataFormatName());

			final NodeList globalScreenDescriptor = globalRoot.getElementsByTagName("LogicalScreenDescriptor");

			if (globalScreenDescriptor != null && globalScreenDescriptor.getLength() > 0) {
				final IIOMetadataNode screenDescriptor = (IIOMetadataNode) globalScreenDescriptor.item(0);

				if (screenDescriptor != null) {
					final int width = Integer.parseInt(screenDescriptor.getAttribute("logicalScreenWidth"));
					final int height = Integer.parseInt(screenDescriptor.getAttribute("logicalScreenHeight"));
					Optional.of(new Dimension(width, height));
				}
			}
		}
		return Optional.empty();
	}

	private static class ImageFrame {
		private final int delay;

		private final BufferedImage image;

		private final String disposal;

		public ImageFrame(final BufferedImage image, final int delay, final String disposal) {
			this.image = image;
			this.delay = delay;
			this.disposal = disposal;
		}
	}

}
