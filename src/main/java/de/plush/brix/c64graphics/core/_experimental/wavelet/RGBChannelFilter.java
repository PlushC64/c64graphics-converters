/*
 * Copyright (c) 1997, 1998, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package de.plush.brix.c64graphics.core._experimental.wavelet;

import java.awt.image.RGBImageFilter;

/**
 * Extracts a value from a given color channel (i.e. rgba 80 e0 a0 ff -> Filter(Red) -> 80 00 00 00
 * 
 * @author Wanja Gayk
 */
class RGBChannelFilter extends RGBImageFilter {

	private final ColorChannel channel;

	public RGBChannelFilter(final ColorChannel channel) {
		this.channel = channel;
		// canFilterIndexColorModel indicates whether or not it is acceptable
		// to apply the color filtering of the filterRGB method to the color
		// table entries of an IndexColorModel object in lieu of pixel by pixel
		// filtering.
		canFilterIndexColorModel = true;

	}

	@Override
	public int filterRGB(final int x, final int y, final int rgb) {
		return channel.filterRgb(rgb);
	}
}
