package de.plush.brix.c64graphics.core.pipeline.stages.colors;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces.ColorConversions.*;
import static java.lang.Math.round;

import java.awt.Color;
import java.util.function.*;

import de.plush.brix.c64graphics.core.pipeline.stages.colors.spaces.*;

/**
 * Mixes colors in different color spaces.<br>
 * A color is mixed by using the average of each channel (e.g. (x.red + y.red) /2).<br>
 * As for a {@link ColorDistance} the result of doing this is in different color spaces
 * can be quite different.
 */
public enum ColorMix implements BinaryOperator<Color> {

	RGB((a, b) -> new Color(//
			mid(a, b, Color::getRed), //
			mid(a, b, Color::getGreen), //
			mid(a, b, Color::getBlue) //
	)), //
	/** Recommended for C64 color mixes. */
	HUNTER_Lab((a, b) -> mix(a, b, //
			(rgb) -> convertXYZtoHunterLab(convertRGBtoXYZ(rgb)), //
			(x, y) -> new ColorHunterLab(//
					mid(x, y, ColorHunterLab::L), //
					mid(x, y, ColorHunterLab::a), //
					mid(x, y, ColorHunterLab::b)), //
			(r) -> new Color(convertXYZtoRGB(convertHunterLabtoXYZ(r)))//
	)), //
	CIE_Lab((a, b) -> mix(a, b, //
			(rgb) -> convertXYZtoCIELab(convertRGBtoXYZ(rgb)), //
			(x, y) -> new ColorCieLab(//
					mid(x, y, ColorCieLab::L), //
					mid(x, y, ColorCieLab::a), //
					mid(x, y, ColorCieLab::b)), //
			(r) -> new Color(convertXYZtoRGB(convertCIELabtoXYZ(r)))//
	)), //
	DIN99b_Lab((a, b) -> mix(a, b, //
			(rgb) -> convertCIELabToDIN99bLab(convertXYZtoCIELab(convertRGBtoXYZ(rgb))), //
			(x, y) -> new ColorDIN99Lab(//
					mid(x, y, ColorDIN99Lab::L99), //
					mid(x, y, ColorDIN99Lab::a99), //
					mid(x, y, ColorDIN99Lab::b99)), //
			(r) -> new Color(convertXYZtoRGB(convertCIELabtoXYZ(convertDIN99bLabToCIELab(r))))//
	)), //
	DIN99o_Lab((a, b) -> mix(a, b, //
			(rgb) -> convertCIELabToDIN99oLab(convertXYZtoCIELab(convertRGBtoXYZ(rgb))), //
			(x, y) -> new ColorDIN99Lab(//
					mid(x, y, ColorDIN99Lab::L99), //
					mid(x, y, ColorDIN99Lab::a99), //
					mid(x, y, ColorDIN99Lab::b99)), //
			(r) -> new Color(convertXYZtoRGB(convertCIELabtoXYZ(convertDIN99oLabToCIELab(r))))//
	)), //
	YUV((a, b) -> mix(a, b, //
			(rgb) -> convertRGBtoYUV(rgb), //
			(x, y) -> new ColorYUV(//
					mid(x, y, ColorYUV::Y), //
					mid(x, y, ColorYUV::U), //
					mid(x, y, ColorYUV::V)), //
			(r) -> new Color(convertYUVtoRGB(r))//
	)), //
	YCbCrJPEG((a, b) -> mix(a, b, //
			(rgb) -> convertRGBtoYCbCrJPEG(rgb), //
			(x, y) -> new ColorYCbCr(//
					mid(x, y, ColorYCbCr::Y), //
					mid(x, y, ColorYCbCr::Cb), //
					mid(x, y, ColorYCbCr::Cr)), //
			(r) -> new Color(convertYCbCrJPEGToRGB(r))//
	)), //
	YPbPr((a, b) -> mix(a, b, //
			(rgb) -> convertRGBtoYPbPr(rgb), //
			(x, y) -> new ColorYPbPr(//
					mid(x, y, ColorYPbPr::Y), //
					mid(x, y, ColorYPbPr::Pb), //
					mid(x, y, ColorYPbPr::Pr)), //
			(r) -> new Color(convertYPbPrtoRGB(r))//
	)), //
	CIE_CMYK((a, b) -> mix(a, b, //
			(rgb) -> convertCMYtoCMYK(convertRGBtoCMY(rgb)), //
			(x, y) -> new ColorCmyk(//
					mid(x, y, ColorCmyk::C), //
					mid(x, y, ColorCmyk::M), //
					mid(x, y, ColorCmyk::Y), //
					mid(x, y, ColorCmyk::K)), //
			(r) -> new Color(convertCMYtoRGB(convertCMYKtoCMY(r)))//
	)), //
	CIE_LUV((a, b) -> mix(a, b, //
			(rgb) -> convertXYZtoCIELuv(convertRGBtoXYZ(rgb)), //
			(x, y) -> new ColorCieLuv(//
					mid(x, y, ColorCieLuv::L), //
					mid(x, y, ColorCieLuv::u), //
					mid(x, y, ColorCieLuv::v)), //
			(r) -> new Color(convertXYZtoRGB(convertCIELuvToXYZ(r)))//
	)),

	/**
	 * Note, cylindrical models, such as "CIE LCH", "HSV", "HSB", "HSL", "NCS" or the Munsell color system
	 * won't work properly.
	 * If the angle of two colors is close to 180°, just like with a gimbal lock you can't properly
	 * decide what direction "the middle" is.
	 */
	;

	private final BinaryOperator<Color> mixer;

	ColorMix(final BinaryOperator<Color> mixer) {
		this.mixer = mixer;
	}

	@Override
	public Color apply(final Color a, final Color b) {
		return mixer.apply(a, b);
	}

	private static int mid(final int x, final int y) {
		return (int) round(0.5 * (x + y));
	}

	private static double mid(final double x, final double y) {
		return 0.5 * (x + y);
	}

	private static <T> int mid(final T x, final T y, final ToIntFunction<T> valueExtractor) {
		return mid(valueExtractor.applyAsInt(x), valueExtractor.applyAsInt(y));
	}

	private static <T> double mid(final T x, final T y, final ToDoubleFunction<T> valueExtractor) {
		return mid(valueExtractor.applyAsDouble(x), valueExtractor.applyAsDouble(y));
	}

	private static <T> Color mix(final Color a, final Color b, //
			final Function<Color, T> converter, //
			final BinaryOperator<T> mixer, //
			final Function<T, Color> reconverter) {
		final T x = converter.apply(a);
		final T y = converter.apply(b);
		final T mix = mixer.apply(x, y);
		return reconverter.apply(mix);
	}

}
