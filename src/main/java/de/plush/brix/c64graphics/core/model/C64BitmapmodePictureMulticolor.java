package de.plush.brix.c64graphics.core.model;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;
import static java.lang.System.arraycopy;

import java.awt.Dimension;
import java.net.*;

import de.plush.brix.c64graphics.core.pipeline.stages.compression.*;
import de.plush.brix.c64graphics.core.util.*;

/**
 * This is what makes up a typical Koala Image.
 *
 * @author Wanja Gayk
 */
public class C64BitmapmodePictureMulticolor implements IRollBlockwise {

	private final C64BitmapmodePictureHires bitmapScreen;

	private final C64ColorRam colorRam;

	private final byte backgroundColorCode;

	public C64BitmapmodePictureMulticolor(final C64Bitmap bitmap, final C64Screen screen) {
		this(bitmap, screen, new C64ColorRam((byte) 0));
	}

	public C64BitmapmodePictureMulticolor(final C64BitmapmodePictureHires bitmapScreen, final C64ColorRam colorRam) {
		this(bitmapScreen, colorRam, (byte) 0);
	}

	public C64BitmapmodePictureMulticolor(final C64BitmapmodePictureHires bitmapScreen, final C64ColorRam colorRam, final byte backgroundColorCode) {
		this(bitmapScreen.bitmap(), bitmapScreen.screen(), colorRam, backgroundColorCode);
	}

	public C64BitmapmodePictureMulticolor(final C64Bitmap bitmap, final C64Screen screen, final C64ColorRam colorRam) {
		this(bitmap, screen, colorRam, (byte) 0);
	}

	public C64BitmapmodePictureMulticolor(final C64Bitmap bitmap, final C64Screen screen, final C64ColorRam colorRam, final byte backgroundColorCode) {
		bitmapScreen = new C64BitmapmodePictureHires(bitmap, screen);
		this.colorRam = new C64ColorRam(colorRam);
		this.backgroundColorCode = backgroundColorCode;
	}

	public C64BitmapmodePictureMulticolor(final C64BitmapmodePictureMulticolor other) {
		bitmapScreen = new C64BitmapmodePictureHires(other.bitmapScreen);
		colorRam = new C64ColorRam(other.colorRam);
		backgroundColorCode = other.backgroundColorCode;
	}

	public Dimension sizeBlocks() {
		return C64Bitmap.sizeBlocks();
	}

	public Dimension sizePixels() {
		return C64Bitmap.sizePixels();
	}

	public void setBitmapBlock(final int xBlock, final int yBlock, final C64Char chrBlock) {
		bitmapScreen.setBitmapBlock(xBlock, yBlock, chrBlock);
	}

	public C64Char bitmapBlockAt(final int xBlock, final int yBlock) {
		return bitmapScreen.bitmapBlockAt(xBlock, yBlock);
	}

	public void setScreenCode(final int x, final int y, final byte screenCode) {
		bitmapScreen.setScreenCode(x, y, screenCode);
	}

	public byte screenCodeAt(final int x, final int y) {
		return bitmapScreen.screenCodeAt(x, y);
	}

	public byte[][] screenCodeMatrix() {
		return bitmapScreen.screenCodeMatrix();
	}

	public void setColorCode(final int x, final int y, final byte colorCode) {
		colorRam.setColorCode(x, y, colorCode);
	}

	public byte colorCodeAt(final int x, final int y) {
		return colorRam.colorCodeAt(x, y);
	}

	public byte[][] colorCodeMatrix() {
		return colorRam.colorCodeMatrix();
	}

	/** @return a snapshot of the current bitmap. */
	public C64Bitmap bitmap() {
		return bitmapScreen.bitmap();
	}

	/** @return a snapshot of the current screen. */
	public C64Screen screen() {
		return bitmapScreen.screen();
	}

	/** @return a snapshot of the current colorRam. */
	public C64ColorRam colorRam() {
		return new C64ColorRam(colorRam);
	}

	public byte backgroundColorCode() {
		return backgroundColorCode;
	}

	@Override
	public void rollLeftBlockwise(final int n) {
		bitmapScreen.rollLeftBlockwise(n);
		colorRam.rollLeftBlockwise(n);
	}

	@Override
	public void rollRightBlockwise(final int n) {
		bitmapScreen.rollRightBlockwise(n);
		colorRam.rollRightBlockwise(n);
	}

	@Override
	public void rollUpBlockwise(final int n) {
		bitmapScreen.rollUpBlockwise(n);
		colorRam.rollUpBlockwise(n);
	}

	@Override
	public void rollDownBlockwise(final int n) {
		bitmapScreen.rollDownBlockwise(n);
		colorRam.rollDownBlockwise(n);
	}

	/**
	 * From <a href=
	 * "http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03">http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03</a>:<br>
	 * <style> #gfxformat th { background:lightgray; text-align: left; width:auto}</style> <style> #gfxformat td.first { width:18ch; }</style><style> #gfxformat
	 * td { width:auto}</style>
	 * <table id="gfxformat">
	 * <th colspan="2">Koalapainter (by Audio Light for Koala Technologies Corp.) (pc-ext: .kla)</th>
	 * <tr>
	 * <td class="first">$6000 - $8710</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$6000- $7f3f</td>
	 * <td>Bitmap</td>
	 * <tr>
	 * <td class="first">$7f40 - $8327</td>
	 * <td>Screen RAM</td>
	 * <tr>
	 * <td class="first">$8328 - $870f</td>
	 * <td>Color RAM</td>
	 * <tr>
	 * <td class="first">$8710</td>
	 * <td>Background</td>
	 * </table>
	 *
	 * @return A provider that will provide the C64 binary of this image at the time this method was invoked.
	 */
	public C64BinaryProvider binaryProviderKoala() {
		return koalaFormat().binaryProvider(this);
	}

	public void saveKoala(final URI loc, final StartAddress startAddress) {
		koalaFormat().save(binaryProviderKoala(), loc, startAddress);
	}

	public void saveAmicaPaint(final URI loc, final StartAddress startAddress) {
		koalaFormat().save(binaryProviderAmicaPaint(), loc, startAddress);
	}

	public static C64BitmapmodePictureMulticolor loadKoala(final URL loc, final StartAddress startAddress) {
		return koalaFormat().load(Utils.readFile(loc, startAddress));
	}

	public static C64BitmapmodePictureMulticolor loadKoala(final URI loc, final StartAddress startAddress) {
		return koalaFormat().load(Utils.readFile(loc, startAddress));
	}

	public static C64BitmapmodePictureMulticolor loadAmicaPaint(final URL loc, final StartAddress startAddress) {
		return koalaFormat().load(new RLEdepacker(toUnsignedByte(0xC2)).apply(Utils.readFile(loc, startAddress)));
	}

	public static C64BitmapmodePictureMulticolor loadAmicaPaint(final URI loc, final StartAddress startAddress) {
		return koalaFormat().load(new RLEdepacker(toUnsignedByte(0xC2)).apply(Utils.readFile(loc, startAddress)));
	}

	public static C64BitmapmodePictureMulticolor fromKoalaBinary(final byte[] binary) {
		return koalaFormat().load(binary);
	}

	public static C64BitmapmodePictureMulticolor fromAmicaPaintBinary(final byte[] binary) {
		return koalaFormat().load(new RLEdepacker(toUnsignedByte(0xC2)).apply(binary));
	}

	private static C64MulticolorFormat koalaFormat() {
		return new C64MulticolorFormat()//
				.withLoadAddress(0x6000, 0x8710)//
				.withBitmapAddress(0x6000)//
				.withScreenAddress(0x7f40)//
				.withCramAddress(0x8328)//
				.withBackgroundAddress(0x8710);
	}

	public C64BinaryProvider binaryProviderAmicaPaint() {
		return () -> new RLEpacker(toUnsignedByte(0xC2)).apply(binaryProviderKoala().toC64Binary());
	}

	/**
	 * From <a href=
	 * "http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03">http://codebase64.org/doku.php?id=base:c64_grafix_files_specs_list_v0.03</a>:<br>
	 * <style> #gfxformat th { background:lightgray; text-align: left; width:auto}</style> <style> #gfxformat td.first { width:18ch; }</style><style> #gfxformat
	 * td { width:auto}</style>
	 * <table id="gfxformat">
	 * <th colspan="2">Image System (Multicolor) (pc-ext: .ism;.ims)</th>
	 * <tr>
	 * <td class="first">$3C00 - $63E8</td>
	 * <td>load address</td>
	 * <tr>
	 * <td class="first">$3C00 - $3FE7</td>
	 * <td>ColorRAM</td>
	 * <tr>
	 * <td class="first">$4000 - $5F3F</td>
	 * <td>Bitmap</td>
	 * <tr>
	 * <td class="first">$5FFF</td>
	 * <td>Background</td>
	 * <tr>
	 * <td class="first">$6000 - $63E7</td>
	 * <td>Screen RAM</td>
	 * </table>
	 *
	 * @return A provider that will provide the C64 binary of this image at the time this method was invoked.
	 */
	public C64BinaryProvider binaryProviderImageSystem() {
		return imageSystemFormat().binaryProvider(this);
	}

	public void saveImageSystem(final URI loc, final StartAddress startAddress) {
		imageSystemFormat().save(binaryProviderImageSystem(), loc, startAddress);
	}

	public static C64BitmapmodePictureMulticolor loadImageSystem(final URL loc, final StartAddress startAddress) {
		return koalaFormat().load(Utils.readFile(loc, startAddress));
	}

	public static C64BitmapmodePictureMulticolor loadImageSystem(final URI loc, final StartAddress startAddress) {
		return koalaFormat().load(Utils.readFile(loc, startAddress));
	}

	public static C64BitmapmodePictureMulticolor fromImageSystemBinary(final byte[] binary) {
		return imageSystemFormat().load(binary);
	}

	private static C64MulticolorFormat imageSystemFormat() {
		return new C64MulticolorFormat()//
				.withLoadAddress(0x3c00, 0x63E8)//
				.withCramAddress(0x3c00)//
				.withBitmapAddress(0x4000)//
				.withBackgroundAddress(0x5fff)//
				.withScreenAddress(0x6000);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (bitmapScreen == null ? 0 : bitmapScreen.hashCode());
		result = prime * result + (colorRam == null ? 0 : colorRam.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final C64BitmapmodePictureMulticolor other = (C64BitmapmodePictureMulticolor) obj;
		if (bitmapScreen == null) {
			if (other.bitmapScreen != null) { return false; }
		} else if (!bitmapScreen.equals(other.bitmapScreen)) { return false; }
		if (colorRam == null) {
			if (other.colorRam != null) { return false; }
		} else if (!colorRam.equals(other.colorRam)) { return false; }
		return true;
	}

	private static class C64MulticolorFormat {
		int loadAddress, endAddress, bitmapAddress, screenAddress, cramAddress, backgroundAddress = -1;

		int length, bitmapOffset, screenOffset, cramOffset, backgroundOffset = -1;

		C64MulticolorFormat withLoadAddress(final int loadAddress, final int endAddress) {
			this.loadAddress = loadAddress;
			this.endAddress = endAddress;
			return this;
		}

		C64MulticolorFormat withBitmapAddress(final int bitmapAddress) {
			this.bitmapAddress = bitmapAddress;
			return this;
		}

		C64MulticolorFormat withScreenAddress(final int screensAddress) {
			screenAddress = screensAddress;
			return this;
		}

		C64MulticolorFormat withCramAddress(final int cramAddress) {
			this.cramAddress = cramAddress;
			return this;
		}

		C64MulticolorFormat withBackgroundAddress(final int backgroundAddress) {
			this.backgroundAddress = backgroundAddress;
			return this;
		}

		void calcOffsets() {
			length = endAddress - loadAddress + 1; // $4000 - $4000 is one byte in the codebase64 spec.
			bitmapOffset = bitmapAddress - loadAddress;
			screenOffset = screenAddress - loadAddress;
			cramOffset = cramAddress - loadAddress;
			backgroundOffset = backgroundAddress < 0 ? -1 : backgroundAddress - loadAddress;
		}

		C64BitmapmodePictureMulticolor load(final byte[] binary) {
			calcOffsets();
			final byte[] bitmapBinary = new byte[C64Bitmap.WIDTH_BLOCKS * C64Bitmap.HEIGHT_BLOCKS * C64Char.HEIGHT_PIXELS];
			System.arraycopy(binary, bitmapOffset, bitmapBinary, 0, bitmapBinary.length);
			final C64Bitmap bitmap = C64Bitmap.binaryToC64Bitmap(bitmapBinary);

			final byte[] screenBinary = new byte[C64Screen.WIDTH_BLOCKS * C64Screen.HEIGHT_BLOCKS];
			System.arraycopy(binary, screenOffset, screenBinary, 0, screenBinary.length);
			final C64Screen screen = C64Screen.fromBinary(screenBinary);

			final byte[] cramBinary = new byte[C64Screen.WIDTH_BLOCKS * C64Screen.HEIGHT_BLOCKS];
			System.arraycopy(binary, cramOffset, cramBinary, 0, cramBinary.length);
			final C64ColorRam cram = C64ColorRam.binaryToC64ColorRam(cramBinary);

			final byte backgroundColorCode = backgroundOffset < 0 ? (byte) 0 : binary[backgroundOffset];
			return new C64BitmapmodePictureMulticolor(bitmap, screen, cram, backgroundColorCode);
		}

		void save(final C64BinaryProvider binaryProvider, final URI uri, final StartAddress startAddress) {
			switch (startAddress) {
			case InFile -> Utils.writeFile(uri, binaryProvider.toC64Binary(), loadAddress);
			case NotInFile -> Utils.writeFile(uri, binaryProvider.toC64Binary());
			}
		}

		C64BinaryProvider binaryProvider(final C64BitmapmodePictureMulticolor obj) {
			calcOffsets();
			final byte[] binary = new byte[length];

			final byte[] screenBinary = obj.screen().toC64Binary();
			arraycopy(screenBinary, 0, binary, screenOffset, screenBinary.length);
			final byte[] bitmapBinary = obj.bitmap().toC64Binary();
			arraycopy(bitmapBinary, 0, binary, bitmapOffset, bitmapBinary.length);
			final byte[] cramBinary = obj.colorRam().toC64Binary();
			arraycopy(cramBinary, 0, binary, cramOffset, cramBinary.length);
			if (backgroundOffset >= 0) {
				binary[backgroundOffset] = obj.backgroundColorCode;
			}
			return () -> binary;
		}
	}
}
