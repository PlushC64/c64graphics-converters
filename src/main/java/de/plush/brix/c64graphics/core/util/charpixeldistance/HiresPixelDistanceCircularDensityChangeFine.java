package de.plush.brix.c64graphics.core.util.charpixeldistance;

import static de.plush.brix.c64graphics.core.util.Utils.*;
import static java.lang.Long.bitCount;
import static java.lang.Math.*;
import static java.util.Arrays.copyOfRange;

import java.util.Arrays;
import java.util.stream.IntStream;

import de.plush.brix.c64graphics.core.model.C64Char;

/**
 * Compares characters by separating them into different regions and evaluating the change in pixel density in a circular fashion.<br>
 * This is meant as a secondary or complementary sort criteria, for it only values regional density changes.
 * <p>
 * Example: The following character is split into a left top, a right top, a right bottom and a left bottom square.<br>
 * In the left picture, each square is given a number for its pixel density:<br>
 * 0 = clear, 1 = all pixels set. <br>
 * <img src="doc-files/FineCirular1.png"/><br>
 * In the right picture, starting with the left top in a circular fashion the change in pixel density is determined:<br>
 * negative value = decreasing density, positive value = increasing density.<br>
 * For this image we get "0.3135, 0.375, -0.625, -0.0625". <br>
 * For a comparison see a different, but still rather similar character:<br>
 * <img src="doc-files/FineCirular2.png"/><br>
 * The second character is also classified as: "0.4375, 0.3125, -0.625, -0.125".<br>
 * The distance between these two characters is calculated by normalizing the sum of differences, like this:<br>
 * <code>min(1, IntStream.range(0, 4).map(index -&gt; abs(a[index] - b[index])).sum() / 8.0)</code> <br>
 * In this case the difference between both characters comes out as: <code>0.031125</code>.
 * </p>
 * <p>
 * This third character looks a bit more different:<br>
 * <img src="doc-files/FineCirular3.png"/><br>
 * It is classified as "0.0625, 0.5, -0.5625, 0", the difference to the first one is <code>0.062625</code></br>
 * Watching this third character will demonstrate, how a character that looks almost like a 180� rotation of the first, is also classified as more
 * different:<br>
 * <img src="doc-files/FineCirular4.png"/><br>
 * This one is classified as "-0.75, 0.125, 0.3125, 0.3125", compared to the first character the difference comes out as <code>0.32825</code>.
 * </p>
 * <p>
 * <b>Note that this measure only cares for <u>changes</u> in density.</br>
 * This means that all-blank and all-colored characters are considered equal</b>.
 * </p>
 *
 * @author Wanja Gayk
 *
 */
public class HiresPixelDistanceCircularDensityChangeFine implements C64CharHiresPixelDistanceMeasure {

	@Override
	public double distanceOf(final C64Char c1, final C64Char c2) {
		return c1 == c2 || c1.equals(c2) ? 0 : circularPixelDensity(c1, c2);
	}

	private static double circularPixelDensity(final C64Char a, final C64Char b) {
		final double[] circularChangeA = circularChange(areaCountsClockwise(a.toC64Binary()));
		final double[] circularChangeB = circularChange(areaCountsClockwise(b.toC64Binary()));
		return normalizedSumOfDifferences(circularChangeA, circularChangeB);
	}

	private static double normalizedSumOfDifferences(final double[] circularChangeA, final double[] circularChangeB) {
		return min(1, IntStream.range(0, 4).mapToDouble(index -> abs(circularChangeA[index] - circularChangeB[index])).sum() / 8.0);
	}

	private static int[] areaCountsClockwise(final byte[] xs) {
		final int leftTop = sumOf(copyOfRange(xs, 0, 4), x -> bitCount(toPositiveInt(highNibble(x))));
		final int rightTop = sumOf(copyOfRange(xs, 0, 4), x -> bitCount(toPositiveInt(lowNibble(x))));
		final int rightBottom = sumOf(copyOfRange(xs, 4, 8), x -> bitCount(toPositiveInt(lowNibble(x))));
		final int leftBottom = sumOf(copyOfRange(xs, 4, 8), x -> bitCount(toPositiveInt(highNibble(x))));
		return new int[] { leftTop, rightTop, rightBottom, leftBottom };
	}

	/**
	 * @param pixelCounts
	 *            array of 4 numbers, each a number of pixels per region, each between 0 and 16 (inclusive)
	 * @return array of 4 values, clockwise change of density, each between -1 and 1 (inclusive)
	 */
	private static double[] circularChange(final int[] pixelCounts) {
		final double[] densities = asDensities(pixelCounts); // 4 values, each from 0 to 1 (inclusive)
		// compare in a circular fashion:
		return new double[] { densities[1] - densities[0] //
				, densities[2] - densities[1] //
				, densities[3] - densities[2] //
				, densities[0] - densities[3] //
		};
	}

	/**
	 *
	 * @param pixelCounts
	 *            array of 4 numbers, each a number of pixels per region, each between 0 and 16 (inclusive)
	 * @return array of 4 values, each between 0 and 1 (inclusive)
	 */
	private static double[] asDensities(final int[] pixelCounts) {
		return Arrays.stream(pixelCounts).mapToDouble(x -> x / 16.0).toArray();
	}

	public static void main(final String[] args) {
		// System.out.println(Arrays.toString(asNegZeroPos(new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 })));
		//
		final C64Char half = new C64Char(new byte[] { (byte) 0xaa, (byte) 0xaa, (byte) 0xaa, (byte) 0xaa, (byte) 0xaa, (byte) 0xaa, (byte) 0xaa, (byte) 0xaa });
		final C64Char full = new C64Char(new byte[] { (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff });
		final double a = new HiresPixelDistanceCircularDensityChangeFine().distanceOf(C64Char.BLANK, C64Char.BLANK);
		final double b = new HiresPixelDistanceCircularDensityChangeFine().distanceOf(C64Char.BLANK, half);
		final double c = new HiresPixelDistanceCircularDensityChangeFine().distanceOf(C64Char.BLANK, full);

		System.out.println(a);
		System.out.println(b);
		System.out.println(c);

		System.out.println("--");

		System.out.println(normalizedSumOfDifferences(new double[] { 0.3135, 0.375, -0.625, -0.0625 }, new double[] { 0.4375, 0.3125, -0.625, -0.125 }));
		System.out.println(normalizedSumOfDifferences(new double[] { 0.3135, 0.375, -0.625, -0.0625 }, new double[] { 0.0625, 0.5, -0.5625, 0 }));
		System.out.println(normalizedSumOfDifferences(new double[] { 0.3135, 0.375, -0.625, -0.0625 }, new double[] { -0.75, 0.125, 0.3125, 0.3125 }));
		System.out.println("--");
		System.out.println(normalizedSumOfDifferences(new double[] { 0.4375, 0.3125, -0.625, -0.125 }, new double[] { 0.0625, 0.5, -0.5625, 0 }));
		System.out.println(normalizedSumOfDifferences(new double[] { 0.4375, 0.3125, -0.625, -0.125 }, new double[] { -0.75, 0.125, 0.3125, 0.3125 }));
		System.out.println("--");
		System.out.println(normalizedSumOfDifferences(new double[] { 0.0625, 0.5, -0.5625, 0 }, new double[] { -0.75, 0.125, 0.3125, 0.3125 }));

		// System.out.println(normalizedSumOfDifferences(new double[] { 1, 1, -1, 0 }, new double[] { -1, 0, 1, 1 }));

	}
}
