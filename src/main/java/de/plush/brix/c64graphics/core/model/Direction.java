package de.plush.brix.c64graphics.core.model;

public interface Direction {

	public enum Vertical implements Direction {
		Up, Down
	}

	public enum Horizontal implements Direction {
		Left, Right
	}

	default Displacement by(final int n) {
		return new Displacement(this, n);
	}
}