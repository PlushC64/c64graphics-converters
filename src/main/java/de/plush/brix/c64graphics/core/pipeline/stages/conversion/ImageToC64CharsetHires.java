package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.function.*;

import de.plush.brix.c64graphics.core.model.C64Char;

public class ImageToC64CharsetHires extends AbstractImageToC64Charset {

	private final Supplier<Color> backgroundColorProvider;

	private final Color foreground;

	public ImageToC64CharsetHires(final CharsetFormat grid, final Supplier<Color> backgroundColorProvider, final Color foreground) {
		super(grid);
		this.backgroundColorProvider = backgroundColorProvider;
		this.foreground = foreground;
	}

	@Override
	protected Function<BufferedImage, C64Char> createImageToC64Char() {
		final Color background = backgroundColorProvider.get(); // don't get in constructor, the value may be determined after construction.
		return new ImageToC64CharHires(background, foreground);
	}

}
