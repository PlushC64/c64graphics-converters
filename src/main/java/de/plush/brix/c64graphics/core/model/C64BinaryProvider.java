package de.plush.brix.c64graphics.core.model;

public interface C64BinaryProvider {

	byte[] toC64Binary();
}
