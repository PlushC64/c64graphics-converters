package de.plush.brix.c64graphics.core.emitters;

import java.awt.*;
import java.awt.image.BufferedImage;

public class StarAnimationEmitter extends AbstractGeneratedFramesEmitter {

	private final Dimension dimension;

	private final int branches;

	private final double startAngleRadians;

	private double frameAngleRadians;
	private final Color backgroundColor;
	private final Color pixelColor;


	public StarAnimationEmitter(final int frames, final Dimension dimension, final int branches, final Color backgroundColor, final Color pixelColor) {
		super(frames);
		this.dimension = new Dimension(dimension);
		this.branches = branches;
		this.pixelColor = pixelColor;
		this.backgroundColor = backgroundColor;
		startAngleRadians = Math.PI / 10; // first branch points up
	}

	@Override
	protected BufferedImage createImage(final int frame) {
		frameAngleRadians = -(Math.PI / branches * 2 / frames) * (frame % frames) + startAngleRadians;

		final BufferedImage image = new BufferedImage(dimension.width, dimension.height, BufferedImage.TYPE_INT_ARGB);
		final Graphics2D g = (Graphics2D) image.getGraphics();

		g.setColor(backgroundColor);
		g.fillRect(0, 0, dimension.width, dimension.height);

		g.setColor(pixelColor);
		final int outerRadius = Math.min(dimension.width / 2, dimension.height / 2);
		final int innerRadius = outerRadius / 2;

		final int midX = dimension.width / 2;
		final int midY = dimension.height / 2;

		final int[] xPoints = new int[branches * 2];
		final int[] yPoints = new int[branches * 2];
		for (int side = 0; side < branches * 2; ++side) {
			final double angle = Math.PI / branches * side + frameAngleRadians;
			final int radius = side % 2 == 0 ? outerRadius : innerRadius;
			xPoints[side] = midX - (int) (radius * Math.cos(angle));
			yPoints[side] = midY - (int) (radius * Math.sin(angle));
		}
		g.fillPolygon(xPoints, yPoints, branches * 2);
		return image;
	}

}
