package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.*;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.Palette;

/**
 * Note that the returned Image is of type {link BufferedImage#TYPE_INT_ARGB} and has a transparent background color
 * 
 * @author Wanja Gayk
 */
public class C64SpriteGridToImageHires implements IPipelineStage<C64SpriteGrid, PreProcessedSpriteImage> {

	private final Palette palette;
	private final byte spriteColorCode;
	private final C64SpriteToImageHires c64SpriteToImageHires;

	public C64SpriteGridToImageHires(final Palette palette, final Color spriteColor) {
		this.palette = palette;
		spriteColorCode = palette.colorCodeOrException(spriteColor);
		c64SpriteToImageHires = new C64SpriteToImageHires(palette, spriteColor);
	}

	@Override
	public PreProcessedSpriteImage apply(final C64SpriteGrid spriteGrid) {
		final var result = new PreProcessedSpriteImage(spriteGrid.sizePixels().width, spriteGrid.sizePixels().height, BufferedImage.TYPE_INT_ARGB, palette,
				spriteColorCode, (byte) 0, (byte) 0);
		final Graphics2D g = result.createGraphics();
		try {
			final Dimension blocks = spriteGrid.sizeSprites();
			for (int y = 0; y < blocks.height; ++y) {
				for (int x = 0; x < blocks.width; ++x) {
					final Image spriteImage = c64SpriteToImageHires.apply(spriteGrid.spriteAt(x, y));
					g.drawImage(spriteImage, x * C64Sprite.WIDTH_PIXELS, y * C64Sprite.HEIGHT_PIXELS, null);
				}
			}
			return result;
		} finally {
			g.dispose();
		}
	}

}
