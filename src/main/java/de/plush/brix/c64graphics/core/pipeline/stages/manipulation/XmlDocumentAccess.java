/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.util.function.*;

import org.w3c.dom.Document;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.*;

public class XmlDocumentAccess implements IPipelineStage<String, String> {

	private final Function<Document, String> documentToString = new XmlDocumentToString();
	private final Function<String, Document> stringToDocument = new StringToXmlDocument();
	private final Consumer<Document> docModifier;

	public XmlDocumentAccess(final Consumer<Document> docModifier) {
		this.docModifier = docModifier;
	}

	@Override
	public String apply(final String xmlString) {
		final Document doc = stringToDocument.apply(xmlString);
		docModifier.accept(doc);
		return documentToString.apply(doc);
	}

}