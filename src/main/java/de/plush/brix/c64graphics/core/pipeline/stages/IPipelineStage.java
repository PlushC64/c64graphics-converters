package de.plush.brix.c64graphics.core.pipeline.stages;

import java.io.IOException;
import java.util.Objects;
import java.util.function.Function;

import javax.annotation.WillCloseWhenClosed;

public interface IPipelineStage<In, Out> extends Function<In, Out> {

	/**
	 * Returns a function that always returns its input argument.
	 *
	 * @param <T>
	 *            the type of the input and output objects to the function
	 * @return a function that always returns its input argument
	 */
	static <T> IPipelineStage<T, T> identity() {
		return t -> t;
	}

	@Override
	default <V> IPipelineStage<In, V> andThen(@WillCloseWhenClosed final Function<? super Out, ? extends V> after) {
		final var self = this;
		if (!(self instanceof AutoCloseable && after instanceof AutoCloseable)) {
			return (final In t) -> after.apply(apply(t));
		}
		Objects.requireNonNull(after);
		class CloseablePipelineStage implements IPipelineStage<In, V>, AutoCloseable {
			@Override
			public V apply(final In in) {
				return after.apply(self.apply(in));
			}

			@Override
			public void close() throws Exception {
				// adapted a decompiled try-with-resources block
				Throwable throwable = null;
				try {
					((AutoCloseable) self).close();
				} catch (final IOException ex) {
					throwable = ex;
					throw ex;
				} finally {
					if (throwable != null) {
						try {
							((AutoCloseable) after).close();
						} catch (final Throwable throwable3) {
							throwable.addSuppressed(throwable3);
						}
					} else {
						((AutoCloseable) after).close();
					}
				}
			}
		}
		return new CloseablePipelineStage();
	}

}
