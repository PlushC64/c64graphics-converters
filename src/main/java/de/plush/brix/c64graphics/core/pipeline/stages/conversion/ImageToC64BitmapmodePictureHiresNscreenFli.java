package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;
import static de.plush.brix.c64graphics.core.util.UtilsImage.*;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.*;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes.*;

/**
 * Multicolor Bitmap, 2 screens (FLI-effect every 4 lines), ColorRAM.
 *
 * @author Wanja Gayk
 */
class ImageToC64BitmapmodePictureHiresNscreenFli implements IPipelineStage<BufferedImage, C64BitmapmodePictureHiresNscreenFLI> {

	private final IHiresFLIColorClashResolver colorClashResolver;

	private final Palette palette;

	private List<Color> preferredForegroundColors = emptyList();

	private List<Color> preferredBackgroundColors = emptyList();

	private final int partialImageHeight;

	private final int numScreens;

	/**
	 * Using the standard {@link MulticolorHalfcharFLIColorClashResolver}. <br>
	 * <p>
	 * Use {@link #ImageToC64BitmapmodePictureHiresNscreenFli(int, Palette, IHiresFLIColorClashResolver)} if you want to use a custom color clash resolver.
	 * </p>
	 *
	 * @param palette
	 *            the C64 target palette
	 * @param colorDistanceMeasure
	 *            used to measure how similar/dissimilar a color is.
	 */
	public ImageToC64BitmapmodePictureHiresNscreenFli(final Palette palette, final ColorDistanceMeasure colorDistanceMeasure) {
		this(2, palette, new HiresNscreenFLIColorClashResolver(2, colorDistanceMeasure));
	}

	/**
	 * Allows using a custom color clash resolver.
	 *
	 * @param palette
	 *            the C64 target palette
	 * @param colorClashResolver
	 *            the color clash resolver used for preprocessing.
	 */
	public ImageToC64BitmapmodePictureHiresNscreenFli(final int screens, final Palette palette, final IHiresFLIColorClashResolver colorClashResolver) {
		numScreens = screens;
		this.palette = palette;
		this.colorClashResolver = colorClashResolver;
		partialImageHeight = C64Char.HEIGHT_PIXELS / screens;
	}

	public ImageToC64BitmapmodePictureHiresNscreenFli withPreferredBackgroundColors(final Color... colors) {
		preferredBackgroundColors = asList(colors);
		preferredBackgroundColors.forEach(palette::colorCodeOrException);
		checkSettingsConsistency();
		return this;
	}

	public ImageToC64BitmapmodePictureHiresNscreenFli withPreferredForegroundColors(final Color... colors) {
		preferredForegroundColors = asList(colors);
		preferredForegroundColors.forEach(palette::colorCodeOrException);
		checkSettingsConsistency();
		return this;
	}

	private void checkSettingsConsistency() {
		crossCheck(preferredForegroundColors, preferredBackgroundColors, x -> "You can't force these colors to be in both foreground and background: " + x);
	}

	private static void crossCheck(final Collection<Color> a, final Collection<Color> b, final Function<Collection<Color>, String> messageProvider) {
		final Set<Color> x = a.stream().filter(b::contains).collect(toSet());
		if (!x.isEmpty()) {
			throw new IllegalArgumentException(messageProvider.apply(x));
		}
	}

	@SuppressWarnings("checkstyle:CyclomaticComplexity") // found no meaningful way to extract methods that make it more readable
	@Override
	public C64BitmapmodePictureHiresNscreenFLI apply(final BufferedImage original) {
		final BufferedImage sanitized = colorClashResolver.apply(original);

		// new ImageDisplay<>(2f, "colorClashedRemoved", Grid.Invisible).show(sanitized, null);

		final C64Bitmap bitmap = new C64Bitmap();
		final C64Screen[] screens = IntStream.range(0, C64Char.HEIGHT_PIXELS / partialImageHeight)//
				.mapToObj(__ -> new C64Screen((byte) 0))//
				.toArray(C64Screen[]::new);

		final Dimension blocks = C64Bitmap.sizeBlocks();
		for (int y = 0; y < blocks.height; ++y) {
			// Start at char 3 (FLI-bug):
			for (int x = 3; x < blocks.width; ++x) {
				final byte[] blockBytes = new byte[C64Char.HEIGHT_PIXELS];
				for (int lineStart = 0; lineStart < 8; lineStart += partialImageHeight) {
					try {
						final int xPix = x * C64Char.WIDTH_PIXELS;
						final int yPix = y * C64Char.HEIGHT_PIXELS + lineStart;
						final BufferedImage partialImage = sanitized.getSubimage(xPix, yPix, C64Char.WIDTH_PIXELS, partialImageHeight);

						final List<Color> colors = colors(partialImage).stream().collect(toList());
						if (colors.size() == 1) {
							colors.add(palette.color((byte) 0)); // default
						}
						colors.sort(UtilsColor.SORT_BY_BRIGHTNESS); // to get some predictable patterns
						Color background = preferredBackgroundColors.stream().filter(colors::contains).findFirst().orElse(null);
						Color foreground = preferredForegroundColors.stream().filter(colors::contains).findFirst().orElse(null);
						Stream.of(background, foreground).filter(c -> c != null).forEach(colors::remove);
						// list of colors has no preferred color left, from here we can assign colors that are not preferred by anyone to empty slots
						final Iterator<Color> colorsLeft = colors.iterator();
						background = background == null ? colorsLeft.hasNext() ? colorsLeft.next() : palette.color((byte) 0) : background;
						foreground = foreground == null ? colorsLeft.hasNext() ? colorsLeft.next() : palette.color((byte) 0) : foreground;
						Stream.of(background, foreground).forEach(colors::remove);

						for (int line = lineStart; line < lineStart + partialImageHeight; ++line) {
							final C64Char block = new ImageToC64CharHires(background, foreground)
									.apply(repetitiveBlockImage(partialImage.getSubimage(0, line - lineStart, C64Char.WIDTH_PIXELS, 1)));
							blockBytes[line] = block.toC64Binary()[lineStart];
						}
						screens[lineStart / partialImageHeight].setScreenCode(x, y,
								toUnsignedByte(palette.colorCodeOrException(background) << 4 | palette.colorCodeOrException(foreground)));
					} catch (final Exception e) {
						throw new IllegalStateException("In Block x=" + x + ", y=" + y + ", line=" + lineStart, e);
					}
				}
				bitmap.setBlock(x, y, new C64Char(blockBytes));
			}
		}
		return new C64BitmapmodePictureHiresNscreenFLI(numScreens, bitmap, screens);
	}

}
