package de.plush.brix.c64graphics.core.util;

import static java.lang.Byte.toUnsignedInt;
import static java.lang.Math.*;
import static java.lang.String.format;
import static java.nio.file.Files.readAllBytes;
import static java.util.Arrays.*;
import static java.util.Objects.requireNonNull;
import static java.util.function.Predicate.not;
import static java.util.stream.Collectors.*;

import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.*;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.*;
import java.util.stream.*;

import javax.annotation.WillClose;

import de.plush.brix.c64graphics.core.util.collections.ByteArrayList;
import de.plush.brix.c64graphics.core.util.functions.*;

@SuppressWarnings({ "checkstyle:classDataAbstractionCoupling", "classFanOutComplexity" })
public class Utils {

	private static final float PI = 3.1415927f;

	private static final float PI_2 = PI / 2f;

	private static final float MINUS_PI_2 = -PI_2;

	protected Utils() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Kappa.atan2 : Average Error 0.00231 / Largest Error 0.00488, claims to be about 10 times as fast as {@link Math#atan2}
	 *
	 * @see <a href = "http://www.java-gaming.org/index.php?topic=36467.0">http://www.java-gaming.org/index.php?topic=36467.0</a>
	 * @see Math#atan2(double, double)
	 * @param y
	 *            the ordinate coordinatex the abscissa coordinate
	 * @param x
	 *            the abscissa coordinate
	 * @return the theta component of the point (r, theta) in polar coordinates that corresponds to the point (x, y) in Cartesian coordinates.
	 */
	public static final float fastAtan2(final float y, final float x) {
		if (x == 0.0f) {
			if (y > 0.0f) { return PI_2; }
			if (y == 0.0f) { return 0.0f; }
			return MINUS_PI_2;
		}

		final float atan;
		final float z = y / x;
		if (Math.abs(z) < 1.0f) {
			atan = z / (1.0f + 0.28f * z * z);
			if (x < 0.0f) { return y < 0.0f ? atan - PI : atan + PI; }
			return atan;
		}
		atan = PI_2 - z / (z * z + 0.28f);
		return y < 0.0f ? atan - PI : atan;
	}

	/**
	 * Return the least power of two greater than or equal to the specified value.
	 *
	 * <p>
	 * Note that this function will return 1 when the argument is 0.
	 *
	 * @param x
	 *            a long integer smaller than or equal to 2<sup>62</sup>.
	 * @return the least power of two greater than or equal to the specified value.
	 */
	public static long nextPowerOfTwo(long x) {
		if (x == 0) { return 1; }
		x--;
		x |= x >> 1;
		x |= x >> 2;
		x |= x >> 4;
		x |= x >> 8;
		x |= x >> 16;
		return (x | x >> 32) + 1;
	}

	public static int hashCodeOfRegion(final byte[] bytes, final int start, final int end) {
		if (bytes == null) { return 0; }
		int result = 1;
		for (int t = start; t < end; ++t) {
			result = 31 * result + bytes[t];
		}
		return result;
	}

	public static int hashCodeOfRegion(final int[] integers, final int start, final int end) {
		if (integers == null) { return 0; }
		int result = 1;
		for (int t = start; t < end; ++t) {
			result = 31 * result + integers[t];
		}
		return result;
	}

	public static int hashCodeOfRegion(final double[] doubles, final int start, final int end) {
		if (doubles == null) { return 0; }
		int result = 1;
		for (int t = start; t < end; ++t) {
			final long bits = Double.doubleToLongBits(doubles[t]);
			result = 31 * result + (int) (bits ^ bits >>> 32);
		}
		return result;
	}

	/** sum = for(0 to x){ sum+= accu( a[x], b[x]) }. */
	public static int sumOf(final byte[] a, final byte[] b, final ByteToIntBiFunction accumulator) {
		if (a.length != b.length) { throw new IllegalStateException("byte[] lengths not equal: " + a.length + " != " + b.length); }
		int sum = 0;
		for (int t = 0; t < a.length; ++t) {
			sum += accumulator.apply(a[t], b[t]);
		}
		return sum;
	}

	/** sum = for(0 to x){ sum+= accu( a[x]) }. */
	public static int sumOf(final byte[] a, final ByteToIntFunction accumulator) {
		int distance = 0;
		for (int t = 0; t < a.length; ++t) {
			distance += accumulator.valueOf(a[t]);
		}
		return distance;
	}

	public static String toHexString(final byte[] bytes) {
		return ByteArrayList.of(bytes).mapToObj(Utils::toHexString).collect(joining(","));
	}

	public static String toHexString(final Byte b) {
		return format("%02x", b.intValue() & 0xff);
	}

	public static String toHexString(final byte b) {
		return format("%02x", Byte.toUnsignedInt(b));
	}

	public static String toHexString(final int i) {
		final var abs = Math.abs(i);
		final var width = abs <= 0xff ? 2 : abs <= 0xffff ? 4 : 8;
		return format("%0" + width + "x", i);
	}

	public static String toBinaryString(final Byte b) {
		final StringBuilder sb = new StringBuilder(8);
		short pattern = (short) 0x80;
		for (int x = 0; x < 8; ++x) {
			sb.append((pattern & b) == 0 ? '0' : '1');
			pattern >>= 1;
		}
		return sb.toString();
	}

	/**
	 * Converts a signed int to an unsigned byte.
	 *
	 * @param value
	 *            a signed int value
	 * @return an unsigned byte that represents the lowest 8 bits of the given int.
	 */
	public static byte toUnsignedByte(final int value) {
		return (byte) (0xFF & value);
	}

	public static byte highByte(final int value16Bit) {
		return toUnsignedByte(value16Bit >> 8 & 0xff);
	}

	public static byte lowByte(final int value16Bit) {
		return toUnsignedByte(value16Bit & 0xff);
	}

	public static byte hiLoNibbleToByte(final byte hiNibble, final byte lowNibble) {
		final int mask = 0b00001111;
		return toUnsignedByte((hiNibble & mask) << 4 | lowNibble & mask);
	}

	public static byte lowNibble(final byte value8Bit) {
		return toUnsignedByte(value8Bit & 0x0f);
	}

	public static byte highNibble(final byte value8Bit) {
		return toUnsignedByte(value8Bit & 0xf0);
	}

	public static byte or(final byte b, final int mask) {
		return toUnsignedByte(toPositiveInt(b) | mask);
	}

	public static byte setBit(final byte b, final int bit, final boolean value) {
		if (value) { return toUnsignedByte(toPositiveInt(b) | 1 << bit); }
		return toUnsignedByte(toPositiveInt(b) & ~(1 << bit));
	}

	/**
	 * Converts an unsigned byte to a positive integer.
	 *
	 * @param value
	 *            an unsigned byte value
	 * @return an int that represents the unsigned byte's value.
	 */
	public static int toPositiveInt(final byte value) {
		return Byte.toUnsignedInt(value);
	}

	public static int bitCount(final byte value) {
		int bits = value;
		bits = bits - (bits >>> 1 & 0b01010101); // building partial sums 2bit nibbles by adding even and odd bits.
		bits = (bits & 0b00110011) + (bits >> 2 & 0b00110011); // building partial sums of 4 bit nibbles by adding partial sums of 2 bit nibbles
		bits = (bits & 0b00001111) + (bits >> 4); // building sum of byte by adding partial sums of 4 bit nibbles
		return bits;
		/*
		 * Above is a slightly optimized version of:
		 * int val = Byte.toUnsignedInt(value);
		 * val = (val & 0b01010101) + ((val >> 1) & 0b01010101); // building partial sums 2bit nibbles by adding even and odd bits.
		 * val = (val & 0b00110011) + ((val >> 2) & 0b00110011); // building partial sums of 4 bit nibbles by adding partial sums of 2 bit nibbles
		 * val = (val & 0b00001111) + ((val >> 4) & 0b00001111); // building sum of byte by adding partial sums of 4 bit nibbles
		 * return val;
		 */
	}

	public static byte[][] deepCopy(final byte[][] original) {
		final byte[][] copy = original.clone();
		for (int t = 0; t < original.length; ++t) {
			copy[t] = original[t].clone();
		}
		return copy;
	}

	public static int[][] deepCopy(final int[][] original) {
		final int[][] copy = original.clone();
		for (int t = 0; t < original.length; ++t) {
			copy[t] = original[t].clone();
		}
		return copy;
	}

	public static double[][] deepCopy(final double[][] original) {
		final double[][] copy = original.clone();
		for (int t = 0; t < original.length; ++t) {
			copy[t] = original[t].clone();
		}
		return copy;
	}

	public static <T> T[][] deepCopy(final T[][] original) {
		final T[][] copy = original.clone();
		for (int t = 0; t < original.length; ++t) {
			copy[t] = original[t].clone();
		}
		return copy;
	}

	@SuppressWarnings("unchecked")
	public static <T> T copyFromCopyConstructorOrOriginal(final T original) {
		try {
			return (T) original.getClass().getConstructor(original.getClass()).newInstance(original);
		} catch (final Exception e) {
			return original;
		}
	}

	public static byte rollBitsLeft(final byte b, int n) {
		if (n == 0) { return b; }
		if (n < 0) { return rollBitsRight(b, -n); }
		n = n % Byte.SIZE;
		final byte shifted = (byte) (b << n & 0xff);
		final byte rolledIn = (byte) (toUnsignedInt(b) >>> Byte.SIZE - n & 0xff);
		return (byte) (shifted | rolledIn);
	}

	public static byte rollBitsRight(final byte b, int n) {
		if (n == 0) { return b; }
		if (n < 0) { return rollBitsLeft(b, -n); }
		n = n % Byte.SIZE;
		final byte shifted = (byte) (toUnsignedInt(b) >>> n & 0xff);
		final byte rolledIn = (byte) (b << Byte.SIZE - n & 0xff);
		return (byte) (shifted | rolledIn);
	}

	public static void rollLeft(final byte[] array, int n) {
		if (array.length > 1) {
			if (n < 0) {
				rollRight(array, -n);
			} else {
				n = n % array.length;
				if (n > 0) {
					final byte[] left = Arrays.copyOf(array, n);
					System.arraycopy(array, n, array, 0, array.length - n);
					System.arraycopy(left, 0, array, array.length - n, n);
				}
			}
		}
	}

	public static <T> void rollLeft(final T[] array, int n) {
		if (array.length > 1) {
			if (n < 0) {
				rollRight(array, -n);
			} else {
				n = n % array.length;
				if (n > 0) {
					final T[] left = Arrays.copyOf(array, n);
					System.arraycopy(array, n, array, 0, array.length - n);
					System.arraycopy(left, 0, array, array.length - n, n);
				}
			}
		}
	}

	public static void rollRight(final byte[] array, int n) {
		if (array.length > 1) {
			if (n < 0) {
				rollLeft(array, -n);
			} else if (n > 0) {
				n = n % array.length;
				final byte[] right = Arrays.copyOfRange(array, array.length - n, array.length);
				System.arraycopy(array, 0, array, n, array.length - n);
				System.arraycopy(right, 0, array, 0, n);
			}
		}
	}

	public static <T> void rollRight(final T[] array, int n) {
		if (array.length > 1) {
			if (n < 0) {
				rollLeft(array, -n);
			} else {
				n = n % array.length;
				if (n > 0) {
					final T[] right = Arrays.copyOfRange(array, array.length - n, array.length);
					System.arraycopy(array, 0, array, n, array.length - n);
					System.arraycopy(right, 0, array, 0, n);
				}
			}
		}
	}

	/** reverses the bit order, like: <tt>11010100 -&gt; 00101011</tt> */
	public static byte reverseBitOrder(final byte b) {
		byte x = b;
		for (int left = 0; left < Byte.SIZE; ++left) {
			final var right = Byte.SIZE - 1 - left;
			x = setBit(x, left, testBit(b, right));
		}
		return x;
	}

	public static byte[] reversedCopy(final byte[] bytes) {
		final var copy = copyOf(bytes, bytes.length);
		reverse(copy);
		return copy;
	}

	public static void reverse(final byte[] bytes) {
		for (int left = 0, mid = bytes.length >>> 1, right = bytes.length - 1; left < mid; ++left, --right) {
			final byte tmp = bytes[left];
			bytes[left] = bytes[right];
			bytes[right] = tmp;
		}
	}

	public static void applyToEach(final byte[] bytes, final ByteToByteFunction transform) {
		for (int t = 0; t < bytes.length; ++t) {
			bytes[t] = transform.valueOf(bytes[t]);
		}
	}

	public static byte[] transformedCopy(final byte[] bytes, final ByteToByteFunction transform) {
		final byte[] copy = Arrays.copyOf(bytes, bytes.length);
		applyToEach(copy, transform);
		return copy;
	}

	/** @return a combined copy. */
	public static byte[] and(final byte[] bytesA, final byte[] bytesB) {
		return combine(bytesA, bytesB, (a, b) -> (byte) (a & b));
	}

	/** @return a combined copy. */
	public static byte[] or(final byte[] bytesA, final byte[] bytesB) {
		return combine(bytesA, bytesB, (a, b) -> (byte) (a | b));
	}

	/** @return a combined copy. */
	public static byte[] xor(final byte[] bytesA, final byte[] bytesB) {
		return combine(bytesA, bytesB, (a, b) -> (byte) (a ^ b));
	}

	/** @return a combined copy. */
	public static byte[] combine(final byte[] bytesA, final byte[] bytesB, final ByteByteToByteFunction combiner) {
		final byte[] result = Arrays.copyOf(bytesA, max(bytesA.length, bytesB.length));
		for (int x = 0; x < bytesB.length; ++x) {
			result[x] = combiner.valueOf(result[x], bytesB[x]);
		}
		return result;
	}

	public static byte[] toByteArray(final Collection<Byte> source) {
		final byte[] binary = new byte[source.size()];
		int t = 0;
		for (final Byte b : source) {
			binary[t++] = b;
		}
		return binary;
	}

	public static <T> T[] toArray(final T[] a, final T[] b) {
		final var result = Arrays.copyOf(a, a.length + b.length);
		System.arraycopy(b, 0, result, a.length, b.length);
		return result;
	}

	public static byte[] toByteArray(final byte[] a, final byte[] b) {
		final var result = Arrays.copyOf(a, a.length + b.length);
		System.arraycopy(b, 0, result, a.length, b.length);
		return result;
	}

	public static byte[] toByteArray(final byte[]... arrays) {
		return toByteArray(Stream.of(arrays));
	}

	public static byte[] toByteArray(final Stream<byte[]> byteArrayStream) {
		final BiFunction<byte[], byte[], byte[]> accumulator = (acc, x) -> toByteArray(acc, x);
		return toByteArray(byteArrayStream, accumulator);
	}

	public static <T> byte[] toByteArray(final Stream<T> objectStream, final Function<T, byte[]> objectToByteArray) {
		final BiFunction<byte[], T, byte[]> accumulator = (acc, x) -> toByteArray(acc, objectToByteArray.apply(x));
		return toByteArray(objectStream, accumulator);
	}

	private static <T> byte[] toByteArray(final Stream<T> byteArrayStream, final BiFunction<byte[], T, byte[]> accumulator) {
		final BinaryOperator<byte[]> combiner = (left, right) -> toByteArray(left, right);
		return byteArrayStream.reduce(new byte[0], accumulator, combiner);
	}

	public static String toString(final int[][] matrix) {
		if (matrix.length == 0) { return "[[]]"; }
		// assume matrix is uniform and typical value is a 2 digit number - last comma+space:
		final var lineFiguresInclCommasAndSpaces = (matrix[0].length * 4 - 2) * matrix.length;
		final var lineBracketing = 3 * matrix.length; // [+]+newline=3
		final StringBuilder sb = new StringBuilder(lineFiguresInclCommasAndSpaces + lineBracketing + 3);
		sb.append("[\n");
		for (int y = 0; y < matrix.length; ++y) {
			sb.append(Arrays.toString(matrix[y]));
			sb.append('\n');
		}
		sb.append(']');
		return sb.toString();
	}

	public static byte leastOccuringByteIn(final Iterable<byte[]> chunks) {
		return leastOccuringByteIn(StreamSupport.stream(chunks.spliterator(), true));
	}

	public static byte leastOccuringByteIn(final Stream<byte[]> chunks) {
		final long[] counts = byteCount(chunks);
		long min = Long.MAX_VALUE;
		int minIndex = -1;
		for (int t = 0; t < counts.length; ++t) {
			if (counts[t] < min) {
				min = counts[t];
				minIndex = t;
			}
		}
		return toUnsignedByte(minIndex);
	}

	/** returns an array of length 256 (there exist 256 bytes), where each entry of the 256 entries holds the count for the byte. */
	public static long[] byteCount(final Iterable<byte[]> chunks) {
		return byteCount(StreamSupport.stream(chunks.spliterator(), true));
	}

	/** returns an array of length 256 (there exist 256 bytes), where each entry of the 256 entries holds the count for the byte. */
	public static long[] byteCount(final Stream<byte[]> chunks) {
		return chunks.reduce(new long[256], (counts, chunk) -> {
			for (final byte b : chunk) {
				counts[toPositiveInt(b)]++;
			}
			return counts;
		}, (counts1, counts2) -> {
			for (int t = 0; t < 256; ++t) {
				counts1[t] += counts2[t];
			}
			return counts1;
		});
	}

	public static void sortUnsigned(final byte[] bytes, final int left, final int right) {
		final int[] counts = new int[0x100];
		for (int t = left; t < right; ++t) {
			final int index = Utils.toPositiveInt(bytes[t]);
			counts[index] += 1;
		}
		int x = left - 1;
		for (int t = 0; t < 0x100; ++t) {
			var count = counts[t];
			while (--count > -1) {
				bytes[++x] = Utils.toUnsignedByte(t);
			}
		}
		// we could use insertionSort for arrays smaller than 30 bytes, as Arrays.sort does, but I think this is good enough
	}

	public static boolean testBit(final byte b, final int n) {
		return (b & (byte) 1 << n) != (byte) 0;
	}

	public static <N> Optional<N> breadthFirstSearch(final N root, final Function<N, Stream<N>> childrenOfNode, final Predicate<N> matcher) {
		final var discovered = new HashSet<N>();
		final Queue<N> q = new ArrayDeque<>();
		discovered.add(root);
		q.offer(root);
		while (!q.isEmpty()) {
			final var node = q.poll();
			if (matcher.test(node)) { return Optional.of(node); }
			childrenOfNode.apply(node).filter(not(discovered::contains))//
					.peek(discovered::add)//
					.forEach(q::offer);
		}
		return Optional.empty();
	}

	public static void printCharCountStatistics(final List<? extends CountableObject<? extends Object>> items) {
		System.out.println("(value - count):");
		Collections.sort(items);
		Collections.reverse(items);
		for (final CountableObject<? extends Object> item : items) {
			final String pattern = item.toString().replaceAll("\n$", "");
			System.out.println(pattern + " - " + item.count + "\n");
		}
	}

	public static <T> T defaultIfNull(final T object, final T _default) {
		return object != null ? object : _default;
	}

	public static void close(@WillClose final List<? extends AutoCloseable> closeables) throws IllegalStateException {
		try {
			_close(closeables);
		} catch (final Exception e) {
			throw new IllegalStateException(e);
		}
	}

	private static void _close(@WillClose final List<? extends AutoCloseable> closeables) throws Exception {
		if (closeables.isEmpty()) { return; }
		try (AutoCloseable x = closeables.get(closeables.size() - 1)) {
			_close(closeables.subList(0, closeables.size() - 1));
		}
	}

	public static <T> Stream<List<T>> pagesOfN(final List<T> list, final int pageSize) {
		return IntStream.range(0, (list.size() + pageSize - 1) / pageSize)//
				.mapToObj(i -> List.copyOf(list.subList(i * pageSize, min(pageSize * (i + 1), list.size()))));
	}

	public static <T> Stream<Set<T>> subsetStream(final Collection<T> superSet, final int targetSetSize) {
		return subsetStream(superSet, targetSetSize, HashSet::new);
	}

	public static <C extends Collection<T>, T> Stream<C> subsetStream(final Collection<T> superSet, final int targetSetSize,
			final IntFunction<C> combinationCollectionProducer) {
		final List<T> _superSet = superSet instanceof List ? (List<T>) superSet : new ArrayList<>(superSet);
		return asStream(new CombinationIterator<>(_superSet, min(superSet.size(), targetSetSize), combinationCollectionProducer), false);
	}

	public static <T> Set<List<T>> permutations(final Collection<T> _original) {
		final List<T> original = _original instanceof List ? (List<T>) _original : new ArrayList<>(_original);
		final Set<List<T>> results = new LinkedHashSet<>();
		if (original.size() == 1) {
			results.add(asList(original.get(0)));
		} else {
			for (int i = 0; i < original.size(); ++i) {
				final T first = original.get(i);
				final List<T> remains = Stream.concat(original.stream().limit(i), original.stream().skip(i + 1)).collect(toList());
				permutations(remains).stream()//
						.map(innerPermutation -> Stream.concat(Stream.of(first), innerPermutation.stream()).collect(toList()))//
						.collect(toCollection(() -> results));
			}
		}
		return results;
	}

	public static class WeightedObject<T> {
		public final T object;

		public final double weight;

		public WeightedObject(final T object, final double weight) {
			this.object = object;
			this.weight = weight;
		}

		public double weight() {
			return weight;
		}

		public T object() {
			// System.out.println(object);
			return object;
		}
	}

	@Deprecated()
	public static void writeFile(final String filePath, final byte[] fileBytes) {
		writeFile(filePath, List.of(fileBytes)); // asList will create varargs warning
	}

	public static void writeFile(final URI uri, final byte[] fileBytes) {
		writeFile(uri, List.of(fileBytes)); // asList will create varargs warning
	}

	public static void writeFile(final File filePath, final byte[] fileBytes) {
		writeFile(filePath, List.of(fileBytes)); // asList will create varargs warning
	}

	public static void writeFile(final Path path, final byte[] fileBytes) {
		writeFile(path, List.of(fileBytes)); // asList will create varargs warning
	}

	@Deprecated
	public static void writeFile(final String filePath, final byte[] fileBytes, final int includedLoadAdress) {
		check16BitRange(includedLoadAdress, "start address");
		check16BitRange(includedLoadAdress + fileBytes.length, "end address");
		writeFile(filePath, asList(new byte[] { lowByte(includedLoadAdress), highByte(includedLoadAdress) }, fileBytes));
	}

	public static void writeFile(final URI uri, final byte[] fileBytes, final int includedLoadAdress) {
		check16BitRange(includedLoadAdress, "start address");
		check16BitRange(includedLoadAdress + fileBytes.length, "end address");
		writeFile(uri, asList(new byte[] { lowByte(includedLoadAdress), highByte(includedLoadAdress) }, fileBytes));
	}

	public static void writeFile(final File filePath, final byte[] fileBytes, final int includedLoadAdress) {
		check16BitRange(includedLoadAdress, "start address");
		check16BitRange(includedLoadAdress + fileBytes.length, "end address");
		writeFile(filePath, asList(new byte[] { lowByte(includedLoadAdress), highByte(includedLoadAdress) }, fileBytes));
	}

	public static void writeFile(final Path path, final byte[] fileBytes, final int includedLoadAdress) {
		check16BitRange(includedLoadAdress, "start address");
		check16BitRange(includedLoadAdress + fileBytes.length, "end address");
		writeFile(path, asList(new byte[] { lowByte(includedLoadAdress), highByte(includedLoadAdress) }, fileBytes));
	}

	private static void check16BitRange(final int value16Bit, final String extraMessage) {
		if (value16Bit < 0 || 0xFFFF < value16Bit) {
			throw new IllegalArgumentException((extraMessage == null ? "" : extraMessage) + " value must be between 0 and $FFFF (inclusive)");
		}
	}

	@Deprecated
	public static void writeFile(final String filePath, final List<byte[]> fileBytes) {
		writeFile(new File(filePath), fileBytes);
	}

	public static void writeFile(final URI uri, final List<byte[]> fileBytes) {
		writeFile(new File(uri), fileBytes);
	}

	public static void writeFile(final File file, final List<byte[]> fileBytes) {
		writeFile(file.toPath(), fileBytes);
	}

	public static void writeFile(final Path path, final List<byte[]> fileBytes) {
		try {
			final var parent = path.getParent();
			if (parent != null) {
				if (parent.toFile().mkdirs()) {
					System.out.println("created directory: " + parent);
				}
			}
			final byte[] data = toByteArray(fileBytes.stream());
			Files.write(path, data, StandardOpenOption.CREATE);
			System.out.println("wrote file " + path + " (" + data.length + " / $" + Integer.toHexString(data.length) + " bytes)");
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	public static byte[] readFileInto(final URI uri, final byte[] targetBinary, final StartAddress startAddress) {
		try {
			return readFileInto(uri.toURL(), targetBinary, startAddress);
		} catch (final MalformedURLException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public static byte[] readFileInto(final URL url, final byte[] targetBinary, final StartAddress startAddress) {
		return readFileInto(url, 0, targetBinary, 0, startAddress);
	}

	public static byte[] readFileInto(final URL url, final int sourceOffset, final byte[] targetBinary, final StartAddress startAddress) {
		return readFileInto(url, sourceOffset, targetBinary, 0, startAddress);
	}

	public static byte[] readFileInto(final URL url, final byte[] targetBinary, final int targetOffset, final StartAddress startAddress) {
		return readFileInto(url, 0, targetBinary, targetOffset, startAddress);
	}

	public static byte[] readFileInto(final URL url, final int sourceOffset, final byte[] targetBinary, final int targetOffset,
			final StartAddress startAddress) {
		try (final InputStream input = url.openStream()) {
			final long skipped = input.skip(startAddress.skipBytes + sourceOffset);
			if (skipped < startAddress.skipBytes + sourceOffset) {
				throw new IllegalStateException("Could not skip " + (startAddress.skipBytes + sourceOffset) + " bytes from " + url);
			}
			int len = 0;
			do {
				len = input.read(targetBinary, targetOffset + len, targetBinary.length - targetOffset - len);
			} while (len > 0);
			return targetBinary;
		} catch (final IOException e) {
			throw new IllegalStateException(e);
		}
	}

	public static byte[] readFile(final URL url, final StartAddress startAddress) {
		try {
			return readFile(url.toURI(), startAddress);
		} catch (final URISyntaxException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public static byte[] readFile(final File file, final StartAddress startAddress) {
		return readFile(file.toPath(), startAddress);
	}

	public static byte[] readFile(final URI uri, final StartAddress startAddress) {
		return readFile(Paths.get(uri), startAddress);
	}

	public static byte[] readFile(final Path path, final StartAddress startAddress) {
		try {
			final byte[] allBytes = readAllBytes(path);
			return copyOfRange(allBytes, startAddress.skipBytes, allBytes.length);
		} catch (final IOException e) {
			throw new IllegalStateException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public static <T extends Serializable> T deepClone(final T src) {
		try (final ByteArrayOutputStream bo = new ByteArrayOutputStream(); final ObjectOutputStream oos = new ObjectOutputStream(bo);) {
			oos.writeObject(src);
			oos.close();
			final ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bo.toByteArray()));
			return (T) ois.readObject();
		} catch (final IOException | ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @return a function that returns <tt>null</tt> if the input is <tt>null</tt>, else the result of the given function.
	 */
	public static <X, Y> Function<X, Y> nullable(final Function<X, Y> nonNull) {
		return (final X x) -> x == null ? null : nonNull.apply(x);
	}

	public static <T> Supplier<T> supplierOf(final T value) {
		return () -> value;
	}

	public static <T> Supplier<T> lazy(final Supplier<T> supplier) {
		return new Supplier<>() {
			private T value;

			@Override
			public T get() {
				return value != null ? value : (value = supplier.get());
			}
		};
	}

	public static ByteSupplier lazy(final ByteSupplier supplier) {
		return new ByteSupplier() {
			private boolean initialized;

			private byte value;

			@Override
			public byte getAsByte() {
				if (!initialized) {
					initialized = true;
					value = supplier.getAsByte();
				}
				return value;
			}
		};
	}

	public static <T> Stream<T> asStream(final Iterator<T> sourceIterator, final boolean parallel) {
		final Iterable<T> iterable = () -> sourceIterator;
		return StreamSupport.stream(iterable.spliterator(), parallel);
	}

	/**
	 * Zip zwo streams.
	 * Adapted from JDK 8 lambda prototype b93, later removed from JDK.
	 * 
	 * @see <a href =
	 *      "http://download.java.net/lambda/b93/docs/api/java/util/stream/Streams.html">Proposed API</a>
	 * @see <a href=
	 *      "http://mail.openjdk.java.net/pipermail/lambda-libs-spec-observers/2013-June/002029.html">Removal discussion </a>
	 */
	public static <A, B, C> Stream<C> zip(final Stream<? extends A> a, final Stream<? extends B> b,
			final BiFunction<? super A, ? super B, ? extends C> zipper) {
		requireNonNull(zipper);
		final Spliterator<? extends A> aSpliterator = requireNonNull(a).spliterator();
		final Spliterator<? extends B> bSpliterator = requireNonNull(b).spliterator();

		// Zipping loses DISTINCT and SORTED characteristics
		final int characteristics = aSpliterator.characteristics() & bSpliterator.characteristics() & ~(Spliterator.DISTINCT | Spliterator.SORTED);
		final long zipSize = (characteristics & Spliterator.SIZED) != 0 ? Math.min(aSpliterator.getExactSizeIfKnown(), bSpliterator.getExactSizeIfKnown()) : -1;
		final Iterator<A> aIterator = Spliterators.iterator(aSpliterator);
		final Iterator<B> bIterator = Spliterators.iterator(bSpliterator);
		final Iterator<C> cIterator = new Iterator<>() {
			@Override
			public boolean hasNext() {
				return aIterator.hasNext() && bIterator.hasNext();
			}

			@Override
			public C next() {
				return zipper.apply(aIterator.next(), bIterator.next());
			}
		};
		final Spliterator<C> split = Spliterators.spliterator(cIterator, zipSize, characteristics);
		return a.isParallel() || b.isParallel() ? StreamSupport.stream(split, true) : StreamSupport.stream(split, false);
	}

	/** Given a stream, returns an optional element for the head and a stream for the tail. */
	public static <T> Map.Entry<Optional<T>, Stream<T>> headAndTail(final Stream<T> stream) {
		final AtomicReference<Optional<T>> head = new AtomicReference<>(Optional.empty());
		final var spliterator = stream.spliterator();
		spliterator.tryAdvance(x -> head.set(Optional.of(x)));
		return Map.entry(head.get(), StreamSupport.stream(spliterator, stream.isParallel()));
	}

}