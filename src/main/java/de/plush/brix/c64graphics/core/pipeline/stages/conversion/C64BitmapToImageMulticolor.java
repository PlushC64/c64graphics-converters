package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.function.*;

import de.plush.brix.c64graphics.core.model.C64Char;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.C64CharToImageMulticolor.Mode;
import de.plush.brix.c64graphics.core.util.functions.ByteSupplier;

public class C64BitmapToImageMulticolor extends AbstractC64BitmapToImage {

	private final Palette palette;

	private final Supplier<Color> backgroundColorProvider;

	private final Color mc1D022;

	private final Color mc2D023;

	private final Color cramD800;

	public C64BitmapToImageMulticolor() {
		this(C64ColorsColodore.PALETTE, () -> BLACK.color, BLUE.color, LIGHT_BLUE.color, CYAN.color);
	}

	public C64BitmapToImageMulticolor(final Palette palette) {
		this(palette, () -> palette.color(BLACK.colorCode()), palette.color(BLUE.colorCode()), palette.color(LIGHT_BLUE.colorCode()),
				palette.color(CYAN.colorCode()));
	}

	public C64BitmapToImageMulticolor(final Palette palette, final ByteSupplier backgroundColorCodeProvider, final byte mc1D022colorCode,
			final byte mc2D023colorCode, final byte cramD800colorCode) {
		this.palette = palette;
		backgroundColorProvider = () -> palette.color(backgroundColorCodeProvider.getAsByte());
		mc1D022 = palette.color(mc1D022colorCode);
		mc2D023 = palette.color(mc2D023colorCode);
		cramD800 = palette.color(cramD800colorCode);
	}

	public C64BitmapToImageMulticolor(final Palette palette, final Supplier<Color> backgroundColorProvider, final Color mc1D022, final Color mc2D023,
			final Color cramD800) {
		this.palette = palette;
		this.backgroundColorProvider = backgroundColorProvider;
		this.mc1D022 = mc1D022;
		this.mc2D023 = mc2D023;
		this.cramD800 = cramD800;
	}

	@Override
	protected Function<C64Char, ? extends BufferedImage> createCharToImage() {
		return new C64CharToImageMulticolor(palette, backgroundColorProvider, mc1D022, mc2D023, cramD800, Mode.Bitmap);
	}

}
