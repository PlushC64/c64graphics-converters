package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.model.C64CharsetScreen;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;

public class C64CharsetToImageHires extends AbstractCharsetToImage {

	private final Supplier<Color> backgroundSupplier;

	private final Palette palette;

	private final Color pixelColor;

	public C64CharsetToImageHires(final CharsetFormat grid) {
		this(grid, C64ColorsColodore.PALETTE, () -> BLACK.color, WHITE.color);
	}

	public C64CharsetToImageHires(final CharsetFormat grid, final Palette palette, final Supplier<Color> backgroundSupplier, final Color pixelColor) {
		super(grid);
		this.palette = palette;
		this.backgroundSupplier = backgroundSupplier;
		this.pixelColor = pixelColor;
	}

	@Override
	protected IPipelineStage<C64CharsetScreen, BufferedImage> createCharsetScreenToImage() {
		return new C64CharsetScreenToImageHires(palette, backgroundSupplier, pixelColor);
	}
}
