/*
 * Copyright (c) 1997, 1998, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package de.plush.brix.c64graphics.core._experimental.wavelet;

import java.awt.image.RGBImageFilter;

/**
 * Creates a Gray color from a given color channel (i.e. rgba 80 e0 a0 ff -> Filter(Red) -> 80 80 80 ff
 * 
 * @author Wanja Gayk
 */
class RGBChannelToGrayScaleFilter extends RGBImageFilter {

	private final ColorChannel channel;

	public RGBChannelToGrayScaleFilter(final ColorChannel channel) {
		// canFilterIndexColorModel indicates whether or not it is acceptable
		// to apply the color filtering of the filterRGB method to the color
		// table entries of an IndexColorModel object in lieu of pixel by pixel
		// filtering.
		canFilterIndexColorModel = true;
		this.channel = channel;
	}

	@Override
	public int filterRGB(final int x, final int y, final int rgb) {
		final int channelValue = channel.fromRgb(rgb);
		// return new Color(channelValue, channelValue, channelValue).getRGB();
		return ((0xff << 8 | channelValue) << 8 | channelValue) << 8 | channelValue;
	}
}
