package de.plush.brix.c64graphics.core.pipeline.stages.manipulation;

import java.awt.*;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

/**
 *
 */
public class Pixelize implements IPipelineStage<BufferedImage, BufferedImage> {

	private final Dimension fusedPixelSize;

	/**
	 * @param fusedPixelSize
	 *            the size of the target pixel
	 */
	public Pixelize(final Dimension fusedPixelSize) {
		this.fusedPixelSize = new Dimension(fusedPixelSize);
	}

	@Override
	public BufferedImage apply(final BufferedImage original) {
		final BufferedImage intermediate = new BufferedImage(original.getWidth() / fusedPixelSize.width, original.getHeight() / fusedPixelSize.height,
				original.getType());
		final Graphics2D gi = (Graphics2D) intermediate.getGraphics();
		final BufferedImage result = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());
		final Graphics2D g = (Graphics2D) result.getGraphics();
		try {
			gi.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
			gi.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
			gi.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
			gi.drawImage(original, 0, 0, intermediate.getWidth(), intermediate.getHeight(), null);

			g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
			g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED);
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
			g.drawImage(intermediate, 0, 0, original.getWidth(), original.getHeight(), null);
			return result;
		} finally {
			gi.dispose();
			g.dispose();
		}
	}
}