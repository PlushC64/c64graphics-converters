package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.Color;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.C64Char;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

public class ImageToC64CharMulticolor implements IPipelineStage<BufferedImage, C64Char> {

	private final Color background;

	private final Color mc1D022;

	private final Color mc2D023;

	private final Color cramD800;

	public ImageToC64CharMulticolor(final Color background, final Color mc1D022, final Color mc2D023, final Color cramD800) {
		this.background = background;
		this.mc1D022 = mc1D022;
		this.mc2D023 = mc2D023;
		this.cramD800 = cramD800;
	}

	@Override
	public C64Char apply(final BufferedImage blockImage) {
		final byte[] bytes = new byte[C64Char.HEIGHT_PIXELS];
		for (int line = 0; line < Math.min(C64Char.HEIGHT_PIXELS, blockImage.getHeight()); ++line) {
			bytes[line] = asByte(blockImage.getSubimage(0, line, blockImage.getWidth(), 1), background, mc1D022, mc2D023, cramD800);
		}
		return new C64Char(bytes);
	}

	@SuppressFBWarnings(value = { "SF_SWITCH_FALLTHROUGH", "SF_SWITCH_NO_DEFAULT" }, //
			justification = "The fall-through is intentional to set every necessary bit, also there is a default")
	// static to encourage extensive inlining into this method.
	private static byte asByte(final BufferedImage imageNx1, final Color background, final Color mc1, final Color mc2, final Color cramD800) {
		byte x = 0;
		switch (imageNx1.getWidth()) { // force unrolled jump-table instead of loop
		case 8, 7:
			x |= asBit(imageNx1, 6, background, mc1, mc2, cramD800);
		case 6, 5:
			x |= asBit(imageNx1, 4, background, mc1, mc2, cramD800);
		case 4, 3:
			x |= asBit(imageNx1, 2, background, mc1, mc2, cramD800);
		case 2, 1:
			x |= asBit(imageNx1, 0, background, mc1, mc2, cramD800);
		default: // do nothing
		}
		return x;
	}

	private static byte asBit(final BufferedImage imageNx1, final int x, final Color background, final Color mc1, final Color mc2, final Color cramD800) {
		final int shift = C64Char.WIDTH_PIXELS - 2 - x;
		final var pixel = new Color(imageNx1.getRGB(x, 0) & 0xFFFFFF);
		return (byte) (//@formatter:off
			  pixel.equals(background) ? 0 
			: pixel.equals(mc1) ? 0b01 << shift //d022
			: pixel.equals(mc2) ? 0b10 << shift //d023
			: pixel.equals(cramD800) ? 0b11<<shift //d023
			: error(pixel, background, mc1, mc2, background));
	}

	private static byte error(final Color pixel, final Color background, final Color mc1, final Color mc2, final Color cramD800) {
		throw new IllegalArgumentException(pixel
				+ " is not among the given colors for background (" + background + "), mc1 (" + mc1+ "), mc2  (" + mc2+ ") and colorRam (" + cramD800+ "). Did you forget to solve color clashes?");
	}
}
