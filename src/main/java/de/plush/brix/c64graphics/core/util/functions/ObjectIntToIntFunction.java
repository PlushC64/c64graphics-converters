package de.plush.brix.c64graphics.core.util.functions;

@FunctionalInterface
public interface ObjectIntToIntFunction<T> {
	int valueOf(T object, int value);
}
