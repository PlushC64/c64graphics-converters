package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.C64CharToImageMulticolor.Mode;
import de.plush.brix.c64graphics.core.util.functions.ByteSupplier;

public class C64CharsetScreenToImageMulticolor extends AbstractCharsetScreenToImage {

	private final Supplier<Color> backgroundColorProvider;

	private final Color mc1_D022;

	private final Color mc2_D023;

	private final Palette palette;

	public C64CharsetScreenToImageMulticolor() {
		this(C64ColorsColodore.PALETTE, () -> C64ColorsColodore.BLACK.color, C64ColorsColodore.BLUE.color, C64ColorsColodore.LIGHT_BLUE.color,
				C64ColorsColodore.DARK_GRAY.color); // dark gray becomes CYAN in Multicolor mode
	}

	/**
	 * <style> ulx > ul { margin-top: 0px; margin-bottom: 0px; } </style>
	 *
	 * @param backgroundColorProvider
	 *            color for 00 bit pairs
	 * @param mc1_D022
	 *            color for 01 bit pairs, all C64 palette colors feasible
	 * @param mc2_D023
	 *            color for 11 bit pairs, all C64 palette colors feasible
	 * @param colorRamColor
	 *            color for 11 bit pairs<br>
	 *            <ulx>
	 *            <li>In {@link Mode#Bitmap Char mode} only the colors for color codes $00 to $07 (included) are displayed and bit 3 of the color code
	 *            toggles between multicolor and hires (i.e: $07/yellow stays yellow in a hires resolution character, $0F/light gray becomes yellow in a
	 *            multicolor resolution character).<br>
	 *            </ulx>
	 */
	public C64CharsetScreenToImageMulticolor(final Palette palette, final Supplier<Color> backgroundColorProvider, final Color mc1_D022, final Color mc2_D023,
			final Color colorRamColor) {
		this(palette, backgroundColorProvider, mc1_D022, mc2_D023, new C64ColorRam(palette.colorCodeOrException(colorRamColor)));
	}

	/**
	 * <style> ulx > ul { margin-top: 0px; margin-bottom: 0px; } </style>
	 *
	 * @param backgroundColorCodeProvider
	 *            color code for 00 bit pairs, color code must be present in palette
	 * @param mc1_D022colorCode
	 *            color code for 01 bit pairs, all C64 palette colors feasible, color code must be present in palette
	 * @param mc2_D023colorCode
	 *            color code for 11 bit pairs, all C64 palette colors feasible, color code must be present in palette
	 * @param colorRamColorCode
	 *            color code for 11 bit pairs<br>
	 *            <ulx>
	 *            <li>In {@link Mode#Bitmap Char mode} only the colors for color codes $00 to $07 (included) are displayed and bit 3 of the color code
	 *            toggles between multicolor and hires (i.e: $07/yellow stays yellow in a hires resolution character, $0F/light gray becomes yellow in a
	 *            multicolor resolution character).<br>
	 *            </ulx>
	 */
	public C64CharsetScreenToImageMulticolor(final Palette palette, final ByteSupplier backgroundColorCodeProvider, final byte mc1_D022colorCode,
			final byte mc2_D023colorCode, final byte colorRamColorCode) {
		this(palette, () -> palette.color(backgroundColorCodeProvider.getAsByte()), palette.color(mc1_D022colorCode), palette.color(mc2_D023colorCode),
				new C64ColorRam(colorRamColorCode));
	}

	/**
	 * <style> ulx > ul { margin-top: 0px; margin-bottom: 0px; } </style>
	 *
	 * @param backgroundColorProvider
	 *            color for 00 bit pairs
	 * @param mc1_D022
	 *            color for 01 bit pairs, all C64 palette colors feasible
	 * @param mc2_D023
	 *            color for 11 bit pairs, all C64 palette colors feasible
	 * @param colorRAM
	 *            color matrix for 11 bit pairs<br>
	 *            <ulx>
	 *            <li>In {@link Mode#Bitmap Char mode} only be the colors for color codes $00 to $07 (included) are displayed and bit 3 of the color code
	 *            toggles between multicolor and hires (i.e: $07/yellow stays yellow in a hires resolution character, $0F/light gray becomes yellow in a
	 *            multicolor resolution character).<br>
	 *            </ulx>
	 */
	public C64CharsetScreenToImageMulticolor(final Palette palette, final Supplier<Color> backgroundColorProvider, final Color mc1_D022, final Color mc2_D023,
			final C64ColorRam colorRAM) {
		super(colorRAM);
		this.palette = palette;
		this.backgroundColorProvider = backgroundColorProvider;
		this.mc1_D022 = mc1_D022;
		this.mc2_D023 = mc2_D023;
	}

	@Override
	protected IPipelineStage<C64CharsetScreenColorRam, BufferedImage> createC64CharsetScreenColorRamToImage() {
		return new C64CharmodePictureToImageMulticolor(palette)//
				.withForcedBackgroundColor(backgroundColorProvider.get())//
				.withForcedMulticolor1D022(mc1_D022)//
				.withForcedMulticolor2D023(mc2_D023);
	}

}
