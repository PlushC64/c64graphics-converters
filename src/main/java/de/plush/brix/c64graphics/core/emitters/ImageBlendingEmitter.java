package de.plush.brix.c64graphics.core.emitters;

import static de.plush.brix.c64graphics.core.util.UtilsImage.loadImage;

import java.awt.image.BufferedImage;
import java.io.File;

import de.plush.brix.c64graphics.core.util.BlendFunction;

public class ImageBlendingEmitter extends AbstractGeneratedFramesEmitter {

	private final BufferedImage startImage;

	private final BufferedImage endImage;

	private final BlendFunction<BufferedImage> blendFunction;

	private final double[] magnitudes;

	public ImageBlendingEmitter(final File startImageFile, final File endImageFile, final BlendFunction<BufferedImage> blendFunction,
			final double... magnitudes) {
		this(loadImage(startImageFile), loadImage(endImageFile), blendFunction, magnitudes);
	}

	public ImageBlendingEmitter(final BufferedImage startImage, final BufferedImage endImage, final BlendFunction<BufferedImage> blendFunction,
			final double... magnitudes) {
		super(magnitudes.length);
		this.startImage = startImage;
		this.endImage = endImage;
		this.blendFunction = blendFunction;
		this.magnitudes = magnitudes;
	}

	@Override
	protected BufferedImage createImage(final int frame) {
		return blendFunction.blend(startImage, magnitudes[frame], endImage);
	}

}
