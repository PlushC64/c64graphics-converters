/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.misc;

import java.util.concurrent.TimeUnit;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class Wait<T> implements IPipelineStage<T, T> {

	private final long time;
	private final TimeUnit timeUnit;

	public Wait(final long time, final TimeUnit timeUnit) {
		this.time = time;
		this.timeUnit = timeUnit;
	}

	@Override
	public T apply(final T original) {
		try {
			timeUnit.sleep(time);
		} catch (final InterruptedException e) {
			throw new IllegalStateException(e);
		}
		return original;
	}
}