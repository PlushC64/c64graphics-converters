package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;

import java.awt.Color;
import java.util.Optional;
import java.util.function.Supplier;

import de.plush.brix.c64graphics.core.model.C64CharsetScreenColorRam;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;
import de.plush.brix.c64graphics.core.pipeline.stages.conversion.C64CharToImageMulticolor.Mode;

public class C64CharmodePictureToImageMulticolor extends AbstractCharmodePictureToImage {

	private Optional<Supplier<Color>> forcedBg = Optional.empty();

	private Optional<Color> forcedMc1 = Optional.empty();

	private Optional<Color> forcedMc2 = Optional.empty();

	private final Palette palette;

	public C64CharmodePictureToImageMulticolor() {
		this(C64ColorsColodore.PALETTE);
	}

	public C64CharmodePictureToImageMulticolor(final Palette palette) {
		this.palette = palette;
	}

	/**
	 * @param background
	 *            the color of 0 bits, overrides the value in the {@link C64CharsetScreenColorRam}
	 * @return this
	 */
	public C64CharmodePictureToImageMulticolor withForcedBackgroundColor(final Color background) {
		forcedBg = Optional.ofNullable(background).map(c -> () -> c);
		return this;
	}

	/**
	 * @param mc1_D022
	 *            color for 01 bit pairs, all C64 palette colors feasible, overrides the value in the {@link C64CharsetScreenColorRam#mc1_D022ColorCode()}
	 * @return this
	 */
	public C64CharmodePictureToImageMulticolor withForcedMulticolor1D022(final Color mc1_D022) {
		forcedMc1 = Optional.ofNullable(mc1_D022);
		return this;
	}

	/**
	 * @param mc2_D023
	 *            color for 11 bit pairs, all C64 palette colors feasible, overrides the value in the {@link C64CharsetScreenColorRam#mc2_D023ColorCode()}
	 * @return this
	 */
	public C64CharmodePictureToImageMulticolor withForcedMulticolor2D023(final Color mc2_D023) {
		forcedMc2 = Optional.ofNullable(mc2_D023);
		return this;
	}

	@Override
	protected C64CharToImageMulticolor createC64CharToImage(final byte backgroundColorCode, final byte mc1_D022ColorCode, final byte mc2_D023ColorCode,
			final byte d800ColorCode) {
		return new C64CharToImageMulticolor(palette, //
				forcedBg.orElse(() -> palette.color(toUnsignedByte(backgroundColorCode & 0x0F))), //
				forcedMc1.orElse(palette.color(toUnsignedByte(mc1_D022ColorCode & 0x0F))), //
				forcedMc2.orElse(palette.color(toUnsignedByte(mc2_D023ColorCode & 0x0F))), //
				palette.color(toUnsignedByte(d800ColorCode & 0x0F)), //
				Mode.Char);
	}

}
