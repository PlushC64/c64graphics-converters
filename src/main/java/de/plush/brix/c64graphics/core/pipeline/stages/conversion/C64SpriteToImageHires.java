package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.Color;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.Palette;

/**
 * Note that the returned Image is of type {link BufferedImage#TYPE_INT_ARGB} and has a transparent background color
 * 
 * @author Wanja Gayk
 */
public class C64SpriteToImageHires implements IPipelineStage<C64Sprite, PreProcessedSpriteImage> {

	private final Color transparent = new Color(0, 0, 0, 0); // with alpha-channel
	private final Palette palette;

	private final Color spriteColor;

	public C64SpriteToImageHires(final Palette palette, final Color spriteColor) {
		this.palette = palette;
		this.spriteColor = spriteColor;
	}

	@Override
	public PreProcessedSpriteImage apply(final C64Sprite sprite) {
		// do not get color in constructor, as provider may need to convert an image first (which is after pipeline construction)
		final byte spriteColorCode = palette.colorCodeOrException(spriteColor);
		final var result = new PreProcessedSpriteImage(C64Sprite.WIDTH_PIXELS, C64Sprite.HEIGHT_PIXELS, BufferedImage.TYPE_INT_ARGB, palette, spriteColorCode,
				(byte) 0, (byte) 0);
		int n = -1;
		for (byte b : sprite) {
			++n;
			if (n < 0x3f) { // sprites have 0x40 bytes, the last one is not used.
				final int columnStart = n % 3 * C64Char.WIDTH_PIXELS;
				final int y = n / 3;
				for (int x = 0; x < C64Char.WIDTH_PIXELS; ++x) {
					final boolean pixelSet = (b & 0x80) != 0;
					final Color color = pixelSet ? spriteColor : transparent;
					result.setRGB(columnStart + x, y, color.getRGB());
					b <<= 1;
				}
			}
		}
		return result;
	}

}
