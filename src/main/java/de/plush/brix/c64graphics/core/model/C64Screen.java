package de.plush.brix.c64graphics.core.model;

import static de.plush.brix.c64graphics.core.util.Utils.*;

import java.awt.*;
import java.net.*;
import java.util.Arrays;

import de.plush.brix.c64graphics.core.util.*;
import de.plush.brix.c64graphics.core.util.collections.ByteSet;
import de.plush.brix.c64graphics.core.util.functions.ByteToByteFunction;

public class C64Screen implements C64BinaryProvider, IRollBlockwise {
	public static final int WIDTH_BLOCKS = 40;

	public static final int HEIGHT_BLOCKS = 25;

	private final byte[][] screenCodes;

	public C64Screen(final C64Screen other) {
		this(other.screenCodes);
	}

	public C64Screen(final byte[][] screenCodes) {
		this.screenCodes = deepCopy(screenCodes);
	}

	public C64Screen(final byte defaultBlock) {
		screenCodes = new byte[HEIGHT_BLOCKS][WIDTH_BLOCKS];
		for (final byte[] line : screenCodes) {
			Arrays.fill(line, defaultBlock);
		}
	}

	public ByteSet uniqueScreenCodes() {
		return Arrays.stream(screenCodes)//
				.map(byteArray -> ByteSet.of(byteArray))//
				.reduce(ByteSet.empty(), (bs1, bs2) -> bs1.withAll(bs2));//
	}

	public static Dimension sizeBlocks() {
		return new Dimension(WIDTH_BLOCKS, HEIGHT_BLOCKS);
	}

	public void setScreenCode(final int xBlock, final int yBlock, final byte screenCode) {
		screenCodes[yBlock][xBlock] = screenCode;
	}

	public byte screenCodeAt(final int xBlock, final int yBlock) {
		return screenCodes[yBlock][xBlock];
	}

	public void modifyScreenCodeAt(final Point pBlock, final ByteToByteFunction modifier) {
		modifyScreenCodeAt(pBlock.x, pBlock.y, modifier);
	}

	public void modifyScreenCodeAt(final int xBlock, final int yBlock, final ByteToByteFunction modifier) {
		final byte code = screenCodeAt(yBlock, xBlock);
		setScreenCode(yBlock, xBlock, modifier.valueOf(code));
	}

	/**
	 * @param modifier
	 *            a function that takes the current position and screen code and returns the screen code that should be set at that position.<br>
	 *            Note that there are no guarantees about the iteration order.
	 */
	public void modifyEveryScreenCode(final ObjByteToByteFunction<Point> modifier) {
		for (int xBlock = 0; xBlock < C64Screen.HEIGHT_BLOCKS; ++xBlock) {
			for (int yBlock = 0; yBlock < C64Screen.WIDTH_BLOCKS; ++yBlock) {
				final byte code = screenCodeAt(yBlock, xBlock);
				setScreenCode(yBlock, xBlock, modifier.apply(new Point(xBlock, yBlock), code));
			}
		}
	}

	public byte[][] screenCodeMatrix() {
		return Utils.deepCopy(screenCodes);
	}

	@Override
	public void rollLeftBlockwise(final int nBlocks) {
		for (final byte[] line : screenCodes) {
			Utils.rollLeft(line, nBlocks);
		}
	}

	@Override
	public void rollRightBlockwise(final int nBlocks) {
		for (final byte[] line : screenCodes) {
			Utils.rollRight(line, nBlocks);
		}
	}

	@Override
	public void rollUpBlockwise(final int nBlocks) {
		Utils.rollLeft(screenCodes, nBlocks);
	}

	@Override
	public void rollDownBlockwise(final int nBlocks) {
		Utils.rollRight(screenCodes, nBlocks);
	}

	public static C64Screen loadScreen(final URL url, final StartAddress startAddress) {
		return fromBinary(Utils.readFile(url, startAddress));
	}

	public static C64Screen loadScreen(final URI uri, final StartAddress startAddress) {
		return fromBinary(Utils.readFile(uri, startAddress));
	}

	public static C64Screen fromBinary(final byte[] bytes) {
		final byte[][] screenCodes = new byte[HEIGHT_BLOCKS][WIDTH_BLOCKS];
		for (int t = 0; t < bytes.length; ++t) {
			final int x = t % WIDTH_BLOCKS;
			final int y = t / WIDTH_BLOCKS;
			screenCodes[y][x] = bytes[t];
		}
		return new C64Screen(screenCodes);
	}

	@Override
	public byte[] toC64Binary() {
		final byte[] binary = new byte[WIDTH_BLOCKS * HEIGHT_BLOCKS];
		for (int index = 0; index < binary.length; ++index) {
			binary[index] = screenCodeAt(index % WIDTH_BLOCKS, index / WIDTH_BLOCKS);
		}
		return binary;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(screenCodes);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final C64Screen other = (C64Screen) obj;
		if (!Arrays.deepEquals(screenCodes, other.screenCodes)) { return false; }
		return true;
	}

	@Override
	public String toString() {
		final String intro = "C64Screen \n";
		final StringBuilder sb = new StringBuilder(1000 * 3 + intro.length());
		sb.append(intro);
		for (int y = 0; y < C64Screen.HEIGHT_BLOCKS; ++y) {
			for (int x = 0; x < C64Screen.WIDTH_BLOCKS; ++x) {
				final String hexString = toHexString(screenCodeAt(x, y));
				if (hexString.length() == 1) {
					sb.append(0);
				}
				sb.append(hexString);
				if (x + 1 < C64Screen.WIDTH_BLOCKS) {
					sb.append(',');
				}
			}
			sb.append('\n');
		}
		return sb.toString();
	}
}
