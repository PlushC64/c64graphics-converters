package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.util.Utils.toUnsignedByte;

import java.awt.*;
import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.*;
import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.Palette;

public class C64BitmapmodePictureHiresWithHiresSpriteOverlayToImage implements IPipelineStage<C64BitmapmodePictureHiresWithHiresSpriteOverlay, BufferedImage> {

	private final Palette palette;
	private final C64BitmapmodePictureHiresToImage hiresPicToImage;

	public C64BitmapmodePictureHiresWithHiresSpriteOverlayToImage(final Palette palette) {
		this.palette = palette;
		hiresPicToImage = new C64BitmapmodePictureHiresToImage(palette);
	}

	@Override
	public BufferedImage apply(final C64BitmapmodePictureHiresWithHiresSpriteOverlay picWithOverlay) {
		final C64SpriteGrid spriteOverlay = picWithOverlay.spriteOverlay();
		final Point overlayLocationAtBlock = picWithOverlay.spriteOverlayLocationAtBlock();
		final Color spriteColor = palette.color(toUnsignedByte(picWithOverlay.spriteOverlayColorCode() & 0x0F));

		final var spriteGridToImageHires = new C64SpriteGridToImageHires(palette, spriteColor);
		final BufferedImage result = hiresPicToImage.apply(picWithOverlay.c64BitmapmodePictureHires());
		final Graphics2D g = result.createGraphics();
		try {
			final Image overlayPic = spriteGridToImageHires.apply(spriteOverlay);
			g.drawImage(overlayPic, overlayLocationAtBlock.x * C64Char.WIDTH_PIXELS, overlayLocationAtBlock.y * C64Char.HEIGHT_PIXELS, null);
		} finally {
			g.dispose();
		}
		return result;
	}
}
