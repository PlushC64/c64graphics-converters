/**
 *
 */
package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.io.StringReader;

import javax.xml.parsers.*;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import de.plush.brix.c64graphics.core.pipeline.stages.IPipelineStage;

public class StringToXmlDocument implements IPipelineStage<String, Document> {

	private final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();

	@Override
	public Document apply(final String xmlString) {
		try {
			final DocumentBuilder builder = docBuilderFactory.newDocumentBuilder();
			final StringReader strReader = new StringReader(xmlString);
			final InputSource is = new InputSource(strReader);
			return builder.parse(is);
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

}