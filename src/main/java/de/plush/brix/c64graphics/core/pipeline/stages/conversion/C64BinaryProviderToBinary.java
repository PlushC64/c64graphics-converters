package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.util.function.Function;

import de.plush.brix.c64graphics.core.model.C64BinaryProvider;

public class C64BinaryProviderToBinary implements Function<C64BinaryProvider, byte[]> {

	@Override
	public byte[] apply(C64BinaryProvider provider) {
		return provider.toC64Binary();
	}

}
