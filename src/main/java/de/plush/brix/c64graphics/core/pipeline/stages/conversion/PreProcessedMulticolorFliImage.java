package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import java.awt.image.BufferedImage;

import de.plush.brix.c64graphics.core.model.C64ColorRam;

public class PreProcessedMulticolorFliImage extends BufferedImage {

	private final byte backgroundColorCode;

	private final C64ColorRam cram;

	public PreProcessedMulticolorFliImage(final int width, final int height, final int imageType, final byte backgroundColorCode) {
		super(width, height, imageType);
		this.backgroundColorCode = backgroundColorCode;
		cram = new C64ColorRam((byte) 0);
	}

	public byte backgroundColorCode() {
		return backgroundColorCode;
	}

	public void setColorCode(final int x, final int y, final byte colorCode) {
		cram.setColorCode(x, y, colorCode);
	}

	public byte colorCodeAt(final int x, final int y) {
		return cram.colorCodeAt(x, y);
	}

}