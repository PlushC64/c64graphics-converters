package de.plush.brix.c64graphics.core.util;

import static java.util.Comparator.*;

import java.util.Comparator;

public class ObjectIntPair<T> implements Comparable<ObjectIntPair<T>> {

	private static final Comparator<ObjectIntPair<?>> ORDER_BY_INT = nullsFirst(comparingLong(ObjectIntPair::getTwo));

	private static final Comparator<Object> ORDER_BY_IDENTiTY = comparingInt(System::identityHashCode);

	private static final Comparator<ObjectIntPair<?>> NATURAL_ORDER = nullsFirst(new Comparator<>() {
		@SuppressWarnings({ "rawtypes", "unchecked" })
		@Override
		public int compare(final ObjectIntPair<?> o1, final ObjectIntPair<?> o2) {
			final int c = ((Comparator) (o1.one instanceof Comparable && o2.one instanceof Comparable //
					? naturalOrder() //
					: ORDER_BY_IDENTiTY)//
			).compare(o1.one, o2.one);
			return c != 0 ? c : ORDER_BY_INT.compare(o1, o2);
		}
	});

	private final T one;

	private final int two;

	public ObjectIntPair(final T newOne, final int newTwo) {
		this.one = newOne;
		this.two = newTwo;
	}

	public static <T> ObjectIntPair<T> pair(final T one, final int two) {
		return new ObjectIntPair<>(one, two);
	}

	/** same as "pair", may read better depending on the context */
	public static <T> ObjectIntPair<T> of(final T one, final int two) {
		return new ObjectIntPair<>(one, two);
	}

	public T getOne() {
		return this.one;
	}

	public int getTwo() {
		return this.two;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (one == null ? 0 : one.hashCode());
		result = prime * result + two;
		return result;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final ObjectIntPair other = (ObjectIntPair) obj;
		if (one == null) {
			if (other.one != null) { return false; }
		} else if (!one.equals(other.one)) { return false; }
		if (two != other.two) { return false; }
		return true;
	}

	@Override
	public String toString() {
		return this.one + ":" + this.two;
	}

	@Override
	public int compareTo(final ObjectIntPair<T> o) {
		return NATURAL_ORDER.compare(this, o);
	}
}
