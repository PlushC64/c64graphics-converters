package de.plush.brix.c64graphics.core.model;

public class PackedCharsetScreenBinary {

	public final byte[] charset;
	public final byte[] screens;

	public PackedCharsetScreenBinary(final byte[] charsetBinary, final byte[] packedScreensBinary) {
		charset = charsetBinary;
		screens = packedScreensBinary;
	}
}
