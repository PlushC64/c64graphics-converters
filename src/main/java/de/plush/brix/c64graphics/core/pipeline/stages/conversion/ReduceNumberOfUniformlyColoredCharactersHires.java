package de.plush.brix.c64graphics.core.pipeline.stages.conversion;

import static de.plush.brix.c64graphics.core.pipeline.stages.colors.C64ColorsColodore.*;

import java.awt.Color;
import java.util.function.*;

import de.plush.brix.c64graphics.core.model.C64Char;
import de.plush.brix.c64graphics.core.pipeline.stages.colors.*;

public class ReduceNumberOfUniformlyColoredCharactersHires extends AbstractReduceNumberOfUniformlyColoredCharacters {

	private final Palette palette;

	private final Supplier<Color> background;

	private final Color colorRamColor;

	public ReduceNumberOfUniformlyColoredCharactersHires(final int maxCharacters, final ColorDistanceMeasure distanceMeasure) {
		this(maxCharacters, PALETTE, distanceMeasure, BLACK, WHITE.color);
	}

	/**
	 * Allows for more precision, by considering the real colors
	 */
	public ReduceNumberOfUniformlyColoredCharactersHires(final int maxCharacters, //
			final Palette palette, final ColorDistanceMeasure distanceMeasure, //
			final Supplier<Color> background, final Color colorRamColor) {
		super(maxCharacters, distanceMeasure);
		this.palette = palette;
		this.background = background;
		this.colorRamColor = colorRamColor;
	}

	@Override
	public Function<C64Char, PreProcessedCharacterImage> createC64CharToImage() {
		return new C64CharToImageHires(palette, background, colorRamColor);
	}

}
