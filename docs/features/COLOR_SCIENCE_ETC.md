# C64Graphics library for Java

## Color Science

Note: It is advisable to not use this library as part of your C64 demo or game build chain, generating data on the fly, as with newer versions of the library bugs may be fixed and the outcome of color conversions might change.

Color science related things mainly reside in the **de.plush.brix.c64graphics.core.pipeline.stages.colors** package.

### Color palettes

There are currently 3 supported color palettes. Of course you can implement your own, see [C64ColorsColodore](../../src/main/java/de/plush/brix/c64graphics/core/pipeline/stages/colors/C64ColorsColodore.java) for an example.  

![Palettes](palettes.png)  

A [full C64]((../../src/main/java/de/plush/brix/c64graphics/core/pipeline/stages/colors/PaletteC64Full.java) color palette color palette also provides the notion of [color clusters]((../../src/main/java/de/plush/brix/c64graphics/core/pipeline/stages/colors/ColorClusterIndexes.java), i.e. colors that have a similar brightness and can be mixed.  
![Color Clusters](c64-luminance-comparison.png)

### Color distance measuring

This library contains the implementation of several color distance measures. These can be found in the [ColorDistance](../../src/main/java/de/plush/brix/c64graphics/core/pipeline/stages/colors/ColorDistance.java) enumeration.  
You are free to provide own implementations by implementing the [ColorDistanceMeasure](../../src/main/java/de/plush/brix/c64graphics/core/pipeline/stages/colors/ColorDistanceMeasure.java) interface that is used by all classes that deal with color distances.  

This overview shows the same dithering algorithm used with different color distance measures.  
You can generate this overview running the class
[ExampleColorDistanceMeasures](../../src/main/java/de/plush/brix/c64graphics/examples/ExampleColorDistanceMeasures.java) in the [examples](../../src/main/java/de/plush/brix/c64graphics/examples) directory.

![color distance overview](color_distance_overview.png)

### Color mixing
As with color distance measuring, mixing colors can be accomplished in different color spaces. 

Color mixers for different color spaces can be found in the [ColorMix](../../src/main/java/de/plush/brix/c64graphics/core/pipeline/stages/colors/ColorMix.java) enumeration. A color is mixed by using the average of each channel (e.g. (x.red + y.red) /2).  
The result of mixing channels in different color spaces can have quite different results. 

As you can see in this overview, no mixer will yield a perfect result matching what the eye will percieve when squinting and looking at alternating colors (left and right side of the diagram), however HunterLab generally does fine with those C64Colodore palette colors that have a similar luminosity:

![color mixing](mixing_colors.png)

You can generate this overview running the class
[ExampleColorMixing](../../src/main/java/de/plush/brix/c64graphics/examples/ExampleColorMixing.java) in the [examples](../../src/main/java/de/plush/brix/c64graphics/examples) directory.

### Dithering

Dithering related things reside in the **de.plush.brix.c64graphics.core.pipeline.stages.dithering** package.

This library has some different Dithering algorithms preset. These can be found in the [Dithering](../../src/main/java/de/plush/brix/c64graphics/core/pipeline/stages/dithering/Dithering.java) enumeration.   
You are free to create own implementations by implementing the [DitheringSpec](../../src/main/java/de/plush/brix/c64graphics/core/pipeline/stages/dithering/DitheringSpec.java) interface that is used by all classes that deal with dithering.  
A DitheringSpec is an abstract matrix, since all dithering is matrix-based, it also references an algorithm that uses this matrix to dither a picture.  
The class [PaletteConversion](../../src/main/java/de/plush/brix/c64graphics/core/pipeline/stages/manipulation/PaletteConversion.java) will put that to use and, depending on the Dithering Spec's algorithm, apply a palette to the picture, as an easy to use frontend.  
Currently the DitheringSpecs use 3 different algorithms:
1. A sliding window algorithm that slides an error-distribution matrix/window over the image (Floyd Steinberg, Burkes, Stucki, etc).
1. An ordered dither that applies the matrix from the DitheringSpec as tiles.  
1. The LumaPairingDither that will mix colors-paits of similar luminosity from the original palette, apply an "enhanced" palette to the picture and then replace these mixed colors with the orginating palette, using a simple C64-Character sized tile-matrix. See also [https://kodiak64.com/blog/luma-driven-graphics-on-c64](https://kodiak64.com/blog/luma-driven-graphics-on-c64") for the basic idea. The color mixing is done in the HunterLab color space by default.

You can generate this overview running the class
[ExampleDitherings](../../src/main/java/de/plush/brix/c64graphics/examples/ExampleDitherings.java) in the [examples](../../src/main/java/de/plush/brix/c64graphics/examples) directory.

![dithering overview](dithering_overview.png)

### Quantization

Sometimes it may be desirable to reduce the number of colors to, say 32 or 16, but not use the C64 palette, so you can apply own color replacements (i.e. replacing a color with a pattern).  
For this reason this library features the [Octree](https://en.wikipedia.org/wiki/Octree), [Neu Quant](https://scientificgems.wordpress.com/stuff/neuquant-fast-high-quality-image-quantization/) and [Median Cut](https://muthu.co/reducing-the-number-of-colors-of-an-image-using-median-cut-algorithm/) algorithms to quantize images. These can be applied using the [Quantize](../../src/main/java/de/plush/brix/c64graphics/core/pipeline/stages/manipulation/Quantize.java) class. 

You can generate this overview running the class
[ExampleQuantizations](../../src/main/java/de/plush/brix/c64graphics/examples/ExampleQuantizations.java) in the [examples](../../src/main/java/de/plush/brix/c64graphics/examples) directory, that also shows how it's done.

![quantization overview](quantization_overview.png) 

### Further Reading
If I have seen further it is by standing on the shoulders of Giants:

* [Calculating the color palette of the VIC II by Philip 'Pepto' Timmermann](https://www.pepto.de/projects/colorvic/)
* [Accurately reproducing the Video Output of a Commodore C64 by 'Groepaz'](http://hitmen.c02.at/temp/palstuff/)
* [Neue RGB Palette für den C64: PALette (Tobias Albert, forum64.de)](https://www.forum64.de/index.php?thread/124965-neue-rgb-palette-für-den-c64-palette)
* [Luma & Chroma Waveform Measurements and Emulation by Christopher Jam](https://jamontoads.net/p/lumachroma.html)
* [Luma Driven Graphics on the C64 by Jon 'Kodiak64' Woods](https://kodiak64.com/blog/luma-driven-graphics-on-c64)
* [Old VIC-II Colors and Color Blending](https://ilesj.wordpress.com/2016/03/30/old-vic-ii-colors-and-color-blending/)
* [Bildbearbeitung mit GoDot (www.godot64.de)](https://www.godot64.de/">https://www.godot64.de/)
* [Color FAQ by Charles Poynton](http://poynton.ca/notes/colour_and_gamma/ColorFAQ.html)
* [Image Dithering: Eleven Algorithms and Source Code by Tanner Helland](http://www.tannerhelland.com/4660/dithering-eleven-algorithms-source-code/)
* [Color difference (Wikipedia)](https://en.wikipedia.org/wiki/Color_difference)
* [Which color differencing equation should be used?](https://www.internationalcircle.net/wp-content/uploads/2022/01/ICJ_06_2013_02_069-1.pdf)
* [Apache Commons-Imaging source code](https://github.com/apache/commons-imaging)
* [YCbCr (Wikipedia)](https://en.wikipedia.org/wiki/YCbCr), [Recommendation ITU-R BT.601-7 Studio encoding parameters of digital television](https://www.itu.int/dms_pubrec/itu-r/rec/bt/R-REC-BT.601-7-201103-I!!PDF-E.pdf)
* [YPrPr (Wikipedia)](https://en.wikipedia.org/wiki/YPbPr)
* [YUV (Wikipedia)](https://en.wikipedia.org/wiki/YUV)
* [DIN99 Farbraum (Wikipedia)](https://de.wikipedia.org/w/index.php?title=Diskussion:DIN99-Farbraum)

