# C64Graphics library for Java
- - - 
_Created by:_  Wanja 'Brix' Gayk

_Thanks to:_ 

* Kurt Spencer (OpenSimplex Noise in Java)
* András Jankovics, Miguel Lemos (Image Tracer for Java)
* Mark McKay (SVG Salamander)

## Building the project
This project is built using Maven. It requires a Java 17 SDK.  
Call `mvn install` to build and create the library as jar-file.  

### Platform
This library is compatible both in source code and runtime with Java 17 in both 32 and 64 bit editions.  

However, there is the [c64graphics-xuggler-extension](https://bitbucket.org/PlushC64/c64graphics-xuggler-extension) that is only compatible to 32-Bit-Java-17 runtimes. If you need to use that extension, you can get a 32 Bit Java 17 runtime at [adoptium.net](https://adoptium.net/releases.html).  
The c64graphics-xuggler-extension has been deprecated and the [c64graphics-humblevideo-extension](https://bitbucket.org/PlushC64/c64graphics-humblevideo-extension) should be used instead to convert videos. It is both compatible to 32 and 64 bit Java.

## Features

Many. It allows you to convert PC-graphics to various C-64 formats (HiRes- or Multicolor Charset+Screen, PetSCII, plain HiRes- and Multicolor Bitmaps, Koala, AmicaPaint, FLI, Hires-FLI, Sprites, etc) and back, create and read from character sets in 1x1,1x2, 2x1, 2x2, 3x3 and other formats, reduce graphics to character sets, mirror character sets, rotate each character by 90/180/270 degrees, allows you to build image manipulation pipelines, that resize, stretch, flood fill or mask out images before converting them and many other things, so you can batch-convert whole directories or generated animations.
See also this [overview of Color Science and Dithering](docs/features/COLOR_SCIENCE_ETC.md).

## Using the library

To use it in your own project, add it as a Maven dependency:

##### Latest release:
![version](docs/images/latest_release.svg)

### Key Concepts
The c64 graphics library is built around the concept of _image emitters_, _image consumers_ and _conversion pipelines_.

* Image Emitter    
    Reads a file, emits one or more images then calls "close" on all registered consumers.
    Image emitters can be found in the package **de.plush.brix.c64graphics.core.emitters**.


* Image Consumer    
    Consumes an emitted image and does something with it, eg. feeding it into a conversion pipeline.    


* Conversion Pipeline    
    A compound Function<A,B>, that consists of different pipeline stages (each of its own a Function<A,B>).  
    Pipeline stages for image manipulation can be found in the package **de.plush.brix.c64graphics.core.pipeline.stages**.  
    Pipeline stages for image conversion can be found the the package **de.plush.brix.c64graphics.core.pipeline.stages.conversion**	.
    
Image Emitters and Image Consumers are usually used when there is a batch-conversion of some sort going on, e.g. extracting frames from a GIF or getting all images from a directory.  
When converting single images it often leads to clearer code to setup and use a Conversion Pipeline directly.

### Building a conversion pipeline
The following pipeline takes a BufferedImage, gets the  most common color (this stage sits in the pipeline, and is referenced later), rescales it to C64-Size, applies the C64-Palette using Atkinson-dithering, converts the BufferedImage to a C64 image model, gets a function that will create a byte[] from the image, then writes these bytes to disc:
Note that the image type conversion requires a stage to resolve color clashes.  
Stages for resolving color clashes can be found in the package **de.plush.brix.c64graphics.core.pipeline.stages.conversion.colorclashes**.
- - -
```java
    Palette palette = C64ColorsColodore.PALETTE;
    ColorDistance colorDistanceMeasure = ColorDistance.EUCLIDEAN_WEIGHTED;

    BackgroundColorFinder backgroundProviderPC = new BackgroundColorFinder(Calculate.FIRST_FRAME_AND_REUSE);

    Function<BufferedImage, ?> pipeline = backgroundProviderPC // get most common color
    	.andThen(new Rescale(C64CharsetScreen.sizePixels(), backgroundProviderPC)) // using color finder to fill frame when resizing
    	.andThen(new PaletteConversion(palette, colorDistanceMeasure, Dithering.Atkinson)) // we now have c64 colors in hires resolution
    	.andThen(new ImageToC64BitmapmodePictureHires(palette, new HiEddiColorClashResolver(colorDistanceMeasure))) //
    	.andThen(pic -> pic.binaryProviderHiEddi())// some C64 models aren't binary providers themselves, but provide several - we need to pick one
    	.andThen(new WriteC64BinaryToFile<>(n -> 0x2000, n -> outDir + picName.replaceAll("\\.jpg", "") + ".hed")) //
    ;
```
- - -
Find more examples in the package **de.plush.brix.c64graphics.examples**.

### Running images through the pipeline
For single images, the easiest thing is to just load an image file and apply it to the pipeline:
- - -
```java
    BufferedImage image = ImageIO.read(file);
    pipeline.apply(image);
```
- - -
For GIF images or videos, _ImageEmitters_  and  _ImageConsumers_  are the weapon of choice:
- - -
```java
    try (final var consumer = new PipelineConsumer<>(pipeline);
         final var emitter = new GifEmitter(gifFile)) {
         emitter.add(consumer);
         emitter.run();
    }
```

If you want to combine several images, you can accumulate results in the post processing stage of a PipelineConsumer:

```java
    final Function<BufferedImage, C64CharsetScreen> pipeline = // .... 

    final Consumer<List<C64CharsetScreen>> postProcessing = charScreens -> {
     // do some things and write the result to disc
    };
    try (final var consumer = new PipelineConsumer<>(pipeline, postProcessing);
         final var emitter = new GifEmitter(gifFile)) {
         emitter.add(consumer);
         emitter.run();
    }
```
- - -
Alternatively you could create a stateful pipeline-stage, but that would be unaware of the end of the image stream, which makes a PipelineConsumer the better choice.

## Contributing

Note to stick to this libraries platform requirements. Don't use language features that are newer than this libraries target version.

### Development/Versioning:

Versioning of the pom-file is done automatically using the 
[jgitver-maven-plugin](https://github.com/jgitver/jgitver-maven-plugin).  
To create a new version, create an annotated tag on the latest commit.  
To create a version number-jump, without creating a release, set a lightweight-tag.

### Space/Time/Quality tradeoff
* When trading off quality vs. speed, quality usually takes preference. 
* When trading off quality vs. memory, quality usually takes preference. 
* When trading off speed vs memory, speed usually takes preference.

**Reasoning**: Memory is cheap, keeps getting cheaper and every generation of computers gets stuffed with more memory 
(my current machine has 64 Gigs of memory and I don't expect to buy anything with less than that in the future).
Memory is not made to idle around free and look nice, it is there to be used. 
It's okay to be greedy and quite frankly I don't care to play nice with any other application on the system.
When in doubt, have the user chose which algorithm to use: The fast and less accurate or the slow and good quality one.

### Code style

_Trigger warning:_ I'm voicing some strong opinions here, but if you step on my turf to contribute, please follow my style, as I would follow yours in your project.  
Though I'm not obliged to give any reasons, I think it's easier to explain why I made some choices that you might not like, so you can at least relate and understand.  
However, I'm not here to discuss these, I've got way too many years as a software developer behind me and I've grown biased, old and stubborn.

#### Boolean parameters
Boolean parameters are usually a shit choice. If you read `C64Charset.loadCharset(file("1x2 hires.prg"), true);` what do you think `true` does?  
`C64Charset.loadCharset(file("1x2 hires.prg"), StartAddress.InFile);` tells the story way better, which is why I prefer Enums to boolean parameters, even though it might seem wasteful.

#### Comments
Source code comments should ideally describe why the code is doing something, not what the code is doing.  
Sometimes, when code is hard to read, it's okay to write a comment that explains what's going on, but generally this is a warning sign that the code should be refactored to become more readable.

#### Final references
I make extensive use of the word "final" for references. The reason is not speed (the compiler is clever enough), but mind mapping: References that don't show a "final" keyword shall stand out as being changed and should get extra attention.

#### Functional purity
I urge every contributor to keep pipeline stages free of state and free of side effects, if possible. For some use cases this might not apply, but these should be the exception rather than the norm and their statefulness should be documented. 

#### Getters
You will see throughout the source code that I hate getters.  
**Reasoning**: `if (list.size() < 10){` reads better than `if (list.getSize() < 10){`.  
Beans are a thing of the past, any reason for the bean-convention has vanished,
we can now bind to attributes using lambdas and JavaFX uses properties that are not exposed by getters either 
(even though they might have a get/set API). The `getXY` syntax is stupid and pointless and I won't argue about it anymore.
No, I don't care the IDE's autocompletion makes you happy when you type "get", searching for a property: 
Code is more often read than written and I'm willing to take the pain writing something when it means I can read it better later.
Some code in this project is already damn hard to read and I don't want to make it worse by using a pointless prefix.

#### Imports
For some awkward reason, urban performance myths and IDE presets have lead to the wide spread bonkers idea that every resource should have its dedicated import.  
However, compilers are smart enough to solve a mix of dedicated imports and '\*'-imports to not include "the world".
All imports shall do is to help programmers with name spaces, so classes don't need to be fully qualified. Reading the source in its raw form, the primary information for the programmer is what packages are referenced. This is architectural information, where more detail is just ceremony.  
To keep this architectural information concise, I have set the Eclipse preferences to generate a '\*'-import whenever 2 or more resources share the same package. Maybe I'll raise it to 3 one day, which in hindsight is the better number, but I don't consider more to be beneficial, rather the contrary.

#### Inheritance
Generally one should favor composition over inheritance. There are several upsides to this, like getting some pitfalls from Liskov's substitution principle out of the way, opening possibilities to evolve the code by not constraining classes to be a specialization of the parent class. However, it will lead to more clutter caused by delegation.  
I do occasionally break that rule for abstract base classes and I have done so for some core model classes.  
Still, for example color clash resolvers are injected into converter pipeline stages, rather than extending the converter for that purpose, as it enforces a code style that allows a user to create their own and use them with a converter stage. This should not stop you from providing a default in a constructor.  
Classes that use inheritance are not generally safe from being refactored into classes that favor composition.

#### Interfaces
In production code, if an interface has only one implementation, it is redundant and should be deleted. However, this is meant to be a library that a user can tap into and extend. So I do introduce interfaces where I want to allow users to mix in their own implementations of palettes, color clash resolvers, color- and image distance measures, etc. 

#### Law of Demeter
I don't follow the [Law of Demeter](https://dzone.com/articles/the-genius-of-the-law-of-demeter) strictly, but its basic principle of keeping dependency chains short, paired with avoiding to leak internal state, is good practice to keep the library in a shape that can be changed well. For this reason I do delegate a lot and I do defensive copies a lot. 

#### Mutability
I don't like mutable objects, but for performance and readability reasons it is sometimes hard to stay immutable.  
However, code correctness usually trumps speed and memory consumption.  
Just like the *String* object in Java which is a basic building block, the *C64Char* object is immutable, so it can be safely shared.
Most model objects are mutable however. Since sharing mutable data between objects can cause heavy problems, 
one of the key stategies used throughout the library is making **defensive copies**. This is to avoid nasty surprises, like a later pipeline-stage modifying an earlier result that might be buffered. 
There might be places where I was not overly strict with this, but I'm looking forward to either clean them up or at least keep 'em few and rare.
If you're asking yourself if you should make something mutable or share mutable live data, you're probably just being lazy. Think again.

#### Streams
I'm making extensive use of Java streams, even if sometimes a for-loop would be just as easy.  
There are two main reasons for that:  
1. It's often more readable, even if it's shit to debug. I expect coming Java releases to improve the readability of traces and IDE support to become better too, by public demand.   
Still, if the code is fast enough, short and easy to read, lambdas are my primary choice.
1. Project Loom (Lightweight Java threads, a.k.a. Fibers) will significantly boost the performance of parallelized lambdas, making them beneficial at lower thresholds.   
The code change to use lightweight threads with lambdas is very small compared to transforming for-loops, so why not start using streams and lambdas in the first place?  

For some lightweight pipeline for-loops are still the best choice, so I don't fully refrain from using them. Loop unrolling is a powerful compiler tool!  
The JEP 338: Vector API will probably open quite a few new opportunities for these lightweigt loops that don't do a lot but primitive-to-primitive-mapping, value shifting and stuff, and they seem to be mainly driven using for-loops, looking at the material publicized to far.

### Licensing

This library is published under the [MIT License](https://opensource.org/licenses/MIT)

Copyright (c) 2020 Wanja Gayk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
